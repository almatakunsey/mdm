﻿using Microsoft.EntityFrameworkCore;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Projection;


namespace Vtmis.Core.VesselEntityFrameworkCore
{
    public class VtsDatabaseContext : DbContext
    {
        public VtsDatabaseContext()
        {
            VtsConnectionString = AppHelper.GetVtsProdDbConnectionString();
        }
        public VtsDatabaseContext(DbContextOptions<VtsDatabaseContext> options)
       : base(options)
        {

            //this.sqlConnectionString = sqlConnectionString;

        }

        public string PostFixDbName { get; private set; }

        protected string VtsConnectionString { get; set; }
        public string Username { get; private set; }
        public string Password { get; private set; }

        public VtsDatabaseContext(string vtsConnectionString) : base()
        {
            this.VtsConnectionString = vtsConnectionString;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Ship>()
            .Property(f => f.ShipID)
            .ValueGeneratedOnAdd();

            modelBuilder.Entity<ShipList>()
                .HasKey(k => k.ShipListID);

            modelBuilder.Entity<ShipList>()
               .HasIndex(i => new { i.ShipListID, i.RecvTime, i.Latitude, i.Longitude, i.MMSI, i.IMO, i.CallSign });
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(this.VtsConnectionString, builder => builder.UseRowNumberForPaging());
            }
            //var connectionString = configuration["sqlserver_ip"]; //ConfigurationManager.AppSettings["sqlserver_ip"].ToString();

            //if (!optionsBuilder.IsConfigured)
            //{
            //    //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
            //    optionsBuilder.UseSqlServer(sqlConnectionString);
            //}
        }
        public DbSet<ExtendedShipList> ExtendedShipLists { get; set; }
        public DbSet<ShipList> ShipLists { get; set; }
        public DbSet<Ship> Ships { get; set; }
        public DbSet<Flag> Flags { get; set; }
        public DbSet<Photo> Photos { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Alarm> Alarms { get; set; }
        public DbSet<Zone> Zones { get; set; }
        public DbSet<MethInfo> MethInfos { get; set; }
        public DbSet<Draught> Draughts { get; set; }
        public DbSet<Aton> Atons { get; set; }
        public DbSet<AxShip> AxShips { get; set; }

        public DbSet<ShipTypes> ShipTypes { get; set; }


        //Projection
        public DbQuery<WeatherInstruments> WeatherInstruments { get; set; }
        public DbQuery<WeatherStationQuery> WeatherStationQuery { get; set; }
    }
}