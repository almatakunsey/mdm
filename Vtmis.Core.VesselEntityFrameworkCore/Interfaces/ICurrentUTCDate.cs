﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace CleanArchitecture.Domain.Interfaces
{
    public interface ICurrentUTCDate
    {
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        DateTime ReceivedTime { get; set; }
    }
}
