﻿namespace CleanArchitecture.Domain.Interfaces
{
    public interface ICommonProp
    {
        string StationId { get; set; }
        int MMSI { get; set; }
        string Name { get; set; }
        int? RAIM { get; set; }
        int? MessageId { get; set; }
        string RawData { get; set; }
    }
}
