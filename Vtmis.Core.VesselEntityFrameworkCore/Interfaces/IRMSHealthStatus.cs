﻿using CleanArchitecture.Domain.Entities;

namespace CleanArchitecture.Domain.Interfaces
{
    public interface IRMSHealthStatus
    {
        string Status { get; set; }
        ValDesc<int?> RMSLight { get; set; }
        ValDesc<int?> RMSRACON { get; set; }
        ValDesc<int?> RMSHealth { get; set; }
    }
}
