﻿using CleanArchitecture.Domain.Entities;

namespace CleanArchitecture.Domain.Interfaces
{
    public interface ICoordinate
    {
        Coordinate Coordinate { get; set; }
    }
}
