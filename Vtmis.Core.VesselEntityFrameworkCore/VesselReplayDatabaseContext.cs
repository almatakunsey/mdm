﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Vtmis.Core.VesselEntityFrameworkCore.Models;

namespace Vtmis.Core.VesselEntityFrameworkCore
{
    public class VesselReplayDatabaseContext :DbContext
    {
        public VesselReplayDatabaseContext(string postFixDbName)
        {
            PostFixDbName = postFixDbName;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer($"Data Source=localhost;Initial Catalog=AIS{PostFixDbName};Integrated Security=False;User Id=vessel_sa;Password=p@ssw0rd123;MultipleActiveResultSets=True");
            }
        }

        public DbSet<ShipPosition> ShipPositions { get; set; }

        public DbSet<ShipStatic> ShipStatics { get; set; }

        public string PostFixDbName { get; }
    }
}
