﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using Vtmis.Core.Common.Constants.Mongo;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.VesselEntityFrameworkCore
{
    public class MongoDbContext
    {
        public IMongoDatabase Database { get; set; }
        private MongoClient _client;
        public MongoDbContext()
        {
            //if (AppHelper.GetEnvironmentName().IsEqual(HostingEnvironment.LOCAL))
            //{
            //_client = new MongoClient("mongodb://10.1.2.13:27017");
            //}
            //else
            //{
            var mongoDbPort = 27017;
            List<MongoServerAddress> servers = new List<MongoServerAddress>();
            foreach (var server in Convert.ToString(AppHelper.GetMongoServers()).Split(","))
            {
                servers.Add(new MongoServerAddress(server, mongoDbPort));
            }
            var settings = new MongoClientSettings
            {
                Servers = servers,
                ConnectionMode = ConnectionMode.ReplicaSet,
                ReplicaSetName = AppHelper.GetReplicaSet(),
                Credential = MongoCredential.CreateCredential(AppHelper.GetMongoDbAuth(), AppHelper.GetMongoUsername(), AppHelper.GetMongoPassword())
            };
            //_client = new MongoClient("mongodb://root:VisitDK_2019@s-zf8565c7c29c4124-pub.mongodb.kualalumpur.rds.aliyuncs.com:3717/admin");
            _client = new MongoClient(settings);
            //}
            Database = _client.GetDatabase(MongoConst.AIS_DB);
        }
    }
}
