﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using System;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;

namespace Vtmis.Core.VesselEntityFrameworkCore
{

    public class VesselDatabaseContext : DbContext
    {
        string sqlConnectionString;
        //public static readonly LoggerFactory MyLoggerFactory
        //         = new LoggerFactory(new[]
        //         {
        //            new ConsoleLoggerProvider((category, level)
        //                => category == DbLoggerCategory.Database.Command.Name
        //                   && level == LogLevel.Information, true)
        //         });

        public VesselDatabaseContext(string postFixDbName, string sqlConnectionString, string username, string password)
        {
            //connectionString = ConfigurationManager.AppSettings["sqlserver_ip"].ToString();

            PostFixDbName = postFixDbName;
            this.sqlConnectionString = sqlConnectionString;
            Username = username;
            Password = password;
            //Database.EnsureCreated();
        }

        public VesselDatabaseContext()
        {
            //Get current date
            sqlConnectionString = AppHelper.GetAisDbServerIp();
            PostFixDbName = CommonHelper.GetAisDbNameFromDate(DateTime.Now);
            Username = AppHelper.GetAisDbUserId();
            Password = AppHelper.GetAisDbPassword();
        }

        public VesselDatabaseContext(DateTime aisDbDate)
        {
            //Get current date
            sqlConnectionString = AppHelper.GetAisDbServerIp();
            PostFixDbName = CommonHelper.GetAisDbNameFromDate(aisDbDate);
            Username = AppHelper.GetAisDbUserId();
            Password = AppHelper.GetAisDbPassword();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ShipPosition>()
                 .HasKey(c => new { c.ShipPosID, c.RecvTime });
            modelBuilder.Entity<ShipPosition>()
                .HasIndex(i => new { i.ShipPosID, i.RecvTime, i.Latitude, i.Longitude, i.MMSI });

            modelBuilder.Entity<ShipStatic>()
                 .HasKey(c => new { c.ShipStatID });
            modelBuilder.Entity<ShipStatic>()
                .HasIndex(i => new { i.ShipStatID, i.Name, i.MMSI, i.CallSign, i.IMO });
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            //var connectionString = configuration["sqlserver_ip"]; //ConfigurationManager.AppSettings["sqlserver_ip"].ToString();

            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder
                    //.UseLoggerFactory(MyLoggerFactory)
                    .UseSqlServer($"Server={sqlConnectionString};Database={PostFixDbName};User Id={Username};Password={Password};MultipleActiveResultSets=True;ConnectRetryCount=5",
                    b => b.UseRowNumberForPaging());
            }
        }

        public DbSet<ShipPosition> ShipPositions { get; set; }
        public DbSet<ShipStatic> ShipStatics { get; set; }
        public DbSet<AtonMonData> AtonMonDatas { get; set; }
        public DbSet<AtonPosData> AtonPosDatas { get; set; }
        public DbSet<AtonMetData> AtonMetDatas { get; set; }
        public DbSet<ReportDetail> ReportDetails { get; set; }

        //Projection
        public DbQuery<QueryShipPosStatic> QueryShipPosStatics { get; set; }
        public DbQuery<QueryMetStaticAtonPos> QueryMetStaticAtonPos { get; set; }
        public DbQuery<QueryUpdateTarget> QueryUpdateTarget { get; set; }
        public DbQuery<QueryAllShipPosStatic> QueryAllShipPosStatics { get; set; }
        //public DbQuery<AtonList> AtonLists { get; set; }
        //public DbQuery<AlarmDemo> Demo { get; set; }
        //public DbQuery<SP> SPs { get; set; }
        public DbQuery<QueryAtonList> QueryAtonLists { get; set; }
        public DbQuery<QueryMethydroList> QueryMethydroLists { get; set; }
        public DbQuery<QueryMonList> QueryMonLists { get; set; }
        public DbQuery<QueryAlarmTrigger> QueryAlarmTriggers { get; set; }

        //Props
        public string PostFixDbName { get; }
        public string Username { get; }
        public string Password { get; }
    }

    public class AlarmDemo
    {
        public int MMSI { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime RecvTime { get; set; }
        public DateTime LocalRecvTime { get; set; }
    }
}
