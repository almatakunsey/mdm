﻿using System.ComponentModel.DataAnnotations;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Mmdis.Dto
{
    public class IdMmsiImoCallSign
    {
        public int TargetId { get; set; }
        [Required]
        public int Mmsi { get; set; }
        [Required]
        public string Imo { get; set; }
        [Required]
        public string CallSign { get; set; }
    }
}
