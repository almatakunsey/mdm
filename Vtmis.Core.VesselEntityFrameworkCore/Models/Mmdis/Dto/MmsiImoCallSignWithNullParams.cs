﻿namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Mmdis.Dto
{
    public class MmsiImoCallSignWithNullParams
    {
        public int? TargetId { get; set; }
        public int? Mmsi { get; set; }
        public int? Imo { get; set; }
        public string CallSign { get; set; }
    }
}
