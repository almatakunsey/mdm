﻿using CleanArchitecture.Domain.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace CleanArchitecture.Domain.Entities
{
    public class Target : ICommonProp, ICurrentUTCDate, ICoordinate
    {
        [BsonIgnoreIfDefault]
        public ObjectId? Id { get; set; }      
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime ReceivedTime { get; set; }
        public string StationId { get; set; }
        public int MMSI { get; set; }
        public int? MessageId { get; set; }
        public string RawData { get; set; }     
        public string Name { get; set; }      
        public int? RAIM { get; set; }
        public Coordinate Coordinate { get; set; }
        public TargetType TargetType { get; set; }
        public Vessel Vessel { get; set; }
        public AtoN AtoN { get; set; }
        public BaseStation BaseStation { get; set; }
        public SAR SAR { get; set; }
        public BinaryMessage BinaryMessage { get; set; } // msg 6 or 8.. will replace previous data if exist
        public bool DataComplete { get; set; }
    }
}
