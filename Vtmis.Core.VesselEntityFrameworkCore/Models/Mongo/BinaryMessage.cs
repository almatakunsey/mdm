﻿using CleanArchitecture.Domain.Interfaces;
using System;

namespace CleanArchitecture.Domain.Entities
{
    public class BinaryMessage : ICurrentUTCDate
    {
        public DateTime ReceivedTime { get; set; }
        public int? DAC { get; set; }
        public int? FI { get; set; }
        public string BinaryData { get; set; }        
    }
}
