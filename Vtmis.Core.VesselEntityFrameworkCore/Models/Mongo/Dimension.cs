﻿namespace CleanArchitecture.Domain.Entities
{
    public class Dimension
    {
        public int? DimensionA { get; set; }
        public int? DimensionB { get; set; }
        public int? DimensionC { get; set; }
        public int? DimensionD { get; set; }
    }
}
