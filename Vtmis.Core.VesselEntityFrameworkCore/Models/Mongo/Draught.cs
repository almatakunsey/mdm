﻿using CleanArchitecture.Domain.Interfaces;
using System;

namespace CleanArchitecture.Domain.Entities
{
    public class Draught : ICurrentUTCDate, ICoordinate
    {
        public DateTime ReceivedTime { get; set; }
        public int? DAC { get; set; }
        public int? FI { get; set; }
        public string BinaryData { get; set; }
        public AirDraught AirDraught { get; set; }
        public Single? Battery { get; set; } // Voltage
        public ValDesc<bool?> Supply { get; set; } // Ship Load?
        public ValDesc<bool?> Half { get; set; } // 1/2 Full? Loaded
        public ValDesc<bool?> Cover { get; set; } // Casing Cover : true (Open) : false (Closed)
        public Hopper Hopper { get; set; }
        public ValDesc<int?> Sonar { get; set; } // Sonar : true (ok) : false (??)       
        public Coordinate Coordinate { get; set; }
    }
}
