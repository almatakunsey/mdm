﻿namespace CleanArchitecture.Domain.Entities
{
    public class ValDesc<T>
    {
        public T Value { get; set; }
        public string Description { get; set; }
    }
}
