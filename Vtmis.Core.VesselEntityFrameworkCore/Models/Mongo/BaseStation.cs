﻿namespace CleanArchitecture.Domain.Entities
{
    public class BaseStation
    {
        public int? PositionFixingDevice { get; set; }
        public string SubMessage { get; set; }
    }
}
