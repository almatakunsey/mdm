﻿namespace CleanArchitecture.Domain.Entities
{
    public class Coordinate
    {        
        public double? Altitude { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string GeoJson
        {
            get; set;
        }
    }
}
