﻿namespace CleanArchitecture.Domain
{
    public enum TargetType
    {
        Vessel,
        Aton,
        BaseStation,
        Methydro,
        RMS,
        SART,
        SAR,
        None
    }
}
