﻿using System;

namespace CleanArchitecture.Domain.Entities.RemoteMonitor
{
    public class RMS6
    {
        public float? AnalogueInt { get; set; }
        public float? AnalogueExt1 { get; set; }
        public float? AnalogueExt2 { get; set; }
        public short? DigitalInput { get; set; }
        public short? OffPosition68 { get; set; }
        public short? Health { get; set; }
        public short? Light { get; set; }
        public short? RACON { get; set; }
        public DateTime RecvTime { get; set; }
        public short? DAC { get; set; }
        public short? FI { get; set; }
        public float? VoltageData { get; set; }
        public float? CurrentData { get; set; }
        public short? Battery { get; set; }
        public short? PowerSupplyType { get; set; }
    }
}
