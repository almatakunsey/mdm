﻿namespace CleanArchitecture.Common.RMS
{
    public enum RMSTypes
    {
        IALA,
        LightBeacon,
        Lighthouse,
        SelfContainedLantern,
        ZeniLite,
        None
    }
}