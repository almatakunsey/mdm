﻿using CleanArchitecture.Domain.Interfaces;

namespace CleanArchitecture.Domain.Entities.RemoteMonitor
{
    public class RMStatus : IRMSHealthStatus
    {
        public string BinaryStatus { get; set; }
        public string Status { get; set; }
        public ValDesc<int?> RMSLight { get; set; }
        public ValDesc<int?> RMSRACON { get; set; }
        public ValDesc<int?> RMSHealth { get; set; }
    }
}
