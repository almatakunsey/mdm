﻿namespace CleanArchitecture.Domain.Entities.RemoteMonitor
{
    public class RMSLightBeaconApp
    {
        public Analogue Analogue { get; set; }
        public RMStatus RMStatus { get; set; }
        public ValDesc<int?> Beat { get; set; }
        public ValDesc<int?> LanternBatt { get; set; }
        public ValDesc<int?> Lantern { get; set; }
        public ValDesc<int?> Ambient { get; set; }
        public ValDesc<int?> Door { get; set; }
        public ValDesc<int?> OffPosition { get; set; }       
    }
}
