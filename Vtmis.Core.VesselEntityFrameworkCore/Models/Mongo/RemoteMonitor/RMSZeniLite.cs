﻿namespace CleanArchitecture.Domain.Entities.RemoteMonitor
{
    public class RMSZeniLite
    {
        public double? Voltage { get; set; }
        public double? Current { get; set; }
        public ValDesc<int?> PowerSupplyType { get; set; }
        public ValDesc<int?> Light { get; set; }
        public ValDesc<int?> BatteryStatus { get; set; }
        public ValDesc<int?> OffPosition { get; set; }
    }
}
