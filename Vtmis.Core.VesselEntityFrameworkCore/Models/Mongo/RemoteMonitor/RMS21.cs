﻿using System;

namespace CleanArchitecture.Domain.Entities.RemoteMonitor
{
    public class RMS21
    {
        public short? Health21 { get; set; }
        public short? Light21 { get; set; }
        public short? RACON21 { get; set; }
    }
}
