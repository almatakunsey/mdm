﻿using CleanArchitecture.Common.RMS;
using CleanArchitecture.Domain.Interfaces;
using System;

namespace CleanArchitecture.Domain.Entities.RemoteMonitor
{
    public class RMS : ICurrentUTCDate
    {
        public DateTime ReceivedTime { get; set; }
        public int? DAC { get; set; }
        public int? FI { get; set; }
        public string BinaryData { get; set; }
        public RMSTypes RMSType { get; set; }
        public RMSLightBeaconApp RMSLightBeaconApp { get; set; }
        public RMSLighthouse RMSLighthouse { get; set; }
        public RMSIALA RMSIALA { get; set; }
        public RMSSelfContainedLantern RMSSelfContainedLantern { get; set; }
        public RMSZeniLite RMSZeniLite { get; set; }
    }
}
