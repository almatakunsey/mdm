﻿namespace CleanArchitecture.Domain.Entities.RemoteMonitor
{
    public class RMSIALA
    {
        public Analogue Analogue { get; set; }
        public string DigitalInput { get; set; }
        public RMStatus RMStatus { get; set; }
        public ValDesc<int?> OffPosition { get; set; }
    }
}
