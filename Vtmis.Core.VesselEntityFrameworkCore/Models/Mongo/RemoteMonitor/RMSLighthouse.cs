﻿namespace CleanArchitecture.Domain.Entities.RemoteMonitor
{
    public class RMSLighthouse
    {
        public Analogue Analogue { get; set; }
        public ValDesc<int?> OffPosition { get; set; }
        public ValDesc<int?> LightSensor { get; set; }
        public ValDesc<int?> Beat { get; set; }
        public ValDesc<int?> MainLanternCondition { get; set; }
        public ValDesc<int?> MainLanternStatus { get; set; }
        public ValDesc<int?> StandbyLanternCondition { get; set; }
        public ValDesc<int?> StandbyLanternStatus { get; set; }
        public ValDesc<int?> EmergencyLanternCondition { get; set; }
        public ValDesc<int?> EmergencyLanternStatus { get; set; }
        public ValDesc<int?> OpticDriveAStatus { get; set; }
        public ValDesc<int?> OpticDriveACondition { get; set; }
        public ValDesc<int?> OpticDriveBStatus { get; set; }
        public ValDesc<int?> OpticDriveBCondition { get; set; }
        public ValDesc<int?> HatchDoor { get; set; }
        public ValDesc<int?> MainPower { get; set; }
        public ValDesc<int?> BMSCondition { get; set; } // Regulator or Battery Management System
        public RMStatus RMStatus { get; set; }
    }
}
