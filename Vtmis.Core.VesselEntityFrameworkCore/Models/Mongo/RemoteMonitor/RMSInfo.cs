﻿using CleanArchitecture.Common.RMS;
using CleanArchitecture.Domain.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace CleanArchitecture.Domain.Entities.RemoteMonitor
{
    public class RMSInfo : ICurrentUTCDate, ICoordinate
    {
        [BsonIgnoreIfDefault]
        public ObjectId? Id { get; set; }
        public int MMSI { get; set; }
        public string StationId { get; set; }
        public DateTime ReceivedTime { get; set; }
        public string Name { get; set; }
        public Coordinate Coordinate { get; set; }
        public int MessageId { get; set; }
        public int? DAC { get; set; }
        public int? FI { get; set; }
        public string BinaryData { get; set; }
        public RMSTypes RMSType { get; set; }
        public RMSLightBeaconApp RMSLightBeaconApp { get; set; }
        public RMSLighthouse RMSLighthouse { get; set; }
        public RMSIALA RMSIALA { get; set; }
        public RMSSelfContainedLantern RMSSelfContainedLantern { get; set; }
        public RMSZeniLite RMSZeniLite { get; set; }
    }
}
