﻿namespace CleanArchitecture.Domain.Entities.RemoteMonitor
{
    public class RMSSelfContainedLantern
    {
        public Analogue Analogue { get; set; }
        public RMStatus RMStatus { get; set; }
        public ValDesc<int?> OffPosition { get; set; }

        public ValDesc<int?> LightSensor { get; set; }
        public ValDesc<int?> Beat { get; set; }
        public ValDesc<int?> AlarmActive { get; set; }
        public ValDesc<int?> FlasherOffPowerThres { get; set; }
        public ValDesc<int?> FlasherOffLowVin { get; set; }
        public ValDesc<int?> FlasherOffPhotoCell { get; set; }
        public ValDesc<int?> FlasherOffTemperature { get; set; }
        public ValDesc<int?> FlasherOffForceOff { get; set; }
        public ValDesc<int?> IsNight { get; set; }
        public ValDesc<int?> ErrorLedShort { get; set; }
        public ValDesc<int?> ErrorLedOpen { get; set; }
        public ValDesc<int?> ErrorLedVoltageLow { get; set; }
        public ValDesc<int?> ErrorVinLow { get; set; }
        public ValDesc<int?> ErrorPowerThres { get; set; }
        public ValDesc<int?> FlasherAdjToLed { get; set; }
        public ValDesc<int?> GSensorInterruptOccured { get; set; }
        public ValDesc<int?> SolarChargingOn { get; set; }
    }
}
