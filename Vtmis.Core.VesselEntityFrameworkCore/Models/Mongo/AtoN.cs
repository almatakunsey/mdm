﻿using CleanArchitecture.Domain.Entities.Methydro;
using CleanArchitecture.Domain.Entities.RemoteMonitor;

namespace CleanArchitecture.Domain.Entities
{
    public class AtoN
    {
        public int? AtonType { get; set; }
        public int? PositionAccuracy { get; set; }
        public Dimension Dimension { get; set; }//
        public int? PositionFixingDevice { get; set; }
        public int? OffPosition { get; set; }
        public int? VirtualAton { get; set; }
        public string NameExtension { get; set; }
        public int? RegionalReserved { get; set; }
        public Instruments Instruments { get; set; }
        public RMS RMS { get; set; }//
        public RMStatus AtoNStatus { get; set; }     //      
    }
}
