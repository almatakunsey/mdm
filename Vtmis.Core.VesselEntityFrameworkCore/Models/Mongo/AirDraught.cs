﻿using System;

namespace CleanArchitecture.Domain.Entities
{
    public class AirDraught
    {
        public Single? Empty { get; set; } // Max Air Draught
        public Single? Full { get; set; } //  Min Air Draught
        public Single? Actual { get; set; } // Actual Air Draught
    }
}
