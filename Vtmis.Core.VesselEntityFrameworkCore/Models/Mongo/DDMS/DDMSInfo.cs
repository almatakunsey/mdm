﻿using CleanArchitecture.Domain.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace CleanArchitecture.Domain.Entities.DDMS
{
    public class DDMSInfo : ICurrentUTCDate, ICoordinate
    {
        [BsonIgnoreIfDefault]
        public ObjectId? Id { get; set; }
        public int MMSI { get; set; }
        public string StationId { get; set; }
        public DateTime ReceivedTime { get; set; } // last received instruments
        public string Name { get; set; }
        public Coordinate Coordinate { get; set; }
        public int MessageId { get; set; }
        public int? DAC { get; set; }
        public int? FI { get; set; }
        public AirDraught AirDraught { get; set; }
        public Single? Battery { get; set; } // Voltage
        public ValDesc<bool?> Supply { get; set; } // Ship Load?
        public ValDesc<bool?> Half { get; set; } // 1/2 Full?
        public ValDesc<bool?> Cover { get; set; } // Casing Cover : true (Open) : false (Closed)
        public Hopper Hopper { get; set; }
        public ValDesc<int?> Sonar { get; set; } // Sonar : true (ok) : false (??)       
    }
}
