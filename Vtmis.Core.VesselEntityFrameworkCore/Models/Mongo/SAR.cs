﻿namespace CleanArchitecture.Domain.Entities
{
    public class SAR
    {
        public double? SOG { get; set; }
        public int? PositionAccuracy { get; set; }
        public double? COG { get; set; }
        public int? RegionalReserved { get; set; }
        public int? DTE { get; set; }
        public string SubMessage { get; set; }
    }
}