﻿namespace CleanArchitecture.Domain.Entities
{
    public class Analogue
    {
        public float? AnalogueInt { get; set; }
        public float? AnalogueExt1 { get; set; }
        public float? AnalogueExt2 { get; set; }
    }
}
