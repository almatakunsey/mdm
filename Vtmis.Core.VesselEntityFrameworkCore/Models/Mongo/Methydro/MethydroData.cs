﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace CleanArchitecture.Domain.Entities.Methydro
{
    public class MethydroData
    {
        [BsonIgnoreIfDefault]
        public ObjectId? Id { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime? RecvTime_UTC { get; set; }
        public int MMSI { get; set; }
        public string MMSIExtension { get; set; }
        public int MessageId { get; set; }
        public Instruments Instruments { get; set; }
    }
}
