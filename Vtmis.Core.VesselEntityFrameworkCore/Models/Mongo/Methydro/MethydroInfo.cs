﻿using CleanArchitecture.Domain.Interfaces;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace CleanArchitecture.Domain.Entities.Methydro
{
    public class MethydroInfo : ICurrentUTCDate, ICoordinate
    {
        [BsonIgnoreIfDefault]
        public ObjectId? Id { get; set; }
        public int MMSI { get; set; }
        public string StationId { get; set; }
        public DateTime ReceivedTime { get; set; } // last received instruments
        public string Name { get; set; }
        public Coordinate Coordinate { get; set; }
        public int MessageId { get; set; }
        public int? DAC { get; set; }
        public int? FI { get; set; }        
        public Instruments Instruments { get; set; }
     
    }
}
