﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class SeaInstrument
    {
        public DoubleSensorData SeaState { get; set; }
        public DoubleSensorData Salinity { get; set; }
    }
}
