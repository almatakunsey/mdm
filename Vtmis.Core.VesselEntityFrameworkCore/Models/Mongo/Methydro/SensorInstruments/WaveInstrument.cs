﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class WaveInstrument
    {
        public DoubleSensorData SignificantWaveHeight { get; set; }
        public DoubleSensorData WavePeriod { get; set; }
        public DoubleSensorData WaveDirection { get; set; }
    }
}
