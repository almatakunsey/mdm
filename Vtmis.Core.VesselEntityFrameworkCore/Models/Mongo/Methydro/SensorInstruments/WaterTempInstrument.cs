﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class WaterTempInstrument
    {
        public DoubleSensorData WaterTemperature { get; set; }
    }
}
