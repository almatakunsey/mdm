﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class SurfaceCurrentInstrument2
    {
        public DoubleSensorData SurfaceCurrentSpeed2 { get; set; }
        public DoubleSensorData SurfaceCurrentDirection2 { get; set; }
    }
}
