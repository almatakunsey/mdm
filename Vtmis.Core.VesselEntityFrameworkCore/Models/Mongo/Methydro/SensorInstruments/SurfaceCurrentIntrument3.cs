﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class SurfaceCurrentIntrument3
    {
        public DoubleSensorData SurfaceCurrentSpeed3 { get; set; }
        public DoubleSensorData SurfaceCurrentDirection3 { get; set; }
    }
}
