﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class IceInstrument
    {
        public DoubleSensorData Ice { get; set; }       
    }
}
