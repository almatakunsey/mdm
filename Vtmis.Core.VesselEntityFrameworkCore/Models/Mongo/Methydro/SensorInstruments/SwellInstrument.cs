﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class SwellInstrument
    {
        public DoubleSensorData SwellHeight { get; set; }
        public DoubleSensorData SwellPeriod { get; set; }
        public DoubleSensorData SwellDirection { get; set; }
    }
}
