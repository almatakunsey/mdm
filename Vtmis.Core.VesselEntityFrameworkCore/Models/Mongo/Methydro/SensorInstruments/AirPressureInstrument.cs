﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class AirPressureInstrument
    {
        public DoubleSensorData AirPressure { get; set; }
        public DoubleSensorData AirPressureTendency { get; set; }
    }
}
