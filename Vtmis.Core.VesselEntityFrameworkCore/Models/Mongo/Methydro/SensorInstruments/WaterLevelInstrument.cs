﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class WaterLevelInstrument
    {
        public DoubleSensorData WaterLevel { get; set; }
        public DoubleSensorData WaterLevelTrend { get; set; }
    }
}
