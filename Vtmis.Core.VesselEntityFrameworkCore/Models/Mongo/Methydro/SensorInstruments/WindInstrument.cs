﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class WindInstrument
    {
        public DoubleSensorData AverageWindSpeed { get; set; }
        public DoubleSensorData WindGust { get; set; }
        public DoubleSensorData WindDirection { get; set; }
        public DoubleSensorData WindGustDirection { get; set; }
    }
}
