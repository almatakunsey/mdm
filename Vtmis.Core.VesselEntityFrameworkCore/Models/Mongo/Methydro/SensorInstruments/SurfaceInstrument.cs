﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class SurfaceInstrument
    {
        public SurfaceCurrentInstrument SurfaceCurrentInstrument { get; set; }
        public SurfaceCurrentInstrument2 SurfaceCurrentInstrument2 { get; set; }
        public SurfaceCurrentIntrument3 SurfaceCurrentInstrument3 { get; set; }
        public DoubleSensorData SurfaceMeasuringLevel2 { get; set; }      
        public DoubleSensorData SurfaceMeasuringLevel3 { get; set; }
    }
}
