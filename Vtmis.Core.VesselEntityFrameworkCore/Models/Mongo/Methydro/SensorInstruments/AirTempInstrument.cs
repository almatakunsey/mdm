﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class AirTempInstrument
    {
        public DoubleSensorData AirTemperature { get; set; }
        public DoubleSensorData RelativeHumidity { get; set; }
        public DoubleSensorData DewPoint { get; set; }
    }
}
