﻿namespace CleanArchitecture.Domain.Entities.Methydro.SensorInstruments
{
    public class SurfaceCurrentInstrument
    {
        public DoubleSensorData SurfaceCurrentSpeed { get; set; }
        public DoubleSensorData SurfaceCurrentDirection { get; set; }
    }
}
