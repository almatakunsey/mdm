﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace CleanArchitecture.Domain.Entities.Methydro
{
    public class BaseSensorData<T>
    {
        public T Value { get; set; }
        public string Description { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime? ReceivedTime { get; set; }
    }
}
