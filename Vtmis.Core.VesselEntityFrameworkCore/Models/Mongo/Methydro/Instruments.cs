﻿using CleanArchitecture.Domain.Entities.Methydro.SensorInstruments;
using CleanArchitecture.Domain.Interfaces;
using System;

namespace CleanArchitecture.Domain.Entities.Methydro
{
    public class Instruments : ICurrentUTCDate, ICoordinate
    {
        public DateTime ReceivedTime { get; set; } // last received instruments
        public int? DAC { get; set; }
        public int? FI { get; set; }
        public WindInstrument WindInstrument { get; set; }
        public AirTempInstrument AirTempInstrument { get; set; }
        public AirPressureInstrument AirPressureInstrument { get; set; }     
        public WaterLevelInstrument WaterLevelInstrument { get; set; }
        public SurfaceInstrument SurfaceInstrument { get; set; }
        public WaveInstrument WaveInstrument { get; set; }
        public SwellInstrument SwellInstrument { get; set; }       
        public WaterTempInstrument WaterTempInstrument { get; set; }
        public SeaInstrument SeaInstrument { get; set; }
        public IceInstrument IceInstrument { get; set; }
        public DoubleSensorData Precipitation { get; set; }
        public DoubleSensorData HorizontalVisibility { get; set; }
        public Coordinate Coordinate { get; set; }
    }
}
