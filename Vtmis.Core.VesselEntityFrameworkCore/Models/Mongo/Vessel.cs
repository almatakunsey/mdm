﻿using MongoDB.Bson.Serialization.Attributes;
using System;

namespace CleanArchitecture.Domain.Entities
{
    public class Vessel
    {
        public int? IMO { get; set; }
        public string CallSign { get; set; }
        public Dimension Dimension { get; set; }
        public int? ShipType { get; set; }
        public int? NavigationalStatus { get; set; }
        public int? RegionalReserved { get; set; }
        public double? ROT { get; set; }
        public double? SOG { get; set; }
        public double? COG { get; set; }
        public double? TrueHeading { get; set; }
        public int? PositionAccuracy { get; set; }
        public int? PositionFixingDevice { get; set; }
        public int? ManeuverIndicator { get; set; }
        public string SubMessage { get; set; }
        public double? DraughtVal { get; set; }
        public Draught Draught { get; set; }
        public string Destination { get; set; }
        public int? DTE { get; set; }
        public string Vendor { get; set; }
        public int? AISVersion { get; set; }
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc)]
        public DateTime? ETA { get; set; }
        public VesselClass VesselClass { get; set; }
    }
}
