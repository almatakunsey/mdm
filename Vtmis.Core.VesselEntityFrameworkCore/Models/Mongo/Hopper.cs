﻿using System;

namespace CleanArchitecture.Domain.Entities
{
    public class Hopper
    {
        public int? Hoppers { get; set; }
        public ValDesc<int?> HopperFlag { get; set; }
    }
}
