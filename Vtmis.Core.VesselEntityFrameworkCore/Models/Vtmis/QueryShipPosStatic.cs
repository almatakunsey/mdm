﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    public class QueryShipPosStatic
    {
        public int MMSI { get; set; }
        public double SOG { get; set; }
        public double COG { get; set; }
        public double TrueHeading { get; set; }
        public DateTime LocalRecvTime { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public Int16? ShipType { get; set; }
        public DateTime RecvTime { get; set; }
    }
}
