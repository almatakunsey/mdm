﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    public class QueryMonList
    {
        public int MMSI { get; set; }
        public string Name { get; set; }
        public string AisName { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public Int16? AtoNType { get; set; }
        public Int16? VirtualAtoN { get; set; }
        public Int16? OffPosition21 { get; set; }
        public Int16? OffPosition68 { get; set; }
        public DateTime? RecvTime6 { get; set; }
        public DateTime? RecvTime8 { get; set; }
        public DateTime? RecvTime21 { get; set; }

        public Int16? RACON { get; set; }
        public Int16? Light { get; set; }
        public Int16? Health { get; set; }
        public Int16? RACON21 { get; set; }
        public Int16? Light21 { get; set; }
        public Int16? Health21 { get; set; }
        public Single? AnalogueInt { get; set; }
        public Single? AnalogueExt1 { get; set; }
        public Single? AnalogueExt2 { get; set; }
        public byte? B0 { get; set; }
        public byte? B1 { get; set; }
        public byte? B2 { get; set; }
        public byte? B3 { get; set; }
        public byte? B4 { get; set; }
        public byte? B5 { get; set; }
        public byte? B6 { get; set; }
        public byte? B7 { get; set; }
        public Single? VoltageData { get; set; }
        public Single? CurrentData { get; set; }
        public Int16? Battery { get; set; }
        public Int16? PhotoCell { get; set; }
    }
}
