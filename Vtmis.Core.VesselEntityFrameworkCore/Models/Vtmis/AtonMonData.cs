﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models
{
    [Table("AtonMonData")]
    public class AtonMonData
    {
        [Key]
        public int MMSI { get; set; }
        public Single? AnalogueInt { get; set; }
        public Single? AnalogueExt1 { get; set; }
        public Single? AnalogueExt2 { get; set; }
        public Int16? DigitalInput { get; set; }
        public Int16? OffPosition68 { get; set; }
        public Int16? Health { get; set; }
        public Int16? Light { get; set; }
        public Int16? RACON { get; set; }
        public DateTime RecvTime { get; set; }
        public Int16? DAC { get; set; }
        public Int16? FI { get; set; }
        public byte? B2 { get; set; }
        public Single? VoltageData { get; set; }
        public Single? CurrentData { get; set; }
        public Int16? Battery { get; set; }
    }
}
