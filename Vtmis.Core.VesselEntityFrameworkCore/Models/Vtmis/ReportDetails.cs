﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models
{
    [Table("ReportDetailView")]
    public class ReportDetail
    {
        [Key]
        public Guid ViewID { get; set; }
        public decimal MinSpeed { get; set; }
        public decimal MaxSpeed { get; set; }
        public int ZoneId { get; set; }
        public string ZoneName { get; set; }
        public string ReportTypeNumber { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Radius { get; set; }
        [NotMapped]
        public double Distance { get; set; }
        [NotMapped]
        public double MaxLatitude { get; set; }
        [NotMapped]
        public double MinLatitude { get; set; }
        [NotMapped]
        public double MaxLongitude { get; set; }
        [NotMapped]
        public double MinLongitude { get; set; }

    }


}
