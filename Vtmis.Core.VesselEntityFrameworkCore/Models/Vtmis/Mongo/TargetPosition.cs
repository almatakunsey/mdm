﻿using CleanArchitecture.Domain.Entities;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using Vtmis.Core.Common.Helpers.Methydro;
using Vtmis.Core.Common.Helpers.RMS;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis.Mongo
{
    [BsonIgnoreExtraElements]
    public class TargetPosition
    {
        [BsonElement("_id")]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("RecvTime_UTC")]
        public DateTime? RecvTime_UTC { get; set; }
        [BsonElement("RecvTime_Msg6")]
        public DateTime? RecvTime_Msg6 { get; set; }
        [BsonElement("RecvTime_Msg8")]
        public DateTime? RecvTime_Msg8 { get; set; }
        [BsonIgnoreIfDefault]
        public MethydroResult Methydro { get; set; }
        [BsonIgnoreIfDefault]
        public RMS RMS { get; set; }
        [BsonIgnoreIfDefault]
        public Draught Draught { get; set; }
        [BsonElement("RawData")]
        public string RawData { get; set; }
        [BsonElement("MMSI")]
        public int? MMSI { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        [BsonElement("NameExtension")]
        public string NameExtension { get; set; }
        [BsonElement("MessageID")]
        public int? MessageId { get; set; }
        [BsonElement("Navigational_Status")]
        public int? NavigationalStatus { get; set; }
        [BsonElement("ROT")]
        public double? ROT { get; set; }
        [BsonElement("SOG")]
        public double? SOG { get; set; }
        [BsonElement("COG")]
        public double? COG { get; set; }
        [BsonElement("Position_Accuracy")]
        public int? PositionAccuracy { get; set; }
        [BsonElement("Altitude")]
        public double? Altitude { get; set; }
        [BsonElement("Latitude")]
        public double? Latitude { get; set; }
        [BsonElement("Longitude")]
        public double? Longitude { get; set; }
        [BsonElement("TrueHeading")]
        public int? TrueHeading { get; set; }


        [BsonElement("RAIM")]
        public int? RAIM { get; set; }
        [BsonElement("Position_Fixing_Device")]
        public int? PositionFixingDevice { get; set; }
        [BsonElement("IMO")]
        public int? IMO { get; set; }
        [BsonElement("CallSign")]
        public string CallSign { get; set; }
        [BsonElement("ShipType")]
        public int? ShipType { get; set; }
        [BsonElement("Destination")]
        public string Destination { get; set; }
        [BsonElement("Dimension_A")]
        public int? Dimension_A { get; set; }
        [BsonElement("Dimension_B")]
        public int? Dimension_B { get; set; }
        [BsonElement("Dimension_C")]
        public int? Dimension_C { get; set; }
        [BsonElement("Dimension_D")]
        public int? Dimension_D { get; set; }
        [BsonElement("OffPosition")]
        public int? OffPosition { get; set; }
        [BsonElement("VirtualAton")]
        public int? VirtualAton { get; set; }
        [BsonElement("ETA")]
        public DateTime? ETA { get; set; }
        [BsonElement("Max_Draught")]
        public double? MaxDraught { get; set; }
        [BsonElement("AtoNType")]
        public int? AtoNType { get; set; }
        [BsonElement("VendorId")]
        public string VendorId { get; set; }
        [BsonElement("DTE")]
        public int? DTE { get; set; }
        [BsonElement("AISVersion")]
        public int? AISVersion { get; set; }
        [BsonElement("DesignatedAreaCode")]
        public int? DesignatedAreaCode { get; set; }
        [BsonElement("FunctionalId")]
        public int? FunctionalId { get; set; }
        [BsonElement("BinaryData")]
        public string BinaryData { get; set; }
        [BsonElement("DataComplete")]
        public bool DataComplete { get; set; } = false;
    }
}
