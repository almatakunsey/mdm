﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis.Mongo
{
    public interface ITargetPosition
    {
        short? AtoNType { get; set; }
        string AtoNType_Desc { get; set; }
        string BinaryData { get; set; }
        string CallSign { get; set; }
        double? COG { get; set; }
        string COG_Desc { get; set; }
        bool DataComplete { get; set; }
        string Destination { get; set; }
        short? Dimension_A { get; set; }
        short? Dimension_B { get; set; }
        short? Dimension_C { get; set; }
        short? Dimension_D { get; set; }
        DateTime? ETA { get; set; }
        string ETA_Desc { get; set; }
        short? Health { get; set; }
        string Id { get; set; }
        int? IMO { get; set; }
        string IMO_Desc { get; set; }
        double? Latitude { get; set; }
        string Latitude_Desc { get; set; }
        double? Longitude { get; set; }
        string Longitude_Desc { get; set; }
        double? MaxDraught { get; set; }
        string MaxDraught_Desc { get; set; }
        short MessageId { get; set; }
        string MessageId_Desc { get; set; }
        int? MMSI { get; set; }
        string Name { get; set; }
        short? NavigationalStatus { get; set; }
        string NavigationalStatus_Desc { get; set; }
        short? OffPosition21 { get; set; }
        string OffPosition21_Desc { get; set; }
        short? PositionAccuracy { get; set; }
        string PositionAccuracy_Desc { get; set; }
        short? PositionFixingDevice { get; set; }
        string PositionFixingDevice_Desc { get; set; }
        short? RAIM { get; set; }
        string RAIM_Desc { get; set; }
        DateTime? RecvTime_Local { get; set; }
        DateTime? RecvTime_UTC { get; set; }
        DateTime? UniversalTime { get; }
        double? ROT { get; set; }
        string ROT_Desc { get; set; }
        string Safety_Related_Text { get; set; }
        short? ShipType { get; set; }
        string ShipType_Desc { get; set; }
        double? SOG { get; set; }
        string SOG_Desc { get; set; }
        short? TrueHeading { get; set; }
        string TrueHeading_Desc { get; set; }
        string Vendor { get; set; }
    }
}