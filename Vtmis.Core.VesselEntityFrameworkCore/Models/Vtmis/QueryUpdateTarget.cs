﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    public class RMSDTO
    {
        public int? LanternBatt { get; set; }
        public int? BatteryStat { get; set; } // zeni lite only
        public int Id { get; set; }
        public double? Health21 { get; set; }
        public double? Light21 { get; set; }
        public double? RACON21 { get; set; }
        public double? AnalogueExt1 { get; set; }
        public double? AnalogueExt2 { get; set; }
        public double? AnalogueInt { get; set; }
        public double? OffPosition68 { get; set; }
        public double? Health { get; set; }
        public double? Light { get; set; }
        public double? RACON { get; set; }
        public double? OffPosition21 { get; set; }
        public string DigitalInput { get; set; }
        public string AtonStatus { get; set; }
        public string AtonStatus68 { get; set; }
        public string OffPosition { get; set; }
    }
    public class QueryUpdateTarget
    {
        public bool IsMethydro { get; set; }
        public string StationId { get; set; }
        public RMSDTO RMS { get; set; }
        public int? DAC { get; set; }
        public int? FI { get; set; }
        public int Total { get; set; }
        public int MMSI { get; set; }
        public DateTime RecvTime { get; set; }
        public int ShipPosID { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public DateTime? LocalRecvTime { get; set; }
        public double? ROT { get; set; }
        public double? SOG { get; set; }
        public double? COG { get; set; }
        public Int16? TrueHeading { get; set; }
        public Int16 MessageId { get; set; }
        public Int16? PositionAccuracy { get; set; }
        public Int16? RAIM { get; set; }
        public string IP { get; set; }
        public Int16? NavigationalStatus { get; set; }
        public Int16? VirtualAtoN { get; set; }
        public int? AtonPosMMSI { get; set; }    
     
        public int? MetMMSI { get; set; }
        public string Name { get; set; }
        public string CallSign { get; set; }
        public string Destination { get; set; }
        public string Vendor { get; set; }
        public int? IMO { get; set; }
        public Int16? PositionFixingDevice { get; set; }
        public DateTime? ETA { get; set; }
        public int? Dimension_A { get; set; }
        public int? Dimension_B { get; set; }
        public int? Dimension_C { get; set; }
        public int? Dimension_D { get; set; }
        public Int16? AtoNType { get; set; }
        public double? MaxDraught { get; set; }
        public Int16? ShipType { get; set; }        
    }
}
