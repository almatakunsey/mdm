﻿using System;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    public class QueryAllShipPosStatic
    {       
        public int MMSI { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        //public Int16 TrueHeading { get; set; }
        public DateTime? LocalRecvTime { get; set; }
        public double? SOG { get; set; }
        //public double? ROT { get; set; }
        public double? COG { get; set; }
        public DateTime RecvTime { get; set; }
        public Int16 MessageId { get; set; }
        //public Int16 RAIM { get; set; }
        public Int16? NavigationalStatus { get; set; }
        public string NavigationalStatusToString
        {
            get
            {
                return TargetHelper.GetNavStatusDescriptionByNavStatusId(NavigationalStatus);
            }
        }
        //public DateTime? UTC { get; set; }
        //public string IP { get; set; }
        //public Int16? PositionAccuracy { get; set; }
     
        public string Name { get; set; }
        public string CallSign { get; set; }
        //public string Vendor { get; set; }
        public string Destination { get; set; }
        public int? IMO { get; set; }
        public Int16? ShipType { get; set; }
        //public Int16? AISVersion { get; set; }
        //public Int16? AtoNType { get; set; }
        //public Int16? VirtualAtoN { get; set; }
        public DateTime? ETA { get; set; }
        //public double? MaxDraught { get; set; }
        public Int16? Dimension_A { get; set; }
        public Int16? Dimension_B { get; set; }
        public Int16? Dimension_C { get; set; }
        public Int16? Dimension_D { get; set; }
        //public Int16? PositionFixingDevice { get; set; }
    }
}
