﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    public class QueryUpdateTargetMongoDb
    {
        [BsonElement("_id")]
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("MMSI")]
        public int? MMSI { get; set; }
        [BsonElement("Name")]
        public string Name { get; set; }
        [BsonElement("MessageID")]
        public Int16 MessageId { get; set; }
        [BsonElement("MessageID_Desc")]
        public string MessageId_Desc { get; set; }
        [BsonElement("Navigational_Status")]
        public Int16? NavigationalStatus { get; set; }
        [BsonElement("Navigational_Status_Desc")]
        public string NavigationalStatus_Desc { get; set; }
        [BsonElement("ROT")]
        public double? ROT { get; set; }
        [BsonElement("ROT_Desc")]
        public string ROT_Desc { get; set; }
        [BsonElement("SOG")]
        public double? SOG { get; set; }
        [BsonElement("SOG_Desc")]
        public string SOG_Desc { get; set; }
        [BsonElement("COG")]
        public double? COG { get; set; }
        [BsonElement("COG_Desc")]
        public string COG_Desc { get; set; }
        [BsonElement("Position_Accuracy")]
        public Int16? PositionAccuracy { get; set; }
        [BsonElement("Position_Accuracy_Desc")]
        public string PositionAccuracy_Desc { get; set; }
        [BsonElement("Latitude")]
        public double? Latitude { get; set; }
        [BsonElement("Latitude_Desc")]
        public string Latitude_Desc { get; set; }
        [BsonElement("Longitude")]
        public double? Longitude { get; set; }
        [BsonElement("Longitude_Desc")]
        public string Longitude_Desc { get; set; }
        [BsonElement("TrueHeading")]
        public Int16? TrueHeading { get; set; }
        [BsonElement("TrueHeading_Desc")]
        public string TrueHeading_Desc { get; set; }
       
        [BsonElement("RecvTime_Local")]
        public DateTime? RecvTime_Local { get; set; }
        [BsonElement("RAIM")]
        public Int16? RAIM { get; set; }
        [BsonElement("RAIM_Desc")]
        public string RAIM_Desc { get; set; }
        [BsonElement("Position_Fixing_Device")]
        public Int16? PositionFixingDevice { get; set; }
        [BsonElement("Position_Fixing_Device_Desc")]
        public string PositionFixingDevice_Desc { get; set; }
        [BsonElement("IMO")]
        public int? IMO { get; set; }
        [BsonElement("IMO_Desc")]
        public string IMO_Desc { get; set; }
        [BsonElement("CallSign")]
        public string CallSign { get; set; }
        [BsonElement("ShipType")]
        public Int16? ShipType { get; set; }
        [BsonElement("ShipType_Desc")]
        public string ShipType_Desc { get; set; }
        [BsonElement("Destination")]
        public string Destination { get; set; }
        [BsonElement("Dimension_A")]
        public Int16? Dimension_A { get; set; }
        [BsonElement("Dimension_B")]
        public Int16? Dimension_B { get; set; }
        [BsonElement("Dimension_C")]
        public Int16? Dimension_C { get; set; }
        [BsonElement("Dimension_D")]
        public Int16? Dimension_D { get; set; }
        [BsonElement("ETA")]
        public DateTime? ETA { get; set; }
        [BsonElement("ETA_Desc")]
        public string ETA_Desc { get; set; }
        [BsonElement("Max_Draught")]
        public double? MaxDraught { get; set; }
        [BsonElement("Max_Draught_Desc")]
        public string MaxDraught_Desc { get; set; }
        [BsonElement("AtoNType")]
        public Int16? AtoNType { get; set; }
        [BsonElement("AtoNType_Desc")]
        public string AtoNType_Desc { get; set; }
        [BsonElement("OffPosition")]
        public Int16? OffPosition21 { get; set; }
        [BsonElement("OffPosition_Desc")]
        public string OffPosition21_Desc { get; set; }
        [BsonElement("Safety_Related_Text")]
        public string Safety_Related_Text { get; set; }
        [BsonElement("VendorID")]
        public string Vendor { get; set; }
        [BsonElement("Health")]
        public Int16? Health { get; set; }
        [BsonElement("BinaryData")]
        public string BinaryData { get; set; }
        [BsonElement("RawData")]
        public string RawData { get; set; }      
    }
}
