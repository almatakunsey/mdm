﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models
{
    [Table("ShipPosition")]
    public class ShipPosition
    {
        [Key]
        public int ShipPosID { get; set; }
        public int MMSI { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public Int16? TrueHeading { get; set; }
        public DateTime? LocalRecvTime { get; set; }
        public double? SOG { get; set; }
        public double? ROT { get; set; }
        public double? COG { get; set; }
        public DateTime RecvTime { get; set; }
        public Int16 MessageId { get; set; }
        public Int16 RAIM { get; set; }
        public Int16? NavigationalStatus { get; set; }
        public string NavigationalStatusToString
        {
            get
            {
                return TargetHelper.GetNavStatusDescriptionByNavStatusId(NavigationalStatus);
            }
        }
        public DateTime? UTC { get; set; }
        public string IP { get; set; }
        public Int16? PositionAccuracy { get; set; }        
    }


}
