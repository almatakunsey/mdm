﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    [Table("AtonPosData")]
    public class AtonPosData
    {
        [Key]
        public int MMSI { get; set; }
        public string Name { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public Int16? PositionAccuracy { get; set; }
        public Int16? OffPosition21 { get; set; }
        public Int16? VirtualAtoN { get; set; }
        public Int16? Health21 { get; set; }
        public Int16? Light21 { get; set; }
        public Int16? RACON21 { get; set; }
        public Int16? PositionFixingDevice { get; set; }
        public DateTime RecvTime { get; set; }
        public int Error21 { get; set; }
        public int Error68 { get; set; }
    }
}
