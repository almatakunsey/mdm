﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    [Table("AtonMetData")]
    public class AtonMetData
    {
        [Key]
        public int StnID { get; set; }
        public int MMSI { get; set; }
        public DateTime RecvTime { get; set; }
        public Int16? DAC { get; set; } // 533 utk MY
        public Int16? FI { get; set; } 
        public float? WaterLevel { get; set; }
        public float? WaterTemp { get; set; }
        public float? SurfCurSpeed { get; set; }
        public Int16? SurfCurDir { get; set; }
        public float? WaveHeight { get; set; }
        public Int16? WavePeriod { get; set; }
        public Int16? WaveDir { get; set; }
        public float? AirTemp { get; set; }
        public Int16? AirPres { get; set; }
        public Int16? RelHumidity { get; set; }
        public Int16? WndSpeed { get; set; }
        public Int16? WndGust { get; set; }
        public Int16? WndDir { get; set; }
        public Int16? WndGustDir { get; set; }
        public DateTime LocalRecvTime
        {
            get
            {
                DateTime date = DateTime.SpecifyKind(
                    RecvTime, DateTimeKind.Utc);
                return date.ToLocalTime();
            }
        }
    }
}
