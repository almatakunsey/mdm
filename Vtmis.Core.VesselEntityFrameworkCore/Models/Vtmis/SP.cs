﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    public class SP
    {
      
        public int MMSI { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public Int16 TrueHeading { get; set; }
        public DateTime? LocalRecvTime { get; set; }
        public double? SOG { get; set; }
        public double? ROT { get; set; }
        public double? COG { get; set; }
        public DateTime RecvTime { get; set; }
        //public bool IsInPolygon { get; set; }
    }
}
