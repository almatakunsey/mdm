﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models
{
    [Table("ShipStatic")]
    public class ShipStatic
    {
        //[Key]
        public int ShipStatID { get; set; }
        public string Name { get; set; }
        public int MMSI { get; set; }
        public string CallSign { get; set; }
        public string Vendor { get; set; }
        public string Destination { get; set; }
        public int? IMO { get; set; }
        public Int16? ShipType { get; set; }
        public Int16? AISVersion { get; set; }
        public Int16? AtoNType { get; set; }
        public Int16? VirtualAtoN { get; set; }
        public DateTime? ETA { get; set; }
        public double? MaxDraught { get; set; }
        public Int16? Dimension_A { get; set; }
        public Int16? Dimension_B { get; set; }
        public Int16? Dimension_C { get; set; }
        public Int16? Dimension_D { get; set; }
        public Int16? PositionFixingDevice { get; set; }
    }
}
