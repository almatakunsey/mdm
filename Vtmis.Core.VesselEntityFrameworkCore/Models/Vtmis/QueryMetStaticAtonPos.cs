﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    public class QueryMetStaticAtonPos
    {
        public int StnID { get; set; }
        public int MMSI { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public Int16? DAC { get; set; }
        public Int16? FI { get; set; }
        public string Name { get; set; }
        public string PosName { get; set; }
        public DateTime RecvTime { get; set; }
        public DateTime LocalRecvTime
        {
            get
            {
                DateTime date = DateTime.SpecifyKind(
                    RecvTime, DateTimeKind.Utc);
                return date.ToLocalTime();
            }
        }
        public Int16? AirPres { get; set; }
        public Single? AirTemp { get; set; }
        public Int16? RelHumidity { get; set; }
        public Int16? WndDir { get; set; }
        public Int16? WndGust { get; set; }
        public Int16? WndGustDir { get; set; }
        public Int16? WndSpeed { get; set; }
        public Int16? SurfCurDir { get; set; }
        public Single? SurfCurSpeed { get; set; }
        public Single? WaterLevel { get; set; }
        public Single? WaterTemp { get; set; }
        public Int16? WaveDir { get; set; }
        public Single? WaveHeight { get; set; }
        public Int16? WavePeriod { get; set; }
        public Single? Visibility { get; set; }
        public Int16? CurDir2 { get; set; }
        public Int16? CurDir3 { get; set; }
        public Single? CurSpeed2 { get; set; }
        public Single? CurSpeed3 { get; set; }
    }
}
