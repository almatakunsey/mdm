﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    public class QueryMethydroList
    {
        public int MMSI { get; set; }
        public string Name { get; set; }
        public string AisName { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public Int16? AtoNType { get; set; }
        public Int16? VirtualAtoN { get; set; }
        public Int16? OffPosition21 { get; set; }
        public Int16? OffPosition68 { get; set; }
        public DateTime? RecvTime6 { get; set; }
        public DateTime? RecvTime8 { get; set; }
        public DateTime? RecvTime21 { get; set; }

        public Single? AirTemp { get; set; }
        public Int16? AirPres { get; set; }
        public Int16? WndSpeed { get; set; }
        public Int16? WndGust { get; set; }
        public Int16? WndDir { get; set; }
    }
}
