﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis
{
    public class QueryAlarmTrigger
    {
        public int MMSI { get; set; }
        public DateTime RecvTime { get; set; }
        public DateTime? LocalRecvTime { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public string Name { get; set; }
    }
}
