﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{
    [Table("ShipList")]
    public class ShipList
    {
        //[Key]
        public int ShipListID { get; set; }
        public int? MMSI { get; set; }
        public string Name { get; set; }
        public int? IMO { get; set; }
        public string CallSign { get; set; }
        public double? SOG { get; set; }        
        public double? COG { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public DateTime RecvTime { get; set; }
        public DateTime? ETA { get; set; }
        public Int16? Dimension_A { get; set; }
        public Int16? Dimension_B { get; set; }
        public Int16? Dimension_C { get; set; }
        public Int16? Dimension_D { get; set; }
        public double? MaxDraught { get; set; }
        public Int16? NavigationalStatus { get; set; }
        public Int16? ShipType { get; set; }
        public DateTime LocalRecvTime
        {
            get
            {
                var utcTime = DateTime.SpecifyKind(RecvTime, DateTimeKind.Utc);
                return utcTime.ToLocalTime();
            }
        }
    }

}
