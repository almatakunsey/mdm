﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{

    [Table("ShipTypes")]
    public class ShipTypes
    {
        [Key]
        public int ShipTypeID { get; set; }
        public int ShipTypeIDLast { get; set; }
        public string ShipTypeName { get; set; }
        public int ShipTypeIdx { get; set; }
    }
}
