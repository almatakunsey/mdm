﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{
    [Table("Zones")]
    public class Zone
    {
        [Key]
        public int ZoneID { get; set; }
        public string ZoneName { get; set; }
    }
}
