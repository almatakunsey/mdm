﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{
    [Table("AxShips")]
    public class AxShip
    {
        [Key]
        public int ShipID { get; set; }
        public int MMSI { get; set; }
        public Single Middle { get; set; }
    }
}
