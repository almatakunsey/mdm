﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{
    public class Event
    {
        [Key]
        public int EventID { get; set; }
        public string CallSign { get; set; }
        public int MMSI { get; set; }
        public string ShipName { get; set; }
        public double Lng { get; set; }
        public double Lat { get; set; }
        public double SOG { get; set; }
        public int AlarmId { get; set; }
        public int ZoneId { get; set; }
        public DateTime TimeStmp { get; set; }
        public string Info { get; set; }
        public Alarm Alarm { get; set; }
        public Zone Zone { get; set; }
    }
}
