﻿using System.ComponentModel.DataAnnotations;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{
    public class Flag
    {
        [Key]
        public int FlagID { get; set; }
        public string FlagName { get; set; }
        public string FileName { get; set; }
    }
}
