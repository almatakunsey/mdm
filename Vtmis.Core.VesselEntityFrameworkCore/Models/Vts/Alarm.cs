﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{
    [Table("Alarms")]
    public class Alarm
    {
        [Key]
        public int AlarmID { get; set; }
        public string AlarmName { get; set; }
        public string AlarmMsg { get; set; }
        public bool Enable { get; set; }
        public int UserId { get; set; }
    }
}
