﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{
    [Table("Draught")]
    public class Draught
    {
        [Key]
        public int MMSI { get; set; }
        public DateTime? RecvTime { get; set; }
        public Single? Empty { get; set; } // Max Air Draught
        public Single? Full { get; set; } //  Min Air Draught
        public Single? Actual { get; set; } // Actual Air Draught
        public bool? Supply { get; set; } // Ship Load?
        public bool? Half { get; set; } // 1/2 Full?
        public bool? Cover { get; set; } // Casing Cover : true (Open) : false (Closed)
        public Single? Battery { get; set; } // Voltage
        public Int16? Sonar { get; set; } // Sonar : true (ok) : false (??)
        public Int16? Hoppers { get; set; }
        public Int16? HopperFlag { get; set; }
        public double? Lng { get; set; }
        public double? Lat { get; set; }
    }
}
