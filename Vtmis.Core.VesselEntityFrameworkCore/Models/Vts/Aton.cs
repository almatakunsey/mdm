﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{
    [Table("Atons")]
    public class Aton
    {
        [Key]
        public int AtonID { get; set; }
        public int AtonMMSI { get; set; }
        public string AtonName { get; set; }
    }
}
