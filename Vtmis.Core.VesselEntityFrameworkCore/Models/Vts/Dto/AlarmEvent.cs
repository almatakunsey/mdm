﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto
{
    public class AlarmEvent
    {
        public int EventID { get; set; }
        public int MMSI { get; set; }
        public string ShipName { get; set; }
        public string CallSign { get; set; }
        public string Coordinate { get; set; }
        public int AlarmID { get; set; }
        public string DoorStatus { get; set; }
        public DateTime Timestamp { get; set; }
        public string Photo { get; set; }
        public string Flag { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public float? SOG { get; set; }
        public float? COG { get; set; }
        public string Message { get; set; }
        public string Info { get; set; }
        public string Zone { get; set; }
        public int ZoneID { get; set; }
        public DateTime Aging { get; set; }
    }
}
