﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto
{
    public class ResponseExtendedShipList
    {
        public int Id { get; set; }
        public bool IsBlackList { get; set; }
        public string CallSign { get; set; }
        public int MMSI { get; set; }
        public string IMO { get; set; }
        public string OfficialNo { get; set; }
        public string VesselName { get; set; }
        public int VesselCategory { get; set; }
        public string VesselType { get; set; }
        public int Value { get; set; }
        public string ValueCurrency { get; set; }
        public string PortOfRegistry { get; set; }
        public string TradingArea { get; set; }
        public int PlyingLimit { get; set; }
        public int NoOfDeck { get; set; }
        public int NoOfHull { get; set; }
        public string HullMaterial { get; set; }
        public double Length { get; set; }
        public double Breadth { get; set; }
        public double Draught { get; set; }
        public double AnnualTonnageDue { get; set; }
        public double GrossTonnage { get; set; }
        public double NetTonnage { get; set; }
        public double DeadWeight { get; set; }
        public string EngineDescription { get; set; }
        public string PropulsionType { get; set; }
        public int NumberOfEngine { get; set; }
        public int YardNumber { get; set; }
        public DateTime? DateOfBuildingContract { get; set; }
        public DateTime? DateOfDelivery { get; set; }
        public DateTime? DateOfKeelLaid { get; set; }
        public string PlaceOfBuilt { get; set; }
        public string BuilderName { get; set; }
        public string BuilderAddress1 { get; set; }
        public string BuilderAddress2 { get; set; }
        public string BuilderAddress3 { get; set; }
        public string BuilderCountryCode { get; set; }
        public double TotalEnginePower { get; set; }
        public double Speed { get; set; }
        public int NumberOfCabin { get; set; }
        public int NoOfPassenger { get; set; }
        public int TotalNumberOfCrew { get; set; }
        public int Status { get; set; }
        public string BuilderPostCode { get; set; }
        public string TypeOfRegistry { get; set; }
        public string CountryFlag { get; set; }
        public string Class { get; set; }
        public bool IsCharter { get; set; }
        public string LocalityStatus { get; set; }
        public double Beam { get; set; }
        public double Propel { get; set; }
    }
}
