﻿namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto
{
    public class CreateShipDto
    {
        
        public string ShipName { get; set; }
        public int IMO { get; set; }
        public int MMSI { get; set; }
        public string CallSign { get; set; }
        public string Flag { get; set; }
        public string Port { get; set; }
        public int CompanyID { get; set; }
        public int UserID { get; set; }
        public bool ShowToAll { get; set; }
        public int LitGalFormatID { get; set; }
    }
}
