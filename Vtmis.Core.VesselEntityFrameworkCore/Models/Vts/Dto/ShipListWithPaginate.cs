﻿namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto
{
    public class ShipListWithPaginate
    {
        public ShipList ShipList { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
