﻿namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto
{
    public abstract class Paginate
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
