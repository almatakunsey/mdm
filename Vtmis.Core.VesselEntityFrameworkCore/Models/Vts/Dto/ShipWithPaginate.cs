﻿using System.Collections.Generic;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto
{
    public class ShipWithPaginate
    {
        public List<ShipDto> Ships { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
