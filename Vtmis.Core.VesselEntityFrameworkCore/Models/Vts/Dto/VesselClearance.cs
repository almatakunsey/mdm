﻿using System;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto
{
    public class VesselClearance
    {
        public int EventID { get; set; }
        public int MMSI { get; set; }
        public string CallSign { get; set; }
        public string Photo { get; set; }
        public string ShipName { get; set; }
        public string Flag { get; set; }
        public double? SOG { get; set; }
        public DateTime? ATA { get; set; }
        public DateTime? ETA { get; set; }
        public DateTime? ATD { get; set; }
        public int AlarmID { get; set; }
        public string Status { get; set; }
        public string Zone { get; set; }
        public DateTime TimeStamp { get; set; }
        public DateTime Aging { get; set; }
        public string Info { get; set; }
        public DateTime? UTC { get; set; }
        public DateTime? LocalRecvTime { get; set; }
    }
}
