﻿using System.ComponentModel.DataAnnotations;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{
    public class Photo
    {
        [Key]
        public int PhotoID { get; set; }
        public int MMSI { get; set; }
        public string FileName { get; set; }
    }
}
