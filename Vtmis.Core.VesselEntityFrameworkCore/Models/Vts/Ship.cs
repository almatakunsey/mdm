﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.Core.VesselEntityFrameworkCore.Models.Vts
{
    [Table("Ships")]
    public class Ship
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ShipID { get; set; }
        public string ShipName { get; set; }
        public int IMO { get; set; }
        public int MMSI { get; set; }
        public string CallSign { get; set; }
        public string Flag { get; set; }
        public string Port { get; set; }
        public int CompanyID { get; set; }
        public int UserID { get; set; }
        public bool ShowToAll { get; set; }
        public int LitGalFormatID { get; set; }
    }
}
