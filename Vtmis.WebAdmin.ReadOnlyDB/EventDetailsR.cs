﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("EventDetails")]
    public class EventDetailsR
    {
        [Key]
        public long Id { get; set; }
        public EventDetailsR()
        {
            EventHistories = new List<EventHistoryR>();
        }
        [ForeignKey("EventDetailsId")]
        public virtual List<EventHistoryR> EventHistories { get; set; }
        public long EventId { get; set; }
        public virtual EventR Event { get; set; }
    }
}
