﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("AbpRoles")]
    public class AbpRole
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int? TenantId { get; set; }
        public string DisplayName { get; set; }
        public bool IsDeleted { get; set; }
    }
}
