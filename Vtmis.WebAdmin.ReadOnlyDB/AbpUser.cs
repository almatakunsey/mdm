﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("AbpUsers")]
    public class AbpUser
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public int? TenantId { get; set; }
    }
}
