﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("FilterDetail")]
    public class FilterDetailsR
    {
        [Key]
        public int Id { get; set; }
        public Condition Condition { get; set; }
        public string ColumnName { get; set; }
        public string Operation { get; set; }
        public string ColumnValue { get; set; }
        public int RowIndex { get; set; }
    }
}
