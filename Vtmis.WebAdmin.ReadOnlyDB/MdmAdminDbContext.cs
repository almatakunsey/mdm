﻿using Microsoft.EntityFrameworkCore;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.Alarms;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    public class MdmAdminDbContext : DbContext
    {
        public MdmAdminDbContext()
        {
            ConnectionString = AppHelper.GetMdmDbConnectionString();
        }

        public MdmAdminDbContext(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public string ConnectionString { get; private set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ForNpgsqlUseSerialColumns();
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            //var connectionString = configuration["sqlserver_ip"]; //ConfigurationManager.AppSettings["sqlserver_ip"].ToString();

            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder
                    //.UseLoggerFactory(MyLoggerFactory)
                    .UseNpgsql(ConnectionString);
            }
        }

        public DbSet<AbpPermission> AbpPermissions { get; set; }
        public DbSet<AbpUserRole> AbpUserRoles { get; set; }
        public DbSet<AbpUser> AbpUsers { get; set; }
        public DbSet<AbpRole> AbpRoles { get; set; }
        public DbSet<ZoneR> Zones { get; set; }
        public DbSet<ZoneItemR> ZoneItems { get; set; }
        //public DbSet<AlarmR> Alarms { get; set; }
        //public DbSet<AlarmItemR> AlarmItems { get; set; }
        public DbSet<Alarm> Alarms { get; set; }
        public DbSet<AlarmItem> AlarmItems { get; set; }
        public DbSet<Vtmis.WebAdmin.Events.Event> Events { get; set; }
        public DbSet<Vtmis.WebAdmin.Events.EventDetails> EventDetails { get; set; }
        public DbSet<Vtmis.WebAdmin.Events.EventHistory> EventHistories { get; set; }
        public DbSet<AlarmNotification> AlarmNotifications { get; set; }
        public DbSet<FilterR> Filters { get; set; }
        public DbSet<FilterItemR> FilterItems { get; set; }
        public DbSet<FilterDetailsR> FilterDetails { get; set; }
        public DbSet<FilterGroupR> FilterGroups { get; set; }
    }
}
