﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("FilterGroups")]
    public class FilterGroupR
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }
        public string MMSIList { get; set; }
        public string NameList { get; set; }
        public string CallSignList { get; set; }
        public DateTime CreationTime { set; get; }
    }
}
