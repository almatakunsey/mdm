﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("Filters")]
    public class FilterR
    {
        public FilterR()
        {
            FilterItems = new List<FilterItemR>();           
        }

        [Key]
        public int Id { get; set; }

        [ForeignKey("FilterId")]
        public virtual ICollection<FilterItemR> FilterItems { get; set; }
        public string Name { get; set; }
        public bool IsEnable { get; set; }
        public bool IsShowShips { get; set; }
        public int? TenantId { set; get; }
        public long UserId { get; set; }
    }
}
