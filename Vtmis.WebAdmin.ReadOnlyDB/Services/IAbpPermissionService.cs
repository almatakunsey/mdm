﻿using System.Threading.Tasks;

namespace Vtmis.WebAdmin.ReadOnlyDB.Services
{
    public interface IAbpPermissionService
    {
        Task<bool> IsGrantedByTenantIdAsync(string permissionName, int? tenantId);
        Task<bool> IsGrantedByRoleIdAsync(string permissionName, int? roleId);
    }
}