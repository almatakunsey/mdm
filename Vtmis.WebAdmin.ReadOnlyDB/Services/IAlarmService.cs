﻿using Dapper;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Events;
namespace Vtmis.WebAdmin.ReadOnlyDB.Services
{
    public interface IAlarmService
    {
        Task<(string query, DynamicParameters parameters)> GenerateAlarmQueryAsync(Event alarmEvent);
    }
}