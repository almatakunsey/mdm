﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories;

namespace Vtmis.WebAdmin.ReadOnlyDB.Services
{
    public class AbpPermissionService : IAbpPermissionService
    {
        private readonly IAbpPermissionRepository _abpPermissionRepository;

        public AbpPermissionService(IAbpPermissionRepository abpPermissionRepository)
        {
            _abpPermissionRepository = abpPermissionRepository;
        }

        public async Task<bool> IsGrantedByTenantIdAsync(string permissionName, int? tenantId)
        {
            return await _abpPermissionRepository.IsGrantedByTenantIdAsync(permissionName, tenantId);
        }

        public async Task<bool> IsGrantedByRoleIdAsync(string permissionName, int? roleId)
        {
            return await _abpPermissionRepository.IsGrantedByRoleIdAsync(permissionName, roleId);
        }

       
    }
}
