﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("AbpUserRoles")]
    public class AbpUserRole
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public int RoleId { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }

        public virtual AbpRole Role { get; set; }
        public virtual AbpUser User { get; set; }
    }
}
