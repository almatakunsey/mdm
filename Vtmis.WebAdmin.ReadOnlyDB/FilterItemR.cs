﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("FilterItem")]
    public class FilterItemR
    {
        public FilterItemR()
        {
            FilterDetails = new List<FilterDetailsR>();
        }
        [Key]
        public int Id { get; set; }
        public string Colour { get; set; }
        public string BorderColour { get; set; }
        [ForeignKey("FilterItemId")]
        public virtual ICollection<FilterDetailsR> FilterDetails { get; set; }
    }
}
