﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("Zones")]
    public class ZoneR
    {
        public ZoneR()
        {
            CreationTime = DateTime.Now;
            ZoneItems = new List<ZoneItemR>();
        }
        //[ForeignKey("UserId")]
        //public virtual User User { get; protected set; }
        //public virtual long UserId { get; protected set; }
        [Key]
        public int Id { get; set; }
        public int? TenantId { get; set; }
        public DateTime CreationTime { get; set; }

        public string Name { get; set; }
        public bool IsEnable { get; set; }

        public decimal Radius { get; set; }
        public string Colour { get; set; }
        public bool IsFill { get; set; }

        public ZoneType ZoneType { get; set; }

        //[ForeignKey("ZoneId")]
        public virtual ICollection<ZoneItemR> ZoneItems { get; set; }

    }
    public enum ZoneType
    {
        Circle, Polygon, Line_L, Line_R
    }

}
