﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("AlarmItems")]
    public class AlarmItemR
    {
        public AlarmItemR()
        {
            CreationTime = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }

        [ForeignKey("AlarmId")]
        public virtual AlarmR Alarm { get; set; }
        public virtual int AlarmId { get; set; }

        public Condition Condition { get; set; }
        public string ColumnType { get; set; }
        public string ColumnItem { get; set; }
        public string Operation { get; set; }
        public string ColumnValue { get; set; }
        public int RowIndex { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
