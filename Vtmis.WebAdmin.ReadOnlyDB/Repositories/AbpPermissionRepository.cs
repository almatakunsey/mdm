﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.WebAdmin.ReadOnlyDB.Repositories
{
    public class AbpPermissionRepository : IAbpPermissionRepository
    {
        private readonly MdmAdminDbContext _dbContext;

        public AbpPermissionRepository(MdmAdminDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _dbContext.SaveChangesAsync()) > 0;
        }

        public IQueryable<AbpPermission> Query()
        {
            return _dbContext.AbpPermissions.AsQueryable();
        }
        public IQueryable<AbpPermission> GetAll()
        {
            return _dbContext.AbpPermissions.IgnoreQueryFilters().AsNoTracking().AsQueryable();
        }

        public async Task<AbpPermission> GetAsync(long id)
        {
            return await _dbContext.AbpPermissions.IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<AbpPermission>> GetByTenantIdAsync(int? tenantId)
        {
            return await _dbContext.AbpPermissions.AsNoTracking().IgnoreQueryFilters().Where(x => x.TenantId == tenantId).ToListAsync();
        }

        public async Task<IEnumerable<AbpPermission>> GetByRoleIdAsync(int? roleId)
        {
            return await _dbContext.AbpPermissions.AsNoTracking().IgnoreQueryFilters().Where(x => x.RoleId == roleId).ToListAsync();
        }

        public async Task<bool> IsGrantedByTenantIdAsync(string permissionName, int? tenantId)
        {
            return await _dbContext.AbpPermissions.AsNoTracking().IgnoreQueryFilters()
                    .AnyAsync(x => x.Name.IsEqual(permissionName) && x.IsGranted == true && x.TenantId == tenantId);
        }

        public async Task<bool> IsGrantedByRoleIdAsync(string permissionName, int? roleId)
        {
            return await _dbContext.AbpPermissions.AsNoTracking().IgnoreQueryFilters()
                    .AnyAsync(x => x.Name.IsEqual(permissionName) && x.IsGranted == true && x.RoleId == roleId);
        }

        public async Task<bool> UpdatePermissionByRoleId(List<string> permissionsName, int? roleId, int? tenantId)
        {
            foreach (var permission in permissionsName)
            {
                var data = await _dbContext.AbpPermissions.FirstOrDefaultAsync(x => x.Name.IsEqual(permission) && x.RoleId == roleId);
                if (data != null)
                {
                    data.IsGranted = true;
                    _dbContext.AbpPermissions.Update(data);
                }
                else
                {
                    await _dbContext.AbpPermissions.AddAsync(new AbpPermission
                    {
                        CreationTime = DateTime.Now,
                        IsGranted = true,
                        Discriminator = "RolePermissionSetting",
                        Name = permission,
                        RoleId = roleId,
                        TenantId = tenantId
                    });
                }               
            }
            
            var nonGrantedPermission = await _dbContext.AbpPermissions.Where(x => permissionsName.Any(a => a.IsEqual(x.Name)) == false && x.RoleId == roleId)
                                            .ToListAsync();
            foreach (var item in nonGrantedPermission)
            {
                item.IsGranted = false;
                _dbContext.AbpPermissions.Update(item);
            }
            return (await _dbContext.SaveChangesAsync()) > 0;
        }
    }
}
