﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Vtmis.WebAdmin.ReadOnlyDB.Repositories
{
    public class MdmUserRoleRepository : IMdmUserRoleRepository
    {
        private readonly MdmAdminDbContext _dbContext;

        public MdmUserRoleRepository(MdmAdminDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public IQueryable<AbpUserRole> GetAll()
        {
            return _dbContext.AbpUserRoles.AsNoTracking().AsQueryable();
        }
        public async Task DeleteByRoleAndUserId(int roleId, long userId)
        {
            var result = await _dbContext.AbpUserRoles.FirstOrDefaultAsync(x => x.RoleId == roleId && x.UserId == userId);
            _dbContext.AbpUserRoles.Remove(result);
            await _dbContext.SaveChangesAsync();
        }

        public async Task RemoveAllRoleFromUser(long userId)
        {
            var result = _dbContext.AbpUserRoles.Where(x => x.UserId == userId);
            _dbContext.AbpUserRoles.RemoveRange(result);
            await _dbContext.SaveChangesAsync();
        }

        public async Task SetRoles(long userId, int? tenantId, int[] roleIds)
        {
            var data = new List<AbpUserRole>();
            var currentDate = DateTime.Now;
            foreach (var item in roleIds)
            {
                data.Add(new AbpUserRole { CreationTime = currentDate, RoleId = item, TenantId = tenantId, UserId = userId });
            }
            await _dbContext.AbpUserRoles.AddRangeAsync(data);
            await _dbContext.SaveChangesAsync();
        }
    }
}
