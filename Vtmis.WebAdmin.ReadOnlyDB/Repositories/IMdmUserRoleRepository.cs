﻿using System.Linq;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.ReadOnlyDB.Repositories
{
    public interface IMdmUserRoleRepository
    {
        Task SetRoles(long userId, int? tenantId, int[] roleIds);
        IQueryable<AbpUserRole> GetAll();
        Task DeleteByRoleAndUserId(int roleId, long userId);
        Task RemoveAllRoleFromUser(long userId);
    }
}