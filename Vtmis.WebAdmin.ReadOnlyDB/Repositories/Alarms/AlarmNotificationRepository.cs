﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Alarms;

namespace Vtmis.WebAdmin.ReadOnlyDB.Repositories.Alarms
{
    public class AlarmNotificationRepository : IAlarmNotificationRepository
    {
        private readonly MdmAdminDbContext _dbContext;

        public AlarmNotificationRepository(MdmAdminDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Return all alarm notifications with pagination
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<PaginatedList<AlarmNotification>> GetAsync(int pageIndex, int pageSize)
        {
            var qry = _dbContext.AlarmNotifications.AsNoTracking();
            return await new PaginatedList<AlarmNotification>().CreateAsync(qry, pageIndex, pageSize);
        }

        /// <summary>
        /// Get alarm notifications by user id with pagination
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<PaginatedList<AlarmNotification>> GetAsync(long userId, int pageIndex, int pageSize)
        {
            var qry = _dbContext.AlarmNotifications.AsNoTracking().Where(x => x.UserId == userId);
            return await new PaginatedList<AlarmNotification>().CreateAsync(qry, pageIndex, pageSize);
        }

        /// <summary>
        /// Get alarm notifications by tenant id with pagination
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<PaginatedList<AlarmNotification>> GetAsync(int? tenantId, int pageIndex, int pageSize)
        {
            var qry = _dbContext.AlarmNotifications.AsNoTracking().Where(x => x.TenantId == tenantId);
            return await new PaginatedList<AlarmNotification>().CreateAsync(qry, pageIndex, pageSize);
        }

        /// <summary>
        /// Create AlarmNotification
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<AlarmNotification> CreateAsync(AlarmNotification input)
        {
            await _dbContext.AlarmNotifications.AddAsync(input);
            return input;
        }

        /// <summary>
        /// Save Changes Async
        /// </summary>
        /// <returns></returns>
        public async Task<bool> SaveChangesAsync()
        {
            return (await _dbContext.SaveChangesAsync()) > 0;
        }
    }
}
