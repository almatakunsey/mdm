﻿using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Alarms;

namespace Vtmis.WebAdmin.ReadOnlyDB.Repositories.Alarms
{
    public interface IAlarmNotificationRepository
    {
        /// <summary>
        /// Return all alarm notifications with pagination
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PaginatedList<AlarmNotification>> GetAsync(int pageIndex, int pageSize);
        /// <summary>
        /// Get alarm notifications by user id with pagination
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PaginatedList<AlarmNotification>> GetAsync(long userId, int pageIndex, int pageSize);
        /// <summary>
        /// Get alarm notifications by tenant id with pagination
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PaginatedList<AlarmNotification>> GetAsync(int? tenantId, int pageIndex, int pageSize);
        /// <summary>
        /// Create AlarmNotification
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<AlarmNotification> CreateAsync(AlarmNotification input);
        /// <summary>
        /// Save Changes Async
        /// </summary>
        /// <returns></returns>
        Task<bool> SaveChangesAsync();
    }
}