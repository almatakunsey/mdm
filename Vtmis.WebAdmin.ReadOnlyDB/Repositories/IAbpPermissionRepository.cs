﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.ReadOnlyDB.Repositories
{
    public interface IAbpPermissionRepository
    {
        Task<bool> SaveChangesAsync();
        IQueryable<AbpPermission> Query();
        IQueryable<AbpPermission> GetAll();
        Task<AbpPermission> GetAsync(long id);
        Task<IEnumerable<AbpPermission>> GetByTenantIdAsync(int? tenantId);
        Task<IEnumerable<AbpPermission>> GetByRoleIdAsync(int? roleId);
        Task<bool> IsGrantedByTenantIdAsync(string permissionName, int? tenantId);
        Task<bool> IsGrantedByRoleIdAsync(string permissionName, int? roleId);
        Task<bool> UpdatePermissionByRoleId(List<string> permissionsName, int? roleId, int? tenantId);
    }
}
