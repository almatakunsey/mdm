﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.ReadOnlyDB.Repositories.Events
{
    public class EventRepository : IEventRepository
    {
        private readonly MdmAdminDbContext _dbContext;

        public EventRepository(MdmAdminDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        /// <summary>
        /// Get All Events by Alarm Id including all collections
        /// </summary>
        /// <param name="alarmId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Vtmis.WebAdmin.Events.Event>> GetEventsByAlarmId(long alarmId)
        {
            return await _dbContext.Events.AsNoTracking().Include(x => x.EventDetails).Include("EventDetails.EventHistories")
                            .Where(x => x.AlarmId == alarmId).ToListAsync();
        }

        /// <summary>
        ///  Get All Event Details by Event Id including all collections
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Vtmis.WebAdmin.Events.EventDetails>> GetEventDetailsByEventId(long eventId)
        {
            return await _dbContext.EventDetails.AsNoTracking().Include(x => x.EventHistories)
                           .Where(x => x.EventId == eventId).ToListAsync();
        }
    }
}
