﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.ReadOnlyDB.Repositories.Events
{
    public interface IEventRepository
    {
        /// <summary>
        /// Get All Events by Alarm Id including all collections
        /// </summary>
        /// <param name="alarmId"></param>
        /// <returns></returns>
        Task<IEnumerable<Vtmis.WebAdmin.Events.Event>> GetEventsByAlarmId(long alarmId);
        /// <summary>
        ///  Get All Event Details by Event Id including all collections
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        Task<IEnumerable<Vtmis.WebAdmin.Events.EventDetails>> GetEventDetailsByEventId(long eventId);
    }
}