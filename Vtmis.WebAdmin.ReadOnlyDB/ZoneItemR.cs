﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("ZoneItems")]
    public class ZoneItemR
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreationTime { get; set; }

        public double Latitude { get; set; }
        public double Logitude { get; set; }
        public int Order { get; set; }

        [ForeignKey("ZoneId")]
        public virtual ZoneR Zone { get; set; }
        public virtual int ZoneId { get; set; }
    }
}
