﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("Events")]
    public class EventR
    {
        public EventR()
        {
            EventDetails = new List<EventDetailsR>();
        }
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletionTime { get; set; }
        public bool Kill { get; set; }
        [ForeignKey("EventId")]
        public virtual List<EventDetailsR> EventDetails { get; set; }

        //Alarm
        public int AlarmId { get; set; }
        public virtual AlarmR Alarm { get; set; }
    }
}
