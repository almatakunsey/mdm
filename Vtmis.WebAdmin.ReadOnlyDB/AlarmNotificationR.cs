﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("AlarmNotifications")]
    public class AlarmNotificationR
    {
        [Key]
        public long Id { get; set; }
        public long AlarmId { get; set; }
        public long EventId { get; }
        public long EventDetailsId { get; }
        public bool IsRead { get; set; }
        public long UserId { get; set; }
        public int? TenantId { get; set; }

        public AlarmR Alarm { get; set; }
        public EventR Event { get; set; }
        public EventDetailsR EventDetails { get; set; }
    }
}
