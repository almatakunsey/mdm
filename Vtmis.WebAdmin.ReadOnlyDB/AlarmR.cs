﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("Alarms")]
    public class AlarmR
    {
        [Key]
        public int Id { get; set; }
        public bool Changes { get; set; }      
        public int? TenantId { get; set; }
        public long UserId { get; set; }

        public string Name { get; set; }

        public bool IsEnabled { get; set; }

        /// <summary>
        /// If priority Medium or High, user must be notified
        /// </summary>
        public AlarmPriority Priority { get; set; }

        /// <summary>
        /// Message to be displayed to notify user
        /// </summary>
        public string Message { get; set; }

        public string Color { get; set; }

        /// <summary>
        /// Specify the minimum time interval between notifications - In seconds
        /// </summary>
        public int RepeatInterval { get; set; }

        [ForeignKey("AlarmId")]
        public virtual IList<AlarmItemR> AlarmItems { get; set; }

        //public virtual IList<AlarmCondition> Conditions { get; set; }

        /// <summary>
        /// Send AIS Safety Message if true
        /// </summary>
        public bool IsSendAISSafetyMessage { get; set; }

        public string AISMessage { get; set; }

        /// <summary>
        /// Send email alert if true
        /// </summary>
        public bool IsSendEmail { get; set; }

        //[EmailAddress(ErrorMessage = "Invalid email address format")]
        public string EmailAddress { get; set; }
    }

    public enum AlarmPriority
    {
        Low,
        Medium,
        High
    }
}
