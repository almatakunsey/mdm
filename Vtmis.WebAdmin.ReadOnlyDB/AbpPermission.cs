﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("AbpPermissions")]
    public class AbpPermission
    {
        [Key]
        public long Id { get; set; }
        public DateTime CreationTime { get; set; }
        public long? CreatorUserId { get; set; }
        public string Discriminator { get; set; }
        public bool IsGranted { get; set; }
        public string Name { get; set; }
        public int? TenantId { get; set; }
        public int? RoleId { get; set; }
        public long? UserId { get; set; }
    }
}
