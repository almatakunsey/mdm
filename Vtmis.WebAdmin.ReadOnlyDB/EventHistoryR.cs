﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.ReadOnlyDB
{
    [Table("EventHistories")]
    public class EventHistoryR
    {
        [Key]
        public long Id { get; set; }
        public int? MMSI { get; set; }
        public DateTime? TargetRecvTime { get; set; } // RecvTime from AIS ShipPosition 
    }
}
