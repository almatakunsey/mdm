﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.Model.DTO.Target;

namespace Vtmis.WebVessel.Tracking.Repository.Interfaces
{
    public interface ICacheService
    {
        Task RemoveAllVesselHistoryFromRedis();
        Task SeedVesselHistoriesToRedis(IEnumerable<VesselPosition> targets);
        Task PublishTargetsToServiceBus(string channelName, string message);
        EICSModel GetEICSEQuipment(int? mmsi);
        string GetLloydsShipType(int? imo, string callSign, string name);
        Task RemoveAllVesselTrackFromRedis();
        Task SeedVesselTrackToRedis(List<VesselRoute> vesselTrack);
        Task UpdateVesselHistoryTrack(VesselPosition item);
        Task<List<VesselRoute>> GetAllVesselTrack();
    }
}
