﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Vtmis.Core.Model.AkkaModel.Messages.Route;
using Vtmis.Core.Model.DTO.Methydro;
using Vtmis.Core.Model.DTO.DDMS;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.Core.Model.ViewModels.Target;
using Vtmis.Core.Model.ViewModels.Target.Vessel;
using Vtmis.Core.Model.DTO.Target;

namespace Vtmis.WebVessel.Tracking.Repository.Interfaces
{
    public interface IDapperAisRepository
    {
        IDbConnection Dapper { get; set; }

        /// <summary>
        /// Query previous coordinate
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="recvtime"></param>
        /// <returns></returns>
        Task<QueryTargetCoordinate> QueryPreviousCoordinatesAsync(int mmsi, DateTime recvtime);
        /// <summary>
        /// Query Advanced Search
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="advancedSearch"></param>
        /// <returns></returns>
        Task<IEnumerable<VesselPositionViewModel>> QueryAdvancedSearchAsync(DateTime startDate, DateTime endDate, AdvancedSearchViewModel advancedSearch);
        /// <summary>
        /// Query Weather Historical
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        Task<IEnumerable<ResponseWeatherHistorical>> QueryWeatherHistorical(int mmsi, DateTime fromDate, DateTime toDate);
        /// <summary>
        /// Query vessel eta
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="requestDate"></param>
        /// <param name="lastTransmit"></param>
        /// <param name="minSog"></param>
        /// <param name="maxSog"></param>
        /// <returns></returns>
        Task<ResponseVesselEta> QueryVesselEta(int mmsi, DateTime requestDate, int lastTransmit, double minSog, double maxSog);
        /// <summary>
        /// Query Route ETA
        /// </summary>
        /// <param name="requestDate">In local time</param>
        /// <param name="lastTransmit">In minutes</param>
        /// <param name="minSog"></param>
        /// <param name="maxSog"></param>
        /// <returns></returns>
        Task<IEnumerable<QueryShipPosStatic>> QueryRouteEta(DateTime requestDate, int lastTransmit, double minSog, double maxSog);
        /// <summary>
        /// Query Route ETA with pagination
        /// </summary>
        /// <param name="requestDate">In local time</param>
        /// <param name="lastTransmit">In minutes</param>
        /// <param name="minSog"></param>
        /// <param name="maxSog"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<IEnumerable<QueryShipPosStatic>> QueryRouteEta(DateTime requestDate, int lastTransmit, double minSog, double maxSog, int pageIndex, int pageSize);
        /// <summary>
        /// Query DDMS Historical
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <returns></returns>
        Task<IEnumerable<ResponseDDMSHistorical>> QueryDDMSHistorical(int mmsi, DateTime startdate, DateTime enddate);

        Task<DateTime?> TargetLastRouteDockingTimeAsync(double placeLatitude, double placeLongitude, int mmsi, double placeRadius, DateTime requestDate);
    }
}
