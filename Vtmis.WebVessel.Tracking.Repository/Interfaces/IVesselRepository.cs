﻿using System.Collections.Generic;
using System.Linq;
using Vtmis.Core.VesselEntityFrameworkCore.Models;

namespace Vtmis.WebVessel.Tracking.Repository.Interfaces
{
    public interface IVesselRepository
    {
        IEnumerable<ShipPosition> GetAllShipPosition();
        ShipPosition GetShipPositionById(int id);
        IQueryable<ShipPosition> QueryShipPosition();

        IEnumerable<ShipStatic> GetAllShipStatic();
        ShipStatic GetShipStaticById(int id);
        IQueryable<ShipStatic> QueryShipStatic();
    }
}
