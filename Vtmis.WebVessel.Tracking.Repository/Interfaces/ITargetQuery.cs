﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers.RMS;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages.Route;
using Vtmis.Core.Model.AkkaModel.Messages.Target;
using Vtmis.Core.Model.AkkaModel.Messages.Target.DDMS;
using Vtmis.Core.Model.DTO.DDMS;
using Vtmis.Core.Model.DTO.Methydro;
using Vtmis.Core.Model.DTO.Target;
using Vtmis.Core.Model.ViewModels.Target;
using Vtmis.Core.Model.ViewModels.Target.Vessel;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;

namespace Vtmis.WebVessel.Tracking.Repository.Interfaces
{
    public interface ITargetQuery
    {
        Task UpdateVesselHistoryAndTrack();
        Task SeedVesselHistory();
        Task SeedVesselTrack();
        [AutomaticRetry(Attempts = 0, OnAttemptsExceeded = AttemptsExceededAction.Delete)]
        Task QueryUpdateTargets(DateTime startDate, string userId);
        /// <summary>
        /// Query Route ETA
        /// </summary>
        /// <param name="requestDate"></param>
        /// <param name="lastTransmit"></param>
        /// <param name="minSog"></param>
        /// <param name="maxSog"></param>
        /// <returns></returns>
        Task<IEnumerable<QueryShipPosStatic>> QueryRouteEta(DateTime requestDate, int lastTransmit, double minSog, double maxSog);
        /// <summary>
        /// Query Vessel ETA
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="requestDate"></param>
        /// <param name="lastTransmit"></param>
        /// <param name="minSog"></param>
        /// <param name="maxSog"></param>
        /// <returns></returns>
        Task<ResponseVesselEta> QueryVesselEta(int mmsi, DateTime requestDate, int lastTransmit, double minSog, double maxSog);
        /// <summary>
        /// Query for Restore vessel track
        /// </summary>
        /// <returns></returns>
        Task<List<VesselRoute>> QueryVesselTrack();
        /// <summary>
        /// Query for Restore vessel position
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Vtmis.Core.Model.AkkaModel.Messages.VesselPosition>> QueryVesselHistory();
        Task<IEnumerable<VesselPositionViewModel>> QueryAdvanceSearchMongoDBAsync(DateTime startDate, DateTime endDate, AdvancedSearchViewModel advancedSearch, bool isHost, IEnumerable<int> targets);
        /// <summary>
        /// Query Target latest position to be pushed to map
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        Task<IEnumerable<QueryUpdateTarget>> QueryUpdateTargetsAsync(DateTime startDate);
        /// <summary>
        /// Query TargetInfo Async
        /// </summary>
        /// <param name="mmsi"></param>
        /// <returns></returns>
        Task<object> QueryRMSInfoAsync(int mmsi);
        /// <summary>
        /// Query Aton List
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PaginatedList<ResponseAtonList>> QueryAtonListAsync(RequestAtonList input);
        /// <summary>
        /// Query monitoring data
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PaginatedList<ResponseMonList>> QueryMonListAsync(RequestMonList input);
        /// <summary>
        /// Query Lighthouse
        /// </summary>
        /// <param name="mmsi"></param>
        /// <returns></returns>
        Task<RMSLighthouse> QueryLighthouse(int mmsi);
        /// <summary>
        /// Query DDMS Info
        /// </summary>
        /// <param name="mmsi"></param>
        /// <returns></returns>
        Task<ResponseDDMSInfo> QueryDDMSInfo(int mmsi);
        /// <summary>
        /// Query DDMS Historical
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        Task<List<ResponseDDMSHistorical>> QueryDDMSHistoricalAsync(int mmsi, DateTime startDate, DateTime endDate);
        /// <summary>
        /// Query RMS Historical
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        Task<List<object>> QueryRMSHistoricalAsync(int mmsi, DateTime startDate, DateTime endDate);
        /// <summary>
        /// Query Methydro station name
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PaginatedList<MethydroStation>> QueryMethydroStation(int pageIndex, int pageSize);
        /// <summary>
        /// Query methydro instruments via mmsi and date range
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        Task<MethydroDetailInstruments> QueryInstrumentDataByMmsiDateRangeAsync(int mmsi, DateTime startDate, DateTime endDate);
        /// <summary>
        /// Query methydro historical
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        Task<List<ResponseWeatherHistorical>> QueryMethydroHistorical(int mmsi, DateTime startDate, DateTime endDate);
        /// <summary>
        /// Query methydro list
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        Task<PaginatedList<ResponseMethydroList>> QueryMetList(RequestMethydroList m);
        /// <summary>
        /// Query Playback
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        Task<IEnumerable<ShipPosition>> QueryPlayback(int mmsi, DateTime startDate, DateTime endDate);
    }
}
