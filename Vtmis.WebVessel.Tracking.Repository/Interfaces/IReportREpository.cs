﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Model.DTO;
using Vtmis.Core.Model.DTO.Report;

namespace Vtmis.WebVessel.Tracking.Repository.Interfaces
{
    public interface IReportREpository
    {
        IDbConnection Dapper { get; set; }
        Task<IEnumerable<ResponseReportEnterExit>> QueryReportEnterExit(RequestReportEnterExit input);

        Task<IEnumerable<ResponseArrivalDeparture>> QueryReportArrivalDeparture(RequestReportEnterExit input);
    }
}
