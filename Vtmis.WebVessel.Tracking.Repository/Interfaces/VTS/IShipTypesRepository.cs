﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;

namespace Vtmis.WebVessel.Tracking.Repository.Interfaces
{
    public interface IShipTypesRepository
    {
       Task<IEnumerable<ShipTypes>> GetAllAsync();
    }
}
