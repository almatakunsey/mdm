﻿using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;

namespace Vtmis.WebVessel.Tracking.Repository.Interfaces.VTS
{
    public interface IAxShipRepository
    {
        /// <summary>
        /// Get By MMSI
        /// </summary>
        /// <param name="mmsi"></param>
        /// <returns></returns>
        Task<AxShip> GetAsync(int mmsi);
        IQueryable<AxShip> Query();
    }
}
