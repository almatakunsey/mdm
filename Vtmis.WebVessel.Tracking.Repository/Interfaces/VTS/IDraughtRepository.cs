﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;

namespace Vtmis.WebVessel.Tracking.Repository.Interfaces.VTS
{
    public interface IDraughtRepository
    {
        IQueryable<Draught> Query();
        Task<IEnumerable<Draught>> GetAllAsync();
        Task<PaginatedList<Draught>> GetAllAsync(int pageIndex, int pageSize);
        Task<Draught> GetAsync(int mmsi);        
    }
}
