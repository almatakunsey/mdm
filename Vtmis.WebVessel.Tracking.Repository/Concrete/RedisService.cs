﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.Model.DTO.Target;
using Vtmis.WebAdmin.EntityFrameworkCore.Seed;
using Vtmis.WebAdmin.Lloyds;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;

namespace Vtmis.WebVessel.Tracking.Repository.Concrete
{
    public class RedisService : ICacheService
    {
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        private readonly IDatabase _redisDb;
        private readonly ISubscriber _pub;

        public RedisService(IConnectionMultiplexer connectionMultiplexer)
        {
            _connectionMultiplexer = connectionMultiplexer;
            _redisDb = _connectionMultiplexer.GetDatabase();
            _pub = _connectionMultiplexer.GetSubscriber();
        }
        public async Task UpdateVesselHistoryTrack(VesselPosition item)
        {
            var position = JsonConvert.SerializeObject(item);
            await _redisDb.HashSetAsync($"vesselHistory_{AppHelper.GetEnvironmentName()}", item.VesselId, position, When.Always, CommandFlags.FireAndForget);

            var vtStorage = await _redisDb.HashGetAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}", item.VesselId);
            if (vtStorage.HasValue)
            {               
                var vt = JsonConvert.DeserializeObject<LinkedList<VesselRoutePosition>>(vtStorage);
                vt.AddLast(new VesselRoutePosition(item.Lat, item.Lgt));
                if (vt.Count > 10)
                {
                    vt.RemoveFirst();
                }

                var coordinate = JsonConvert.SerializeObject(vt);
                await _redisDb.HashSetAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}", item.VesselId, coordinate, When.Always, CommandFlags.FireAndForget);
            }
            else
            {
                var vt = new LinkedList<VesselRoutePosition>();
                vt.AddLast(
                    new VesselRoutePosition(item.Lat, item.Lgt)
                );
                var coordinate = JsonConvert.SerializeObject(vt);
                await _redisDb.HashSetAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}", item.VesselId, coordinate, When.Always, CommandFlags.FireAndForget);
            }

        }
        public async Task<List<VesselRoute>> GetAllVesselTrack()
        {
            var listVt = new List<VesselRoute>();
            var getAllHashKeys = await _redisDb.HashGetAllAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}");
            if (getAllHashKeys.Any())
            {
                foreach (var item in getAllHashKeys)
                {
                    var vr = new VesselRoute { VesselId = item.Name };
                    vr.VesselRoutePosition = JsonConvert.DeserializeObject<LinkedList<VesselRoutePosition>>(item.Value);
                    listVt.Add(vr);
                }
            }
            return listVt;
        }
        public async Task RemoveAllVesselTrackFromRedis()
        {
            Console.WriteLine("vesselTrack_...");
            var getAllHashKeys = await _redisDb.HashGetAllAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}");

            if (getAllHashKeys.Any())
            {
                Console.WriteLine("Removing all vesseltrack");
                foreach (var item in getAllHashKeys)
                {
                    await _redisDb.HashDeleteAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}", item.Name, CommandFlags.FireAndForget);
                }
                Console.WriteLine("vesseltrack deleted");
            }
        }

        public async Task SeedVesselTrackToRedis(List<VesselRoute> targets)
        {
            if (targets.Any())
            {
                Console.WriteLine("seeding vessel track");
                foreach (var item in targets)
                {
                    var coordinate = JsonConvert.SerializeObject(item.VesselRoutePosition);
                    await _redisDb.HashSetAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}", item.VesselId, coordinate, When.Always, CommandFlags.FireAndForget);
                }
                Console.WriteLine("seeding vessel track completed");
            }
        }
        public async Task RemoveAllVesselHistoryFromRedis()
        {
            Console.WriteLine("vesselHistory_...");
            var getAllHashKeysHistory = await _redisDb.HashGetAllAsync($"vesselHistory_{AppHelper.GetEnvironmentName()}");
            if (getAllHashKeysHistory.Any())
            {
                Console.WriteLine("Removing all history");
                foreach (var item in getAllHashKeysHistory)
                {
                    await _redisDb.HashDeleteAsync($"vesselHistory_{AppHelper.GetEnvironmentName()}", item.Name, CommandFlags.FireAndForget);
                }
                Console.WriteLine("history deleted");
            }
        }

        public async Task SeedVesselHistoriesToRedis(IEnumerable<VesselPosition> targets)
        {
            if (targets.Any())
            {
                Console.WriteLine("seeding history");
                foreach (var item in targets)
                {
                    var vp = JsonConvert.SerializeObject(item);
                    await _redisDb.HashSetAsync($"vesselHistory_{AppHelper.GetEnvironmentName()}", item.VesselId, vp, When.Always, CommandFlags.FireAndForget);
                }
                Console.WriteLine("seeding history completed");
            }
        }

        public async Task PublishTargetsToServiceBus(string channelName, string message)
        {
            await _pub.PublishAsync(channelName, message);
        }

        public EICSModel GetEICSEQuipment(int? mmsi)
        {
            var _redis = _connectionMultiplexer.GetDatabase();
            if (mmsi != null)
            {
                var data = _redis.HashGet($"eics_{AppHelper.GetEnvironmentName()}", mmsi);
                if (data.HasValue)
                {
                    var equipments = JsonConvert.DeserializeObject<EICSSeedData>(data);
                    return new EICSModel
                    {
                        Equipments = equipments.EICS
                    };
                }
            }
            return new EICSModel();
        }

        public string GetLloydsShipType(int? imo, string callSign, string name)
        {
            var _redis = _connectionMultiplexer.GetDatabase();
            if (imo == null && string.IsNullOrWhiteSpace(callSign))
            {
                return null;
            }

            if (imo != null)
            {
                var data = _redis.HashGet($"Lloyds_{AppHelper.GetEnvironmentName()}", imo);
                if (data.HasValue)
                {
                    var lloyd = JsonConvert.DeserializeObject<Lloyd>(data);
                    return lloyd.VesselType;
                }
            }

            if (string.IsNullOrWhiteSpace(callSign) == false)
            {
                var data = _redis.HashGet($"Lloyds_{AppHelper.GetEnvironmentName()}", callSign);
                if (data.HasValue)
                {
                    var lloyd = JsonConvert.DeserializeObject<Lloyd>(data);
                    return lloyd.VesselType;
                }
            }

            if (string.IsNullOrWhiteSpace(name) == false)
            {
                var data = _redis.HashGet($"Lloyds_{AppHelper.GetEnvironmentName()}", name);
                if (data.HasValue)
                {
                    var lloyd = JsonConvert.DeserializeObject<Lloyd>(data);
                    return lloyd.VesselType;
                }
            }
            return null;
        }

      
    }
}
