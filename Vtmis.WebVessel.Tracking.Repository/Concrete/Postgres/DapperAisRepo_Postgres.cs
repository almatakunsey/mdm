﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers.RMS;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages.Target;
using Vtmis.Core.Model.AkkaModel.Messages.Target.DDMS;
using Vtmis.Core.Model.AkkaModel.Messages.Target.RMS;
using Vtmis.Core.Model.DTO.DDMS;
using Vtmis.Core.Model.DTO.Methydro;
using Vtmis.Core.Model.ViewModels.Target;
using Vtmis.Core.Model.ViewModels.Target.Vessel;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.Core.Model.DTO.Target;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.Model.AkkaModel.Messages.Route;

namespace Vtmis.WebVessel.Tracking.Repository.Concrete.Postgres
{
    public class DapperAisRepo_Postgres
    {
        private readonly NpgsqlConnection _dapper;
        public DapperAisRepo_Postgres(NpgsqlConnection dapper)
        {
            //dapper.ConnectionString = AppHelper.GetPostgresConnectionString();
            _dapper = dapper;
        }

        public Task<IEnumerable<VesselPositionViewModel>> QueryAdvanceSearchMongoDBAsync(DateTime startDate, DateTime endDate, AdvancedSearchViewModel advancedSearch)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<VesselPositionViewModel>> QueryAdvanceSearchMongoDBAsync(DateTime startDate, DateTime endDate, AdvancedSearchViewModel advancedSearch, bool isHost, IEnumerable<int> targets)
        {
            throw new NotImplementedException();
        }

        public Task<PaginatedList<ResponseAtonList>> QueryAtonListAsync(RequestAtonList input)
        {
            throw new NotImplementedException();
        }

        public Task<List<ResponseDDMSHistorical>> QueryDDMSHistoricalAsync(int mmsi, DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public Task<ResponseDDMSInfo> QueryDDMSInfo(int mmsi)
        {
            throw new NotImplementedException();
        }

        public Task<MethydroDetailInstruments> QueryInstrumentDataByMmsiDateRangeAsync(int mmsi, DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public Task<RMSLighthouse> QueryLighthouse(int mmsi)
        {
            throw new NotImplementedException();
        }

        public Task<List<ResponseWeatherHistorical>> QueryMethydroHistorical(int mmsi, DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public Task<PaginatedList<MethydroStation>> QueryMethydroStation(int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }

        public Task<PaginatedList<ResponseMethydroList>> QueryMetList(RequestMethydroList m)
        {
            throw new NotImplementedException();
        }

        public Task<PaginatedList<ResponseMonList>> QueryMonListAsync(RequestMonList input)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ShipPosition>> QueryPlayback(int mmsi, DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public Task<List<object>> QueryRMSHistoricalAsync(int mmsi, DateTime startDate, DateTime endDate)
        {
            throw new NotImplementedException();
        }

        public Task<object> QueryRMSInfoAsync(int mmsi)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<QueryShipPosStatic>> QueryRouteEta(DateTime requestDate, int lastTransmit, double minSog, double maxSog)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<QueryUpdateTarget>> QueryUpdateTargetsAsync(DateTime startDate, DateTime endDate)
        {
            var query = await _dapper.QueryAsync<QueryUpdateTarget>(@"
                SELECT sp.MMSI, sp.RecvTime,
                sp.ShipPosID, sp.Latitude, sp.Longitude, sp.LocalRecvTime,
                sp.ROT, sp.SOG, sp.COG, sp.TrueHeading, sp.MessageId, 
                sp.PositionAccuracy, sp.RAIM, sp.IP, sp.NavigationalStatus,
                ap.MMSI AtonPosMMSI, ap.VirtualAtoN, ap.OffPosition21, ap.Health21, ap.Light21, 
                ap.RACON21,
                am.AnalogueExt1, am.AnalogueExt2, am.AnalogueInt, 
                am.OffPosition68, am.Health, am.Light, am.RACON, am.DigitalInput,
                met.MMSI metMMSI,
                ss.Name, ss.CallSign, ss.Destination, ss.Vendor, ss.IMO,
                ss.Dimension_A, ss.Dimension_B, ss.Dimension_C, ss.Dimension_D,
                ss.PositionFixingDevice, ss.ETA, ss.AtoNType, ss.MaxDraught,
                ss.ShipType
                --ShipPosition
                FROM (
                    SELECT spG.MMSI, spG.RecvTime, spG.ShipPosID, spG.Latitude, spG.Longitude, spG.LocalRecvTime, 
                    spG.ROT, spG.SOG, spG.COG,
                    spG.TrueHeading, spG.MessageId, spG.PositionAccuracy, spG.RAIM, spG.IP, spG.NavigationalStatus
                    --ShipPositionGroup
                    FROM (
                        SELECT MMSI, MAX(RecvTime) RecvTime FROM dbo.""ShipPosition"" WHERE MMSI IS NOT NULL AND RecvTime > @startDate 
                        AND RecvTime <= @endDate GROUP BY MMSI
                    ) spF 
                    INNER JOIN dbo.""ShipPosition"" spG on spF.MMSI = spG.MMSI WHERE spG.RecvTime = spF.RecvTime
                ) sp
                --AtonPosData
                LEFT JOIN LATERAL
                (
                    SELECT iap.MMSI, iap.RecvTime, iap.VirtualAtoN, iap.OffPosition21, iap.Health21, iap.Light21, iap.RACON21
                    FROM dbo.""AtonPosData"" iap 
                    WHERE iap.MMSI = sp.MMSI AND iap.RecvTime <= @endDate
                    ORDER BY iap.RecvTime DESC
                    LIMIT 1
                ) ap ON true
                --AtonMonData
                LEFT JOIN LATERAL
                (
                    SELECT iam.MMSI, iam.RecvTime, iam.AnalogueExt1, iam.AnalogueExt2, iam.AnalogueInt, iam.OffPosition68, 
                    iam.Health, iam.Light, iam.RACON, iam.DigitalInput
                    FROM dbo.""AtonMonData"" iam 
                    WHERE iam.MMSI = sp.MMSI AND iam.RecvTime <= @endDate
                    ORDER BY iam.RecvTime DESC
                    LIMIT 1
                ) am ON true
                --AtonMetData
                LEFT JOIN LATERAL
                (
                    SELECT imet.MMSI, imet.RecvTime
                    FROM dbo.""AtonMetData"" imet 
                    WHERE imet.MMSI = sp.MMSI AND imet.RecvTime <= @endDate
                    ORDER BY imet.RecvTime DESC
                    LIMIT 1
                ) met ON true
                --ShipStatic
                LEFT JOIN LATERAL
                (
                    SELECT iss.Name, iss.CallSign, iss.Destination, iss.Vendor, iss.IMO,
                    iss.Dimension_A, iss.Dimension_B, iss.Dimension_C, iss.Dimension_D,
                    iss.PositionFixingDevice, iss.ETA, iss.AtoNType, iss.MaxDraught,
                    iss.ShipType
                    FROM dbo.""ShipStatic"" iss 
                    WHERE iss.MMSI = sp.MMSI
                    LIMIT 1
                ) ss ON true
            ", new { startDate, endDate });
            return query;
        }

        public Task<ResponseVesselEta> QueryVesselEta(int mmsi, DateTime requestDate, int lastTransmit, double minSog, double maxSog)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<VesselPosition>> QueryVesselHistory()
        {
            throw new NotImplementedException();
        }

        public Task<List<VesselRoute>> QueryVesselTrack()
        {
            throw new NotImplementedException();
        }
    }
}
