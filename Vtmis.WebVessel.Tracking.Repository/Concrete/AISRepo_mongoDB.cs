﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants.Mongo;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Common.Helpers.DDMS;
using Vtmis.Core.Common.Helpers.Methydro;
using Vtmis.Core.Common.Helpers.RMS;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages.Target;
using Vtmis.Core.Model.AkkaModel.Messages.Target.DDMS;
using Vtmis.Core.Model.DTO.DDMS;
using Vtmis.Core.Model.ViewModels.Target;
using Vtmis.Core.Model.ViewModels.Target.Vessel;
using Vtmis.Core.Model.DTO.Methydro;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using MongoDB.Driver.Linq;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.Core.Model.DTO.RMS;
using Vtmis.WebAdmin.EICS;
using Vtmis.Core.Model.DTO.Target;
using Vtmis.Core.Model.AkkaModel.Messages.Route;
using Newtonsoft.Json;
using CleanArchitecture.Domain.Entities;
using CleanArchitecture.Domain.Entities.RemoteMonitor;
using CleanArchitecture.Domain.Entities.DDMS;
using CleanArchitecture.Domain.Entities.Methydro;
using Hangfire;

namespace Vtmis.WebVessel.Tracking.Repository.Concrete
{
    public class AISRepo_mongoDB : ITargetQuery
    {
        private readonly MongoDbContext _mongoDbContext;
        private readonly IMongoCollection<Target> _collection;
        private readonly IMongoCollection<Target> _fullMessagesCollection;
        private readonly IDapperAdmin _dapperAdmin;
        private readonly ICacheService _cacheService;
        private readonly IMongoCollection<RMSInfo> _rmsMessagesCollection;
        private readonly IMongoCollection<DDMSInfo> _ddmsMessagesCollection;
        private readonly IMongoCollection<MethydroInfo> _methydroMessagesCollection;
        public AISRepo_mongoDB(ICacheService cacheService)
        {
            _mongoDbContext = new MongoDbContext();
            _collection = _mongoDbContext.Database.GetCollection<Target>(MongoConst.TargetPositionCollectionName);
            _fullMessagesCollection = _mongoDbContext.Database.GetCollection<Target>(MongoConst.FullMessages);
            _rmsMessagesCollection = _mongoDbContext.Database.GetCollection<RMSInfo>(MongoConst.RMSMessages);
            _ddmsMessagesCollection = _mongoDbContext.Database.GetCollection<DDMSInfo>(MongoConst.DDMSMessages);
            _methydroMessagesCollection = _mongoDbContext.Database.GetCollection<MethydroInfo>(MongoConst.MethydroMessages);
            _cacheService = cacheService;
        }

        public async Task<ResponseVesselEta> QueryVesselEta(int mmsi, DateTime requestDate, int lastTransmit, double minSog, double maxSog)
        {
            var requestDateUTC = DateTime.SpecifyKind(requestDate, DateTimeKind.Local).ToUniversalTime().AddMinutes(-(lastTransmit));
            var filterDateUTC = DateTime.SpecifyKind(requestDate, DateTimeKind.Local).ToUniversalTime();

            var s = await _collection.Find(f => f.MMSI == mmsi &&
                    f.TargetType == CleanArchitecture.Domain.TargetType.Vessel && f.Coordinate.Latitude != null &&
                    f.Vessel.SOG >= minSog && f.Vessel.SOG <= maxSog).FirstOrDefaultAsync();
            if (s == null)
            {
                return null;
            }
            return new ResponseVesselEta
            {
                TrueHeading = (s?.Vessel?.TrueHeading.HasValue == false || s?.Vessel?.TrueHeading == 511) ? (double)s?.Vessel?.COG : (double)s?.Vessel?.TrueHeading,
                COG = s?.Vessel?.COG != null ? s.Vessel?.COG.Value : 0,
                SOG = s?.Vessel?.SOG != null ? s.Vessel.SOG.Value : 0,
                Latitude = s?.Coordinate?.Latitude.ToStringOrDefault(),
                LocalRecvTime = s.ReceivedTime.ToLocalTime(),
                Longitude = s?.Coordinate?.Longitude.ToStringOrDefault(),
                MMSI = s.MMSI,
                Name = s.Name,
                ShipTypeId = (short?)s.Vessel?.ShipType
            };
        }

        public async Task<IEnumerable<QueryShipPosStatic>> QueryRouteEta(DateTime requestDate, int lastTransmit, double minSog, double maxSog)
        {
            var requestDateUTC = DateTime.SpecifyKind(requestDate, DateTimeKind.Local).ToUniversalTime().AddMinutes(-(lastTransmit));
            var filterDateUTC = DateTime.SpecifyKind(requestDate, DateTimeKind.Local).ToUniversalTime();

            var qry = await _collection.Find(f =>
                    f.TargetType == CleanArchitecture.Domain.TargetType.Vessel && f.Coordinate.Latitude != null && f.Vessel.SOG != null &&
                    f.Vessel.SOG >= minSog && f.Vessel.SOG <= maxSog && f.ReceivedTime >= requestDateUTC).ToListAsync();

            qry = qry.Where(f => (filterDateUTC - f.ReceivedTime).TotalMinutes <= lastTransmit).ToList();
            if (qry.Any() == false)
            {
                return null;
            }
            return qry.Select(s => new QueryShipPosStatic
            {
                TrueHeading = (s.Vessel?.TrueHeading.HasValue == false || s.Vessel?.TrueHeading == 511) ? (double)s.Vessel?.COG : (double)s.Vessel?.TrueHeading,
                COG = s.Vessel.COG != null ? s.Vessel.COG.Value : 0,
                SOG = s.Vessel.SOG != null ? s.Vessel.SOG.Value : 0,
                Latitude = s.Coordinate.Latitude.HasValue ? s.Coordinate.Latitude.Value : 0,
                LocalRecvTime = s.ReceivedTime.ToLocalTime(),
                Longitude = s.Coordinate.Longitude.HasValue ? s.Coordinate.Longitude.Value : 0,
                MMSI = s.MMSI,
                Name = s.Name,
                RecvTime = s.ReceivedTime,
                ShipType = (short?)s.Vessel?.ShipType
            });
        }

        public async Task SeedVesselTrack()
        {
            await _cacheService.RemoveAllVesselTrackFromRedis();
            //Console.WriteLine("QueryVesselTrack...");
            //var vesselTrack = await QueryVesselTrack();
            //await _cacheService.SeedVesselTrackToRedis(vesselTrack);
        }

        public async Task<List<VesselRoute>> QueryVesselTrack()
        {
            try
            {
                //var endDateUtc = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local).ToUniversalTime();
                var startDateUtc = DateTime.SpecifyKind(DateTime.Now.AddMinutes(-10), DateTimeKind.Local).ToUniversalTime();
                var qry = await _collection.AsQueryable().Where(f =>
                             f.Coordinate.Latitude != null && f.ReceivedTime >= startDateUtc
                            ).ToListAsync();

                var vesselRoutes = new List<VesselRoute>();
                if (qry.Any())
                {
                    foreach (var item in qry)
                    {
                        var listRoute = new LinkedList<Vtmis.Core.Model.AkkaModel.Messages.VesselRoutePosition>();
                        var vessel = await _fullMessagesCollection.Find(f => f.MMSI == item.MMSI && f.Coordinate.Latitude != null && f.ReceivedTime >= startDateUtc
                            ).Limit(10).ToListAsync();

                        if (vessel.Any())
                        {
                            foreach (var v in vessel.OrderBy(o => o.ReceivedTime))
                            {
                                listRoute.AddLast(new Vtmis.Core.Model.AkkaModel.Messages.VesselRoutePosition(v.Coordinate.Latitude.Value, v.Coordinate.Longitude.Value));
                            }
                        }
                        var route = new VesselRoute { VesselId = item.StationId, VesselRoutePosition = listRoute };
                        vesselRoutes.Add(route);
                    }
                }
                return vesselRoutes;
            }
            catch (Exception err)
            {

                throw;
            }

        }

        public async Task SeedVesselHistory()
        {
            await _cacheService.RemoveAllVesselHistoryFromRedis();
            Console.WriteLine("Query Vessel History...");
            var targets = await QueryVesselHistory();
            await _cacheService.SeedVesselHistoriesToRedis(targets);
        }

        public async Task<IEnumerable<Vtmis.Core.Model.AkkaModel.Messages.VesselPosition>> QueryVesselHistory()
        {
            try
            {
                var startDateUtc = DateTime.SpecifyKind(DateTime.Now.AddMinutes(-30), DateTimeKind.Local).ToUniversalTime();
                var qry = await _collection.Find(f =>
                             f.Coordinate.Latitude != null && f.ReceivedTime >= startDateUtc
                            ).SortBy(s => s.ReceivedTime).ToListAsync();
                var result = qry.Select(x => MapVesselHistory(x));

                var finalResult = MapVesselPosition(result);
                return finalResult;
            }
            catch (Exception err)
            {

                throw;
            }

        }

        private QueryUpdateTarget MapVesselHistory(Target x)
        {
            double? sog = null;
            double? cog = null;
            short? positionAccuracy = null;
            int? positionFixingDevice = null;
            int? dimensionA = null;
            int? dimensionB = null;
            int? dimensionC = null;
            int? dimensionD = null;
            if (x.TargetType == CleanArchitecture.Domain.TargetType.Vessel)
            {
                sog = x.Vessel.SOG;
                cog = x.Vessel.COG;
                positionAccuracy = (short?)x.Vessel.PositionAccuracy;
                positionFixingDevice = x.Vessel.PositionFixingDevice;

                if (x.Vessel?.Dimension != null)
                {
                    dimensionA = x.Vessel.Dimension.DimensionA;
                    dimensionB = x.Vessel.Dimension.DimensionB;
                    dimensionC = x.Vessel.Dimension.DimensionC;
                    dimensionD = x.Vessel.Dimension.DimensionD;
                }
            }
            else if (x.TargetType == CleanArchitecture.Domain.TargetType.SAR)
            {
                sog = x.SAR.SOG;
                positionAccuracy = (short?)x.SAR.PositionAccuracy;

            }
            else if (x.TargetType == CleanArchitecture.Domain.TargetType.Aton)
            {
                positionAccuracy = (short?)x.AtoN.PositionAccuracy;

                positionFixingDevice = x.AtoN.PositionFixingDevice;

                if (x.AtoN?.Dimension != null)
                {
                    dimensionA = x.AtoN.Dimension.DimensionA;
                    dimensionB = x.AtoN.Dimension.DimensionB;
                    dimensionC = x.AtoN.Dimension.DimensionC;
                    dimensionD = x.AtoN.Dimension.DimensionD;
                }
            }

            string atonStatus68 = null;
            double? analogueInt = null;
            double? analogueExt1 = null;
            double? analogueExt2 = null;
            int? health = null;
            string healthDesc = null;
            int? light = null;
            string lightDesc = null;
            int? racon = null;
            string raconDesc = null;
            double? offpostition = null;
            int? lanternBatt = null;
            int? batteryStat = null;
            if (x.TargetType == CleanArchitecture.Domain.TargetType.Aton)
            {
                if (x.AtoN.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.IALA)
                {
                    atonStatus68 = x.AtoN.RMS.RMSIALA.RMStatus.Status;
                    analogueInt = x?.AtoN?.RMS?.RMSIALA?.Analogue?.AnalogueInt;
                    analogueExt1 = x?.AtoN?.RMS?.RMSIALA?.Analogue?.AnalogueExt1;
                    analogueExt2 = x?.AtoN?.RMS?.RMSIALA?.Analogue?.AnalogueExt2;
                    health = x.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSHealth?.Value;
                    healthDesc = x.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSHealth?.Description;
                    light = x.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSLight?.Value;
                    lightDesc = x.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSLight?.Description;
                    racon = x.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSRACON?.Value;
                    raconDesc = x.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSRACON?.Description;

                    offpostition = x.AtoN?.RMS?.RMSIALA?.OffPosition.Value;
                }
                else if (x.AtoN.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.LightBeacon)
                {
                    atonStatus68 = x.AtoN.RMS.RMSLightBeaconApp.RMStatus.Status;
                    analogueInt = x.AtoN.RMS.RMSLightBeaconApp.Analogue.AnalogueInt;
                    analogueExt1 = x.AtoN.RMS.RMSLightBeaconApp.Analogue.AnalogueExt1;
                    analogueExt2 = x.AtoN.RMS.RMSLightBeaconApp.Analogue.AnalogueExt2;

                    health = x.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Value;
                    healthDesc = x.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Description;
                    light = x.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSLight?.Value;
                    lightDesc = x.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSLight?.Description;
                    racon = x.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Value;
                    raconDesc = x.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Description;

                    offpostition = x.AtoN?.RMS?.RMSLightBeaconApp?.OffPosition.Value;
                    lanternBatt = x?.AtoN?.RMS?.RMSLightBeaconApp?.LanternBatt?.Value;
                }
                else if (x.AtoN.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.Lighthouse)
                {
                    atonStatus68 = x.AtoN.RMS.RMSLighthouse.RMStatus.Status;
                    analogueInt = x.AtoN.RMS.RMSLighthouse.Analogue.AnalogueInt;
                    analogueExt1 = x.AtoN.RMS.RMSLighthouse.Analogue.AnalogueExt1;
                    analogueExt2 = x.AtoN.RMS.RMSLighthouse.Analogue.AnalogueExt2;

                    health = x.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSHealth?.Value;
                    healthDesc = x.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSHealth?.Description;
                    light = x.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSLight?.Value;
                    lightDesc = x.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSLight?.Description;
                    racon = x.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSRACON?.Value;
                    raconDesc = x.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSRACON?.Description;

                    offpostition = x.AtoN?.RMS?.RMSLighthouse?.OffPosition.Value;
                }
                else if (x.AtoN.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.SelfContainedLantern)
                {
                    atonStatus68 = x.AtoN.RMS.RMSSelfContainedLantern.RMStatus.Status;
                    analogueInt = x.AtoN.RMS.RMSSelfContainedLantern.Analogue.AnalogueInt;
                    analogueExt1 = x.AtoN.RMS.RMSSelfContainedLantern.Analogue.AnalogueExt1;
                    analogueExt2 = x.AtoN.RMS.RMSSelfContainedLantern.Analogue.AnalogueExt2;

                    health = x.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSHealth?.Value;
                    healthDesc = x.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSHealth?.Description;
                    light = x.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSLight?.Value;
                    lightDesc = x.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSLight?.Description;
                    racon = x.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSRACON?.Value;
                    raconDesc = x.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSRACON?.Description;

                    offpostition = x.AtoN?.RMS?.RMSSelfContainedLantern?.OffPosition.Value;
                }
                else if (x.AtoN.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.ZeniLite)
                {
                    offpostition = x.AtoN?.RMS?.RMSZeniLite?.OffPosition.Value;
                    batteryStat = x?.AtoN?.RMS?.RMSZeniLite?.BatteryStatus?.Value;
                }
            }
            return new QueryUpdateTarget
            {
                StationId = x.StationId,
                IsMethydro = x.TargetType == CleanArchitecture.Domain.TargetType.Methydro,
                MMSI = x.MMSI,
                RecvTime = DateTime.SpecifyKind(x.ReceivedTime, DateTimeKind.Local).ToUniversalTime(),
                ShipPosID = x.MMSI,
                Latitude = x.Coordinate.Latitude,
                Longitude = x.Coordinate.Longitude,
                ROT = x.Vessel?.ROT,
                SOG = sog,
                COG = cog,
                TrueHeading = (short?)x.Vessel?.TrueHeading,
                MessageId = (short)x.MessageId,
                PositionAccuracy = positionAccuracy,
                RAIM = (short?)x.RAIM,
                LocalRecvTime = x.ReceivedTime.ToLocalTime(),
                NavigationalStatus = (short?)x.Vessel?.NavigationalStatus,
                AtonPosMMSI = x.MMSI,
                DAC = x.AtoN?.RMS?.DAC,
                FI = x.AtoN?.RMS?.FI,
                RMS = new RMSDTO
                {
                    LanternBatt = lanternBatt,
                    BatteryStat = batteryStat,
                    OffPosition21 = x.AtoN?.OffPosition,
                    AtonStatus = x.AtoN?.AtoNStatus?.Status,
                    AtonStatus68 = atonStatus68,
                    OffPosition = RMSHelper.GetOffPosition(x.AtoN?.OffPosition),
                    AnalogueExt1 = analogueExt1,
                    AnalogueExt2 = analogueExt2,
                    AnalogueInt = analogueInt,
                    DigitalInput = x.AtoN?.RMS?.RMSIALA?.DigitalInput,
                    Health = health,
                    Health21 = x.AtoN?.AtoNStatus?.RMSHealth?.Value,
                    Light = light,
                    Light21 = x.AtoN?.AtoNStatus?.RMSLight?.Value,
                    OffPosition68 = offpostition,
                    RACON = racon,
                    RACON21 = x.AtoN?.AtoNStatus?.RMSRACON?.Value
                },
                MetMMSI = x.MMSI,
                Name = x.Name,
                CallSign = x.Vessel?.CallSign,
                Destination = x.Vessel?.Destination,
                Vendor = x.Vessel?.Vendor,
                IMO = x.Vessel?.IMO,
                PositionFixingDevice = (short?)positionFixingDevice,
                ETA = x.Vessel?.ETA,
                Dimension_A = dimensionA,
                Dimension_B = dimensionB,
                Dimension_C = dimensionC,
                Dimension_D = dimensionD,
                AtoNType = (short?)x.AtoN?.AtonType,
                MaxDraught = x.Vessel?.DraughtVal,
                ShipType = (short?)x.Vessel?.ShipType,
                VirtualAtoN = (short?)x.AtoN?.VirtualAton
            };
        }

        public async Task<IEnumerable<ShipPosition>> QueryPlayback(int mmsi, DateTime startDate, DateTime endDate)
        {
            var startDateUtc = DateTime.SpecifyKind(startDate, DateTimeKind.Local).ToUniversalTime();
            var endDateUtc = DateTime.SpecifyKind(endDate, DateTimeKind.Local).ToUniversalTime();

            var qry = await _fullMessagesCollection.Find(f => f.MMSI == mmsi &&
                               f.Coordinate.Latitude != null && f.ReceivedTime >= startDateUtc && f.ReceivedTime <= endDateUtc).SortBy(s => s.ReceivedTime).ToListAsync();

            if (qry.Any())
            {
                return qry.Select(x => MapPlayback(x));
            }
            return null;
        }

        private ShipPosition MapPlayback(Target x)
        {
            double? cog = null;
            double? sog = null;
            if (x.TargetType == CleanArchitecture.Domain.TargetType.Vessel)
            {
                cog = x.Vessel?.COG;
                sog = x.Vessel?.SOG;
            }
            else if (x.TargetType == CleanArchitecture.Domain.TargetType.SAR)
            {
                cog = x.SAR?.COG;
                sog = x.SAR?.SOG;
            }
            return new ShipPosition
            {
                Latitude = x.Coordinate.Latitude.Value,
                Longitude = x.Coordinate.Longitude.Value,
                RecvTime = x.ReceivedTime,
                LocalRecvTime = DateTime.SpecifyKind(x.ReceivedTime, DateTimeKind.Utc).ToLocalTime(),
                COG = cog,
                SOG = sog,
                NavigationalStatus = (short?)x.Vessel?.NavigationalStatus,
                TrueHeading = (short)x.Vessel?.TrueHeading,
                IP = x.Vessel?.Destination
            };
        }

        public async Task<PaginatedList<ResponseMethydroList>> QueryMetList(RequestMethydroList m)
        {
            var result = new PaginatedList<ResponseMethydroList>().SetNull();

            var query = _collection.Find(f => f.MessageId == 8 && f.AtoN.Instruments != null);

            var totalItems = await query.CountDocumentsAsync();
            var items = await query.Skip((m.PageIndex - 1) * m.PageSize).Limit(m.PageSize).ToListAsync();

            var listResult = new List<ResponseMethydroList>();
            if (items.Any())
            {
                foreach (var item in items)
                {
                    double? offpostition = null;
                    string atonStatus68 = null;
                    double? analogueInt = null;
                    double? analogueExt1 = null;
                    double? analogueExt2 = null;
                    int? health = null;
                    string healthDesc = null;
                    int? light = null;
                    string lightDesc = null;
                    int? racon = null;
                    string raconDesc = null;

                    if (item?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.IALA)
                    {
                        atonStatus68 = item?.AtoN?.RMS?.RMSIALA?.RMStatus?.Status;
                        health = item?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSHealth?.Value;
                        healthDesc = item?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSHealth?.Description;
                        light = item?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSLight?.Value;
                        lightDesc = item?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSLight?.Description;
                        racon = item?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSRACON?.Value;
                        raconDesc = item?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSRACON?.Description;

                        offpostition = item?.AtoN?.RMS?.RMSIALA?.OffPosition.Value;
                    }
                    else if (item?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.LightBeacon)
                    {
                        atonStatus68 = item?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.Status;
                        analogueInt = item?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueInt;
                        analogueExt1 = item?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueExt1;
                        analogueExt2 = item?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueExt2;

                        health = item?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Value;
                        healthDesc = item?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Description;
                        light = item?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSLight?.Value;
                        lightDesc = item?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSLight?.Description;
                        racon = item?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Value;
                        raconDesc = item?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Description;

                        offpostition = item?.AtoN?.RMS?.RMSLightBeaconApp?.OffPosition.Value;
                    }
                    else if (item?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.Lighthouse)
                    {
                        atonStatus68 = item?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.Status;
                        analogueInt = item?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueInt;
                        analogueExt1 = item?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueExt1;
                        analogueExt2 = item?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueExt2;

                        health = item?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSHealth?.Value;
                        healthDesc = item?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSHealth?.Description;
                        light = item?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSLight?.Value;
                        lightDesc = item?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSLight?.Description;
                        racon = item?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSRACON?.Value;
                        raconDesc = item?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSRACON?.Description;

                        offpostition = item?.AtoN?.RMS?.RMSLighthouse?.OffPosition?.Value;
                    }
                    else if (item?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.SelfContainedLantern)
                    {
                        atonStatus68 = item?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.Status;
                        analogueInt = item?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueInt;
                        analogueExt1 = item?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueExt1;
                        analogueExt2 = item?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueExt2;

                        health = item?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSHealth?.Value;
                        healthDesc = item?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSHealth?.Description;
                        light = item?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSLight?.Value;
                        lightDesc = item?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSLight?.Description;
                        racon = item?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSRACON?.Value;
                        raconDesc = item?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSRACON?.Description;

                        offpostition = item?.AtoN?.RMS?.RMSSelfContainedLantern?.OffPosition.Value;
                    }
                    else if (item?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.ZeniLite)
                    {
                        offpostition = item?.AtoN?.RMS?.RMSZeniLite?.OffPosition?.Value;
                    }

                    string name = item.Name;
                    string report21 = CommonHelper.TimeAgo(item?.ReceivedTime.ToLocalTime());
                    string report6 = CommonHelper.TimeAgo(item?.AtoN?.RMS?.ReceivedTime.ToLocalTime());

                    var d = _collection.Find(x => x.MMSI == item.MMSI && x.MessageId == 21).Limit(1).FirstOrDefault();
                    if (d != null)
                    {
                        name = d.Name;
                        report21 = CommonHelper.TimeAgo(d?.ReceivedTime.ToLocalTime());
                        report21 = CommonHelper.TimeAgo(d?.AtoN?.RMS?.ReceivedTime.ToLocalTime());
                    }

                    listResult.Add(new ResponseMethydroList
                    {
                        StationId = item?.StationId,
                        Name = name,
                        AisName = name,
                        LastReport21 = report21,
                        LastReport6 = report6,
                        LastReport8 = CommonHelper.TimeAgo(item?.AtoN?.Instruments?.ReceivedTime.ToLocalTime()),
                        Latitude = item?.AtoN?.Instruments?.Coordinate?.Latitude.ToStringOrDefault(),
                        Longitude = item?.AtoN?.Instruments?.Coordinate?.Longitude.ToStringOrDefault(),
                        MMSI = item.MMSI,
                        OffPosition21 = RMSHelper.GetOffPosition(item?.AtoN?.OffPosition),
                        OffPosition68 = RMSHelper.GetOffPosition((int?)offpostition),
                        Type = TargetHelper.GetAtonTypeByAtonTypeId(item?.AtoN?.AtonType),
                        VirtualAton = TargetHelper.GetVirtualAtonStatus(item?.AtoN?.VirtualAton),
                        Health21 = item?.AtoN?.AtoNStatus?.RMSHealth?.Description,
                        Health68 = healthDesc,
                        Light21 = item?.AtoN?.AtoNStatus?.RMSLight?.Description,
                        Light68 = lightDesc,
                        Racon21 = item?.AtoN?.AtoNStatus?.RMSRACON?.Description,
                        Racon68 = raconDesc,
                        AirPres = item?.AtoN?.Instruments?.AirPressureInstrument?.AirPressure?.Value,
                        AirTemp = item?.AtoN?.Instruments?.AirTempInstrument?.AirTemperature?.Value,
                        WndDir = item?.AtoN?.Instruments?.WindInstrument?.WindDirection?.Value,
                        WndGust = item?.AtoN?.Instruments?.WindInstrument?.WindGust?.Value,
                        WndSpeed = item?.AtoN?.Instruments?.WindInstrument?.AverageWindSpeed?.Value
                    });
                }
            }
            result.Items = listResult;

            result.TotalItems = (int)totalItems;
            result.PageIndex = m.PageIndex;
            result.TotalPages = m.PageSize;

            return result;
        }

        public async Task<List<ResponseWeatherHistorical>> QueryMethydroHistorical(int mmsi, DateTime startDate, DateTime endDate)
        {
            var startDateUtc = DateTime.SpecifyKind(startDate, DateTimeKind.Local).ToUniversalTime();
            var endDateUtc = DateTime.SpecifyKind(endDate, DateTimeKind.Local).ToUniversalTime();
            var dacCol = MethydroHelper._MethydroCodes.Where(x => x.Key != MethydroTypes.None).Select(s => s.Value.Item1);
            var fiCol = MethydroHelper._MethydroCodes.Where(x => x.Key != MethydroTypes.None).Select(s => s.Value.Item2);

            //var atonData = await _collection.Find(x => x.StationId == mmsi.ToString() && x.MessageId == 21).Limit(1).FirstOrDefaultAsync();

            var qry = _fullMessagesCollection.Find(f => f.MMSI == mmsi && f.MessageId == 21 && f.AtoN.Instruments != null
                                && f.ReceivedTime >= startDateUtc && f.ReceivedTime <= endDateUtc).SortByDescending(s => s.ReceivedTime);

            var qryResult = await qry.ToListAsync();
            var decoded = qryResult.Select(x => MapMethydroHistorical(x));

            return decoded.ToList();
        }

        private ResponseWeatherHistorical MapMethydroHistorical(Target x)
        {
            return new ResponseWeatherHistorical
            {
                Name = x?.Name,
                UtcRecvTime = x.AtoN.Instruments.ReceivedTime,
                AirPressure = x.AtoN.Instruments.AirPressureInstrument?.AirPressure?.Value,
                AirTemperature = x.AtoN.Instruments.AirTempInstrument?.AirTemperature?.Value,
                RelHumidity = x.AtoN.Instruments.AirTempInstrument?.RelativeHumidity?.Value,
                WindDirection = x.AtoN.Instruments.WindInstrument?.WindDirection?.Value,
                WindGust = x.AtoN.Instruments.WindInstrument?.WindGust?.Value,
                WindGustDirection = x.AtoN.Instruments.WindInstrument?.WindGustDirection?.Value,
                WindSpeed = x.AtoN.Instruments.WindInstrument?.AverageWindSpeed?.Value,
                SurfCurrentDirection = x.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument?.SurfaceCurrentDirection?.Value,
                CurrentDirection2 = x.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument2?.SurfaceCurrentDirection2?.Value,
                CurrentDirection3 = x.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument3?.SurfaceCurrentDirection3?.Value,
                SurfCurrentSpeed = x.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument?.SurfaceCurrentSpeed?.Value,
                CurrentSpeed2 = x.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument2?.SurfaceCurrentSpeed2?.Value,
                CurrentSpeed3 = x.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument3?.SurfaceCurrentSpeed3?.Value,
                Visibility = x.AtoN.Instruments.HorizontalVisibility?.Value,
                WaterLevel = x.AtoN.Instruments.WaterLevelInstrument?.WaterLevel?.Value,
                WaterTemp = x.AtoN.Instruments.WaterTempInstrument?.WaterTemperature?.Value,
                WaveDirection = x.AtoN.Instruments.WaveInstrument?.WaveDirection?.Value,
                WaveHeight = x.AtoN.Instruments.WaveInstrument?.SignificantWaveHeight?.Value,
                WavePeriod = x.AtoN.Instruments.WaveInstrument?.WavePeriod?.Value
            };
        }

        public async Task<MethydroDetailInstruments> QueryInstrumentDataByMmsiDateRangeAsync(int mmsi, DateTime startDate, DateTime endDate)
        {
            var startDateUtc = DateTime.SpecifyKind(startDate, DateTimeKind.Local).ToUniversalTime();
            var endDateUtc = DateTime.SpecifyKind(endDate, DateTimeKind.Local).ToUniversalTime();
            var dacCol = MethydroHelper._MethydroCodes.Where(x => x.Key != MethydroTypes.None).Select(s => s.Value.Item1);
            var fiCol = MethydroHelper._MethydroCodes.Where(x => x.Key != MethydroTypes.None).Select(s => s.Value.Item2);
            var query = await _collection.Find(f => f.MMSI == mmsi && f.MessageId == 21 && f.AtoN.Instruments != null).FirstOrDefaultAsync();

            if (query == null)
            {
                return null;
            }

            return new MethydroDetailInstruments
            {
                DAC = query.BinaryMessage?.DAC,
                FI = query.BinaryMessage?.FI,
                LocalRecvTime = query.ReceivedTime.ToLocalTime(),
                MMSI = query.MMSI,
                UtcRecvTime = query.ReceivedTime,
                StationName = query.Name,
                Instruments = new List<MethydroInstruments> {
                  new MethydroInstruments
                    {
                        MetAirData = new MetAirData
                        {
                            AirPressure = (decimal?)query.AtoN.Instruments.AirPressureInstrument?.AirPressure?.Value,
                            AirTemperature = (float?)query.AtoN.Instruments.AirTempInstrument?.AirTemperature?.Value,
                            Humidity = (decimal?)query.AtoN.Instruments.AirTempInstrument?.RelativeHumidity?.Value,
                            WindDirection = (decimal?)query.AtoN.Instruments.WindInstrument?.WindDirection?.Value,
                            WindGust = (decimal?)query.AtoN.Instruments.WindInstrument?.WindGust?.Value,
                            WindGustDirection = (decimal?)query.AtoN.Instruments.WindInstrument?.WindGustDirection?.Value,
                            WindSpeed = (decimal?)query.AtoN.Instruments.WindInstrument?.AverageWindSpeed?.Value,
                        },
                        Date = query.ReceivedTime,
                        MetWaterData = new MetWaterData
                        {
                            CurrentDirection = (decimal?)query.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument?.SurfaceCurrentDirection?.Value,
                            CurrentDirection2 = (decimal?)query.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument2?.SurfaceCurrentDirection2?.Value,
                            CurrentDirection3 = (decimal?)query.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument3?.SurfaceCurrentDirection3?.Value,
                            CurrentSpeed = (float?)query.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument?.SurfaceCurrentSpeed?.Value,
                            CurrentSpeed2 = (float?)query.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument2?.SurfaceCurrentSpeed2?.Value,
                            CurrentSpeed3 = (float?)query.AtoN.Instruments.SurfaceInstrument?.SurfaceCurrentInstrument3?.SurfaceCurrentSpeed3?.Value,
                            Visibility = (float?)query.AtoN.Instruments.HorizontalVisibility?.Value,
                            WaterLevel = (float?)query.AtoN.Instruments.WaterLevelInstrument?.WaterLevel?.Value,
                            WaterTemperature = (float?)query.AtoN.Instruments.WaterTempInstrument?.WaterTemperature?.Value,
                            WaveDirection = (decimal?)query.AtoN.Instruments.WaveInstrument?.WaveDirection?.Value,
                            WaveHeight = (float?)query.AtoN.Instruments.WaveInstrument?.SignificantWaveHeight?.Value,
                            WavePeriod = (decimal?)query.AtoN.Instruments.WaveInstrument?.WavePeriod?.Value
                        }
                    }
                }
            };
        }

        private string GetStationName(string posName, double? latitude, double? longitude, string name, int mmsi, short? dac, short? fi, DateTime localRecvTime)
        {
            string stationName;
            stationName = posName;
            if (string.IsNullOrEmpty(stationName))
            {
                stationName = name;

                if (string.IsNullOrEmpty(stationName))
                {
                    if (latitude == null)
                    {
                        return $@"{mmsi}[{dac}_{fi}] - ({CommonHelper.TimeAgo(localRecvTime)})";
                    }
                    return $@"{mmsi}[{dac}_{fi}] - {CommonHelper.ConvertDecimalCoordToDegree(
                                                   latitude.Value, longitude.Value
                                               )} ({CommonHelper.TimeAgo(localRecvTime)})";
                }
            }
            return $@"{mmsi}[{dac}_{fi}] - {stationName} ({CommonHelper.TimeAgo(localRecvTime)})";
        }
        public async Task<PaginatedList<MethydroStation>> QueryMethydroStation(int pageIndex, int pageSize)
        {
            var result = new PaginatedList<MethydroStation>().SetNull();
            var dacCol = MethydroHelper._MethydroCodes.Where(x => x.Key != MethydroTypes.None).Select(s => s.Value.Item1);
            var fiCol = MethydroHelper._MethydroCodes.Where(x => x.Key != MethydroTypes.None).Select(s => s.Value.Item2);
            var query = _collection.Find(f => f.MessageId == 21 && f.AtoN.Instruments != null);
            //ICollection<TargetPosition> itemsTask = new HashSet<TargetPosition>();
            var totalItems = query.CountDocumentsAsync();
            var itemsTask = query.Skip((pageIndex - 1) * pageSize).Limit(pageSize).ToListAsync();
            await Task.WhenAll(totalItems, itemsTask);
            result.Items = itemsTask.Result
              .Select(s => MapMetStation(s)).ToList();

            result.TotalItems = (int)totalItems.Result;
            result.PageIndex = pageIndex;
            result.TotalPages = pageSize;

            return result;
        }

        private MethydroStation MapMetStation(Target s)
        {
            return new MethydroStation
            {
                DAC = s.AtoN.Instruments.DAC,
                FI = s.AtoN.Instruments.FI,
                MMSI = s.MMSI,
                UtcRecvTime = s.ReceivedTime,
                StationName = GetStationName(s.Name, s.AtoN?.Instruments?.Coordinate?.Latitude, s.AtoN?.Instruments?.Coordinate?.Longitude, s.AtoN?.NameExtension, s.MMSI, (short?)s.AtoN?.Instruments?.DAC,
                                         (short?)s.AtoN?.Instruments?.FI,
                                         s.ReceivedTime.ToLocalTime())
            };
        }

        public async Task<IEnumerable<VesselPositionViewModel>> QueryAdvanceSearchMongoDBAsync(DateTime startDate, DateTime endDate, AdvancedSearchViewModel advancedSearch, bool isHost, IEnumerable<int> targets)
        {
            var where = string.Empty;
            var a = string.Empty;
            if (string.IsNullOrEmpty(advancedSearch.TargetCallSign) == false || string.IsNullOrEmpty(advancedSearch.TargetName) == false ||
                    advancedSearch.TargetImo != 0 || advancedSearch.TargetMmsi.ToInt() != 0)
            {
                where = "{$and: [";
                a = "{$and: [";
                if (string.IsNullOrEmpty(advancedSearch.TargetCallSign) == false)
                {
                    //where = "" + where + "{$where : '/" + advancedSearch.TargetCallSign.ToUpper() + ".*/.test(this.Vessel.CallSign)'}";
                    where = "" + where + "{$expr: {$regexMatch: {'input': { $toString: '$Vessel.CallSign'}, 'regex': /" + advancedSearch.TargetCallSign.ToUpper() + "/}}}";
                }
                if (string.IsNullOrEmpty(advancedSearch.TargetName) == false)
                {
                    if (where.IsEqual(a) == false)
                    {
                        where = $"" + where + ",";
                    }
                    //where = "" + where + "{$where : '/" + advancedSearch.TargetName.ToUpper() + ".*/.test(this.Name)'}";
                    where = "" + where + "{$expr: {$regexMatch: {'input': { $toString: '$Name'}, 'regex': /" + advancedSearch.TargetName.ToUpper() + "/}}}";
                }
                if (advancedSearch.TargetImo != 0)
                {
                    if (where.IsEqual(a) == false)
                    {
                        where = $"" + where + ",";
                    }
                    //where = "" + where + "{Vessel.IMO : /" + advancedSearch.TargetImo + ".*/.test(this.Vessel.IMO)'}";
                    where = "" + where + "{$expr: {$regexMatch: {'input': { $toString: '$Vessel.IMO'}, 'regex': /" + advancedSearch.TargetImo + "/}}}";
                }
                if (advancedSearch.TargetMmsi.ToInt() != 0)
                {
                    if (where.IsEqual(a) == false)
                    {
                        where = $"" + where + ",";
                    }
                    //where = "" + where + "{$where : '/" + advancedSearch.TargetMmsi.ToInt() + ".*/.test(this.MMSI)'}";
                    where = "" + where + "{$expr: {$regexMatch: {'input': { $toString: '$MMSI'}, 'regex': /" + advancedSearch.TargetMmsi.ToInt() + "/}}}";
                }
            }

            if (string.IsNullOrEmpty(where))
            {
                return null;
            }
            if (isHost)
            {
                where = "" + where + ",{ReceivedTime : {$gte: new ISODate('" + startDate.ToString("yyyy-MM-ddTHH:mm:ss") + "')" +
              "}} ]}";
            }
            else
            {
                var jsonstring = JsonConvert.SerializeObject(targets.ToArray());
                where = "" + where + ",{ReceivedTime : {$gte: new ISODate('" + startDate.ToString("yyyy-MM-ddTHH:mm:ss") + "')" +
                 "}}, {MMSI: { $in: " + jsonstring + " }} ]}";
            }

            var query = await _collection.FindAsync(where, new FindOptions<Target>() { Skip = (advancedSearch.PageIndex - 1) * advancedSearch.PageSize, Limit = advancedSearch.PageSize });

            var search = (await query.ToListAsync()).Select(x => MapSearch(x));

            return search;
        }

        private VesselPositionViewModel MapSearch(Target x)
        {
            double? sog = null;
            double? cog = null;
            if (x.TargetType == CleanArchitecture.Domain.TargetType.Vessel)
            {
                sog = x.Vessel?.SOG;
                cog = x.Vessel?.COG;
            }
            else if (x.TargetType == CleanArchitecture.Domain.TargetType.SAR)
            {
                sog = x.SAR?.SOG;
                cog = x.SAR?.COG;
            }
            return new VesselPositionViewModel
            {
                MMSI = x.MMSI,
                VesselName = x.Name,
                Imo = x.Vessel?.IMO,
                CallSign = x.Vessel?.CallSign,
                VesselId = x.MMSI,
                Lat = x.Coordinate?.Latitude,
                Lgt = x.Coordinate?.Longitude,
                Sog = sog,
                Cog = cog,
                Rot = x.Vessel?.ROT,
                ReceivedTime = x.ReceivedTime,
                LocalReceivedTime = x.ReceivedTime.ToLocalTime(),
                Destination = x.Vessel?.Destination,
                ShipType = x.Vessel?.ShipType,
                ETA = x.Vessel?.ETA,
                NavStatusId = x.Vessel?.NavigationalStatus
            };
        }

        public async Task<IEnumerable<QueryUpdateTarget>> QueryUpdateTargetsAsync(DateTime startDate)
        {
            var cursor = await _collection.Find(f =>
                 f.Coordinate.Latitude != null && f.ReceivedTime >= startDate
            ).ToListAsync();

            var result = cursor.Select(x => MapUpdate(x));
            return result;

        }

        private QueryUpdateTarget MapUpdate(Target x)
        {
            double? sog = null;
            double? cog = null;
            int? positionAccuracy = null;
            int? positionFixingDevice = null;
            int? dimensionA = null;
            int? dimensionB = null;
            int? dimensionC = null;
            int? dimensionD = null;

            if (x.TargetType == CleanArchitecture.Domain.TargetType.Vessel)
            {
                positionFixingDevice = x.Vessel?.PositionFixingDevice;
                sog = x.Vessel?.SOG;
                cog = x.Vessel?.COG;
                positionAccuracy = x.Vessel?.PositionAccuracy;
                if (x.Vessel.Dimension != null)
                {
                    dimensionA = x.Vessel?.Dimension?.DimensionA;
                    dimensionB = x.Vessel?.Dimension?.DimensionB;
                    dimensionC = x.Vessel?.Dimension?.DimensionC;
                    dimensionD = x.Vessel?.Dimension?.DimensionD;
                }
            }
            else if (x.TargetType == CleanArchitecture.Domain.TargetType.SAR)
            {
                sog = x.SAR?.SOG;
                cog = x.SAR?.COG;
                positionAccuracy = x.SAR?.PositionAccuracy;
            }
            else if (x.TargetType == CleanArchitecture.Domain.TargetType.Aton)
            {
                positionAccuracy = x.AtoN?.PositionAccuracy;
                positionFixingDevice = x.AtoN?.PositionFixingDevice;
                if (x.AtoN.Dimension != null)
                {
                    dimensionA = x.AtoN?.Dimension?.DimensionA;
                    dimensionB = x.AtoN?.Dimension?.DimensionB;
                    dimensionC = x.AtoN?.Dimension?.DimensionC;
                    dimensionD = x.AtoN?.Dimension?.DimensionD;
                }
            }

            string atonStatus68 = null;
            double? analogueInt = null;
            double? analogueExt1 = null;
            double? analogueExt2 = null;
            int? health = null;
            string healthDesc = null;
            int? light = null;
            string lightDesc = null;
            int? racon = null;
            string raconDesc = null;
            double? offpostition = null;
            int? lanternBatt = null;
            int? batteryStat = null;
            if (x.TargetType == CleanArchitecture.Domain.TargetType.Aton)
            {
                if (x.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.IALA)
                {
                    atonStatus68 = x?.AtoN?.RMS?.RMSIALA?.RMStatus?.Status;
                    analogueInt = x?.AtoN?.RMS?.RMSIALA?.Analogue?.AnalogueInt;
                    analogueExt1 = x?.AtoN?.RMS?.RMSIALA?.Analogue?.AnalogueExt1;
                    analogueExt2 = x?.AtoN?.RMS?.RMSIALA?.Analogue?.AnalogueExt2;
                    health = x?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSHealth?.Value;
                    healthDesc = x?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSHealth?.Description;
                    light = x?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSLight?.Value;
                    lightDesc = x?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSLight?.Description;
                    racon = x?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSRACON?.Value;
                    raconDesc = x?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSRACON?.Description;

                    offpostition = x?.AtoN?.RMS?.RMSIALA?.OffPosition.Value;
                }
                else if (x.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.LightBeacon)
                {
                    atonStatus68 = x?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.Status;
                    analogueInt = x?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueInt;
                    analogueExt1 = x?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueExt1;
                    analogueExt2 = x?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueExt2;

                    health = x?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Value;
                    healthDesc = x?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Description;
                    light = x?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSLight?.Value;
                    lightDesc = x?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSLight?.Description;
                    racon = x?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Value;
                    raconDesc = x?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Description;

                    offpostition = x?.AtoN?.RMS?.RMSLightBeaconApp?.OffPosition.Value;
                    lanternBatt = x?.AtoN?.RMS?.RMSLightBeaconApp?.LanternBatt?.Value;
                }
                else if (x.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.Lighthouse)
                {
                    atonStatus68 = x?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.Status;
                    analogueInt = x?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueInt;
                    analogueExt1 = x?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueExt1;
                    analogueExt2 = x?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueExt2;

                    health = x?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSHealth?.Value;
                    healthDesc = x?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSHealth?.Description;
                    light = x?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSLight?.Value;
                    lightDesc = x?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSLight?.Description;
                    racon = x?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSRACON?.Value;
                    raconDesc = x?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSRACON?.Description;

                    offpostition = x?.AtoN?.RMS?.RMSLighthouse?.OffPosition?.Value;

                }
                else if (x.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.SelfContainedLantern)
                {
                    atonStatus68 = x?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.Status;
                    analogueInt = x?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueInt;
                    analogueExt1 = x?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueExt1;
                    analogueExt2 = x?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueExt2;

                    health = x?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSHealth?.Value;
                    healthDesc = x?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSHealth?.Description;
                    light = x?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSLight?.Value;
                    lightDesc = x?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSLight?.Description;
                    racon = x?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSRACON?.Value;
                    raconDesc = x?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSRACON?.Description;

                    offpostition = x?.AtoN?.RMS?.RMSSelfContainedLantern?.OffPosition.Value;
                }
                else if (x.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.ZeniLite)
                {
                    offpostition = x?.AtoN?.RMS?.RMSZeniLite?.OffPosition?.Value;
                    batteryStat = x?.AtoN?.RMS?.RMSZeniLite?.BatteryStatus?.Value;
                }
            }
            return new QueryUpdateTarget
            {
                StationId = x.StationId,
                IsMethydro = x.TargetType == CleanArchitecture.Domain.TargetType.Methydro,
                MMSI = x.MMSI,
                RecvTime = DateTime.SpecifyKind(x.ReceivedTime, DateTimeKind.Local).ToUniversalTime(),
                ShipPosID = x.MMSI,
                Latitude = x.Coordinate?.Latitude,
                Longitude = x.Coordinate?.Longitude,
                ROT = x.Vessel?.ROT,
                SOG = sog,
                COG = cog,
                TrueHeading = (short?)x.Vessel?.TrueHeading,
                MessageId = (short)x.MessageId,
                PositionAccuracy = (short?)positionAccuracy,
                RAIM = (short?)x.RAIM,
                LocalRecvTime = x.ReceivedTime.ToLocalTime(),
                NavigationalStatus = (short?)x.Vessel?.NavigationalStatus,
                AtonPosMMSI = x.MMSI,
                DAC = x.AtoN?.RMS?.DAC,
                FI = x.AtoN?.RMS?.FI,
                RMS = new RMSDTO
                {
                    LanternBatt = lanternBatt,
                    BatteryStat = batteryStat,
                    OffPosition21 = x.AtoN?.OffPosition,
                    AtonStatus = x.AtoN?.AtoNStatus?.Status,
                    AtonStatus68 = atonStatus68,
                    AnalogueExt1 = analogueExt1,
                    AnalogueExt2 = analogueExt2,
                    AnalogueInt = analogueInt,
                    DigitalInput = x.AtoN?.RMS?.RMSIALA?.DigitalInput,
                    Health = health,
                    Health21 = x.AtoN?.AtoNStatus?.RMSHealth?.Value,
                    Light = light,
                    Light21 = x.AtoN?.AtoNStatus?.RMSLight?.Value,
                    OffPosition68 = offpostition,
                    RACON = racon,
                    RACON21 = x.AtoN?.AtoNStatus?.RMSRACON?.Value
                },
                MetMMSI = x.MMSI,
                Name = x.Name,
                CallSign = x.Vessel?.CallSign,
                Destination = x.Vessel?.Destination,
                Vendor = x.Vessel?.Vendor,
                IMO = x.Vessel?.IMO,
                PositionFixingDevice = (short?)positionFixingDevice,
                ETA = x.Vessel?.ETA,
                Dimension_A = dimensionA,
                Dimension_B = dimensionB,
                Dimension_C = dimensionC,
                Dimension_D = dimensionD,
                AtoNType = (short?)x.AtoN?.AtonType,
                MaxDraught = x.Vessel?.DraughtVal,
                ShipType = (short?)x.Vessel?.ShipType,
                VirtualAtoN = (short?)x.AtoN?.VirtualAton
            };
        }

        [AutomaticRetry(Attempts = 0, OnAttemptsExceeded = AttemptsExceededAction.Delete)]
        public async Task QueryUpdateTargets(DateTime startDate, string connectionId)
        {
            var cursor = await _collection.Find(f =>
                 f.Coordinate.Latitude != null && f.ReceivedTime >= startDate
            ).ToListAsync();

            var qry = cursor.Select(x => MapUpdate(x));
            var result = MapVesselPosition(qry);
            if (result.Any())
            {
                var obj = JsonConvert.SerializeObject(result);
                await _cacheService.PublishTargetsToServiceBus($"targetsub_{AppHelper.GetEnvironmentName()}_{connectionId}", obj);
            }
        }

        private IEnumerable<Vtmis.Core.Model.AkkaModel.Messages.VesselPosition> MapVesselPosition(IEnumerable<QueryUpdateTarget> targets)
        {
            if (targets.Any() == false)
            {
                return new List<Vtmis.Core.Model.AkkaModel.Messages.VesselPosition>();
            }
            return from q in targets
                   select new Vtmis.Core.Model.AkkaModel.Messages.VesselPosition(q.StationId, q.MMSI, q.Latitude.Value, q.Longitude.Value, q.Name, q.CallSign, q.LocalRecvTime,
                     q.Destination, q.Vendor, q.IMO, q.ROT, q.SOG, q.COG, q.TrueHeading.HasValue ? q.TrueHeading.Value : (short)0, q.ShipType, q.MessageId, q.Dimension_A,
                     q.Dimension_B, q.Dimension_C, q.Dimension_D, q.PositionAccuracy,
                     q.PositionFixingDevice, q.Name.IsEqual("sart active"), q.RAIM, q.RecvTime, q.ETA, (float?)q.MaxDraught, q.NavigationalStatus, q.IP,
                     q.IsMethydro, q.RMS?.DigitalInput,
                     _cacheService.GetEICSEQuipment(q.MMSI), _cacheService.GetLloydsShipType(q.IMO, q.CallSign, q.Name),
                     //msg21
                     new Vtmis.Core.Model.AkkaModel.Messages.Msg21(q.RMS?.OffPosition21, q.RMS?.Health21, q.RMS?.AtonStatus, q.RMS?.Light21, q.RMS?.RACON21,
                     q.MessageId == 21 && RMSHelper.GetRMSType(q.DAC, q.FI) == RMSTypes.LighthouseRenewableEnergy,
                     q.VirtualAtoN == null ? -1 : q.VirtualAtoN, q.AtoNType),
                     // rms
                     new Vtmis.Core.Model.AkkaModel.Messages.RMSStats(q.RMS?.AnalogueExt1, q.RMS?.AnalogueExt2, q.RMS?.AnalogueInt, q.RMS?.OffPosition68, q.RMS?.Health,
                     q.RMS?.Light, q.RMS?.RACON, q.RMS?.AtonStatus68, q.RMS?.LanternBatt, q.RMS?.BatteryStat)
                     );
        }

        public async Task UpdateVesselHistoryAndTrack()
        {
            var qry = await QueryUpdateTargetsAsync(DateTime.Now.AddMinutes(-1).ToUniversalTime());
            var targets = MapVesselPosition(qry);
            if (targets.Any())
            {
                foreach (var item in targets)
                {
                    await _cacheService.UpdateVesselHistoryTrack(item);
                }
            }
            Console.WriteLine("Finish update history and track");
        }


        public async Task<List<object>> QueryRMSHistoricalAsync(int mmsi, DateTime startDate, DateTime endDate)
        {
            var startDateUtc = DateTime.SpecifyKind(startDate, DateTimeKind.Local).ToUniversalTime();
            var endDateUtc = DateTime.SpecifyKind(endDate, DateTimeKind.Local).ToUniversalTime();
            var dacCol = RMSHelper._RMSCode.Where(x => x.Key != RMSTypes.None).Select(s => s.Value.Item1);
            var fiCol = RMSHelper._RMSCode.Where(x => x.Key != RMSTypes.None).Select(s => s.Value.Item2);
            var qry = _fullMessagesCollection.Find(f => f.MMSI == mmsi && f.AtoN.RMS != null
                                && f.ReceivedTime >= startDateUtc && f.ReceivedTime <= endDateUtc).SortByDescending(s => s.ReceivedTime);

            var qryResult = await qry.ToListAsync();
            var decoded = qryResult.Select(x => MapRMSHistorical(x));

            return decoded.ToList();
        }

        private object MapRMSHistorical(Target x)
        {
            var rmsType = RMSHelper.GetRMSType(x.AtoN?.RMS?.DAC, x.AtoN?.RMS?.FI);

            if (rmsType == RMSTypes.IALA)
            {
                return new ResponseIALAHistorical
                {
                    MMSI = x.MMSI,
                    UtcDateTime = x.ReceivedTime,
                    LocalDateTime = x.ReceivedTime.ToLocalTime(),
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    Name = x.Name,
                    ExternalDigitalInputs = x.AtoN.RMS.RMSIALA?.DigitalInput,
                    OffPosition = x.AtoN.RMS.RMSIALA?.OffPosition?.Description,
                    Status = x.AtoN.RMS.RMSIALA?.RMStatus?.Status,
                    SupplyVoltageExt1 = x.AtoN.RMS.RMSIALA?.Analogue?.AnalogueExt1.ToStringOrDefault(true),
                    SupplyVoltageExt2 = x.AtoN.RMS.RMSIALA?.Analogue?.AnalogueExt2.ToStringOrDefault(true),
                    SupplyVoltageInt = x.AtoN.RMS.RMSIALA?.Analogue?.AnalogueInt.ToStringOrDefault(true),
                    OffPositionVal = x.AtoN.RMS.RMSIALA?.OffPosition?.Value,
                    StatusSpecific = new RMSHealthStatus
                    {
                        Health = x.AtoN.RMS.RMSIALA?.RMStatus?.RMSHealth?.Description,
                        Light = x.AtoN.RMS.RMSIALA?.RMStatus?.RMSLight?.Description,
                        Racon = x.AtoN.RMS.RMSIALA?.RMStatus?.RMSRACON?.Description
                    },
                    StatusSpecificVal = new RMSHealthStatusVal
                    {
                        Health = x.AtoN.RMS.RMSIALA?.RMStatus?.RMSHealth?.Value,
                        Light = x.AtoN.RMS.RMSIALA?.RMStatus?.RMSLight?.Value,
                        Racon = x.AtoN.RMS.RMSIALA?.RMStatus?.RMSRACON?.Value
                    }
                };
            }
            else if (rmsType == RMSTypes.LightBeaconApplication)
            {
                //var decodeResult = (RMSLightBeaconApp)decode;
                return new ResponseLightBeaconHistorical
                {
                    MMSI = x.MMSI,
                    UtcDateTime = x.ReceivedTime,
                    LocalDateTime = x.ReceivedTime.ToLocalTime(),
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    Name = x.Name,
                    OffPosition = x.AtoN.RMS.RMSLightBeaconApp?.OffPosition?.Description,
                    Status = x.AtoN.RMS.RMSLightBeaconApp?.RMStatus?.Status,
                    SupplyVoltageExt1 = x.AtoN.RMS.RMSLightBeaconApp?.Analogue?.AnalogueExt1.ToStringOrDefault(true),
                    SupplyVoltageExt2 = x.AtoN.RMS.RMSLightBeaconApp?.Analogue?.AnalogueExt2.ToStringOrDefault(true),
                    SupplyVoltageInt = x.AtoN.RMS.RMSLightBeaconApp?.Analogue?.AnalogueInt.ToStringOrDefault(true),
                    OffPositionVal = x.AtoN.RMS.RMSLightBeaconApp?.OffPosition?.Value,
                    Ambient = x.AtoN.RMS.RMSLightBeaconApp?.Ambient?.Description,
                    Beat = x.AtoN.RMS.RMSLightBeaconApp?.Beat?.Description,
                    Door = x.AtoN.RMS.RMSLightBeaconApp?.Door?.Description,
                    Lantern = x.AtoN.RMS.RMSLightBeaconApp?.Lantern?.Description,
                    LanternBatt = x.AtoN.RMS.RMSLightBeaconApp?.LanternBatt?.Description,
                    AmbientVal = x.AtoN.RMS.RMSLightBeaconApp?.Ambient?.Value,
                    BeatVal = x.AtoN.RMS.RMSLightBeaconApp?.Beat?.Value,
                    DoorVal = x.AtoN.RMS.RMSLightBeaconApp?.Door?.Value,
                    LanternBattVal = x.AtoN.RMS.RMSLightBeaconApp?.LanternBatt?.Value,
                    LanternVal = x.AtoN.RMS.RMSLightBeaconApp?.Lantern?.Value,
                    StatusSpecific = new RMSHealthStatus
                    {
                        Health = x.AtoN.RMS.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Description,
                        Light = x.AtoN.RMS.RMSLightBeaconApp?.RMStatus?.RMSLight?.Description,
                        Racon = x.AtoN.RMS.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Description
                    },
                    StatusSpecificVal = new RMSHealthStatusVal
                    {
                        Health = x.AtoN.RMS.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Value,
                        Light = x.AtoN.RMS.RMSLightBeaconApp?.RMStatus?.RMSLight?.Value,
                        Racon = x.AtoN.RMS.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Value
                    }
                };
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                //var decodeResult = (RMSLighthouse)decode;
                return new ResponseLighthouseHistorical
                {
                    MMSI = x.MMSI,
                    UtcDateTime = x.ReceivedTime,
                    LocalDateTime = x.ReceivedTime.ToLocalTime(),
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    Name = x.Name,
                    OffPosition = x.AtoN.RMS.RMSLighthouse?.OffPosition.Description,
                    Status = x.AtoN.RMS.RMSLighthouse?.RMStatus?.Status,
                    SupplyVoltageExt1 = x.AtoN.RMS.RMSLighthouse?.Analogue?.AnalogueExt1.ToStringOrDefault(true),
                    SupplyVoltageExt2 = x.AtoN.RMS.RMSLighthouse?.Analogue?.AnalogueExt2.ToStringOrDefault(true),
                    SupplyVoltageInt = x.AtoN.RMS.RMSLighthouse?.Analogue?.AnalogueInt.ToStringOrDefault(true),
                    OffPositionVal = x.AtoN.RMS.RMSLighthouse?.OffPosition?.Value,
                    LightSensor = x.AtoN.RMS.RMSLighthouse?.LightSensor?.Description,
                    Beat = x.AtoN.RMS.RMSLighthouse?.Beat?.Description,
                    MainLanternCondition = x.AtoN.RMS.RMSLighthouse?.MainLanternCondition?.Description,
                    MainLanternStatus = x.AtoN.RMS.RMSLighthouse?.MainLanternStatus?.Description,
                    StandbyLanternCondition = x.AtoN.RMS.RMSLighthouse?.StandbyLanternCondition?.Description,
                    StandbyLanternStatus = x.AtoN.RMS.RMSLighthouse?.StandbyLanternStatus?.Description,
                    EmergencyLanternCondition = x.AtoN.RMS.RMSLighthouse?.EmergencyLanternCondition?.Description,
                    EmergencyLanternStatus = x.AtoN.RMS.RMSLighthouse?.EmergencyLanternStatus?.Description,
                    OpticDriveAStatus = x.AtoN.RMS.RMSLighthouse?.OpticDriveAStatus?.Description,
                    OpticDriveACondition = x.AtoN.RMS.RMSLighthouse?.OpticDriveBCondition?.Description,
                    OpticDriveBStatus = x.AtoN.RMS.RMSLighthouse?.OpticDriveBStatus?.Description,
                    OpticDriveBCondition = x.AtoN.RMS.RMSLighthouse?.OpticDriveBCondition?.Description,
                    HatchDoor = x.AtoN.RMS.RMSLighthouse?.HatchDoor?.Description,
                    MainPower = x.AtoN.RMS.RMSLighthouse?.MainPower?.Description,
                    BMSCondition = x.AtoN.RMS.RMSLighthouse?.BMSCondition?.Description,

                    LightSensorVal = x.AtoN.RMS.RMSLighthouse?.LightSensor?.Value,
                    BeatVal = x.AtoN.RMS.RMSLighthouse?.Beat?.Value,
                    MainLanternConditionVal = x.AtoN.RMS.RMSLighthouse?.MainLanternCondition?.Value,
                    MainLanternStatusVal = x.AtoN.RMS.RMSLighthouse?.MainLanternStatus?.Value,
                    StandbyLanternConditionVal = x.AtoN.RMS.RMSLighthouse?.StandbyLanternCondition?.Value,
                    StandbyLanternStatusVal = x.AtoN.RMS.RMSLighthouse?.StandbyLanternStatus?.Value,
                    EmergencyLanternConditionVal = x.AtoN.RMS.RMSLighthouse?.EmergencyLanternCondition?.Value,
                    EmergencyLanternStatusVal = x.AtoN.RMS.RMSLighthouse?.EmergencyLanternStatus?.Value,
                    OpticDriveAStatusVal = x.AtoN.RMS.RMSLighthouse?.OpticDriveAStatus?.Value,
                    OpticDriveAConditionVal = x.AtoN.RMS.RMSLighthouse?.OpticDriveBCondition?.Value,
                    OpticDriveBStatusVal = x.AtoN.RMS.RMSLighthouse?.OpticDriveBStatus?.Value,
                    OpticDriveBConditionVal = x.AtoN.RMS.RMSLighthouse?.OpticDriveBCondition?.Value,
                    HatchDoorVal = x.AtoN.RMS.RMSLighthouse?.HatchDoor?.Value,
                    MainPowerVal = x.AtoN.RMS.RMSLighthouse?.MainPower?.Value,
                    BMSConditionVal = x.AtoN.RMS.RMSLighthouse?.BMSCondition?.Value,
                    StatusSpecific = new RMSHealthStatus
                    {
                        Health = x.AtoN.RMS.RMSLighthouse?.RMStatus?.RMSHealth?.Description,
                        Light = x.AtoN.RMS.RMSLighthouse?.RMStatus?.RMSLight?.Description,
                        Racon = x.AtoN.RMS.RMSLighthouse?.RMStatus?.RMSRACON?.Description
                    },
                    StatusSpecificVal = new RMSHealthStatusVal
                    {
                        Health = x.AtoN.RMS.RMSLighthouse?.RMStatus?.RMSHealth?.Value,
                        Light = x.AtoN.RMS.RMSLighthouse?.RMStatus?.RMSLight?.Value,
                        Racon = x.AtoN.RMS.RMSLighthouse?.RMStatus?.RMSRACON?.Value
                    }
                };
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                //var decodeResult = (RMSSelfContainedLantern)decode;
                return new ResponseSelfContainedLanternHistorical
                {
                    MMSI = x.MMSI,
                    UtcDateTime = x.ReceivedTime,
                    LocalDateTime = x.ReceivedTime.ToLocalTime(),
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    Name = x.Name,
                    OffPosition = x.AtoN.RMS.RMSSelfContainedLantern?.OffPosition?.Description,
                    Status = x.AtoN.RMS.RMSSelfContainedLantern?.RMStatus?.Status,
                    SupplyVoltageExt1 = x.AtoN.RMS.RMSSelfContainedLantern?.Analogue?.AnalogueExt1.ToStringOrDefault(true),
                    SupplyVoltageExt2 = x.AtoN.RMS.RMSSelfContainedLantern?.Analogue?.AnalogueExt2.ToStringOrDefault(true),
                    SupplyVoltageInt = x.AtoN.RMS.RMSSelfContainedLantern?.Analogue?.AnalogueInt.ToStringOrDefault(true),
                    OffPositionVal = x.AtoN.RMS.RMSSelfContainedLantern?.OffPosition?.Value,
                    LightSensor = x.AtoN.RMS.RMSSelfContainedLantern?.LightSensor?.Description,
                    Beat = x.AtoN.RMS.RMSSelfContainedLantern?.Beat?.Description,
                    AlarmActive = x.AtoN.RMS.RMSSelfContainedLantern?.AlarmActive?.Description,
                    FlasherOffPowerThres = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherOffPowerThres?.Description,
                    FlasherOffLowVin = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherOffLowVin?.Description,
                    FlasherOffPhotoCell = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherOffPhotoCell?.Description,
                    FlasherOffTemperature = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherOffTemperature?.Description,
                    FlasherOffForceOff = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherOffForceOff?.Description,
                    IsNight = x.AtoN.RMS.RMSSelfContainedLantern?.IsNight?.Description,
                    ErrorLedShort = x.AtoN.RMS.RMSSelfContainedLantern?.ErrorLedShort?.Description,
                    ErrorLedOpen = x.AtoN.RMS.RMSSelfContainedLantern?.ErrorLedOpen?.Description,
                    ErrorLedVoltageLow = x.AtoN.RMS.RMSSelfContainedLantern?.ErrorLedVoltageLow?.Description,
                    ErrorVinLow = x.AtoN.RMS.RMSSelfContainedLantern?.ErrorVinLow?.Description,
                    ErrorPowerThres = x.AtoN.RMS.RMSSelfContainedLantern?.ErrorPowerThres?.Description,
                    FlasherAdjToLed = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherAdjToLed?.Description,
                    GSensorInterruptOccured = x.AtoN.RMS.RMSSelfContainedLantern?.GSensorInterruptOccured?.Description,
                    SolarChargingOn = x.AtoN.RMS.RMSSelfContainedLantern?.SolarChargingOn?.Description,

                    LightSensorVal = x.AtoN.RMS.RMSSelfContainedLantern?.LightSensor?.Value,
                    BeatVal = x.AtoN.RMS.RMSSelfContainedLantern?.Beat?.Value,
                    AlarmActiveVal = x.AtoN.RMS.RMSSelfContainedLantern?.AlarmActive?.Value,
                    FlasherOffPowerThresVal = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherOffPowerThres?.Value,
                    FlasherOffLowVinVal = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherOffLowVin?.Value,
                    FlasherOffPhotoCellVal = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherOffPhotoCell?.Value,
                    FlasherOffTemperatureVal = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherOffTemperature?.Value,
                    FlasherOffForceOffVal = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherOffForceOff?.Value,
                    IsNightVal = x.AtoN.RMS.RMSSelfContainedLantern?.IsNight?.Value,
                    ErrorLedShortVal = x.AtoN.RMS.RMSSelfContainedLantern?.ErrorLedShort?.Value,
                    ErrorLedOpenVal = x.AtoN.RMS.RMSSelfContainedLantern?.ErrorLedOpen?.Value,
                    ErrorLedVoltageLowVal = x.AtoN.RMS.RMSSelfContainedLantern?.ErrorLedVoltageLow?.Value,
                    ErrorVinLowVal = x.AtoN.RMS.RMSSelfContainedLantern?.ErrorVinLow?.Value,
                    ErrorPowerThresVal = x.AtoN.RMS.RMSSelfContainedLantern?.ErrorPowerThres?.Value,
                    FlasherAdjToLedVal = x.AtoN.RMS.RMSSelfContainedLantern?.FlasherAdjToLed?.Value,
                    GSensorInterruptOccuredVal = x.AtoN.RMS.RMSSelfContainedLantern?.GSensorInterruptOccured?.Value,
                    SolarChargingOnVal = x.AtoN.RMS.RMSSelfContainedLantern?.SolarChargingOn?.Value,
                    StatusSpecific = new RMSHealthStatus
                    {
                        Health = x.AtoN.RMS.RMSSelfContainedLantern?.RMStatus.RMSHealth?.Description,
                        Light = x.AtoN.RMS.RMSSelfContainedLantern?.RMStatus.RMSLight?.Description,
                        Racon = x.AtoN.RMS.RMSSelfContainedLantern?.RMStatus.RMSRACON?.Description
                    },
                    StatusSpecificVal = new RMSHealthStatusVal
                    {
                        Health = x.AtoN.RMS.RMSSelfContainedLantern?.RMStatus.RMSHealth?.Value,
                        Light = x.AtoN.RMS.RMSSelfContainedLantern?.RMStatus.RMSLight?.Value,
                        Racon = x.AtoN.RMS.RMSSelfContainedLantern?.RMStatus.RMSRACON?.Value
                    }
                };
            }
            else if (rmsType == RMSTypes.ZeniLiteBuoys)
            {
                //var decodeResult = (RMSZeniLite)decode;
                return new ResponseZeniLiteHistorical
                {
                    MMSI = x.MMSI,
                    UtcDateTime = x.ReceivedTime,
                    LocalDateTime = x.ReceivedTime.ToLocalTime(),
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    Name = x.Name,
                    OffPosition = x.AtoN.RMS.RMSZeniLite?.OffPosition?.Description,
                    VoltageData = x.AtoN.RMS.RMSZeniLite?.Voltage,
                    CurrentData = x.AtoN.RMS.RMSZeniLite?.Current,
                    LightStatus = x.AtoN.RMS.RMSZeniLite?.Light?.Description,
                    BatteryStatus = x.AtoN.RMS.RMSZeniLite?.BatteryStatus?.Description,
                    BatteryStatusVal = x.AtoN.RMS.RMSZeniLite?.BatteryStatus?.Value,
                    LightStatusVal = x.AtoN.RMS.RMSZeniLite?.Light?.Value,
                    PowerSupplyType = x.AtoN.RMS.RMSZeniLite?.PowerSupplyType?.Description,
                    PowerSupplyTypeVal = x.AtoN.RMS.RMSZeniLite?.PowerSupplyType?.Value,
                    OffPositionVal = x.AtoN.RMS.RMSZeniLite?.OffPosition?.Value
                };
            }
            return null;
        }

        public async Task<List<ResponseDDMSHistorical>> QueryDDMSHistoricalAsync(int mmsi, DateTime startDate, DateTime endDate)
        {
            var startDateUtc = DateTime.SpecifyKind(startDate, DateTimeKind.Local).ToUniversalTime();
            var endDateUtc = DateTime.SpecifyKind(endDate, DateTimeKind.Local).ToUniversalTime();
            var dacFi = DDMSHelper._DDMSCode.FirstOrDefault(x => x.Key == DDMSTypes.Dredging);
            var qry = await _fullMessagesCollection.FindAsync(f => f.MMSI == mmsi && f.Coordinate.Latitude != null && f.Vessel.Draught != null
                && f.ReceivedTime >= startDateUtc && f.ReceivedTime <= endDateUtc);

            var qryResult = await qry.ToListAsync();
            var decoded = qryResult.Select(x => MapDDMSHistorical(x));
            return decoded.ToList();
        }

        private ResponseDDMSHistorical MapDDMSHistorical(Target x)
        {
            return new ResponseDDMSHistorical
            {
                Lat = x.Coordinate.Latitude,
                Lng = x.Coordinate.Longitude,
                Name = x.Name,
                Speed = x.Vessel.SOG,
                Actual = x.Vessel.Draught.AirDraught?.Actual.Value.ToStringOrDefault(true),
                Battery = x.Vessel.Draught.Battery.Value.ToStringOrDefault(true),
                Cover = x.Vessel.Draught.Cover?.Value,
                Empty = x.Vessel.Draught.AirDraught?.Empty.Value.ToStringOrDefault(true),
                Full = x.Vessel.Draught.AirDraught?.Full.Value.ToStringOrDefault(true),
                HopperFlag = x.Vessel.Draught.Hopper?.HopperFlag?.Value,
                Hoppers = x.Vessel.Draught.Hopper?.Hoppers.Value,
                HopperDesc = x.Vessel.Draught.Hopper?.HopperFlag?.Description,
                Sonar = x.Vessel.Draught.Sonar?.Value,
                Supply = x.Vessel.Draught.Supply?.Value == true ? 1 : 0, //x.Vessel.Draught.Supply == true ? 1 : 0,
                UtcRecvTime = x.Vessel.Draught.ReceivedTime,
                MiddleValue = (x.Vessel.Draught.AirDraught?.Empty.Value + x.Vessel.Draught.AirDraught?.Full.Value) / 2,
                Half = x.Vessel.Draught.Half?.Value == true ? 1 : 0,
                CoverDesc = x.Vessel.Draught.Cover?.Description,
                Loaded = x.Vessel.Draught.Half?.Value == true ? 1 : 0,
                LoadedDesc = x.Vessel.Draught.Half?.Description,
                SonarDesc = x.Vessel.Draught.Sonar?.Description,
                SupplyDesc = x.Vessel.Draught.Supply?.Description
            };
        }

        public async Task<ResponseDDMSInfo> QueryDDMSInfo(int mmsi)
        {
            var cursor = await _fullMessagesCollection.Find(f => f.MMSI == mmsi && f.Vessel.Draught != null).Limit(1).SortByDescending(o => o.ReceivedTime).FirstOrDefaultAsync();// FirstOrDefaultAsync();

            if (cursor == null)
            {
                return null;
            }

            var x = cursor;

            return new ResponseDDMSInfo
            {
                ActualAirDraught = x.Vessel.Draught.AirDraught?.Actual.ToStringOrDefault(true),
                CasingCover = x.Vessel.Draught.Cover?.Description,
                HalfFull = x.Vessel.Draught.Half.Description.IsEqual("loaded") ? "Yes" : "No",
                MaxAirDraught = x.Vessel.Draught.AirDraught?.Empty.ToStringOrDefault(true),
                MinAirDraught = x.Vessel.Draught.AirDraught?.Full.ToStringOrDefault(true),
                ShipLoad = x.Vessel.Draught.Half?.Description,
                Sonar = x.Vessel.Draught.Sonar?.Description,
                Supply = x.Vessel.Draught.Supply?.Description,
                Voltage = x.Vessel.Draught.Battery.ToStringOrDefault(true),
                NumHoppers = x.Vessel.Draught.Hopper.Hoppers.Value,
                Hopper = x.Vessel.Draught.Hopper?.HopperFlag?.Description
            };
        }

        public async Task<object> QueryRMSInfoAsync(int mmsi)
        {
            var cursor = await _fullMessagesCollection.Find(f => f.MMSI == mmsi && f.AtoN.RMS != null).Limit(1).SortByDescending(o => o.ReceivedTime).FirstOrDefaultAsync();// FirstOrDefaultAsync();

            if (cursor == null)
            {
                return null;
            }

            if (cursor.AtoN.RMS.DAC == null || cursor.AtoN.RMS.FI == null)
            {
                return null;
            }
            //var result = RMSHelper.DecodeRMS(cursor.BinaryData, cursor.DesignatedAreaCode, cursor.FunctionalId);
            var formatType = RMSHelper.GetRMSType(cursor.AtoN.RMS.DAC, cursor.AtoN.RMS.FI);
            if (formatType == RMSTypes.SelfContainedLantern) // Mando Pro
            {
                //var c = (RMSSelfContainedLantern)result;
                //c.DAC = RMSHelper._RMSCode.FirstOrDefault(x => x.Key == RMSTypes.SelfContainedLantern).Value.Item1;
                //c.FI = RMSHelper._RMSCode.FirstOrDefault(x => x.Key == RMSTypes.SelfContainedLantern).Value.Item2;
                var x = cursor;
                return new Core.Common.Helpers.RMS.RMSSelfContainedLantern
                {
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    OffPosition = x.AtoN.RMS.RMSSelfContainedLantern.OffPosition.Description.ToStringOrDefault(),
                    Status = x.AtoN.RMS.RMSSelfContainedLantern.RMStatus.Status,
                    SupplyVoltageExt1 = x.AtoN.RMS.RMSSelfContainedLantern.Analogue.AnalogueExt1.ToStringOrDefault(true),
                    SupplyVoltageExt2 = x.AtoN.RMS.RMSSelfContainedLantern.Analogue.AnalogueExt2.ToStringOrDefault(true),
                    SupplyVoltageInt = x.AtoN.RMS.RMSSelfContainedLantern.Analogue.AnalogueInt.ToStringOrDefault(true),
                    LightSensor = x.AtoN.RMS.RMSSelfContainedLantern.LightSensor.Description,
                    Beat = x.AtoN.RMS.RMSSelfContainedLantern.Beat.Description,
                    AlarmActive = x.AtoN.RMS.RMSSelfContainedLantern.AlarmActive.Description,
                    FlasherOffPowerThres = x.AtoN.RMS.RMSSelfContainedLantern.FlasherOffPowerThres.Description,
                    FlasherOffLowVin = x.AtoN.RMS.RMSSelfContainedLantern.FlasherOffLowVin.Description,
                    FlasherOffPhotoCell = x.AtoN.RMS.RMSSelfContainedLantern.FlasherOffPhotoCell.Description,
                    FlasherOffTemperature = x.AtoN.RMS.RMSSelfContainedLantern.FlasherOffTemperature.Description,
                    FlasherOffForceOff = x.AtoN.RMS.RMSSelfContainedLantern.FlasherOffForceOff.Description,
                    IsNight = x.AtoN.RMS.RMSSelfContainedLantern.IsNight.Description,
                    ErrorLedShort = x.AtoN.RMS.RMSSelfContainedLantern.ErrorLedShort.Description,
                    ErrorLedOpen = x.AtoN.RMS.RMSSelfContainedLantern.ErrorLedOpen.Description,
                    ErrorLedVoltageLow = x.AtoN.RMS.RMSSelfContainedLantern.ErrorLedVoltageLow.Description,
                    ErrorVinLow = x.AtoN.RMS.RMSSelfContainedLantern.ErrorVinLow.Description,
                    ErrorPowerThres = x.AtoN.RMS.RMSSelfContainedLantern.ErrorPowerThres.Description,
                    FlasherAdjToLed = x.AtoN.RMS.RMSSelfContainedLantern.FlasherAdjToLed.Description,
                    GSensorInterruptOccured = x.AtoN.RMS.RMSSelfContainedLantern.GSensorInterruptOccured.Description,
                    SolarChargingOn = x.AtoN.RMS.RMSSelfContainedLantern.SolarChargingOn.Description
                };
            }
            else if (formatType == RMSTypes.IALA)
            {
                var x = cursor;
                return new Core.Common.Helpers.RMS.RMSIALA
                {
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    ExternalDigitalInputs = x.AtoN.RMS.RMSIALA.DigitalInput,
                    OffPosition = x.AtoN.RMS.RMSIALA.OffPosition.Description,
                    Status = x.AtoN.RMS.RMSIALA.RMStatus.Status,
                    SupplyVoltageExt1 = x.AtoN.RMS.RMSIALA.Analogue.AnalogueExt1.ToStringOrDefault(true),
                    SupplyVoltageExt2 = x.AtoN.RMS.RMSIALA.Analogue.AnalogueExt2.ToStringOrDefault(true),
                    SupplyVoltageInt = x.AtoN.RMS.RMSIALA.Analogue.AnalogueInt.ToStringOrDefault(true)
                };
            }
            else if (formatType == RMSTypes.LightBeaconApplication)
            {
                var x = cursor;
                return new Core.Common.Helpers.RMS.RMSLightBeaconApp
                {
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    OffPosition = x.AtoN.RMS.RMSLightBeaconApp.OffPosition.Description,
                    Status = x.AtoN.RMS.RMSLightBeaconApp.RMStatus.Status,
                    SupplyVoltageExt1 = x.AtoN.RMS.RMSLightBeaconApp.Analogue.AnalogueExt1.ToStringOrDefault(true),
                    SupplyVoltageExt2 = x.AtoN.RMS.RMSLightBeaconApp.Analogue.AnalogueExt2.ToStringOrDefault(true),
                    SupplyVoltageInt = x.AtoN.RMS.RMSLightBeaconApp.Analogue.AnalogueInt.ToStringOrDefault(true),
                    Ambient = x.AtoN.RMS.RMSLightBeaconApp.Ambient.Description,
                    Beat = x.AtoN.RMS.RMSLightBeaconApp.Beat.Description,
                    Door = x.AtoN.RMS.RMSLightBeaconApp.Door.Description,
                    Lantern = x.AtoN.RMS.RMSLightBeaconApp.Lantern.Description,
                    LanternBatt = x.AtoN.RMS.RMSLightBeaconApp.LanternBatt.Description
                };
            }
            else if (formatType == RMSTypes.LighthouseRenewableEnergy)
            {
                var x = cursor;
                return new Core.Common.Helpers.RMS.RMSLighthouse
                {
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    OffPosition = x.AtoN.RMS.RMSLighthouse.OffPosition.Description,
                    Status = x.AtoN.RMS.RMSLighthouse.RMStatus.Status,
                    SupplyVoltageExt1 = x.AtoN.RMS.RMSLighthouse.Analogue.AnalogueExt1.ToStringOrDefault(true),
                    SupplyVoltageExt2 = x.AtoN.RMS.RMSLighthouse.Analogue.AnalogueExt2.ToStringOrDefault(true),
                    SupplyVoltageInt = x.AtoN.RMS.RMSLighthouse.Analogue.AnalogueInt.ToStringOrDefault(true),
                    LightSensor = x.AtoN.RMS.RMSLighthouse.LightSensor.Description,
                    Beat = x.AtoN.RMS.RMSLighthouse.Beat.Description,
                    MainLanternCondition = x.AtoN.RMS.RMSLighthouse.MainLanternCondition.Description,
                    MainLanternStatus = x.AtoN.RMS.RMSLighthouse.MainLanternStatus.Description,
                    StandbyLanternCondition = x.AtoN.RMS.RMSLighthouse.StandbyLanternCondition.Description,
                    StandbyLanternStatus = x.AtoN.RMS.RMSLighthouse.StandbyLanternStatus.Description,
                    EmergencyLanternCondition = x.AtoN.RMS.RMSLighthouse.EmergencyLanternCondition.Description,
                    EmergencyLanternStatus = x.AtoN.RMS.RMSLighthouse.EmergencyLanternStatus.Description,
                    OpticDriveAStatus = x.AtoN.RMS.RMSLighthouse.OpticDriveAStatus.Description,
                    OpticDriveACondition = x.AtoN.RMS.RMSLighthouse.OpticDriveBCondition.Description,
                    OpticDriveBStatus = x.AtoN.RMS.RMSLighthouse.OpticDriveBStatus.Description,
                    OpticDriveBCondition = x.AtoN.RMS.RMSLighthouse.OpticDriveBCondition.Description,
                    HatchDoor = x.AtoN.RMS.RMSLighthouse.HatchDoor.Description,
                    MainPower = x.AtoN.RMS.RMSLighthouse.MainPower.Description,
                    BMSCondition = x.AtoN.RMS.RMSLighthouse.BMSCondition.Description
                };
            }
            else if (formatType == RMSTypes.ZeniLiteBuoys)
            {
                var x = cursor;
                return new Core.Common.Helpers.RMS.RMSZeniLite
                {
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    OffPosition = x.AtoN.RMS.RMSZeniLite.OffPosition.Description,
                    VoltageData = x.AtoN.RMS.RMSZeniLite.Voltage,
                    CurrentData = x.AtoN.RMS.RMSZeniLite.Current,
                    LightStatus = x.AtoN.RMS.RMSZeniLite.Light.Description,
                    BatteryStatus = x.AtoN.RMS.RMSZeniLite.BatteryStatus.Description,
                    PowerSupplyType = x.AtoN.RMS.RMSZeniLite.PowerSupplyType.Description
                };
            }
            return null;
        }

        public async Task<PaginatedList<ResponseAtonList>> QueryAtonListAsync(RequestAtonList input)
        {
            var result = new PaginatedList<ResponseAtonList>().SetNull();
            var query = _collection.Find(f => f.MessageId == 21 || f.MessageId == 8)
                                .SortByDescending(s => s.ReceivedTime);
            var totalItems = query.CountDocumentsAsync();
            var itemsTask = query.Skip((input.PageIndex - 1) * input.PageSize).Limit(input.PageSize).ToListAsync();
            await Task.WhenAll(totalItems, itemsTask);
            result.Items = itemsTask.Result
              .Select(s => MapAtonList(s)).ToList();

            result.TotalItems = (int)totalItems.Result;
            result.PageIndex = input.PageIndex;
            result.TotalPages = input.PageSize;

            return result;
        }

        private ResponseAtonList MapAtonList(Target s)
        {
            double? offpostition = null;
            string atonStatus68 = null;
            double? analogueInt = null;
            double? analogueExt1 = null;
            double? analogueExt2 = null;
            int? health = null;
            string healthDesc = null;
            int? light = null;
            string lightDesc = null;
            int? racon = null;
            string raconDesc = null;
            if (s?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.IALA)
            {
                atonStatus68 = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.Status;
                health = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSHealth?.Value;
                healthDesc = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSHealth?.Description;
                light = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSLight?.Value;
                lightDesc = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSLight?.Description;
                racon = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSRACON?.Value;
                raconDesc = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSRACON?.Description;

                offpostition = s?.AtoN?.RMS?.RMSIALA?.OffPosition.Value;
            }
            else if (s?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.LightBeacon)
            {
                atonStatus68 = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.Status;
                analogueInt = s?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueInt;
                analogueExt1 = s?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueExt1;
                analogueExt2 = s?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueExt2;

                health = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Value;
                healthDesc = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Description;
                light = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSLight?.Value;
                lightDesc = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSLight?.Description;
                racon = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Value;
                raconDesc = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Description;

                offpostition = s?.AtoN?.RMS?.RMSLightBeaconApp?.OffPosition.Value;
            }
            else if (s?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.Lighthouse)
            {
                atonStatus68 = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.Status;
                analogueInt = s?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueInt;
                analogueExt1 = s?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueExt1;
                analogueExt2 = s?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueExt2;

                health = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSHealth?.Value;
                healthDesc = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSHealth?.Description;
                light = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSLight?.Value;
                lightDesc = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSLight?.Description;
                racon = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSRACON?.Value;
                raconDesc = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSRACON?.Description;

                offpostition = s?.AtoN?.RMS?.RMSLighthouse?.OffPosition?.Value;
            }
            else if (s?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.SelfContainedLantern)
            {
                atonStatus68 = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.Status;
                analogueInt = s?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueInt;
                analogueExt1 = s?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueExt1;
                analogueExt2 = s?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueExt2;

                health = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSHealth?.Value;
                healthDesc = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSHealth?.Description;
                light = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSLight?.Value;
                lightDesc = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSLight?.Description;
                racon = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSRACON?.Value;
                raconDesc = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSRACON?.Description;

                offpostition = s?.AtoN?.RMS?.RMSSelfContainedLantern?.OffPosition.Value;
            }
            else if (s?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.ZeniLite)
            {
                offpostition = s?.AtoN?.RMS?.RMSZeniLite?.OffPosition?.Value;
            }

            string name = s.Name;
            string report21 = CommonHelper.TimeAgo(s?.ReceivedTime.ToLocalTime());
            string report6 = CommonHelper.TimeAgo(s?.AtoN?.RMS?.ReceivedTime.ToLocalTime());
            if (s.MessageId == 8)
            {
                var d = _collection.Find(x => x.MMSI == s.MMSI && x.MessageId == 21).Limit(1).FirstOrDefault();
                if (d != null)
                {
                    name = d.Name;
                    report21 = CommonHelper.TimeAgo(d?.ReceivedTime.ToLocalTime());
                    report21 = CommonHelper.TimeAgo(d?.AtoN?.RMS?.ReceivedTime.ToLocalTime());
                }
            }

            return new ResponseAtonList
            {
                StationId = s.StationId,
                Name = name,
                AisName = name,
                LastReport21 = report21,
                LastReport6 = report6,
                LastReport8 = CommonHelper.TimeAgo(s?.AtoN?.Instruments?.ReceivedTime.ToLocalTime()),
                Latitude = s.Coordinate?.Latitude.ToStringOrDefault(),
                Longitude = s.Coordinate?.Longitude.ToStringOrDefault(),
                MMSI = s.MMSI,
                OffPosition21 = RMSHelper.GetOffPosition(s?.AtoN?.OffPosition),
                OffPosition68 = RMSHelper.GetOffPosition((int?)offpostition),
                Type = TargetHelper.GetAtonTypeByAtonTypeId(s.AtoN?.AtonType),
                VirtualAton = TargetHelper.GetVirtualAtonStatus(s.AtoN?.VirtualAton),
                Health21 = s?.AtoN?.AtoNStatus?.RMSHealth?.Description,
                Light21 = s?.AtoN?.AtoNStatus?.RMSLight?.Description,
                Racon21 = s?.AtoN?.AtoNStatus?.RMSRACON?.Description,
                Racon68 = raconDesc,
                Light68 = lightDesc,
                Health68 = healthDesc,
                Health68Val = health,
                Light68Val = light,
                Racon68Val = racon,
                Health21Val = s?.AtoN?.AtoNStatus?.RMSHealth?.Value,
                Light21Val = s?.AtoN?.AtoNStatus?.RMSLight?.Value,
                Racon21Val = s?.AtoN?.AtoNStatus?.RMSRACON?.Value
            };
        }

        public async Task<PaginatedList<ResponseMonList>> QueryMonListAsync(RequestMonList input)
        {
            var result = new PaginatedList<ResponseMonList>().SetNull();
            var query = _collection.Find(f => f.MessageId == 21)
                                .SortByDescending(s => s.ReceivedTime);
            var totalItems = query.CountDocumentsAsync();
            var itemsTask = query.Skip((input.PageIndex - 1) * input.PageSize).Limit(input.PageSize).ToListAsync();
            await Task.WhenAll(totalItems, itemsTask);
            result.Items = itemsTask.Result
              .Select(s => MapMonList(s)).ToList();

            result.TotalItems = (int)totalItems.Result;
            result.PageIndex = input.PageIndex;
            result.TotalPages = input.PageSize;

            return result;
        }

        private ResponseMonList MapMonList(Target s)
        {
            double? offpostition = null;
            string atonStatus68 = null;
            double? analogueInt = null;
            double? analogueExt1 = null;
            double? analogueExt2 = null;
            int? health = null;
            string healthDesc = null;
            int? light = null;
            string lightDesc = null;
            int? racon = null;
            string raconDesc = null;
            if (s?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.IALA)
            {
                atonStatus68 = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.Status;
                health = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSHealth?.Value;
                healthDesc = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSHealth?.Description;
                light = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSLight?.Value;
                lightDesc = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSLight?.Description;
                racon = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSRACON?.Value;
                raconDesc = s?.AtoN?.RMS?.RMSIALA?.RMStatus?.RMSRACON?.Description;

                offpostition = s?.AtoN?.RMS?.RMSIALA?.OffPosition.Value;
            }
            else if (s?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.LightBeacon)
            {
                atonStatus68 = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.Status;
                analogueInt = s?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueInt;
                analogueExt1 = s?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueExt1;
                analogueExt2 = s?.AtoN?.RMS?.RMSLightBeaconApp?.Analogue?.AnalogueExt2;

                health = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Value;
                healthDesc = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSHealth?.Description;
                light = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSLight?.Value;
                lightDesc = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSLight?.Description;
                racon = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Value;
                raconDesc = s?.AtoN?.RMS?.RMSLightBeaconApp?.RMStatus?.RMSRACON?.Description;

                offpostition = s?.AtoN?.RMS?.RMSLightBeaconApp?.OffPosition.Value;
            }
            else if (s?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.Lighthouse)
            {
                atonStatus68 = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.Status;
                analogueInt = s?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueInt;
                analogueExt1 = s?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueExt1;
                analogueExt2 = s?.AtoN?.RMS?.RMSLighthouse?.Analogue?.AnalogueExt2;

                health = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSHealth?.Value;
                healthDesc = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSHealth?.Description;
                light = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSLight?.Value;
                lightDesc = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSLight?.Description;
                racon = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSRACON?.Value;
                raconDesc = s?.AtoN?.RMS?.RMSLighthouse?.RMStatus?.RMSRACON?.Description;

                offpostition = s?.AtoN?.RMS?.RMSLighthouse?.OffPosition?.Value;
            }
            else if (s?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.SelfContainedLantern)
            {
                atonStatus68 = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.Status;
                analogueInt = s?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueInt;
                analogueExt1 = s?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueExt1;
                analogueExt2 = s?.AtoN?.RMS?.RMSSelfContainedLantern?.Analogue?.AnalogueExt2;

                health = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSHealth?.Value;
                healthDesc = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSHealth?.Description;
                light = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSLight?.Value;
                lightDesc = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSLight?.Description;
                racon = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSRACON?.Value;
                raconDesc = s?.AtoN?.RMS?.RMSSelfContainedLantern?.RMStatus?.RMSRACON?.Description;

                offpostition = s?.AtoN?.RMS?.RMSSelfContainedLantern?.OffPosition.Value;
            }
            else if (s?.AtoN?.RMS?.RMSType == CleanArchitecture.Common.RMS.RMSTypes.ZeniLite)
            {
                offpostition = s?.AtoN?.RMS?.RMSZeniLite?.OffPosition?.Value;
            }
            return new ResponseMonList
            {
                StationId = s.StationId,
                Name = s.Name,
                AisName = s.Name,
                LastReport21 = CommonHelper.TimeAgo(s?.ReceivedTime.ToLocalTime()),
                LastReport6 = CommonHelper.TimeAgo(s?.AtoN?.RMS?.ReceivedTime.ToLocalTime()),
                LastReport8 = CommonHelper.TimeAgo(s?.AtoN?.Instruments?.ReceivedTime.ToLocalTime()),
                Latitude = s.Coordinate?.Latitude.ToStringOrDefault(),
                Longitude = s.Coordinate?.Longitude.ToStringOrDefault(),
                MMSI = s.MMSI,
                OffPosition21 = RMSHelper.GetOffPosition(s?.AtoN?.OffPosition),
                OffPosition68 = RMSHelper.GetOffPosition((int?)offpostition),
                Type = TargetHelper.GetAtonTypeByAtonTypeId(s.AtoN?.AtonType),
                VirtualAton = TargetHelper.GetVirtualAtonStatus(s.AtoN?.VirtualAton),
                Health21 = s?.AtoN?.AtoNStatus?.RMSHealth?.Description,
                Light21 = s?.AtoN?.AtoNStatus?.RMSLight?.Description,
                Racon21 = s?.AtoN?.AtoNStatus?.RMSRACON?.Description,
                Racon68 = raconDesc,
                Light68 = lightDesc,
                Health68 = healthDesc,
                Health68Val = health,
                Light68Val = light,
                Racon68Val = racon,
                Health21Val = s?.AtoN?.AtoNStatus?.RMSHealth?.Value,
                Light21Val = s?.AtoN?.AtoNStatus?.RMSLight?.Value,
                Racon21Val = s?.AtoN?.AtoNStatus?.RMSRACON?.Value,
                AnalogueExt1 = (float?)analogueExt1,
                AnalogueExt2 = (float?)analogueExt2,
                AnalogueInt = (float?)analogueInt,
                Battery = s?.AtoN?.RMS?.RMSZeniLite?.BatteryStatus?.Description,
                CurrentData = (float?)s?.AtoN?.RMS?.RMSZeniLite?.Current,
                PhotoCell = (short?)s?.AtoN?.RMS?.RMSSelfContainedLantern?.FlasherOffPhotoCell?.Value,
                VoltageData = (float?)s?.AtoN?.RMS?.RMSZeniLite?.Voltage
            };
        }

        public async Task<Core.Common.Helpers.RMS.RMSLighthouse> QueryLighthouse(int mmsi)
        {
            var cursor = await _fullMessagesCollection.Find(f => f.MMSI == mmsi && f.AtoN.RMS.RMSType == CleanArchitecture.Common.RMS.RMSTypes.Lighthouse).Limit(1).SortByDescending(o => o.ReceivedTime).FirstOrDefaultAsync();
            if (cursor == null)
            {
                throw new NotFoundException("Lighthouse does not exist");
            }
            if (cursor.AtoN.RMS.DAC == null || cursor.AtoN.RMS.FI == null)
            {
                throw new RequestFaultException("Invalid DAC or FI");
            }
            //var result = RMSHelper.DecodeRMS(cursor.BinaryData, cursor.DesignatedAreaCode, cursor.FunctionalId);
            var formatType = RMSHelper.GetRMSType(cursor.AtoN.RMS.DAC, cursor.AtoN.RMS.FI);
            if (formatType == RMSTypes.LighthouseRenewableEnergy)
            {
                //var c = (RMSLighthouse)result;
                var x = cursor;
                return new Core.Common.Helpers.RMS.RMSLighthouse
                {
                    DAC = x.AtoN.RMS.DAC,
                    FI = x.AtoN.RMS.FI,
                    OffPosition = x.AtoN.RMS.RMSLighthouse.OffPosition.Description,
                    Status = x.AtoN.RMS.RMSLighthouse.RMStatus.Status,
                    SupplyVoltageExt1 = x.AtoN.RMS.RMSLighthouse.Analogue.AnalogueExt1.ToStringOrDefault(true),
                    SupplyVoltageExt2 = x.AtoN.RMS.RMSLighthouse.Analogue.AnalogueExt2.ToStringOrDefault(true),
                    SupplyVoltageInt = x.AtoN.RMS.RMSLighthouse.Analogue.AnalogueInt.ToStringOrDefault(true),
                    LightSensor = x.AtoN.RMS.RMSLighthouse.LightSensor.Description,
                    Beat = x.AtoN.RMS.RMSLighthouse.Beat.Description,
                    MainLanternCondition = x.AtoN.RMS.RMSLighthouse.MainLanternCondition.Description,
                    MainLanternStatus = x.AtoN.RMS.RMSLighthouse.MainLanternStatus.Description,
                    StandbyLanternCondition = x.AtoN.RMS.RMSLighthouse.StandbyLanternCondition.Description,
                    StandbyLanternStatus = x.AtoN.RMS.RMSLighthouse.StandbyLanternStatus.Description,
                    EmergencyLanternCondition = x.AtoN.RMS.RMSLighthouse.EmergencyLanternCondition.Description,
                    EmergencyLanternStatus = x.AtoN.RMS.RMSLighthouse.EmergencyLanternStatus.Description,
                    OpticDriveAStatus = x.AtoN.RMS.RMSLighthouse.OpticDriveAStatus.Description,
                    OpticDriveACondition = x.AtoN.RMS.RMSLighthouse.OpticDriveBCondition.Description,
                    OpticDriveBStatus = x.AtoN.RMS.RMSLighthouse.OpticDriveBStatus.Description,
                    OpticDriveBCondition = x.AtoN.RMS.RMSLighthouse.OpticDriveBCondition.Description,
                    HatchDoor = x.AtoN.RMS.RMSLighthouse.HatchDoor.Description,
                    MainPower = x.AtoN.RMS.RMSLighthouse.MainPower.Description,
                    BMSCondition = x.AtoN.RMS.RMSLighthouse.BMSCondition.Description
                };
            }
            else
            {
                throw new RequestFaultException("Not a lighthouse");
            }
        }
    }
}
