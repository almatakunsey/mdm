﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.DTO;
using Vtmis.Core.Model.DTO.Report;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;

namespace Vtmis.WebVessel.Tracking.Repository.Concrete
{
    public class ReportRepository : IReportREpository
    {
        public IDbConnection Dapper { get; set; }

        public ReportRepository(IDbConnection dapper)
        {
            Dapper = dapper;
        }

        public async Task<IEnumerable<ResponseReportEnterExit>> QueryReportEnterExit(RequestReportEnterExit input)
        {

            Dapper.ConnectionString = AppHelper.GetMdmDbConnectionString();
            //because use 2 different DB
            //so need to Open & Close DB
            Dapper.Open();
            var query = await Dapper.QueryAsync<ResponseLatitudeLogitude>(@"
                          Select a.latitude,a.logitude,c.zonetype,c.radius
                            FROM ZoneItems a inner join ReportItems b on a.ZoneId = b.ZoneId
                             inner join Zones c on a.ZoneId = c.Id   
                            WHERE ReportId = @reportid", new {reportid = input.ReportId });
            Dapper.Close();

            var zonetype = query.FirstOrDefault()?.ZoneType;

            string location = "";
            if (zonetype == (int)ZoneType.Polygon)
            {
                var lineString = new List<string>();
                foreach (var zoneItem in query)
                {
                    lineString.Add($"{zoneItem.Logitude} {zoneItem.Latitude}");
                }
                var coordinate = query.ToList();
                lineString.Add($"{coordinate[0].Logitude} {coordinate[0].Latitude}");
                location = $"geography::STGeomFromText('POLYGON(({string.Join(", ", lineString.ToArray())}))', 4326)";
            }
            else if (zonetype == (int)ZoneType.Circle)
            {
                var lineString = new List<string>();
                var coordinate = query.ToList();
                location = $"geography::Point({coordinate[0].Latitude},{coordinate[0].Logitude},4326).STBuffer({coordinate[0].Radius.NMToMeters()})";
            }
            else if(zonetype == (int)ZoneType.Line_L || zonetype == (int)ZoneType.Line_R)
            {
                var lineString = new List<string>();
                foreach (var zoneItem in query)
                {
                    lineString.Add($"{zoneItem.Logitude} {zoneItem.Latitude}");
                }
                location = $"geography::STGeomFromText('LINESTRING({string.Join(", ", lineString.ToArray())})', 4326)"; 
            }


            var result = new List<ResponseReportEnterExit>();

            foreach (var item in CommonHelper.EachDay(input.EnterTime, input.ExitTime))
            {
                Dapper.ConnectionString = AppHelper.GetAisConnectionString(item);

                Dapper.Open();
                var queryvessel = await Dapper.QueryAsync<ResponseReportEnterExit>($@"
                        SELECT  sp.MMSI,ss.Name, ss.ShipType TypeId, ss.Destination, ss.CallSign, ss.IMO, ss.ETA, (ss.Dimension_A + ss.Dimension_B) Length,
                        (
                            SELECT TOP 1 et.RecvTime                                        
                            FROM (
                                 SELECT isp.RecvTime, isp.Latitude, isp.Longitude, (SELECT TOP 1 Latitude FROM ShipPosition WHERE Latitude IS NOT NULL AND MMSI=isp.MMSI  AND RecvTime<isp.RecvTime  ORDER BY isp.RecvTime DESC) PrevLat, 
	                            (SELECT TOP 1 Longitude FROM ShipPosition WHERE MMSI=isp.MMSI  AND RecvTime<isp.RecvTime ORDER BY isp.RecvTime DESC) PrevLong 
                                FROM ShipPosition isp
                            ) et 
                            WHERE {location}.STIntersects(geography::Point(ISNULL(et.PrevLat,0), ISNULL(et.PrevLong,0), 4326)) = 0 AND 
                            {location}.STIntersects(geography::Point(ISNULL(et.Latitude,0), ISNULL(et.Longitude,0), 4326)) = 1 AND
                            RecvTime Between @startdate and @enddate
                        ) EnterTime,
                            ( SELECT TOP 1 et.RecvTime                                        
                            FROM (
                                 SELECT isp.RecvTime, isp.Latitude, isp.Longitude, (SELECT TOP 1 Latitude FROM ShipPosition WHERE Latitude IS NOT NULL AND MMSI=isp.MMSI  AND RecvTime>isp.RecvTime  ORDER BY isp.RecvTime DESC) PrevLat, 
	                            (SELECT TOP 1 Longitude FROM ShipPosition WHERE MMSI=isp.MMSI  AND RecvTime>isp.RecvTime ORDER BY isp.RecvTime DESC) PrevLong 
                                FROM ShipPosition isp
                            ) et 
                            WHERE {location}.STIntersects(geography::Point(ISNULL(et.PrevLat,0),ISNULL(et.PrevLong,0), 4326)) = 1 AND 
                            {location}.STIntersects(geography::Point(ISNULL(et.Latitude,0), ISNULL(et.Longitude,0), 4326)) = 0 AND
                            RecvTime Between @startdate and @enddate
                        ) ExitTime
                        FROM ShipPosition sp
                        OUTER APPLY
                        (
                            SELECT Name, ShipType, Destination, CallSign, IMO, ETA, Dimension_A, Dimension_B
                            FROM ShipStatic
                            WHERE MMSI=sp.MMSI
                        ) ss
                         WHERE sp.MMSI > 0
                         ORDER BY sp.MMSI
                         OFFSET {(input.PageIndex - 1) * input.PageSize} ROWS FETCH NEXT {input.PageSize} ROWS ONLY
                        ", new { startdate = input.EnterTime, enddate = input.ExitTime });
                Dapper.Close();

                return queryvessel;
            }

            return result;
        }

        public async Task<IEnumerable<ResponseArrivalDeparture>> QueryReportArrivalDeparture(RequestReportEnterExit input)
        {

            Dapper.ConnectionString = AppHelper.GetMdmDbConnectionString();
            //because use 2 different DB
            //so need to Open & Close DB
            Dapper.Open();
            var query = await Dapper.QueryAsync<ResponseLatitudeLogitude>(@"
                          Select a.latitude,a.logitude,c.zonetype,c.radius
                            FROM ZoneItems a inner join ReportItems b on a.ZoneId = b.ZoneId
                             inner join Zones c on a.ZoneId = c.Id   
                            WHERE ReportId = @reportid", new { reportid = input.ReportId });
            Dapper.Close();

            var zonetype = query.FirstOrDefault()?.ZoneType;

            string location = "";
            if (zonetype == (int)ZoneType.Polygon)
            {
                var lineString = new List<string>();
                foreach (var zoneItem in query)
                {
                    lineString.Add($"{zoneItem.Logitude} {zoneItem.Latitude}");
                }
                var coordinate = query.ToList();
                lineString.Add($"{coordinate[0].Logitude} {coordinate[0].Latitude}");
                location = $"geography::STGeomFromText('POLYGON(({string.Join(", ", lineString.ToArray())}))', 4326)";
            }
            else if (zonetype == (int)ZoneType.Circle)
            {
                var lineString = new List<string>();
                var coordinate = query.ToList();
                location = $"geography::Point({coordinate[0].Latitude},{coordinate[0].Logitude},4326).STBuffer({coordinate[0].Radius.NMToMeters()})";
            }
            else if (zonetype == (int)ZoneType.Line_L || zonetype == (int)ZoneType.Line_R)
            {
                var lineString = new List<string>();
                foreach (var zoneItem in query)
                {
                    lineString.Add($"{zoneItem.Logitude} {zoneItem.Latitude}");
                }
                location = $"geography::STGeomFromText('LINESTRING({string.Join(", ", lineString.ToArray())})', 4326)";
            }


            var result = new List<ResponseArrivalDeparture>();

            foreach (var item in CommonHelper.EachDay(input.EnterTime, input.ExitTime))
            {
                Dapper.ConnectionString = AppHelper.GetAisConnectionString(item);

                Dapper.Open();
                var queryvessel = await Dapper.QueryAsync<ResponseArrivalDeparture>($@"
                        SELECT  sp.MMSI,ss.Name, ss.ShipType TypeId, ss.Destination, ss.CallSign, ss.IMO, ss.ETA, (ss.Dimension_A + ss.Dimension_B) Length,
                        (
                            SELECT TOP 1 et.RecvTime                                        
                            FROM (
                                 SELECT isp.RecvTime, isp.Latitude, isp.Longitude, (SELECT TOP 1 Latitude FROM ShipPosition WHERE Latitude IS NOT NULL AND MMSI=isp.MMSI  AND RecvTime<isp.RecvTime  ORDER BY isp.RecvTime DESC) PrevLat, 
	                            (SELECT TOP 1 Longitude FROM ShipPosition WHERE MMSI=isp.MMSI  AND RecvTime<isp.RecvTime ORDER BY isp.RecvTime DESC) PrevLong 
                                FROM ShipPosition isp
                            ) et 
                            WHERE {location}.STIntersects(geography::Point(ISNULL(et.PrevLat,0), ISNULL(et.PrevLong,0), 4326)) = 0 AND 
                            {location}.STIntersects(geography::Point(ISNULL(et.Latitude,0), ISNULL(et.Longitude,0), 4326)) = 1 AND
                            RecvTime Between @startdate and @enddate
                        ) EnterTime,
                            ( SELECT TOP 1 et.RecvTime                                        
                            FROM (
                                 SELECT isp.RecvTime, isp.Latitude, isp.Longitude, (SELECT TOP 1 Latitude FROM ShipPosition WHERE Latitude IS NOT NULL AND MMSI=isp.MMSI  AND RecvTime>isp.RecvTime  ORDER BY isp.RecvTime DESC) PrevLat, 
	                            (SELECT TOP 1 Longitude FROM ShipPosition WHERE MMSI=isp.MMSI  AND RecvTime>isp.RecvTime ORDER BY isp.RecvTime DESC) PrevLong 
                                FROM ShipPosition isp
                            ) et 
                            WHERE {location}.STIntersects(geography::Point(ISNULL(et.PrevLat,0),ISNULL(et.PrevLong,0), 4326)) = 1 AND 
                            {location}.STIntersects(geography::Point(ISNULL(et.Latitude,0), ISNULL(et.Longitude,0), 4326)) = 0 AND
                            RecvTime Between @startdate and @enddate
                        ) ExitTime
                        FROM ShipPosition sp
                        OUTER APPLY
                        (
                            SELECT Name, ShipType, Destination, CallSign, IMO, ETA, Dimension_A, Dimension_B
                            FROM ShipStatic
                            WHERE MMSI=sp.MMSI
                        ) ss
                         WHERE sp.MMSI > 0
                         ORDER BY sp.MMSI
                         OFFSET {(input.PageIndex - 1) * input.PageSize} ROWS FETCH NEXT {input.PageSize} ROWS ONLY
                        ", new { startdate = input.EnterTime, enddate = input.ExitTime });
                Dapper.Close();

                return queryvessel;
            }

            return result;
        }
    }
}
