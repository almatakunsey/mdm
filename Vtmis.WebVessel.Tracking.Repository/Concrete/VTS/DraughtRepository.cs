﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;
using Vtmis.WebVessel.Tracking.Repository.Interfaces.VTS;

namespace Vtmis.WebVessel.Tracking.Repository.Concrete.VTS
{
    public class DraughtRepository : IDraughtRepository
    {
        private readonly VtsDatabaseContext _dbContext;

        public DraughtRepository(VtsDatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Draught> GetAsync(int mmsi)
        {
            return await _dbContext.Draughts.FirstOrDefaultAsync(x => x.MMSI == mmsi);
        }

        public async Task<PaginatedList<Draught>> GetAllAsync(int pageIndex, int pageSize)
        {
            return await new PaginatedList<Draught>().CreateAsync(_dbContext.Draughts, pageIndex, pageSize);
        }

        public async Task<IEnumerable<Draught>> GetAllAsync()
        {
            return await _dbContext.Draughts.ToListAsync();
        }

        public IQueryable<Draught> Query()
        {
            return _dbContext.Draughts.AsQueryable();
        }
    }
}
