﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;

namespace Vtmis.WebVessel.Tracking.Repository.Concrete.VTS
{
    public class ShipTypesRepository : IShipTypesRepository
    {
        private readonly VtsDatabaseContext _dbContext;

        public ShipTypesRepository(VtsDatabaseContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<ShipTypes>> GetAllAsync()
        {
            return await _dbContext.ShipTypes.Where(x => x.ShipTypeName.IsEqual("all") == false).ToListAsync();
        }
    }
}
