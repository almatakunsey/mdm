﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;
using Vtmis.WebVessel.Tracking.Repository.Interfaces.VTS;

namespace Vtmis.WebVessel.Tracking.Repository.Concrete.VTS
{
    public class AxShipRepository : IAxShipRepository
    {
        private readonly VtsDatabaseContext _context;

        public AxShipRepository(VtsDatabaseContext context)
        {
            _context = context;
        }

        public async Task<AxShip> GetAsync(int mmsi)
        {
            return await _context.AxShips.FirstOrDefaultAsync(x => x.MMSI == mmsi);
        }

        public IQueryable<AxShip> Query()
        {
            return _context.AxShips.AsQueryable();
        }
    }
}
