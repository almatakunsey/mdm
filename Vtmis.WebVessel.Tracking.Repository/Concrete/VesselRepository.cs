﻿using System.Collections.Generic;
using System.Linq;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;

namespace Vtmis.WebVessel.Tracking.Repository.Concrete
{
    public class VesselRepository : IVesselRepository
    {
        private readonly VesselDatabaseContext _context;
        public VesselRepository(VesselDatabaseContext context)
        {
            _context = context;
        }

        public IEnumerable<ShipPosition> GetAllShipPosition()
        {
            return _context.ShipPositions.ToList();
        }

        public IEnumerable<ShipStatic> GetAllShipStatic()
        {
            return _context.ShipStatics.ToList();
        }

        public ShipPosition GetShipPositionById(int id)
        {
            return _context.ShipPositions.Where(x => x.ShipPosID == id).FirstOrDefault();
        }

        public ShipStatic GetShipStaticById(int id)
        {
            return _context.ShipStatics.Where(x => x.ShipStatID == id).FirstOrDefault();
        }

        public IQueryable<ShipPosition> QueryShipPosition()
        {
            return _context.ShipPositions.AsQueryable();
        }

        public IQueryable<ShipStatic> QueryShipStatic()
        {
            return _context.ShipStatics.AsQueryable();
        }
    }
}
