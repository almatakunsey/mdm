﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Vtmis.Core.Model.AkkaModel.Messages.Route;
using Vtmis.Core.Model.DTO.Methydro;
using Vtmis.Core.Model.DTO.DDMS;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Vtmis.Core.Model.ViewModels.Target.Vessel;
using Vtmis.Core.Model.ViewModels.Target;
using Vtmis.Core.Common.Helpers;
using System.Linq;
using Vtmis.Core.Model.DTO.Target;

namespace Vtmis.WebVessel.Tracking.Repository.Concrete
{
    public class DapperAisRepository : IDapperAisRepository
    {

        public IDbConnection Dapper { get; set; }

        public DapperAisRepository(IDbConnection dapper)
        {
            Dapper = dapper;
        }

        public async Task<IEnumerable<VesselPositionViewModel>> QueryAdvancedSearchAsync(DateTime startDate, DateTime endDate, AdvancedSearchViewModel advancedSearch)
        {
            var results = new List<VesselPositionViewModel>();

            foreach (var item in CommonHelper.EachDay(startDate, endDate))
            {
                Dapper.ConnectionString = AppHelper.GetAisConnectionString(item);
                var where = string.Empty;
                if (string.IsNullOrEmpty(advancedSearch.TargetCallSign) == false || string.IsNullOrEmpty(advancedSearch.TargetName) == false ||
                        advancedSearch.TargetImo != 0 || advancedSearch.TargetMmsi.ToInt() != 0)
                {
                    where = "WHERE";
                    if (string.IsNullOrEmpty(advancedSearch.TargetCallSign) == false)
                    {
                        where = $"{where} ss.CallSign LIKE '%' + @callSign + '%'";
                    }
                    if (string.IsNullOrEmpty(advancedSearch.TargetName) == false)
                    {
                        if (where.IsEqual("where") == false)
                        {
                            where = $"{where} AND";
                        }
                        where = $"{where} ss.Name LIKE '%' + @vesselName + '%'";
                    }
                    if (advancedSearch.TargetImo != 0)
                    {
                        if (where.IsEqual("where") == false)
                        {
                            where = $"{where} AND";
                        }
                        where = $"{where} ss.IMO LIKE '%' + @imo + '%'";
                    }
                    if (advancedSearch.TargetMmsi.ToInt() != 0)
                    {
                        if (where.IsEqual("where") == false)
                        {
                            where = $"{where} AND";
                        }
                        where = $"{where} sp.MMSI LIKE '%' + @mmsi + '%'";
                    }
                }
                Console.WriteLine(where);
                var qry = await Dapper.QueryAsync<VesselPositionViewModel>($@"
                    SELECT sp.ShipPosID VesselId, sp.MMSI, sp.Latitude Lat, sp.Longitude Lgt, ss.Name VesselName, 
                    ss.CallSign,
                    sp.SOG, sp.ROT, sp.COG, sp.RecvTime ReceivedTime, sp.LocalRecvTime LocalReceivedTime,
                    ss.Destination, ss.IMO, ss.ShipType, ss.ETA,
                    sp.NavigationalStatus NavStatusId
                    --ShipPosition
                    FROM (
                        SELECT spG.MMSI, spG.RecvTime, spG.ShipPosID, spG.Latitude, spG.Longitude, spG.LocalRecvTime, 
                        spG.ROT, spG.SOG, spG.COG,
                        spG.MessageId,spG.NavigationalStatus
                        --ShipPositionGroup
                        FROM (
                            SELECT MMSI, MAX(RecvTime) RecvTime FROM ShipPosition WHERE MMSI IS NOT NULL AND 
                            (RecvTime >= @startDate 
                              AND RecvTime <= @endDate) GROUP BY MMSI
                        ) spF 
                        INNER JOIN ShipPosition spG on spF.MMSI = spG.MMSI WHERE spG.RecvTime = spF.RecvTime
                    ) sp
                    --ShipStatic
                    OUTER APPLY
                    (
                        SELECT TOP 1 iss.Name, iss.CallSign, iss.Destination, iss.IMO, iss.ETA,                     
                        iss.ShipType
                        FROM ShipStatic iss 
                        WHERE iss.MMSI = sp.MMSI
                    ) ss
                    {where}
                ", new
                {
                    startDate,
                    endDate,
                    vesselName = string.IsNullOrEmpty(advancedSearch.TargetName) ? "" : advancedSearch.TargetName,
                    callSign = string.IsNullOrEmpty(advancedSearch.TargetCallSign) ? "" : advancedSearch.TargetCallSign,
                    mmsi = advancedSearch.TargetMmsi.ToInt() > 0 ? advancedSearch.TargetMmsi.ToStringOrDefault() : "",
                    imo = advancedSearch.TargetImo > 0 ? advancedSearch.TargetImo.ToStringOrDefault() : ""
                });

                if (qry.Any())
                {
                    //from t1 in db.Table1
                    //join t2 in db.Table2 on t1.field equals t2.field
                    //select new { t1.field2, t2.field3 }
                    results.AddRange(qry);

                }
            }

            var gp = results.GroupBy(x => x.MMSI)
                           .Select(g => new { MMSI = g.Key, RecvTime = g.Max(m => m.ReceivedTime) });
            var a = from b in results
                    join m in gp on b.MMSI equals m.MMSI
                    where m.RecvTime == b.ReceivedTime
                    select b;
            return a;
        }

        public async Task<IEnumerable<ResponseDDMSHistorical>> QueryDDMSHistorical(int mmsi, DateTime startdate, DateTime enddate)
        {
            //change utc (universal time) into (local Time)
            DateTime utcstartdate = DateTime.SpecifyKind(
                 startdate, DateTimeKind.Local).ToUniversalTime();
            DateTime utcenddate = DateTime.SpecifyKind(
                 enddate, DateTimeKind.Local).ToUniversalTime();

            var query = await Dapper.QueryAsync<ResponseDDMSHistorical>(@"
                SELECT d.RecvTime UtcRecvTime, d.[Empty], d.[Full], d.[Actual], d.Supply, d.[Half], d.[Battery], aa.[Name], d.[Cover],
                d.[Sonar],d.[Hoppers],d.[HopperFlag], d.[Lng], d.[Lat]
                FROM Draught d
                OUTER APPLY(
                        SELECT iss.[Name] FROM ShipStatic iss
                        WHERE iss.MMSI = d.MMSI
                       ) aa
                WHERE d.MMSI= @mmsi AND d.RecvTime >= @utcstartdate AND d.RecvTime <= @utcenddate
                
             ", new { mmsi, utcstartdate, utcenddate });

            return query;
        }
        public async Task<IEnumerable<ResponseWeatherHistorical>> QueryWeatherHistorical(int mmsi, DateTime fromDate, DateTime toDate)
        {

            DateTime fromUtcDate = DateTime.SpecifyKind(
                   fromDate, DateTimeKind.Local).ToUniversalTime();
            DateTime toUtcDate = DateTime.SpecifyKind(
                   toDate, DateTimeKind.Local).ToUniversalTime();

            var query = await Dapper.QueryAsync<ResponseWeatherHistorical>(@"
                SELECT ss.Name, met.RecvTime UtcRecvTime, met.AirPres AirPressure, met.AirTemp AirTemperature, met.RelHumidity,
                met.WndDir WindDirection, met.WndGust WindGust, met.WndGustDir WindGustDirection, met.WndSpeed WindSpeed,
                met.SurfCurSpeed SurfCurrentSpeed, met.SurfCurDir, met.WaterLevel, met.WaterTemp WaterTemperature,
                met.WaveDir WaveDirection, met.WaveHeight, met.WavePeriod, met.Visibility, met.CurDir2 CurrentDirection2,
                met.CurDir3 CurrentDirection3, met.CurSpeed2 CurrentSpeed2, met.CurSpeed3 CurrentSpeed3
                FROM AtonMetData met
                OUTER APPLY(
					SELECT Name FROM ShipStatic iss
					WHERE iss.MMSI=met.MMSI
				) ss
                WHERE met.MMSI=@mmsi AND met.RecvTime >= @fromUtcDate AND met.RecvTime <= @toUtcDate
            ", new { fromUtcDate, toUtcDate, mmsi });

            return query;
        }

        /// <summary>
        /// Query vessel eta
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="requestDate"></param>
        /// <param name="lastTransmit"></param>
        /// <param name="minSog"></param>
        /// <param name="maxSog"></param>
        /// <returns></returns>
        public async Task<ResponseVesselEta> QueryVesselEta(int mmsi, DateTime requestDate, int lastTransmit, double minSog, double maxSog)
        {
            var query = await Dapper.QueryFirstOrDefaultAsync<ResponseVesselEta>(@"                
                SELECT TOP 1 ss.Name, ss.ShipType ShipTypeId, sp.MMSI, sp.SOG, sp.COG, sp.LocalRecvTime, sp.RecvTime, sp.Latitude, sp.NavigationalStatus, sp.Longitude                
                FROM ShipPosition sp                        
                --ShipStatic
                OUTER APPLY
                (
                    SELECT TOP 1 iss.Name, iss.ShipType
                    FROM ShipStatic iss 
                    WHERE iss.MMSI = sp.MMSI
                ) ss
                 WHERE sp.MMSI = @mmsi AND sp.LocalRecvTime <= @p0 AND 
                (DATEDIFF(minute,sp.LocalRecvTime, @p0) <= @p1) AND (sp.MessageId = @p2 OR sp.MessageId = @p3 OR sp.MessageId = @p4 OR sp.MessageId = @p5)
                AND (sp.SOG >= @minSog AND sp.SOG <= @maxSog)     
                ORDER BY sp.LocalRecvTime DESC
            ", new { p0 = requestDate, p1 = lastTransmit, p2 = 1, p3 = 2, p4 = 3, p5 = 18, minSog, maxSog, mmsi });
            return query;
        }

        /// <summary>
        /// Query Route ETA
        /// </summary>
        /// <param name="requestDate">In local time</param>
        /// <param name="lastTransmit">In minutes</param>
        /// <param name="minSog"></param>
        /// <param name="maxSog"></param>
        /// <returns></returns>
        public async Task<IEnumerable<QueryShipPosStatic>> QueryRouteEta(DateTime requestDate, int lastTransmit, double minSog, double maxSog)
        {
            requestDate = requestDate.ConvertToUTC();
            var query = await Dapper.QueryAsync<QueryShipPosStatic>(@"
                SELECT sp.LocalRecvTime, sp.RecvTime, sp.MMSI, ss.Name, ss.ShipType, sp.SOG, sp.COG, sp.Latitude, sp.Longitude
                FROM (
                    SELECT spG.MMSI, spG.MessageId, spG.SOG, spG.COG, spG.LocalRecvTime, spG.RecvTime, spG.Latitude, spG.NavigationalStatus, spG.Longitude 
                    FROM (
                        SELECT MMSI, MAX(RecvTime) RecvTime, MAX(DataSourceTypeID) DataSourceTypeID 
                        FROM ShipPosition WHERE RecvTime <= @p0 AND 
                        (DATEDIFF(minute,RecvTime, @p0) <= @p1) AND (MessageId = @p2 OR MessageId = @p3 OR MessageId = @p4 OR MessageId = @p5)
                        AND (SOG >= @minSog AND SOG <= @maxSog)
                        GROUP BY MMSI
                        ) spF 
                        INNER JOIN ShipPosition spG on spF.MMSI = spG.MMSI WHERE spG.RecvTime = spF.RecvTime
                        AND spG.DataSourceTypeID = spF.DataSourceTypeID
                ) sp
                --ShipStatic
                OUTER APPLY
                (
                    SELECT TOP 1 iss.Name, iss.ShipType
                    FROM ShipStatic iss 
                    WHERE iss.MMSI = sp.MMSI
                ) ss
            ", new { p0 = requestDate, p1 = lastTransmit, p2 = 1, p3 = 2, p4 = 3, p5 = 18, minSog, maxSog });
            return query;
        }

        public async Task<QueryTargetCoordinate> QueryPreviousCoordinatesAsync(int mmsi, DateTime recvtime)
        {
            return await Dapper.QueryFirstOrDefaultAsync<QueryTargetCoordinate>($@"
                    SELECT TOP 1 MMSI, RecvTime, Latitude, Longitude, COG
                    FROM ShipPosition
                    WHERE MMSI=@mmsi AND RecvTime < @recvtime AND (DATEDIFF(minute,RecvTime, @recvtime) >= 1)
                    ORDER BY RecvTime DESC
                ", new { mmsi, recvtime });
        }

        /// <summary>
        /// Query Route ETA with pagination
        /// </summary>
        /// <param name="requestDate">In local time</param>
        /// <param name="lastTransmit">In minutes</param>
        /// <param name="minSog"></param>
        /// <param name="maxSog"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public async Task<IEnumerable<QueryShipPosStatic>> QueryRouteEta(DateTime requestDate, int lastTransmit, double minSog, double maxSog, int pageIndex, int pageSize)
        {
            requestDate = requestDate.ConvertToUTC();
            var query = await Dapper.QueryAsync<QueryShipPosStatic>($@"
                 SELECT spG.LocalRecvTime, spG.RecvTime, spG.MMSI, ss.Name, ss.ShipType, spG.SOG, spG.COG, spG.Latitude, spG.Longitude
                FROM 
                (SELECT sp.MMSI, sp.MessageId, sp.SOG, sp.COG, sp.LocalRecvTime, sp.RecvTime, sp.Latitude, sp.NavigationalStatus, sp.Longitude 
                FROM
                (SELECT MMSI, MAX(RecvTime) RecvTime 
                FROM ShipPosition WHERE RecvTime <= @p0 AND 
                (DATEDIFF(minute,RecvTime, @p0) <= @p1) AND (MessageId = @p2 OR MessageId = @p3 OR MessageId = @p4 OR MessageId = @p5)
                AND (SOG >= @minSog AND SOG <= @maxSog)
                GROUP BY MMSI) spList 
                INNER JOIN ShipPosition sp on spList.MMSI = sp.MMSI WHERE spList.RecvTime = sp.RecvTime) spG
                --ShipStatic
                OUTER APPLY
                (
                    SELECT TOP 1 iss.Name, iss.ShipType
                    FROM ShipStatic iss 
                    WHERE iss.MMSI = spG.MMSI
                ) ss
                ORDER BY spG.LocalRecvTime
                OFFSET {(pageIndex - 1) * pageSize} ROWS FETCH NEXT {pageSize} ROWS ONLY
            ", new { p0 = requestDate, p1 = lastTransmit, p2 = 1, p3 = 2, p4 = 3, p5 = 18, minSog, maxSog });
            return query;
        }

        public async Task<DateTime?> TargetLastRouteDockingTimeAsync(double placeLatitude, double placeLongitude,int mmsi, double placeRadius, DateTime requestDate)
        {
            requestDate = requestDate.ConvertToUTC();
            return await Dapper.QueryFirstOrDefaultAsync<DateTime?>($@"
                DECLARE @place geography = geography::Point({placeLatitude},{placeLongitude},4326).STBuffer({placeRadius});

                SELECT TOP 1 RecvTime FROM ShipPosition
                WHERE MMSI=@mmsi AND geography::Point(Latitude, Longitude, 4326).STIntersects(@place)=1 AND RecvTime < @requestDate
                ORDER BY RecvTime DESC
            ", new { mmsi, requestDate });
        }
    }
}
