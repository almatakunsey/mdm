﻿using System.Collections.Generic;
using System.Reflection;
using Abp.Domain.Uow;
using Abp.Modules;
using Abp.Runtime.Session;
using Castle.MicroKernel.Registration;
using Castle.Windsor.MsDependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Abp.Reflection.Extensions;
using Abp;
using Vtmis.WebAdmin.Identity;

namespace Vtmis.WebAdmin.Application.Tests
{
    [DependsOn(
        //typeof(WebAdminWebHostModule),
        //typeof(WebAdminApplicationModule),
        //typeof(WebAdminCoreModule),
        typeof(WebAdminEntityFrameworkModule),
        typeof(AbpTestBaseModule))]
    public class TestAppModule : AbpModule
    {
        private DbContextOptions<WebAdminDbContext> _hostDbContextOptions;
        private Dictionary<int, DbContextOptions<WebAdminDbContext>> _tenantDbContextOptions;

        public override void PreInitialize()
        {
           
            Configuration.Localization.IsEnabled = false;
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            SetupInMemoryDb();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }

        private void SetupInMemoryDb()
        {
            var services = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase();

            IdentityRegistrar.Register(services);

            var serviceProvider = WindsorRegistrationHelper.CreateServiceProvider(
                IocManager.IocContainer,
                services
            );

            var hostDbContextOptionsBuilder = new DbContextOptionsBuilder<WebAdminDbContext>();
            hostDbContextOptionsBuilder.UseInMemoryDatabase().UseInternalServiceProvider(serviceProvider);

            _hostDbContextOptions = hostDbContextOptionsBuilder.Options;
            _tenantDbContextOptions = new Dictionary<int, DbContextOptions<WebAdminDbContext>>();

            IocManager.IocContainer.Register(
                Component
                    .For<DbContextOptions<WebAdminDbContext>>()
                    .UsingFactoryMethod((kernel) =>
                    {
                        lock (_tenantDbContextOptions)
                        {
                            var currentUow = kernel.Resolve<ICurrentUnitOfWorkProvider>().Current;
                            var abpSession = kernel.Resolve<IAbpSession>();

                            var tenantId = currentUow != null ? currentUow.GetTenantId() : abpSession.TenantId;

                            if (tenantId == null)
                            {
                                return _hostDbContextOptions;
                            }

                            if (!_tenantDbContextOptions.ContainsKey(tenantId.Value))
                            {
                                var optionsBuilder = new DbContextOptionsBuilder<WebAdminDbContext>();
                                optionsBuilder.UseInMemoryDatabase(tenantId.Value.ToString()).UseInternalServiceProvider(serviceProvider);
                                _tenantDbContextOptions[tenantId.Value] = optionsBuilder.Options;
                            }

                            return _tenantDbContextOptions[tenantId.Value];
                        }
                    }, true)
                    .LifestyleTransient()
            );
        }
    }

    [DependsOn(typeof(AbpKernelModule))]
    public class AbpTestBaseModule : AbpModule
    {
        public override void PreInitialize()
        {
         
            Configuration.EventBus.UseDefaultEventBus = false;
            Configuration.DefaultNameOrConnectionString = "Server=localhost; Database=Vtmis_WebAdminDb;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(AbpTestBaseModule).GetAssembly());
        }
    }
}
