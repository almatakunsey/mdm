﻿namespace Vtmis.WebAdmin.Application.Tests.VesselGroups
{
    //[TestClass]
    //public class VesselGroupAppService_Test : TestAppTestBase
    //{
    //    private readonly IVesselGroupAppService _vesselGroupAppService;
    //    private readonly IObjectMapper _objectMapper;
    //    private readonly int _vesselGroupId;
    //    private readonly string _vesselGroupName;
    //    public VesselGroupAppService_Test()
    //    {
    //        LoginAsDefaultTenantAdmin();
    //        _objectMapper = Resolve<IObjectMapper>();
    //        _vesselGroupAppService = Resolve<IVesselGroupAppService>();
    //        _vesselGroupId = new Random().Next(100, 999); // Temp solution b4 services can be mock
    //        _vesselGroupName = "New Vessel Group";
    //    }

    //[TestMethod]
    //public async Task Should_Create_VesselGroup()
    //{
    //    // Arrange
    //    //var vesselGroupName = Guid.NewGuid().ToString();

    //    // Act
    //    await _vesselGroupAppService.CreateAsync(new CreateVesselGroupDto
    //    {
    //        Name = _vesselGroupName
    //    });

    //    // Assert
    //    await UsingDbContextAsync(async context =>
    //    {
    //        (await context.VesselGroups.FirstOrDefaultAsync(e => e.Name == _vesselGroupName)).ShouldNotBe(null);
    //    });
    //}

    //[TestMethod]
    //public async Task Should_Return_VesselGroup_List()
    //{
    //    var vesselGroups = await _vesselGroupAppService.GetListAsync();

    //    vesselGroups.Items.Count.ShouldBeGreaterThan(0);
    //}

    //[TestMethod]
    //public async Task Should_Return_VesselGroup_Detail()
    //{
    //    await UsingDbContextAsync(async context =>
    //    {
    //        (await _vesselGroupAppService.GetDetailAsync(
    //            new EntityDto<int> { Id = 1 })).ShouldNotBeNull();
    //    });
    //}

    //[TestMethod]
    //public async Task Should_Not_Return_VesselGroup_Detail()
    //{
    //    var vesselGroupId = 9999;
    //    await UsingDbContextAsync(async context =>
    //    {
    //        var result = await context.VesselGroups.FirstOrDefaultAsync(e => e.Id == vesselGroupId);
    //        result.ShouldBe(null);

    //        await _vesselGroupAppService.GetDetailAsync(
    //            new EntityDto<int> { Id = vesselGroupId })
    //            .ShouldThrowAsync<UserFriendlyException>("Could not found the vesselGroup, maybe it's deleted.");
    //    });
    //}

    //[TestMethod]
    //public async Task Shoud_Update_VesselGroup()
    //{
    //    var input = new VesselGroupDto
    //    {
    //        Id = 1,
    //        Name = "New Updated Vessel Group",
    //        TenantId = 1
    //    };

    //    await UsingDbContextAsync(async context =>
    //    {
    //        var vesselGroup = await context.VesselGroups.FirstOrDefaultAsync(e => e.Id == input.Id);
    //        await _vesselGroupAppService.UpdateAsync(input);
    //        var updatedVesselGroup = await context.VesselGroups.FirstOrDefaultAsync(e => e.Id == input.Id);
    //        updatedVesselGroup.Name.ShouldBeSameAs(input.Name);
    //    });
    //}

    //[TestMethod]
    //public async Task Shoud_Not_Update_VesselGroup_When_Location_IsNull()
    //{
    //    var input = new VesselGroupDto
    //    {
    //        Id = 9999,
    //        Name = Guid.NewGuid().ToString()
    //    };

    //    await UsingDbContextAsync(async context =>
    //    {
    //        var vesselGroup = await context.VesselGroups.FirstOrDefaultAsync(e => e.Id == input.Id);
    //        vesselGroup.ShouldBe(null);
    //        await _vesselGroupAppService.UpdateAsync(input)
    //            .ShouldThrowAsync<UserFriendlyException>("Could not found the vessel group, maybe it's deleted.");
    //    });

    //}

    //[TestMethod]
    //public async Task Should_Delete_VesselGroup()
    //{
    //    var vesselGroupId = 3;
    //    await UsingDbContextAsync(async context =>
    //    {
    //        context.VesselGroups.FirstOrDefaultAsync(e => e.Id == vesselGroupId).ShouldNotBeNull();
    //        await _vesselGroupAppService.DeleteAsync(vesselGroupId);
    //    });
    //}

    //[TestMethod]
    //public async Task Should_Throw_Exception_When_Delete_VesselGroup()
    //{
    //    var vesselGroupId = 9999;
    //    await UsingDbContextAsync(async context =>
    //    {
    //        var result = await context.VesselGroups.FirstOrDefaultAsync(e => e.Id == vesselGroupId);
    //        result.ShouldBe(null);
    //    });
    //    await _vesselGroupAppService.DeleteAsync(vesselGroupId)
    //        .ShouldThrowAsync<UserFriendlyException>("Could not found the vessel group, maybe it's deleted.");
    //}
}

