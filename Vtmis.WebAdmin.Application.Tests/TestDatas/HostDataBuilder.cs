using Vtmis.WebAdmin.EntityFrameworkCore;

namespace Vtmis.WebAdmin.Application.Tests.TestDatas
{
    public class HostDataBuilder
    {
        private readonly WebAdminDbContext _context;

        public HostDataBuilder(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            new HostUserBuilder(_context).Build();
            new HostTenantsBuilder(_context).Build();
            new InitialTestLocationBuilder(_context).Build();
        }
    }
}