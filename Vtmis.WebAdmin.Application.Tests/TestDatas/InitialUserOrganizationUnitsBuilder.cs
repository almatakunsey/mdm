﻿using System.Linq;
using Abp.Authorization.Users;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Vtmis.WebAdmin.MultiTenancy;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.Application.Tests.TestDatas
{
    public class InitialUserOrganizationUnitsBuilder
    {
        private readonly WebAdminDbContext _context;

        public InitialUserOrganizationUnitsBuilder(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            AddUsersToOus();
        }

        private void AddUsersToOus()
        {
            var defaultTenant = _context.Tenants.Single(t => t.TenancyName == Tenant.DefaultTenantName);
            var adminUser = _context.Users.Single(u => u.TenantId == defaultTenant.Id && u.UserName == User.AdminUserName);

            var ou11 = _context.OrganizationUnits.Single(ou => ou.DisplayName == "OU11");
            var ou21 = _context.OrganizationUnits.Single(ou => ou.DisplayName == "OU21");

            _context.UserOrganizationUnits.Add(new UserOrganizationUnit(defaultTenant.Id, adminUser.Id, ou11.Id));
            _context.UserOrganizationUnits.Add(new UserOrganizationUnit(defaultTenant.Id, adminUser.Id, ou21.Id));
        }
    }
}