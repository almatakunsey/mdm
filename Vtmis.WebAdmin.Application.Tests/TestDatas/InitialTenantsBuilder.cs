﻿
using Vtmis.WebAdmin.EntityFrameworkCore;
using Vtmis.WebAdmin.MultiTenancy;

namespace Vtmis.WebAdmin.Application.Tests.TestDatas
{
    public class InitialTenantsBuilder
    {
        private readonly WebAdminDbContext _context;

        public InitialTenantsBuilder(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            CreateTenants();
        }

        private void CreateTenants()
        {
            _context.Tenants.Add(new Tenant(Tenant.DefaultTenantName, Tenant.DefaultTenantName));
            _context.SaveChanges();
        }
    }
}