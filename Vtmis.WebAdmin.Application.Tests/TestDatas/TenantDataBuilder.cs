using Vtmis.WebAdmin.EntityFrameworkCore;

namespace Vtmis.WebAdmin.Application.Tests.TestDatas
{
    public class TenantDataBuilder
    {
        private readonly WebAdminDbContext _context;

        public TenantDataBuilder(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Build(int tenantId)
        {
            new TenantUserBuilder(_context).Build(tenantId);
        }
    }
}