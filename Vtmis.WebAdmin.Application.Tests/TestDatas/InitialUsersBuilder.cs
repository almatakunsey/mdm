using System.Linq;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Vtmis.WebAdmin.MultiTenancy;

namespace Vtmis.WebAdmin.Application.Tests.TestDatas
{
    public class InitialUsersBuilder
    {
        private readonly WebAdminDbContext _context;

        public InitialUsersBuilder(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            CreateUsers();
        }

        private void CreateUsers()
        {
            var defaultTenant = _context.Tenants.Single(t => t.TenancyName == Tenant.DefaultTenantName);

            var admin = _context.Users.Add(
                new User
                {
                    TenantId = defaultTenant.Id,
                    Name = "System",
                    Surname = "Administrator",
                    UserName = User.AdminUserName,
                    Password = "AM4OLBpptxBYmM79lGOX9egzZk3vIQU3d/gFCJzaBjAPXzYIK3tQ2N7X4fcrHtElTw==", //123qwe,
                    EmailAddress = "admin@aspnetboilerplate.com"
                });

            _context.Users.Add(
                new User
                {
                    TenantId = defaultTenant.Id,
                    Name = "System",
                    Surname = "Manager",
                    UserName = "manager",
                    Password = "AM4OLBpptxBYmM79lGOX9egzZk3vIQU3d/gFCJzaBjAPXzYIK3tQ2N7X4fcrHtElTw==",
                    EmailAddress = "manager@aspnetboilerplate.com"
                });
        }
    }
}