using System.Linq;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.Application.Tests.TestDatas
{
    public class TenantUserBuilder
    {
        private readonly WebAdminDbContext _context;

        public TenantUserBuilder(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Build(int tenantId)
        {
            CreateUsers(tenantId);
        }

        private void CreateUsers(int tenantId)
        {
            var adminUser = _context.Users.FirstOrDefault(u => u.TenantId == tenantId && u.UserName == User.AdminUserName);

            if (adminUser == null)
            {
                adminUser = _context.Users.Add(
                    new User
                    {
                        TenantId = tenantId,
                        Name = "System",
                        Surname = "Administrator",
                        UserName = User.AdminUserName,
                        Password = "AM4OLBpptxBYmM79lGOX9egzZk3vIQU3d/gFCJzaBjAPXzYIK3tQ2N7X4fcrHtElTw==", //123qwe,
                        EmailAddress = "admin@aspnetboilerplate.com"
                    }).Entity;

                _context.SaveChanges();
            }
        }
    }
}