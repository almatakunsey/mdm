﻿
//using EntityFramework.DynamicFilters;
using Vtmis.WebAdmin.EntityFrameworkCore;

namespace Vtmis.WebAdmin.Application.Tests.TestDatas
{
    public class InitialTestDataBuilder
    {
        private readonly WebAdminDbContext _context;

        public InitialTestDataBuilder(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            //_context.DisableAllFilters();

            new InitialTenantsBuilder(_context).Build();
            new InitialUsersBuilder(_context).Build();
            new InitialTestLanguagesBuilder(_context).Build();
            new InitialTestOrganizationUnitsBuilder(_context).Build();
            new InitialUserOrganizationUnitsBuilder(_context).Build();
            //new InitialTestLocationBuilder(_context).Build();
        }
    }
}