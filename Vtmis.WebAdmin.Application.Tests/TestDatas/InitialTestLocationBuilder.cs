﻿using System;
using System.Linq;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Vtmis.WebAdmin.Locations;
using Vtmis.WebAdmin.MultiTenancy;

namespace Vtmis.WebAdmin.Application.Tests.TestDatas
{
    public class InitialTestLocationBuilder
    {
        private readonly WebAdminDbContext _context;

        public InitialTestLocationBuilder(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Build()
        {
            InitializeLocationOnDatabase();
        }

        private void InitializeLocationOnDatabase()
        {
            var locations = _context.Locations.Where(u => u.Name.StartsWith("Location"));
            if (locations.Any()) return;

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName == Tenant.DefaultTenantName);

            _context.Locations.Add(new Location { Name = "Location1", Longitude = "123456", Latitude = "789012", TenantId = defaultTenant.Id });
            _context.Locations.Add(new Location { Name = "LocationToBeDeleted", Longitude = "123456", Latitude = "789012", TenantId = defaultTenant.Id });
        }
    }
}