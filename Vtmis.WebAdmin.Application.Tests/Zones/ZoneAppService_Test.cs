﻿using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Zones;
using Vtmis.WebAdmin.Zones.Dto;

namespace Vtmis.WebAdmin.Application.Tests.Zones
{
    [TestClass]
    public class ZoneAppService_Test : TestAppTestBase
    {
        private readonly IZoneService _zoneService;
        private readonly UserManager _userManager;
        private readonly IObjectMapper _objectMapper;
        private readonly CreateZoneDto _newZone;
        private readonly CreateZoneDto _newZone_invalid_userId;
        private readonly int _zoneId;
        public ZoneAppService_Test()
        {
            LoginAsDefaultTenantAdmin();
            _objectMapper = Resolve<IObjectMapper>();
            _zoneService = Resolve<IZoneService>();
            _userManager = Resolve<UserManager>();
            _newZone = new CreateZoneDto
            {
                UserId = 2,
                ZoneDto = new ZoneDto
                {
                    Name = "Test Zone 1",
                    Colour = "Red",
                    IsEnable = true,
                    IsFill = true,
                    Radius = 100,
                    ZoneType = ZoneType.Circle,
                    ZoneItems = new List<ResponseZoneItemDto>
                    {
                       new ResponseZoneItemDto
                       {
                           Latitude = "100",
                           Logitude = "200"
                       }
                    }
                }
            };
            _newZone_invalid_userId = new CreateZoneDto
            {
                UserId = 977782364
            };
            _zoneId = 2;
        }

        [TestMethod]
        public async Task Should_Create_Zone()
        {
            await UsingDbContextAsync(async context =>
            {

                var user = context.Users.Where(c => c.Id == _newZone.UserId).FirstOrDefault();
                user.ShouldNotBe(null);
                // Assert
                var filter = await context.Zones.FirstOrDefaultAsync(c => c.Name == _newZone.ZoneDto.Name
                        && c.UserId == user.Id);
                filter.ShouldBe(null);
                filter = _objectMapper.Map<Zone>(_newZone.ZoneDto);
                filter.SetPersonalizeUser(user);
                filter.ZoneItems = _objectMapper.Map<List<ZoneItem>>(_newZone.ZoneDto.ZoneItems);
                filter.UpdateZoneItemWithId();

                context.Zones.Add(filter);
                var result = context.SaveChanges();
                result.ShouldBeGreaterThan(0);
            });

        }

        [TestMethod]
        public void Should_Throw_Exception_In_Create_Zone_When_Invalid_UserId_Is_Specified()
        {
            Should.Throw<UserFriendlyException>(() => _zoneService.Create(_newZone_invalid_userId))
                                .Message.ShouldBe("Invalid user!");
        }

        [TestMethod]
        public void Should_Throw_Exception_In_Create_Zone_When_Duplicate_ZoneName_Is_Specified()
        {
            Should.Throw<UserFriendlyException>(() => _zoneService.Create(_newZone))
                                .Message.ShouldBe("Zone with same name exist!");
        }

        [TestMethod]
        public async Task Should_Update_Zone()
        {
            //We can work with repositories instead of DbContext
            var zoneRepository = Resolve<IRepository<Zone>>();
            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken cancellationToken = source.Token;
            var updatedZone = new UpdateZoneDto
            {
                UserId = 2,
                ZoneDto = new ZoneDto
                {
                    Id = 2,
                    Name = "Updated Zone 1",
                    Colour = "Red",
                    IsEnable = true,
                    IsFill = true,
                    Radius = 100,
                    ZoneType = ZoneType.Circle,
                    ZoneItems = new List<ResponseZoneItemDto>
                    {
                       new ResponseZoneItemDto
                       {
                           Latitude = "100",
                           Logitude = "200"
                       }
                    }
                }
            };
            await UsingDbContextAsync(async context =>
            {
                var zone = await zoneRepository.GetAsync(updatedZone.ZoneDto.Id);
                zone.ShouldNotBe(null);
                await zoneRepository.EnsureCollectionLoadedAsync(zone, x => x.ZoneItems, cancellationToken);
                zone.ZoneItems.Any().ShouldBe(true);

                _objectMapper.Map(updatedZone.ZoneDto, zone);
                _objectMapper.Map(updatedZone.ZoneDto.ZoneItems, zone.ZoneItems);

                await zoneRepository.UpdateAsync(zone);
            });                
        }

        [TestMethod]
        public async Task Should_Delete_Zone()
        {           
            await UsingDbContextAsync(async context =>
            {
                context.Zones.FirstOrDefaultAsync(e => e.Id == _zoneId).ShouldNotBeNull();
                await _zoneService.Delete(_zoneId);
            });
        }

        [TestMethod]
        public async Task Should_Return_Zone_List()
        {
            var zones = await _zoneService.GetAllAsync();

            zones.Count.ShouldBeGreaterThan(0);
        }

        [TestMethod]
        public async Task Should_Return_Zone_Detail()
        {
            await UsingDbContextAsync(async context =>
            {
                var zone = await context.Zones.FirstOrDefaultAsync(e => e.Name == _newZone.ZoneDto.Name);

                 _zoneService.GetById(zone.Id).ShouldNotBeNull();
            });
        }
    }
}