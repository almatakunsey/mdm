﻿using Shouldly;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Locations;
using Vtmis.WebAdmin.Locations.Dto;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services.Dto;
using Abp.ObjectMapping;
using Abp.UI;

namespace Vtmis.WebAdmin.Application.Tests.Locations
{
    [TestClass]
    public class LocationAppService_Test : TestAppTestBase
    {
        private readonly ILocationAppService _locationAppService;
        private readonly ILookupNormalizer _lookupNormalizer;
        private readonly IObjectMapper _objectMapper;
        private readonly Guid _locationId;
        public LocationAppService_Test()
        {
            LoginAsDefaultTenantAdmin();
            _objectMapper = Resolve<IObjectMapper>();
            _locationAppService = Resolve<ILocationAppService>();
            _lookupNormalizer = LocalIocManager.Resolve<ILookupNormalizer>();
            _locationId = new Guid("00713486-994A-40A4-BFFF-92876B64D3E8"); // Temp solution b4 services can be mock
           
        }

        [TestMethod]
        public async Task Should_Create_Location()
        {
            // Arrange
            var locationName = Guid.NewGuid().ToString();
                        
            // Act
            await _locationAppService.CreateAsync(new CreateLocationDto
            {
                Name = locationName,
                Latitude = "12345",
                Longitude = "67890"
            });

            // Assert
            await UsingDbContextAsync(async context =>
            {
                (await context.Locations.FirstOrDefaultAsync(e => e.Name == locationName)).ShouldNotBe(null);
            });
        }

        [TestMethod]
        public async Task Should_Return_Location_List()
        {
            var locations = await _locationAppService.GetListAsync();

            locations.Items.Count.ShouldBeGreaterThan(0);
        }

        [TestMethod]
        public async Task Should_Return_Location_Detail()
        {
            await UsingDbContextAsync(async context =>
            {
                var location = await context.Locations.FirstOrDefaultAsync(e => e.Name == "Location1");

                (await _locationAppService.GetDetailAsync(new EntityDto<Guid> { Id = location.Id })).ShouldNotBeNull();
            });
        }

        [TestMethod]
        public async Task Should_Delete_Location()
        {
            //var locationId = new Guid("A8B026D7-5B92-4C85-94DA-9075A5C2E2D5"); // Temp solution b4 services can be mock
            await UsingDbContextAsync(async context =>
            {               
                context.Locations.FirstOrDefaultAsync(e => e.Id == _locationId).ShouldNotBeNull();
                await _locationAppService.DeleteAsync(_locationId);
            });
           
        }

        [TestMethod]
        public async Task Should_Throw_Exception_When_Delete_Location()
        {
            
            await UsingDbContextAsync(async context =>
            {
                var result = await context.Locations.FirstOrDefaultAsync(e => e.Id == _locationId);
                result.ShouldBe(null);
            });
            await _locationAppService.DeleteAsync(_locationId)
                .ShouldThrowAsync<UserFriendlyException>("Could not found the location, maybe it's deleted.");
        }

        [TestMethod]
        public async Task Shoud_Update_Location()
        {
            var input = new UpdateLocationDto
            {
                Id = new Guid("144ED065-BB86-4137-AAB0-AE475ABB90CD"),
                Name = "New Updated Location",
                Latitude = "12345",
                Longitude = "67890"
            };            

            await UsingDbContextAsync(async context =>
            {
                var location = await context.Locations.FirstOrDefaultAsync(e => e.Id == input.Id);
                await _locationAppService.UpdateAsync(input);
                var updatedLocation = await context.Locations.FirstOrDefaultAsync(e => e.Id == input.Id);
                updatedLocation.Name.ShouldBeSameAs(input.Name);
            });
        }

        [TestMethod]
        public async Task Shoud_Not_Update_Location_When_Location_IsNull()
        {
            var input = new UpdateLocationDto
            {
                Id = Guid.NewGuid(),
                Name = "New Updated Location",
                Latitude = "12345",
                Longitude = "67890"
            };

            await UsingDbContextAsync(async context =>
            {
                var location = await context.Locations.FirstOrDefaultAsync(e => e.Id == input.Id);
                location.ShouldBe(null);
                await _locationAppService.UpdateAsync(input)
                    .ShouldThrowAsync<UserFriendlyException>("Could not found the location, maybe it's deleted.");
            });

        }
    }
}
