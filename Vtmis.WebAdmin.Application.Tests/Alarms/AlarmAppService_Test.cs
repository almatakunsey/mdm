﻿//using Abp.ObjectMapping;
//using Abp.UI;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Shouldly;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Vtmis.WebAdmin.Alarms;
//using Vtmis.WebAdmin.Alarms.Dto;

//namespace Vtmis.WebAdmin.Application.Tests.Alarms
//{
//    [TestClass]
//    public class AlarmAppService_Test : TestAppTestBase
//    {
//        private readonly IAlarmAppService _alarmAppService;
//        private readonly IObjectMapper _objectMapper;
//        private readonly int _alarmId;
//        public AlarmAppService_Test()
//        {
//            LoginAsDefaultTenantAdmin();
//            _objectMapper = Resolve<IObjectMapper>();
//            _alarmAppService = Resolve<IAlarmAppService>();
//            _alarmId = 3;
//        }

//        [TestMethod]
//        public async Task Should_Return_Alarm_List()
//        {
//            var alarms = await _alarmAppService.GetAllAsync();

//            alarms.Count().ShouldBeGreaterThan(0);
//        }

//        [TestMethod]
//        public async Task Should_Return_Alarm_Detail()
//        {
//            await UsingDbContextAsync(async context =>
//            {
//                var alarm = await context.Alarms.FirstOrDefaultAsync(e => e.Name == "Alarm1");

//                (await _alarmAppService.GetByIdAsync(alarm.Id)).ShouldNotBe(null);
//            });
//        }

//        [TestMethod]
//        public async Task Should_Create_Alarm()
//        {
//            // Arrange
//            var alarmName = "Alarm1";

//            // Act
//            await _alarmAppService.CreateAsync(new AlarmDto
//            {
//                Name = alarmName,
//                AISMessage = "AIS Msg",
//                Color = "Red",
//                EmailAddress = "user@email.com",
//                IsEnabled = true,
//                IsSendAISSafetyMessage = true,
//                IsSendEmail = true,
//                Message = "Test Msg",
//                Priority = AlarmPriority.High,
//                RepeatInterval = 1,
//                TenantId = 1,
//                Conditions = new List<AlarmConditionDto>{
//                    new AlarmConditionDto
//                    {
//                        TenantId = 1,
//                        Max = 10,
//                        Min = 5
//                    }
//                }
//            });

//            // Assert
//            await UsingDbContextAsync(async context =>
//            {
//                (await context.Alarms.FirstOrDefaultAsync(e => e.Name == alarmName)).ShouldNotBe(null);
//            });
//        }

//        [TestMethod]
//        public async Task Should_Delete_Alarm()
//        {
            
//            await UsingDbContextAsync(async context =>
//            {
//                context.Alarms.FirstOrDefaultAsync(e => e.Id == _alarmId).ShouldNotBeNull();
//                await _alarmAppService.DeleteAsync(_alarmId);
//            });

//        }

//        [TestMethod]
//        public async Task Should_Throw_Exception_When_Delete_Alarm()
//        {

//            await UsingDbContextAsync(async context =>
//            {
//                var result = await context.Alarms.FirstOrDefaultAsync(e => e.Id == _alarmId);
//                result.ShouldBe(null);
//            });
//            await _alarmAppService.DeleteAsync(_alarmId)
//                .ShouldThrowAsync<UserFriendlyException>("Could not found the alarm, maybe it's deleted.");
//        }

//        [TestMethod]
//        public async Task Shoud_Update_Alarm()
//        {
//            var input = new AlarmDto
//            {
//                Id = _alarmId,
//                Name = "New Updated Alarm",
//                AISMessage = "test ais message",
//                Color = "red",
//                IsEnabled = true,
//                IsSendAISSafetyMessage = true,
//                IsSendEmail = true,
//                Conditions = new List<AlarmConditionDto> {
//                    new AlarmConditionDto
//                    {
//                        Min = 10,
//                        Max = 20
//                    }
//                },
//                EmailAddress = "user@gmail.com",
//                Message = "test msg",
//                Priority = AlarmPriority.High,
//                RepeatInterval = 1,
//                TenantId = 1                
//            };

//            await UsingDbContextAsync(async context =>
//            {
//                var alarm = await context.Alarms.FirstOrDefaultAsync(e => e.Id == input.Id);
//                await _alarmAppService.UpdateAsync(input);
//                var updatedAlarm = await context.Alarms.FirstOrDefaultAsync(e => e.Id == input.Id);
//                updatedAlarm.Name.ShouldBeSameAs(input.Name);
//            });
//        }

//        [TestMethod]
//        public async Task Shoud_Not_Update_Location_When_Location_IsNull()
//        {
//            var input = new AlarmDto
//            {
//                Id = 10
//            };

//            await UsingDbContextAsync(async context =>
//            {
//                var alarm = await context.Alarms.FirstOrDefaultAsync(e => e.Id == input.Id);
//                alarm.ShouldBe(null);
//                await _alarmAppService.UpdateAsync(input)
//                    .ShouldThrowAsync<UserFriendlyException>("Could not found the alarm, maybe it's deleted.");
//            });

//        }
//    }
//}
