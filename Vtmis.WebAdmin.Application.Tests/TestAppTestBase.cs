using System;
using System.Linq;
using System.Threading.Tasks;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore;
using Abp.TestBase;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Vtmis.WebAdmin.Application.Tests.TestDatas;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using Vtmis.WebAdmin.Authorization.Users;
using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using Vtmis.WebAdmin.MultiTenancy;
//using Abp.Zero.SampleApp.EntityFrameworkCore.TestDataBuilders.HostDatas;

namespace Vtmis.WebAdmin.Application.Tests
{
    public class TestAppTestBase : AbpIntegratedTestBase<TestAppModule>
    {
       
        public TestAppTestBase()
        {
            UsingDbContext(context => new HostDataBuilder(context).Build());
        }

        protected virtual void UsingDbContext(Action<WebAdminDbContext> action)
        {
            using (var uow = Resolve<IUnitOfWorkManager>().Begin())
            {
                using (var contextProvider = LocalIocManager.ResolveAsDisposable<IDbContextProvider<WebAdminDbContext>>())
                {
                    var dbContext = contextProvider.Object.GetDbContext();

                    action(dbContext);
                    dbContext.SaveChanges(true);
                }

                uow.Complete();
            }
        }

        protected virtual T UsingDbContext<T>(Func<WebAdminDbContext, T> func)
        {
            T result;

            using (var uow = LocalIocManager.Resolve<IUnitOfWorkManager>().Begin())
            {
                using (var contextProvider = LocalIocManager.ResolveAsDisposable<IDbContextProvider<WebAdminDbContext>>())
                {
                    var dbContext = contextProvider.Object.GetDbContext();

                    result = func(dbContext);
                    dbContext.SaveChanges(true);
                }

                uow.Complete();
            }

            return result;
        }

        protected virtual async Task UsingDbContextAsync(Func<WebAdminDbContext, Task> action)
        {
            using (var uow = LocalIocManager.Resolve<IUnitOfWorkManager>().Begin())
            {
                using (var contextProvider = LocalIocManager.ResolveAsDisposable<IDbContextProvider<WebAdminDbContext>>())
                {
                    var dbContext = contextProvider.Object.GetDbContext();                    

                    await action(dbContext);
                    await dbContext.SaveChangesAsync(true);
                }

                await uow.CompleteAsync();
            }
        }

        protected virtual async Task<T> UsingDbContextAsync<T>(Func<WebAdminDbContext, Task<T>> func)
        {
            T result;

            using (var uow = LocalIocManager.Resolve<IUnitOfWorkManager>().Begin())
            {
                using (var contextProvider = LocalIocManager.ResolveAsDisposable<IDbContextProvider<WebAdminDbContext>>())
                {
                    var dbContext = contextProvider.Object.GetDbContext();

                    result = await func(dbContext);
                    await dbContext.SaveChangesAsync(true);
                }

                await uow.CompleteAsync();
            }

            return result;
        }

        #region Login

        protected void LoginAsHostAdmin()
        {
            LoginAsHost(AbpUserBase.AdminUserName);
        }

        protected void LoginAsDefaultTenantAdmin()
        {
            LoginAsTenant(AbpTenantBase.DefaultTenantName, AbpUserBase.AdminUserName);
        }

        protected void LoginAsHost(string userName)
        {
            AbpSession.TenantId = null;

            var user =
                UsingDbContext(
                    context =>
                        context.Users.FirstOrDefault(u => u.TenantId == AbpSession.TenantId && u.UserName == userName));
            if (user == null)
            {
                throw new Exception("There is no user: " + userName + " for host.");
            }

            AbpSession.UserId = user.Id;
        }

        protected void LoginAsTenant(string tenancyName, string userName)
        {
            var tenant = UsingDbContext(context => context.Tenants.FirstOrDefault(t => t.TenancyName == tenancyName));
            if (tenant == null)
            {
                throw new Exception("There is no tenant: " + tenancyName);
            }

            AbpSession.TenantId = tenant.Id;

            var user =
                UsingDbContext(
                    context =>
                        context.Users.FirstOrDefault(u => u.TenantId == AbpSession.TenantId && u.UserName == userName));
            if (user == null)
            {
                throw new Exception("There is no user: " + userName + " for tenant: " + tenancyName);
            }

            AbpSession.UserId = user.Id;
        }

        #endregion

        /// <summary>
        /// Gets current user if <see cref="IAbpSession.UserId"/> is not null.
        /// Throws exception if it's null.
        /// </summary>
        protected async Task<User> GetCurrentUserAsync()
        {
            var userId = AbpSession.GetUserId();
            return await UsingDbContext(context => context.Users.SingleAsync(u => u.Id == userId));
        }

        /// <summary>
        /// Gets current tenant if <see cref="IAbpSession.TenantId"/> is not null.
        /// Throws exception if there is no current tenant.
        /// </summary>
        protected async Task<Tenant> GetCurrentTenantAsync()
        {
            var tenantId = AbpSession.GetTenantId();
            return await UsingDbContext(context => context.Tenants.SingleAsync(t => t.Id == tenantId));
        }
    }
}