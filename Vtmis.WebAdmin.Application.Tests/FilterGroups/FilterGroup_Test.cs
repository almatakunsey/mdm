﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vtmis.WebAdmin.Authorization.Users;
using System.Linq;
using Vtmis.WebAdmin.EntityFrameworkCore;
using System.Threading.Tasks;
using Shouldly;
using Vtmis.WebAdmin.MultiTenancy;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Vtmis.WebAdmin.Filters;

namespace Vtmis.WebAdmin.Application.Tests.Users
{
    [TestClass]
    public class FilterGroup_Test : TestAppTestBase
    {
        //private readonly IUserAppService _userAppService;
        //private readonly RoleManager _roleManager;
        //private readonly ILookupNormalizer _lookupNormalizer;
        private readonly FilterGroupService _filterGroupServices;
        public FilterGroup_Test()
        {
           
            //_lookupNormalizer = LocalIocManager.Resolve<ILookupNormalizer>();
            //_roleManager = Resolve<RoleManager>();
            //LocalIocManager.Resolve<UserManager>();
            //LocalIocManager.Resolve<UserRegistrationManager>();
            //_userAppService = LocalIocManager.Resolve<IUserAppService>();
            _filterGroupServices = LocalIocManager.Resolve<FilterGroupService>();
        }

      

        [TestMethod]
        public async Task Seed_Data_Test()
        {
            await UsingDbContextAsync(async context =>
            {
                (await context.Tenants.CountAsync()).ShouldBeGreaterThan(1);
            });
        }

        

        [TestMethod]
        public async Task Should_return_filter_group_list()
        {
            var list = await _filterGroupServices.GetAll();

            list.Count.ShouldBe(6);
        }
    }

    public class UserLoginHelper
    {
        public static void CreateTestUsers(WebAdminDbContext context)
        {
            var defaultTenant = context.Tenants.Single(t => t.TenancyName == Tenant.DefaultTenantName);

            context.Users.Add(
                new User
                {
                    UserName = "userOwner",
                    Name = "Owner",
                    Surname = "One",
                    EmailAddress = "owner@aspnetboilerplate.com",
                    IsEmailConfirmed = true,
                    Password = "AM4OLBpptxBYmM79lGOX9egzZk3vIQU3d/gFCJzaBjAPXzYIK3tQ2N7X4fcrHtElTw==" //123qwe
                });

            context.Users.Add(
                new User
                {
                    TenantId = defaultTenant.Id, //A user of tenant1
                    UserName = "user1",
                    Name = "User",
                    Surname = "One",
                    EmailAddress = "user-one@aspnetboilerplate.com",
                    IsEmailConfirmed = false,
                    Password = "AM4OLBpptxBYmM79lGOX9egzZk3vIQU3d/gFCJzaBjAPXzYIK3tQ2N7X4fcrHtElTw==" //123qwe
                });
        }
    }
}
