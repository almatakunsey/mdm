﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Threading.Tasks;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto;
using Vtmis.WebAdmin.Vts;
using Vtmis.WebVessel.Tracking.Services.Concrete;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using Xunit;

namespace Vtmis.MMDIS.Tests.Services
{
    public class VtsVesselService_Test : VtsInMemoryProvider
    {
        private IVtsVesselService _vtsVesselService;
        private readonly IMapper _mapper;
        private readonly Mock<IExtendedShipService> _extendedShipService;
        public VtsVesselService_Test()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CreateExtendedShipList, ExtendedShipList>().ReverseMap();
                cfg.CreateMap<UpdateExtendedShipList, ExtendedShipList>()
                  .ForMember(x => x.Id, op => op.Ignore());
                cfg.CreateMap<ExtendedShipList, ResponseExtendedShipList>();
            });
            var mapper = config.CreateMapper();
            _mapper = mapper;
            _extendedShipService = new Mock<IExtendedShipService>();
        }

        private void Init()
        {
            var context = Context;
            _vtsVesselService = new VtsVesselService(context, _extendedShipService.Object, _mapper);
            InternalContext = context;
        }

        private async Task CreateVessel()
        {
            await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList
            {
                AnnualTonnageDue = 1,
                Beam = 2,
                Breadth = 3,
                BuilderAddress1 = "Address 1",
                BuilderAddress2 = "Address 2",
                BuilderAddress3 = "Address 3",
                BuilderCountryCode = "MY",
                BuilderName = "Builder Name",
                BuilderPostCode = "A1",
                CallSign = "CS1",
                Class = "Class A",
                CountryFlag = "Malaysia",
                DateOfBuildingContract = DateTime.Now,
                DateOfDelivery = DateTime.Now,
                DateOfKeelLaid = DateTime.Now,
                DeadWeight = 4,
                Draught = 5,
                EngineDescription = "Engine Desc",
                GrossTonnage = 6,
                HullMaterial = "Hull Material",
                IMO = "IMO",
                IsCharter = true,
                Length = 7,
                LocalityStatus = "L Status",
                MMSI = 1111,
                NetTonnage = 8,
                NoOfDeck = 9,
                NoOfHull = 10,
                NoOfPassenger = 11,
                NumberOfCabin = 12,
                NumberOfEngine = 13,
                OfficialNo = "OfficalNo1",
                PlaceOfBuilt = "Place built",
                PlyingLimit = 14,
                PortOfRegistry = "PortOfRegistry",
                Propel = 16,
                PropulsionType = "propulsion type",
                Speed = 17,
                Status = 18,
                TotalEnginePower = 19,
                TotalNumberOfCrew = 20,
                TradingArea = "TradingArea",
                TypeOfRegistry = "type of reg",
                Value = 22,
                ValueCurrency = "MYR",
                VesselCategory = 23,
                VesselName = "Vessel A",
                VesselType = "Ferry",
                YardNumber = 25
            });
        }

        [Theory]
        [InlineData(1, 0, null, null)]
        [InlineData(1, 0, "", "")]
        [InlineData(1, 0, " ", " ")]
        [InlineData(0, 1111, "IMO", "CS1")]
        [InlineData(1, 1111, "IMO", "CS1")]
        public async Task Should_Delete_Vessel(int id, int mmsi, string imo, string callSign)
        {
            Init();
            await CreateVessel();
            await _vtsVesselService.SoftDeleteVesselAsync(id, mmsi, imo, callSign);

            var result = await InternalContext.ExtendedShipLists.FirstOrDefaultAsync(x => x.Id == 1);
            Assert.True(result.IsDeleted);
        }

        [Fact]
        public async Task Should_Throw_NotFoundException_When_Delete_Vessel()
        {
            Init();
            await Assert.ThrowsAsync<NotFoundException>(() => _vtsVesselService
               .SoftDeleteVesselAsync(1, 0, null, null));
        }

        [Fact]
        public async Task Should_Return_All_Vessels_Without_DeletedVessel()
        {
            Init();
            await CreateVessel();
            await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList { MMSI = 222 });
            await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList { MMSI = 333 });
            await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList { MMSI = 444 });
            await _vtsVesselService.SoftDeleteVesselAsync(4, 0, null, null);
            var results = _vtsVesselService.GetAllShips();
            Assert.Equal(3, results.TotalItems);
        }

        [Fact]
        public async Task Should_Return_All_Vessels_With_DeletedVessel()
        {
            Init();
            await CreateVessel();
            await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList { MMSI = 222 });
            await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList { MMSI = 333 });
            await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList { MMSI = 444 });
            await _vtsVesselService.SoftDeleteVesselAsync(4, 0, null, null);
            var results = _vtsVesselService.GetAllShips(includeDeletedShips: true);
            Assert.Equal(4, results.TotalItems);
        }

        [Theory]
        [InlineData(1, 0, null, null)]
        [InlineData(1, 0, "", "")]
        [InlineData(1, 0, " ", " ")]
        [InlineData(0, 1111, "IMO", "CS1")]
        [InlineData(1, 1111, "IMO", "CS1")]
        public async Task Should_Return_Ship(int id, int mmsi, string imo, string callSign)
        {
            Init();
            await CreateVessel();
            var result = await _vtsVesselService.GetShip(id, mmsi, imo, callSign);
            Assert.NotNull(result);
        }

        [Theory]
        [InlineData(1, 0, null, null)]
        [InlineData(1, 0, "", "")]
        [InlineData(1, 0, " ", " ")]
        [InlineData(0, 1111, "IMO", "CS1")]
        [InlineData(1, 1111, "IMO", "CS1")]
        public async Task Should_Throw_NotFoundException_When_GetShip(int id, int mmsi, string imo,
            string callSign)
        {
            Init();
            await Assert.ThrowsAsync<NotFoundException>(() => _vtsVesselService
                .GetShip(id, mmsi, imo, callSign));
        }

        [Fact]
        public async Task Should_Create_Vessel()
        {
            Init();
            var input = new CreateExtendedShipList
            {
                AnnualTonnageDue = 1,
                Beam = 2,
                Breadth = 3,
                BuilderAddress1 = "Address 1",
                BuilderAddress2 = "Address 2",
                BuilderAddress3 = "Address 3",
                BuilderCountryCode = "MY",
                BuilderName = "Builder Name",
                BuilderPostCode = "A1",
                CallSign = "CS1",
                Class = "Class A",
                CountryFlag = "Malaysia",
                DateOfBuildingContract = DateTime.Now,
                DateOfDelivery = DateTime.Now,
                DateOfKeelLaid = DateTime.Now,
                DeadWeight = 4,
                Draught = 5,
                EngineDescription = "Engine Desc",
                GrossTonnage = 6,
                HullMaterial = "Hull Material",
                IMO = "IMO",
                IsCharter = true,
                Length = 7,
                LocalityStatus = "L Status",
                MMSI = 1111,
                NetTonnage = 8,
                NoOfDeck = 9,
                NoOfHull = 10,
                NoOfPassenger = 11,
                NumberOfCabin = 12,
                NumberOfEngine = 13,
                OfficialNo = "OfficalNo1",
                PlaceOfBuilt = "Place built",
                PlyingLimit = 14,
                PortOfRegistry = "PortOfRegistry",
                Propel = 16,
                PropulsionType = "propulsion type",
                Speed = 17,
                Status = 18,
                TotalEnginePower = 19,
                TotalNumberOfCrew = 20,
                TradingArea = "TradingArea",
                TypeOfRegistry = "type of reg",
                Value = 22,
                ValueCurrency = "MYR",
                VesselCategory = 23,
                VesselName = "Vessel A",
                VesselType = "Ferry",
                YardNumber = 25
            };

            var result = await _vtsVesselService.CreateVesselAsync(input);
            Assert.Equal(1, result.Id);

            foreach (var pi in result.GetType().GetProperties())
            {
                if ((pi.PropertyType != typeof(string)) && (pi.PropertyType != typeof(DateTime?)) &&
                    (pi.PropertyType != typeof(int)) && (pi.PropertyType != typeof(double)) &&
                    (pi.PropertyType != typeof(bool)))
                {
                    Assert.True(false);
                }
                if (pi.PropertyType == typeof(string))
                {
                    var value = (string)pi.GetValue(result);
                    Assert.NotNull(value);
                }
                if (pi.PropertyType == typeof(DateTime?))
                {
                    var value = (DateTime?)pi.GetValue(result);
                    Assert.NotNull(value);
                }
                if (pi.PropertyType == typeof(int))
                {
                    var value = (int)pi.GetValue(result);
                    Assert.True(value > 0);
                }

                if (pi.PropertyType == typeof(double))
                {
                    var value = (double)pi.GetValue(result);
                    Assert.True(value > 0);
                }
                //if (pi.PropertyType == typeof(bool))
                //{
                //    var value = (bool)pi.GetValue(result);
                   
                //    if (nameof(result.IsBlackList) == "IsBlackList")
                //    {
                //        Assert.False(value);
                //    }
                //    else
                //    {
                //        Assert.True(value);
                //    }                    
                   
                //}
            }

        }

        [Fact]
        public async Task Should_Throw_Already_Exist_Exception_When_Create_Vessel()
        {
            Init();
            await CreateVessel();            
            var input = new CreateExtendedShipList
            {
                AnnualTonnageDue = 1,
                Beam = 2,
                Breadth = 3,
                BuilderAddress1 = "Address 1",
                BuilderAddress2 = "Address 2",
                BuilderAddress3 = "Address 3",
                BuilderCountryCode = "MY",
                BuilderName = "Builder Name",
                BuilderPostCode = "A1",
                CallSign = "CS1",
                Class = "Class A",
                CountryFlag = "Malaysia",
                DateOfBuildingContract = DateTime.Now,
                DateOfDelivery = DateTime.Now,
                DateOfKeelLaid = DateTime.Now,
                DeadWeight = 4,
                Draught = 5,
                EngineDescription = "Engine Desc",
                GrossTonnage = 6,
                HullMaterial = "Hull Material",
                IMO = "IMO",
                IsCharter = true,
                Length = 7,
                LocalityStatus = "L Status",
                MMSI = 1111,
                NetTonnage = 8,
                NoOfDeck = 9,
                NoOfHull = 10,
                NoOfPassenger = 11,
                NumberOfCabin = 12,
                NumberOfEngine = 13,
                OfficialNo = "OfficalNo1",
                PlaceOfBuilt = "Place built",
                PlyingLimit = 14,
                PortOfRegistry = "PortOfRegistry",
                Propel = 16,
                PropulsionType = "propulsion type",
                Speed = 17,
                Status = 18,
                TotalEnginePower = 19,
                TotalNumberOfCrew = 20,
                TradingArea = "TradingArea",
                TypeOfRegistry = "type of reg",
                Value = 22,
                ValueCurrency = "MYR",
                VesselCategory = 23,
                VesselName = "Vessel A",
                VesselType = "Ferry",
                YardNumber = 25
            };
            await Assert.ThrowsAsync<AlreadyExistException>(() => _vtsVesselService
                .CreateVesselAsync(input));
        }

        [Fact]
        public async Task Should_Update_Vessel()
        {
            Init();
            await CreateVessel();
            var input = new UpdateExtendedShipList
            {
                Id = 0,
                AnnualTonnageDue = 1,
                Beam = 2,
                Breadth = 3,
                BuilderAddress1 = "Address 1",
                BuilderAddress2 = "Address 2",
                BuilderAddress3 = "Address 3",
                BuilderCountryCode = "MY",
                BuilderName = "Builder Name",
                BuilderPostCode = "A1",
                CallSign = "CS1",
                Class = "Class A",
                CountryFlag = "Malaysia",
                DateOfBuildingContract = DateTime.Now,
                DateOfDelivery = DateTime.Now,
                DateOfKeelLaid = DateTime.Now,
                DeadWeight = 4,
                Draught = 5,
                EngineDescription = "Engine Desc",
                GrossTonnage = 6,
                HullMaterial = "Hull Material",
                IMO = "IMO",
                IsCharter = true,
                Length = 7,
                LocalityStatus = "L Status",
                MMSI = 1111,
                NetTonnage = 8,
                NoOfDeck = 9,
                NoOfHull = 10,
                NoOfPassenger = 11,
                NumberOfCabin = 12,
                NumberOfEngine = 13,
                OfficialNo = "OfficalNo1",
                PlaceOfBuilt = "Place built",
                PlyingLimit = 14,
                PortOfRegistry = "PortOfRegistry",
                Propel = 16,
                PropulsionType = "propulsion type",
                Speed = 17,
                Status = 18,
                TotalEnginePower = 19,
                TotalNumberOfCrew = 20,
                TradingArea = "TradingArea",
                TypeOfRegistry = "type of reg",
                Value = 22,
                ValueCurrency = "MYR",
                VesselCategory = 23,
                VesselName = "Updated Vessel A",
                VesselType = "Ferry",
                YardNumber = 25
            };

            var result = await _vtsVesselService.UpdateVesselAsync(input);
            Assert.NotNull(result);
            Assert.IsType<ResponseExtendedShipList>(result);
            Assert.Same("Updated Vessel A", result.VesselName);
        }

        //[Fact]
        //public async Task Should_Throw_Already_Exist_Exception_When_Update_Vessel()
        //{
        //    Init();
        //    await CreateVessel();
        //    await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList
        //    {
        //        AnnualTonnageDue = 1,
        //        Beam = 2,
        //        Breadth = 3,
        //        BuilderAddress1 = "Address 1",
        //        BuilderAddress2 = "Address 2",
        //        BuilderAddress3 = "Address 3",
        //        BuilderCountryCode = "MY",
        //        BuilderName = "Builder Name",
        //        BuilderPostCode = "A1",
        //        CallSign = "CS1 A",
        //        Class = "Class A",
        //        CountryFlag = "Malaysia",
        //        DateOfBuildingContract = DateTime.Now,
        //        DateOfDelivery = DateTime.Now,
        //        DateOfKeelLaid = DateTime.Now,
        //        DeadWeight = 4,
        //        Draught = 5,
        //        EngineDescription = "Engine Desc",
        //        GrossTonnage = 6,
        //        HullMaterial = "Hull Material",
        //        IMO = "IMO A",
        //        IsCharter = true,
        //        Length = 7,
        //        LocalityStatus = "L Status",
        //        MMSI = 11112,
        //        NetTonnage = 8,
        //        NoOfDeck = 9,
        //        NoOfHull = 10,
        //        NoOfPassenger = 11,
        //        NumberOfCabin = 12,
        //        NumberOfEngine = 13,
        //        OfficialNo = "OfficalNo1",
        //        PlaceOfBuilt = "Place built",
        //        PlyingLimit = 14,
        //        PortOfRegistry = "PortOfRegistry",
        //        Propel = 16,
        //        PropulsionType = "propulsion type",
        //        Speed = 17,
        //        Status = 18,
        //        TotalEnginePower = 19,
        //        TotalNumberOfCrew = 20,
        //        TradingArea = "TradingArea",
        //        TypeOfRegistry = "type of reg",
        //        Value = 22,
        //        ValueCurrency = "MYR",
        //        VesselCategory = 23,
        //        VesselName = "Vessel B",
        //        VesselType = "Ferry",
        //        YardNumber = 25
        //    });
        //    var input = new UpdateExtendedShipList
        //    {
        //        Id = 1,
        //        AnnualTonnageDue = 1,
        //        Beam = 2,
        //        Breadth = 3,
        //        BuilderAddress1 = "Address 1",
        //        BuilderAddress2 = "Address 2",
        //        BuilderAddress3 = "Address 3",
        //        BuilderCountryCode = "MY",
        //        BuilderName = "Builder Name",
        //        BuilderPostCode = "A1",
        //        CallSign = "CS1",
        //        Class = "Class A",
        //        CountryFlag = "Malaysia",
        //        DateOfBuildingContract = DateTime.Now,
        //        DateOfDelivery = DateTime.Now,
        //        DateOfKeelLaid = DateTime.Now,
        //        DeadWeight = 4,
        //        Draught = 5,
        //        EngineDescription = "Engine Desc",
        //        GrossTonnage = 6,
        //        HullMaterial = "Hull Material",
        //        IMO = "IMO",
        //        IsCharter = true,
        //        Length = 7,
        //        LocalityStatus = "L Status",
        //        MMSI = 11112,
        //        NetTonnage = 8,
        //        NoOfDeck = 9,
        //        NoOfHull = 10,
        //        NoOfPassenger = 11,
        //        NumberOfCabin = 12,
        //        NumberOfEngine = 13,
        //        OfficialNo = "OfficalNo1",
        //        PlaceOfBuilt = "Place built",
        //        PlyingLimit = 14,
        //        PortOfRegistry = "PortOfRegistry",
        //        Propel = 16,
        //        PropulsionType = "propulsion type",
        //        Speed = 17,
        //        Status = 18,
        //        TotalEnginePower = 19,
        //        TotalNumberOfCrew = 20,
        //        TradingArea = "TradingArea",
        //        TypeOfRegistry = "type of reg",
        //        Value = 22,
        //        ValueCurrency = "MYR",
        //        VesselCategory = 23,
        //        VesselName = "Updated Vessel A",
        //        VesselType = "Ferry",
        //        YardNumber = 25
        //    };
        //    await Assert.ThrowsAsync<AlreadyExistException>(() => _vtsVesselService
        //       .UpdateVesselAsync(input));
        //}

        [Fact]
        public async Task Should_Throw_NotFound_Exception_When_Update_Vessel()
        {
            Init();
            await Assert.ThrowsAsync<NotFoundException>(() => _vtsVesselService
                .UpdateVesselAsync(new UpdateExtendedShipList { Id = 999 }));
        }

        [Theory]
        [InlineData(1, 0, null, null)]
        [InlineData(1, 0, "", "")]
        [InlineData(1, 0, " ", " ")]     
        [InlineData(0, 1111, "IMO", "CS1")]
        [InlineData(1, 1111, "IMO", "CS1")]
        public async Task Should_Blacklist_Vessel(int id, int mmsi, string imo, string callSign)
        {
            Init();
            await CreateVessel();
            await _vtsVesselService.BlackListVesselAsync(id, mmsi, imo, callSign);

            var result = await InternalContext.ExtendedShipLists.FirstOrDefaultAsync(x => x.Id == 1);
            Assert.True(result.IsBlackList);
        }

        [Theory]       
        [InlineData(0, 1111, null, null)]
        [InlineData(0, 0, "IMO", null)]
        [InlineData(0, 0, "IMO", "CS1")]
        public async Task Should_Throw_NotFound_Exception_When_Blacklist_Vessel(int id, int mmsi, string imo, string callSign)
        {
            Init();
            await Assert.ThrowsAsync<NotFoundException>(() => _vtsVesselService
                .BlackListVesselAsync(id, mmsi, imo, callSign));
        }
    }
}
