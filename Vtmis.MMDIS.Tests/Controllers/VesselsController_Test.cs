﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Net;
using System.Threading.Tasks;
using Vtmis.Api.MMDIS.Controllers;
using Vtmis.Core.Model;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto;
using Vtmis.WebAdmin.Vts;
using Vtmis.WebVessel.Tracking.Services.Concrete;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using Xunit;

namespace Vtmis.MMDIS.Tests.Controllers
{
    public class VesselsController_Test : VtsInMemoryProvider
    {
        private IVtsVesselService _vtsVesselService;
        private VesselsController _vesselsController;
        private readonly IMapper _mapper;
        private readonly Mock<IExtendedShipService> _extendedShipService;
        public VesselsController_Test()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CreateExtendedShipList, ExtendedShipList>().ReverseMap();
                cfg.CreateMap<UpdateExtendedShipList, ExtendedShipList>();
                cfg.CreateMap<ExtendedShipList, ResponseExtendedShipList>();
            });
            var mapper = config.CreateMapper();
            _mapper = mapper;
            _extendedShipService = new Mock<IExtendedShipService>();
        }

        private void Init()
        {
            var context = Context;
            _vtsVesselService = new VtsVesselService(context, _extendedShipService.Object, _mapper);
            _vesselsController = new VesselsController(_vtsVesselService);
            InternalContext = context;
        }

        [Theory]
        [InlineData(1, 0, null, null)]
        [InlineData(1, 0, "", "")]
        [InlineData(1, 0, " ", " ")]
        [InlineData(0, 1111, "IMO", "CS1")]
        [InlineData(1, 1111, "IMO", "CS1")]
        public async Task Should_Return_BadRequest_When_NotFound_DeleteShip(int id, int mmsi, string imo, string callSign)
        {
            Init();
            var controllerResult = await _vesselsController.Delete(new Core.VesselEntityFrameworkCore.Models.Mmdis.Dto.IdMmsiImoCallSign
            {
                CallSign = callSign,
                Imo = imo,
                Mmsi = mmsi,
                TargetId = id
            });

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        [Theory]
        [InlineData(1, 0, null, null)]
        [InlineData(1, 0, "", "")]
        [InlineData(1, 0, " ", " ")]
        [InlineData(0, 1111, "IMO", "CS1")]
        [InlineData(1, 1111, "IMO", "CS1")]
        public async Task Should_Delete_Vessel(int id, int mmsi, string imo, string callSign)
        {
            Init();
            await CreateVessel();
            var controllerResult = await _vesselsController.Delete(new Core.VesselEntityFrameworkCore.Models.Mmdis.Dto.IdMmsiImoCallSign
            {
                CallSign = callSign,
                Imo = imo,
                Mmsi = mmsi,
                TargetId = id
            });

            var okResult = Assert.IsType<OkResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);
        }

        [Fact]
        public async Task Should_Return_BadRequest_If_InvalidParamater_Supplied_When_GetShip()
        {
            Init();
            var controllerResult = await _vesselsController.GetShip(new Core.VesselEntityFrameworkCore.Models.Mmdis.Dto.IdMmsiImoCallSign
            {
                CallSign = null,
                Imo = null,
                Mmsi = 0,
                TargetId = 0
            });

            var okResult = Assert.IsType<BadRequestResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        [Theory]
        [InlineData(1, 0, null, null)]
        [InlineData(1, 0, "", "")]
        [InlineData(1, 0, " ", " ")]
        [InlineData(0, 1111, "IMO", "CS1")]
        [InlineData(1, 1111, "IMO", "CS1")]
        public async Task Should_Return_Ship(int id, int mmsi, string imo, string callSign)
        {
            Init();
            await CreateVessel();
            var controllerResult = await _vesselsController.GetShip(new Core.VesselEntityFrameworkCore.Models.Mmdis.Dto.IdMmsiImoCallSign {
                CallSign = callSign,
                Imo = imo,
                Mmsi = mmsi,
                TargetId = id
            });

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var results = Assert.IsAssignableFrom<ResponseExtendedShipList>(
                okResult.Value);
            Assert.NotNull(results);
        }

        [Theory]
        [InlineData(1, 0, null, null)]
        [InlineData(1, 0, "", "")]
        [InlineData(1, 0, " ", " ")]
        [InlineData(0, 1111, "IMO", "CS1")]
        [InlineData(1, 1111, "IMO", "CS1")]
        public async Task Should_Return_BadRequest_When_NotFound_GetShip(int id, int mmsi, string imo, string callSign)
        {
            Init();            
            var controllerResult = await _vesselsController.GetShip(new Core.VesselEntityFrameworkCore.Models.Mmdis.Dto.IdMmsiImoCallSign
            {
                CallSign = callSign,
                Imo = imo,
                Mmsi = mmsi,
                TargetId = id
            });

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        [Fact]
        public async Task Get_Should_Return_All_Vessels_WithoutDeletedVessels()
        {
            Init();
            await CreateVessel();
            var controllerResult = _vesselsController.Get();

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var results = Assert.IsAssignableFrom<PaginatedList<ResponseExtendedShipList>>(
                okResult.Value);

            Assert.True(results.TotalItems > 0);
            Assert.Equal(1, results.TotalItems);
        }

        [Fact]
        public async Task Get_Should_Return_All_Vessels_WithDeletedVessels()
        {
            Init();
            await CreateVessel();
            await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList { MMSI = 444 });
            await _vtsVesselService.SoftDeleteVesselAsync(2, 0, null, null);
            var controllerResult = _vesselsController.Get(includeDeletedShips: true);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var results = Assert.IsAssignableFrom<PaginatedList<ResponseExtendedShipList>>(
                okResult.Value);

            Assert.True(results.TotalItems > 0);
            Assert.Equal(2, results.TotalItems);
        }

        private async Task CreateVessel()
        {
            await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList
            {
                AnnualTonnageDue = 1,
                Beam = 2,
                Breadth = 3,
                BuilderAddress1 = "Address 1",
                BuilderAddress2 = "Address 2",
                BuilderAddress3 = "Address 3",
                BuilderCountryCode = "MY",
                BuilderName = "Builder Name",
                BuilderPostCode = "A1",
                CallSign = "CS1",
                Class = "Class A",
                CountryFlag = "Malaysia",
                DateOfBuildingContract = DateTime.Now,
                DateOfDelivery = DateTime.Now,
                DateOfKeelLaid = DateTime.Now,
                DeadWeight = 4,
                Draught = 5,
                EngineDescription = "Engine Desc",
                GrossTonnage = 6,
                HullMaterial = "Hull Material",
                IMO = "IMO",
                IsCharter = true,
                Length = 7,
                LocalityStatus = "L Status",
                MMSI = 1111,
                NetTonnage = 8,
                NoOfDeck = 9,
                NoOfHull = 10,
                NoOfPassenger = 11,
                NumberOfCabin = 12,
                NumberOfEngine = 13,
                OfficialNo = "OfficalNo1",
                PlaceOfBuilt = "Place built",
                PlyingLimit = 14,
                PortOfRegistry = "PortOfRegistry",
                Propel = 16,
                PropulsionType = "propulsion type",
                Speed = 17,
                Status = 18,
                TotalEnginePower = 19,
                TotalNumberOfCrew = 20,
                TradingArea = "TradingArea",
                TypeOfRegistry = "type of reg",
                Value = 22,
                ValueCurrency = "MYR",
                VesselCategory = 23,
                VesselName = "Vessel A",
                VesselType = "Ferry",
                YardNumber = 25
            });
        }

        [Fact]
        public async Task Post_Should_Create_Vessel()
        {
            Init();

            var input = new CreateExtendedShipList
            {
                AnnualTonnageDue = 1,
                Beam = 2,
                Breadth = 3,
                BuilderAddress1 = "Address 1",
                BuilderAddress2 = "Address 2",
                BuilderAddress3 = "Address 3",
                BuilderCountryCode = "MY",
                BuilderName = "Builder Name",
                BuilderPostCode = "A1",
                CallSign = "CS1",
                Class = "Class A",
                CountryFlag = "Malaysia",
                DateOfBuildingContract = DateTime.Now,
                DateOfDelivery = DateTime.Now,
                DateOfKeelLaid = DateTime.Now,
                DeadWeight = 4,
                Draught = 5,
                EngineDescription = "Engine Desc",
                GrossTonnage = 6,
                HullMaterial = "Hull Material",
                IMO = "IMO",
                IsCharter = true,
                Length = 7,
                LocalityStatus = "L Status",
                MMSI = 1111,
                NetTonnage = 8,
                NoOfDeck = 9,
                NoOfHull = 10,
                NoOfPassenger = 11,
                NumberOfCabin = 12,
                NumberOfEngine = 13,
                OfficialNo = "OfficalNo1",
                PlaceOfBuilt = "Place built",
                PlyingLimit = 14,
                PortOfRegistry = "PortOfRegistry",
                Propel = 16,
                PropulsionType = "propulsion type",
                Speed = 17,
                Status = 18,
                TotalEnginePower = 19,
                TotalNumberOfCrew = 20,
                TradingArea = "TradingArea",
                TypeOfRegistry = "type of reg",
                Value = 22,
                ValueCurrency = "MYR",
                VesselCategory = 23,
                VesselName = "Vessel A",
                VesselType = "Ferry",
                YardNumber = 25
            };

            var controllerResult = await _vesselsController.Create(input);

            var okResult = Assert.IsType<CreatedResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.Created, okResult.StatusCode);

            //var result = Assert.IsAssignableFrom<ResponseExtendedShipList>(
            //    okResult.Value);

            //Assert.Same("Vessel A", result.VesselName);
        }

        [Fact]
        public async Task Post_Should_Return_BadRequest_If_Vessel_Already_Exist_When_Create_Vessel()
        {
            Init();
            var input = new CreateExtendedShipList
            {
                AnnualTonnageDue = 1,
                Beam = 2,
                Breadth = 3,
                BuilderAddress1 = "Address 1",
                BuilderAddress2 = "Address 2",
                BuilderAddress3 = "Address 3",
                BuilderCountryCode = "MY",
                BuilderName = "Builder Name",
                BuilderPostCode = "A1",
                CallSign = "CS1",
                Class = "Class A",
                CountryFlag = "Malaysia",
                DateOfBuildingContract = DateTime.Now,
                DateOfDelivery = DateTime.Now,
                DateOfKeelLaid = DateTime.Now,
                DeadWeight = 4,
                Draught = 5,
                EngineDescription = "Engine Desc",
                GrossTonnage = 6,
                HullMaterial = "Hull Material",
                IMO = "IMO",
                IsCharter = true,
                Length = 7,
                LocalityStatus = "L Status",
                MMSI = 1111,
                NetTonnage = 8,
                NoOfDeck = 9,
                NoOfHull = 10,
                NoOfPassenger = 11,
                NumberOfCabin = 12,
                NumberOfEngine = 13,
                OfficialNo = "OfficalNo1",
                PlaceOfBuilt = "Place built",
                PlyingLimit = 14,
                PortOfRegistry = "PortOfRegistry",
                Propel = 16,
                PropulsionType = "propulsion type",
                Speed = 17,
                Status = 18,
                TotalEnginePower = 19,
                TotalNumberOfCrew = 20,
                TradingArea = "TradingArea",
                TypeOfRegistry = "type of reg",
                Value = 22,
                ValueCurrency = "MYR",
                VesselCategory = 23,
                VesselName = "Vessel A",
                VesselType = "Ferry",
                YardNumber = 25
            };
            await CreateVessel();
            var controllerResult = await _vesselsController.Create(input);

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        [Fact]
        public async Task Put_Should_Update_Vessel()
        {
            Init();
            await CreateVessel();
            var input = new UpdateExtendedShipList
            {
                Id = 1,
                AnnualTonnageDue = 1,
                Beam = 2,
                Breadth = 3,
                BuilderAddress1 = "Address 1",
                BuilderAddress2 = "Address 2",
                BuilderAddress3 = "Address 3",
                BuilderCountryCode = "MY",
                BuilderName = "Builder Name",
                BuilderPostCode = "A1",
                CallSign = "cs",
                Class = "Class A",
                CountryFlag = "Malaysia",
                DateOfBuildingContract = DateTime.Now,
                DateOfDelivery = DateTime.Now,
                DateOfKeelLaid = DateTime.Now,
                DeadWeight = 4,
                Draught = 5,
                EngineDescription = "Engine Desc",
                GrossTonnage = 6,
                HullMaterial = "Hull Material",
                IMO = "IMO",
                IsCharter = true,
                Length = 7,
                LocalityStatus = "L Status",
                MMSI = 1111,
                NetTonnage = 8,
                NoOfDeck = 9,
                NoOfHull = 10,
                NoOfPassenger = 11,
                NumberOfCabin = 12,
                NumberOfEngine = 13,
                OfficialNo = "OfficalNo1",
                PlaceOfBuilt = "Place built",
                PlyingLimit = 14,
                PortOfRegistry = "PortOfRegistry",
                Propel = 16,
                PropulsionType = "propulsion type",
                Speed = 17,
                Status = 18,
                TotalEnginePower = 19,
                TotalNumberOfCrew = 20,
                TradingArea = "TradingArea",
                TypeOfRegistry = "type of reg",
                Value = 22,
                ValueCurrency = "MYR",
                VesselCategory = 23,
                VesselName = "Updated Vessel A",
                VesselType = "Ferry",
                YardNumber = 25
            };
            var controllerResult = await _vesselsController.Update(input);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<ResponseExtendedShipList>(
                okResult.Value);

            Assert.Same("Updated Vessel A", result.VesselName);
        }

        [Fact]
        public async Task Put_Should_Return_BadRequest_If_Vessel_Does_Not_Exist_When_Update_Vessel()
        {
            Init();
            await CreateVessel();
            var input = new UpdateExtendedShipList
            {
                Id = 999
            };
            var controllerResult = await _vesselsController.Update(input);

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        //[Fact]
        //public async Task Put_Should_Return_BadRequest_If_Vessel_AlreadyExist_When_Update_Vessel()
        //{
        //    Init();
        //    await CreateVessel();
        //    await _vtsVesselService.CreateVesselAsync(new CreateExtendedShipList
        //    {
        //        AnnualTonnageDue = 1,
        //        Beam = 2,
        //        Breadth = 3,
        //        BuilderAddress1 = "Address 1",
        //        BuilderAddress2 = "Address 2",
        //        BuilderAddress3 = "Address 3",
        //        BuilderCountryCode = "MY",
        //        BuilderName = "Builder Name",
        //        BuilderPostCode = "A1",
        //        CallSign = "CS1 A",
        //        Class = "Class A",
        //        CountryFlag = "Malaysia",
        //        DateOfBuildingContract = DateTime.Now,
        //        DateOfDelivery = DateTime.Now,
        //        DateOfKeelLaid = DateTime.Now,
        //        DeadWeight = 4,
        //        Draught = 5,
        //        EngineDescription = "Engine Desc",
        //        GrossTonnage = 6,
        //        HullMaterial = "Hull Material",
        //        IMO = "IMO A",
        //        IsCharter = true,
        //        Length = 7,
        //        LocalityStatus = "L Status",
        //        MMSI = 11112,
        //        NetTonnage = 8,
        //        NoOfDeck = 9,
        //        NoOfHull = 10,
        //        NoOfPassenger = 11,
        //        NumberOfCabin = 12,
        //        NumberOfEngine = 13,
        //        OfficialNo = "OfficalNo1",
        //        PlaceOfBuilt = "Place built",
        //        PlyingLimit = 14,
        //        PortOfRegistry = "PortOfRegistry",
        //        Propel = 16,
        //        PropulsionType = "propulsion type",
        //        Speed = 17,
        //        Status = 18,
        //        TotalEnginePower = 19,
        //        TotalNumberOfCrew = 20,
        //        TradingArea = "TradingArea",
        //        TypeOfRegistry = "type of reg",
        //        Value = 22,
        //        ValueCurrency = "MYR",
        //        VesselCategory = 23,
        //        VesselName = "Vessel B",
        //        VesselType = "Ferry",
        //        YardNumber = 25
        //    });
        //    var input = new UpdateExtendedShipList
        //    {
        //        Id = 1,
        //        AnnualTonnageDue = 1,
        //        Beam = 2,
        //        Breadth = 3,
        //        BuilderAddress1 = "Address 1",
        //        BuilderAddress2 = "Address 2",
        //        BuilderAddress3 = "Address 3",
        //        BuilderCountryCode = "MY",
        //        BuilderName = "Builder Name",
        //        BuilderPostCode = "A1",
        //        CallSign = "CS1",
        //        Class = "Class A",
        //        CountryFlag = "Malaysia",
        //        DateOfBuildingContract = DateTime.Now,
        //        DateOfDelivery = DateTime.Now,
        //        DateOfKeelLaid = DateTime.Now,
        //        DeadWeight = 4,
        //        Draught = 5,
        //        EngineDescription = "Engine Desc",
        //        GrossTonnage = 6,
        //        HullMaterial = "Hull Material",
        //        IMO = "IMO",
        //        IsCharter = true,
        //        Length = 7,
        //        LocalityStatus = "L Status",
        //        MMSI = 11112,
        //        NetTonnage = 8,
        //        NoOfDeck = 9,
        //        NoOfHull = 10,
        //        NoOfPassenger = 11,
        //        NumberOfCabin = 12,
        //        NumberOfEngine = 13,
        //        OfficialNo = "OfficalNo1",
        //        PlaceOfBuilt = "Place built",
        //        PlyingLimit = 14,
        //        PortOfRegistry = "PortOfRegistry",
        //        Propel = 16,
        //        PropulsionType = "propulsion type",
        //        Speed = 17,
        //        Status = 18,
        //        TotalEnginePower = 19,
        //        TotalNumberOfCrew = 20,
        //        TradingArea = "TradingArea",
        //        TypeOfRegistry = "type of reg",
        //        Value = 22,
        //        ValueCurrency = "MYR",
        //        VesselCategory = 23,
        //        VesselName = "Updated Vessel A",
        //        VesselType = "Ferry",
        //        YardNumber = 25
        //    };
        //    var controllerResult = await _vesselsController.Update(input);

        //    var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
        //    Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        //}

        [Theory]
        [InlineData(1, 0, null, null)]
        [InlineData(1, 0, "", "")]
        [InlineData(1, 0, " ", " ")]
        [InlineData(0, 1111, "IMO", "CS1")]
        [InlineData(1, 1111, "IMO", "CS1")]
        public async Task Should_Blacklist_Vessel(int id, int mmsi, string imo, string callSign)
        {
            Init();
            await CreateVessel();
            var controllerResult = await _vesselsController.Blacklist(new Core.VesselEntityFrameworkCore.Models.Mmdis.Dto.IdMmsiImoCallSign
            {
                CallSign = callSign,
                Imo = imo,
                Mmsi = mmsi,
                TargetId = id
            });

            var okResult = Assert.IsType<OkResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = await InternalContext.ExtendedShipLists.FirstOrDefaultAsync(x => x.Id == 1);
            Assert.True(result.IsBlackList);
        }

        [Theory]
        [InlineData(1, 0, null, null)]
        [InlineData(1, 0, "", "")]
        [InlineData(1, 0, " ", " ")]
        [InlineData(0, 1111, "IMO", "CS1")]
        [InlineData(1, 1111, "IMO", "CS1")]
        public async Task Should_Return_BadRequest_When_Blacklist_Vessel(int id, int mmsi, string imo, string callSign)
        {
            Init();
            var controllerResult = await _vesselsController.Blacklist(new Core.VesselEntityFrameworkCore.Models.Mmdis.Dto.IdMmsiImoCallSign
            {
                CallSign = callSign,
                Imo = imo,
                Mmsi = mmsi,
                TargetId = id
            });

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        [Fact]
        public async Task Should_Return_BadRequest_If_InvalidParamater_Supplied_When_Blacklist_Vessel()
        {
            Init();
            var controllerResult = await _vesselsController.Blacklist(new Core.VesselEntityFrameworkCore.Models.Mmdis.Dto.IdMmsiImoCallSign
            {
                CallSign = null,
                Imo = null,
                Mmsi = 0,
                TargetId = 0
            });

            var okResult = Assert.IsType<BadRequestResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }
    }
}
