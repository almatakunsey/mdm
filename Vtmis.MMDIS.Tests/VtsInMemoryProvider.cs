﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using Vtmis.Core.VesselEntityFrameworkCore;

namespace Vtmis.MMDIS.Tests
{
    public abstract class VtsInMemoryProvider
    {
        public VtsDatabaseContext Context
        {
            get
            {
                DbContextOptions<VtsDatabaseContext> options;
                var builder = new DbContextOptionsBuilder<VtsDatabaseContext>();
                builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
                var serviceProvider = new ServiceCollection()
                                        .AddEntityFrameworkInMemoryDatabase()
                                        .BuildServiceProvider();
                builder.UseInternalServiceProvider(serviceProvider);
                options = builder.Options;
                var dbContext = new VtsDatabaseContext(options);
                dbContext.Database.EnsureDeleted();
                dbContext.Database.EnsureCreated();
                return dbContext;
            }
        }
        public VtsDatabaseContext InternalContext { get; set; }
    }
}
