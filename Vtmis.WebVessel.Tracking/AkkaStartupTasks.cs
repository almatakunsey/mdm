﻿using Akka.Actor;
using Akka.Configuration;
using Akka.DI.AutoFac;
using Autofac;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Npgsql;
using StackExchange.Redis;
using Vtmis.Core.ActorModel.Actors;
using Vtmis.Core.ActorModel.Hubs;
using Vtmis.Core.Common;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;
using Vtmis.WebVessel.Tracking.Repository.Concrete;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;

namespace Vtmis.WebVessel.Tracking
{
    public static class AkkaStartupTasks
    {
        public static void AddActorSystem(IConfiguration configuration, MapTransactionSpecification mts, IHubContext<VesselGeoLocationHub> hubContext, IConnectionMultiplexer redis, ActorSystem actorSystem)
        {
           
            var builder = new ContainerBuilder();
            builder.RegisterType<MapTrackingSupervisorActor>();
            builder.RegisterType<MapTrackingActor>();
            builder.RegisterType<VesselQueryActor>();
            builder.Register(ctx =>
            {
                return mts;
            }).As<MapTransactionSpecification>().SingleInstance();
            builder.Register(ctx =>
            {
                return hubContext;
            }).As<IHubContext<VesselGeoLocationHub>>().SingleInstance();
            builder.Register(ctx =>
            {
                return redis;
            }).As<IConnectionMultiplexer>().SingleInstance();          
            builder.RegisterType<RedisService>().As<ICacheService>();
            builder.RegisterType<AISRepo_mongoDB>().As<ITargetQuery>();

            var container = builder.Build();
            var resolver = new AutoFacDependencyResolver(container, actorSystem);
            actorSystem.ActorOf(resolver.Create<MapTrackingSupervisorActor>(), "MapTrackingSupervisorActor");
            //actorSystem = actorSystem;
        }
    }
}
