﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using Vtmis.WebVessel.Tracking.Services.Concrete;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using System;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using Vtmis.Core.Common.Helpers;
using Microsoft.AspNetCore.SignalR;
using System.Threading;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Vtmis.Core.Common.Constants;
using Vtmis.WebVessel.Tracking.Repository.Concrete;
using System.Data;
using System.Data.SqlClient;
using StackExchange.Redis;
using Vtmis.WebAdmin.EICS;
using Vtmis.WebAdmin;
using Vtmis.WebAdmin.Web.Host.Startup;
using Vtmis.WebAdmin.Authentication.JwtBearer;
using Npgsql;
using Vtmis.Core.Model;
using Vtmis.Core.ActorModel.Hubs;
using Vtmis.Core.Common;
using Akka.Configuration;
using Hangfire;
using Hangfire.PostgreSql;
using Hangfire.Storage;

namespace Vtmis.WebVessel.Tracking
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }

        public static ServiceProvider provider { get; private set; }
        public static string VtsConnectionString { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        /// <summary>
        /// https://stackoverflow.com/questions/45712067/missing-extension-method-addjwtbearerauthentication-for-iservicecollection-in
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            try
            {
                var hangfireDbContext = new HangfireDbContext();
                hangfireDbContext.Database.EnsureCreated();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
          
            MapTransactionSpecification mts = new MapTransactionSpecification();
            services.AddSingleton<MapTransactionSpecification>(mt => mts);
            services.AddResponseCompression();
            AuthConfigurer.Configure(services, Configuration);
            if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.PRODUCTION))
            {
                services.AddSignalR(o =>
                {
                    o.EnableDetailedErrors = true;
                })
                 .AddStackExchangeRedis(AppHelper.GetRedisConnectionString(), options =>
                 {
                     options.Configuration.ChannelPrefix = "MapPortalPrefix";
                     options.Configuration.ClientName = "MapPortal";
                 });
            }
            else
            {
                services.AddSignalR(o =>
                {
                    o.EnableDetailedErrors = true;
                });
            }
            services.AddCors(
              options => options.AddPolicy(
                  "CorsPolicy",
                  builder => builder
                       //.WithOrigins(
                       //    AppHelper.GetCorsOriginList()
                       // )
                       .SetIsOriginAllowed(isOriginAllowed: _ => true)
                      .AllowAnyHeader()
                      .AllowAnyMethod()
                      .AllowCredentials()
              )
          );

            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            services.AddMvc(options => options.Filters.Add(new CorsAuthorizationFilterFactory("CorsPolicy")))
                .AddJsonOptions(options => options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
            // TODO: SSLConfig
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    services.AddHsts(options =>
            //    {
            //        options.Preload = true;
            //        options.IncludeSubDomains = true;
            //        options.MaxAge = TimeSpan.FromHours(23);
            //    });
            //    services.AddHttpsRedirection(options =>
            //    {
            //        options.RedirectStatusCode = StatusCodes.Status308PermanentRedirect;
            //        options.HttpsPort = 443;
            //    });
            //}
            //var dbPostDate = Convert.ToDateTime("2017-08-30");          

            var connectionString = AppHelper.GetVtsDbConnectionString();
            VtsConnectionString = connectionString;
            services.AddHangfire(c =>
                c.UsePostgreSqlStorage(AppHelper.GetHangireDbConnectionString()));
            JobStorage.Current = new PostgreSqlStorage(AppHelper.GetHangireDbConnectionString());

            using (var connection = JobStorage.Current.GetConnection())
            {
                foreach (var recurringJob in StorageConnectionExtensions.GetRecurringJobs(connection))
                {
                    RecurringJob.RemoveIfExists(recurringJob.Id);
                }
            }

            services.AddDbContext<VtsDatabaseContext>(o => o.UseSqlServer(connectionString), ServiceLifetime.Scoped);
            services.AddSingleton<IDbConnection>(ctx =>
            {
                return new SqlConnection(AppHelper.GetAisConnectionString(DateTime.Now));
            });
            services.AddSingleton<IConnectionMultiplexer>(x =>
              ConnectionMultiplexer.Connect(AppHelper.GetRedisConnectionString()));
            //services.AddSingleton(x => new VesselDatabaseContext($"AIS{dbDate.ToString("yyMMdd")}", dbServer, userName, passWord));
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IVesselRepository, Vtmis.WebVessel.Tracking.Repository.Concrete.VesselRepository>();
            services.AddTransient<IVesselService, VesselService>();
            services.AddTransient<IDbConnection>(ctx =>
            {
                return new SqlConnection(AppHelper.GetAisConnectionString(DateTime.Now));
            });
            services.AddTransient(ctx =>
            {
                return new NpgsqlConnection(AppHelper.GetMdmDbConnectionString());
            });
            services.AddTransient<IDapperAisRepository, DapperAisRepository>();
            services.AddTransient<ITargetQuery, AISRepo_mongoDB>();
            services.AddTransient<ICacheService, RedisService>();
            services.AddTransient<IDapperAdmin, DapperAdmin>();
            services.AddTransient<IVesselService, VesselService>();
            services.AddSingleton<IUserIdProvider, NameUserIdProvider>();


            //new AkkaLoggerConfig().SetupLog("MapPortal");

            var config = ConfigurationFactory.ParseString(@"
                akka {
                    loglevel=INFO,
                    loggers=[""Akka.Logger.Serilog.SerilogLogger, Akka.Logger.Serilog""]
                }           
            ");

            //var actorSystem = ActorSystem.Create("vtmis", config);

            //services.AddSingleton<ActorSystem>(c => { return actorSystem; });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// <summary>
        /// https://github.com/aspnet/Security/blob/c5b566ed4abffac4cd7011e0465e012cf503c871/samples/JwtBearerSample/Startup.cs#L47-L53
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app)
        {
            if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.LOCAL))
            {
                app.UseDeveloperExceptionPage();

            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            // TODO: SSLConfig
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    app.UseHsts();
            //    app.UseHttpsRedirection();
            //}
            //app.UseAuthentication();
            app.UseResponseCompression();
            app.UseStaticFiles();
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseJwtTokenMiddleware();
            app.UseSignalR(routes =>
            {
                routes.MapHub<VesselGeoLocationHub>("/vesselgeolocationhub");

            });

            app.UseForwardedHeaders();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Dashboard}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "old_maps",
                    template: "{controller=Dashboard}/{action=Maps}/{id?}");

                routes.MapRoute(
                    name: "signalr",
                    template: "{controller}/{action}/{id?}");
            });

        }

        public ManualResetEvent Wait;
        private readonly IHostingEnvironment _env;

    }
}
