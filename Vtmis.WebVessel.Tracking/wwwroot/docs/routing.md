[<< Home](index.md)

### <u>Files and route grouping</u>

All routes resides in `resources/assets/js/router`

Each route group; eg admin/\*, map/\* will be in their own file

`resources/assets/js/router/map.js`

`resources/assets/js/router/admin.js`

### <u>Layout loading</u>

The layout component will be registered within the route as they will be needed right away

```js
// layouts
Vue.component('default-layout', ()=> import('../components/layouts/HomeLayout.vue'));
Vue.component('main-layout', ()=> import('../components/layouts/MainLayout.vue'));
Vue.component('admin-layout', ()=> import('../components/layouts/AdminLayout.vue'));
```







  