# VTMIS Documentation

This document aims to provide general overview of the VTMIS application to ensure common understanding of the overall working of the application. It does not try to cover everything but enough to give anyone a head up on where to start. 



## General Components

[Alert and Notification](alert.md)

## Routing

[Routing](routing.md)

