[<< Home](index.md)

/components/common/AppNotification.vue

<u>Props</u>

`centertext = default: false`

<u>Event in</u>

The component will listen to some events triggered globally

`Notification.ServerError`

`Notification.Success`

The events expected payload of type object

```js
{
    message: ''
}
```







