function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/** 
 * @overview ASP.NET Core SignalR JavaScript Client.
 * @version 1.0.2.
 * @license
 * Copyright (c) .NET Foundation. All rights reserved.
 * Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
 */
(function (global, factory) {
  (typeof exports === "undefined" ? "undefined" : _typeof(exports)) === "object" && typeof module !== "undefined" ? module.exports = factory() : typeof define === "function" && define.amd ? define(factory) : global.signalR = factory();
})(this, function () {
  "use strict";

  var commonjsGlobal = typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : {};

  function commonjsRequire() {
    throw new Error("Dynamic requires are not currently supported by rollup-plugin-commonjs");
  }

  function unwrapExports(x) {
    return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, "default") ? x["default"] : x;
  }

  function createCommonjsModule(fn, module) {
    return module = {
      exports: {}
    }, fn(module, module.exports), module.exports;
  }

  var extendStatics = Object.setPrototypeOf || {
    __proto__: []
  } instanceof Array && function (d, b) {
    d.__proto__ = b;
  } || function (d, b) {
    for (var p in b) {
      if (b.hasOwnProperty(p)) d[p] = b[p];
    }
  };

  function __extends(d, b) {
    extendStatics(d, b);

    function __() {
      this.constructor = d;
    }

    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
  }

  var __assign = Object.assign || function __assign(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
      }
    }

    return t;
  };

  function __rest(s, e) {
    var t = {};

    for (var p in s) {
      if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
    }

    if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
      if (e.indexOf(p[i]) < 0) t[p[i]] = s[p[i]];
    }
    return t;
  }

  function __decorate(decorators, target, key, desc) {
    var c = arguments.length,
        r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
        d;
    if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
      if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    }
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  }

  function __param(paramIndex, decorator) {
    return function (target, key) {
      decorator(target, key, paramIndex);
    };
  }

  function __metadata(metadataKey, metadataValue) {
    if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
  }

  function __awaiter(thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
      function fulfilled(value) {
        try {
          step(generator.next(value));
        } catch (e) {
          reject(e);
        }
      }

      function rejected(value) {
        try {
          step(generator["throw"](value));
        } catch (e) {
          reject(e);
        }
      }

      function step(result) {
        result.done ? resolve(result.value) : new P(function (resolve) {
          resolve(result.value);
        }).then(fulfilled, rejected);
      }

      step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
  }

  function __generator(thisArg, body) {
    var _ = {
      label: 0,
      sent: function sent() {
        if (t[0] & 1) throw t[1];
        return t[1];
      },
      trys: [],
      ops: []
    },
        f,
        y,
        t,
        g;
    return g = {
      next: verb(0),
      "throw": verb(1),
      "return": verb(2)
    }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
      return this;
    }), g;

    function verb(n) {
      return function (v) {
        return step([n, v]);
      };
    }

    function step(op) {
      if (f) throw new TypeError("Generator is already executing.");

      while (_) {
        try {
          if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
          if (y = 0, t) op = [0, t.value];

          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;

            case 4:
              _.label++;
              return {
                value: op[1],
                done: false
              };

            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;

            case 7:
              op = _.ops.pop();

              _.trys.pop();

              continue;

            default:
              if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                _ = 0;
                continue;
              }

              if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                _.label = op[1];
                break;
              }

              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }

              if (t && _.label < t[2]) {
                _.label = t[2];

                _.ops.push(op);

                break;
              }

              if (t[2]) _.ops.pop();

              _.trys.pop();

              continue;
          }

          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }
      }

      if (op[0] & 5) throw op[1];
      return {
        value: op[0] ? op[1] : void 0,
        done: true
      };
    }
  }

  function __exportStar(m, exports) {
    for (var p in m) {
      if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }
  }

  function __values(o) {
    var m = typeof Symbol === "function" && o[Symbol.iterator],
        i = 0;
    if (m) return m.call(o);
    return {
      next: function next() {
        if (o && i >= o.length) o = void 0;
        return {
          value: o && o[i++],
          done: !o
        };
      }
    };
  }

  function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o),
        r,
        ar = [],
        e;

    try {
      while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
        ar.push(r.value);
      }
    } catch (error) {
      e = {
        error: error
      };
    } finally {
      try {
        if (r && !r.done && (m = i["return"])) m.call(i);
      } finally {
        if (e) throw e.error;
      }
    }

    return ar;
  }

  function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++) {
      ar = ar.concat(__read(arguments[i]));
    }

    return ar;
  }

  function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
  }

  function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []),
        i,
        q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
      return this;
    }, i;

    function verb(n) {
      if (g[n]) i[n] = function (v) {
        return new Promise(function (a, b) {
          q.push([n, v, a, b]) > 1 || resume(n, v);
        });
      };
    }

    function resume(n, v) {
      try {
        step(g[n](v));
      } catch (e) {
        settle(q[0][3], e);
      }
    }

    function step(r) {
      r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
    }

    function fulfill(value) {
      resume("next", value);
    }

    function reject(value) {
      resume("throw", value);
    }

    function settle(f, v) {
      if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
    }
  }

  function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) {
      throw e;
    }), verb("return"), i[Symbol.iterator] = function () {
      return this;
    }, i;

    function verb(n, f) {
      if (o[n]) i[n] = function (v) {
        return (p = !p) ? {
          value: __await(o[n](v)),
          done: n === "return"
        } : f ? f(v) : v;
      };
    }
  }

  function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator];
    return m ? m.call(o) : typeof __values === "function" ? __values(o) : o[Symbol.iterator]();
  }

  function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) {
      Object.defineProperty(cooked, "raw", {
        value: raw
      });
    } else {
      cooked.raw = raw;
    }

    return cooked;
  }

  function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) {
      if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    }
    result["default"] = mod;
    return result;
  }

  function __importDefault(mod) {
    return mod && mod.__esModule ? mod : {
      "default": mod
    };
  }

  var tslib_1 = Object.freeze({
    __extends: __extends,
    __assign: __assign,
    __rest: __rest,
    __decorate: __decorate,
    __param: __param,
    __metadata: __metadata,
    __awaiter: __awaiter,
    __generator: __generator,
    __exportStar: __exportStar,
    __values: __values,
    __read: __read,
    __spread: __spread,
    __await: __await,
    __asyncGenerator: __asyncGenerator,
    __asyncDelegator: __asyncDelegator,
    __asyncValues: __asyncValues,
    __makeTemplateObject: __makeTemplateObject,
    __importStar: __importStar,
    __importDefault: __importDefault
  });
  var es6Promise_auto = createCommonjsModule(function (module, exports) {
    /*!
     * @overview es6-promise - a tiny implementation of Promises/A+.
     * @copyright Copyright (c) 2014 Yehuda Katz, Tom Dale, Stefan Penner and contributors (Conversion to ES6 API by Jake Archibald)
     * @license   Licensed under MIT license
     *            See https://raw.githubusercontent.com/stefanpenner/es6-promise/master/LICENSE
     * @version   v4.2.2+97478eb6
     */
    (function (global, factory) {
      module.exports = factory();
    })(commonjsGlobal, function () {
      function objectOrFunction(x) {
        var type = _typeof(x);

        return x !== null && (type === "object" || type === "function");
      }

      function isFunction(x) {
        return typeof x === "function";
      }

      var _isArray = void 0;

      if (Array.isArray) {
        _isArray = Array.isArray;
      } else {
        _isArray = function _isArray(x) {
          return Object.prototype.toString.call(x) === "[object Array]";
        };
      }

      var isArray = _isArray;
      var len = 0;
      var vertxNext = void 0;
      var customSchedulerFn = void 0;

      var asap = function asap(callback, arg) {
        queue[len] = callback;
        queue[len + 1] = arg;
        len += 2;

        if (len === 2) {
          if (customSchedulerFn) {
            customSchedulerFn(flush);
          } else {
            scheduleFlush();
          }
        }
      };

      function setScheduler(scheduleFn) {
        customSchedulerFn = scheduleFn;
      }

      function setAsap(asapFn) {
        asap = asapFn;
      }

      var browserWindow = typeof window !== "undefined" ? window : undefined;
      var browserGlobal = browserWindow || {};
      var BrowserMutationObserver = browserGlobal.MutationObserver || browserGlobal.WebKitMutationObserver;
      var isNode = typeof self === "undefined" && typeof process !== "undefined" && {}.toString.call(process) === "[object process]";
      var isWorker = typeof Uint8ClampedArray !== "undefined" && typeof importScripts !== "undefined" && typeof MessageChannel !== "undefined";

      function useNextTick() {
        return function () {
          return process.nextTick(flush);
        };
      }

      function useVertxTimer() {
        if (typeof vertxNext !== "undefined") {
          return function () {
            vertxNext(flush);
          };
        }

        return useSetTimeout();
      }

      function useMutationObserver() {
        var iterations = 0;
        var observer = new BrowserMutationObserver(flush);
        var node = document.createTextNode("");
        observer.observe(node, {
          characterData: true
        });
        return function () {
          node.data = iterations = ++iterations % 2;
        };
      }

      function useMessageChannel() {
        var channel = new MessageChannel();
        channel.port1.onmessage = flush;
        return function () {
          return channel.port2.postMessage(0);
        };
      }

      function useSetTimeout() {
        var globalSetTimeout = setTimeout;
        return function () {
          return globalSetTimeout(flush, 1);
        };
      }

      var queue = new Array(1e3);

      function flush() {
        for (var i = 0; i < len; i += 2) {
          var callback = queue[i];
          var arg = queue[i + 1];
          callback(arg);
          queue[i] = undefined;
          queue[i + 1] = undefined;
        }

        len = 0;
      }

      function attemptVertx() {
        try {
          var r = commonjsRequire;
          var vertx = r("vertx");
          vertxNext = vertx.runOnLoop || vertx.runOnContext;
          return useVertxTimer();
        } catch (e) {
          return useSetTimeout();
        }
      }

      var scheduleFlush = void 0;

      if (isNode) {
        scheduleFlush = useNextTick();
      } else if (BrowserMutationObserver) {
        scheduleFlush = useMutationObserver();
      } else if (isWorker) {
        scheduleFlush = useMessageChannel();
      } else if (browserWindow === undefined && typeof commonjsRequire === "function") {
        scheduleFlush = attemptVertx();
      } else {
        scheduleFlush = useSetTimeout();
      }

      function then(onFulfillment, onRejection) {
        var parent = this;
        var child = new this.constructor(noop);

        if (child[PROMISE_ID] === undefined) {
          makePromise(child);
        }

        var _state = parent._state;

        if (_state) {
          var callback = arguments[_state - 1];
          asap(function () {
            return invokeCallback(_state, child, callback, parent._result);
          });
        } else {
          subscribe(parent, child, onFulfillment, onRejection);
        }

        return child;
      }

      function resolve$1(object) {
        var Constructor = this;

        if (object && _typeof(object) === "object" && object.constructor === Constructor) {
          return object;
        }

        var promise = new Constructor(noop);
        resolve(promise, object);
        return promise;
      }

      var PROMISE_ID = Math.random().toString(36).substring(16);

      function noop() {}

      var PENDING = void 0;
      var FULFILLED = 1;
      var REJECTED = 2;
      var GET_THEN_ERROR = new ErrorObject();

      function selfFulfillment() {
        return new TypeError("You cannot resolve a promise with itself");
      }

      function cannotReturnOwn() {
        return new TypeError("A promises callback cannot return that same promise.");
      }

      function getThen(promise) {
        try {
          return promise.then;
        } catch (error) {
          GET_THEN_ERROR.error = error;
          return GET_THEN_ERROR;
        }
      }

      function tryThen(then$$1, value, fulfillmentHandler, rejectionHandler) {
        try {
          then$$1.call(value, fulfillmentHandler, rejectionHandler);
        } catch (e) {
          return e;
        }
      }

      function handleForeignThenable(promise, thenable, then$$1) {
        asap(function (promise) {
          var sealed = false;
          var error = tryThen(then$$1, thenable, function (value) {
            if (sealed) {
              return;
            }

            sealed = true;

            if (thenable !== value) {
              resolve(promise, value);
            } else {
              fulfill(promise, value);
            }
          }, function (reason) {
            if (sealed) {
              return;
            }

            sealed = true;
            reject(promise, reason);
          }, "Settle: " + (promise._label || " unknown promise"));

          if (!sealed && error) {
            sealed = true;
            reject(promise, error);
          }
        }, promise);
      }

      function handleOwnThenable(promise, thenable) {
        if (thenable._state === FULFILLED) {
          fulfill(promise, thenable._result);
        } else if (thenable._state === REJECTED) {
          reject(promise, thenable._result);
        } else {
          subscribe(thenable, undefined, function (value) {
            return resolve(promise, value);
          }, function (reason) {
            return reject(promise, reason);
          });
        }
      }

      function handleMaybeThenable(promise, maybeThenable, then$$1) {
        if (maybeThenable.constructor === promise.constructor && then$$1 === then && maybeThenable.constructor.resolve === resolve$1) {
          handleOwnThenable(promise, maybeThenable);
        } else {
          if (then$$1 === GET_THEN_ERROR) {
            reject(promise, GET_THEN_ERROR.error);
            GET_THEN_ERROR.error = null;
          } else if (then$$1 === undefined) {
            fulfill(promise, maybeThenable);
          } else if (isFunction(then$$1)) {
            handleForeignThenable(promise, maybeThenable, then$$1);
          } else {
            fulfill(promise, maybeThenable);
          }
        }
      }

      function resolve(promise, value) {
        if (promise === value) {
          reject(promise, selfFulfillment());
        } else if (objectOrFunction(value)) {
          handleMaybeThenable(promise, value, getThen(value));
        } else {
          fulfill(promise, value);
        }
      }

      function publishRejection(promise) {
        if (promise._onerror) {
          promise._onerror(promise._result);
        }

        publish(promise);
      }

      function fulfill(promise, value) {
        if (promise._state !== PENDING) {
          return;
        }

        promise._result = value;
        promise._state = FULFILLED;

        if (promise._subscribers.length !== 0) {
          asap(publish, promise);
        }
      }

      function reject(promise, reason) {
        if (promise._state !== PENDING) {
          return;
        }

        promise._state = REJECTED;
        promise._result = reason;
        asap(publishRejection, promise);
      }

      function subscribe(parent, child, onFulfillment, onRejection) {
        var _subscribers = parent._subscribers;
        var length = _subscribers.length;
        parent._onerror = null;
        _subscribers[length] = child;
        _subscribers[length + FULFILLED] = onFulfillment;
        _subscribers[length + REJECTED] = onRejection;

        if (length === 0 && parent._state) {
          asap(publish, parent);
        }
      }

      function publish(promise) {
        var subscribers = promise._subscribers;
        var settled = promise._state;

        if (subscribers.length === 0) {
          return;
        }

        var child = void 0,
            callback = void 0,
            detail = promise._result;

        for (var i = 0; i < subscribers.length; i += 3) {
          child = subscribers[i];
          callback = subscribers[i + settled];

          if (child) {
            invokeCallback(settled, child, callback, detail);
          } else {
            callback(detail);
          }
        }

        promise._subscribers.length = 0;
      }

      function ErrorObject() {
        this.error = null;
      }

      var TRY_CATCH_ERROR = new ErrorObject();

      function tryCatch(callback, detail) {
        try {
          return callback(detail);
        } catch (e) {
          TRY_CATCH_ERROR.error = e;
          return TRY_CATCH_ERROR;
        }
      }

      function invokeCallback(settled, promise, callback, detail) {
        var hasCallback = isFunction(callback),
            value = void 0,
            error = void 0,
            succeeded = void 0,
            failed = void 0;

        if (hasCallback) {
          value = tryCatch(callback, detail);

          if (value === TRY_CATCH_ERROR) {
            failed = true;
            error = value.error;
            value.error = null;
          } else {
            succeeded = true;
          }

          if (promise === value) {
            reject(promise, cannotReturnOwn());
            return;
          }
        } else {
          value = detail;
          succeeded = true;
        }

        if (promise._state !== PENDING) {} else if (hasCallback && succeeded) {
          resolve(promise, value);
        } else if (failed) {
          reject(promise, error);
        } else if (settled === FULFILLED) {
          fulfill(promise, value);
        } else if (settled === REJECTED) {
          reject(promise, value);
        }
      }

      function initializePromise(promise, resolver) {
        try {
          resolver(function resolvePromise(value) {
            resolve(promise, value);
          }, function rejectPromise(reason) {
            reject(promise, reason);
          });
        } catch (e) {
          reject(promise, e);
        }
      }

      var id = 0;

      function nextId() {
        return id++;
      }

      function makePromise(promise) {
        promise[PROMISE_ID] = id++;
        promise._state = undefined;
        promise._result = undefined;
        promise._subscribers = [];
      }

      function validationError() {
        return new Error("Array Methods must be provided an Array");
      }

      function validationError() {
        return new Error("Array Methods must be provided an Array");
      }

      var Enumerator = function () {
        function Enumerator(Constructor, input) {
          this._instanceConstructor = Constructor;
          this.promise = new Constructor(noop);

          if (!this.promise[PROMISE_ID]) {
            makePromise(this.promise);
          }

          if (isArray(input)) {
            this.length = input.length;
            this._remaining = input.length;
            this._result = new Array(this.length);

            if (this.length === 0) {
              fulfill(this.promise, this._result);
            } else {
              this.length = this.length || 0;

              this._enumerate(input);

              if (this._remaining === 0) {
                fulfill(this.promise, this._result);
              }
            }
          } else {
            reject(this.promise, validationError());
          }
        }

        Enumerator.prototype._enumerate = function _enumerate(input) {
          for (var i = 0; this._state === PENDING && i < input.length; i++) {
            this._eachEntry(input[i], i);
          }
        };

        Enumerator.prototype._eachEntry = function _eachEntry(entry, i) {
          var c = this._instanceConstructor;
          var resolve$$1 = c.resolve;

          if (resolve$$1 === resolve$1) {
            var _then = getThen(entry);

            if (_then === then && entry._state !== PENDING) {
              this._settledAt(entry._state, i, entry._result);
            } else if (typeof _then !== "function") {
              this._remaining--;
              this._result[i] = entry;
            } else if (c === Promise$2) {
              var promise = new c(noop);
              handleMaybeThenable(promise, entry, _then);

              this._willSettleAt(promise, i);
            } else {
              this._willSettleAt(new c(function (resolve$$1) {
                return resolve$$1(entry);
              }), i);
            }
          } else {
            this._willSettleAt(resolve$$1(entry), i);
          }
        };

        Enumerator.prototype._settledAt = function _settledAt(state, i, value) {
          var promise = this.promise;

          if (promise._state === PENDING) {
            this._remaining--;

            if (state === REJECTED) {
              reject(promise, value);
            } else {
              this._result[i] = value;
            }
          }

          if (this._remaining === 0) {
            fulfill(promise, this._result);
          }
        };

        Enumerator.prototype._willSettleAt = function _willSettleAt(promise, i) {
          var enumerator = this;
          subscribe(promise, undefined, function (value) {
            return enumerator._settledAt(FULFILLED, i, value);
          }, function (reason) {
            return enumerator._settledAt(REJECTED, i, reason);
          });
        };

        return Enumerator;
      }();

      function all(entries) {
        return new Enumerator(this, entries).promise;
      }

      function race(entries) {
        var Constructor = this;

        if (!isArray(entries)) {
          return new Constructor(function (_, reject) {
            return reject(new TypeError("You must pass an array to race."));
          });
        } else {
          return new Constructor(function (resolve, reject) {
            var length = entries.length;

            for (var i = 0; i < length; i++) {
              Constructor.resolve(entries[i]).then(resolve, reject);
            }
          });
        }
      }

      function reject$1(reason) {
        var Constructor = this;
        var promise = new Constructor(noop);
        reject(promise, reason);
        return promise;
      }

      function needsResolver() {
        throw new TypeError("You must pass a resolver function as the first argument to the promise constructor");
      }

      function needsNew() {
        throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
      }

      var Promise$2 = function () {
        function Promise(resolver) {
          this[PROMISE_ID] = nextId();
          this._result = this._state = undefined;
          this._subscribers = [];

          if (noop !== resolver) {
            typeof resolver !== "function" && needsResolver();
            this instanceof Promise ? initializePromise(this, resolver) : needsNew();
          }
        }

        Promise.prototype["catch"] = function _catch(onRejection) {
          return this.then(null, onRejection);
        };

        Promise.prototype["finally"] = function _finally(callback) {
          var promise = this;
          var constructor = promise.constructor;
          return promise.then(function (value) {
            return constructor.resolve(callback()).then(function () {
              return value;
            });
          }, function (reason) {
            return constructor.resolve(callback()).then(function () {
              throw reason;
            });
          });
        };

        return Promise;
      }();

      Promise$2.prototype.then = then;
      Promise$2.all = all;
      Promise$2.race = race;
      Promise$2.resolve = resolve$1;
      Promise$2.reject = reject$1;
      Promise$2._setScheduler = setScheduler;
      Promise$2._setAsap = setAsap;
      Promise$2._asap = asap;

      function polyfill() {
        var local = void 0;

        if (typeof commonjsGlobal !== "undefined") {
          local = commonjsGlobal;
        } else if (typeof self !== "undefined") {
          local = self;
        } else {
          try {
            local = Function("return this")();
          } catch (e) {
            throw new Error("polyfill failed because global object is unavailable in this environment");
          }
        }

        var P = local.Promise;

        if (P) {
          var promiseToString = null;

          try {
            promiseToString = Object.prototype.toString.call(P.resolve());
          } catch (e) {}

          if (promiseToString === "[object Promise]" && !P.cast) {
            return;
          }
        }

        local.Promise = Promise$2;
      }

      Promise$2.polyfill = polyfill;
      Promise$2.Promise = Promise$2;
      Promise$2.polyfill();
      return Promise$2;
    });
  });
  var Errors = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var HttpError = function (_super) {
      tslib_1.__extends(HttpError, _super);

      function HttpError(errorMessage, statusCode) {
        var _newTarget = this.constructor;

        var _this = this;

        var trueProto = _newTarget.prototype;
        _this = _super.call(this, errorMessage) || this;
        _this.statusCode = statusCode;
        _this.__proto__ = trueProto;
        return _this;
      }

      return HttpError;
    }(Error);

    exports.HttpError = HttpError;

    var TimeoutError = function (_super) {
      tslib_1.__extends(TimeoutError, _super);

      function TimeoutError(errorMessage) {
        var _newTarget = this.constructor;

        if (errorMessage === void 0) {
          errorMessage = "A timeout occurred.";
        }

        var _this = this;

        var trueProto = _newTarget.prototype;
        _this = _super.call(this, errorMessage) || this;
        _this.__proto__ = trueProto;
        return _this;
      }

      return TimeoutError;
    }(Error);

    exports.TimeoutError = TimeoutError;
  });
  unwrapExports(Errors);
  var Errors_1 = Errors.HttpError;
  var Errors_2 = Errors.TimeoutError;
  var ILogger = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var LogLevel;

    (function (LogLevel) {
      LogLevel[LogLevel["Trace"] = 0] = "Trace";
      LogLevel[LogLevel["Debug"] = 1] = "Debug";
      LogLevel[LogLevel["Information"] = 2] = "Information";
      LogLevel[LogLevel["Warning"] = 3] = "Warning";
      LogLevel[LogLevel["Error"] = 4] = "Error";
      LogLevel[LogLevel["Critical"] = 5] = "Critical";
      LogLevel[LogLevel["None"] = 6] = "None";
    })(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
  });
  unwrapExports(ILogger);
  var ILogger_1 = ILogger.LogLevel;
  var HttpClient_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var HttpResponse = function () {
      function HttpResponse(statusCode, statusText, content) {
        this.statusCode = statusCode;
        this.statusText = statusText;
        this.content = content;
      }

      return HttpResponse;
    }();

    exports.HttpResponse = HttpResponse;

    var HttpClient = function () {
      function HttpClient() {}

      HttpClient.prototype.get = function (url, options) {
        return this.send(tslib_1.__assign({}, options, {
          method: "GET",
          url: url
        }));
      };

      HttpClient.prototype.post = function (url, options) {
        return this.send(tslib_1.__assign({}, options, {
          method: "POST",
          url: url
        }));
      };

      HttpClient.prototype["delete"] = function (url, options) {
        return this.send(tslib_1.__assign({}, options, {
          method: "DELETE",
          url: url
        }));
      };

      return HttpClient;
    }();

    exports.HttpClient = HttpClient;

    var DefaultHttpClient = function (_super) {
      tslib_1.__extends(DefaultHttpClient, _super);

      function DefaultHttpClient(logger) {
        var _this = _super.call(this) || this;

        _this.logger = logger;
        return _this;
      }

      DefaultHttpClient.prototype.send = function (request) {
        var _this = this;

        return new Promise(function (resolve, reject) {
          var xhr = new XMLHttpRequest();
          xhr.open(request.method, request.url, true);
          xhr.withCredentials = true;
          xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
          xhr.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");

          if (request.headers) {
            Object.keys(request.headers).forEach(function (header) {
              return xhr.setRequestHeader(header, request.headers[header]);
            });
          }

          if (request.responseType) {
            xhr.responseType = request.responseType;
          }

          if (request.abortSignal) {
            request.abortSignal.onabort = function () {
              xhr.abort();
            };
          }

          if (request.timeout) {
            xhr.timeout = request.timeout;
          }

          xhr.onload = function () {
            if (request.abortSignal) {
              request.abortSignal.onabort = null;
            }

            if (xhr.status >= 200 && xhr.status < 300) {
              resolve(new HttpResponse(xhr.status, xhr.statusText, xhr.response || xhr.responseText));
            } else {
              reject(new Errors.HttpError(xhr.statusText, xhr.status));
            }
          };

          xhr.onerror = function () {
            _this.logger.log(ILogger.LogLevel.Warning, "Error from HTTP request. " + xhr.status + ": " + xhr.statusText);

            reject(new Errors.HttpError(xhr.statusText, xhr.status));
          };

          xhr.ontimeout = function () {
            _this.logger.log(ILogger.LogLevel.Warning, "Timeout from HTTP request.");

            reject(new Errors.TimeoutError());
          };

          xhr.send(request.content || "");
        });
      };

      return DefaultHttpClient;
    }(HttpClient);

    exports.DefaultHttpClient = DefaultHttpClient;
  });
  unwrapExports(HttpClient_1);
  var HttpClient_2 = HttpClient_1.HttpResponse;
  var HttpClient_3 = HttpClient_1.HttpClient;
  var HttpClient_4 = HttpClient_1.DefaultHttpClient;
  var TextMessageFormat_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var TextMessageFormat = function () {
      function TextMessageFormat() {}

      TextMessageFormat.write = function (output) {
        return "" + output + TextMessageFormat.RecordSeparator;
      };

      TextMessageFormat.parse = function (input) {
        if (input[input.length - 1] !== TextMessageFormat.RecordSeparator) {
          throw new Error("Message is incomplete.");
        }

        var messages = input.split(TextMessageFormat.RecordSeparator);
        messages.pop();
        return messages;
      };

      TextMessageFormat.RecordSeparatorCode = 30;
      TextMessageFormat.RecordSeparator = String.fromCharCode(TextMessageFormat.RecordSeparatorCode);
      return TextMessageFormat;
    }();

    exports.TextMessageFormat = TextMessageFormat;
  });
  unwrapExports(TextMessageFormat_1);
  var TextMessageFormat_2 = TextMessageFormat_1.TextMessageFormat;
  var HandshakeProtocol_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var HandshakeProtocol = function () {
      function HandshakeProtocol() {}

      HandshakeProtocol.prototype.writeHandshakeRequest = function (handshakeRequest) {
        return TextMessageFormat_1.TextMessageFormat.write(JSON.stringify(handshakeRequest));
      };

      HandshakeProtocol.prototype.parseHandshakeResponse = function (data) {
        var responseMessage;
        var messageData;
        var remainingData;

        if (data instanceof ArrayBuffer) {
          var binaryData = new Uint8Array(data);
          var separatorIndex = binaryData.indexOf(TextMessageFormat_1.TextMessageFormat.RecordSeparatorCode);

          if (separatorIndex === -1) {
            throw new Error("Message is incomplete.");
          }

          var responseLength = separatorIndex + 1;
          messageData = String.fromCharCode.apply(null, binaryData.slice(0, responseLength));
          remainingData = binaryData.byteLength > responseLength ? binaryData.slice(responseLength).buffer : null;
        } else {
          var textData = data;
          var separatorIndex = textData.indexOf(TextMessageFormat_1.TextMessageFormat.RecordSeparator);

          if (separatorIndex === -1) {
            throw new Error("Message is incomplete.");
          }

          var responseLength = separatorIndex + 1;
          messageData = textData.substring(0, responseLength);
          remainingData = textData.length > responseLength ? textData.substring(responseLength) : null;
        }

        var messages = TextMessageFormat_1.TextMessageFormat.parse(messageData);
        responseMessage = JSON.parse(messages[0]);
        return [remainingData, responseMessage];
      };

      return HandshakeProtocol;
    }();

    exports.HandshakeProtocol = HandshakeProtocol;
  });
  unwrapExports(HandshakeProtocol_1);
  var HandshakeProtocol_2 = HandshakeProtocol_1.HandshakeProtocol;
  var IHubProtocol = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var MessageType;

    (function (MessageType) {
      MessageType[MessageType["Invocation"] = 1] = "Invocation";
      MessageType[MessageType["StreamItem"] = 2] = "StreamItem";
      MessageType[MessageType["Completion"] = 3] = "Completion";
      MessageType[MessageType["StreamInvocation"] = 4] = "StreamInvocation";
      MessageType[MessageType["CancelInvocation"] = 5] = "CancelInvocation";
      MessageType[MessageType["Ping"] = 6] = "Ping";
      MessageType[MessageType["Close"] = 7] = "Close";
    })(MessageType = exports.MessageType || (exports.MessageType = {}));
  });
  unwrapExports(IHubProtocol);
  var IHubProtocol_1 = IHubProtocol.MessageType;
  var Loggers = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var NullLogger = function () {
      function NullLogger() {}

      NullLogger.prototype.log = function (logLevel, message) {};

      NullLogger.instance = new NullLogger();
      return NullLogger;
    }();

    exports.NullLogger = NullLogger;
  });
  unwrapExports(Loggers);
  var Loggers_1 = Loggers.NullLogger;
  var Utils = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var Arg = function () {
      function Arg() {}

      Arg.isRequired = function (val, name) {
        if (val === null || val === undefined) {
          throw new Error("The '" + name + "' argument is required.");
        }
      };

      Arg.isIn = function (val, values, name) {
        if (!(val in values)) {
          throw new Error("Unknown " + name + " value: " + val + ".");
        }
      };

      return Arg;
    }();

    exports.Arg = Arg;

    function getDataDetail(data, includeContent) {
      var length = null;

      if (data instanceof ArrayBuffer) {
        length = "Binary data of length " + data.byteLength;

        if (includeContent) {
          length += ". Content: '" + formatArrayBuffer(data) + "'";
        }
      } else if (typeof data === "string") {
        length = "String data of length " + data.length;

        if (includeContent) {
          length += ". Content: '" + data + "'.";
        }
      }

      return length;
    }

    exports.getDataDetail = getDataDetail;

    function formatArrayBuffer(data) {
      var view = new Uint8Array(data);
      var str = "";
      view.forEach(function (num) {
        var pad = num < 16 ? "0" : "";
        str += "0x" + pad + num.toString(16) + " ";
      });
      return str.substr(0, str.length - 1);
    }

    exports.formatArrayBuffer = formatArrayBuffer;

    function sendMessage(logger, transportName, httpClient, url, accessTokenFactory, content, logMessageContent) {
      return tslib_1.__awaiter(this, void 0, void 0, function () {
        var headers, token, response, _a;

        return tslib_1.__generator(this, function (_b) {
          switch (_b.label) {
            case 0:
              return [4, accessTokenFactory()];

            case 1:
              token = _b.sent();

              if (token) {
                headers = (_a = {}, _a["Authorization"] = "Bearer " + token, _a);
              }

              logger.log(ILogger.LogLevel.Trace, "(" + transportName + " transport) sending data. " + getDataDetail(content, logMessageContent) + ".");
              return [4, httpClient.post(url, {
                content: content,
                headers: headers
              })];

            case 2:
              response = _b.sent();
              logger.log(ILogger.LogLevel.Trace, "(" + transportName + " transport) request complete. Response status: " + response.statusCode + ".");
              return [2];
          }
        });
      });
    }

    exports.sendMessage = sendMessage;

    function createLogger(logger) {
      if (logger === undefined) {
        return new ConsoleLogger(ILogger.LogLevel.Information);
      }

      if (logger === null) {
        return Loggers.NullLogger.instance;
      }

      if (logger.log) {
        return logger;
      }

      return new ConsoleLogger(logger);
    }

    exports.createLogger = createLogger;

    var Subject = function () {
      function Subject(cancelCallback) {
        this.observers = [];
        this.cancelCallback = cancelCallback;
      }

      Subject.prototype.next = function (item) {
        for (var _i = 0, _a = this.observers; _i < _a.length; _i++) {
          var observer = _a[_i];
          observer.next(item);
        }
      };

      Subject.prototype.error = function (err) {
        for (var _i = 0, _a = this.observers; _i < _a.length; _i++) {
          var observer = _a[_i];

          if (observer.error) {
            observer.error(err);
          }
        }
      };

      Subject.prototype.complete = function () {
        for (var _i = 0, _a = this.observers; _i < _a.length; _i++) {
          var observer = _a[_i];

          if (observer.complete) {
            observer.complete();
          }
        }
      };

      Subject.prototype.subscribe = function (observer) {
        this.observers.push(observer);
        return new SubjectSubscription(this, observer);
      };

      return Subject;
    }();

    exports.Subject = Subject;

    var SubjectSubscription = function () {
      function SubjectSubscription(subject, observer) {
        this.subject = subject;
        this.observer = observer;
      }

      SubjectSubscription.prototype.dispose = function () {
        var index = this.subject.observers.indexOf(this.observer);

        if (index > -1) {
          this.subject.observers.splice(index, 1);
        }

        if (this.subject.observers.length === 0) {
          this.subject.cancelCallback()["catch"](function (_) {});
        }
      };

      return SubjectSubscription;
    }();

    exports.SubjectSubscription = SubjectSubscription;

    var ConsoleLogger = function () {
      function ConsoleLogger(minimumLogLevel) {
        this.minimumLogLevel = minimumLogLevel;
      }

      ConsoleLogger.prototype.log = function (logLevel, message) {
        if (logLevel >= this.minimumLogLevel) {
          switch (logLevel) {
            case ILogger.LogLevel.Critical:
            case ILogger.LogLevel.Error:
              console.error(ILogger.LogLevel[logLevel] + ": " + message);
              break;

            case ILogger.LogLevel.Warning:
              console.warn(ILogger.LogLevel[logLevel] + ": " + message);
              break;

            case ILogger.LogLevel.Information:
              console.info(ILogger.LogLevel[logLevel] + ": " + message);
              break;

            default:
              console.log(ILogger.LogLevel[logLevel] + ": " + message);
              break;
          }
        }
      };

      return ConsoleLogger;
    }();

    exports.ConsoleLogger = ConsoleLogger;
  });
  unwrapExports(Utils);
  var Utils_1 = Utils.Arg;
  var Utils_2 = Utils.getDataDetail;
  var Utils_3 = Utils.formatArrayBuffer;
  var Utils_4 = Utils.sendMessage;
  var Utils_5 = Utils.createLogger;
  var Utils_6 = Utils.Subject;
  var Utils_7 = Utils.SubjectSubscription;
  var Utils_8 = Utils.ConsoleLogger;
  var HubConnection_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var DEFAULT_TIMEOUT_IN_MS = 30 * 1e3;

    var HubConnection = function () {
      function HubConnection(connection, logger, protocol) {
        var _this = this;

        Utils.Arg.isRequired(connection, "connection");
        Utils.Arg.isRequired(logger, "logger");
        Utils.Arg.isRequired(protocol, "protocol");
        this.serverTimeoutInMilliseconds = DEFAULT_TIMEOUT_IN_MS;
        this.logger = logger;
        this.protocol = protocol;
        this.connection = connection;
        this.handshakeProtocol = new HandshakeProtocol_1.HandshakeProtocol();

        this.connection.onreceive = function (data) {
          return _this.processIncomingData(data);
        };

        this.connection.onclose = function (error) {
          return _this.connectionClosed(error);
        };

        this.callbacks = {};
        this.methods = {};
        this.closedCallbacks = [];
        this.id = 0;
      }

      HubConnection.create = function (connection, logger, protocol) {
        return new HubConnection(connection, logger, protocol);
      };

      HubConnection.prototype.start = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var handshakeRequest;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                handshakeRequest = {
                  protocol: this.protocol.name,
                  version: this.protocol.version
                };
                this.logger.log(ILogger.LogLevel.Debug, "Starting HubConnection.");
                this.receivedHandshakeResponse = false;
                return [4, this.connection.start(this.protocol.transferFormat)];

              case 1:
                _a.sent();

                this.logger.log(ILogger.LogLevel.Debug, "Sending handshake request.");
                return [4, this.connection.send(this.handshakeProtocol.writeHandshakeRequest(handshakeRequest))];

              case 2:
                _a.sent();

                this.logger.log(ILogger.LogLevel.Information, "Using HubProtocol '" + this.protocol.name + "'.");
                this.cleanupTimeout();
                this.configureTimeout();
                return [2];
            }
          });
        });
      };

      HubConnection.prototype.stop = function () {
        this.logger.log(ILogger.LogLevel.Debug, "Stopping HubConnection.");
        this.cleanupTimeout();
        return this.connection.stop();
      };

      HubConnection.prototype.stream = function (methodName) {
        var _this = this;

        var args = [];

        for (var _i = 1; _i < arguments.length; _i++) {
          args[_i - 1] = arguments[_i];
        }

        var invocationDescriptor = this.createStreamInvocation(methodName, args);
        var subject = new Utils.Subject(function () {
          var cancelInvocation = _this.createCancelInvocation(invocationDescriptor.invocationId);

          var cancelMessage = _this.protocol.writeMessage(cancelInvocation);

          delete _this.callbacks[invocationDescriptor.invocationId];
          return _this.connection.send(cancelMessage);
        });

        this.callbacks[invocationDescriptor.invocationId] = function (invocationEvent, error) {
          if (error) {
            subject.error(error);
            return;
          }

          if (invocationEvent.type === IHubProtocol.MessageType.Completion) {
            if (invocationEvent.error) {
              subject.error(new Error(invocationEvent.error));
            } else {
              subject.complete();
            }
          } else {
            subject.next(invocationEvent.item);
          }
        };

        var message = this.protocol.writeMessage(invocationDescriptor);
        this.connection.send(message)["catch"](function (e) {
          subject.error(e);
          delete _this.callbacks[invocationDescriptor.invocationId];
        });
        return subject;
      };

      HubConnection.prototype.send = function (methodName) {
        var args = [];

        for (var _i = 1; _i < arguments.length; _i++) {
          args[_i - 1] = arguments[_i];
        }

        var invocationDescriptor = this.createInvocation(methodName, args, true);
        var message = this.protocol.writeMessage(invocationDescriptor);
        return this.connection.send(message);
      };

      HubConnection.prototype.invoke = function (methodName) {
        var _this = this;

        var args = [];

        for (var _i = 1; _i < arguments.length; _i++) {
          args[_i - 1] = arguments[_i];
        }

        var invocationDescriptor = this.createInvocation(methodName, args, false);
        var p = new Promise(function (resolve, reject) {
          _this.callbacks[invocationDescriptor.invocationId] = function (invocationEvent, error) {
            if (error) {
              reject(error);
              return;
            }

            if (invocationEvent.type === IHubProtocol.MessageType.Completion) {
              var completionMessage = invocationEvent;

              if (completionMessage.error) {
                reject(new Error(completionMessage.error));
              } else {
                resolve(completionMessage.result);
              }
            } else {
              reject(new Error("Unexpected message type: " + invocationEvent.type));
            }
          };

          var message = _this.protocol.writeMessage(invocationDescriptor);

          _this.connection.send(message)["catch"](function (e) {
            reject(e);
            delete _this.callbacks[invocationDescriptor.invocationId];
          });
        });
        return p;
      };

      HubConnection.prototype.on = function (methodName, newMethod) {
        if (!methodName || !newMethod) {
          return;
        }

        methodName = methodName.toLowerCase();

        if (!this.methods[methodName]) {
          this.methods[methodName] = [];
        }

        if (this.methods[methodName].indexOf(newMethod) !== -1) {
          return;
        }

        this.methods[methodName].push(newMethod);
      };

      HubConnection.prototype.off = function (methodName, method) {
        if (!methodName) {
          return;
        }

        methodName = methodName.toLowerCase();
        var handlers = this.methods[methodName];

        if (!handlers) {
          return;
        }

        if (method) {
          var removeIdx = handlers.indexOf(method);

          if (removeIdx !== -1) {
            handlers.splice(removeIdx, 1);

            if (handlers.length === 0) {
              delete this.methods[methodName];
            }
          }
        } else {
          delete this.methods[methodName];
        }
      };

      HubConnection.prototype.onclose = function (callback) {
        if (callback) {
          this.closedCallbacks.push(callback);
        }
      };

      HubConnection.prototype.processIncomingData = function (data) {
        this.cleanupTimeout();

        if (!this.receivedHandshakeResponse) {
          data = this.processHandshakeResponse(data);
          this.receivedHandshakeResponse = true;
        }

        if (data) {
          var messages = this.protocol.parseMessages(data, this.logger);

          for (var _i = 0, messages_1 = messages; _i < messages_1.length; _i++) {
            var message = messages_1[_i];

            switch (message.type) {
              case IHubProtocol.MessageType.Invocation:
                this.invokeClientMethod(message);
                break;

              case IHubProtocol.MessageType.StreamItem:
              case IHubProtocol.MessageType.Completion:
                var callback = this.callbacks[message.invocationId];

                if (callback != null) {
                  if (message.type === IHubProtocol.MessageType.Completion) {
                    delete this.callbacks[message.invocationId];
                  }

                  callback(message);
                }

                break;

              case IHubProtocol.MessageType.Ping:
                break;

              case IHubProtocol.MessageType.Close:
                this.logger.log(ILogger.LogLevel.Information, "Close message received from server.");
                this.connection.stop(message.error ? new Error("Server returned an error on close: " + message.error) : null);
                break;

              default:
                this.logger.log(ILogger.LogLevel.Warning, "Invalid message type: " + message.type);
                break;
            }
          }
        }

        this.configureTimeout();
      };

      HubConnection.prototype.processHandshakeResponse = function (data) {
        var responseMessage;
        var remainingData;

        try {
          _a = this.handshakeProtocol.parseHandshakeResponse(data), remainingData = _a[0], responseMessage = _a[1];
        } catch (e) {
          var message = "Error parsing handshake response: " + e;
          this.logger.log(ILogger.LogLevel.Error, message);
          var error = new Error(message);
          this.connection.stop(error);
          throw error;
        }

        if (responseMessage.error) {
          var message = "Server returned handshake error: " + responseMessage.error;
          this.logger.log(ILogger.LogLevel.Error, message);
          this.connection.stop(new Error(message));
        } else {
          this.logger.log(ILogger.LogLevel.Debug, "Server handshake complete.");
        }

        return remainingData;

        var _a;
      };

      HubConnection.prototype.configureTimeout = function () {
        var _this = this;

        if (!this.connection.features || !this.connection.features.inherentKeepAlive) {
          this.timeoutHandle = setTimeout(function () {
            return _this.serverTimeout();
          }, this.serverTimeoutInMilliseconds);
        }
      };

      HubConnection.prototype.serverTimeout = function () {
        this.connection.stop(new Error("Server timeout elapsed without receiving a message from the server."));
      };

      HubConnection.prototype.invokeClientMethod = function (invocationMessage) {
        var _this = this;

        var methods = this.methods[invocationMessage.target.toLowerCase()];

        if (methods) {
          methods.forEach(function (m) {
            return m.apply(_this, invocationMessage.arguments);
          });

          if (invocationMessage.invocationId) {
            var message = "Server requested a response, which is not supported in this version of the client.";
            this.logger.log(ILogger.LogLevel.Error, message);
            this.connection.stop(new Error(message));
          }
        } else {
          this.logger.log(ILogger.LogLevel.Warning, "No client method with the name '" + invocationMessage.target + "' found.");
        }
      };

      HubConnection.prototype.connectionClosed = function (error) {
        var _this = this;

        var callbacks = this.callbacks;
        this.callbacks = {};
        Object.keys(callbacks).forEach(function (key) {
          var callback = callbacks[key];
          callback(undefined, error ? error : new Error("Invocation canceled due to connection being closed."));
        });
        this.cleanupTimeout();
        this.closedCallbacks.forEach(function (c) {
          return c.apply(_this, [error]);
        });
      };

      HubConnection.prototype.cleanupTimeout = function () {
        if (this.timeoutHandle) {
          clearTimeout(this.timeoutHandle);
        }
      };

      HubConnection.prototype.createInvocation = function (methodName, args, nonblocking) {
        if (nonblocking) {
          return {
            arguments: args,
            target: methodName,
            type: IHubProtocol.MessageType.Invocation
          };
        } else {
          var id = this.id;
          this.id++;
          return {
            arguments: args,
            invocationId: id.toString(),
            target: methodName,
            type: IHubProtocol.MessageType.Invocation
          };
        }
      };

      HubConnection.prototype.createStreamInvocation = function (methodName, args) {
        var id = this.id;
        this.id++;
        return {
          arguments: args,
          invocationId: id.toString(),
          target: methodName,
          type: IHubProtocol.MessageType.StreamInvocation
        };
      };

      HubConnection.prototype.createCancelInvocation = function (id) {
        return {
          invocationId: id,
          type: IHubProtocol.MessageType.CancelInvocation
        };
      };

      return HubConnection;
    }();

    exports.HubConnection = HubConnection;
  });
  unwrapExports(HubConnection_1);
  var HubConnection_2 = HubConnection_1.HubConnection;
  var ITransport = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var HttpTransportType;

    (function (HttpTransportType) {
      HttpTransportType[HttpTransportType["None"] = 0] = "None";
      HttpTransportType[HttpTransportType["WebSockets"] = 1] = "WebSockets";
      HttpTransportType[HttpTransportType["ServerSentEvents"] = 2] = "ServerSentEvents";
      HttpTransportType[HttpTransportType["LongPolling"] = 4] = "LongPolling";
    })(HttpTransportType = exports.HttpTransportType || (exports.HttpTransportType = {}));

    var TransferFormat;

    (function (TransferFormat) {
      TransferFormat[TransferFormat["Text"] = 1] = "Text";
      TransferFormat[TransferFormat["Binary"] = 2] = "Binary";
    })(TransferFormat = exports.TransferFormat || (exports.TransferFormat = {}));
  });
  unwrapExports(ITransport);
  var ITransport_1 = ITransport.HttpTransportType;
  var ITransport_2 = ITransport.TransferFormat;
  var AbortController_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var AbortController = function () {
      function AbortController() {
        this.isAborted = false;
      }

      AbortController.prototype.abort = function () {
        if (!this.isAborted) {
          this.isAborted = true;

          if (this.onabort) {
            this.onabort();
          }
        }
      };

      Object.defineProperty(AbortController.prototype, "signal", {
        get: function get() {
          return this;
        },
        enumerable: true,
        configurable: true
      });
      Object.defineProperty(AbortController.prototype, "aborted", {
        get: function get() {
          return this.isAborted;
        },
        enumerable: true,
        configurable: true
      });
      return AbortController;
    }();

    exports.AbortController = AbortController;
  });
  unwrapExports(AbortController_1);
  var AbortController_2 = AbortController_1.AbortController;
  var LongPollingTransport_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var SHUTDOWN_TIMEOUT = 5 * 1e3;

    var LongPollingTransport = function () {
      function LongPollingTransport(httpClient, accessTokenFactory, logger, logMessageContent, shutdownTimeout) {
        this.httpClient = httpClient;

        this.accessTokenFactory = accessTokenFactory || function () {
          return null;
        };

        this.logger = logger;
        this.pollAbort = new AbortController_1.AbortController();
        this.logMessageContent = logMessageContent;
        this.shutdownTimeout = shutdownTimeout || SHUTDOWN_TIMEOUT;
      }

      Object.defineProperty(LongPollingTransport.prototype, "pollAborted", {
        get: function get() {
          return this.pollAbort.aborted;
        },
        enumerable: true,
        configurable: true
      });

      LongPollingTransport.prototype.connect = function (url, transferFormat) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var pollOptions, token, closeError, pollUrl, response;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                Utils.Arg.isRequired(url, "url");
                Utils.Arg.isRequired(transferFormat, "transferFormat");
                Utils.Arg.isIn(transferFormat, ITransport.TransferFormat, "transferFormat");
                this.url = url;
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Connecting");

                if (transferFormat === ITransport.TransferFormat.Binary && typeof new XMLHttpRequest().responseType !== "string") {
                  throw new Error("Binary protocols over XmlHttpRequest not implementing advanced features are not supported.");
                }

                pollOptions = {
                  abortSignal: this.pollAbort.signal,
                  headers: {},
                  timeout: 9e4
                };

                if (transferFormat === ITransport.TransferFormat.Binary) {
                  pollOptions.responseType = "arraybuffer";
                }

                return [4, this.accessTokenFactory()];

              case 1:
                token = _a.sent();
                this.updateHeaderToken(pollOptions, token);
                pollUrl = url + "&_=" + Date.now();
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) polling: " + pollUrl);
                return [4, this.httpClient.get(pollUrl, pollOptions)];

              case 2:
                response = _a.sent();

                if (response.statusCode !== 200) {
                  this.logger.log(ILogger.LogLevel.Error, "(LongPolling transport) Unexpected response code: " + response.statusCode);
                  closeError = new Errors.HttpError(response.statusText, response.statusCode);
                  this.running = false;
                } else {
                  this.running = true;
                }

                this.poll(this.url, pollOptions, closeError);
                return [2, Promise.resolve()];
            }
          });
        });
      };

      LongPollingTransport.prototype.updateHeaderToken = function (request, token) {
        if (token) {
          request.headers["Authorization"] = "Bearer " + token;
          return;
        }

        if (request.headers["Authorization"]) {
          delete request.headers["Authorization"];
        }
      };

      LongPollingTransport.prototype.poll = function (url, pollOptions, closeError) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var token, pollUrl, response, e_1;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                _a.trys.push([0,, 8, 9]);

                _a.label = 1;

              case 1:
                if (!this.running) return [3, 7];
                return [4, this.accessTokenFactory()];

              case 2:
                token = _a.sent();
                this.updateHeaderToken(pollOptions, token);
                _a.label = 3;

              case 3:
                _a.trys.push([3, 5,, 6]);

                pollUrl = url + "&_=" + Date.now();
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) polling: " + pollUrl);
                return [4, this.httpClient.get(pollUrl, pollOptions)];

              case 4:
                response = _a.sent();

                if (response.statusCode === 204) {
                  this.logger.log(ILogger.LogLevel.Information, "(LongPolling transport) Poll terminated by server");
                  this.running = false;
                } else if (response.statusCode !== 200) {
                  this.logger.log(ILogger.LogLevel.Error, "(LongPolling transport) Unexpected response code: " + response.statusCode);
                  closeError = new Errors.HttpError(response.statusText, response.statusCode);
                  this.running = false;
                } else {
                  if (response.content) {
                    this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) data received. " + Utils.getDataDetail(response.content, this.logMessageContent));

                    if (this.onreceive) {
                      this.onreceive(response.content);
                    }
                  } else {
                    this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Poll timed out, reissuing.");
                  }
                }

                return [3, 6];

              case 5:
                e_1 = _a.sent();

                if (!this.running) {
                  this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Poll errored after shutdown: " + e_1.message);
                } else {
                  if (e_1 instanceof Errors.TimeoutError) {
                    this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Poll timed out, reissuing.");
                  } else {
                    closeError = e_1;
                    this.running = false;
                  }
                }

                return [3, 6];

              case 6:
                return [3, 1];

              case 7:
                return [3, 9];

              case 8:
                this.stopped = true;

                if (this.shutdownTimer) {
                  clearTimeout(this.shutdownTimer);
                }

                if (this.onclose) {
                  this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Firing onclose event. Error: " + (closeError || "<undefined>"));
                  this.onclose(closeError);
                }

                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) Transport finished.");
                return [7];

              case 9:
                return [2];
            }
          });
        });
      };

      LongPollingTransport.prototype.send = function (data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          return tslib_1.__generator(this, function (_a) {
            if (!this.running) {
              return [2, Promise.reject(new Error("Cannot send until the transport is connected"))];
            }

            return [2, Utils.sendMessage(this.logger, "LongPolling", this.httpClient, this.url, this.accessTokenFactory, data, this.logMessageContent)];
          });
        });
      };

      LongPollingTransport.prototype.stop = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var _this = this;

          var deleteOptions, token, response;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                _a.trys.push([0,, 3, 4]);

                this.running = false;
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) sending DELETE request to " + this.url + ".");
                deleteOptions = {
                  headers: {}
                };
                return [4, this.accessTokenFactory()];

              case 1:
                token = _a.sent();
                this.updateHeaderToken(deleteOptions, token);
                return [4, this.httpClient["delete"](this.url, deleteOptions)];

              case 2:
                response = _a.sent();
                this.logger.log(ILogger.LogLevel.Trace, "(LongPolling transport) DELETE request accepted.");
                return [3, 4];

              case 3:
                if (!this.stopped) {
                  this.shutdownTimer = setTimeout(function () {
                    _this.logger.log(ILogger.LogLevel.Warning, "(LongPolling transport) server did not terminate after DELETE request, canceling poll.");

                    _this.pollAbort.abort();
                  }, this.shutdownTimeout);
                }

                return [7];

              case 4:
                return [2];
            }
          });
        });
      };

      return LongPollingTransport;
    }();

    exports.LongPollingTransport = LongPollingTransport;
  });
  unwrapExports(LongPollingTransport_1);
  var LongPollingTransport_2 = LongPollingTransport_1.LongPollingTransport;
  var ServerSentEventsTransport_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var ServerSentEventsTransport = function () {
      function ServerSentEventsTransport(httpClient, accessTokenFactory, logger, logMessageContent) {
        this.httpClient = httpClient;

        this.accessTokenFactory = accessTokenFactory || function () {
          return null;
        };

        this.logger = logger;
        this.logMessageContent = logMessageContent;
      }

      ServerSentEventsTransport.prototype.connect = function (url, transferFormat) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var _this = this;

          var token;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                Utils.Arg.isRequired(url, "url");
                Utils.Arg.isRequired(transferFormat, "transferFormat");
                Utils.Arg.isIn(transferFormat, ITransport.TransferFormat, "transferFormat");

                if (typeof EventSource === "undefined") {
                  throw new Error("'EventSource' is not supported in your environment.");
                }

                this.logger.log(ILogger.LogLevel.Trace, "(SSE transport) Connecting");
                return [4, this.accessTokenFactory()];

              case 1:
                token = _a.sent();

                if (token) {
                  url += (url.indexOf("?") < 0 ? "?" : "&") + ("access_token=" + encodeURIComponent(token));
                }

                this.url = url;
                return [2, new Promise(function (resolve, reject) {
                  var opened = false;

                  if (transferFormat !== ITransport.TransferFormat.Text) {
                    reject(new Error("The Server-Sent Events transport only supports the 'Text' transfer format"));
                  }

                  var eventSource = new EventSource(url, {
                    withCredentials: true
                  });

                  try {
                    eventSource.onmessage = function (e) {
                      if (_this.onreceive) {
                        try {
                          _this.logger.log(ILogger.LogLevel.Trace, "(SSE transport) data received. " + Utils.getDataDetail(e.data, _this.logMessageContent) + ".");

                          _this.onreceive(e.data);
                        } catch (error) {
                          if (_this.onclose) {
                            _this.onclose(error);
                          }

                          return;
                        }
                      }
                    };

                    eventSource.onerror = function (e) {
                      var error = new Error(e.message || "Error occurred");

                      if (opened) {
                        _this.close(error);
                      } else {
                        reject(error);
                      }
                    };

                    eventSource.onopen = function () {
                      _this.logger.log(ILogger.LogLevel.Information, "SSE connected to " + _this.url);

                      _this.eventSource = eventSource;
                      opened = true;
                      resolve();
                    };
                  } catch (e) {
                    return Promise.reject(e);
                  }
                })];
            }
          });
        });
      };

      ServerSentEventsTransport.prototype.send = function (data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          return tslib_1.__generator(this, function (_a) {
            if (!this.eventSource) {
              return [2, Promise.reject(new Error("Cannot send until the transport is connected"))];
            }

            return [2, Utils.sendMessage(this.logger, "SSE", this.httpClient, this.url, this.accessTokenFactory, data, this.logMessageContent)];
          });
        });
      };

      ServerSentEventsTransport.prototype.stop = function () {
        this.close();
        return Promise.resolve();
      };

      ServerSentEventsTransport.prototype.close = function (e) {
        if (this.eventSource) {
          this.eventSource.close();
          this.eventSource = null;

          if (this.onclose) {
            this.onclose(e);
          }
        }
      };

      return ServerSentEventsTransport;
    }();

    exports.ServerSentEventsTransport = ServerSentEventsTransport;
  });
  unwrapExports(ServerSentEventsTransport_1);
  var ServerSentEventsTransport_2 = ServerSentEventsTransport_1.ServerSentEventsTransport;
  var WebSocketTransport_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var WebSocketTransport = function () {
      function WebSocketTransport(accessTokenFactory, logger, logMessageContent) {
        this.logger = logger;

        this.accessTokenFactory = accessTokenFactory || function () {
          return null;
        };

        this.logMessageContent = logMessageContent;
      }

      WebSocketTransport.prototype.connect = function (url, transferFormat) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var _this = this;

          var token;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                Utils.Arg.isRequired(url, "url");
                Utils.Arg.isRequired(transferFormat, "transferFormat");
                Utils.Arg.isIn(transferFormat, ITransport.TransferFormat, "transferFormat");

                if (typeof WebSocket === "undefined") {
                  throw new Error("'WebSocket' is not supported in your environment.");
                }

                this.logger.log(ILogger.LogLevel.Trace, "(WebSockets transport) Connecting");
                return [4, this.accessTokenFactory()];

              case 1:
                token = _a.sent();

                if (token) {
                  url += (url.indexOf("?") < 0 ? "?" : "&") + ("access_token=" + encodeURIComponent(token));
                }

                return [2, new Promise(function (resolve, reject) {
                  url = url.replace(/^http/, "ws");
                  var webSocket = new WebSocket(url);

                  if (transferFormat === ITransport.TransferFormat.Binary) {
                    webSocket.binaryType = "arraybuffer";
                  }

                  webSocket.onopen = function (event) {
                    _this.logger.log(ILogger.LogLevel.Information, "WebSocket connected to " + url);

                    _this.webSocket = webSocket;
                    resolve();
                  };

                  webSocket.onerror = function (event) {
                    reject(event.error);
                  };

                  webSocket.onmessage = function (message) {
                    _this.logger.log(ILogger.LogLevel.Trace, "(WebSockets transport) data received. " + Utils.getDataDetail(message.data, _this.logMessageContent) + ".");

                    if (_this.onreceive) {
                      _this.onreceive(message.data);
                    }
                  };

                  webSocket.onclose = function (event) {
                    _this.logger.log(ILogger.LogLevel.Trace, "(WebSockets transport) socket closed.");

                    if (_this.onclose) {
                      if (event.wasClean === false || event.code !== 1e3) {
                        _this.onclose(new Error("Websocket closed with status code: " + event.code + " (" + event.reason + ")"));
                      } else {
                        _this.onclose();
                      }
                    }
                  };
                })];
            }
          });
        });
      };

      WebSocketTransport.prototype.send = function (data) {
        if (this.webSocket && this.webSocket.readyState === WebSocket.OPEN) {
          this.logger.log(ILogger.LogLevel.Trace, "(WebSockets transport) sending data. " + Utils.getDataDetail(data, this.logMessageContent) + ".");
          this.webSocket.send(data);
          return Promise.resolve();
        }

        return Promise.reject("WebSocket is not in the OPEN state");
      };

      WebSocketTransport.prototype.stop = function () {
        if (this.webSocket) {
          this.webSocket.close();
          this.webSocket = null;
        }

        return Promise.resolve();
      };

      return WebSocketTransport;
    }();

    exports.WebSocketTransport = WebSocketTransport;
  });
  unwrapExports(WebSocketTransport_1);
  var WebSocketTransport_2 = WebSocketTransport_1.WebSocketTransport;
  var HttpConnection_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var MAX_REDIRECTS = 100;

    var HttpConnection = function () {
      function HttpConnection(url, options) {
        if (options === void 0) {
          options = {};
        }

        this.features = {};
        Utils.Arg.isRequired(url, "url");
        this.logger = Utils.createLogger(options.logger);
        this.baseUrl = this.resolveUrl(url);
        options = options || {};

        options.accessTokenFactory = options.accessTokenFactory || function () {
          return null;
        };

        options.logMessageContent = options.logMessageContent || false;
        this.httpClient = options.httpClient || new HttpClient_1.DefaultHttpClient(this.logger);
        this.connectionState = 2;
        this.options = options;
      }

      HttpConnection.prototype.start = function (transferFormat) {
        transferFormat = transferFormat || ITransport.TransferFormat.Binary;
        Utils.Arg.isIn(transferFormat, ITransport.TransferFormat, "transferFormat");
        this.logger.log(ILogger.LogLevel.Debug, "Starting connection with transfer format '" + ITransport.TransferFormat[transferFormat] + "'.");

        if (this.connectionState !== 2) {
          return Promise.reject(new Error("Cannot start a connection that is not in the 'Disconnected' state."));
        }

        this.connectionState = 0;
        this.startPromise = this.startInternal(transferFormat);
        return this.startPromise;
      };

      HttpConnection.prototype.send = function (data) {
        if (this.connectionState !== 1) {
          throw new Error("Cannot send data if the connection is not in the 'Connected' State.");
        }

        return this.transport.send(data);
      };

      HttpConnection.prototype.stop = function (error) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var e_1;
          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                this.connectionState = 2;
                _a.label = 1;

              case 1:
                _a.trys.push([1, 3,, 4]);

                return [4, this.startPromise];

              case 2:
                _a.sent();

                return [3, 4];

              case 3:
                e_1 = _a.sent();
                return [3, 4];

              case 4:
                if (!this.transport) return [3, 6];
                this.stopError = error;
                return [4, this.transport.stop()];

              case 5:
                _a.sent();

                this.transport = null;
                _a.label = 6;

              case 6:
                return [2];
            }
          });
        });
      };

      HttpConnection.prototype.startInternal = function (transferFormat) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var _this = this;

          var url, negotiateResponse, redirects, _loop_1, this_1, state_1, e_2;

          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                url = this.baseUrl;
                this.accessTokenFactory = this.options.accessTokenFactory;
                _a.label = 1;

              case 1:
                _a.trys.push([1, 12,, 13]);

                if (!this.options.skipNegotiation) return [3, 5];
                if (!(this.options.transport === ITransport.HttpTransportType.WebSockets)) return [3, 3];
                this.transport = this.constructTransport(ITransport.HttpTransportType.WebSockets);
                return [4, this.transport.connect(url, transferFormat)];

              case 2:
                _a.sent();

                return [3, 4];

              case 3:
                throw Error("Negotiation can only be skipped when using the WebSocket transport directly.");

              case 4:
                return [3, 11];

              case 5:
                negotiateResponse = null;
                redirects = 0;

                _loop_1 = function _loop_1() {
                  var accessToken_1;
                  return tslib_1.__generator(this, function (_a) {
                    switch (_a.label) {
                      case 0:
                        return [4, this_1.getNegotiationResponse(url)];

                      case 1:
                        negotiateResponse = _a.sent();

                        if (this_1.connectionState === 2) {
                          return [2, {
                            value: void 0
                          }];
                        }

                        if (negotiateResponse.url) {
                          url = negotiateResponse.url;
                        }

                        if (negotiateResponse.accessToken) {
                          accessToken_1 = negotiateResponse.accessToken;

                          this_1.accessTokenFactory = function () {
                            return accessToken_1;
                          };
                        }

                        redirects++;
                        return [2];
                    }
                  });
                };

                this_1 = this;
                _a.label = 6;

              case 6:
                return [5, _loop_1()];

              case 7:
                state_1 = _a.sent();
                if (_typeof(state_1) === "object") return [2, state_1.value];
                _a.label = 8;

              case 8:
                if (negotiateResponse.url && redirects < MAX_REDIRECTS) return [3, 6];
                _a.label = 9;

              case 9:
                if (redirects === MAX_REDIRECTS && negotiateResponse.url) {
                  throw Error("Negotiate redirection limit exceeded.");
                }

                return [4, this.createTransport(url, this.options.transport, negotiateResponse, transferFormat)];

              case 10:
                _a.sent();

                _a.label = 11;

              case 11:
                if (this.transport instanceof LongPollingTransport_1.LongPollingTransport) {
                  this.features.inherentKeepAlive = true;
                }

                this.transport.onreceive = this.onreceive;

                this.transport.onclose = function (e) {
                  return _this.stopConnection(e);
                };

                this.changeState(0, 1);
                return [3, 13];

              case 12:
                e_2 = _a.sent();
                this.logger.log(ILogger.LogLevel.Error, "Failed to start the connection: " + e_2);
                this.connectionState = 2;
                this.transport = null;
                throw e_2;

              case 13:
                return [2];
            }
          });
        });
      };

      HttpConnection.prototype.getNegotiationResponse = function (url) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var token, headers, negotiateUrl, response, e_3, _a;

          return tslib_1.__generator(this, function (_b) {
            switch (_b.label) {
              case 0:
                return [4, this.accessTokenFactory()];

              case 1:
                token = _b.sent();

                if (token) {
                  headers = (_a = {}, _a["Authorization"] = "Bearer " + token, _a);
                }

                negotiateUrl = this.resolveNegotiateUrl(url);
                this.logger.log(ILogger.LogLevel.Debug, "Sending negotiation request: " + negotiateUrl);
                _b.label = 2;

              case 2:
                _b.trys.push([2, 4,, 5]);

                return [4, this.httpClient.post(negotiateUrl, {
                  content: "",
                  headers: headers
                })];

              case 3:
                response = _b.sent();

                if (response.statusCode !== 200) {
                  throw Error("Unexpected status code returned from negotiate " + response.statusCode);
                }

                return [2, JSON.parse(response.content)];

              case 4:
                e_3 = _b.sent();
                this.logger.log(ILogger.LogLevel.Error, "Failed to complete negotiation with the server: " + e_3);
                throw e_3;

              case 5:
                return [2];
            }
          });
        });
      };

      HttpConnection.prototype.createConnectUrl = function (url, connectionId) {
        return url + (url.indexOf("?") === -1 ? "?" : "&") + ("id=" + connectionId);
      };

      HttpConnection.prototype.createTransport = function (url, requestedTransport, negotiateResponse, requestedTransferFormat) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          var connectUrl, transports, _i, transports_1, endpoint, transport, ex_1;

          return tslib_1.__generator(this, function (_a) {
            switch (_a.label) {
              case 0:
                connectUrl = this.createConnectUrl(url, negotiateResponse.connectionId);
                if (!this.isITransport(requestedTransport)) return [3, 2];
                this.logger.log(ILogger.LogLevel.Debug, "Connection was provided an instance of ITransport, using that directly.");
                this.transport = requestedTransport;
                return [4, this.transport.connect(connectUrl, requestedTransferFormat)];

              case 1:
                _a.sent();

                this.changeState(0, 1);
                return [2];

              case 2:
                transports = negotiateResponse.availableTransports;
                _i = 0, transports_1 = transports;
                _a.label = 3;

              case 3:
                if (!(_i < transports_1.length)) return [3, 9];
                endpoint = transports_1[_i];
                this.connectionState = 0;
                transport = this.resolveTransport(endpoint, requestedTransport, requestedTransferFormat);
                if (!(typeof transport === "number")) return [3, 8];
                this.transport = this.constructTransport(transport);
                if (!(negotiateResponse.connectionId === null)) return [3, 5];
                return [4, this.getNegotiationResponse(url)];

              case 4:
                negotiateResponse = _a.sent();
                connectUrl = this.createConnectUrl(url, negotiateResponse.connectionId);
                _a.label = 5;

              case 5:
                _a.trys.push([5, 7,, 8]);

                return [4, this.transport.connect(connectUrl, requestedTransferFormat)];

              case 6:
                _a.sent();

                this.changeState(0, 1);
                return [2];

              case 7:
                ex_1 = _a.sent();
                this.logger.log(ILogger.LogLevel.Error, "Failed to start the transport '" + ITransport.HttpTransportType[transport] + "': " + ex_1);
                this.connectionState = 2;
                negotiateResponse.connectionId = null;
                return [3, 8];

              case 8:
                _i++;
                return [3, 3];

              case 9:
                throw new Error("Unable to initialize any of the available transports.");
            }
          });
        });
      };

      HttpConnection.prototype.constructTransport = function (transport) {
        switch (transport) {
          case ITransport.HttpTransportType.WebSockets:
            return new WebSocketTransport_1.WebSocketTransport(this.accessTokenFactory, this.logger, this.options.logMessageContent);

          case ITransport.HttpTransportType.ServerSentEvents:
            return new ServerSentEventsTransport_1.ServerSentEventsTransport(this.httpClient, this.accessTokenFactory, this.logger, this.options.logMessageContent);

          case ITransport.HttpTransportType.LongPolling:
            return new LongPollingTransport_1.LongPollingTransport(this.httpClient, this.accessTokenFactory, this.logger, this.options.logMessageContent);

          default:
            throw new Error("Unknown transport: " + transport + ".");
        }
      };

      HttpConnection.prototype.resolveTransport = function (endpoint, requestedTransport, requestedTransferFormat) {
        var transport = ITransport.HttpTransportType[endpoint.transport];

        if (transport === null || transport === undefined) {
          this.logger.log(ILogger.LogLevel.Debug, "Skipping transport '" + endpoint.transport + "' because it is not supported by this client.");
        } else {
          var transferFormats = endpoint.transferFormats.map(function (s) {
            return ITransport.TransferFormat[s];
          });

          if (transportMatches(requestedTransport, transport)) {
            if (transferFormats.indexOf(requestedTransferFormat) >= 0) {
              if (transport === ITransport.HttpTransportType.WebSockets && typeof WebSocket === "undefined" || transport === ITransport.HttpTransportType.ServerSentEvents && typeof EventSource === "undefined") {
                this.logger.log(ILogger.LogLevel.Debug, "Skipping transport '" + ITransport.HttpTransportType[transport] + "' because it is not supported in your environment.'");
              } else {
                this.logger.log(ILogger.LogLevel.Debug, "Selecting transport '" + ITransport.HttpTransportType[transport] + "'");
                return transport;
              }
            } else {
              this.logger.log(ILogger.LogLevel.Debug, "Skipping transport '" + ITransport.HttpTransportType[transport] + "' because it does not support the requested transfer format '" + ITransport.TransferFormat[requestedTransferFormat] + "'.");
            }
          } else {
            this.logger.log(ILogger.LogLevel.Debug, "Skipping transport '" + ITransport.HttpTransportType[transport] + "' because it was disabled by the client.");
          }
        }

        return null;
      };

      HttpConnection.prototype.isITransport = function (transport) {
        return transport && _typeof(transport) === "object" && "connect" in transport;
      };

      HttpConnection.prototype.changeState = function (from, to) {
        if (this.connectionState === from) {
          this.connectionState = to;
          return true;
        }

        return false;
      };

      HttpConnection.prototype.stopConnection = function (error) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
          return tslib_1.__generator(this, function (_a) {
            this.transport = null;
            error = this.stopError || error;

            if (error) {
              this.logger.log(ILogger.LogLevel.Error, "Connection disconnected with error '" + error + "'.");
            } else {
              this.logger.log(ILogger.LogLevel.Information, "Connection disconnected.");
            }

            this.connectionState = 2;

            if (this.onclose) {
              this.onclose(error);
            }

            return [2];
          });
        });
      };

      HttpConnection.prototype.resolveUrl = function (url) {
        if (url.lastIndexOf("https://", 0) === 0 || url.lastIndexOf("http://", 0) === 0) {
          return url;
        }

        if (typeof window === "undefined" || !window || !window.document) {
          throw new Error("Cannot resolve '" + url + "'.");
        }

        var aTag = window.document.createElement("a");
        aTag.href = url;
        this.logger.log(ILogger.LogLevel.Information, "Normalizing '" + url + "' to '" + aTag.href + "'.");
        return aTag.href;
      };

      HttpConnection.prototype.resolveNegotiateUrl = function (url) {
        var index = url.indexOf("?");
        var negotiateUrl = url.substring(0, index === -1 ? url.length : index);

        if (negotiateUrl[negotiateUrl.length - 1] !== "/") {
          negotiateUrl += "/";
        }

        negotiateUrl += "negotiate";
        negotiateUrl += index === -1 ? "" : url.substring(index);
        return negotiateUrl;
      };

      return HttpConnection;
    }();

    exports.HttpConnection = HttpConnection;

    function transportMatches(requestedTransport, actualTransport) {
      return !requestedTransport || (actualTransport & requestedTransport) !== 0;
    }
  });
  unwrapExports(HttpConnection_1);
  var HttpConnection_2 = HttpConnection_1.HttpConnection;
  var JsonHubProtocol_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var JSON_HUB_PROTOCOL_NAME = "json";

    var JsonHubProtocol = function () {
      function JsonHubProtocol() {
        this.name = JSON_HUB_PROTOCOL_NAME;
        this.version = 1;
        this.transferFormat = ITransport.TransferFormat.Text;
      }

      JsonHubProtocol.prototype.parseMessages = function (input, logger) {
        if (typeof input !== "string") {
          throw new Error("Invalid input for JSON hub protocol. Expected a string.");
        }

        if (!input) {
          return [];
        }

        if (logger === null) {
          logger = Loggers.NullLogger.instance;
        }

        var messages = TextMessageFormat_1.TextMessageFormat.parse(input);
        var hubMessages = [];

        for (var _i = 0, messages_1 = messages; _i < messages_1.length; _i++) {
          var message = messages_1[_i];
          var parsedMessage = JSON.parse(message);

          if (typeof parsedMessage.type !== "number") {
            throw new Error("Invalid payload.");
          }

          switch (parsedMessage.type) {
            case IHubProtocol.MessageType.Invocation:
              this.isInvocationMessage(parsedMessage);
              break;

            case IHubProtocol.MessageType.StreamItem:
              this.isStreamItemMessage(parsedMessage);
              break;

            case IHubProtocol.MessageType.Completion:
              this.isCompletionMessage(parsedMessage);
              break;

            case IHubProtocol.MessageType.Ping:
              break;

            case IHubProtocol.MessageType.Close:
              break;

            default:
              logger.log(ILogger.LogLevel.Information, "Unknown message type '" + parsedMessage.type + "' ignored.");
              continue;
          }

          hubMessages.push(parsedMessage);
        }

        return hubMessages;
      };

      JsonHubProtocol.prototype.writeMessage = function (message) {
        return TextMessageFormat_1.TextMessageFormat.write(JSON.stringify(message));
      };

      JsonHubProtocol.prototype.isInvocationMessage = function (message) {
        this.assertNotEmptyString(message.target, "Invalid payload for Invocation message.");

        if (message.invocationId !== undefined) {
          this.assertNotEmptyString(message.invocationId, "Invalid payload for Invocation message.");
        }
      };

      JsonHubProtocol.prototype.isStreamItemMessage = function (message) {
        this.assertNotEmptyString(message.invocationId, "Invalid payload for StreamItem message.");

        if (message.item === undefined) {
          throw new Error("Invalid payload for StreamItem message.");
        }
      };

      JsonHubProtocol.prototype.isCompletionMessage = function (message) {
        if (message.result && message.error) {
          throw new Error("Invalid payload for Completion message.");
        }

        if (!message.result && message.error) {
          this.assertNotEmptyString(message.error, "Invalid payload for Completion message.");
        }

        this.assertNotEmptyString(message.invocationId, "Invalid payload for Completion message.");
      };

      JsonHubProtocol.prototype.assertNotEmptyString = function (value, errorMessage) {
        if (typeof value !== "string" || value === "") {
          throw new Error(errorMessage);
        }
      };

      return JsonHubProtocol;
    }();

    exports.JsonHubProtocol = JsonHubProtocol;
  });
  unwrapExports(JsonHubProtocol_1);
  var JsonHubProtocol_2 = JsonHubProtocol_1.JsonHubProtocol;
  var HubConnectionBuilder_1 = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var HubConnectionBuilder = function () {
      function HubConnectionBuilder() {}

      HubConnectionBuilder.prototype.configureLogging = function (logging) {
        Utils.Arg.isRequired(logging, "logging");

        if (isLogger(logging)) {
          this.logger = logging;
        } else {
          this.logger = new Utils.ConsoleLogger(logging);
        }

        return this;
      };

      HubConnectionBuilder.prototype.withUrl = function (url, transportTypeOrOptions) {
        Utils.Arg.isRequired(url, "url");
        this.url = url;

        if (_typeof(transportTypeOrOptions) === "object") {
          this.httpConnectionOptions = transportTypeOrOptions;
        } else {
          this.httpConnectionOptions = {
            transport: transportTypeOrOptions
          };
        }

        return this;
      };

      HubConnectionBuilder.prototype.withHubProtocol = function (protocol) {
        Utils.Arg.isRequired(protocol, "protocol");
        this.protocol = protocol;
        return this;
      };

      HubConnectionBuilder.prototype.build = function () {
        var httpConnectionOptions = this.httpConnectionOptions || {};

        if (httpConnectionOptions.logger === undefined) {
          httpConnectionOptions.logger = this.logger;
        }

        if (!this.url) {
          throw new Error("The 'HubConnectionBuilder.withUrl' method must be called before building the connection.");
        }

        var connection = new HttpConnection_1.HttpConnection(this.url, httpConnectionOptions);
        return HubConnection_1.HubConnection.create(connection, this.logger || Loggers.NullLogger.instance, this.protocol || new JsonHubProtocol_1.JsonHubProtocol());
      };

      return HubConnectionBuilder;
    }();

    exports.HubConnectionBuilder = HubConnectionBuilder;

    function isLogger(logger) {
      return logger.log !== undefined;
    }
  });
  unwrapExports(HubConnectionBuilder_1);
  var HubConnectionBuilder_2 = HubConnectionBuilder_1.HubConnectionBuilder;
  var cjs = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    exports.VERSION = "0.0.0-DEV_BUILD";
    exports.HttpError = Errors.HttpError;
    exports.TimeoutError = Errors.TimeoutError;
    exports.DefaultHttpClient = HttpClient_1.DefaultHttpClient;
    exports.HttpClient = HttpClient_1.HttpClient;
    exports.HttpResponse = HttpClient_1.HttpResponse;
    exports.HubConnection = HubConnection_1.HubConnection;
    exports.HubConnectionBuilder = HubConnectionBuilder_1.HubConnectionBuilder;
    exports.MessageType = IHubProtocol.MessageType;
    exports.LogLevel = ILogger.LogLevel;
    exports.HttpTransportType = ITransport.HttpTransportType;
    exports.TransferFormat = ITransport.TransferFormat;
    exports.NullLogger = Loggers.NullLogger;
    exports.JsonHubProtocol = JsonHubProtocol_1.JsonHubProtocol;
  });
  unwrapExports(cjs);
  var cjs_1 = cjs.VERSION;
  var cjs_2 = cjs.HttpError;
  var cjs_3 = cjs.TimeoutError;
  var cjs_4 = cjs.DefaultHttpClient;
  var cjs_5 = cjs.HttpClient;
  var cjs_6 = cjs.HttpResponse;
  var cjs_7 = cjs.HubConnection;
  var cjs_8 = cjs.HubConnectionBuilder;
  var cjs_9 = cjs.MessageType;
  var cjs_10 = cjs.LogLevel;
  var cjs_11 = cjs.HttpTransportType;
  var cjs_12 = cjs.TransferFormat;
  var cjs_13 = cjs.NullLogger;
  var cjs_14 = cjs.JsonHubProtocol;
  var browserIndex = createCommonjsModule(function (module, exports) {
    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    if (!Uint8Array.prototype.indexOf) {
      Object.defineProperty(Uint8Array.prototype, "indexOf", {
        value: Array.prototype.indexOf,
        writable: true
      });
    }

    if (!Uint8Array.prototype.slice) {
      Object.defineProperty(Uint8Array.prototype, "slice", {
        value: Array.prototype.slice,
        writable: true
      });
    }

    if (!Uint8Array.prototype.forEach) {
      Object.defineProperty(Uint8Array.prototype, "forEach", {
        value: Array.prototype.forEach,
        writable: true
      });
    }

    tslib_1.__exportStar(cjs, exports);
  });
  var browserIndex$1 = unwrapExports(browserIndex);
  return browserIndex$1;
});

(function (f) {
  if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === "object" && typeof module !== "undefined") {
    module.exports = f();
  } else if (typeof define === "function" && define.amd) {
    define([], f);
  } else {
    var g;

    if (typeof window !== "undefined") {
      g = window;
    } else if (typeof global !== "undefined") {
      g = global;
    } else if (typeof self !== "undefined") {
      g = self;
    } else {
      g = this;
    }

    g.msgpack5 = f();
  }
})(function () {
  var define, module, exports;
  return function e(t, n, r) {
    function s(o, u) {
      if (!n[o]) {
        if (!t[o]) {
          var a = typeof require == "function" && require;
          if (!u && a) return a(o, !0);
          if (i) return i(o, !0);
          var f = new Error("Cannot find module '" + o + "'");
          throw f.code = "MODULE_NOT_FOUND", f;
        }

        var l = n[o] = {
          exports: {}
        };
        t[o][0].call(l.exports, function (e) {
          var n = t[o][1][e];
          return s(n ? n : e);
        }, l, l.exports, e, t, n, r);
      }

      return n[o].exports;
    }

    var i = typeof require == "function" && require;

    for (var o = 0; o < r.length; o++) {
      s(r[o]);
    }

    return s;
  }({
    1: [function (require, module, exports) {
      "use strict";

      var Buffer = require("safe-buffer").Buffer;

      var assert = require("assert");

      var bl = require("bl");

      var streams = require("./lib/streams");

      var buildDecode = require("./lib/decoder");

      var buildEncode = require("./lib/encoder");

      function msgpack(options) {
        var encodingTypes = [];
        var decodingTypes = [];
        options = options || {
          forceFloat64: false,
          compatibilityMode: false,
          disableTimestampEncoding: false
        };

        function registerEncoder(check, encode) {
          assert(check, "must have an encode function");
          assert(encode, "must have an encode function");
          encodingTypes.push({
            check: check,
            encode: encode
          });
          return this;
        }

        function registerDecoder(type, decode) {
          assert(type >= 0, "must have a non-negative type");
          assert(decode, "must have a decode function");
          decodingTypes.push({
            type: type,
            decode: decode
          });
          return this;
        }

        function register(type, constructor, encode, decode) {
          assert(constructor, "must have a constructor");
          assert(encode, "must have an encode function");
          assert(type >= 0, "must have a non-negative type");
          assert(decode, "must have a decode function");

          function check(obj) {
            return obj instanceof constructor;
          }

          function reEncode(obj) {
            var buf = bl();
            var header = Buffer.allocUnsafe(1);
            header.writeInt8(type, 0);
            buf.append(header);
            buf.append(encode(obj));
            return buf;
          }

          this.registerEncoder(check, reEncode);
          this.registerDecoder(type, decode);
          return this;
        }

        return {
          encode: buildEncode(encodingTypes, options.forceFloat64, options.compatibilityMode, options.disableTimestampEncoding),
          decode: buildDecode(decodingTypes),
          register: register,
          registerEncoder: registerEncoder,
          registerDecoder: registerDecoder,
          encoder: streams.encoder,
          decoder: streams.decoder,
          buffer: true,
          type: "msgpack5",
          IncompleteBufferError: buildDecode.IncompleteBufferError
        };
      }

      module.exports = msgpack;
    }, {
      "./lib/decoder": 2,
      "./lib/encoder": 3,
      "./lib/streams": 4,
      assert: 5,
      bl: 7,
      "safe-buffer": 28
    }],
    2: [function (require, module, exports) {
      var bl = require("bl");

      var util = require("util");

      function IncompleteBufferError(message) {
        Error.call(this);

        if (Error.captureStackTrace) {
          Error.captureStackTrace(this, this.constructor);
        }

        this.name = this.constructor.name;
        this.message = message || "unable to decode";
      }

      util.inherits(IncompleteBufferError, Error);

      module.exports = function buildDecode(decodingTypes) {
        return decode;

        function getSize(first) {
          switch (first) {
            case 196:
              return 2;

            case 197:
              return 3;

            case 198:
              return 5;

            case 199:
              return 3;

            case 200:
              return 4;

            case 201:
              return 6;

            case 202:
              return 5;

            case 203:
              return 9;

            case 204:
              return 2;

            case 205:
              return 3;

            case 206:
              return 5;

            case 207:
              return 9;

            case 208:
              return 2;

            case 209:
              return 3;

            case 210:
              return 5;

            case 211:
              return 9;

            case 212:
              return 3;

            case 213:
              return 4;

            case 214:
              return 6;

            case 215:
              return 10;

            case 216:
              return 18;

            case 217:
              return 2;

            case 218:
              return 3;

            case 219:
              return 5;

            case 222:
              return 3;

            default:
              return -1;
          }
        }

        function hasMinBufferSize(first, length) {
          var size = getSize(first);

          if (size !== -1 && length < size) {
            return false;
          } else {
            return true;
          }
        }

        function isValidDataSize(dataLength, bufLength, headerLength) {
          return bufLength >= headerLength + dataLength;
        }

        function buildDecodeResult(value, bytesConsumed) {
          return {
            value: value,
            bytesConsumed: bytesConsumed
          };
        }

        function decode(buf) {
          if (!(buf instanceof bl)) {
            buf = bl().append(buf);
          }

          var result = tryDecode(buf);

          if (result) {
            buf.consume(result.bytesConsumed);
            return result.value;
          } else {
            throw new IncompleteBufferError();
          }
        }

        function tryDecode(buf, offset) {
          offset = offset === undefined ? 0 : offset;
          var bufLength = buf.length - offset;

          if (bufLength <= 0) {
            return null;
          }

          var first = buf.readUInt8(offset);
          var length;
          var result = 0;
          var type;
          var bytePos;

          if (!hasMinBufferSize(first, bufLength)) {
            return null;
          }

          switch (first) {
            case 192:
              return buildDecodeResult(null, 1);

            case 194:
              return buildDecodeResult(false, 1);

            case 195:
              return buildDecodeResult(true, 1);

            case 204:
              result = buf.readUInt8(offset + 1);
              return buildDecodeResult(result, 2);

            case 205:
              result = buf.readUInt16BE(offset + 1);
              return buildDecodeResult(result, 3);

            case 206:
              result = buf.readUInt32BE(offset + 1);
              return buildDecodeResult(result, 5);

            case 207:
              for (bytePos = 7; bytePos >= 0; bytePos--) {
                result += buf.readUInt8(offset + bytePos + 1) * Math.pow(2, 8 * (7 - bytePos));
              }

              return buildDecodeResult(result, 9);

            case 208:
              result = buf.readInt8(offset + 1);
              return buildDecodeResult(result, 2);

            case 209:
              result = buf.readInt16BE(offset + 1);
              return buildDecodeResult(result, 3);

            case 210:
              result = buf.readInt32BE(offset + 1);
              return buildDecodeResult(result, 5);

            case 211:
              result = readInt64BE(buf.slice(offset + 1, offset + 9), 0);
              return buildDecodeResult(result, 9);

            case 202:
              result = buf.readFloatBE(offset + 1);
              return buildDecodeResult(result, 5);

            case 203:
              result = buf.readDoubleBE(offset + 1);
              return buildDecodeResult(result, 9);

            case 217:
              length = buf.readUInt8(offset + 1);

              if (!isValidDataSize(length, bufLength, 2)) {
                return null;
              }

              result = buf.toString("utf8", offset + 2, offset + 2 + length);
              return buildDecodeResult(result, 2 + length);

            case 218:
              length = buf.readUInt16BE(offset + 1);

              if (!isValidDataSize(length, bufLength, 3)) {
                return null;
              }

              result = buf.toString("utf8", offset + 3, offset + 3 + length);
              return buildDecodeResult(result, 3 + length);

            case 219:
              length = buf.readUInt32BE(offset + 1);

              if (!isValidDataSize(length, bufLength, 5)) {
                return null;
              }

              result = buf.toString("utf8", offset + 5, offset + 5 + length);
              return buildDecodeResult(result, 5 + length);

            case 196:
              length = buf.readUInt8(offset + 1);

              if (!isValidDataSize(length, bufLength, 2)) {
                return null;
              }

              result = buf.slice(offset + 2, offset + 2 + length);
              return buildDecodeResult(result, 2 + length);

            case 197:
              length = buf.readUInt16BE(offset + 1);

              if (!isValidDataSize(length, bufLength, 3)) {
                return null;
              }

              result = buf.slice(offset + 3, offset + 3 + length);
              return buildDecodeResult(result, 3 + length);

            case 198:
              length = buf.readUInt32BE(offset + 1);

              if (!isValidDataSize(length, bufLength, 5)) {
                return null;
              }

              result = buf.slice(offset + 5, offset + 5 + length);
              return buildDecodeResult(result, 5 + length);

            case 220:
              if (bufLength < 3) {
                return null;
              }

              length = buf.readUInt16BE(offset + 1);
              return decodeArray(buf, offset, length, 3);

            case 221:
              if (bufLength < 5) {
                return null;
              }

              length = buf.readUInt32BE(offset + 1);
              return decodeArray(buf, offset, length, 5);

            case 222:
              length = buf.readUInt16BE(offset + 1);
              return decodeMap(buf, offset, length, 3);

            case 223:
              throw new Error("map too big to decode in JS");

            case 212:
              return decodeFixExt(buf, offset, 1);

            case 213:
              return decodeFixExt(buf, offset, 2);

            case 214:
              return decodeFixExt(buf, offset, 4);

            case 215:
              return decodeFixExt(buf, offset, 8);

            case 216:
              return decodeFixExt(buf, offset, 16);

            case 199:
              length = buf.readUInt8(offset + 1);
              type = buf.readUInt8(offset + 2);

              if (!isValidDataSize(length, bufLength, 3)) {
                return null;
              }

              return decodeExt(buf, offset, type, length, 3);

            case 200:
              length = buf.readUInt16BE(offset + 1);
              type = buf.readUInt8(offset + 3);

              if (!isValidDataSize(length, bufLength, 4)) {
                return null;
              }

              return decodeExt(buf, offset, type, length, 4);

            case 201:
              length = buf.readUInt32BE(offset + 1);
              type = buf.readUInt8(offset + 5);

              if (!isValidDataSize(length, bufLength, 6)) {
                return null;
              }

              return decodeExt(buf, offset, type, length, 6);
          }

          if ((first & 240) === 144) {
            length = first & 15;
            return decodeArray(buf, offset, length, 1);
          } else if ((first & 240) === 128) {
            length = first & 15;
            return decodeMap(buf, offset, length, 1);
          } else if ((first & 224) === 160) {
            length = first & 31;

            if (isValidDataSize(length, bufLength, 1)) {
              result = buf.toString("utf8", offset + 1, offset + length + 1);
              return buildDecodeResult(result, length + 1);
            } else {
              return null;
            }
          } else if (first >= 224) {
            result = first - 256;
            return buildDecodeResult(result, 1);
          } else if (first < 128) {
            return buildDecodeResult(first, 1);
          } else {
            throw new Error("not implemented yet");
          }
        }

        function readInt64BE(buf, offset) {
          var negate = (buf[offset] & 128) == 128;

          if (negate) {
            var carry = 1;

            for (var i = offset + 7; i >= offset; i--) {
              var v = (buf[i] ^ 255) + carry;
              buf[i] = v & 255;
              carry = v >> 8;
            }
          }

          var hi = buf.readUInt32BE(offset + 0);
          var lo = buf.readUInt32BE(offset + 4);
          return (hi * 4294967296 + lo) * (negate ? -1 : +1);
        }

        function decodeArray(buf, offset, length, headerLength) {
          var result = [];
          var i;
          var totalBytesConsumed = 0;
          offset += headerLength;

          for (i = 0; i < length; i++) {
            var decodeResult = tryDecode(buf, offset);

            if (decodeResult) {
              result.push(decodeResult.value);
              offset += decodeResult.bytesConsumed;
              totalBytesConsumed += decodeResult.bytesConsumed;
            } else {
              return null;
            }
          }

          return buildDecodeResult(result, headerLength + totalBytesConsumed);
        }

        function decodeMap(buf, offset, length, headerLength) {
          var result = {};
          var key;
          var i;
          var totalBytesConsumed = 0;
          offset += headerLength;

          for (i = 0; i < length; i++) {
            var keyResult = tryDecode(buf, offset);

            if (keyResult) {
              offset += keyResult.bytesConsumed;
              var valueResult = tryDecode(buf, offset);

              if (valueResult) {
                key = keyResult.value;
                result[key] = valueResult.value;
                offset += valueResult.bytesConsumed;
                totalBytesConsumed += keyResult.bytesConsumed + valueResult.bytesConsumed;
              } else {
                return null;
              }
            } else {
              return null;
            }
          }

          return buildDecodeResult(result, headerLength + totalBytesConsumed);
        }

        function decodeFixExt(buf, offset, size) {
          var type = buf.readInt8(offset + 1);
          return decodeExt(buf, offset, type, size, 2);
        }

        function decodeTimestamp(buf, size, headerSize) {
          var seconds, nanoseconds;
          nanoseconds = 0;

          switch (size) {
            case 4:
              seconds = buf.readUInt32BE();
              break;

            case 8:
              var upper = buf.readUInt32BE();
              var lower = buf.readUInt32BE(4);
              nanoseconds = upper / 4;
              seconds = (upper & 3) * Math.pow(2, 32) + lower;
              break;

            case 12:
              throw new Error("timestamp 96 is not yet implemented");
          }

          var millis = seconds * 1e3 + Math.round(nanoseconds / 1e6);
          return buildDecodeResult(new Date(millis), size + headerSize);
        }

        function decodeExt(buf, offset, type, size, headerSize) {
          var i, toDecode;
          offset += headerSize;

          if (type < 0) {
            switch (type) {
              case -1:
                toDecode = buf.slice(offset, offset + size);
                return decodeTimestamp(toDecode, size, headerSize);
            }
          }

          for (i = 0; i < decodingTypes.length; i++) {
            if (type === decodingTypes[i].type) {
              toDecode = buf.slice(offset, offset + size);
              var value = decodingTypes[i].decode(toDecode);
              return buildDecodeResult(value, headerSize + size);
            }
          }

          throw new Error("unable to find ext type " + type);
        }
      };

      module.exports.IncompleteBufferError = IncompleteBufferError;
    }, {
      bl: 7,
      util: 33
    }],
    3: [function (require, module, exports) {
      "use strict";

      var Buffer = require("safe-buffer").Buffer;

      var bl = require("bl");

      var TOLERANCE = .1;

      module.exports = function buildEncode(encodingTypes, forceFloat64, compatibilityMode, disableTimestampEncoding) {
        function encode(obj, avoidSlice) {
          var buf, len;

          if (obj === undefined) {
            throw new Error("undefined is not encodable in msgpack!");
          } else if (obj === null) {
            buf = Buffer.allocUnsafe(1);
            buf[0] = 192;
          } else if (obj === true) {
            buf = Buffer.allocUnsafe(1);
            buf[0] = 195;
          } else if (obj === false) {
            buf = Buffer.allocUnsafe(1);
            buf[0] = 194;
          } else if (typeof obj === "string") {
            len = Buffer.byteLength(obj);

            if (len < 32) {
              buf = Buffer.allocUnsafe(1 + len);
              buf[0] = 160 | len;

              if (len > 0) {
                buf.write(obj, 1);
              }
            } else if (len <= 255 && !compatibilityMode) {
              buf = Buffer.allocUnsafe(2 + len);
              buf[0] = 217;
              buf[1] = len;
              buf.write(obj, 2);
            } else if (len <= 65535) {
              buf = Buffer.allocUnsafe(3 + len);
              buf[0] = 218;
              buf.writeUInt16BE(len, 1);
              buf.write(obj, 3);
            } else {
              buf = Buffer.allocUnsafe(5 + len);
              buf[0] = 219;
              buf.writeUInt32BE(len, 1);
              buf.write(obj, 5);
            }
          } else if (obj && (obj.readUInt32LE || obj instanceof Uint8Array)) {
            if (obj instanceof Uint8Array) {
              obj = Buffer.from(obj);
            }

            if (obj.length <= 255) {
              buf = Buffer.allocUnsafe(2);
              buf[0] = 196;
              buf[1] = obj.length;
            } else if (obj.length <= 65535) {
              buf = Buffer.allocUnsafe(3);
              buf[0] = 197;
              buf.writeUInt16BE(obj.length, 1);
            } else {
              buf = Buffer.allocUnsafe(5);
              buf[0] = 198;
              buf.writeUInt32BE(obj.length, 1);
            }

            buf = bl([buf, obj]);
          } else if (Array.isArray(obj)) {
            if (obj.length < 16) {
              buf = Buffer.allocUnsafe(1);
              buf[0] = 144 | obj.length;
            } else if (obj.length < 65536) {
              buf = Buffer.allocUnsafe(3);
              buf[0] = 220;
              buf.writeUInt16BE(obj.length, 1);
            } else {
              buf = Buffer.allocUnsafe(5);
              buf[0] = 221;
              buf.writeUInt32BE(obj.length, 1);
            }

            buf = obj.reduce(function (acc, obj) {
              acc.append(encode(obj, true));
              return acc;
            }, bl().append(buf));
          } else if (!disableTimestampEncoding && typeof obj.getDate === "function") {
            return encodeDate(obj);
          } else if (_typeof(obj) === "object") {
            buf = encodeExt(obj) || encodeObject(obj);
          } else if (typeof obj === "number") {
            if (isFloat(obj)) {
              return encodeFloat(obj, forceFloat64);
            } else if (obj >= 0) {
              if (obj < 128) {
                buf = Buffer.allocUnsafe(1);
                buf[0] = obj;
              } else if (obj < 256) {
                buf = Buffer.allocUnsafe(2);
                buf[0] = 204;
                buf[1] = obj;
              } else if (obj < 65536) {
                buf = Buffer.allocUnsafe(3);
                buf[0] = 205;
                buf.writeUInt16BE(obj, 1);
              } else if (obj <= 4294967295) {
                buf = Buffer.allocUnsafe(5);
                buf[0] = 206;
                buf.writeUInt32BE(obj, 1);
              } else if (obj <= 9007199254740991) {
                buf = Buffer.allocUnsafe(9);
                buf[0] = 207;
                write64BitUint(buf, obj);
              } else {
                return encodeFloat(obj, true);
              }
            } else {
              if (obj >= -32) {
                buf = Buffer.allocUnsafe(1);
                buf[0] = 256 + obj;
              } else if (obj >= -128) {
                buf = Buffer.allocUnsafe(2);
                buf[0] = 208;
                buf.writeInt8(obj, 1);
              } else if (obj >= -32768) {
                buf = Buffer.allocUnsafe(3);
                buf[0] = 209;
                buf.writeInt16BE(obj, 1);
              } else if (obj > -214748365) {
                buf = Buffer.allocUnsafe(5);
                buf[0] = 210;
                buf.writeInt32BE(obj, 1);
              } else if (obj >= -9007199254740991) {
                buf = Buffer.allocUnsafe(9);
                buf[0] = 211;
                write64BitInt(buf, 1, obj);
              } else {
                return encodeFloat(obj, true);
              }
            }
          }

          if (!buf) {
            throw new Error("not implemented yet");
          }

          if (avoidSlice) {
            return buf;
          } else {
            return buf.slice();
          }
        }

        function encodeDate(dt) {
          var encoded;
          var millis = dt * 1;
          var seconds = Math.floor(millis / 1e3);
          var nanos = (millis - seconds * 1e3) * 1e6;

          if (nanos || seconds > 4294967295) {
            encoded = new Buffer(10);
            encoded[0] = 215;
            encoded[1] = -1;
            var upperNanos = nanos * 4;
            var upperSeconds = seconds / Math.pow(2, 32);
            var upper = upperNanos + upperSeconds & 4294967295;
            var lower = seconds & 4294967295;
            encoded.writeInt32BE(upper, 2);
            encoded.writeInt32BE(lower, 6);
          } else {
            encoded = new Buffer(6);
            encoded[0] = 214;
            encoded[1] = -1;
            encoded.writeUInt32BE(Math.floor(millis / 1e3), 2);
          }

          return bl().append(encoded);
        }

        function encodeExt(obj) {
          var i;
          var encoded;
          var length = -1;
          var headers = [];

          for (i = 0; i < encodingTypes.length; i++) {
            if (encodingTypes[i].check(obj)) {
              encoded = encodingTypes[i].encode(obj);
              break;
            }
          }

          if (!encoded) {
            return null;
          }

          length = encoded.length - 1;

          if (length === 1) {
            headers.push(212);
          } else if (length === 2) {
            headers.push(213);
          } else if (length === 4) {
            headers.push(214);
          } else if (length === 8) {
            headers.push(215);
          } else if (length === 16) {
            headers.push(216);
          } else if (length < 256) {
            headers.push(199);
            headers.push(length);
          } else if (length < 65536) {
            headers.push(200);
            headers.push(length >> 8);
            headers.push(length & 255);
          } else {
            headers.push(201);
            headers.push(length >> 24);
            headers.push(length >> 16 & 255);
            headers.push(length >> 8 & 255);
            headers.push(length & 255);
          }

          return bl().append(Buffer.from(headers)).append(encoded);
        }

        function encodeObject(obj) {
          var acc = [];
          var length = 0;
          var key;
          var header;

          for (key in obj) {
            if (obj.hasOwnProperty(key) && obj[key] !== undefined && typeof obj[key] !== "function") {
              ++length;
              acc.push(encode(key, true));
              acc.push(encode(obj[key], true));
            }
          }

          if (length < 16) {
            header = Buffer.allocUnsafe(1);
            header[0] = 128 | length;
          } else {
            header = Buffer.allocUnsafe(3);
            header[0] = 222;
            header.writeUInt16BE(length, 1);
          }

          acc.unshift(header);
          var result = acc.reduce(function (list, buf) {
            return list.append(buf);
          }, bl());
          return result;
        }

        return encode;
      };

      function write64BitUint(buf, obj) {
        for (var currByte = 7; currByte >= 0; currByte--) {
          buf[currByte + 1] = obj & 255;
          obj = obj / 256;
        }
      }

      function write64BitInt(buf, offset, num) {
        var negate = num < 0;

        if (negate) {
          num = Math.abs(num);
        }

        var lo = num % 4294967296;
        var hi = num / 4294967296;
        buf.writeUInt32BE(Math.floor(hi), offset + 0);
        buf.writeUInt32BE(lo, offset + 4);

        if (negate) {
          var carry = 1;

          for (var i = offset + 7; i >= offset; i--) {
            var v = (buf[i] ^ 255) + carry;
            buf[i] = v & 255;
            carry = v >> 8;
          }
        }
      }

      function isFloat(n) {
        return n !== Math.floor(n);
      }

      function encodeFloat(obj, forceFloat64) {
        var buf;
        buf = Buffer.allocUnsafe(5);
        buf[0] = 202;
        buf.writeFloatBE(obj, 1);

        if (forceFloat64 || Math.abs(obj - buf.readFloatBE(1)) > TOLERANCE) {
          buf = Buffer.allocUnsafe(9);
          buf[0] = 203;
          buf.writeDoubleBE(obj, 1);
        }

        return buf;
      }
    }, {
      bl: 7,
      "safe-buffer": 28
    }],
    4: [function (require, module, exports) {
      "use strict";

      var Transform = require("readable-stream").Transform;

      var inherits = require("inherits");

      var bl = require("bl");

      function Base(opts) {
        opts = opts || {};
        opts.objectMode = true;
        opts.highWaterMark = 16;
        Transform.call(this, opts);
        this._msgpack = opts.msgpack;
      }

      inherits(Base, Transform);

      function Encoder(opts) {
        if (!(this instanceof Encoder)) {
          opts = opts || {};
          opts.msgpack = this;
          return new Encoder(opts);
        }

        Base.call(this, opts);
      }

      inherits(Encoder, Base);

      Encoder.prototype._transform = function (obj, enc, done) {
        var buf = null;

        try {
          buf = this._msgpack.encode(obj).slice(0);
        } catch (err) {
          this.emit("error", err);
          return done();
        }

        this.push(buf);
        done();
      };

      function Decoder(opts) {
        if (!(this instanceof Decoder)) {
          opts = opts || {};
          opts.msgpack = this;
          return new Decoder(opts);
        }

        Base.call(this, opts);
        this._chunks = bl();
      }

      inherits(Decoder, Base);

      Decoder.prototype._transform = function (buf, enc, done) {
        if (buf) {
          this._chunks.append(buf);
        }

        try {
          var result = this._msgpack.decode(this._chunks);

          this.push(result);
        } catch (err) {
          if (err instanceof this._msgpack.IncompleteBufferError) {
            done();
          } else {
            this.emit("error", err);
          }

          return;
        }

        if (this._chunks.length > 0) {
          this._transform(null, enc, done);
        } else {
          done();
        }
      };

      module.exports.decoder = Decoder;
      module.exports.encoder = Encoder;
    }, {
      bl: 7,
      inherits: 13,
      "readable-stream": 27
    }],
    5: [function (require, module, exports) {
      (function (global) {
        "use strict";

        function compare(a, b) {
          if (a === b) {
            return 0;
          }

          var x = a.length;
          var y = b.length;

          for (var i = 0, len = Math.min(x, y); i < len; ++i) {
            if (a[i] !== b[i]) {
              x = a[i];
              y = b[i];
              break;
            }
          }

          if (x < y) {
            return -1;
          }

          if (y < x) {
            return 1;
          }

          return 0;
        }

        function isBuffer(b) {
          if (global.Buffer && typeof global.Buffer.isBuffer === "function") {
            return global.Buffer.isBuffer(b);
          }

          return !!(b != null && b._isBuffer);
        }

        var util = require("util/");

        var hasOwn = Object.prototype.hasOwnProperty;
        var pSlice = Array.prototype.slice;

        var functionsHaveNames = function () {
          return function foo() {}.name === "foo";
        }();

        function pToString(obj) {
          return Object.prototype.toString.call(obj);
        }

        function isView(arrbuf) {
          if (isBuffer(arrbuf)) {
            return false;
          }

          if (typeof global.ArrayBuffer !== "function") {
            return false;
          }

          if (typeof ArrayBuffer.isView === "function") {
            return ArrayBuffer.isView(arrbuf);
          }

          if (!arrbuf) {
            return false;
          }

          if (arrbuf instanceof DataView) {
            return true;
          }

          if (arrbuf.buffer && arrbuf.buffer instanceof ArrayBuffer) {
            return true;
          }

          return false;
        }

        var assert = module.exports = ok;
        var regex = /\s*function\s+([^\(\s]*)\s*/;

        function getName(func) {
          if (!util.isFunction(func)) {
            return;
          }

          if (functionsHaveNames) {
            return func.name;
          }

          var str = func.toString();
          var match = str.match(regex);
          return match && match[1];
        }

        assert.AssertionError = function AssertionError(options) {
          this.name = "AssertionError";
          this.actual = options.actual;
          this.expected = options.expected;
          this.operator = options.operator;

          if (options.message) {
            this.message = options.message;
            this.generatedMessage = false;
          } else {
            this.message = getMessage(this);
            this.generatedMessage = true;
          }

          var stackStartFunction = options.stackStartFunction || fail;

          if (Error.captureStackTrace) {
            Error.captureStackTrace(this, stackStartFunction);
          } else {
            var err = new Error();

            if (err.stack) {
              var out = err.stack;
              var fn_name = getName(stackStartFunction);
              var idx = out.indexOf("\n" + fn_name);

              if (idx >= 0) {
                var next_line = out.indexOf("\n", idx + 1);
                out = out.substring(next_line + 1);
              }

              this.stack = out;
            }
          }
        };

        util.inherits(assert.AssertionError, Error);

        function truncate(s, n) {
          if (typeof s === "string") {
            return s.length < n ? s : s.slice(0, n);
          } else {
            return s;
          }
        }

        function inspect(something) {
          if (functionsHaveNames || !util.isFunction(something)) {
            return util.inspect(something);
          }

          var rawname = getName(something);
          var name = rawname ? ": " + rawname : "";
          return "[Function" + name + "]";
        }

        function getMessage(self) {
          return truncate(inspect(self.actual), 128) + " " + self.operator + " " + truncate(inspect(self.expected), 128);
        }

        function fail(actual, expected, message, operator, stackStartFunction) {
          throw new assert.AssertionError({
            message: message,
            actual: actual,
            expected: expected,
            operator: operator,
            stackStartFunction: stackStartFunction
          });
        }

        assert.fail = fail;

        function ok(value, message) {
          if (!value) fail(value, true, message, "==", assert.ok);
        }

        assert.ok = ok;

        assert.equal = function equal(actual, expected, message) {
          if (actual != expected) fail(actual, expected, message, "==", assert.equal);
        };

        assert.notEqual = function notEqual(actual, expected, message) {
          if (actual == expected) {
            fail(actual, expected, message, "!=", assert.notEqual);
          }
        };

        assert.deepEqual = function deepEqual(actual, expected, message) {
          if (!_deepEqual(actual, expected, false)) {
            fail(actual, expected, message, "deepEqual", assert.deepEqual);
          }
        };

        assert.deepStrictEqual = function deepStrictEqual(actual, expected, message) {
          if (!_deepEqual(actual, expected, true)) {
            fail(actual, expected, message, "deepStrictEqual", assert.deepStrictEqual);
          }
        };

        function _deepEqual(actual, expected, strict, memos) {
          if (actual === expected) {
            return true;
          } else if (isBuffer(actual) && isBuffer(expected)) {
            return compare(actual, expected) === 0;
          } else if (util.isDate(actual) && util.isDate(expected)) {
            return actual.getTime() === expected.getTime();
          } else if (util.isRegExp(actual) && util.isRegExp(expected)) {
            return actual.source === expected.source && actual.global === expected.global && actual.multiline === expected.multiline && actual.lastIndex === expected.lastIndex && actual.ignoreCase === expected.ignoreCase;
          } else if ((actual === null || _typeof(actual) !== "object") && (expected === null || _typeof(expected) !== "object")) {
            return strict ? actual === expected : actual == expected;
          } else if (isView(actual) && isView(expected) && pToString(actual) === pToString(expected) && !(actual instanceof Float32Array || actual instanceof Float64Array)) {
            return compare(new Uint8Array(actual.buffer), new Uint8Array(expected.buffer)) === 0;
          } else if (isBuffer(actual) !== isBuffer(expected)) {
            return false;
          } else {
            memos = memos || {
              actual: [],
              expected: []
            };
            var actualIndex = memos.actual.indexOf(actual);

            if (actualIndex !== -1) {
              if (actualIndex === memos.expected.indexOf(expected)) {
                return true;
              }
            }

            memos.actual.push(actual);
            memos.expected.push(expected);
            return objEquiv(actual, expected, strict, memos);
          }
        }

        function isArguments(object) {
          return Object.prototype.toString.call(object) == "[object Arguments]";
        }

        function objEquiv(a, b, strict, actualVisitedObjects) {
          if (a === null || a === undefined || b === null || b === undefined) return false;
          if (util.isPrimitive(a) || util.isPrimitive(b)) return a === b;
          if (strict && Object.getPrototypeOf(a) !== Object.getPrototypeOf(b)) return false;
          var aIsArgs = isArguments(a);
          var bIsArgs = isArguments(b);
          if (aIsArgs && !bIsArgs || !aIsArgs && bIsArgs) return false;

          if (aIsArgs) {
            a = pSlice.call(a);
            b = pSlice.call(b);
            return _deepEqual(a, b, strict);
          }

          var ka = objectKeys(a);
          var kb = objectKeys(b);
          var key, i;
          if (ka.length !== kb.length) return false;
          ka.sort();
          kb.sort();

          for (i = ka.length - 1; i >= 0; i--) {
            if (ka[i] !== kb[i]) return false;
          }

          for (i = ka.length - 1; i >= 0; i--) {
            key = ka[i];
            if (!_deepEqual(a[key], b[key], strict, actualVisitedObjects)) return false;
          }

          return true;
        }

        assert.notDeepEqual = function notDeepEqual(actual, expected, message) {
          if (_deepEqual(actual, expected, false)) {
            fail(actual, expected, message, "notDeepEqual", assert.notDeepEqual);
          }
        };

        assert.notDeepStrictEqual = notDeepStrictEqual;

        function notDeepStrictEqual(actual, expected, message) {
          if (_deepEqual(actual, expected, true)) {
            fail(actual, expected, message, "notDeepStrictEqual", notDeepStrictEqual);
          }
        }

        assert.strictEqual = function strictEqual(actual, expected, message) {
          if (actual !== expected) {
            fail(actual, expected, message, "===", assert.strictEqual);
          }
        };

        assert.notStrictEqual = function notStrictEqual(actual, expected, message) {
          if (actual === expected) {
            fail(actual, expected, message, "!==", assert.notStrictEqual);
          }
        };

        function expectedException(actual, expected) {
          if (!actual || !expected) {
            return false;
          }

          if (Object.prototype.toString.call(expected) == "[object RegExp]") {
            return expected.test(actual);
          }

          try {
            if (actual instanceof expected) {
              return true;
            }
          } catch (e) {}

          if (Error.isPrototypeOf(expected)) {
            return false;
          }

          return expected.call({}, actual) === true;
        }

        function _tryBlock(block) {
          var error;

          try {
            block();
          } catch (e) {
            error = e;
          }

          return error;
        }

        function _throws(shouldThrow, block, expected, message) {
          var actual;

          if (typeof block !== "function") {
            throw new TypeError('"block" argument must be a function');
          }

          if (typeof expected === "string") {
            message = expected;
            expected = null;
          }

          actual = _tryBlock(block);
          message = (expected && expected.name ? " (" + expected.name + ")." : ".") + (message ? " " + message : ".");

          if (shouldThrow && !actual) {
            fail(actual, expected, "Missing expected exception" + message);
          }

          var userProvidedMessage = typeof message === "string";
          var isUnwantedException = !shouldThrow && util.isError(actual);
          var isUnexpectedException = !shouldThrow && actual && !expected;

          if (isUnwantedException && userProvidedMessage && expectedException(actual, expected) || isUnexpectedException) {
            fail(actual, expected, "Got unwanted exception" + message);
          }

          if (shouldThrow && actual && expected && !expectedException(actual, expected) || !shouldThrow && actual) {
            throw actual;
          }
        }

        assert["throws"] = function (block, error, message) {
          _throws(true, block, error, message);
        };

        assert.doesNotThrow = function (block, error, message) {
          _throws(false, block, error, message);
        };

        assert.ifError = function (err) {
          if (err) throw err;
        };

        var objectKeys = Object.keys || function (obj) {
          var keys = [];

          for (var key in obj) {
            if (hasOwn.call(obj, key)) keys.push(key);
          }

          return keys;
        };
      }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
    }, {
      "util/": 33
    }],
    6: [function (require, module, exports) {
      "use strict";

      exports.byteLength = byteLength;
      exports.toByteArray = toByteArray;
      exports.fromByteArray = fromByteArray;
      var lookup = [];
      var revLookup = [];
      var Arr = typeof Uint8Array !== "undefined" ? Uint8Array : Array;
      var code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

      for (var i = 0, len = code.length; i < len; ++i) {
        lookup[i] = code[i];
        revLookup[code.charCodeAt(i)] = i;
      }

      revLookup["-".charCodeAt(0)] = 62;
      revLookup["_".charCodeAt(0)] = 63;

      function placeHoldersCount(b64) {
        var len = b64.length;

        if (len % 4 > 0) {
          throw new Error("Invalid string. Length must be a multiple of 4");
        }

        return b64[len - 2] === "=" ? 2 : b64[len - 1] === "=" ? 1 : 0;
      }

      function byteLength(b64) {
        return b64.length * 3 / 4 - placeHoldersCount(b64);
      }

      function toByteArray(b64) {
        var i, l, tmp, placeHolders, arr;
        var len = b64.length;
        placeHolders = placeHoldersCount(b64);
        arr = new Arr(len * 3 / 4 - placeHolders);
        l = placeHolders > 0 ? len - 4 : len;
        var L = 0;

        for (i = 0; i < l; i += 4) {
          tmp = revLookup[b64.charCodeAt(i)] << 18 | revLookup[b64.charCodeAt(i + 1)] << 12 | revLookup[b64.charCodeAt(i + 2)] << 6 | revLookup[b64.charCodeAt(i + 3)];
          arr[L++] = tmp >> 16 & 255;
          arr[L++] = tmp >> 8 & 255;
          arr[L++] = tmp & 255;
        }

        if (placeHolders === 2) {
          tmp = revLookup[b64.charCodeAt(i)] << 2 | revLookup[b64.charCodeAt(i + 1)] >> 4;
          arr[L++] = tmp & 255;
        } else if (placeHolders === 1) {
          tmp = revLookup[b64.charCodeAt(i)] << 10 | revLookup[b64.charCodeAt(i + 1)] << 4 | revLookup[b64.charCodeAt(i + 2)] >> 2;
          arr[L++] = tmp >> 8 & 255;
          arr[L++] = tmp & 255;
        }

        return arr;
      }

      function tripletToBase64(num) {
        return lookup[num >> 18 & 63] + lookup[num >> 12 & 63] + lookup[num >> 6 & 63] + lookup[num & 63];
      }

      function encodeChunk(uint8, start, end) {
        var tmp;
        var output = [];

        for (var i = start; i < end; i += 3) {
          tmp = (uint8[i] << 16) + (uint8[i + 1] << 8) + uint8[i + 2];
          output.push(tripletToBase64(tmp));
        }

        return output.join("");
      }

      function fromByteArray(uint8) {
        var tmp;
        var len = uint8.length;
        var extraBytes = len % 3;
        var output = "";
        var parts = [];
        var maxChunkLength = 16383;

        for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
          parts.push(encodeChunk(uint8, i, i + maxChunkLength > len2 ? len2 : i + maxChunkLength));
        }

        if (extraBytes === 1) {
          tmp = uint8[len - 1];
          output += lookup[tmp >> 2];
          output += lookup[tmp << 4 & 63];
          output += "==";
        } else if (extraBytes === 2) {
          tmp = (uint8[len - 2] << 8) + uint8[len - 1];
          output += lookup[tmp >> 10];
          output += lookup[tmp >> 4 & 63];
          output += lookup[tmp << 2 & 63];
          output += "=";
        }

        parts.push(output);
        return parts.join("");
      }
    }, {}],
    7: [function (require, module, exports) {
      (function (Buffer) {
        var DuplexStream = require("readable-stream/duplex"),
            util = require("util");

        function BufferList(callback) {
          if (!(this instanceof BufferList)) return new BufferList(callback);
          this._bufs = [];
          this.length = 0;

          if (typeof callback == "function") {
            this._callback = callback;

            var piper = function piper(err) {
              if (this._callback) {
                this._callback(err);

                this._callback = null;
              }
            }.bind(this);

            this.on("pipe", function onPipe(src) {
              src.on("error", piper);
            });
            this.on("unpipe", function onUnpipe(src) {
              src.removeListener("error", piper);
            });
          } else {
            this.append(callback);
          }

          DuplexStream.call(this);
        }

        util.inherits(BufferList, DuplexStream);

        BufferList.prototype._offset = function _offset(offset) {
          var tot = 0,
              i = 0,
              _t;

          if (offset === 0) return [0, 0];

          for (; i < this._bufs.length; i++) {
            _t = tot + this._bufs[i].length;
            if (offset < _t || i == this._bufs.length - 1) return [i, offset - tot];
            tot = _t;
          }
        };

        BufferList.prototype.append = function append(buf) {
          var i = 0;

          if (Buffer.isBuffer(buf)) {
            this._appendBuffer(buf);
          } else if (Array.isArray(buf)) {
            for (; i < buf.length; i++) {
              this.append(buf[i]);
            }
          } else if (buf instanceof BufferList) {
            for (; i < buf._bufs.length; i++) {
              this.append(buf._bufs[i]);
            }
          } else if (buf != null) {
            if (typeof buf == "number") buf = buf.toString();

            this._appendBuffer(new Buffer(buf));
          }

          return this;
        };

        BufferList.prototype._appendBuffer = function appendBuffer(buf) {
          this._bufs.push(buf);

          this.length += buf.length;
        };

        BufferList.prototype._write = function _write(buf, encoding, callback) {
          this._appendBuffer(buf);

          if (typeof callback == "function") callback();
        };

        BufferList.prototype._read = function _read(size) {
          if (!this.length) return this.push(null);
          size = Math.min(size, this.length);
          this.push(this.slice(0, size));
          this.consume(size);
        };

        BufferList.prototype.end = function end(chunk) {
          DuplexStream.prototype.end.call(this, chunk);

          if (this._callback) {
            this._callback(null, this.slice());

            this._callback = null;
          }
        };

        BufferList.prototype.get = function get(index) {
          return this.slice(index, index + 1)[0];
        };

        BufferList.prototype.slice = function slice(start, end) {
          if (typeof start == "number" && start < 0) start += this.length;
          if (typeof end == "number" && end < 0) end += this.length;
          return this.copy(null, 0, start, end);
        };

        BufferList.prototype.copy = function copy(dst, dstStart, srcStart, srcEnd) {
          if (typeof srcStart != "number" || srcStart < 0) srcStart = 0;
          if (typeof srcEnd != "number" || srcEnd > this.length) srcEnd = this.length;
          if (srcStart >= this.length) return dst || new Buffer(0);
          if (srcEnd <= 0) return dst || new Buffer(0);

          var copy = !!dst,
              off = this._offset(srcStart),
              len = srcEnd - srcStart,
              bytes = len,
              bufoff = copy && dstStart || 0,
              start = off[1],
              l,
              i;

          if (srcStart === 0 && srcEnd == this.length) {
            if (!copy) {
              return this._bufs.length === 1 ? this._bufs[0] : Buffer.concat(this._bufs, this.length);
            }

            for (i = 0; i < this._bufs.length; i++) {
              this._bufs[i].copy(dst, bufoff);

              bufoff += this._bufs[i].length;
            }

            return dst;
          }

          if (bytes <= this._bufs[off[0]].length - start) {
            return copy ? this._bufs[off[0]].copy(dst, dstStart, start, start + bytes) : this._bufs[off[0]].slice(start, start + bytes);
          }

          if (!copy) dst = new Buffer(len);

          for (i = off[0]; i < this._bufs.length; i++) {
            l = this._bufs[i].length - start;

            if (bytes > l) {
              this._bufs[i].copy(dst, bufoff, start);
            } else {
              this._bufs[i].copy(dst, bufoff, start, start + bytes);

              break;
            }

            bufoff += l;
            bytes -= l;
            if (start) start = 0;
          }

          return dst;
        };

        BufferList.prototype.shallowSlice = function shallowSlice(start, end) {
          start = start || 0;
          end = end || this.length;
          if (start < 0) start += this.length;
          if (end < 0) end += this.length;

          var startOffset = this._offset(start),
              endOffset = this._offset(end),
              buffers = this._bufs.slice(startOffset[0], endOffset[0] + 1);

          if (endOffset[1] == 0) buffers.pop();else buffers[buffers.length - 1] = buffers[buffers.length - 1].slice(0, endOffset[1]);
          if (startOffset[1] != 0) buffers[0] = buffers[0].slice(startOffset[1]);
          return new BufferList(buffers);
        };

        BufferList.prototype.toString = function toString(encoding, start, end) {
          return this.slice(start, end).toString(encoding);
        };

        BufferList.prototype.consume = function consume(bytes) {
          while (this._bufs.length) {
            if (bytes >= this._bufs[0].length) {
              bytes -= this._bufs[0].length;
              this.length -= this._bufs[0].length;

              this._bufs.shift();
            } else {
              this._bufs[0] = this._bufs[0].slice(bytes);
              this.length -= bytes;
              break;
            }
          }

          return this;
        };

        BufferList.prototype.duplicate = function duplicate() {
          var i = 0,
              copy = new BufferList();

          for (; i < this._bufs.length; i++) {
            copy.append(this._bufs[i]);
          }

          return copy;
        };

        BufferList.prototype.destroy = function destroy() {
          this._bufs.length = 0;
          this.length = 0;
          this.push(null);
        };

        (function () {
          var methods = {
            readDoubleBE: 8,
            readDoubleLE: 8,
            readFloatBE: 4,
            readFloatLE: 4,
            readInt32BE: 4,
            readInt32LE: 4,
            readUInt32BE: 4,
            readUInt32LE: 4,
            readInt16BE: 2,
            readInt16LE: 2,
            readUInt16BE: 2,
            readUInt16LE: 2,
            readInt8: 1,
            readUInt8: 1
          };

          for (var m in methods) {
            (function (m) {
              BufferList.prototype[m] = function (offset) {
                return this.slice(offset, offset + methods[m])[m](0);
              };
            })(m);
          }
        })();

        module.exports = BufferList;
      }).call(this, require("buffer").Buffer);
    }, {
      buffer: 9,
      "readable-stream/duplex": 18,
      util: 33
    }],
    8: [function (require, module, exports) {}, {}],
    9: [function (require, module, exports) {
      "use strict";

      var base64 = require("base64-js");

      var ieee754 = require("ieee754");

      exports.Buffer = Buffer;
      exports.SlowBuffer = SlowBuffer;
      exports.INSPECT_MAX_BYTES = 50;
      var K_MAX_LENGTH = 2147483647;
      exports.kMaxLength = K_MAX_LENGTH;
      Buffer.TYPED_ARRAY_SUPPORT = typedArraySupport();

      if (!Buffer.TYPED_ARRAY_SUPPORT && typeof console !== "undefined" && typeof console.error === "function") {
        console.error("This browser lacks typed array (Uint8Array) support which is required by " + "`buffer` v5.x. Use `buffer` v4.x if you require old browser support.");
      }

      function typedArraySupport() {
        try {
          var arr = new Uint8Array(1);
          arr.__proto__ = {
            __proto__: Uint8Array.prototype,
            foo: function foo() {
              return 42;
            }
          };
          return arr.foo() === 42;
        } catch (e) {
          return false;
        }
      }

      function createBuffer(length) {
        if (length > K_MAX_LENGTH) {
          throw new RangeError("Invalid typed array length");
        }

        var buf = new Uint8Array(length);
        buf.__proto__ = Buffer.prototype;
        return buf;
      }

      function Buffer(arg, encodingOrOffset, length) {
        if (typeof arg === "number") {
          if (typeof encodingOrOffset === "string") {
            throw new Error("If encoding is specified then the first argument must be a string");
          }

          return allocUnsafe(arg);
        }

        return from(arg, encodingOrOffset, length);
      }

      if (typeof Symbol !== "undefined" && Symbol.species && Buffer[Symbol.species] === Buffer) {
        Object.defineProperty(Buffer, Symbol.species, {
          value: null,
          configurable: true,
          enumerable: false,
          writable: false
        });
      }

      Buffer.poolSize = 8192;

      function from(value, encodingOrOffset, length) {
        if (typeof value === "number") {
          throw new TypeError('"value" argument must not be a number');
        }

        if (isArrayBuffer(value)) {
          return fromArrayBuffer(value, encodingOrOffset, length);
        }

        if (typeof value === "string") {
          return fromString(value, encodingOrOffset);
        }

        return fromObject(value);
      }

      Buffer.from = function (value, encodingOrOffset, length) {
        return from(value, encodingOrOffset, length);
      };

      Buffer.prototype.__proto__ = Uint8Array.prototype;
      Buffer.__proto__ = Uint8Array;

      function assertSize(size) {
        if (typeof size !== "number") {
          throw new TypeError('"size" argument must be a number');
        } else if (size < 0) {
          throw new RangeError('"size" argument must not be negative');
        }
      }

      function alloc(size, fill, encoding) {
        assertSize(size);

        if (size <= 0) {
          return createBuffer(size);
        }

        if (fill !== undefined) {
          return typeof encoding === "string" ? createBuffer(size).fill(fill, encoding) : createBuffer(size).fill(fill);
        }

        return createBuffer(size);
      }

      Buffer.alloc = function (size, fill, encoding) {
        return alloc(size, fill, encoding);
      };

      function allocUnsafe(size) {
        assertSize(size);
        return createBuffer(size < 0 ? 0 : checked(size) | 0);
      }

      Buffer.allocUnsafe = function (size) {
        return allocUnsafe(size);
      };

      Buffer.allocUnsafeSlow = function (size) {
        return allocUnsafe(size);
      };

      function fromString(string, encoding) {
        if (typeof encoding !== "string" || encoding === "") {
          encoding = "utf8";
        }

        if (!Buffer.isEncoding(encoding)) {
          throw new TypeError('"encoding" must be a valid string encoding');
        }

        var length = byteLength(string, encoding) | 0;
        var buf = createBuffer(length);
        var actual = buf.write(string, encoding);

        if (actual !== length) {
          buf = buf.slice(0, actual);
        }

        return buf;
      }

      function fromArrayLike(array) {
        var length = array.length < 0 ? 0 : checked(array.length) | 0;
        var buf = createBuffer(length);

        for (var i = 0; i < length; i += 1) {
          buf[i] = array[i] & 255;
        }

        return buf;
      }

      function fromArrayBuffer(array, byteOffset, length) {
        if (byteOffset < 0 || array.byteLength < byteOffset) {
          throw new RangeError("'offset' is out of bounds");
        }

        if (array.byteLength < byteOffset + (length || 0)) {
          throw new RangeError("'length' is out of bounds");
        }

        var buf;

        if (byteOffset === undefined && length === undefined) {
          buf = new Uint8Array(array);
        } else if (length === undefined) {
          buf = new Uint8Array(array, byteOffset);
        } else {
          buf = new Uint8Array(array, byteOffset, length);
        }

        buf.__proto__ = Buffer.prototype;
        return buf;
      }

      function fromObject(obj) {
        if (Buffer.isBuffer(obj)) {
          var len = checked(obj.length) | 0;
          var buf = createBuffer(len);

          if (buf.length === 0) {
            return buf;
          }

          obj.copy(buf, 0, 0, len);
          return buf;
        }

        if (obj) {
          if (isArrayBufferView(obj) || "length" in obj) {
            if (typeof obj.length !== "number" || numberIsNaN(obj.length)) {
              return createBuffer(0);
            }

            return fromArrayLike(obj);
          }

          if (obj.type === "Buffer" && Array.isArray(obj.data)) {
            return fromArrayLike(obj.data);
          }
        }

        throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.");
      }

      function checked(length) {
        if (length >= K_MAX_LENGTH) {
          throw new RangeError("Attempt to allocate Buffer larger than maximum " + "size: 0x" + K_MAX_LENGTH.toString(16) + " bytes");
        }

        return length | 0;
      }

      function SlowBuffer(length) {
        if (+length != length) {
          length = 0;
        }

        return Buffer.alloc(+length);
      }

      Buffer.isBuffer = function isBuffer(b) {
        return b != null && b._isBuffer === true;
      };

      Buffer.compare = function compare(a, b) {
        if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
          throw new TypeError("Arguments must be Buffers");
        }

        if (a === b) return 0;
        var x = a.length;
        var y = b.length;

        for (var i = 0, len = Math.min(x, y); i < len; ++i) {
          if (a[i] !== b[i]) {
            x = a[i];
            y = b[i];
            break;
          }
        }

        if (x < y) return -1;
        if (y < x) return 1;
        return 0;
      };

      Buffer.isEncoding = function isEncoding(encoding) {
        switch (String(encoding).toLowerCase()) {
          case "hex":
          case "utf8":
          case "utf-8":
          case "ascii":
          case "latin1":
          case "binary":
          case "base64":
          case "ucs2":
          case "ucs-2":
          case "utf16le":
          case "utf-16le":
            return true;

          default:
            return false;
        }
      };

      Buffer.concat = function concat(list, length) {
        if (!Array.isArray(list)) {
          throw new TypeError('"list" argument must be an Array of Buffers');
        }

        if (list.length === 0) {
          return Buffer.alloc(0);
        }

        var i;

        if (length === undefined) {
          length = 0;

          for (i = 0; i < list.length; ++i) {
            length += list[i].length;
          }
        }

        var buffer = Buffer.allocUnsafe(length);
        var pos = 0;

        for (i = 0; i < list.length; ++i) {
          var buf = list[i];

          if (!Buffer.isBuffer(buf)) {
            throw new TypeError('"list" argument must be an Array of Buffers');
          }

          buf.copy(buffer, pos);
          pos += buf.length;
        }

        return buffer;
      };

      function byteLength(string, encoding) {
        if (Buffer.isBuffer(string)) {
          return string.length;
        }

        if (isArrayBufferView(string) || isArrayBuffer(string)) {
          return string.byteLength;
        }

        if (typeof string !== "string") {
          string = "" + string;
        }

        var len = string.length;
        if (len === 0) return 0;
        var loweredCase = false;

        for (;;) {
          switch (encoding) {
            case "ascii":
            case "latin1":
            case "binary":
              return len;

            case "utf8":
            case "utf-8":
            case undefined:
              return utf8ToBytes(string).length;

            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return len * 2;

            case "hex":
              return len >>> 1;

            case "base64":
              return base64ToBytes(string).length;

            default:
              if (loweredCase) return utf8ToBytes(string).length;
              encoding = ("" + encoding).toLowerCase();
              loweredCase = true;
          }
        }
      }

      Buffer.byteLength = byteLength;

      function slowToString(encoding, start, end) {
        var loweredCase = false;

        if (start === undefined || start < 0) {
          start = 0;
        }

        if (start > this.length) {
          return "";
        }

        if (end === undefined || end > this.length) {
          end = this.length;
        }

        if (end <= 0) {
          return "";
        }

        end >>>= 0;
        start >>>= 0;

        if (end <= start) {
          return "";
        }

        if (!encoding) encoding = "utf8";

        while (true) {
          switch (encoding) {
            case "hex":
              return hexSlice(this, start, end);

            case "utf8":
            case "utf-8":
              return utf8Slice(this, start, end);

            case "ascii":
              return asciiSlice(this, start, end);

            case "latin1":
            case "binary":
              return latin1Slice(this, start, end);

            case "base64":
              return base64Slice(this, start, end);

            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return utf16leSlice(this, start, end);

            default:
              if (loweredCase) throw new TypeError("Unknown encoding: " + encoding);
              encoding = (encoding + "").toLowerCase();
              loweredCase = true;
          }
        }
      }

      Buffer.prototype._isBuffer = true;

      function swap(b, n, m) {
        var i = b[n];
        b[n] = b[m];
        b[m] = i;
      }

      Buffer.prototype.swap16 = function swap16() {
        var len = this.length;

        if (len % 2 !== 0) {
          throw new RangeError("Buffer size must be a multiple of 16-bits");
        }

        for (var i = 0; i < len; i += 2) {
          swap(this, i, i + 1);
        }

        return this;
      };

      Buffer.prototype.swap32 = function swap32() {
        var len = this.length;

        if (len % 4 !== 0) {
          throw new RangeError("Buffer size must be a multiple of 32-bits");
        }

        for (var i = 0; i < len; i += 4) {
          swap(this, i, i + 3);
          swap(this, i + 1, i + 2);
        }

        return this;
      };

      Buffer.prototype.swap64 = function swap64() {
        var len = this.length;

        if (len % 8 !== 0) {
          throw new RangeError("Buffer size must be a multiple of 64-bits");
        }

        for (var i = 0; i < len; i += 8) {
          swap(this, i, i + 7);
          swap(this, i + 1, i + 6);
          swap(this, i + 2, i + 5);
          swap(this, i + 3, i + 4);
        }

        return this;
      };

      Buffer.prototype.toString = function toString() {
        var length = this.length;
        if (length === 0) return "";
        if (arguments.length === 0) return utf8Slice(this, 0, length);
        return slowToString.apply(this, arguments);
      };

      Buffer.prototype.equals = function equals(b) {
        if (!Buffer.isBuffer(b)) throw new TypeError("Argument must be a Buffer");
        if (this === b) return true;
        return Buffer.compare(this, b) === 0;
      };

      Buffer.prototype.inspect = function inspect() {
        var str = "";
        var max = exports.INSPECT_MAX_BYTES;

        if (this.length > 0) {
          str = this.toString("hex", 0, max).match(/.{2}/g).join(" ");
          if (this.length > max) str += " ... ";
        }

        return "<Buffer " + str + ">";
      };

      Buffer.prototype.compare = function compare(target, start, end, thisStart, thisEnd) {
        if (!Buffer.isBuffer(target)) {
          throw new TypeError("Argument must be a Buffer");
        }

        if (start === undefined) {
          start = 0;
        }

        if (end === undefined) {
          end = target ? target.length : 0;
        }

        if (thisStart === undefined) {
          thisStart = 0;
        }

        if (thisEnd === undefined) {
          thisEnd = this.length;
        }

        if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
          throw new RangeError("out of range index");
        }

        if (thisStart >= thisEnd && start >= end) {
          return 0;
        }

        if (thisStart >= thisEnd) {
          return -1;
        }

        if (start >= end) {
          return 1;
        }

        start >>>= 0;
        end >>>= 0;
        thisStart >>>= 0;
        thisEnd >>>= 0;
        if (this === target) return 0;
        var x = thisEnd - thisStart;
        var y = end - start;
        var len = Math.min(x, y);
        var thisCopy = this.slice(thisStart, thisEnd);
        var targetCopy = target.slice(start, end);

        for (var i = 0; i < len; ++i) {
          if (thisCopy[i] !== targetCopy[i]) {
            x = thisCopy[i];
            y = targetCopy[i];
            break;
          }
        }

        if (x < y) return -1;
        if (y < x) return 1;
        return 0;
      };

      function bidirectionalIndexOf(buffer, val, byteOffset, encoding, dir) {
        if (buffer.length === 0) return -1;

        if (typeof byteOffset === "string") {
          encoding = byteOffset;
          byteOffset = 0;
        } else if (byteOffset > 2147483647) {
          byteOffset = 2147483647;
        } else if (byteOffset < -2147483648) {
          byteOffset = -2147483648;
        }

        byteOffset = +byteOffset;

        if (numberIsNaN(byteOffset)) {
          byteOffset = dir ? 0 : buffer.length - 1;
        }

        if (byteOffset < 0) byteOffset = buffer.length + byteOffset;

        if (byteOffset >= buffer.length) {
          if (dir) return -1;else byteOffset = buffer.length - 1;
        } else if (byteOffset < 0) {
          if (dir) byteOffset = 0;else return -1;
        }

        if (typeof val === "string") {
          val = Buffer.from(val, encoding);
        }

        if (Buffer.isBuffer(val)) {
          if (val.length === 0) {
            return -1;
          }

          return arrayIndexOf(buffer, val, byteOffset, encoding, dir);
        } else if (typeof val === "number") {
          val = val & 255;

          if (typeof Uint8Array.prototype.indexOf === "function") {
            if (dir) {
              return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset);
            } else {
              return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset);
            }
          }

          return arrayIndexOf(buffer, [val], byteOffset, encoding, dir);
        }

        throw new TypeError("val must be string, number or Buffer");
      }

      function arrayIndexOf(arr, val, byteOffset, encoding, dir) {
        var indexSize = 1;
        var arrLength = arr.length;
        var valLength = val.length;

        if (encoding !== undefined) {
          encoding = String(encoding).toLowerCase();

          if (encoding === "ucs2" || encoding === "ucs-2" || encoding === "utf16le" || encoding === "utf-16le") {
            if (arr.length < 2 || val.length < 2) {
              return -1;
            }

            indexSize = 2;
            arrLength /= 2;
            valLength /= 2;
            byteOffset /= 2;
          }
        }

        function read(buf, i) {
          if (indexSize === 1) {
            return buf[i];
          } else {
            return buf.readUInt16BE(i * indexSize);
          }
        }

        var i;

        if (dir) {
          var foundIndex = -1;

          for (i = byteOffset; i < arrLength; i++) {
            if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
              if (foundIndex === -1) foundIndex = i;
              if (i - foundIndex + 1 === valLength) return foundIndex * indexSize;
            } else {
              if (foundIndex !== -1) i -= i - foundIndex;
              foundIndex = -1;
            }
          }
        } else {
          if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength;

          for (i = byteOffset; i >= 0; i--) {
            var found = true;

            for (var j = 0; j < valLength; j++) {
              if (read(arr, i + j) !== read(val, j)) {
                found = false;
                break;
              }
            }

            if (found) return i;
          }
        }

        return -1;
      }

      Buffer.prototype.includes = function includes(val, byteOffset, encoding) {
        return this.indexOf(val, byteOffset, encoding) !== -1;
      };

      Buffer.prototype.indexOf = function indexOf(val, byteOffset, encoding) {
        return bidirectionalIndexOf(this, val, byteOffset, encoding, true);
      };

      Buffer.prototype.lastIndexOf = function lastIndexOf(val, byteOffset, encoding) {
        return bidirectionalIndexOf(this, val, byteOffset, encoding, false);
      };

      function hexWrite(buf, string, offset, length) {
        offset = Number(offset) || 0;
        var remaining = buf.length - offset;

        if (!length) {
          length = remaining;
        } else {
          length = Number(length);

          if (length > remaining) {
            length = remaining;
          }
        }

        var strLen = string.length;
        if (strLen % 2 !== 0) throw new TypeError("Invalid hex string");

        if (length > strLen / 2) {
          length = strLen / 2;
        }

        for (var i = 0; i < length; ++i) {
          var parsed = parseInt(string.substr(i * 2, 2), 16);
          if (numberIsNaN(parsed)) return i;
          buf[offset + i] = parsed;
        }

        return i;
      }

      function utf8Write(buf, string, offset, length) {
        return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length);
      }

      function asciiWrite(buf, string, offset, length) {
        return blitBuffer(asciiToBytes(string), buf, offset, length);
      }

      function latin1Write(buf, string, offset, length) {
        return asciiWrite(buf, string, offset, length);
      }

      function base64Write(buf, string, offset, length) {
        return blitBuffer(base64ToBytes(string), buf, offset, length);
      }

      function ucs2Write(buf, string, offset, length) {
        return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length);
      }

      Buffer.prototype.write = function write(string, offset, length, encoding) {
        if (offset === undefined) {
          encoding = "utf8";
          length = this.length;
          offset = 0;
        } else if (length === undefined && typeof offset === "string") {
          encoding = offset;
          length = this.length;
          offset = 0;
        } else if (isFinite(offset)) {
          offset = offset >>> 0;

          if (isFinite(length)) {
            length = length >>> 0;
            if (encoding === undefined) encoding = "utf8";
          } else {
            encoding = length;
            length = undefined;
          }
        } else {
          throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
        }

        var remaining = this.length - offset;
        if (length === undefined || length > remaining) length = remaining;

        if (string.length > 0 && (length < 0 || offset < 0) || offset > this.length) {
          throw new RangeError("Attempt to write outside buffer bounds");
        }

        if (!encoding) encoding = "utf8";
        var loweredCase = false;

        for (;;) {
          switch (encoding) {
            case "hex":
              return hexWrite(this, string, offset, length);

            case "utf8":
            case "utf-8":
              return utf8Write(this, string, offset, length);

            case "ascii":
              return asciiWrite(this, string, offset, length);

            case "latin1":
            case "binary":
              return latin1Write(this, string, offset, length);

            case "base64":
              return base64Write(this, string, offset, length);

            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return ucs2Write(this, string, offset, length);

            default:
              if (loweredCase) throw new TypeError("Unknown encoding: " + encoding);
              encoding = ("" + encoding).toLowerCase();
              loweredCase = true;
          }
        }
      };

      Buffer.prototype.toJSON = function toJSON() {
        return {
          type: "Buffer",
          data: Array.prototype.slice.call(this._arr || this, 0)
        };
      };

      function base64Slice(buf, start, end) {
        if (start === 0 && end === buf.length) {
          return base64.fromByteArray(buf);
        } else {
          return base64.fromByteArray(buf.slice(start, end));
        }
      }

      function utf8Slice(buf, start, end) {
        end = Math.min(buf.length, end);
        var res = [];
        var i = start;

        while (i < end) {
          var firstByte = buf[i];
          var codePoint = null;
          var bytesPerSequence = firstByte > 239 ? 4 : firstByte > 223 ? 3 : firstByte > 191 ? 2 : 1;

          if (i + bytesPerSequence <= end) {
            var secondByte, thirdByte, fourthByte, tempCodePoint;

            switch (bytesPerSequence) {
              case 1:
                if (firstByte < 128) {
                  codePoint = firstByte;
                }

                break;

              case 2:
                secondByte = buf[i + 1];

                if ((secondByte & 192) === 128) {
                  tempCodePoint = (firstByte & 31) << 6 | secondByte & 63;

                  if (tempCodePoint > 127) {
                    codePoint = tempCodePoint;
                  }
                }

                break;

              case 3:
                secondByte = buf[i + 1];
                thirdByte = buf[i + 2];

                if ((secondByte & 192) === 128 && (thirdByte & 192) === 128) {
                  tempCodePoint = (firstByte & 15) << 12 | (secondByte & 63) << 6 | thirdByte & 63;

                  if (tempCodePoint > 2047 && (tempCodePoint < 55296 || tempCodePoint > 57343)) {
                    codePoint = tempCodePoint;
                  }
                }

                break;

              case 4:
                secondByte = buf[i + 1];
                thirdByte = buf[i + 2];
                fourthByte = buf[i + 3];

                if ((secondByte & 192) === 128 && (thirdByte & 192) === 128 && (fourthByte & 192) === 128) {
                  tempCodePoint = (firstByte & 15) << 18 | (secondByte & 63) << 12 | (thirdByte & 63) << 6 | fourthByte & 63;

                  if (tempCodePoint > 65535 && tempCodePoint < 1114112) {
                    codePoint = tempCodePoint;
                  }
                }

            }
          }

          if (codePoint === null) {
            codePoint = 65533;
            bytesPerSequence = 1;
          } else if (codePoint > 65535) {
            codePoint -= 65536;
            res.push(codePoint >>> 10 & 1023 | 55296);
            codePoint = 56320 | codePoint & 1023;
          }

          res.push(codePoint);
          i += bytesPerSequence;
        }

        return decodeCodePointsArray(res);
      }

      var MAX_ARGUMENTS_LENGTH = 4096;

      function decodeCodePointsArray(codePoints) {
        var len = codePoints.length;

        if (len <= MAX_ARGUMENTS_LENGTH) {
          return String.fromCharCode.apply(String, codePoints);
        }

        var res = "";
        var i = 0;

        while (i < len) {
          res += String.fromCharCode.apply(String, codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH));
        }

        return res;
      }

      function asciiSlice(buf, start, end) {
        var ret = "";
        end = Math.min(buf.length, end);

        for (var i = start; i < end; ++i) {
          ret += String.fromCharCode(buf[i] & 127);
        }

        return ret;
      }

      function latin1Slice(buf, start, end) {
        var ret = "";
        end = Math.min(buf.length, end);

        for (var i = start; i < end; ++i) {
          ret += String.fromCharCode(buf[i]);
        }

        return ret;
      }

      function hexSlice(buf, start, end) {
        var len = buf.length;
        if (!start || start < 0) start = 0;
        if (!end || end < 0 || end > len) end = len;
        var out = "";

        for (var i = start; i < end; ++i) {
          out += toHex(buf[i]);
        }

        return out;
      }

      function utf16leSlice(buf, start, end) {
        var bytes = buf.slice(start, end);
        var res = "";

        for (var i = 0; i < bytes.length; i += 2) {
          res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256);
        }

        return res;
      }

      Buffer.prototype.slice = function slice(start, end) {
        var len = this.length;
        start = ~~start;
        end = end === undefined ? len : ~~end;

        if (start < 0) {
          start += len;
          if (start < 0) start = 0;
        } else if (start > len) {
          start = len;
        }

        if (end < 0) {
          end += len;
          if (end < 0) end = 0;
        } else if (end > len) {
          end = len;
        }

        if (end < start) end = start;
        var newBuf = this.subarray(start, end);
        newBuf.__proto__ = Buffer.prototype;
        return newBuf;
      };

      function checkOffset(offset, ext, length) {
        if (offset % 1 !== 0 || offset < 0) throw new RangeError("offset is not uint");
        if (offset + ext > length) throw new RangeError("Trying to access beyond buffer length");
      }

      Buffer.prototype.readUIntLE = function readUIntLE(offset, byteLength, noAssert) {
        offset = offset >>> 0;
        byteLength = byteLength >>> 0;
        if (!noAssert) checkOffset(offset, byteLength, this.length);
        var val = this[offset];
        var mul = 1;
        var i = 0;

        while (++i < byteLength && (mul *= 256)) {
          val += this[offset + i] * mul;
        }

        return val;
      };

      Buffer.prototype.readUIntBE = function readUIntBE(offset, byteLength, noAssert) {
        offset = offset >>> 0;
        byteLength = byteLength >>> 0;

        if (!noAssert) {
          checkOffset(offset, byteLength, this.length);
        }

        var val = this[offset + --byteLength];
        var mul = 1;

        while (byteLength > 0 && (mul *= 256)) {
          val += this[offset + --byteLength] * mul;
        }

        return val;
      };

      Buffer.prototype.readUInt8 = function readUInt8(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 1, this.length);
        return this[offset];
      };

      Buffer.prototype.readUInt16LE = function readUInt16LE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 2, this.length);
        return this[offset] | this[offset + 1] << 8;
      };

      Buffer.prototype.readUInt16BE = function readUInt16BE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 2, this.length);
        return this[offset] << 8 | this[offset + 1];
      };

      Buffer.prototype.readUInt32LE = function readUInt32LE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 4, this.length);
        return (this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16) + this[offset + 3] * 16777216;
      };

      Buffer.prototype.readUInt32BE = function readUInt32BE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 4, this.length);
        return this[offset] * 16777216 + (this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3]);
      };

      Buffer.prototype.readIntLE = function readIntLE(offset, byteLength, noAssert) {
        offset = offset >>> 0;
        byteLength = byteLength >>> 0;
        if (!noAssert) checkOffset(offset, byteLength, this.length);
        var val = this[offset];
        var mul = 1;
        var i = 0;

        while (++i < byteLength && (mul *= 256)) {
          val += this[offset + i] * mul;
        }

        mul *= 128;
        if (val >= mul) val -= Math.pow(2, 8 * byteLength);
        return val;
      };

      Buffer.prototype.readIntBE = function readIntBE(offset, byteLength, noAssert) {
        offset = offset >>> 0;
        byteLength = byteLength >>> 0;
        if (!noAssert) checkOffset(offset, byteLength, this.length);
        var i = byteLength;
        var mul = 1;
        var val = this[offset + --i];

        while (i > 0 && (mul *= 256)) {
          val += this[offset + --i] * mul;
        }

        mul *= 128;
        if (val >= mul) val -= Math.pow(2, 8 * byteLength);
        return val;
      };

      Buffer.prototype.readInt8 = function readInt8(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 1, this.length);
        if (!(this[offset] & 128)) return this[offset];
        return (255 - this[offset] + 1) * -1;
      };

      Buffer.prototype.readInt16LE = function readInt16LE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 2, this.length);
        var val = this[offset] | this[offset + 1] << 8;
        return val & 32768 ? val | 4294901760 : val;
      };

      Buffer.prototype.readInt16BE = function readInt16BE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 2, this.length);
        var val = this[offset + 1] | this[offset] << 8;
        return val & 32768 ? val | 4294901760 : val;
      };

      Buffer.prototype.readInt32LE = function readInt32LE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 4, this.length);
        return this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16 | this[offset + 3] << 24;
      };

      Buffer.prototype.readInt32BE = function readInt32BE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 4, this.length);
        return this[offset] << 24 | this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3];
      };

      Buffer.prototype.readFloatLE = function readFloatLE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 4, this.length);
        return ieee754.read(this, offset, true, 23, 4);
      };

      Buffer.prototype.readFloatBE = function readFloatBE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 4, this.length);
        return ieee754.read(this, offset, false, 23, 4);
      };

      Buffer.prototype.readDoubleLE = function readDoubleLE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 8, this.length);
        return ieee754.read(this, offset, true, 52, 8);
      };

      Buffer.prototype.readDoubleBE = function readDoubleBE(offset, noAssert) {
        offset = offset >>> 0;
        if (!noAssert) checkOffset(offset, 8, this.length);
        return ieee754.read(this, offset, false, 52, 8);
      };

      function checkInt(buf, value, offset, ext, max, min) {
        if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance');
        if (value > max || value < min) throw new RangeError('"value" argument is out of bounds');
        if (offset + ext > buf.length) throw new RangeError("Index out of range");
      }

      Buffer.prototype.writeUIntLE = function writeUIntLE(value, offset, byteLength, noAssert) {
        value = +value;
        offset = offset >>> 0;
        byteLength = byteLength >>> 0;

        if (!noAssert) {
          var maxBytes = Math.pow(2, 8 * byteLength) - 1;
          checkInt(this, value, offset, byteLength, maxBytes, 0);
        }

        var mul = 1;
        var i = 0;
        this[offset] = value & 255;

        while (++i < byteLength && (mul *= 256)) {
          this[offset + i] = value / mul & 255;
        }

        return offset + byteLength;
      };

      Buffer.prototype.writeUIntBE = function writeUIntBE(value, offset, byteLength, noAssert) {
        value = +value;
        offset = offset >>> 0;
        byteLength = byteLength >>> 0;

        if (!noAssert) {
          var maxBytes = Math.pow(2, 8 * byteLength) - 1;
          checkInt(this, value, offset, byteLength, maxBytes, 0);
        }

        var i = byteLength - 1;
        var mul = 1;
        this[offset + i] = value & 255;

        while (--i >= 0 && (mul *= 256)) {
          this[offset + i] = value / mul & 255;
        }

        return offset + byteLength;
      };

      Buffer.prototype.writeUInt8 = function writeUInt8(value, offset, noAssert) {
        value = +value;
        offset = offset >>> 0;
        if (!noAssert) checkInt(this, value, offset, 1, 255, 0);
        this[offset] = value & 255;
        return offset + 1;
      };

      Buffer.prototype.writeUInt16LE = function writeUInt16LE(value, offset, noAssert) {
        value = +value;
        offset = offset >>> 0;
        if (!noAssert) checkInt(this, value, offset, 2, 65535, 0);
        this[offset] = value & 255;
        this[offset + 1] = value >>> 8;
        return offset + 2;
      };

      Buffer.prototype.writeUInt16BE = function writeUInt16BE(value, offset, noAssert) {
        value = +value;
        offset = offset >>> 0;
        if (!noAssert) checkInt(this, value, offset, 2, 65535, 0);
        this[offset] = value >>> 8;
        this[offset + 1] = value & 255;
        return offset + 2;
      };

      Buffer.prototype.writeUInt32LE = function writeUInt32LE(value, offset, noAssert) {
        value = +value;
        offset = offset >>> 0;
        if (!noAssert) checkInt(this, value, offset, 4, 4294967295, 0);
        this[offset + 3] = value >>> 24;
        this[offset + 2] = value >>> 16;
        this[offset + 1] = value >>> 8;
        this[offset] = value & 255;
        return offset + 4;
      };

      Buffer.prototype.writeUInt32BE = function writeUInt32BE(value, offset, noAssert) {
        value = +value;
        offset = offset >>> 0;
        if (!noAssert) checkInt(this, value, offset, 4, 4294967295, 0);
        this[offset] = value >>> 24;
        this[offset + 1] = value >>> 16;
        this[offset + 2] = value >>> 8;
        this[offset + 3] = value & 255;
        return offset + 4;
      };

      Buffer.prototype.writeIntLE = function writeIntLE(value, offset, byteLength, noAssert) {
        value = +value;
        offset = offset >>> 0;

        if (!noAssert) {
          var limit = Math.pow(2, 8 * byteLength - 1);
          checkInt(this, value, offset, byteLength, limit - 1, -limit);
        }

        var i = 0;
        var mul = 1;
        var sub = 0;
        this[offset] = value & 255;

        while (++i < byteLength && (mul *= 256)) {
          if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
            sub = 1;
          }

          this[offset + i] = (value / mul >> 0) - sub & 255;
        }

        return offset + byteLength;
      };

      Buffer.prototype.writeIntBE = function writeIntBE(value, offset, byteLength, noAssert) {
        value = +value;
        offset = offset >>> 0;

        if (!noAssert) {
          var limit = Math.pow(2, 8 * byteLength - 1);
          checkInt(this, value, offset, byteLength, limit - 1, -limit);
        }

        var i = byteLength - 1;
        var mul = 1;
        var sub = 0;
        this[offset + i] = value & 255;

        while (--i >= 0 && (mul *= 256)) {
          if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
            sub = 1;
          }

          this[offset + i] = (value / mul >> 0) - sub & 255;
        }

        return offset + byteLength;
      };

      Buffer.prototype.writeInt8 = function writeInt8(value, offset, noAssert) {
        value = +value;
        offset = offset >>> 0;
        if (!noAssert) checkInt(this, value, offset, 1, 127, -128);
        if (value < 0) value = 255 + value + 1;
        this[offset] = value & 255;
        return offset + 1;
      };

      Buffer.prototype.writeInt16LE = function writeInt16LE(value, offset, noAssert) {
        value = +value;
        offset = offset >>> 0;
        if (!noAssert) checkInt(this, value, offset, 2, 32767, -32768);
        this[offset] = value & 255;
        this[offset + 1] = value >>> 8;
        return offset + 2;
      };

      Buffer.prototype.writeInt16BE = function writeInt16BE(value, offset, noAssert) {
        value = +value;
        offset = offset >>> 0;
        if (!noAssert) checkInt(this, value, offset, 2, 32767, -32768);
        this[offset] = value >>> 8;
        this[offset + 1] = value & 255;
        return offset + 2;
      };

      Buffer.prototype.writeInt32LE = function writeInt32LE(value, offset, noAssert) {
        value = +value;
        offset = offset >>> 0;
        if (!noAssert) checkInt(this, value, offset, 4, 2147483647, -2147483648);
        this[offset] = value & 255;
        this[offset + 1] = value >>> 8;
        this[offset + 2] = value >>> 16;
        this[offset + 3] = value >>> 24;
        return offset + 4;
      };

      Buffer.prototype.writeInt32BE = function writeInt32BE(value, offset, noAssert) {
        value = +value;
        offset = offset >>> 0;
        if (!noAssert) checkInt(this, value, offset, 4, 2147483647, -2147483648);
        if (value < 0) value = 4294967295 + value + 1;
        this[offset] = value >>> 24;
        this[offset + 1] = value >>> 16;
        this[offset + 2] = value >>> 8;
        this[offset + 3] = value & 255;
        return offset + 4;
      };

      function checkIEEE754(buf, value, offset, ext, max, min) {
        if (offset + ext > buf.length) throw new RangeError("Index out of range");
        if (offset < 0) throw new RangeError("Index out of range");
      }

      function writeFloat(buf, value, offset, littleEndian, noAssert) {
        value = +value;
        offset = offset >>> 0;

        if (!noAssert) {
          checkIEEE754(buf, value, offset, 4, 34028234663852886e22, -34028234663852886e22);
        }

        ieee754.write(buf, value, offset, littleEndian, 23, 4);
        return offset + 4;
      }

      Buffer.prototype.writeFloatLE = function writeFloatLE(value, offset, noAssert) {
        return writeFloat(this, value, offset, true, noAssert);
      };

      Buffer.prototype.writeFloatBE = function writeFloatBE(value, offset, noAssert) {
        return writeFloat(this, value, offset, false, noAssert);
      };

      function writeDouble(buf, value, offset, littleEndian, noAssert) {
        value = +value;
        offset = offset >>> 0;

        if (!noAssert) {
          checkIEEE754(buf, value, offset, 8, 17976931348623157e292, -17976931348623157e292);
        }

        ieee754.write(buf, value, offset, littleEndian, 52, 8);
        return offset + 8;
      }

      Buffer.prototype.writeDoubleLE = function writeDoubleLE(value, offset, noAssert) {
        return writeDouble(this, value, offset, true, noAssert);
      };

      Buffer.prototype.writeDoubleBE = function writeDoubleBE(value, offset, noAssert) {
        return writeDouble(this, value, offset, false, noAssert);
      };

      Buffer.prototype.copy = function copy(target, targetStart, start, end) {
        if (!start) start = 0;
        if (!end && end !== 0) end = this.length;
        if (targetStart >= target.length) targetStart = target.length;
        if (!targetStart) targetStart = 0;
        if (end > 0 && end < start) end = start;
        if (end === start) return 0;
        if (target.length === 0 || this.length === 0) return 0;

        if (targetStart < 0) {
          throw new RangeError("targetStart out of bounds");
        }

        if (start < 0 || start >= this.length) throw new RangeError("sourceStart out of bounds");
        if (end < 0) throw new RangeError("sourceEnd out of bounds");
        if (end > this.length) end = this.length;

        if (target.length - targetStart < end - start) {
          end = target.length - targetStart + start;
        }

        var len = end - start;
        var i;

        if (this === target && start < targetStart && targetStart < end) {
          for (i = len - 1; i >= 0; --i) {
            target[i + targetStart] = this[i + start];
          }
        } else if (len < 1e3) {
          for (i = 0; i < len; ++i) {
            target[i + targetStart] = this[i + start];
          }
        } else {
          Uint8Array.prototype.set.call(target, this.subarray(start, start + len), targetStart);
        }

        return len;
      };

      Buffer.prototype.fill = function fill(val, start, end, encoding) {
        if (typeof val === "string") {
          if (typeof start === "string") {
            encoding = start;
            start = 0;
            end = this.length;
          } else if (typeof end === "string") {
            encoding = end;
            end = this.length;
          }

          if (val.length === 1) {
            var code = val.charCodeAt(0);

            if (code < 256) {
              val = code;
            }
          }

          if (encoding !== undefined && typeof encoding !== "string") {
            throw new TypeError("encoding must be a string");
          }

          if (typeof encoding === "string" && !Buffer.isEncoding(encoding)) {
            throw new TypeError("Unknown encoding: " + encoding);
          }
        } else if (typeof val === "number") {
          val = val & 255;
        }

        if (start < 0 || this.length < start || this.length < end) {
          throw new RangeError("Out of range index");
        }

        if (end <= start) {
          return this;
        }

        start = start >>> 0;
        end = end === undefined ? this.length : end >>> 0;
        if (!val) val = 0;
        var i;

        if (typeof val === "number") {
          for (i = start; i < end; ++i) {
            this[i] = val;
          }
        } else {
          var bytes = Buffer.isBuffer(val) ? val : new Buffer(val, encoding);
          var len = bytes.length;

          for (i = 0; i < end - start; ++i) {
            this[i + start] = bytes[i % len];
          }
        }

        return this;
      };

      var INVALID_BASE64_RE = /[^+/0-9A-Za-z-_]/g;

      function base64clean(str) {
        str = str.trim().replace(INVALID_BASE64_RE, "");
        if (str.length < 2) return "";

        while (str.length % 4 !== 0) {
          str = str + "=";
        }

        return str;
      }

      function toHex(n) {
        if (n < 16) return "0" + n.toString(16);
        return n.toString(16);
      }

      function utf8ToBytes(string, units) {
        units = units || Infinity;
        var codePoint;
        var length = string.length;
        var leadSurrogate = null;
        var bytes = [];

        for (var i = 0; i < length; ++i) {
          codePoint = string.charCodeAt(i);

          if (codePoint > 55295 && codePoint < 57344) {
            if (!leadSurrogate) {
              if (codePoint > 56319) {
                if ((units -= 3) > -1) bytes.push(239, 191, 189);
                continue;
              } else if (i + 1 === length) {
                if ((units -= 3) > -1) bytes.push(239, 191, 189);
                continue;
              }

              leadSurrogate = codePoint;
              continue;
            }

            if (codePoint < 56320) {
              if ((units -= 3) > -1) bytes.push(239, 191, 189);
              leadSurrogate = codePoint;
              continue;
            }

            codePoint = (leadSurrogate - 55296 << 10 | codePoint - 56320) + 65536;
          } else if (leadSurrogate) {
            if ((units -= 3) > -1) bytes.push(239, 191, 189);
          }

          leadSurrogate = null;

          if (codePoint < 128) {
            if ((units -= 1) < 0) break;
            bytes.push(codePoint);
          } else if (codePoint < 2048) {
            if ((units -= 2) < 0) break;
            bytes.push(codePoint >> 6 | 192, codePoint & 63 | 128);
          } else if (codePoint < 65536) {
            if ((units -= 3) < 0) break;
            bytes.push(codePoint >> 12 | 224, codePoint >> 6 & 63 | 128, codePoint & 63 | 128);
          } else if (codePoint < 1114112) {
            if ((units -= 4) < 0) break;
            bytes.push(codePoint >> 18 | 240, codePoint >> 12 & 63 | 128, codePoint >> 6 & 63 | 128, codePoint & 63 | 128);
          } else {
            throw new Error("Invalid code point");
          }
        }

        return bytes;
      }

      function asciiToBytes(str) {
        var byteArray = [];

        for (var i = 0; i < str.length; ++i) {
          byteArray.push(str.charCodeAt(i) & 255);
        }

        return byteArray;
      }

      function utf16leToBytes(str, units) {
        var c, hi, lo;
        var byteArray = [];

        for (var i = 0; i < str.length; ++i) {
          if ((units -= 2) < 0) break;
          c = str.charCodeAt(i);
          hi = c >> 8;
          lo = c % 256;
          byteArray.push(lo);
          byteArray.push(hi);
        }

        return byteArray;
      }

      function base64ToBytes(str) {
        return base64.toByteArray(base64clean(str));
      }

      function blitBuffer(src, dst, offset, length) {
        for (var i = 0; i < length; ++i) {
          if (i + offset >= dst.length || i >= src.length) break;
          dst[i + offset] = src[i];
        }

        return i;
      }

      function isArrayBuffer(obj) {
        return obj instanceof ArrayBuffer || obj != null && obj.constructor != null && obj.constructor.name === "ArrayBuffer" && typeof obj.byteLength === "number";
      }

      function isArrayBufferView(obj) {
        return typeof ArrayBuffer.isView === "function" && ArrayBuffer.isView(obj);
      }

      function numberIsNaN(obj) {
        return obj !== obj;
      }
    }, {
      "base64-js": 6,
      ieee754: 12
    }],
    10: [function (require, module, exports) {
      (function (Buffer) {
        function isArray(arg) {
          if (Array.isArray) {
            return Array.isArray(arg);
          }

          return objectToString(arg) === "[object Array]";
        }

        exports.isArray = isArray;

        function isBoolean(arg) {
          return typeof arg === "boolean";
        }

        exports.isBoolean = isBoolean;

        function isNull(arg) {
          return arg === null;
        }

        exports.isNull = isNull;

        function isNullOrUndefined(arg) {
          return arg == null;
        }

        exports.isNullOrUndefined = isNullOrUndefined;

        function isNumber(arg) {
          return typeof arg === "number";
        }

        exports.isNumber = isNumber;

        function isString(arg) {
          return typeof arg === "string";
        }

        exports.isString = isString;

        function isSymbol(arg) {
          return _typeof(arg) === "symbol";
        }

        exports.isSymbol = isSymbol;

        function isUndefined(arg) {
          return arg === void 0;
        }

        exports.isUndefined = isUndefined;

        function isRegExp(re) {
          return objectToString(re) === "[object RegExp]";
        }

        exports.isRegExp = isRegExp;

        function isObject(arg) {
          return _typeof(arg) === "object" && arg !== null;
        }

        exports.isObject = isObject;

        function isDate(d) {
          return objectToString(d) === "[object Date]";
        }

        exports.isDate = isDate;

        function isError(e) {
          return objectToString(e) === "[object Error]" || e instanceof Error;
        }

        exports.isError = isError;

        function isFunction(arg) {
          return typeof arg === "function";
        }

        exports.isFunction = isFunction;

        function isPrimitive(arg) {
          return arg === null || typeof arg === "boolean" || typeof arg === "number" || typeof arg === "string" || _typeof(arg) === "symbol" || typeof arg === "undefined";
        }

        exports.isPrimitive = isPrimitive;
        exports.isBuffer = Buffer.isBuffer;

        function objectToString(o) {
          return Object.prototype.toString.call(o);
        }
      }).call(this, {
        isBuffer: require("../../is-buffer/index.js")
      });
    }, {
      "../../is-buffer/index.js": 14
    }],
    11: [function (require, module, exports) {
      function EventEmitter() {
        this._events = this._events || {};
        this._maxListeners = this._maxListeners || undefined;
      }

      module.exports = EventEmitter;
      EventEmitter.EventEmitter = EventEmitter;
      EventEmitter.prototype._events = undefined;
      EventEmitter.prototype._maxListeners = undefined;
      EventEmitter.defaultMaxListeners = 10;

      EventEmitter.prototype.setMaxListeners = function (n) {
        if (!isNumber(n) || n < 0 || isNaN(n)) throw TypeError("n must be a positive number");
        this._maxListeners = n;
        return this;
      };

      EventEmitter.prototype.emit = function (type) {
        var er, handler, len, args, i, listeners;
        if (!this._events) this._events = {};

        if (type === "error") {
          if (!this._events.error || isObject(this._events.error) && !this._events.error.length) {
            er = arguments[1];

            if (er instanceof Error) {
              throw er;
            } else {
              var err = new Error('Uncaught, unspecified "error" event. (' + er + ")");
              err.context = er;
              throw err;
            }
          }
        }

        handler = this._events[type];
        if (isUndefined(handler)) return false;

        if (isFunction(handler)) {
          switch (arguments.length) {
            case 1:
              handler.call(this);
              break;

            case 2:
              handler.call(this, arguments[1]);
              break;

            case 3:
              handler.call(this, arguments[1], arguments[2]);
              break;

            default:
              args = Array.prototype.slice.call(arguments, 1);
              handler.apply(this, args);
          }
        } else if (isObject(handler)) {
          args = Array.prototype.slice.call(arguments, 1);
          listeners = handler.slice();
          len = listeners.length;

          for (i = 0; i < len; i++) {
            listeners[i].apply(this, args);
          }
        }

        return true;
      };

      EventEmitter.prototype.addListener = function (type, listener) {
        var m;
        if (!isFunction(listener)) throw TypeError("listener must be a function");
        if (!this._events) this._events = {};
        if (this._events.newListener) this.emit("newListener", type, isFunction(listener.listener) ? listener.listener : listener);
        if (!this._events[type]) this._events[type] = listener;else if (isObject(this._events[type])) this._events[type].push(listener);else this._events[type] = [this._events[type], listener];

        if (isObject(this._events[type]) && !this._events[type].warned) {
          if (!isUndefined(this._maxListeners)) {
            m = this._maxListeners;
          } else {
            m = EventEmitter.defaultMaxListeners;
          }

          if (m && m > 0 && this._events[type].length > m) {
            this._events[type].warned = true;
            console.error("(node) warning: possible EventEmitter memory " + "leak detected. %d listeners added. " + "Use emitter.setMaxListeners() to increase limit.", this._events[type].length);

            if (typeof console.trace === "function") {
              console.trace();
            }
          }
        }

        return this;
      };

      EventEmitter.prototype.on = EventEmitter.prototype.addListener;

      EventEmitter.prototype.once = function (type, listener) {
        if (!isFunction(listener)) throw TypeError("listener must be a function");
        var fired = false;

        function g() {
          this.removeListener(type, g);

          if (!fired) {
            fired = true;
            listener.apply(this, arguments);
          }
        }

        g.listener = listener;
        this.on(type, g);
        return this;
      };

      EventEmitter.prototype.removeListener = function (type, listener) {
        var list, position, length, i;
        if (!isFunction(listener)) throw TypeError("listener must be a function");
        if (!this._events || !this._events[type]) return this;
        list = this._events[type];
        length = list.length;
        position = -1;

        if (list === listener || isFunction(list.listener) && list.listener === listener) {
          delete this._events[type];
          if (this._events.removeListener) this.emit("removeListener", type, listener);
        } else if (isObject(list)) {
          for (i = length; i-- > 0;) {
            if (list[i] === listener || list[i].listener && list[i].listener === listener) {
              position = i;
              break;
            }
          }

          if (position < 0) return this;

          if (list.length === 1) {
            list.length = 0;
            delete this._events[type];
          } else {
            list.splice(position, 1);
          }

          if (this._events.removeListener) this.emit("removeListener", type, listener);
        }

        return this;
      };

      EventEmitter.prototype.removeAllListeners = function (type) {
        var key, listeners;
        if (!this._events) return this;

        if (!this._events.removeListener) {
          if (arguments.length === 0) this._events = {};else if (this._events[type]) delete this._events[type];
          return this;
        }

        if (arguments.length === 0) {
          for (key in this._events) {
            if (key === "removeListener") continue;
            this.removeAllListeners(key);
          }

          this.removeAllListeners("removeListener");
          this._events = {};
          return this;
        }

        listeners = this._events[type];

        if (isFunction(listeners)) {
          this.removeListener(type, listeners);
        } else if (listeners) {
          while (listeners.length) {
            this.removeListener(type, listeners[listeners.length - 1]);
          }
        }

        delete this._events[type];
        return this;
      };

      EventEmitter.prototype.listeners = function (type) {
        var ret;
        if (!this._events || !this._events[type]) ret = [];else if (isFunction(this._events[type])) ret = [this._events[type]];else ret = this._events[type].slice();
        return ret;
      };

      EventEmitter.prototype.listenerCount = function (type) {
        if (this._events) {
          var evlistener = this._events[type];
          if (isFunction(evlistener)) return 1;else if (evlistener) return evlistener.length;
        }

        return 0;
      };

      EventEmitter.listenerCount = function (emitter, type) {
        return emitter.listenerCount(type);
      };

      function isFunction(arg) {
        return typeof arg === "function";
      }

      function isNumber(arg) {
        return typeof arg === "number";
      }

      function isObject(arg) {
        return _typeof(arg) === "object" && arg !== null;
      }

      function isUndefined(arg) {
        return arg === void 0;
      }
    }, {}],
    12: [function (require, module, exports) {
      exports.read = function (buffer, offset, isLE, mLen, nBytes) {
        var e, m;
        var eLen = nBytes * 8 - mLen - 1;
        var eMax = (1 << eLen) - 1;
        var eBias = eMax >> 1;
        var nBits = -7;
        var i = isLE ? nBytes - 1 : 0;
        var d = isLE ? -1 : 1;
        var s = buffer[offset + i];
        i += d;
        e = s & (1 << -nBits) - 1;
        s >>= -nBits;
        nBits += eLen;

        for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

        m = e & (1 << -nBits) - 1;
        e >>= -nBits;
        nBits += mLen;

        for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

        if (e === 0) {
          e = 1 - eBias;
        } else if (e === eMax) {
          return m ? NaN : (s ? -1 : 1) * Infinity;
        } else {
          m = m + Math.pow(2, mLen);
          e = e - eBias;
        }

        return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
      };

      exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
        var e, m, c;
        var eLen = nBytes * 8 - mLen - 1;
        var eMax = (1 << eLen) - 1;
        var eBias = eMax >> 1;
        var rt = mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0;
        var i = isLE ? 0 : nBytes - 1;
        var d = isLE ? 1 : -1;
        var s = value < 0 || value === 0 && 1 / value < 0 ? 1 : 0;
        value = Math.abs(value);

        if (isNaN(value) || value === Infinity) {
          m = isNaN(value) ? 1 : 0;
          e = eMax;
        } else {
          e = Math.floor(Math.log(value) / Math.LN2);

          if (value * (c = Math.pow(2, -e)) < 1) {
            e--;
            c *= 2;
          }

          if (e + eBias >= 1) {
            value += rt / c;
          } else {
            value += rt * Math.pow(2, 1 - eBias);
          }

          if (value * c >= 2) {
            e++;
            c /= 2;
          }

          if (e + eBias >= eMax) {
            m = 0;
            e = eMax;
          } else if (e + eBias >= 1) {
            m = (value * c - 1) * Math.pow(2, mLen);
            e = e + eBias;
          } else {
            m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
            e = 0;
          }
        }

        for (; mLen >= 8; buffer[offset + i] = m & 255, i += d, m /= 256, mLen -= 8) {}

        e = e << mLen | m;
        eLen += mLen;

        for (; eLen > 0; buffer[offset + i] = e & 255, i += d, e /= 256, eLen -= 8) {}

        buffer[offset + i - d] |= s * 128;
      };
    }, {}],
    13: [function (require, module, exports) {
      if (typeof Object.create === "function") {
        module.exports = function inherits(ctor, superCtor) {
          ctor.super_ = superCtor;
          ctor.prototype = Object.create(superCtor.prototype, {
            constructor: {
              value: ctor,
              enumerable: false,
              writable: true,
              configurable: true
            }
          });
        };
      } else {
        module.exports = function inherits(ctor, superCtor) {
          ctor.super_ = superCtor;

          var TempCtor = function TempCtor() {};

          TempCtor.prototype = superCtor.prototype;
          ctor.prototype = new TempCtor();
          ctor.prototype.constructor = ctor;
        };
      }
    }, {}],
    14: [function (require, module, exports) {
      module.exports = function (obj) {
        return obj != null && (isBuffer(obj) || isSlowBuffer(obj) || !!obj._isBuffer);
      };

      function isBuffer(obj) {
        return !!obj.constructor && typeof obj.constructor.isBuffer === "function" && obj.constructor.isBuffer(obj);
      }

      function isSlowBuffer(obj) {
        return typeof obj.readFloatLE === "function" && typeof obj.slice === "function" && isBuffer(obj.slice(0, 0));
      }
    }, {}],
    15: [function (require, module, exports) {
      var toString = {}.toString;

      module.exports = Array.isArray || function (arr) {
        return toString.call(arr) == "[object Array]";
      };
    }, {}],
    16: [function (require, module, exports) {
      (function (process) {
        "use strict";

        if (!process.version || process.version.indexOf("v0.") === 0 || process.version.indexOf("v1.") === 0 && process.version.indexOf("v1.8.") !== 0) {
          module.exports = nextTick;
        } else {
          module.exports = process.nextTick;
        }

        function nextTick(fn, arg1, arg2, arg3) {
          if (typeof fn !== "function") {
            throw new TypeError('"callback" argument must be a function');
          }

          var len = arguments.length;
          var args, i;

          switch (len) {
            case 0:
            case 1:
              return process.nextTick(fn);

            case 2:
              return process.nextTick(function afterTickOne() {
                fn.call(null, arg1);
              });

            case 3:
              return process.nextTick(function afterTickTwo() {
                fn.call(null, arg1, arg2);
              });

            case 4:
              return process.nextTick(function afterTickThree() {
                fn.call(null, arg1, arg2, arg3);
              });

            default:
              args = new Array(len - 1);
              i = 0;

              while (i < args.length) {
                args[i++] = arguments[i];
              }

              return process.nextTick(function afterTick() {
                fn.apply(null, args);
              });
          }
        }
      }).call(this, require("_process"));
    }, {
      _process: 17
    }],
    17: [function (require, module, exports) {
      var process = module.exports = {};
      var cachedSetTimeout;
      var cachedClearTimeout;

      function defaultSetTimout() {
        throw new Error("setTimeout has not been defined");
      }

      function defaultClearTimeout() {
        throw new Error("clearTimeout has not been defined");
      }

      (function () {
        try {
          if (typeof setTimeout === "function") {
            cachedSetTimeout = setTimeout;
          } else {
            cachedSetTimeout = defaultSetTimout;
          }
        } catch (e) {
          cachedSetTimeout = defaultSetTimout;
        }

        try {
          if (typeof clearTimeout === "function") {
            cachedClearTimeout = clearTimeout;
          } else {
            cachedClearTimeout = defaultClearTimeout;
          }
        } catch (e) {
          cachedClearTimeout = defaultClearTimeout;
        }
      })();

      function runTimeout(fun) {
        if (cachedSetTimeout === setTimeout) {
          return setTimeout(fun, 0);
        }

        if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
          cachedSetTimeout = setTimeout;
          return setTimeout(fun, 0);
        }

        try {
          return cachedSetTimeout(fun, 0);
        } catch (e) {
          try {
            return cachedSetTimeout.call(null, fun, 0);
          } catch (e) {
            return cachedSetTimeout.call(this, fun, 0);
          }
        }
      }

      function runClearTimeout(marker) {
        if (cachedClearTimeout === clearTimeout) {
          return clearTimeout(marker);
        }

        if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
          cachedClearTimeout = clearTimeout;
          return clearTimeout(marker);
        }

        try {
          return cachedClearTimeout(marker);
        } catch (e) {
          try {
            return cachedClearTimeout.call(null, marker);
          } catch (e) {
            return cachedClearTimeout.call(this, marker);
          }
        }
      }

      var queue = [];
      var draining = false;
      var currentQueue;
      var queueIndex = -1;

      function cleanUpNextTick() {
        if (!draining || !currentQueue) {
          return;
        }

        draining = false;

        if (currentQueue.length) {
          queue = currentQueue.concat(queue);
        } else {
          queueIndex = -1;
        }

        if (queue.length) {
          drainQueue();
        }
      }

      function drainQueue() {
        if (draining) {
          return;
        }

        var timeout = runTimeout(cleanUpNextTick);
        draining = true;
        var len = queue.length;

        while (len) {
          currentQueue = queue;
          queue = [];

          while (++queueIndex < len) {
            if (currentQueue) {
              currentQueue[queueIndex].run();
            }
          }

          queueIndex = -1;
          len = queue.length;
        }

        currentQueue = null;
        draining = false;
        runClearTimeout(timeout);
      }

      process.nextTick = function (fun) {
        var args = new Array(arguments.length - 1);

        if (arguments.length > 1) {
          for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
          }
        }

        queue.push(new Item(fun, args));

        if (queue.length === 1 && !draining) {
          runTimeout(drainQueue);
        }
      };

      function Item(fun, array) {
        this.fun = fun;
        this.array = array;
      }

      Item.prototype.run = function () {
        this.fun.apply(null, this.array);
      };

      process.title = "browser";
      process.browser = true;
      process.env = {};
      process.argv = [];
      process.version = "";
      process.versions = {};

      function noop() {}

      process.on = noop;
      process.addListener = noop;
      process.once = noop;
      process.off = noop;
      process.removeListener = noop;
      process.removeAllListeners = noop;
      process.emit = noop;
      process.prependListener = noop;
      process.prependOnceListener = noop;

      process.listeners = function (name) {
        return [];
      };

      process.binding = function (name) {
        throw new Error("process.binding is not supported");
      };

      process.cwd = function () {
        return "/";
      };

      process.chdir = function (dir) {
        throw new Error("process.chdir is not supported");
      };

      process.umask = function () {
        return 0;
      };
    }, {}],
    18: [function (require, module, exports) {
      module.exports = require("./lib/_stream_duplex.js");
    }, {
      "./lib/_stream_duplex.js": 19
    }],
    19: [function (require, module, exports) {
      "use strict";

      var processNextTick = require("process-nextick-args");

      var objectKeys = Object.keys || function (obj) {
        var keys = [];

        for (var key in obj) {
          keys.push(key);
        }

        return keys;
      };

      module.exports = Duplex;

      var util = require("core-util-is");

      util.inherits = require("inherits");

      var Readable = require("./_stream_readable");

      var Writable = require("./_stream_writable");

      util.inherits(Duplex, Readable);
      var keys = objectKeys(Writable.prototype);

      for (var v = 0; v < keys.length; v++) {
        var method = keys[v];
        if (!Duplex.prototype[method]) Duplex.prototype[method] = Writable.prototype[method];
      }

      function Duplex(options) {
        if (!(this instanceof Duplex)) return new Duplex(options);
        Readable.call(this, options);
        Writable.call(this, options);
        if (options && options.readable === false) this.readable = false;
        if (options && options.writable === false) this.writable = false;
        this.allowHalfOpen = true;
        if (options && options.allowHalfOpen === false) this.allowHalfOpen = false;
        this.once("end", onend);
      }

      function onend() {
        if (this.allowHalfOpen || this._writableState.ended) return;
        processNextTick(onEndNT, this);
      }

      function onEndNT(self) {
        self.end();
      }

      Object.defineProperty(Duplex.prototype, "destroyed", {
        get: function get() {
          if (this._readableState === undefined || this._writableState === undefined) {
            return false;
          }

          return this._readableState.destroyed && this._writableState.destroyed;
        },
        set: function set(value) {
          if (this._readableState === undefined || this._writableState === undefined) {
            return;
          }

          this._readableState.destroyed = value;
          this._writableState.destroyed = value;
        }
      });

      Duplex.prototype._destroy = function (err, cb) {
        this.push(null);
        this.end();
        processNextTick(cb, err);
      };

      function forEach(xs, f) {
        for (var i = 0, l = xs.length; i < l; i++) {
          f(xs[i], i);
        }
      }
    }, {
      "./_stream_readable": 21,
      "./_stream_writable": 23,
      "core-util-is": 10,
      inherits: 13,
      "process-nextick-args": 16
    }],
    20: [function (require, module, exports) {
      "use strict";

      module.exports = PassThrough;

      var Transform = require("./_stream_transform");

      var util = require("core-util-is");

      util.inherits = require("inherits");
      util.inherits(PassThrough, Transform);

      function PassThrough(options) {
        if (!(this instanceof PassThrough)) return new PassThrough(options);
        Transform.call(this, options);
      }

      PassThrough.prototype._transform = function (chunk, encoding, cb) {
        cb(null, chunk);
      };
    }, {
      "./_stream_transform": 22,
      "core-util-is": 10,
      inherits: 13
    }],
    21: [function (require, module, exports) {
      (function (process, global) {
        "use strict";

        var processNextTick = require("process-nextick-args");

        module.exports = Readable;

        var isArray = require("isarray");

        var Duplex;
        Readable.ReadableState = ReadableState;

        var EE = require("events").EventEmitter;

        var EElistenerCount = function EElistenerCount(emitter, type) {
          return emitter.listeners(type).length;
        };

        var Stream = require("./internal/streams/stream");

        var Buffer = require("safe-buffer").Buffer;

        var OurUint8Array = global.Uint8Array || function () {};

        function _uint8ArrayToBuffer(chunk) {
          return Buffer.from(chunk);
        }

        function _isUint8Array(obj) {
          return Buffer.isBuffer(obj) || obj instanceof OurUint8Array;
        }

        var util = require("core-util-is");

        util.inherits = require("inherits");

        var debugUtil = require("util");

        var debug = void 0;

        if (debugUtil && debugUtil.debuglog) {
          debug = debugUtil.debuglog("stream");
        } else {
          debug = function debug() {};
        }

        var BufferList = require("./internal/streams/BufferList");

        var destroyImpl = require("./internal/streams/destroy");

        var StringDecoder;
        util.inherits(Readable, Stream);
        var kProxyEvents = ["error", "close", "destroy", "pause", "resume"];

        function prependListener(emitter, event, fn) {
          if (typeof emitter.prependListener === "function") {
            return emitter.prependListener(event, fn);
          } else {
            if (!emitter._events || !emitter._events[event]) emitter.on(event, fn);else if (isArray(emitter._events[event])) emitter._events[event].unshift(fn);else emitter._events[event] = [fn, emitter._events[event]];
          }
        }

        function ReadableState(options, stream) {
          Duplex = Duplex || require("./_stream_duplex");
          options = options || {};
          this.objectMode = !!options.objectMode;
          if (stream instanceof Duplex) this.objectMode = this.objectMode || !!options.readableObjectMode;
          var hwm = options.highWaterMark;
          var defaultHwm = this.objectMode ? 16 : 16 * 1024;
          this.highWaterMark = hwm || hwm === 0 ? hwm : defaultHwm;
          this.highWaterMark = Math.floor(this.highWaterMark);
          this.buffer = new BufferList();
          this.length = 0;
          this.pipes = null;
          this.pipesCount = 0;
          this.flowing = null;
          this.ended = false;
          this.endEmitted = false;
          this.reading = false;
          this.sync = true;
          this.needReadable = false;
          this.emittedReadable = false;
          this.readableListening = false;
          this.resumeScheduled = false;
          this.destroyed = false;
          this.defaultEncoding = options.defaultEncoding || "utf8";
          this.awaitDrain = 0;
          this.readingMore = false;
          this.decoder = null;
          this.encoding = null;

          if (options.encoding) {
            if (!StringDecoder) StringDecoder = require("string_decoder/").StringDecoder;
            this.decoder = new StringDecoder(options.encoding);
            this.encoding = options.encoding;
          }
        }

        function Readable(options) {
          Duplex = Duplex || require("./_stream_duplex");
          if (!(this instanceof Readable)) return new Readable(options);
          this._readableState = new ReadableState(options, this);
          this.readable = true;

          if (options) {
            if (typeof options.read === "function") this._read = options.read;
            if (typeof options.destroy === "function") this._destroy = options.destroy;
          }

          Stream.call(this);
        }

        Object.defineProperty(Readable.prototype, "destroyed", {
          get: function get() {
            if (this._readableState === undefined) {
              return false;
            }

            return this._readableState.destroyed;
          },
          set: function set(value) {
            if (!this._readableState) {
              return;
            }

            this._readableState.destroyed = value;
          }
        });
        Readable.prototype.destroy = destroyImpl.destroy;
        Readable.prototype._undestroy = destroyImpl.undestroy;

        Readable.prototype._destroy = function (err, cb) {
          this.push(null);
          cb(err);
        };

        Readable.prototype.push = function (chunk, encoding) {
          var state = this._readableState;
          var skipChunkCheck;

          if (!state.objectMode) {
            if (typeof chunk === "string") {
              encoding = encoding || state.defaultEncoding;

              if (encoding !== state.encoding) {
                chunk = Buffer.from(chunk, encoding);
                encoding = "";
              }

              skipChunkCheck = true;
            }
          } else {
            skipChunkCheck = true;
          }

          return readableAddChunk(this, chunk, encoding, false, skipChunkCheck);
        };

        Readable.prototype.unshift = function (chunk) {
          return readableAddChunk(this, chunk, null, true, false);
        };

        function readableAddChunk(stream, chunk, encoding, addToFront, skipChunkCheck) {
          var state = stream._readableState;

          if (chunk === null) {
            state.reading = false;
            onEofChunk(stream, state);
          } else {
            var er;
            if (!skipChunkCheck) er = chunkInvalid(state, chunk);

            if (er) {
              stream.emit("error", er);
            } else if (state.objectMode || chunk && chunk.length > 0) {
              if (typeof chunk !== "string" && !state.objectMode && Object.getPrototypeOf(chunk) !== Buffer.prototype) {
                chunk = _uint8ArrayToBuffer(chunk);
              }

              if (addToFront) {
                if (state.endEmitted) stream.emit("error", new Error("stream.unshift() after end event"));else addChunk(stream, state, chunk, true);
              } else if (state.ended) {
                stream.emit("error", new Error("stream.push() after EOF"));
              } else {
                state.reading = false;

                if (state.decoder && !encoding) {
                  chunk = state.decoder.write(chunk);
                  if (state.objectMode || chunk.length !== 0) addChunk(stream, state, chunk, false);else maybeReadMore(stream, state);
                } else {
                  addChunk(stream, state, chunk, false);
                }
              }
            } else if (!addToFront) {
              state.reading = false;
            }
          }

          return needMoreData(state);
        }

        function addChunk(stream, state, chunk, addToFront) {
          if (state.flowing && state.length === 0 && !state.sync) {
            stream.emit("data", chunk);
            stream.read(0);
          } else {
            state.length += state.objectMode ? 1 : chunk.length;
            if (addToFront) state.buffer.unshift(chunk);else state.buffer.push(chunk);
            if (state.needReadable) emitReadable(stream);
          }

          maybeReadMore(stream, state);
        }

        function chunkInvalid(state, chunk) {
          var er;

          if (!_isUint8Array(chunk) && typeof chunk !== "string" && chunk !== undefined && !state.objectMode) {
            er = new TypeError("Invalid non-string/buffer chunk");
          }

          return er;
        }

        function needMoreData(state) {
          return !state.ended && (state.needReadable || state.length < state.highWaterMark || state.length === 0);
        }

        Readable.prototype.isPaused = function () {
          return this._readableState.flowing === false;
        };

        Readable.prototype.setEncoding = function (enc) {
          if (!StringDecoder) StringDecoder = require("string_decoder/").StringDecoder;
          this._readableState.decoder = new StringDecoder(enc);
          this._readableState.encoding = enc;
          return this;
        };

        var MAX_HWM = 8388608;

        function computeNewHighWaterMark(n) {
          if (n >= MAX_HWM) {
            n = MAX_HWM;
          } else {
            n--;
            n |= n >>> 1;
            n |= n >>> 2;
            n |= n >>> 4;
            n |= n >>> 8;
            n |= n >>> 16;
            n++;
          }

          return n;
        }

        function howMuchToRead(n, state) {
          if (n <= 0 || state.length === 0 && state.ended) return 0;
          if (state.objectMode) return 1;

          if (n !== n) {
            if (state.flowing && state.length) return state.buffer.head.data.length;else return state.length;
          }

          if (n > state.highWaterMark) state.highWaterMark = computeNewHighWaterMark(n);
          if (n <= state.length) return n;

          if (!state.ended) {
            state.needReadable = true;
            return 0;
          }

          return state.length;
        }

        Readable.prototype.read = function (n) {
          debug("read", n);
          n = parseInt(n, 10);
          var state = this._readableState;
          var nOrig = n;
          if (n !== 0) state.emittedReadable = false;

          if (n === 0 && state.needReadable && (state.length >= state.highWaterMark || state.ended)) {
            debug("read: emitReadable", state.length, state.ended);
            if (state.length === 0 && state.ended) endReadable(this);else emitReadable(this);
            return null;
          }

          n = howMuchToRead(n, state);

          if (n === 0 && state.ended) {
            if (state.length === 0) endReadable(this);
            return null;
          }

          var doRead = state.needReadable;
          debug("need readable", doRead);

          if (state.length === 0 || state.length - n < state.highWaterMark) {
            doRead = true;
            debug("length less than watermark", doRead);
          }

          if (state.ended || state.reading) {
            doRead = false;
            debug("reading or ended", doRead);
          } else if (doRead) {
            debug("do read");
            state.reading = true;
            state.sync = true;
            if (state.length === 0) state.needReadable = true;

            this._read(state.highWaterMark);

            state.sync = false;
            if (!state.reading) n = howMuchToRead(nOrig, state);
          }

          var ret;
          if (n > 0) ret = fromList(n, state);else ret = null;

          if (ret === null) {
            state.needReadable = true;
            n = 0;
          } else {
            state.length -= n;
          }

          if (state.length === 0) {
            if (!state.ended) state.needReadable = true;
            if (nOrig !== n && state.ended) endReadable(this);
          }

          if (ret !== null) this.emit("data", ret);
          return ret;
        };

        function onEofChunk(stream, state) {
          if (state.ended) return;

          if (state.decoder) {
            var chunk = state.decoder.end();

            if (chunk && chunk.length) {
              state.buffer.push(chunk);
              state.length += state.objectMode ? 1 : chunk.length;
            }
          }

          state.ended = true;
          emitReadable(stream);
        }

        function emitReadable(stream) {
          var state = stream._readableState;
          state.needReadable = false;

          if (!state.emittedReadable) {
            debug("emitReadable", state.flowing);
            state.emittedReadable = true;
            if (state.sync) processNextTick(emitReadable_, stream);else emitReadable_(stream);
          }
        }

        function emitReadable_(stream) {
          debug("emit readable");
          stream.emit("readable");
          flow(stream);
        }

        function maybeReadMore(stream, state) {
          if (!state.readingMore) {
            state.readingMore = true;
            processNextTick(maybeReadMore_, stream, state);
          }
        }

        function maybeReadMore_(stream, state) {
          var len = state.length;

          while (!state.reading && !state.flowing && !state.ended && state.length < state.highWaterMark) {
            debug("maybeReadMore read 0");
            stream.read(0);
            if (len === state.length) break;else len = state.length;
          }

          state.readingMore = false;
        }

        Readable.prototype._read = function (n) {
          this.emit("error", new Error("_read() is not implemented"));
        };

        Readable.prototype.pipe = function (dest, pipeOpts) {
          var src = this;
          var state = this._readableState;

          switch (state.pipesCount) {
            case 0:
              state.pipes = dest;
              break;

            case 1:
              state.pipes = [state.pipes, dest];
              break;

            default:
              state.pipes.push(dest);
              break;
          }

          state.pipesCount += 1;
          debug("pipe count=%d opts=%j", state.pipesCount, pipeOpts);
          var doEnd = (!pipeOpts || pipeOpts.end !== false) && dest !== process.stdout && dest !== process.stderr;
          var endFn = doEnd ? onend : unpipe;
          if (state.endEmitted) processNextTick(endFn);else src.once("end", endFn);
          dest.on("unpipe", onunpipe);

          function onunpipe(readable, unpipeInfo) {
            debug("onunpipe");

            if (readable === src) {
              if (unpipeInfo && unpipeInfo.hasUnpiped === false) {
                unpipeInfo.hasUnpiped = true;
                cleanup();
              }
            }
          }

          function onend() {
            debug("onend");
            dest.end();
          }

          var ondrain = pipeOnDrain(src);
          dest.on("drain", ondrain);
          var cleanedUp = false;

          function cleanup() {
            debug("cleanup");
            dest.removeListener("close", onclose);
            dest.removeListener("finish", onfinish);
            dest.removeListener("drain", ondrain);
            dest.removeListener("error", onerror);
            dest.removeListener("unpipe", onunpipe);
            src.removeListener("end", onend);
            src.removeListener("end", unpipe);
            src.removeListener("data", ondata);
            cleanedUp = true;
            if (state.awaitDrain && (!dest._writableState || dest._writableState.needDrain)) ondrain();
          }

          var increasedAwaitDrain = false;
          src.on("data", ondata);

          function ondata(chunk) {
            debug("ondata");
            increasedAwaitDrain = false;
            var ret = dest.write(chunk);

            if (false === ret && !increasedAwaitDrain) {
              if ((state.pipesCount === 1 && state.pipes === dest || state.pipesCount > 1 && indexOf(state.pipes, dest) !== -1) && !cleanedUp) {
                debug("false write response, pause", src._readableState.awaitDrain);
                src._readableState.awaitDrain++;
                increasedAwaitDrain = true;
              }

              src.pause();
            }
          }

          function onerror(er) {
            debug("onerror", er);
            unpipe();
            dest.removeListener("error", onerror);
            if (EElistenerCount(dest, "error") === 0) dest.emit("error", er);
          }

          prependListener(dest, "error", onerror);

          function onclose() {
            dest.removeListener("finish", onfinish);
            unpipe();
          }

          dest.once("close", onclose);

          function onfinish() {
            debug("onfinish");
            dest.removeListener("close", onclose);
            unpipe();
          }

          dest.once("finish", onfinish);

          function unpipe() {
            debug("unpipe");
            src.unpipe(dest);
          }

          dest.emit("pipe", src);

          if (!state.flowing) {
            debug("pipe resume");
            src.resume();
          }

          return dest;
        };

        function pipeOnDrain(src) {
          return function () {
            var state = src._readableState;
            debug("pipeOnDrain", state.awaitDrain);
            if (state.awaitDrain) state.awaitDrain--;

            if (state.awaitDrain === 0 && EElistenerCount(src, "data")) {
              state.flowing = true;
              flow(src);
            }
          };
        }

        Readable.prototype.unpipe = function (dest) {
          var state = this._readableState;
          var unpipeInfo = {
            hasUnpiped: false
          };
          if (state.pipesCount === 0) return this;

          if (state.pipesCount === 1) {
            if (dest && dest !== state.pipes) return this;
            if (!dest) dest = state.pipes;
            state.pipes = null;
            state.pipesCount = 0;
            state.flowing = false;
            if (dest) dest.emit("unpipe", this, unpipeInfo);
            return this;
          }

          if (!dest) {
            var dests = state.pipes;
            var len = state.pipesCount;
            state.pipes = null;
            state.pipesCount = 0;
            state.flowing = false;

            for (var i = 0; i < len; i++) {
              dests[i].emit("unpipe", this, unpipeInfo);
            }

            return this;
          }

          var index = indexOf(state.pipes, dest);
          if (index === -1) return this;
          state.pipes.splice(index, 1);
          state.pipesCount -= 1;
          if (state.pipesCount === 1) state.pipes = state.pipes[0];
          dest.emit("unpipe", this, unpipeInfo);
          return this;
        };

        Readable.prototype.on = function (ev, fn) {
          var res = Stream.prototype.on.call(this, ev, fn);

          if (ev === "data") {
            if (this._readableState.flowing !== false) this.resume();
          } else if (ev === "readable") {
            var state = this._readableState;

            if (!state.endEmitted && !state.readableListening) {
              state.readableListening = state.needReadable = true;
              state.emittedReadable = false;

              if (!state.reading) {
                processNextTick(nReadingNextTick, this);
              } else if (state.length) {
                emitReadable(this);
              }
            }
          }

          return res;
        };

        Readable.prototype.addListener = Readable.prototype.on;

        function nReadingNextTick(self) {
          debug("readable nexttick read 0");
          self.read(0);
        }

        Readable.prototype.resume = function () {
          var state = this._readableState;

          if (!state.flowing) {
            debug("resume");
            state.flowing = true;
            resume(this, state);
          }

          return this;
        };

        function resume(stream, state) {
          if (!state.resumeScheduled) {
            state.resumeScheduled = true;
            processNextTick(resume_, stream, state);
          }
        }

        function resume_(stream, state) {
          if (!state.reading) {
            debug("resume read 0");
            stream.read(0);
          }

          state.resumeScheduled = false;
          state.awaitDrain = 0;
          stream.emit("resume");
          flow(stream);
          if (state.flowing && !state.reading) stream.read(0);
        }

        Readable.prototype.pause = function () {
          debug("call pause flowing=%j", this._readableState.flowing);

          if (false !== this._readableState.flowing) {
            debug("pause");
            this._readableState.flowing = false;
            this.emit("pause");
          }

          return this;
        };

        function flow(stream) {
          var state = stream._readableState;
          debug("flow", state.flowing);

          while (state.flowing && stream.read() !== null) {}
        }

        Readable.prototype.wrap = function (stream) {
          var state = this._readableState;
          var paused = false;
          var self = this;
          stream.on("end", function () {
            debug("wrapped end");

            if (state.decoder && !state.ended) {
              var chunk = state.decoder.end();
              if (chunk && chunk.length) self.push(chunk);
            }

            self.push(null);
          });
          stream.on("data", function (chunk) {
            debug("wrapped data");
            if (state.decoder) chunk = state.decoder.write(chunk);
            if (state.objectMode && (chunk === null || chunk === undefined)) return;else if (!state.objectMode && (!chunk || !chunk.length)) return;
            var ret = self.push(chunk);

            if (!ret) {
              paused = true;
              stream.pause();
            }
          });

          for (var i in stream) {
            if (this[i] === undefined && typeof stream[i] === "function") {
              this[i] = function (method) {
                return function () {
                  return stream[method].apply(stream, arguments);
                };
              }(i);
            }
          }

          for (var n = 0; n < kProxyEvents.length; n++) {
            stream.on(kProxyEvents[n], self.emit.bind(self, kProxyEvents[n]));
          }

          self._read = function (n) {
            debug("wrapped _read", n);

            if (paused) {
              paused = false;
              stream.resume();
            }
          };

          return self;
        };

        Readable._fromList = fromList;

        function fromList(n, state) {
          if (state.length === 0) return null;
          var ret;
          if (state.objectMode) ret = state.buffer.shift();else if (!n || n >= state.length) {
            if (state.decoder) ret = state.buffer.join("");else if (state.buffer.length === 1) ret = state.buffer.head.data;else ret = state.buffer.concat(state.length);
            state.buffer.clear();
          } else {
            ret = fromListPartial(n, state.buffer, state.decoder);
          }
          return ret;
        }

        function fromListPartial(n, list, hasStrings) {
          var ret;

          if (n < list.head.data.length) {
            ret = list.head.data.slice(0, n);
            list.head.data = list.head.data.slice(n);
          } else if (n === list.head.data.length) {
            ret = list.shift();
          } else {
            ret = hasStrings ? copyFromBufferString(n, list) : copyFromBuffer(n, list);
          }

          return ret;
        }

        function copyFromBufferString(n, list) {
          var p = list.head;
          var c = 1;
          var ret = p.data;
          n -= ret.length;

          while (p = p.next) {
            var str = p.data;
            var nb = n > str.length ? str.length : n;
            if (nb === str.length) ret += str;else ret += str.slice(0, n);
            n -= nb;

            if (n === 0) {
              if (nb === str.length) {
                ++c;
                if (p.next) list.head = p.next;else list.head = list.tail = null;
              } else {
                list.head = p;
                p.data = str.slice(nb);
              }

              break;
            }

            ++c;
          }

          list.length -= c;
          return ret;
        }

        function copyFromBuffer(n, list) {
          var ret = Buffer.allocUnsafe(n);
          var p = list.head;
          var c = 1;
          p.data.copy(ret);
          n -= p.data.length;

          while (p = p.next) {
            var buf = p.data;
            var nb = n > buf.length ? buf.length : n;
            buf.copy(ret, ret.length - n, 0, nb);
            n -= nb;

            if (n === 0) {
              if (nb === buf.length) {
                ++c;
                if (p.next) list.head = p.next;else list.head = list.tail = null;
              } else {
                list.head = p;
                p.data = buf.slice(nb);
              }

              break;
            }

            ++c;
          }

          list.length -= c;
          return ret;
        }

        function endReadable(stream) {
          var state = stream._readableState;
          if (state.length > 0) throw new Error('"endReadable()" called on non-empty stream');

          if (!state.endEmitted) {
            state.ended = true;
            processNextTick(endReadableNT, state, stream);
          }
        }

        function endReadableNT(state, stream) {
          if (!state.endEmitted && state.length === 0) {
            state.endEmitted = true;
            stream.readable = false;
            stream.emit("end");
          }
        }

        function forEach(xs, f) {
          for (var i = 0, l = xs.length; i < l; i++) {
            f(xs[i], i);
          }
        }

        function indexOf(xs, x) {
          for (var i = 0, l = xs.length; i < l; i++) {
            if (xs[i] === x) return i;
          }

          return -1;
        }
      }).call(this, require("_process"), typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
    }, {
      "./_stream_duplex": 19,
      "./internal/streams/BufferList": 24,
      "./internal/streams/destroy": 25,
      "./internal/streams/stream": 26,
      _process: 17,
      "core-util-is": 10,
      events: 11,
      inherits: 13,
      isarray: 15,
      "process-nextick-args": 16,
      "safe-buffer": 28,
      "string_decoder/": 29,
      util: 8
    }],
    22: [function (require, module, exports) {
      "use strict";

      module.exports = Transform;

      var Duplex = require("./_stream_duplex");

      var util = require("core-util-is");

      util.inherits = require("inherits");
      util.inherits(Transform, Duplex);

      function TransformState(stream) {
        this.afterTransform = function (er, data) {
          return afterTransform(stream, er, data);
        };

        this.needTransform = false;
        this.transforming = false;
        this.writecb = null;
        this.writechunk = null;
        this.writeencoding = null;
      }

      function afterTransform(stream, er, data) {
        var ts = stream._transformState;
        ts.transforming = false;
        var cb = ts.writecb;

        if (!cb) {
          return stream.emit("error", new Error("write callback called multiple times"));
        }

        ts.writechunk = null;
        ts.writecb = null;
        if (data !== null && data !== undefined) stream.push(data);
        cb(er);
        var rs = stream._readableState;
        rs.reading = false;

        if (rs.needReadable || rs.length < rs.highWaterMark) {
          stream._read(rs.highWaterMark);
        }
      }

      function Transform(options) {
        if (!(this instanceof Transform)) return new Transform(options);
        Duplex.call(this, options);
        this._transformState = new TransformState(this);
        var stream = this;
        this._readableState.needReadable = true;
        this._readableState.sync = false;

        if (options) {
          if (typeof options.transform === "function") this._transform = options.transform;
          if (typeof options.flush === "function") this._flush = options.flush;
        }

        this.once("prefinish", function () {
          if (typeof this._flush === "function") this._flush(function (er, data) {
            done(stream, er, data);
          });else done(stream);
        });
      }

      Transform.prototype.push = function (chunk, encoding) {
        this._transformState.needTransform = false;
        return Duplex.prototype.push.call(this, chunk, encoding);
      };

      Transform.prototype._transform = function (chunk, encoding, cb) {
        throw new Error("_transform() is not implemented");
      };

      Transform.prototype._write = function (chunk, encoding, cb) {
        var ts = this._transformState;
        ts.writecb = cb;
        ts.writechunk = chunk;
        ts.writeencoding = encoding;

        if (!ts.transforming) {
          var rs = this._readableState;
          if (ts.needTransform || rs.needReadable || rs.length < rs.highWaterMark) this._read(rs.highWaterMark);
        }
      };

      Transform.prototype._read = function (n) {
        var ts = this._transformState;

        if (ts.writechunk !== null && ts.writecb && !ts.transforming) {
          ts.transforming = true;

          this._transform(ts.writechunk, ts.writeencoding, ts.afterTransform);
        } else {
          ts.needTransform = true;
        }
      };

      Transform.prototype._destroy = function (err, cb) {
        var _this = this;

        Duplex.prototype._destroy.call(this, err, function (err2) {
          cb(err2);

          _this.emit("close");
        });
      };

      function done(stream, er, data) {
        if (er) return stream.emit("error", er);
        if (data !== null && data !== undefined) stream.push(data);
        var ws = stream._writableState;
        var ts = stream._transformState;
        if (ws.length) throw new Error("Calling transform done when ws.length != 0");
        if (ts.transforming) throw new Error("Calling transform done when still transforming");
        return stream.push(null);
      }
    }, {
      "./_stream_duplex": 19,
      "core-util-is": 10,
      inherits: 13
    }],
    23: [function (require, module, exports) {
      (function (process, global) {
        "use strict";

        var processNextTick = require("process-nextick-args");

        module.exports = Writable;

        function WriteReq(chunk, encoding, cb) {
          this.chunk = chunk;
          this.encoding = encoding;
          this.callback = cb;
          this.next = null;
        }

        function CorkedRequest(state) {
          var _this = this;

          this.next = null;
          this.entry = null;

          this.finish = function () {
            onCorkedFinish(_this, state);
          };
        }

        var asyncWrite = !process.browser && ["v0.10", "v0.9."].indexOf(process.version.slice(0, 5)) > -1 ? setImmediate : processNextTick;
        var Duplex;
        Writable.WritableState = WritableState;

        var util = require("core-util-is");

        util.inherits = require("inherits");
        var internalUtil = {
          deprecate: require("util-deprecate")
        };

        var Stream = require("./internal/streams/stream");

        var Buffer = require("safe-buffer").Buffer;

        var OurUint8Array = global.Uint8Array || function () {};

        function _uint8ArrayToBuffer(chunk) {
          return Buffer.from(chunk);
        }

        function _isUint8Array(obj) {
          return Buffer.isBuffer(obj) || obj instanceof OurUint8Array;
        }

        var destroyImpl = require("./internal/streams/destroy");

        util.inherits(Writable, Stream);

        function nop() {}

        function WritableState(options, stream) {
          Duplex = Duplex || require("./_stream_duplex");
          options = options || {};
          this.objectMode = !!options.objectMode;
          if (stream instanceof Duplex) this.objectMode = this.objectMode || !!options.writableObjectMode;
          var hwm = options.highWaterMark;
          var defaultHwm = this.objectMode ? 16 : 16 * 1024;
          this.highWaterMark = hwm || hwm === 0 ? hwm : defaultHwm;
          this.highWaterMark = Math.floor(this.highWaterMark);
          this.finalCalled = false;
          this.needDrain = false;
          this.ending = false;
          this.ended = false;
          this.finished = false;
          this.destroyed = false;
          var noDecode = options.decodeStrings === false;
          this.decodeStrings = !noDecode;
          this.defaultEncoding = options.defaultEncoding || "utf8";
          this.length = 0;
          this.writing = false;
          this.corked = 0;
          this.sync = true;
          this.bufferProcessing = false;

          this.onwrite = function (er) {
            onwrite(stream, er);
          };

          this.writecb = null;
          this.writelen = 0;
          this.bufferedRequest = null;
          this.lastBufferedRequest = null;
          this.pendingcb = 0;
          this.prefinished = false;
          this.errorEmitted = false;
          this.bufferedRequestCount = 0;
          this.corkedRequestsFree = new CorkedRequest(this);
        }

        WritableState.prototype.getBuffer = function getBuffer() {
          var current = this.bufferedRequest;
          var out = [];

          while (current) {
            out.push(current);
            current = current.next;
          }

          return out;
        };

        (function () {
          try {
            Object.defineProperty(WritableState.prototype, "buffer", {
              get: internalUtil.deprecate(function () {
                return this.getBuffer();
              }, "_writableState.buffer is deprecated. Use _writableState.getBuffer " + "instead.", "DEP0003")
            });
          } catch (_) {}
        })();

        var realHasInstance;

        if (typeof Symbol === "function" && Symbol.hasInstance && typeof Function.prototype[Symbol.hasInstance] === "function") {
          realHasInstance = Function.prototype[Symbol.hasInstance];
          Object.defineProperty(Writable, Symbol.hasInstance, {
            value: function value(object) {
              if (realHasInstance.call(this, object)) return true;
              return object && object._writableState instanceof WritableState;
            }
          });
        } else {
          realHasInstance = function realHasInstance(object) {
            return object instanceof this;
          };
        }

        function Writable(options) {
          Duplex = Duplex || require("./_stream_duplex");

          if (!realHasInstance.call(Writable, this) && !(this instanceof Duplex)) {
            return new Writable(options);
          }

          this._writableState = new WritableState(options, this);
          this.writable = true;

          if (options) {
            if (typeof options.write === "function") this._write = options.write;
            if (typeof options.writev === "function") this._writev = options.writev;
            if (typeof options.destroy === "function") this._destroy = options.destroy;
            if (typeof options["final"] === "function") this._final = options["final"];
          }

          Stream.call(this);
        }

        Writable.prototype.pipe = function () {
          this.emit("error", new Error("Cannot pipe, not readable"));
        };

        function writeAfterEnd(stream, cb) {
          var er = new Error("write after end");
          stream.emit("error", er);
          processNextTick(cb, er);
        }

        function validChunk(stream, state, chunk, cb) {
          var valid = true;
          var er = false;

          if (chunk === null) {
            er = new TypeError("May not write null values to stream");
          } else if (typeof chunk !== "string" && chunk !== undefined && !state.objectMode) {
            er = new TypeError("Invalid non-string/buffer chunk");
          }

          if (er) {
            stream.emit("error", er);
            processNextTick(cb, er);
            valid = false;
          }

          return valid;
        }

        Writable.prototype.write = function (chunk, encoding, cb) {
          var state = this._writableState;
          var ret = false;
          var isBuf = _isUint8Array(chunk) && !state.objectMode;

          if (isBuf && !Buffer.isBuffer(chunk)) {
            chunk = _uint8ArrayToBuffer(chunk);
          }

          if (typeof encoding === "function") {
            cb = encoding;
            encoding = null;
          }

          if (isBuf) encoding = "buffer";else if (!encoding) encoding = state.defaultEncoding;
          if (typeof cb !== "function") cb = nop;
          if (state.ended) writeAfterEnd(this, cb);else if (isBuf || validChunk(this, state, chunk, cb)) {
            state.pendingcb++;
            ret = writeOrBuffer(this, state, isBuf, chunk, encoding, cb);
          }
          return ret;
        };

        Writable.prototype.cork = function () {
          var state = this._writableState;
          state.corked++;
        };

        Writable.prototype.uncork = function () {
          var state = this._writableState;

          if (state.corked) {
            state.corked--;
            if (!state.writing && !state.corked && !state.finished && !state.bufferProcessing && state.bufferedRequest) clearBuffer(this, state);
          }
        };

        Writable.prototype.setDefaultEncoding = function setDefaultEncoding(encoding) {
          if (typeof encoding === "string") encoding = encoding.toLowerCase();
          if (!(["hex", "utf8", "utf-8", "ascii", "binary", "base64", "ucs2", "ucs-2", "utf16le", "utf-16le", "raw"].indexOf((encoding + "").toLowerCase()) > -1)) throw new TypeError("Unknown encoding: " + encoding);
          this._writableState.defaultEncoding = encoding;
          return this;
        };

        function decodeChunk(state, chunk, encoding) {
          if (!state.objectMode && state.decodeStrings !== false && typeof chunk === "string") {
            chunk = Buffer.from(chunk, encoding);
          }

          return chunk;
        }

        function writeOrBuffer(stream, state, isBuf, chunk, encoding, cb) {
          if (!isBuf) {
            var newChunk = decodeChunk(state, chunk, encoding);

            if (chunk !== newChunk) {
              isBuf = true;
              encoding = "buffer";
              chunk = newChunk;
            }
          }

          var len = state.objectMode ? 1 : chunk.length;
          state.length += len;
          var ret = state.length < state.highWaterMark;
          if (!ret) state.needDrain = true;

          if (state.writing || state.corked) {
            var last = state.lastBufferedRequest;
            state.lastBufferedRequest = {
              chunk: chunk,
              encoding: encoding,
              isBuf: isBuf,
              callback: cb,
              next: null
            };

            if (last) {
              last.next = state.lastBufferedRequest;
            } else {
              state.bufferedRequest = state.lastBufferedRequest;
            }

            state.bufferedRequestCount += 1;
          } else {
            doWrite(stream, state, false, len, chunk, encoding, cb);
          }

          return ret;
        }

        function doWrite(stream, state, writev, len, chunk, encoding, cb) {
          state.writelen = len;
          state.writecb = cb;
          state.writing = true;
          state.sync = true;
          if (writev) stream._writev(chunk, state.onwrite);else stream._write(chunk, encoding, state.onwrite);
          state.sync = false;
        }

        function onwriteError(stream, state, sync, er, cb) {
          --state.pendingcb;

          if (sync) {
            processNextTick(cb, er);
            processNextTick(finishMaybe, stream, state);
            stream._writableState.errorEmitted = true;
            stream.emit("error", er);
          } else {
            cb(er);
            stream._writableState.errorEmitted = true;
            stream.emit("error", er);
            finishMaybe(stream, state);
          }
        }

        function onwriteStateUpdate(state) {
          state.writing = false;
          state.writecb = null;
          state.length -= state.writelen;
          state.writelen = 0;
        }

        function onwrite(stream, er) {
          var state = stream._writableState;
          var sync = state.sync;
          var cb = state.writecb;
          onwriteStateUpdate(state);
          if (er) onwriteError(stream, state, sync, er, cb);else {
            var finished = needFinish(state);

            if (!finished && !state.corked && !state.bufferProcessing && state.bufferedRequest) {
              clearBuffer(stream, state);
            }

            if (sync) {
              asyncWrite(afterWrite, stream, state, finished, cb);
            } else {
              afterWrite(stream, state, finished, cb);
            }
          }
        }

        function afterWrite(stream, state, finished, cb) {
          if (!finished) onwriteDrain(stream, state);
          state.pendingcb--;
          cb();
          finishMaybe(stream, state);
        }

        function onwriteDrain(stream, state) {
          if (state.length === 0 && state.needDrain) {
            state.needDrain = false;
            stream.emit("drain");
          }
        }

        function clearBuffer(stream, state) {
          state.bufferProcessing = true;
          var entry = state.bufferedRequest;

          if (stream._writev && entry && entry.next) {
            var l = state.bufferedRequestCount;
            var buffer = new Array(l);
            var holder = state.corkedRequestsFree;
            holder.entry = entry;
            var count = 0;
            var allBuffers = true;

            while (entry) {
              buffer[count] = entry;
              if (!entry.isBuf) allBuffers = false;
              entry = entry.next;
              count += 1;
            }

            buffer.allBuffers = allBuffers;
            doWrite(stream, state, true, state.length, buffer, "", holder.finish);
            state.pendingcb++;
            state.lastBufferedRequest = null;

            if (holder.next) {
              state.corkedRequestsFree = holder.next;
              holder.next = null;
            } else {
              state.corkedRequestsFree = new CorkedRequest(state);
            }
          } else {
            while (entry) {
              var chunk = entry.chunk;
              var encoding = entry.encoding;
              var cb = entry.callback;
              var len = state.objectMode ? 1 : chunk.length;
              doWrite(stream, state, false, len, chunk, encoding, cb);
              entry = entry.next;

              if (state.writing) {
                break;
              }
            }

            if (entry === null) state.lastBufferedRequest = null;
          }

          state.bufferedRequestCount = 0;
          state.bufferedRequest = entry;
          state.bufferProcessing = false;
        }

        Writable.prototype._write = function (chunk, encoding, cb) {
          cb(new Error("_write() is not implemented"));
        };

        Writable.prototype._writev = null;

        Writable.prototype.end = function (chunk, encoding, cb) {
          var state = this._writableState;

          if (typeof chunk === "function") {
            cb = chunk;
            chunk = null;
            encoding = null;
          } else if (typeof encoding === "function") {
            cb = encoding;
            encoding = null;
          }

          if (chunk !== null && chunk !== undefined) this.write(chunk, encoding);

          if (state.corked) {
            state.corked = 1;
            this.uncork();
          }

          if (!state.ending && !state.finished) endWritable(this, state, cb);
        };

        function needFinish(state) {
          return state.ending && state.length === 0 && state.bufferedRequest === null && !state.finished && !state.writing;
        }

        function callFinal(stream, state) {
          stream._final(function (err) {
            state.pendingcb--;

            if (err) {
              stream.emit("error", err);
            }

            state.prefinished = true;
            stream.emit("prefinish");
            finishMaybe(stream, state);
          });
        }

        function prefinish(stream, state) {
          if (!state.prefinished && !state.finalCalled) {
            if (typeof stream._final === "function") {
              state.pendingcb++;
              state.finalCalled = true;
              processNextTick(callFinal, stream, state);
            } else {
              state.prefinished = true;
              stream.emit("prefinish");
            }
          }
        }

        function finishMaybe(stream, state) {
          var need = needFinish(state);

          if (need) {
            prefinish(stream, state);

            if (state.pendingcb === 0) {
              state.finished = true;
              stream.emit("finish");
            }
          }

          return need;
        }

        function endWritable(stream, state, cb) {
          state.ending = true;
          finishMaybe(stream, state);

          if (cb) {
            if (state.finished) processNextTick(cb);else stream.once("finish", cb);
          }

          state.ended = true;
          stream.writable = false;
        }

        function onCorkedFinish(corkReq, state, err) {
          var entry = corkReq.entry;
          corkReq.entry = null;

          while (entry) {
            var cb = entry.callback;
            state.pendingcb--;
            cb(err);
            entry = entry.next;
          }

          if (state.corkedRequestsFree) {
            state.corkedRequestsFree.next = corkReq;
          } else {
            state.corkedRequestsFree = corkReq;
          }
        }

        Object.defineProperty(Writable.prototype, "destroyed", {
          get: function get() {
            if (this._writableState === undefined) {
              return false;
            }

            return this._writableState.destroyed;
          },
          set: function set(value) {
            if (!this._writableState) {
              return;
            }

            this._writableState.destroyed = value;
          }
        });
        Writable.prototype.destroy = destroyImpl.destroy;
        Writable.prototype._undestroy = destroyImpl.undestroy;

        Writable.prototype._destroy = function (err, cb) {
          this.end();
          cb(err);
        };
      }).call(this, require("_process"), typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
    }, {
      "./_stream_duplex": 19,
      "./internal/streams/destroy": 25,
      "./internal/streams/stream": 26,
      _process: 17,
      "core-util-is": 10,
      inherits: 13,
      "process-nextick-args": 16,
      "safe-buffer": 28,
      "util-deprecate": 30
    }],
    24: [function (require, module, exports) {
      "use strict";

      function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
          throw new TypeError("Cannot call a class as a function");
        }
      }

      var Buffer = require("safe-buffer").Buffer;

      function copyBuffer(src, target, offset) {
        src.copy(target, offset);
      }

      module.exports = function () {
        function BufferList() {
          _classCallCheck(this, BufferList);

          this.head = null;
          this.tail = null;
          this.length = 0;
        }

        BufferList.prototype.push = function push(v) {
          var entry = {
            data: v,
            next: null
          };
          if (this.length > 0) this.tail.next = entry;else this.head = entry;
          this.tail = entry;
          ++this.length;
        };

        BufferList.prototype.unshift = function unshift(v) {
          var entry = {
            data: v,
            next: this.head
          };
          if (this.length === 0) this.tail = entry;
          this.head = entry;
          ++this.length;
        };

        BufferList.prototype.shift = function shift() {
          if (this.length === 0) return;
          var ret = this.head.data;
          if (this.length === 1) this.head = this.tail = null;else this.head = this.head.next;
          --this.length;
          return ret;
        };

        BufferList.prototype.clear = function clear() {
          this.head = this.tail = null;
          this.length = 0;
        };

        BufferList.prototype.join = function join(s) {
          if (this.length === 0) return "";
          var p = this.head;
          var ret = "" + p.data;

          while (p = p.next) {
            ret += s + p.data;
          }

          return ret;
        };

        BufferList.prototype.concat = function concat(n) {
          if (this.length === 0) return Buffer.alloc(0);
          if (this.length === 1) return this.head.data;
          var ret = Buffer.allocUnsafe(n >>> 0);
          var p = this.head;
          var i = 0;

          while (p) {
            copyBuffer(p.data, ret, i);
            i += p.data.length;
            p = p.next;
          }

          return ret;
        };

        return BufferList;
      }();
    }, {
      "safe-buffer": 28
    }],
    25: [function (require, module, exports) {
      "use strict";

      var processNextTick = require("process-nextick-args");

      function destroy(err, cb) {
        var _this = this;

        var readableDestroyed = this._readableState && this._readableState.destroyed;
        var writableDestroyed = this._writableState && this._writableState.destroyed;

        if (readableDestroyed || writableDestroyed) {
          if (cb) {
            cb(err);
          } else if (err && (!this._writableState || !this._writableState.errorEmitted)) {
            processNextTick(emitErrorNT, this, err);
          }

          return;
        }

        if (this._readableState) {
          this._readableState.destroyed = true;
        }

        if (this._writableState) {
          this._writableState.destroyed = true;
        }

        this._destroy(err || null, function (err) {
          if (!cb && err) {
            processNextTick(emitErrorNT, _this, err);

            if (_this._writableState) {
              _this._writableState.errorEmitted = true;
            }
          } else if (cb) {
            cb(err);
          }
        });
      }

      function undestroy() {
        if (this._readableState) {
          this._readableState.destroyed = false;
          this._readableState.reading = false;
          this._readableState.ended = false;
          this._readableState.endEmitted = false;
        }

        if (this._writableState) {
          this._writableState.destroyed = false;
          this._writableState.ended = false;
          this._writableState.ending = false;
          this._writableState.finished = false;
          this._writableState.errorEmitted = false;
        }
      }

      function emitErrorNT(self, err) {
        self.emit("error", err);
      }

      module.exports = {
        destroy: destroy,
        undestroy: undestroy
      };
    }, {
      "process-nextick-args": 16
    }],
    26: [function (require, module, exports) {
      module.exports = require("events").EventEmitter;
    }, {
      events: 11
    }],
    27: [function (require, module, exports) {
      exports = module.exports = require("./lib/_stream_readable.js");
      exports.Stream = exports;
      exports.Readable = exports;
      exports.Writable = require("./lib/_stream_writable.js");
      exports.Duplex = require("./lib/_stream_duplex.js");
      exports.Transform = require("./lib/_stream_transform.js");
      exports.PassThrough = require("./lib/_stream_passthrough.js");
    }, {
      "./lib/_stream_duplex.js": 19,
      "./lib/_stream_passthrough.js": 20,
      "./lib/_stream_readable.js": 21,
      "./lib/_stream_transform.js": 22,
      "./lib/_stream_writable.js": 23
    }],
    28: [function (require, module, exports) {
      var buffer = require("buffer");

      var Buffer = buffer.Buffer;

      function copyProps(src, dst) {
        for (var key in src) {
          dst[key] = src[key];
        }
      }

      if (Buffer.from && Buffer.alloc && Buffer.allocUnsafe && Buffer.allocUnsafeSlow) {
        module.exports = buffer;
      } else {
        copyProps(buffer, exports);
        exports.Buffer = SafeBuffer;
      }

      function SafeBuffer(arg, encodingOrOffset, length) {
        return Buffer(arg, encodingOrOffset, length);
      }

      copyProps(Buffer, SafeBuffer);

      SafeBuffer.from = function (arg, encodingOrOffset, length) {
        if (typeof arg === "number") {
          throw new TypeError("Argument must not be a number");
        }

        return Buffer(arg, encodingOrOffset, length);
      };

      SafeBuffer.alloc = function (size, fill, encoding) {
        if (typeof size !== "number") {
          throw new TypeError("Argument must be a number");
        }

        var buf = Buffer(size);

        if (fill !== undefined) {
          if (typeof encoding === "string") {
            buf.fill(fill, encoding);
          } else {
            buf.fill(fill);
          }
        } else {
          buf.fill(0);
        }

        return buf;
      };

      SafeBuffer.allocUnsafe = function (size) {
        if (typeof size !== "number") {
          throw new TypeError("Argument must be a number");
        }

        return Buffer(size);
      };

      SafeBuffer.allocUnsafeSlow = function (size) {
        if (typeof size !== "number") {
          throw new TypeError("Argument must be a number");
        }

        return buffer.SlowBuffer(size);
      };
    }, {
      buffer: 9
    }],
    29: [function (require, module, exports) {
      "use strict";

      var Buffer = require("safe-buffer").Buffer;

      var isEncoding = Buffer.isEncoding || function (encoding) {
        encoding = "" + encoding;

        switch (encoding && encoding.toLowerCase()) {
          case "hex":
          case "utf8":
          case "utf-8":
          case "ascii":
          case "binary":
          case "base64":
          case "ucs2":
          case "ucs-2":
          case "utf16le":
          case "utf-16le":
          case "raw":
            return true;

          default:
            return false;
        }
      };

      function _normalizeEncoding(enc) {
        if (!enc) return "utf8";
        var retried;

        while (true) {
          switch (enc) {
            case "utf8":
            case "utf-8":
              return "utf8";

            case "ucs2":
            case "ucs-2":
            case "utf16le":
            case "utf-16le":
              return "utf16le";

            case "latin1":
            case "binary":
              return "latin1";

            case "base64":
            case "ascii":
            case "hex":
              return enc;

            default:
              if (retried) return;
              enc = ("" + enc).toLowerCase();
              retried = true;
          }
        }
      }

      function normalizeEncoding(enc) {
        var nenc = _normalizeEncoding(enc);

        if (typeof nenc !== "string" && (Buffer.isEncoding === isEncoding || !isEncoding(enc))) throw new Error("Unknown encoding: " + enc);
        return nenc || enc;
      }

      exports.StringDecoder = StringDecoder;

      function StringDecoder(encoding) {
        this.encoding = normalizeEncoding(encoding);
        var nb;

        switch (this.encoding) {
          case "utf16le":
            this.text = utf16Text;
            this.end = utf16End;
            nb = 4;
            break;

          case "utf8":
            this.fillLast = utf8FillLast;
            nb = 4;
            break;

          case "base64":
            this.text = base64Text;
            this.end = base64End;
            nb = 3;
            break;

          default:
            this.write = simpleWrite;
            this.end = simpleEnd;
            return;
        }

        this.lastNeed = 0;
        this.lastTotal = 0;
        this.lastChar = Buffer.allocUnsafe(nb);
      }

      StringDecoder.prototype.write = function (buf) {
        if (buf.length === 0) return "";
        var r;
        var i;

        if (this.lastNeed) {
          r = this.fillLast(buf);
          if (r === undefined) return "";
          i = this.lastNeed;
          this.lastNeed = 0;
        } else {
          i = 0;
        }

        if (i < buf.length) return r ? r + this.text(buf, i) : this.text(buf, i);
        return r || "";
      };

      StringDecoder.prototype.end = utf8End;
      StringDecoder.prototype.text = utf8Text;

      StringDecoder.prototype.fillLast = function (buf) {
        if (this.lastNeed <= buf.length) {
          buf.copy(this.lastChar, this.lastTotal - this.lastNeed, 0, this.lastNeed);
          return this.lastChar.toString(this.encoding, 0, this.lastTotal);
        }

        buf.copy(this.lastChar, this.lastTotal - this.lastNeed, 0, buf.length);
        this.lastNeed -= buf.length;
      };

      function utf8CheckByte(_byte) {
        if (_byte <= 127) return 0;else if (_byte >> 5 === 6) return 2;else if (_byte >> 4 === 14) return 3;else if (_byte >> 3 === 30) return 4;
        return -1;
      }

      function utf8CheckIncomplete(self, buf, i) {
        var j = buf.length - 1;
        if (j < i) return 0;
        var nb = utf8CheckByte(buf[j]);

        if (nb >= 0) {
          if (nb > 0) self.lastNeed = nb - 1;
          return nb;
        }

        if (--j < i) return 0;
        nb = utf8CheckByte(buf[j]);

        if (nb >= 0) {
          if (nb > 0) self.lastNeed = nb - 2;
          return nb;
        }

        if (--j < i) return 0;
        nb = utf8CheckByte(buf[j]);

        if (nb >= 0) {
          if (nb > 0) {
            if (nb === 2) nb = 0;else self.lastNeed = nb - 3;
          }

          return nb;
        }

        return 0;
      }

      function utf8CheckExtraBytes(self, buf, p) {
        if ((buf[0] & 192) !== 128) {
          self.lastNeed = 0;
          return "�".repeat(p);
        }

        if (self.lastNeed > 1 && buf.length > 1) {
          if ((buf[1] & 192) !== 128) {
            self.lastNeed = 1;
            return "�".repeat(p + 1);
          }

          if (self.lastNeed > 2 && buf.length > 2) {
            if ((buf[2] & 192) !== 128) {
              self.lastNeed = 2;
              return "�".repeat(p + 2);
            }
          }
        }
      }

      function utf8FillLast(buf) {
        var p = this.lastTotal - this.lastNeed;
        var r = utf8CheckExtraBytes(this, buf, p);
        if (r !== undefined) return r;

        if (this.lastNeed <= buf.length) {
          buf.copy(this.lastChar, p, 0, this.lastNeed);
          return this.lastChar.toString(this.encoding, 0, this.lastTotal);
        }

        buf.copy(this.lastChar, p, 0, buf.length);
        this.lastNeed -= buf.length;
      }

      function utf8Text(buf, i) {
        var total = utf8CheckIncomplete(this, buf, i);
        if (!this.lastNeed) return buf.toString("utf8", i);
        this.lastTotal = total;
        var end = buf.length - (total - this.lastNeed);
        buf.copy(this.lastChar, 0, end);
        return buf.toString("utf8", i, end);
      }

      function utf8End(buf) {
        var r = buf && buf.length ? this.write(buf) : "";
        if (this.lastNeed) return r + "�".repeat(this.lastTotal - this.lastNeed);
        return r;
      }

      function utf16Text(buf, i) {
        if ((buf.length - i) % 2 === 0) {
          var r = buf.toString("utf16le", i);

          if (r) {
            var c = r.charCodeAt(r.length - 1);

            if (c >= 55296 && c <= 56319) {
              this.lastNeed = 2;
              this.lastTotal = 4;
              this.lastChar[0] = buf[buf.length - 2];
              this.lastChar[1] = buf[buf.length - 1];
              return r.slice(0, -1);
            }
          }

          return r;
        }

        this.lastNeed = 1;
        this.lastTotal = 2;
        this.lastChar[0] = buf[buf.length - 1];
        return buf.toString("utf16le", i, buf.length - 1);
      }

      function utf16End(buf) {
        var r = buf && buf.length ? this.write(buf) : "";

        if (this.lastNeed) {
          var end = this.lastTotal - this.lastNeed;
          return r + this.lastChar.toString("utf16le", 0, end);
        }

        return r;
      }

      function base64Text(buf, i) {
        var n = (buf.length - i) % 3;
        if (n === 0) return buf.toString("base64", i);
        this.lastNeed = 3 - n;
        this.lastTotal = 3;

        if (n === 1) {
          this.lastChar[0] = buf[buf.length - 1];
        } else {
          this.lastChar[0] = buf[buf.length - 2];
          this.lastChar[1] = buf[buf.length - 1];
        }

        return buf.toString("base64", i, buf.length - n);
      }

      function base64End(buf) {
        var r = buf && buf.length ? this.write(buf) : "";
        if (this.lastNeed) return r + this.lastChar.toString("base64", 0, 3 - this.lastNeed);
        return r;
      }

      function simpleWrite(buf) {
        return buf.toString(this.encoding);
      }

      function simpleEnd(buf) {
        return buf && buf.length ? this.write(buf) : "";
      }
    }, {
      "safe-buffer": 28
    }],
    30: [function (require, module, exports) {
      (function (global) {
        module.exports = deprecate;

        function deprecate(fn, msg) {
          if (config("noDeprecation")) {
            return fn;
          }

          var warned = false;

          function deprecated() {
            if (!warned) {
              if (config("throwDeprecation")) {
                throw new Error(msg);
              } else if (config("traceDeprecation")) {
                console.trace(msg);
              } else {
                console.warn(msg);
              }

              warned = true;
            }

            return fn.apply(this, arguments);
          }

          return deprecated;
        }

        function config(name) {
          try {
            if (!global.localStorage) return false;
          } catch (_) {
            return false;
          }

          var val = global.localStorage[name];
          if (null == val) return false;
          return String(val).toLowerCase() === "true";
        }
      }).call(this, typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
    }, {}],
    31: [function (require, module, exports) {
      arguments[4][13][0].apply(exports, arguments);
    }, {
      dup: 13
    }],
    32: [function (require, module, exports) {
      module.exports = function isBuffer(arg) {
        return arg && _typeof(arg) === "object" && typeof arg.copy === "function" && typeof arg.fill === "function" && typeof arg.readUInt8 === "function";
      };
    }, {}],
    33: [function (require, module, exports) {
      (function (process, global) {
        var formatRegExp = /%[sdj%]/g;

        exports.format = function (f) {
          if (!isString(f)) {
            var objects = [];

            for (var i = 0; i < arguments.length; i++) {
              objects.push(inspect(arguments[i]));
            }

            return objects.join(" ");
          }

          var i = 1;
          var args = arguments;
          var len = args.length;
          var str = String(f).replace(formatRegExp, function (x) {
            if (x === "%%") return "%";
            if (i >= len) return x;

            switch (x) {
              case "%s":
                return String(args[i++]);

              case "%d":
                return Number(args[i++]);

              case "%j":
                try {
                  return JSON.stringify(args[i++]);
                } catch (_) {
                  return "[Circular]";
                }

              default:
                return x;
            }
          });

          for (var x = args[i]; i < len; x = args[++i]) {
            if (isNull(x) || !isObject(x)) {
              str += " " + x;
            } else {
              str += " " + inspect(x);
            }
          }

          return str;
        };

        exports.deprecate = function (fn, msg) {
          if (isUndefined(global.process)) {
            return function () {
              return exports.deprecate(fn, msg).apply(this, arguments);
            };
          }

          if (process.noDeprecation === true) {
            return fn;
          }

          var warned = false;

          function deprecated() {
            if (!warned) {
              if (process.throwDeprecation) {
                throw new Error(msg);
              } else if (process.traceDeprecation) {
                console.trace(msg);
              } else {
                console.error(msg);
              }

              warned = true;
            }

            return fn.apply(this, arguments);
          }

          return deprecated;
        };

        var debugs = {};
        var debugEnviron;

        exports.debuglog = function (set) {
          if (isUndefined(debugEnviron)) debugEnviron = process.env.NODE_DEBUG || "";
          set = set.toUpperCase();

          if (!debugs[set]) {
            if (new RegExp("\\b" + set + "\\b", "i").test(debugEnviron)) {
              var pid = process.pid;

              debugs[set] = function () {
                var msg = exports.format.apply(exports, arguments);
                console.error("%s %d: %s", set, pid, msg);
              };
            } else {
              debugs[set] = function () {};
            }
          }

          return debugs[set];
        };

        function inspect(obj, opts) {
          var ctx = {
            seen: [],
            stylize: stylizeNoColor
          };
          if (arguments.length >= 3) ctx.depth = arguments[2];
          if (arguments.length >= 4) ctx.colors = arguments[3];

          if (isBoolean(opts)) {
            ctx.showHidden = opts;
          } else if (opts) {
            exports._extend(ctx, opts);
          }

          if (isUndefined(ctx.showHidden)) ctx.showHidden = false;
          if (isUndefined(ctx.depth)) ctx.depth = 2;
          if (isUndefined(ctx.colors)) ctx.colors = false;
          if (isUndefined(ctx.customInspect)) ctx.customInspect = true;
          if (ctx.colors) ctx.stylize = stylizeWithColor;
          return formatValue(ctx, obj, ctx.depth);
        }

        exports.inspect = inspect;
        inspect.colors = {
          bold: [1, 22],
          italic: [3, 23],
          underline: [4, 24],
          inverse: [7, 27],
          white: [37, 39],
          grey: [90, 39],
          black: [30, 39],
          blue: [34, 39],
          cyan: [36, 39],
          green: [32, 39],
          magenta: [35, 39],
          red: [31, 39],
          yellow: [33, 39]
        };
        inspect.styles = {
          special: "cyan",
          number: "yellow",
          "boolean": "yellow",
          undefined: "grey",
          "null": "bold",
          string: "green",
          date: "magenta",
          regexp: "red"
        };

        function stylizeWithColor(str, styleType) {
          var style = inspect.styles[styleType];

          if (style) {
            return "[" + inspect.colors[style][0] + "m" + str + "[" + inspect.colors[style][1] + "m";
          } else {
            return str;
          }
        }

        function stylizeNoColor(str, styleType) {
          return str;
        }

        function arrayToHash(array) {
          var hash = {};
          array.forEach(function (val, idx) {
            hash[val] = true;
          });
          return hash;
        }

        function formatValue(ctx, value, recurseTimes) {
          if (ctx.customInspect && value && isFunction(value.inspect) && value.inspect !== exports.inspect && !(value.constructor && value.constructor.prototype === value)) {
            var ret = value.inspect(recurseTimes, ctx);

            if (!isString(ret)) {
              ret = formatValue(ctx, ret, recurseTimes);
            }

            return ret;
          }

          var primitive = formatPrimitive(ctx, value);

          if (primitive) {
            return primitive;
          }

          var keys = Object.keys(value);
          var visibleKeys = arrayToHash(keys);

          if (ctx.showHidden) {
            keys = Object.getOwnPropertyNames(value);
          }

          if (isError(value) && (keys.indexOf("message") >= 0 || keys.indexOf("description") >= 0)) {
            return formatError(value);
          }

          if (keys.length === 0) {
            if (isFunction(value)) {
              var name = value.name ? ": " + value.name : "";
              return ctx.stylize("[Function" + name + "]", "special");
            }

            if (isRegExp(value)) {
              return ctx.stylize(RegExp.prototype.toString.call(value), "regexp");
            }

            if (isDate(value)) {
              return ctx.stylize(Date.prototype.toString.call(value), "date");
            }

            if (isError(value)) {
              return formatError(value);
            }
          }

          var base = "",
              array = false,
              braces = ["{", "}"];

          if (isArray(value)) {
            array = true;
            braces = ["[", "]"];
          }

          if (isFunction(value)) {
            var n = value.name ? ": " + value.name : "";
            base = " [Function" + n + "]";
          }

          if (isRegExp(value)) {
            base = " " + RegExp.prototype.toString.call(value);
          }

          if (isDate(value)) {
            base = " " + Date.prototype.toUTCString.call(value);
          }

          if (isError(value)) {
            base = " " + formatError(value);
          }

          if (keys.length === 0 && (!array || value.length == 0)) {
            return braces[0] + base + braces[1];
          }

          if (recurseTimes < 0) {
            if (isRegExp(value)) {
              return ctx.stylize(RegExp.prototype.toString.call(value), "regexp");
            } else {
              return ctx.stylize("[Object]", "special");
            }
          }

          ctx.seen.push(value);
          var output;

          if (array) {
            output = formatArray(ctx, value, recurseTimes, visibleKeys, keys);
          } else {
            output = keys.map(function (key) {
              return formatProperty(ctx, value, recurseTimes, visibleKeys, key, array);
            });
          }

          ctx.seen.pop();
          return reduceToSingleString(output, base, braces);
        }

        function formatPrimitive(ctx, value) {
          if (isUndefined(value)) return ctx.stylize("undefined", "undefined");

          if (isString(value)) {
            var simple = "'" + JSON.stringify(value).replace(/^"|"$/g, "").replace(/'/g, "\\'").replace(/\\"/g, '"') + "'";
            return ctx.stylize(simple, "string");
          }

          if (isNumber(value)) return ctx.stylize("" + value, "number");
          if (isBoolean(value)) return ctx.stylize("" + value, "boolean");
          if (isNull(value)) return ctx.stylize("null", "null");
        }

        function formatError(value) {
          return "[" + Error.prototype.toString.call(value) + "]";
        }

        function formatArray(ctx, value, recurseTimes, visibleKeys, keys) {
          var output = [];

          for (var i = 0, l = value.length; i < l; ++i) {
            if (hasOwnProperty(value, String(i))) {
              output.push(formatProperty(ctx, value, recurseTimes, visibleKeys, String(i), true));
            } else {
              output.push("");
            }
          }

          keys.forEach(function (key) {
            if (!key.match(/^\d+$/)) {
              output.push(formatProperty(ctx, value, recurseTimes, visibleKeys, key, true));
            }
          });
          return output;
        }

        function formatProperty(ctx, value, recurseTimes, visibleKeys, key, array) {
          var name, str, desc;
          desc = Object.getOwnPropertyDescriptor(value, key) || {
            value: value[key]
          };

          if (desc.get) {
            if (desc.set) {
              str = ctx.stylize("[Getter/Setter]", "special");
            } else {
              str = ctx.stylize("[Getter]", "special");
            }
          } else {
            if (desc.set) {
              str = ctx.stylize("[Setter]", "special");
            }
          }

          if (!hasOwnProperty(visibleKeys, key)) {
            name = "[" + key + "]";
          }

          if (!str) {
            if (ctx.seen.indexOf(desc.value) < 0) {
              if (isNull(recurseTimes)) {
                str = formatValue(ctx, desc.value, null);
              } else {
                str = formatValue(ctx, desc.value, recurseTimes - 1);
              }

              if (str.indexOf("\n") > -1) {
                if (array) {
                  str = str.split("\n").map(function (line) {
                    return "  " + line;
                  }).join("\n").substr(2);
                } else {
                  str = "\n" + str.split("\n").map(function (line) {
                    return "   " + line;
                  }).join("\n");
                }
              }
            } else {
              str = ctx.stylize("[Circular]", "special");
            }
          }

          if (isUndefined(name)) {
            if (array && key.match(/^\d+$/)) {
              return str;
            }

            name = JSON.stringify("" + key);

            if (name.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)) {
              name = name.substr(1, name.length - 2);
              name = ctx.stylize(name, "name");
            } else {
              name = name.replace(/'/g, "\\'").replace(/\\"/g, '"').replace(/(^"|"$)/g, "'");
              name = ctx.stylize(name, "string");
            }
          }

          return name + ": " + str;
        }

        function reduceToSingleString(output, base, braces) {
          var numLinesEst = 0;
          var length = output.reduce(function (prev, cur) {
            numLinesEst++;
            if (cur.indexOf("\n") >= 0) numLinesEst++;
            return prev + cur.replace(/\u001b\[\d\d?m/g, "").length + 1;
          }, 0);

          if (length > 60) {
            return braces[0] + (base === "" ? "" : base + "\n ") + " " + output.join(",\n  ") + " " + braces[1];
          }

          return braces[0] + base + " " + output.join(", ") + " " + braces[1];
        }

        function isArray(ar) {
          return Array.isArray(ar);
        }

        exports.isArray = isArray;

        function isBoolean(arg) {
          return typeof arg === "boolean";
        }

        exports.isBoolean = isBoolean;

        function isNull(arg) {
          return arg === null;
        }

        exports.isNull = isNull;

        function isNullOrUndefined(arg) {
          return arg == null;
        }

        exports.isNullOrUndefined = isNullOrUndefined;

        function isNumber(arg) {
          return typeof arg === "number";
        }

        exports.isNumber = isNumber;

        function isString(arg) {
          return typeof arg === "string";
        }

        exports.isString = isString;

        function isSymbol(arg) {
          return _typeof(arg) === "symbol";
        }

        exports.isSymbol = isSymbol;

        function isUndefined(arg) {
          return arg === void 0;
        }

        exports.isUndefined = isUndefined;

        function isRegExp(re) {
          return isObject(re) && objectToString(re) === "[object RegExp]";
        }

        exports.isRegExp = isRegExp;

        function isObject(arg) {
          return _typeof(arg) === "object" && arg !== null;
        }

        exports.isObject = isObject;

        function isDate(d) {
          return isObject(d) && objectToString(d) === "[object Date]";
        }

        exports.isDate = isDate;

        function isError(e) {
          return isObject(e) && (objectToString(e) === "[object Error]" || e instanceof Error);
        }

        exports.isError = isError;

        function isFunction(arg) {
          return typeof arg === "function";
        }

        exports.isFunction = isFunction;

        function isPrimitive(arg) {
          return arg === null || typeof arg === "boolean" || typeof arg === "number" || typeof arg === "string" || _typeof(arg) === "symbol" || typeof arg === "undefined";
        }

        exports.isPrimitive = isPrimitive;
        exports.isBuffer = require("./support/isBuffer");

        function objectToString(o) {
          return Object.prototype.toString.call(o);
        }

        function pad(n) {
          return n < 10 ? "0" + n.toString(10) : n.toString(10);
        }

        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        function timestamp() {
          var d = new Date();
          var time = [pad(d.getHours()), pad(d.getMinutes()), pad(d.getSeconds())].join(":");
          return [d.getDate(), months[d.getMonth()], time].join(" ");
        }

        exports.log = function () {
          console.log("%s - %s", timestamp(), exports.format.apply(exports, arguments));
        };

        exports.inherits = require("inherits");

        exports._extend = function (origin, add) {
          if (!add || !isObject(add)) return origin;
          var keys = Object.keys(add);
          var i = keys.length;

          while (i--) {
            origin[keys[i]] = add[keys[i]];
          }

          return origin;
        };

        function hasOwnProperty(obj, prop) {
          return Object.prototype.hasOwnProperty.call(obj, prop);
        }
      }).call(this, require("_process"), typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {});
    }, {
      "./support/isBuffer": 32,
      _process: 17,
      inherits: 31
    }]
  }, {}, [1])(1);
});

(function webpackUniversalModuleDefinition(root, factory) {
  if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === "object" && (typeof module === "undefined" ? "undefined" : _typeof(module)) === "object") module.exports = factory(require("msgpack5"), require("signalR"));else if (typeof define === "function" && define.amd) define(["msgpack5", "signalR"], factory);else if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === "object") exports["msgpack"] = factory(require("msgpack5"), require("signalR"));else root["signalR"] = root["signalR"] || {}, root["signalR"]["protocols"] = root["signalR"]["protocols"] || {}, root["signalR"]["protocols"]["msgpack"] = factory(root["msgpack5"], root["signalR"]);
})(window, function (__WEBPACK_EXTERNAL_MODULE__6__, __WEBPACK_EXTERNAL_MODULE__7__) {
  return function (modules) {
    var installedModules = {};

    function __webpack_require__(moduleId) {
      if (installedModules[moduleId]) {
        return installedModules[moduleId].exports;
      }

      var module = installedModules[moduleId] = {
        i: moduleId,
        l: false,
        exports: {}
      };
      modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
      module.l = true;
      return module.exports;
    }

    __webpack_require__.m = modules;
    __webpack_require__.c = installedModules;

    __webpack_require__.d = function (exports, name, getter) {
      if (!__webpack_require__.o(exports, name)) {
        Object.defineProperty(exports, name, {
          enumerable: true,
          get: getter
        });
      }
    };

    __webpack_require__.r = function (exports) {
      if (typeof Symbol !== "undefined" && Symbol.toStringTag) {
        Object.defineProperty(exports, Symbol.toStringTag, {
          value: "Module"
        });
      }

      Object.defineProperty(exports, "__esModule", {
        value: true
      });
    };

    __webpack_require__.t = function (value, mode) {
      if (mode & 1) value = __webpack_require__(value);
      if (mode & 8) return value;
      if (mode & 4 && _typeof(value) === "object" && value && value.__esModule) return value;
      var ns = Object.create(null);

      __webpack_require__.r(ns);

      Object.defineProperty(ns, "default", {
        enumerable: true,
        value: value
      });
      if (mode & 2 && typeof value != "string") for (var key in value) {
        __webpack_require__.d(ns, key, function (key) {
          return value[key];
        }.bind(null, key));
      }
      return ns;
    };

    __webpack_require__.n = function (module) {
      var getter = module && module.__esModule ? function getDefault() {
        return module["default"];
      } : function getModuleExports() {
        return module;
      };

      __webpack_require__.d(getter, "a", getter);

      return getter;
    };

    __webpack_require__.o = function (object, property) {
      return Object.prototype.hasOwnProperty.call(object, property);
    };

    __webpack_require__.p = "";
    return __webpack_require__(__webpack_require__.s = 0);
  }([function (module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);

    var _index__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1);

    __webpack_require__.d(__webpack_exports__, "VERSION", function () {
      return _index__WEBPACK_IMPORTED_MODULE_0__["VERSION"];
    });

    __webpack_require__.d(__webpack_exports__, "MessagePackHubProtocol", function () {
      return _index__WEBPACK_IMPORTED_MODULE_0__["MessagePackHubProtocol"];
    });
  }, function (module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);

    __webpack_require__.d(__webpack_exports__, "VERSION", function () {
      return VERSION;
    });

    var _MessagePackHubProtocol__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2);

    __webpack_require__.d(__webpack_exports__, "MessagePackHubProtocol", function () {
      return _MessagePackHubProtocol__WEBPACK_IMPORTED_MODULE_0__["MessagePackHubProtocol"];
    });

    var VERSION = "1.1.0";
  }, function (module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);

    __webpack_require__.d(__webpack_exports__, "MessagePackHubProtocol", function () {
      return MessagePackHubProtocol;
    });

    var buffer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(3);

    var buffer__WEBPACK_IMPORTED_MODULE_0___default = __webpack_require__.n(buffer__WEBPACK_IMPORTED_MODULE_0__);

    var msgpack5__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6);

    var msgpack5__WEBPACK_IMPORTED_MODULE_1___default = __webpack_require__.n(msgpack5__WEBPACK_IMPORTED_MODULE_1__);

    var _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7);

    var _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2___default = __webpack_require__.n(_aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__);

    var _BinaryMessageFormat__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8);

    var _Utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9);

    var SERIALIZED_PING_MESSAGE = new Uint8Array([145, _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Ping]);

    var MessagePackHubProtocol = function () {
      function MessagePackHubProtocol() {
        this.name = "messagepack";
        this.version = 1;
        this.transferFormat = _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["TransferFormat"].Binary;
      }

      MessagePackHubProtocol.prototype.parseMessages = function (input, logger) {
        if (!(input instanceof buffer__WEBPACK_IMPORTED_MODULE_0__["Buffer"]) && !Object(_Utils__WEBPACK_IMPORTED_MODULE_4__["isArrayBuffer"])(input)) {
          throw new Error("Invalid input for MessagePack hub protocol. Expected an ArrayBuffer or Buffer.");
        }

        if (logger === null) {
          logger = _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["NullLogger"].instance;
        }

        var messages = _BinaryMessageFormat__WEBPACK_IMPORTED_MODULE_3__["BinaryMessageFormat"].parse(input);

        var hubMessages = [];

        for (var _i = 0, messages_1 = messages; _i < messages_1.length; _i++) {
          var message = messages_1[_i];
          var parsedMessage = this.parseMessage(message, logger);

          if (parsedMessage) {
            hubMessages.push(parsedMessage);
          }
        }

        return hubMessages;
      };

      MessagePackHubProtocol.prototype.writeMessage = function (message) {
        switch (message.type) {
          case _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Invocation:
            return this.writeInvocation(message);

          case _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].StreamInvocation:
            return this.writeStreamInvocation(message);

          case _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].StreamItem:
          case _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Completion:
            throw new Error("Writing messages of type '" + message.type + "' is not supported.");

          case _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Ping:
            return _BinaryMessageFormat__WEBPACK_IMPORTED_MODULE_3__["BinaryMessageFormat"].write(SERIALIZED_PING_MESSAGE);

          default:
            throw new Error("Invalid message type.");
        }
      };

      MessagePackHubProtocol.prototype.parseMessage = function (input, logger) {
        if (input.length === 0) {
          throw new Error("Invalid payload.");
        }

        var msgpack = msgpack5__WEBPACK_IMPORTED_MODULE_1__();
        var properties = msgpack.decode(buffer__WEBPACK_IMPORTED_MODULE_0__["Buffer"].from(input));

        if (properties.length === 0 || !(properties instanceof Array)) {
          throw new Error("Invalid payload.");
        }

        var messageType = properties[0];

        switch (messageType) {
          case _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Invocation:
            return this.createInvocationMessage(this.readHeaders(properties), properties);

          case _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].StreamItem:
            return this.createStreamItemMessage(this.readHeaders(properties), properties);

          case _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Completion:
            return this.createCompletionMessage(this.readHeaders(properties), properties);

          case _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Ping:
            return this.createPingMessage(properties);

          case _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Close:
            return this.createCloseMessage(properties);

          default:
            logger.log(_aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["LogLevel"].Information, "Unknown message type '" + messageType + "' ignored.");
            return null;
        }
      };

      MessagePackHubProtocol.prototype.createCloseMessage = function (properties) {
        if (properties.length < 2) {
          throw new Error("Invalid payload for Close message.");
        }

        return {
          error: properties[1],
          type: _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Close
        };
      };

      MessagePackHubProtocol.prototype.createPingMessage = function (properties) {
        if (properties.length < 1) {
          throw new Error("Invalid payload for Ping message.");
        }

        return {
          type: _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Ping
        };
      };

      MessagePackHubProtocol.prototype.createInvocationMessage = function (headers, properties) {
        if (properties.length < 5) {
          throw new Error("Invalid payload for Invocation message.");
        }

        var invocationId = properties[2];

        if (invocationId) {
          return {
            arguments: properties[4],
            headers: headers,
            invocationId: invocationId,
            target: properties[3],
            type: _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Invocation
          };
        } else {
          return {
            arguments: properties[4],
            headers: headers,
            target: properties[3],
            type: _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Invocation
          };
        }
      };

      MessagePackHubProtocol.prototype.createStreamItemMessage = function (headers, properties) {
        if (properties.length < 4) {
          throw new Error("Invalid payload for StreamItem message.");
        }

        return {
          headers: headers,
          invocationId: properties[2],
          item: properties[3],
          type: _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].StreamItem
        };
      };

      MessagePackHubProtocol.prototype.createCompletionMessage = function (headers, properties) {
        if (properties.length < 4) {
          throw new Error("Invalid payload for Completion message.");
        }

        var errorResult = 1;
        var voidResult = 2;
        var nonVoidResult = 3;
        var resultKind = properties[3];

        if (resultKind !== voidResult && properties.length < 5) {
          throw new Error("Invalid payload for Completion message.");
        }

        var error;
        var result;

        switch (resultKind) {
          case errorResult:
            error = properties[4];
            break;

          case nonVoidResult:
            result = properties[4];
            break;
        }

        var completionMessage = {
          error: error,
          headers: headers,
          invocationId: properties[2],
          result: result,
          type: _aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Completion
        };
        return completionMessage;
      };

      MessagePackHubProtocol.prototype.writeInvocation = function (invocationMessage) {
        var msgpack = msgpack5__WEBPACK_IMPORTED_MODULE_1__();
        var payload = msgpack.encode([_aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].Invocation, invocationMessage.headers || {}, invocationMessage.invocationId || null, invocationMessage.target, invocationMessage.arguments]);
        return _BinaryMessageFormat__WEBPACK_IMPORTED_MODULE_3__["BinaryMessageFormat"].write(payload.slice());
      };

      MessagePackHubProtocol.prototype.writeStreamInvocation = function (streamInvocationMessage) {
        var msgpack = msgpack5__WEBPACK_IMPORTED_MODULE_1__();
        var payload = msgpack.encode([_aspnet_signalr__WEBPACK_IMPORTED_MODULE_2__["MessageType"].StreamInvocation, streamInvocationMessage.headers || {}, streamInvocationMessage.invocationId, streamInvocationMessage.target, streamInvocationMessage.arguments]);
        return _BinaryMessageFormat__WEBPACK_IMPORTED_MODULE_3__["BinaryMessageFormat"].write(payload.slice());
      };

      MessagePackHubProtocol.prototype.readHeaders = function (properties) {
        var headers = properties[1];

        if (_typeof(headers) !== "object") {
          throw new Error("Invalid headers.");
        }

        return headers;
      };

      return MessagePackHubProtocol;
    }();
  }, function (module, exports, __webpack_require__) {
    "use strict";
    /*!
     * The buffer module from node.js, for the browser.
     *
     * @author   Feross Aboukhadijeh <https://feross.org>
     * @license  MIT
     */

    var base64 = __webpack_require__(4);

    var ieee754 = __webpack_require__(5);

    exports.Buffer = Buffer;
    exports.SlowBuffer = SlowBuffer;
    exports.INSPECT_MAX_BYTES = 50;
    var K_MAX_LENGTH = 2147483647;
    exports.kMaxLength = K_MAX_LENGTH;
    Buffer.TYPED_ARRAY_SUPPORT = typedArraySupport();

    if (!Buffer.TYPED_ARRAY_SUPPORT && typeof console !== "undefined" && typeof console.error === "function") {
      console.error("This browser lacks typed array (Uint8Array) support which is required by " + "`buffer` v5.x. Use `buffer` v4.x if you require old browser support.");
    }

    function typedArraySupport() {
      try {
        var arr = new Uint8Array(1);
        arr.__proto__ = {
          __proto__: Uint8Array.prototype,
          foo: function foo() {
            return 42;
          }
        };
        return arr.foo() === 42;
      } catch (e) {
        return false;
      }
    }

    function createBuffer(length) {
      if (length > K_MAX_LENGTH) {
        throw new RangeError("Invalid typed array length");
      }

      var buf = new Uint8Array(length);
      buf.__proto__ = Buffer.prototype;
      return buf;
    }

    function Buffer(arg, encodingOrOffset, length) {
      if (typeof arg === "number") {
        if (typeof encodingOrOffset === "string") {
          throw new Error("If encoding is specified then the first argument must be a string");
        }

        return allocUnsafe(arg);
      }

      return from(arg, encodingOrOffset, length);
    }

    if (typeof Symbol !== "undefined" && Symbol.species && Buffer[Symbol.species] === Buffer) {
      Object.defineProperty(Buffer, Symbol.species, {
        value: null,
        configurable: true,
        enumerable: false,
        writable: false
      });
    }

    Buffer.poolSize = 8192;

    function from(value, encodingOrOffset, length) {
      if (typeof value === "number") {
        throw new TypeError('"value" argument must not be a number');
      }

      if (isArrayBuffer(value)) {
        return fromArrayBuffer(value, encodingOrOffset, length);
      }

      if (typeof value === "string") {
        return fromString(value, encodingOrOffset);
      }

      return fromObject(value);
    }

    Buffer.from = function (value, encodingOrOffset, length) {
      return from(value, encodingOrOffset, length);
    };

    Buffer.prototype.__proto__ = Uint8Array.prototype;
    Buffer.__proto__ = Uint8Array;

    function assertSize(size) {
      if (typeof size !== "number") {
        throw new TypeError('"size" argument must be a number');
      } else if (size < 0) {
        throw new RangeError('"size" argument must not be negative');
      }
    }

    function alloc(size, fill, encoding) {
      assertSize(size);

      if (size <= 0) {
        return createBuffer(size);
      }

      if (fill !== undefined) {
        return typeof encoding === "string" ? createBuffer(size).fill(fill, encoding) : createBuffer(size).fill(fill);
      }

      return createBuffer(size);
    }

    Buffer.alloc = function (size, fill, encoding) {
      return alloc(size, fill, encoding);
    };

    function allocUnsafe(size) {
      assertSize(size);
      return createBuffer(size < 0 ? 0 : checked(size) | 0);
    }

    Buffer.allocUnsafe = function (size) {
      return allocUnsafe(size);
    };

    Buffer.allocUnsafeSlow = function (size) {
      return allocUnsafe(size);
    };

    function fromString(string, encoding) {
      if (typeof encoding !== "string" || encoding === "") {
        encoding = "utf8";
      }

      if (!Buffer.isEncoding(encoding)) {
        throw new TypeError('"encoding" must be a valid string encoding');
      }

      var length = byteLength(string, encoding) | 0;
      var buf = createBuffer(length);
      var actual = buf.write(string, encoding);

      if (actual !== length) {
        buf = buf.slice(0, actual);
      }

      return buf;
    }

    function fromArrayLike(array) {
      var length = array.length < 0 ? 0 : checked(array.length) | 0;
      var buf = createBuffer(length);

      for (var i = 0; i < length; i += 1) {
        buf[i] = array[i] & 255;
      }

      return buf;
    }

    function fromArrayBuffer(array, byteOffset, length) {
      if (byteOffset < 0 || array.byteLength < byteOffset) {
        throw new RangeError("'offset' is out of bounds");
      }

      if (array.byteLength < byteOffset + (length || 0)) {
        throw new RangeError("'length' is out of bounds");
      }

      var buf;

      if (byteOffset === undefined && length === undefined) {
        buf = new Uint8Array(array);
      } else if (length === undefined) {
        buf = new Uint8Array(array, byteOffset);
      } else {
        buf = new Uint8Array(array, byteOffset, length);
      }

      buf.__proto__ = Buffer.prototype;
      return buf;
    }

    function fromObject(obj) {
      if (Buffer.isBuffer(obj)) {
        var len = checked(obj.length) | 0;
        var buf = createBuffer(len);

        if (buf.length === 0) {
          return buf;
        }

        obj.copy(buf, 0, 0, len);
        return buf;
      }

      if (obj) {
        if (isArrayBufferView(obj) || "length" in obj) {
          if (typeof obj.length !== "number" || numberIsNaN(obj.length)) {
            return createBuffer(0);
          }

          return fromArrayLike(obj);
        }

        if (obj.type === "Buffer" && Array.isArray(obj.data)) {
          return fromArrayLike(obj.data);
        }
      }

      throw new TypeError("First argument must be a string, Buffer, ArrayBuffer, Array, or array-like object.");
    }

    function checked(length) {
      if (length >= K_MAX_LENGTH) {
        throw new RangeError("Attempt to allocate Buffer larger than maximum " + "size: 0x" + K_MAX_LENGTH.toString(16) + " bytes");
      }

      return length | 0;
    }

    function SlowBuffer(length) {
      if (+length != length) {
        length = 0;
      }

      return Buffer.alloc(+length);
    }

    Buffer.isBuffer = function isBuffer(b) {
      return b != null && b._isBuffer === true;
    };

    Buffer.compare = function compare(a, b) {
      if (!Buffer.isBuffer(a) || !Buffer.isBuffer(b)) {
        throw new TypeError("Arguments must be Buffers");
      }

      if (a === b) return 0;
      var x = a.length;
      var y = b.length;

      for (var i = 0, len = Math.min(x, y); i < len; ++i) {
        if (a[i] !== b[i]) {
          x = a[i];
          y = b[i];
          break;
        }
      }

      if (x < y) return -1;
      if (y < x) return 1;
      return 0;
    };

    Buffer.isEncoding = function isEncoding(encoding) {
      switch (String(encoding).toLowerCase()) {
        case "hex":
        case "utf8":
        case "utf-8":
        case "ascii":
        case "latin1":
        case "binary":
        case "base64":
        case "ucs2":
        case "ucs-2":
        case "utf16le":
        case "utf-16le":
          return true;

        default:
          return false;
      }
    };

    Buffer.concat = function concat(list, length) {
      if (!Array.isArray(list)) {
        throw new TypeError('"list" argument must be an Array of Buffers');
      }

      if (list.length === 0) {
        return Buffer.alloc(0);
      }

      var i;

      if (length === undefined) {
        length = 0;

        for (i = 0; i < list.length; ++i) {
          length += list[i].length;
        }
      }

      var buffer = Buffer.allocUnsafe(length);
      var pos = 0;

      for (i = 0; i < list.length; ++i) {
        var buf = list[i];

        if (!Buffer.isBuffer(buf)) {
          throw new TypeError('"list" argument must be an Array of Buffers');
        }

        buf.copy(buffer, pos);
        pos += buf.length;
      }

      return buffer;
    };

    function byteLength(string, encoding) {
      if (Buffer.isBuffer(string)) {
        return string.length;
      }

      if (isArrayBufferView(string) || isArrayBuffer(string)) {
        return string.byteLength;
      }

      if (typeof string !== "string") {
        string = "" + string;
      }

      var len = string.length;
      if (len === 0) return 0;
      var loweredCase = false;

      for (;;) {
        switch (encoding) {
          case "ascii":
          case "latin1":
          case "binary":
            return len;

          case "utf8":
          case "utf-8":
          case undefined:
            return utf8ToBytes(string).length;

          case "ucs2":
          case "ucs-2":
          case "utf16le":
          case "utf-16le":
            return len * 2;

          case "hex":
            return len >>> 1;

          case "base64":
            return base64ToBytes(string).length;

          default:
            if (loweredCase) return utf8ToBytes(string).length;
            encoding = ("" + encoding).toLowerCase();
            loweredCase = true;
        }
      }
    }

    Buffer.byteLength = byteLength;

    function slowToString(encoding, start, end) {
      var loweredCase = false;

      if (start === undefined || start < 0) {
        start = 0;
      }

      if (start > this.length) {
        return "";
      }

      if (end === undefined || end > this.length) {
        end = this.length;
      }

      if (end <= 0) {
        return "";
      }

      end >>>= 0;
      start >>>= 0;

      if (end <= start) {
        return "";
      }

      if (!encoding) encoding = "utf8";

      while (true) {
        switch (encoding) {
          case "hex":
            return hexSlice(this, start, end);

          case "utf8":
          case "utf-8":
            return utf8Slice(this, start, end);

          case "ascii":
            return asciiSlice(this, start, end);

          case "latin1":
          case "binary":
            return latin1Slice(this, start, end);

          case "base64":
            return base64Slice(this, start, end);

          case "ucs2":
          case "ucs-2":
          case "utf16le":
          case "utf-16le":
            return utf16leSlice(this, start, end);

          default:
            if (loweredCase) throw new TypeError("Unknown encoding: " + encoding);
            encoding = (encoding + "").toLowerCase();
            loweredCase = true;
        }
      }
    }

    Buffer.prototype._isBuffer = true;

    function swap(b, n, m) {
      var i = b[n];
      b[n] = b[m];
      b[m] = i;
    }

    Buffer.prototype.swap16 = function swap16() {
      var len = this.length;

      if (len % 2 !== 0) {
        throw new RangeError("Buffer size must be a multiple of 16-bits");
      }

      for (var i = 0; i < len; i += 2) {
        swap(this, i, i + 1);
      }

      return this;
    };

    Buffer.prototype.swap32 = function swap32() {
      var len = this.length;

      if (len % 4 !== 0) {
        throw new RangeError("Buffer size must be a multiple of 32-bits");
      }

      for (var i = 0; i < len; i += 4) {
        swap(this, i, i + 3);
        swap(this, i + 1, i + 2);
      }

      return this;
    };

    Buffer.prototype.swap64 = function swap64() {
      var len = this.length;

      if (len % 8 !== 0) {
        throw new RangeError("Buffer size must be a multiple of 64-bits");
      }

      for (var i = 0; i < len; i += 8) {
        swap(this, i, i + 7);
        swap(this, i + 1, i + 6);
        swap(this, i + 2, i + 5);
        swap(this, i + 3, i + 4);
      }

      return this;
    };

    Buffer.prototype.toString = function toString() {
      var length = this.length;
      if (length === 0) return "";
      if (arguments.length === 0) return utf8Slice(this, 0, length);
      return slowToString.apply(this, arguments);
    };

    Buffer.prototype.equals = function equals(b) {
      if (!Buffer.isBuffer(b)) throw new TypeError("Argument must be a Buffer");
      if (this === b) return true;
      return Buffer.compare(this, b) === 0;
    };

    Buffer.prototype.inspect = function inspect() {
      var str = "";
      var max = exports.INSPECT_MAX_BYTES;

      if (this.length > 0) {
        str = this.toString("hex", 0, max).match(/.{2}/g).join(" ");
        if (this.length > max) str += " ... ";
      }

      return "<Buffer " + str + ">";
    };

    Buffer.prototype.compare = function compare(target, start, end, thisStart, thisEnd) {
      if (!Buffer.isBuffer(target)) {
        throw new TypeError("Argument must be a Buffer");
      }

      if (start === undefined) {
        start = 0;
      }

      if (end === undefined) {
        end = target ? target.length : 0;
      }

      if (thisStart === undefined) {
        thisStart = 0;
      }

      if (thisEnd === undefined) {
        thisEnd = this.length;
      }

      if (start < 0 || end > target.length || thisStart < 0 || thisEnd > this.length) {
        throw new RangeError("out of range index");
      }

      if (thisStart >= thisEnd && start >= end) {
        return 0;
      }

      if (thisStart >= thisEnd) {
        return -1;
      }

      if (start >= end) {
        return 1;
      }

      start >>>= 0;
      end >>>= 0;
      thisStart >>>= 0;
      thisEnd >>>= 0;
      if (this === target) return 0;
      var x = thisEnd - thisStart;
      var y = end - start;
      var len = Math.min(x, y);
      var thisCopy = this.slice(thisStart, thisEnd);
      var targetCopy = target.slice(start, end);

      for (var i = 0; i < len; ++i) {
        if (thisCopy[i] !== targetCopy[i]) {
          x = thisCopy[i];
          y = targetCopy[i];
          break;
        }
      }

      if (x < y) return -1;
      if (y < x) return 1;
      return 0;
    };

    function bidirectionalIndexOf(buffer, val, byteOffset, encoding, dir) {
      if (buffer.length === 0) return -1;

      if (typeof byteOffset === "string") {
        encoding = byteOffset;
        byteOffset = 0;
      } else if (byteOffset > 2147483647) {
        byteOffset = 2147483647;
      } else if (byteOffset < -2147483648) {
        byteOffset = -2147483648;
      }

      byteOffset = +byteOffset;

      if (numberIsNaN(byteOffset)) {
        byteOffset = dir ? 0 : buffer.length - 1;
      }

      if (byteOffset < 0) byteOffset = buffer.length + byteOffset;

      if (byteOffset >= buffer.length) {
        if (dir) return -1;else byteOffset = buffer.length - 1;
      } else if (byteOffset < 0) {
        if (dir) byteOffset = 0;else return -1;
      }

      if (typeof val === "string") {
        val = Buffer.from(val, encoding);
      }

      if (Buffer.isBuffer(val)) {
        if (val.length === 0) {
          return -1;
        }

        return arrayIndexOf(buffer, val, byteOffset, encoding, dir);
      } else if (typeof val === "number") {
        val = val & 255;

        if (typeof Uint8Array.prototype.indexOf === "function") {
          if (dir) {
            return Uint8Array.prototype.indexOf.call(buffer, val, byteOffset);
          } else {
            return Uint8Array.prototype.lastIndexOf.call(buffer, val, byteOffset);
          }
        }

        return arrayIndexOf(buffer, [val], byteOffset, encoding, dir);
      }

      throw new TypeError("val must be string, number or Buffer");
    }

    function arrayIndexOf(arr, val, byteOffset, encoding, dir) {
      var indexSize = 1;
      var arrLength = arr.length;
      var valLength = val.length;

      if (encoding !== undefined) {
        encoding = String(encoding).toLowerCase();

        if (encoding === "ucs2" || encoding === "ucs-2" || encoding === "utf16le" || encoding === "utf-16le") {
          if (arr.length < 2 || val.length < 2) {
            return -1;
          }

          indexSize = 2;
          arrLength /= 2;
          valLength /= 2;
          byteOffset /= 2;
        }
      }

      function read(buf, i) {
        if (indexSize === 1) {
          return buf[i];
        } else {
          return buf.readUInt16BE(i * indexSize);
        }
      }

      var i;

      if (dir) {
        var foundIndex = -1;

        for (i = byteOffset; i < arrLength; i++) {
          if (read(arr, i) === read(val, foundIndex === -1 ? 0 : i - foundIndex)) {
            if (foundIndex === -1) foundIndex = i;
            if (i - foundIndex + 1 === valLength) return foundIndex * indexSize;
          } else {
            if (foundIndex !== -1) i -= i - foundIndex;
            foundIndex = -1;
          }
        }
      } else {
        if (byteOffset + valLength > arrLength) byteOffset = arrLength - valLength;

        for (i = byteOffset; i >= 0; i--) {
          var found = true;

          for (var j = 0; j < valLength; j++) {
            if (read(arr, i + j) !== read(val, j)) {
              found = false;
              break;
            }
          }

          if (found) return i;
        }
      }

      return -1;
    }

    Buffer.prototype.includes = function includes(val, byteOffset, encoding) {
      return this.indexOf(val, byteOffset, encoding) !== -1;
    };

    Buffer.prototype.indexOf = function indexOf(val, byteOffset, encoding) {
      return bidirectionalIndexOf(this, val, byteOffset, encoding, true);
    };

    Buffer.prototype.lastIndexOf = function lastIndexOf(val, byteOffset, encoding) {
      return bidirectionalIndexOf(this, val, byteOffset, encoding, false);
    };

    function hexWrite(buf, string, offset, length) {
      offset = Number(offset) || 0;
      var remaining = buf.length - offset;

      if (!length) {
        length = remaining;
      } else {
        length = Number(length);

        if (length > remaining) {
          length = remaining;
        }
      }

      var strLen = string.length;
      if (strLen % 2 !== 0) throw new TypeError("Invalid hex string");

      if (length > strLen / 2) {
        length = strLen / 2;
      }

      for (var i = 0; i < length; ++i) {
        var parsed = parseInt(string.substr(i * 2, 2), 16);
        if (numberIsNaN(parsed)) return i;
        buf[offset + i] = parsed;
      }

      return i;
    }

    function utf8Write(buf, string, offset, length) {
      return blitBuffer(utf8ToBytes(string, buf.length - offset), buf, offset, length);
    }

    function asciiWrite(buf, string, offset, length) {
      return blitBuffer(asciiToBytes(string), buf, offset, length);
    }

    function latin1Write(buf, string, offset, length) {
      return asciiWrite(buf, string, offset, length);
    }

    function base64Write(buf, string, offset, length) {
      return blitBuffer(base64ToBytes(string), buf, offset, length);
    }

    function ucs2Write(buf, string, offset, length) {
      return blitBuffer(utf16leToBytes(string, buf.length - offset), buf, offset, length);
    }

    Buffer.prototype.write = function write(string, offset, length, encoding) {
      if (offset === undefined) {
        encoding = "utf8";
        length = this.length;
        offset = 0;
      } else if (length === undefined && typeof offset === "string") {
        encoding = offset;
        length = this.length;
        offset = 0;
      } else if (isFinite(offset)) {
        offset = offset >>> 0;

        if (isFinite(length)) {
          length = length >>> 0;
          if (encoding === undefined) encoding = "utf8";
        } else {
          encoding = length;
          length = undefined;
        }
      } else {
        throw new Error("Buffer.write(string, encoding, offset[, length]) is no longer supported");
      }

      var remaining = this.length - offset;
      if (length === undefined || length > remaining) length = remaining;

      if (string.length > 0 && (length < 0 || offset < 0) || offset > this.length) {
        throw new RangeError("Attempt to write outside buffer bounds");
      }

      if (!encoding) encoding = "utf8";
      var loweredCase = false;

      for (;;) {
        switch (encoding) {
          case "hex":
            return hexWrite(this, string, offset, length);

          case "utf8":
          case "utf-8":
            return utf8Write(this, string, offset, length);

          case "ascii":
            return asciiWrite(this, string, offset, length);

          case "latin1":
          case "binary":
            return latin1Write(this, string, offset, length);

          case "base64":
            return base64Write(this, string, offset, length);

          case "ucs2":
          case "ucs-2":
          case "utf16le":
          case "utf-16le":
            return ucs2Write(this, string, offset, length);

          default:
            if (loweredCase) throw new TypeError("Unknown encoding: " + encoding);
            encoding = ("" + encoding).toLowerCase();
            loweredCase = true;
        }
      }
    };

    Buffer.prototype.toJSON = function toJSON() {
      return {
        type: "Buffer",
        data: Array.prototype.slice.call(this._arr || this, 0)
      };
    };

    function base64Slice(buf, start, end) {
      if (start === 0 && end === buf.length) {
        return base64.fromByteArray(buf);
      } else {
        return base64.fromByteArray(buf.slice(start, end));
      }
    }

    function utf8Slice(buf, start, end) {
      end = Math.min(buf.length, end);
      var res = [];
      var i = start;

      while (i < end) {
        var firstByte = buf[i];
        var codePoint = null;
        var bytesPerSequence = firstByte > 239 ? 4 : firstByte > 223 ? 3 : firstByte > 191 ? 2 : 1;

        if (i + bytesPerSequence <= end) {
          var secondByte, thirdByte, fourthByte, tempCodePoint;

          switch (bytesPerSequence) {
            case 1:
              if (firstByte < 128) {
                codePoint = firstByte;
              }

              break;

            case 2:
              secondByte = buf[i + 1];

              if ((secondByte & 192) === 128) {
                tempCodePoint = (firstByte & 31) << 6 | secondByte & 63;

                if (tempCodePoint > 127) {
                  codePoint = tempCodePoint;
                }
              }

              break;

            case 3:
              secondByte = buf[i + 1];
              thirdByte = buf[i + 2];

              if ((secondByte & 192) === 128 && (thirdByte & 192) === 128) {
                tempCodePoint = (firstByte & 15) << 12 | (secondByte & 63) << 6 | thirdByte & 63;

                if (tempCodePoint > 2047 && (tempCodePoint < 55296 || tempCodePoint > 57343)) {
                  codePoint = tempCodePoint;
                }
              }

              break;

            case 4:
              secondByte = buf[i + 1];
              thirdByte = buf[i + 2];
              fourthByte = buf[i + 3];

              if ((secondByte & 192) === 128 && (thirdByte & 192) === 128 && (fourthByte & 192) === 128) {
                tempCodePoint = (firstByte & 15) << 18 | (secondByte & 63) << 12 | (thirdByte & 63) << 6 | fourthByte & 63;

                if (tempCodePoint > 65535 && tempCodePoint < 1114112) {
                  codePoint = tempCodePoint;
                }
              }

          }
        }

        if (codePoint === null) {
          codePoint = 65533;
          bytesPerSequence = 1;
        } else if (codePoint > 65535) {
          codePoint -= 65536;
          res.push(codePoint >>> 10 & 1023 | 55296);
          codePoint = 56320 | codePoint & 1023;
        }

        res.push(codePoint);
        i += bytesPerSequence;
      }

      return decodeCodePointsArray(res);
    }

    var MAX_ARGUMENTS_LENGTH = 4096;

    function decodeCodePointsArray(codePoints) {
      var len = codePoints.length;

      if (len <= MAX_ARGUMENTS_LENGTH) {
        return String.fromCharCode.apply(String, codePoints);
      }

      var res = "";
      var i = 0;

      while (i < len) {
        res += String.fromCharCode.apply(String, codePoints.slice(i, i += MAX_ARGUMENTS_LENGTH));
      }

      return res;
    }

    function asciiSlice(buf, start, end) {
      var ret = "";
      end = Math.min(buf.length, end);

      for (var i = start; i < end; ++i) {
        ret += String.fromCharCode(buf[i] & 127);
      }

      return ret;
    }

    function latin1Slice(buf, start, end) {
      var ret = "";
      end = Math.min(buf.length, end);

      for (var i = start; i < end; ++i) {
        ret += String.fromCharCode(buf[i]);
      }

      return ret;
    }

    function hexSlice(buf, start, end) {
      var len = buf.length;
      if (!start || start < 0) start = 0;
      if (!end || end < 0 || end > len) end = len;
      var out = "";

      for (var i = start; i < end; ++i) {
        out += toHex(buf[i]);
      }

      return out;
    }

    function utf16leSlice(buf, start, end) {
      var bytes = buf.slice(start, end);
      var res = "";

      for (var i = 0; i < bytes.length; i += 2) {
        res += String.fromCharCode(bytes[i] + bytes[i + 1] * 256);
      }

      return res;
    }

    Buffer.prototype.slice = function slice(start, end) {
      var len = this.length;
      start = ~~start;
      end = end === undefined ? len : ~~end;

      if (start < 0) {
        start += len;
        if (start < 0) start = 0;
      } else if (start > len) {
        start = len;
      }

      if (end < 0) {
        end += len;
        if (end < 0) end = 0;
      } else if (end > len) {
        end = len;
      }

      if (end < start) end = start;
      var newBuf = this.subarray(start, end);
      newBuf.__proto__ = Buffer.prototype;
      return newBuf;
    };

    function checkOffset(offset, ext, length) {
      if (offset % 1 !== 0 || offset < 0) throw new RangeError("offset is not uint");
      if (offset + ext > length) throw new RangeError("Trying to access beyond buffer length");
    }

    Buffer.prototype.readUIntLE = function readUIntLE(offset, byteLength, noAssert) {
      offset = offset >>> 0;
      byteLength = byteLength >>> 0;
      if (!noAssert) checkOffset(offset, byteLength, this.length);
      var val = this[offset];
      var mul = 1;
      var i = 0;

      while (++i < byteLength && (mul *= 256)) {
        val += this[offset + i] * mul;
      }

      return val;
    };

    Buffer.prototype.readUIntBE = function readUIntBE(offset, byteLength, noAssert) {
      offset = offset >>> 0;
      byteLength = byteLength >>> 0;

      if (!noAssert) {
        checkOffset(offset, byteLength, this.length);
      }

      var val = this[offset + --byteLength];
      var mul = 1;

      while (byteLength > 0 && (mul *= 256)) {
        val += this[offset + --byteLength] * mul;
      }

      return val;
    };

    Buffer.prototype.readUInt8 = function readUInt8(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 1, this.length);
      return this[offset];
    };

    Buffer.prototype.readUInt16LE = function readUInt16LE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 2, this.length);
      return this[offset] | this[offset + 1] << 8;
    };

    Buffer.prototype.readUInt16BE = function readUInt16BE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 2, this.length);
      return this[offset] << 8 | this[offset + 1];
    };

    Buffer.prototype.readUInt32LE = function readUInt32LE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 4, this.length);
      return (this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16) + this[offset + 3] * 16777216;
    };

    Buffer.prototype.readUInt32BE = function readUInt32BE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 4, this.length);
      return this[offset] * 16777216 + (this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3]);
    };

    Buffer.prototype.readIntLE = function readIntLE(offset, byteLength, noAssert) {
      offset = offset >>> 0;
      byteLength = byteLength >>> 0;
      if (!noAssert) checkOffset(offset, byteLength, this.length);
      var val = this[offset];
      var mul = 1;
      var i = 0;

      while (++i < byteLength && (mul *= 256)) {
        val += this[offset + i] * mul;
      }

      mul *= 128;
      if (val >= mul) val -= Math.pow(2, 8 * byteLength);
      return val;
    };

    Buffer.prototype.readIntBE = function readIntBE(offset, byteLength, noAssert) {
      offset = offset >>> 0;
      byteLength = byteLength >>> 0;
      if (!noAssert) checkOffset(offset, byteLength, this.length);
      var i = byteLength;
      var mul = 1;
      var val = this[offset + --i];

      while (i > 0 && (mul *= 256)) {
        val += this[offset + --i] * mul;
      }

      mul *= 128;
      if (val >= mul) val -= Math.pow(2, 8 * byteLength);
      return val;
    };

    Buffer.prototype.readInt8 = function readInt8(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 1, this.length);
      if (!(this[offset] & 128)) return this[offset];
      return (255 - this[offset] + 1) * -1;
    };

    Buffer.prototype.readInt16LE = function readInt16LE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 2, this.length);
      var val = this[offset] | this[offset + 1] << 8;
      return val & 32768 ? val | 4294901760 : val;
    };

    Buffer.prototype.readInt16BE = function readInt16BE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 2, this.length);
      var val = this[offset + 1] | this[offset] << 8;
      return val & 32768 ? val | 4294901760 : val;
    };

    Buffer.prototype.readInt32LE = function readInt32LE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 4, this.length);
      return this[offset] | this[offset + 1] << 8 | this[offset + 2] << 16 | this[offset + 3] << 24;
    };

    Buffer.prototype.readInt32BE = function readInt32BE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 4, this.length);
      return this[offset] << 24 | this[offset + 1] << 16 | this[offset + 2] << 8 | this[offset + 3];
    };

    Buffer.prototype.readFloatLE = function readFloatLE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 4, this.length);
      return ieee754.read(this, offset, true, 23, 4);
    };

    Buffer.prototype.readFloatBE = function readFloatBE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 4, this.length);
      return ieee754.read(this, offset, false, 23, 4);
    };

    Buffer.prototype.readDoubleLE = function readDoubleLE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 8, this.length);
      return ieee754.read(this, offset, true, 52, 8);
    };

    Buffer.prototype.readDoubleBE = function readDoubleBE(offset, noAssert) {
      offset = offset >>> 0;
      if (!noAssert) checkOffset(offset, 8, this.length);
      return ieee754.read(this, offset, false, 52, 8);
    };

    function checkInt(buf, value, offset, ext, max, min) {
      if (!Buffer.isBuffer(buf)) throw new TypeError('"buffer" argument must be a Buffer instance');
      if (value > max || value < min) throw new RangeError('"value" argument is out of bounds');
      if (offset + ext > buf.length) throw new RangeError("Index out of range");
    }

    Buffer.prototype.writeUIntLE = function writeUIntLE(value, offset, byteLength, noAssert) {
      value = +value;
      offset = offset >>> 0;
      byteLength = byteLength >>> 0;

      if (!noAssert) {
        var maxBytes = Math.pow(2, 8 * byteLength) - 1;
        checkInt(this, value, offset, byteLength, maxBytes, 0);
      }

      var mul = 1;
      var i = 0;
      this[offset] = value & 255;

      while (++i < byteLength && (mul *= 256)) {
        this[offset + i] = value / mul & 255;
      }

      return offset + byteLength;
    };

    Buffer.prototype.writeUIntBE = function writeUIntBE(value, offset, byteLength, noAssert) {
      value = +value;
      offset = offset >>> 0;
      byteLength = byteLength >>> 0;

      if (!noAssert) {
        var maxBytes = Math.pow(2, 8 * byteLength) - 1;
        checkInt(this, value, offset, byteLength, maxBytes, 0);
      }

      var i = byteLength - 1;
      var mul = 1;
      this[offset + i] = value & 255;

      while (--i >= 0 && (mul *= 256)) {
        this[offset + i] = value / mul & 255;
      }

      return offset + byteLength;
    };

    Buffer.prototype.writeUInt8 = function writeUInt8(value, offset, noAssert) {
      value = +value;
      offset = offset >>> 0;
      if (!noAssert) checkInt(this, value, offset, 1, 255, 0);
      this[offset] = value & 255;
      return offset + 1;
    };

    Buffer.prototype.writeUInt16LE = function writeUInt16LE(value, offset, noAssert) {
      value = +value;
      offset = offset >>> 0;
      if (!noAssert) checkInt(this, value, offset, 2, 65535, 0);
      this[offset] = value & 255;
      this[offset + 1] = value >>> 8;
      return offset + 2;
    };

    Buffer.prototype.writeUInt16BE = function writeUInt16BE(value, offset, noAssert) {
      value = +value;
      offset = offset >>> 0;
      if (!noAssert) checkInt(this, value, offset, 2, 65535, 0);
      this[offset] = value >>> 8;
      this[offset + 1] = value & 255;
      return offset + 2;
    };

    Buffer.prototype.writeUInt32LE = function writeUInt32LE(value, offset, noAssert) {
      value = +value;
      offset = offset >>> 0;
      if (!noAssert) checkInt(this, value, offset, 4, 4294967295, 0);
      this[offset + 3] = value >>> 24;
      this[offset + 2] = value >>> 16;
      this[offset + 1] = value >>> 8;
      this[offset] = value & 255;
      return offset + 4;
    };

    Buffer.prototype.writeUInt32BE = function writeUInt32BE(value, offset, noAssert) {
      value = +value;
      offset = offset >>> 0;
      if (!noAssert) checkInt(this, value, offset, 4, 4294967295, 0);
      this[offset] = value >>> 24;
      this[offset + 1] = value >>> 16;
      this[offset + 2] = value >>> 8;
      this[offset + 3] = value & 255;
      return offset + 4;
    };

    Buffer.prototype.writeIntLE = function writeIntLE(value, offset, byteLength, noAssert) {
      value = +value;
      offset = offset >>> 0;

      if (!noAssert) {
        var limit = Math.pow(2, 8 * byteLength - 1);
        checkInt(this, value, offset, byteLength, limit - 1, -limit);
      }

      var i = 0;
      var mul = 1;
      var sub = 0;
      this[offset] = value & 255;

      while (++i < byteLength && (mul *= 256)) {
        if (value < 0 && sub === 0 && this[offset + i - 1] !== 0) {
          sub = 1;
        }

        this[offset + i] = (value / mul >> 0) - sub & 255;
      }

      return offset + byteLength;
    };

    Buffer.prototype.writeIntBE = function writeIntBE(value, offset, byteLength, noAssert) {
      value = +value;
      offset = offset >>> 0;

      if (!noAssert) {
        var limit = Math.pow(2, 8 * byteLength - 1);
        checkInt(this, value, offset, byteLength, limit - 1, -limit);
      }

      var i = byteLength - 1;
      var mul = 1;
      var sub = 0;
      this[offset + i] = value & 255;

      while (--i >= 0 && (mul *= 256)) {
        if (value < 0 && sub === 0 && this[offset + i + 1] !== 0) {
          sub = 1;
        }

        this[offset + i] = (value / mul >> 0) - sub & 255;
      }

      return offset + byteLength;
    };

    Buffer.prototype.writeInt8 = function writeInt8(value, offset, noAssert) {
      value = +value;
      offset = offset >>> 0;
      if (!noAssert) checkInt(this, value, offset, 1, 127, -128);
      if (value < 0) value = 255 + value + 1;
      this[offset] = value & 255;
      return offset + 1;
    };

    Buffer.prototype.writeInt16LE = function writeInt16LE(value, offset, noAssert) {
      value = +value;
      offset = offset >>> 0;
      if (!noAssert) checkInt(this, value, offset, 2, 32767, -32768);
      this[offset] = value & 255;
      this[offset + 1] = value >>> 8;
      return offset + 2;
    };

    Buffer.prototype.writeInt16BE = function writeInt16BE(value, offset, noAssert) {
      value = +value;
      offset = offset >>> 0;
      if (!noAssert) checkInt(this, value, offset, 2, 32767, -32768);
      this[offset] = value >>> 8;
      this[offset + 1] = value & 255;
      return offset + 2;
    };

    Buffer.prototype.writeInt32LE = function writeInt32LE(value, offset, noAssert) {
      value = +value;
      offset = offset >>> 0;
      if (!noAssert) checkInt(this, value, offset, 4, 2147483647, -2147483648);
      this[offset] = value & 255;
      this[offset + 1] = value >>> 8;
      this[offset + 2] = value >>> 16;
      this[offset + 3] = value >>> 24;
      return offset + 4;
    };

    Buffer.prototype.writeInt32BE = function writeInt32BE(value, offset, noAssert) {
      value = +value;
      offset = offset >>> 0;
      if (!noAssert) checkInt(this, value, offset, 4, 2147483647, -2147483648);
      if (value < 0) value = 4294967295 + value + 1;
      this[offset] = value >>> 24;
      this[offset + 1] = value >>> 16;
      this[offset + 2] = value >>> 8;
      this[offset + 3] = value & 255;
      return offset + 4;
    };

    function checkIEEE754(buf, value, offset, ext, max, min) {
      if (offset + ext > buf.length) throw new RangeError("Index out of range");
      if (offset < 0) throw new RangeError("Index out of range");
    }

    function writeFloat(buf, value, offset, littleEndian, noAssert) {
      value = +value;
      offset = offset >>> 0;

      if (!noAssert) {
        checkIEEE754(buf, value, offset, 4, 34028234663852886e22, -34028234663852886e22);
      }

      ieee754.write(buf, value, offset, littleEndian, 23, 4);
      return offset + 4;
    }

    Buffer.prototype.writeFloatLE = function writeFloatLE(value, offset, noAssert) {
      return writeFloat(this, value, offset, true, noAssert);
    };

    Buffer.prototype.writeFloatBE = function writeFloatBE(value, offset, noAssert) {
      return writeFloat(this, value, offset, false, noAssert);
    };

    function writeDouble(buf, value, offset, littleEndian, noAssert) {
      value = +value;
      offset = offset >>> 0;

      if (!noAssert) {
        checkIEEE754(buf, value, offset, 8, 17976931348623157e292, -17976931348623157e292);
      }

      ieee754.write(buf, value, offset, littleEndian, 52, 8);
      return offset + 8;
    }

    Buffer.prototype.writeDoubleLE = function writeDoubleLE(value, offset, noAssert) {
      return writeDouble(this, value, offset, true, noAssert);
    };

    Buffer.prototype.writeDoubleBE = function writeDoubleBE(value, offset, noAssert) {
      return writeDouble(this, value, offset, false, noAssert);
    };

    Buffer.prototype.copy = function copy(target, targetStart, start, end) {
      if (!start) start = 0;
      if (!end && end !== 0) end = this.length;
      if (targetStart >= target.length) targetStart = target.length;
      if (!targetStart) targetStart = 0;
      if (end > 0 && end < start) end = start;
      if (end === start) return 0;
      if (target.length === 0 || this.length === 0) return 0;

      if (targetStart < 0) {
        throw new RangeError("targetStart out of bounds");
      }

      if (start < 0 || start >= this.length) throw new RangeError("sourceStart out of bounds");
      if (end < 0) throw new RangeError("sourceEnd out of bounds");
      if (end > this.length) end = this.length;

      if (target.length - targetStart < end - start) {
        end = target.length - targetStart + start;
      }

      var len = end - start;
      var i;

      if (this === target && start < targetStart && targetStart < end) {
        for (i = len - 1; i >= 0; --i) {
          target[i + targetStart] = this[i + start];
        }
      } else if (len < 1e3) {
        for (i = 0; i < len; ++i) {
          target[i + targetStart] = this[i + start];
        }
      } else {
        Uint8Array.prototype.set.call(target, this.subarray(start, start + len), targetStart);
      }

      return len;
    };

    Buffer.prototype.fill = function fill(val, start, end, encoding) {
      if (typeof val === "string") {
        if (typeof start === "string") {
          encoding = start;
          start = 0;
          end = this.length;
        } else if (typeof end === "string") {
          encoding = end;
          end = this.length;
        }

        if (val.length === 1) {
          var code = val.charCodeAt(0);

          if (code < 256) {
            val = code;
          }
        }

        if (encoding !== undefined && typeof encoding !== "string") {
          throw new TypeError("encoding must be a string");
        }

        if (typeof encoding === "string" && !Buffer.isEncoding(encoding)) {
          throw new TypeError("Unknown encoding: " + encoding);
        }
      } else if (typeof val === "number") {
        val = val & 255;
      }

      if (start < 0 || this.length < start || this.length < end) {
        throw new RangeError("Out of range index");
      }

      if (end <= start) {
        return this;
      }

      start = start >>> 0;
      end = end === undefined ? this.length : end >>> 0;
      if (!val) val = 0;
      var i;

      if (typeof val === "number") {
        for (i = start; i < end; ++i) {
          this[i] = val;
        }
      } else {
        var bytes = Buffer.isBuffer(val) ? val : new Buffer(val, encoding);
        var len = bytes.length;

        for (i = 0; i < end - start; ++i) {
          this[i + start] = bytes[i % len];
        }
      }

      return this;
    };

    var INVALID_BASE64_RE = /[^+/0-9A-Za-z-_]/g;

    function base64clean(str) {
      str = str.trim().replace(INVALID_BASE64_RE, "");
      if (str.length < 2) return "";

      while (str.length % 4 !== 0) {
        str = str + "=";
      }

      return str;
    }

    function toHex(n) {
      if (n < 16) return "0" + n.toString(16);
      return n.toString(16);
    }

    function utf8ToBytes(string, units) {
      units = units || Infinity;
      var codePoint;
      var length = string.length;
      var leadSurrogate = null;
      var bytes = [];

      for (var i = 0; i < length; ++i) {
        codePoint = string.charCodeAt(i);

        if (codePoint > 55295 && codePoint < 57344) {
          if (!leadSurrogate) {
            if (codePoint > 56319) {
              if ((units -= 3) > -1) bytes.push(239, 191, 189);
              continue;
            } else if (i + 1 === length) {
              if ((units -= 3) > -1) bytes.push(239, 191, 189);
              continue;
            }

            leadSurrogate = codePoint;
            continue;
          }

          if (codePoint < 56320) {
            if ((units -= 3) > -1) bytes.push(239, 191, 189);
            leadSurrogate = codePoint;
            continue;
          }

          codePoint = (leadSurrogate - 55296 << 10 | codePoint - 56320) + 65536;
        } else if (leadSurrogate) {
          if ((units -= 3) > -1) bytes.push(239, 191, 189);
        }

        leadSurrogate = null;

        if (codePoint < 128) {
          if ((units -= 1) < 0) break;
          bytes.push(codePoint);
        } else if (codePoint < 2048) {
          if ((units -= 2) < 0) break;
          bytes.push(codePoint >> 6 | 192, codePoint & 63 | 128);
        } else if (codePoint < 65536) {
          if ((units -= 3) < 0) break;
          bytes.push(codePoint >> 12 | 224, codePoint >> 6 & 63 | 128, codePoint & 63 | 128);
        } else if (codePoint < 1114112) {
          if ((units -= 4) < 0) break;
          bytes.push(codePoint >> 18 | 240, codePoint >> 12 & 63 | 128, codePoint >> 6 & 63 | 128, codePoint & 63 | 128);
        } else {
          throw new Error("Invalid code point");
        }
      }

      return bytes;
    }

    function asciiToBytes(str) {
      var byteArray = [];

      for (var i = 0; i < str.length; ++i) {
        byteArray.push(str.charCodeAt(i) & 255);
      }

      return byteArray;
    }

    function utf16leToBytes(str, units) {
      var c, hi, lo;
      var byteArray = [];

      for (var i = 0; i < str.length; ++i) {
        if ((units -= 2) < 0) break;
        c = str.charCodeAt(i);
        hi = c >> 8;
        lo = c % 256;
        byteArray.push(lo);
        byteArray.push(hi);
      }

      return byteArray;
    }

    function base64ToBytes(str) {
      return base64.toByteArray(base64clean(str));
    }

    function blitBuffer(src, dst, offset, length) {
      for (var i = 0; i < length; ++i) {
        if (i + offset >= dst.length || i >= src.length) break;
        dst[i + offset] = src[i];
      }

      return i;
    }

    function isArrayBuffer(obj) {
      return obj instanceof ArrayBuffer || obj != null && obj.constructor != null && obj.constructor.name === "ArrayBuffer" && typeof obj.byteLength === "number";
    }

    function isArrayBufferView(obj) {
      return typeof ArrayBuffer.isView === "function" && ArrayBuffer.isView(obj);
    }

    function numberIsNaN(obj) {
      return obj !== obj;
    }
  }, function (module, exports, __webpack_require__) {
    "use strict";

    exports.byteLength = byteLength;
    exports.toByteArray = toByteArray;
    exports.fromByteArray = fromByteArray;
    var lookup = [];
    var revLookup = [];
    var Arr = typeof Uint8Array !== "undefined" ? Uint8Array : Array;
    var code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    for (var i = 0, len = code.length; i < len; ++i) {
      lookup[i] = code[i];
      revLookup[code.charCodeAt(i)] = i;
    }

    revLookup["-".charCodeAt(0)] = 62;
    revLookup["_".charCodeAt(0)] = 63;

    function placeHoldersCount(b64) {
      var len = b64.length;

      if (len % 4 > 0) {
        throw new Error("Invalid string. Length must be a multiple of 4");
      }

      return b64[len - 2] === "=" ? 2 : b64[len - 1] === "=" ? 1 : 0;
    }

    function byteLength(b64) {
      return b64.length * 3 / 4 - placeHoldersCount(b64);
    }

    function toByteArray(b64) {
      var i, l, tmp, placeHolders, arr;
      var len = b64.length;
      placeHolders = placeHoldersCount(b64);
      arr = new Arr(len * 3 / 4 - placeHolders);
      l = placeHolders > 0 ? len - 4 : len;
      var L = 0;

      for (i = 0; i < l; i += 4) {
        tmp = revLookup[b64.charCodeAt(i)] << 18 | revLookup[b64.charCodeAt(i + 1)] << 12 | revLookup[b64.charCodeAt(i + 2)] << 6 | revLookup[b64.charCodeAt(i + 3)];
        arr[L++] = tmp >> 16 & 255;
        arr[L++] = tmp >> 8 & 255;
        arr[L++] = tmp & 255;
      }

      if (placeHolders === 2) {
        tmp = revLookup[b64.charCodeAt(i)] << 2 | revLookup[b64.charCodeAt(i + 1)] >> 4;
        arr[L++] = tmp & 255;
      } else if (placeHolders === 1) {
        tmp = revLookup[b64.charCodeAt(i)] << 10 | revLookup[b64.charCodeAt(i + 1)] << 4 | revLookup[b64.charCodeAt(i + 2)] >> 2;
        arr[L++] = tmp >> 8 & 255;
        arr[L++] = tmp & 255;
      }

      return arr;
    }

    function tripletToBase64(num) {
      return lookup[num >> 18 & 63] + lookup[num >> 12 & 63] + lookup[num >> 6 & 63] + lookup[num & 63];
    }

    function encodeChunk(uint8, start, end) {
      var tmp;
      var output = [];

      for (var i = start; i < end; i += 3) {
        tmp = (uint8[i] << 16) + (uint8[i + 1] << 8) + uint8[i + 2];
        output.push(tripletToBase64(tmp));
      }

      return output.join("");
    }

    function fromByteArray(uint8) {
      var tmp;
      var len = uint8.length;
      var extraBytes = len % 3;
      var output = "";
      var parts = [];
      var maxChunkLength = 16383;

      for (var i = 0, len2 = len - extraBytes; i < len2; i += maxChunkLength) {
        parts.push(encodeChunk(uint8, i, i + maxChunkLength > len2 ? len2 : i + maxChunkLength));
      }

      if (extraBytes === 1) {
        tmp = uint8[len - 1];
        output += lookup[tmp >> 2];
        output += lookup[tmp << 4 & 63];
        output += "==";
      } else if (extraBytes === 2) {
        tmp = (uint8[len - 2] << 8) + uint8[len - 1];
        output += lookup[tmp >> 10];
        output += lookup[tmp >> 4 & 63];
        output += lookup[tmp << 2 & 63];
        output += "=";
      }

      parts.push(output);
      return parts.join("");
    }
  }, function (module, exports) {
    exports.read = function (buffer, offset, isLE, mLen, nBytes) {
      var e, m;
      var eLen = nBytes * 8 - mLen - 1;
      var eMax = (1 << eLen) - 1;
      var eBias = eMax >> 1;
      var nBits = -7;
      var i = isLE ? nBytes - 1 : 0;
      var d = isLE ? -1 : 1;
      var s = buffer[offset + i];
      i += d;
      e = s & (1 << -nBits) - 1;
      s >>= -nBits;
      nBits += eLen;

      for (; nBits > 0; e = e * 256 + buffer[offset + i], i += d, nBits -= 8) {}

      m = e & (1 << -nBits) - 1;
      e >>= -nBits;
      nBits += mLen;

      for (; nBits > 0; m = m * 256 + buffer[offset + i], i += d, nBits -= 8) {}

      if (e === 0) {
        e = 1 - eBias;
      } else if (e === eMax) {
        return m ? NaN : (s ? -1 : 1) * Infinity;
      } else {
        m = m + Math.pow(2, mLen);
        e = e - eBias;
      }

      return (s ? -1 : 1) * m * Math.pow(2, e - mLen);
    };

    exports.write = function (buffer, value, offset, isLE, mLen, nBytes) {
      var e, m, c;
      var eLen = nBytes * 8 - mLen - 1;
      var eMax = (1 << eLen) - 1;
      var eBias = eMax >> 1;
      var rt = mLen === 23 ? Math.pow(2, -24) - Math.pow(2, -77) : 0;
      var i = isLE ? 0 : nBytes - 1;
      var d = isLE ? 1 : -1;
      var s = value < 0 || value === 0 && 1 / value < 0 ? 1 : 0;
      value = Math.abs(value);

      if (isNaN(value) || value === Infinity) {
        m = isNaN(value) ? 1 : 0;
        e = eMax;
      } else {
        e = Math.floor(Math.log(value) / Math.LN2);

        if (value * (c = Math.pow(2, -e)) < 1) {
          e--;
          c *= 2;
        }

        if (e + eBias >= 1) {
          value += rt / c;
        } else {
          value += rt * Math.pow(2, 1 - eBias);
        }

        if (value * c >= 2) {
          e++;
          c /= 2;
        }

        if (e + eBias >= eMax) {
          m = 0;
          e = eMax;
        } else if (e + eBias >= 1) {
          m = (value * c - 1) * Math.pow(2, mLen);
          e = e + eBias;
        } else {
          m = value * Math.pow(2, eBias - 1) * Math.pow(2, mLen);
          e = 0;
        }
      }

      for (; mLen >= 8; buffer[offset + i] = m & 255, i += d, m /= 256, mLen -= 8) {}

      e = e << mLen | m;
      eLen += mLen;

      for (; eLen > 0; buffer[offset + i] = e & 255, i += d, e /= 256, eLen -= 8) {}

      buffer[offset + i - d] |= s * 128;
    };
  }, function (module, exports) {
    module.exports = __WEBPACK_EXTERNAL_MODULE__6__;
  }, function (module, exports) {
    module.exports = __WEBPACK_EXTERNAL_MODULE__7__;
  }, function (module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);

    __webpack_require__.d(__webpack_exports__, "BinaryMessageFormat", function () {
      return BinaryMessageFormat;
    });

    var BinaryMessageFormat = function () {
      function BinaryMessageFormat() {}

      BinaryMessageFormat.write = function (output) {
        var size = output.byteLength || output.length;
        var lenBuffer = [];

        do {
          var sizePart = size & 127;
          size = size >> 7;

          if (size > 0) {
            sizePart |= 128;
          }

          lenBuffer.push(sizePart);
        } while (size > 0);

        size = output.byteLength || output.length;
        var buffer = new Uint8Array(lenBuffer.length + size);
        buffer.set(lenBuffer, 0);
        buffer.set(output, lenBuffer.length);
        return buffer.buffer;
      };

      BinaryMessageFormat.parse = function (input) {
        var result = [];
        var uint8Array = new Uint8Array(input);
        var maxLengthPrefixSize = 5;
        var numBitsToShift = [0, 7, 14, 21, 28];

        for (var offset = 0; offset < input.byteLength;) {
          var numBytes = 0;
          var size = 0;
          var byteRead = void 0;

          do {
            byteRead = uint8Array[offset + numBytes];
            size = size | (byteRead & 127) << numBitsToShift[numBytes];
            numBytes++;
          } while (numBytes < Math.min(maxLengthPrefixSize, input.byteLength - offset) && (byteRead & 128) !== 0);

          if ((byteRead & 128) !== 0 && numBytes < maxLengthPrefixSize) {
            throw new Error("Cannot read message size.");
          }

          if (numBytes === maxLengthPrefixSize && byteRead > 7) {
            throw new Error("Messages bigger than 2GB are not supported.");
          }

          if (uint8Array.byteLength >= offset + numBytes + size) {
            result.push(uint8Array.slice ? uint8Array.slice(offset + numBytes, offset + numBytes + size) : uint8Array.subarray(offset + numBytes, offset + numBytes + size));
          } else {
            throw new Error("Incomplete message.");
          }

          offset = offset + numBytes + size;
        }

        return result;
      };

      return BinaryMessageFormat;
    }();
  }, function (module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);

    __webpack_require__.d(__webpack_exports__, "isArrayBuffer", function () {
      return isArrayBuffer;
    });

    function isArrayBuffer(val) {
      return val && typeof ArrayBuffer !== "undefined" && (val instanceof ArrayBuffer || val.constructor && val.constructor.name === "ArrayBuffer");
    }
  }]);
});
