
(function () {
    var RSVD = 0, WNG = 0, SAR = 0, FSH = 0, TUG = 0, DREDGER = 0, DIVE = 0,
        MILITARY = 0, SAILING = 0, PLEASURE = 0, HSCRAFT = 0, PVESSEL = 0,
        PORTTENDER = 0, ANTIPOLL = 0, LAWENF = 0, LOCALVESSEL = 0, MEDICAL = 0, SPECIAL = 0,
        PASSENGER = 0, CARGO = 0, CARGOHAZARDA = 0, CARGOHAZARDB = 0, CARGOHAZARDC = 0,
        CARGOHAZARDD = 0, TANKER = 0, TANKERHAZARDA = 0, TANKERHAZARDB = 0, TANKERHAZARDC = 0,
        TANKERHAZARDD = 0, OTHER = 0, UNKNOWN = 0;
L.Control.ListMarkers = L.Control.extend({

	includes: L.version[0]==='1' ? L.Evented.prototype : L.Mixin.Events,

	options: {		
		layer: false,
		maxItems: 50000,
		collapsed: false,		
		label: 'title',
		itemIcon: L.Icon.Default.imagePath+'/marker.png',
		itemArrow: '&#10148;',	//visit: http://character-code.com/arrows-html-codes.php
		maxZoom: 20,
		position: 'bottomleft'
		//TODO autocollapse
	},

	initialize: function(options) {
		L.Util.setOptions(this, options);
		this._container = null;
		this._list = null;
        this._layer = this.options.layer || new L.LayerGroup();
	},

	onAdd: function (map) {

		this._map = map;
	
		var container = this._container = L.DomUtil.create('div', 'list-markers');

		this._list = L.DomUtil.create('ul', 'list-markers-ul', null);

		//this._initToggle();

		map.on('moveend', this._updateList, this);
			
		this._updateList();

		return container;
	},
	
	onRemove: function(map) {
		map.off('moveend', this._updateList, this);
		this._container = null;
		this._list = null;		
	},

	_createItem: function (layer) {
        var li = L.DomUtil.create('li', 'list-markers-li')
            a = L.DomUtil.create('a', 'selected-ship'),
			icon = this.options.itemIcon ? '<img src="'+this.options.itemIcon+'" />' : '',
			that = this;
           
        a.href = '#';       
		L.DomEvent
			.disableClickPropagation(a)
			.on(a, 'click', L.DomEvent.stop, this)
            .on(a, 'click', function (e) {             
                selectShip(this, layer.options.targetId);
				this._moveTo( layer.getLatLng() );
			}, this)
            .on(a, 'mouseover', function (e) {                   
				that.fire('item-mouseover', {layer: layer });
			}, this)
			.on(a, 'mouseout', function(e) {
				that.fire('item-mouseout', {layer: layer });
			}, this);			

			
		
		//console.log('_createItem',layer.options);

		if( layer.options.hasOwnProperty(this.options.label) )
        {
            //console.log(layer.options[this.options.label]);
            //a.innerHTML = icon + '<span>' + layer.options[this.options.label] + '</span>';
		    a.innerHTML = '<div class="container-fluid"><div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px;"><div class="col-lg-12">' +
                '<i class="fa fa-ship" aria-hidden="true" style="padding-right: 10px;color:mediumspringgreen;"></i><span class="">' + layer.options[this.options.label] + '</span></div></div></div>';
		    $('#listvessel').append(a);
            //$('#listvessel').append('<div>' + icon + '<span>' + layer.options[this.options.label] + '</span></div>');
            //TODO use related marker icon!
		    //TODO use template for item

            var x = layer.options.targetInfo.shipType;
            switch(true) {
                case ((x >= 10 && x <= 19) || (x >= 38 && x <= 39)):
                    RSVD += 1;
                    break;
                case (x >= 20 && x <= 28):
                    WNG += 1;
                    break;
                case (x == 29 || x == 51):
                    SAR += 1;
                    break;
                case (x == 30):
                    FSH += 1;
                    break;
                case ((x >= 31 && x <= 32) || x == 52):
                    TUG += 1;
                    break;
                case (x == 33):
                    DREDGER += 1;
                    break;
                case (x == 34):
                    DIVE += 1;
                    break;
                case (x == 35):
                    MILITARY += 1;
                    break;
                case (x == 36):
                    SAILING += 1;
                    break;
                case (x == 37):
                    PLEASURE += 1;
                    break;
                case (x >= 40 && x <= 49):
                    HSCRAFT += 1;
                    break;
                case (x == 50):
                    PVESSEL += 1;
                    break;
                case (x == 53):
                    PORTTENDER += 1;
                    break;
                case (x == 54):
                    ANTIPOLL += 1;
                    break;
                case (x == 55):
                    LAWENF += 1;
                    break;
                case (x == 56 || x == 57):
                    LOCALVESSEL += 1;
                    break;
                case (x == 58):
                    MEDICAL += 1;
                    break;
                case (x == 59):
                    SPECIAL += 1;
                    break;
                case (x >= 60 && x <= 69):
                    PASSENGER += 1;
                    break;
                case (x == 70 || (x >= 75 && x <= 79)):
                    CARGO += 1;
                    break;
                case (x == 71):
                    CARGOHAZARDA += 1;
                    break;
                case (x == 72):
                    CARGOHAZARDB += 1;
                    break;
                case (x == 73):
                    CARGOHAZARDC += 1;
                    break;
                case (x == 74):
                    CARGOHAZARDD += 1;
                    break;
                case (x == 80 || (x >= 85 && x <= 89)):
                    TANKER += 1;
                    break;
                case (x == 81):
                    TANKERHAZARDA += 1;
                    break;
                case (x == 82):
                    TANKERHAZARDB += 1;
                    break;
                case (x == 83):
                    TANKERHAZARDC += 1;
                    break;
                case (x == 84):
                    TANKERHAZARDD += 1;
                    break;
                case (x >= 90 && x <= 99):
                    OTHER += 1;
                    break;
                default:
                    UNKNOWN += 1;
                    break;
            }
		}
		else
            console.log("propertyName '" + this.options.label + "' not found in marker");
		return li;
	},

	_updateList: function() {
        $('#listvessel').html('');
        listOfMarkersInCurrentView = [];
	    $('#listvessel_total').html('');
	    RSVD = 0, WNG = 0, SAR = 0, FSH = 0, TUG = 0, DREDGER = 0, DIVE = 0,
        MILITARY = 0, SAILING = 0, PLEASURE = 0, HSCRAFT = 0, PVESSEL = 0,
        PORTTENDER = 0, ANTIPOLL = 0, LAWENF = 0, LOCALVESSEL = 0, MEDICAL = 0, SPECIAL = 0,
        PASSENGER = 0, CARGO = 0, CARGOHAZARDA = 0, CARGOHAZARDB = 0, CARGOHAZARDC = 0,
        CARGOHAZARDD = 0, TANKER = 0, TANKERHAZARDA = 0, TANKERHAZARDB = 0, TANKERHAZARDC = 0,
        TANKERHAZARDD = 0, OTHER = 0, UNKNOWN = 0;
		var that = this,
            n = 0,
            count = 0;
		this._list.innerHTML = '';
        this._layer.eachLayer(function (layer) {
            if (layer instanceof L.Marker)
                if (that._map.getBounds().contains(layer.getLatLng()))
                    if (++n < that.options.maxItems) {
                        count += 1;
                        listOfMarkersInCurrentView.push(layer);
                        //hideShowMarker(showMarkers, layer, isTooltipVisible);
                        that._list.appendChild(that._createItem(layer))
                    };
        });

        var b = '';

        if (RSVD != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Reserved: ' + RSVD + '</div></div>'; }
        if (WNG != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Wing In Grnd: ' + WNG + '</div></div>'; }
        if (SAR != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">SAR Aircraft: ' + SAR + '</div></div>'; }
        if (FSH != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Fishing	: ' + FSH + '</div></div>'; }
        if (TUG != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Tug: ' + TUG + '</div></div>'; }
        if (DREDGER != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Dredger: ' + DREDGER + '</div></div>'; }
        if (DIVE != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Dive Vessel: ' + DIVE + '</div></div>'; }
        if (MILITARY != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Military Ops: ' + MILITARY + '</div></div>'; }
        if (SAILING != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Sailing Vessel: ' + SAILING + '</div></div>'; }
        if (PLEASURE != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Pleasure Craft: ' + PLEASURE + '</div></div>'; }
        if (HSCRAFT != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">High-Speed Craft: ' + HSCRAFT + '</div></div>'; }
        if (PVESSEL != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Pilot Vessel: ' + PVESSEL + '</div></div>'; }
        if (PORTTENDER != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Port Tender: ' + PORTTENDER + '</div></div>'; }
        if (ANTIPOLL != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Anti-Pollution: ' + ANTIPOLL + '</div></div>'; }
        if (LAWENF != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Law Enforce: ' + LAWENF + '</div></div>'; }
        if (LOCALVESSEL != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Local Vessel: ' + LOCALVESSEL + '</div></div>'; }
        if (MEDICAL != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Medical Trans: ' + MEDICAL + '</div></div>'; }
        if (SPECIAL != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Special Craft: ' + SPECIAL + '</div></div>'; }
        if (PASSENGER != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Passenger: ' + PASSENGER + '</div></div>'; }
        if (CARGO != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Cargo: ' + CARGO + '</div></div>'; }
        if (CARGOHAZARDA != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Cargo - Hazard A (Major): ' + CARGOHAZARDA + '</div></div>'; }
        if (CARGOHAZARDB != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Cargo - Hazard B: ' + CARGOHAZARDB + '</div></div>'; }
        if (CARGOHAZARDC != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Cargo - Hazard C (Minor): ' + CARGOHAZARDC + '</div></div>'; }
        if (CARGOHAZARDD != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Cargo - Hazard D (Recognizable): ' + CARGOHAZARDD + '</div></div>'; }
        if (TANKER != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Tanker: ' + TANKER + '</div></div>'; }
        if (TANKERHAZARDA != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Tanker - Hazard A (Major): ' + TANKERHAZARDA + '</div></div>'; }
        if (TANKERHAZARDB != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Tanker - Hazard B: ' + TANKERHAZARDB + '</div></div>'; }
        if (TANKERHAZARDC != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Tanker - Hazard C (Minor): ' + TANKERHAZARDC + '</div></div>'; }
        if (TANKERHAZARDD != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Tanker - Hazard D (Recognizable): ' + TANKERHAZARDD + '</div></div>'; }
        if (OTHER != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Other: ' + OTHER + '</div></div>'; }
        if (UNKNOWN != 0) { b += '<div class="row" style="margin-bottom:2px;border-bottom: 1px solid rgba(189, 186, 186, 0.27);padding-bottom:3px;padding-top:3px; color: white; background-color: lightcoral; font-size: medium; margin-right: 0;"><div class="col-lg-12">Unknown: ' + UNKNOWN + '</div></div>'; }

        document.getElementById("listvessel_total").innerHTML += b;
        toggleVesselNamesOnVesselLimitByMarker(listOfMarkersInCurrentView);
	},

	_initToggle: function () {

		/* inspired by L.Control.Layers */

		var container = this._container;

		//Makes this work on IE10 Touch devices by stopping it from firing a mouseout event when the touch is released
		container.setAttribute('aria-haspopup', true);

		if (!L.Browser.touch) {
			L.DomEvent
				.disableClickPropagation(container);
				//.disableScrollPropagation(container);
		} else {
			L.DomEvent.on(container, 'click', L.DomEvent.stopPropagation);
		}

		if (this.options.collapsed)
		{
			this._collapse();

			if (!L.Browser.android) {
				L.DomEvent
					.on(container, 'mouseover', this._expand, this)
					.on(container, 'mouseout', this._collapse, this);
			}
			var link = this._button = L.DomUtil.create('a', 'list-markers-toggle', container);
			link.href = '#';
			link.title = 'List Markers';

			if (L.Browser.touch) {
				L.DomEvent
					.on(link, 'click', L.DomEvent.stop)
					.on(link, 'click', this._expand, this);
			}
			else {
				L.DomEvent.on(link, 'focus', this._expand, this);
			}

			this._map.on('click', this._collapse, this);
			// TODO keyboard accessibility
		}
	},

	_expand: function () {
		this._container.className = this._container.className.replace(' list-markers-collapsed', '');
	},

	_collapse: function () {
		L.DomUtil.addClass(this._container, 'list-markers-collapsed');
	},

    _moveTo: function(latlng) {
		if(this.options.maxZoom)
			this._map.setView(latlng, Math.min(17, this.options.maxZoom) );
		else
			this._map.panTo(latlng);    
    }
});

L.control.listMarkers = function (options) {
    return new L.Control.ListMarkers(options);
};

L.Map.addInitHook(function () {
    if (this.options.listMarkersControl) {
        this.listMarkersControl = L.control.listMarkers(this.options.listMarkersControl);
        this.addControl(this.listMarkersControl);
    }
});

}).call(this);
