﻿$(document).ready(function () {
    loadItemZone();

    $("#zoneModalId").click(function () {
        clearForm();
        loadItemZone();
    });

    function loadItemZone () {
        var value = $.ajax({
            url: applicationHostUrl + '/api/services/app/ZoneService/GetAll?userId=' + userId,
            async: false
        }).responseText;

        const select = document.querySelector('.zoneSelect');
        $('#zoneSelect').find('option').remove();
        select.insertAdjacentHTML('beforeend', `<option value="New">New</option>`)
        $.each(JSON.parse(value).result, function (index) {
            select.insertAdjacentHTML('beforeend', `<option value="${JSON.parse(value).result[index].id}">${JSON.parse(value).result[index].name}</option>`);
            if(JSON.parse(value).result[index].isEnable == true) {

             var value2 = $.ajax({
                url: applicationHostUrl + '/api/services/app/ZoneService/GetById?=' + JSON.parse(value).result[index].id,
                async: false
             }).responseText;

                if(JSON.parse(value).result[index].zoneType == 0) {
                    if(JSON.parse(value).result[index].isFill == true) {
                            L.circle([JSON.stringify(JSON.parse(value2).result.zoneItemDtos[0].latitude), JSON.stringify(JSON.parse(value2).result.zoneItemDtos[0].logitude)], {
                            color: JSON.parse(value2).result.colour, fillColor: JSON.parse(value2).result.colour,
                            fillOpacity: 0.2,
                            radius: JSON.parse(value2).result.radius
                        }).addTo(map);
                    }
                    else {
                        L.circle([JSON.stringify(JSON.parse(value2).result.zoneItemDtos[0].latitude), JSON.stringify(JSON.parse(value2).result.zoneItemDtos[0].logitude)], {
                            color: JSON.parse(value2).result.colour, fillColor: JSON.parse(value2).result.colour,
                            fillOpacity: 0,
                            radius: JSON.parse(value2).result.radius
                        }).addTo(map);
                    }
                }
                if(JSON.parse(value).result[index].zoneType == 1) {
                    var obj = JSON.parse(value2).result.zoneItemDtos;
                    var dataArray = [];
                    $.each(obj, function (index) {
                        dataArray.push([obj[index].latitude, obj[index].logitude]);
                    });

                    if(JSON.parse(value).result[index].isFill == true) {
                        L.polygon(dataArray, {
                            color: JSON.parse(value2).result.colour, fillColor: JSON.parse(value2).result.colour,
                            fillOpacity: 0.2
                        }).addTo(map);
                    }
                    else {
                        L.polygon(dataArray, {
                            color: JSON.parse(value2).result.colour, fillColor: JSON.parse(value2).result.colour,
                            fillOpacity: 0
                        }).addTo(map);
                    }
                }
                if(JSON.parse(value).result[index].zoneType == 2 || JSON.parse(value).result[index].zoneType == 3) {
                    var obj = JSON.parse(value2).result.zoneItemDtos;
                    var dataArray = [];
                    $.each(obj, function (index) {
                        dataArray.push([obj[index].latitude, obj[index].logitude]);
                    });

                    L.polyline(dataArray, {
                        color: JSON.parse(value2).result.colour
                    }).addTo(map);
                }
            }
        });
    }

    $('.colorZone').colorpicker();
    var divStyle = document.getElementById("colorPickerZone");
    $('#colorPickerZone').colorpicker({
        color: divStyle.backgroundColor
    }).on('changeColor', function(ev) {
        divStyle.style.backgroundColor = ev.color.toHex();
        divStyle.style.color = ev.color.toHex();
    });

    //create Tabulator on DOM element with id "example-table"
    $("#zone-table").tabulator({
        height: '100%', // set height of table, this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
        layout: "fitColumns", //fit columns to width of table (optional)
        responsiveLayout: true,
        addRowPos: "bottom",
        columns: [ //Define Table Columns
            { title: "Latitude", field: "latitude", editor: true, validator: ["required", "numeric"] },
            { title: "Longitude", field: "logitude", editor: true, validator: ["required", "numeric"] },
            { title: "Remove", width: 62, formatter: "buttonCross", align: 'center', cellClick: function (e, cell) { deleteRow(cell.getRow().getData().id); } }
        ],
        validationFailed: function (cell, value, validators) {
            //alert(cell + "\n" + value + "\n" + validators);
        },
        dataEdited: function (data) {
            //alert(JSON.stringify(data));
        },
    });

var tabledata = [
        {id:Math.random(), Latitude:"", Longitude:""}
    ];
    
    //load sample data into the table
    $("#zone-table").tabulator("setData", tabledata);

    $("#zoneSelect").change(function() {
        var selectedZone = this.options[this.selectedIndex].value;
        if(selectedZone == "New"){
            clearForm();
        }
        else {
                var value = $.ajax({
                url: applicationHostUrl + '/api/services/app/ZoneService/GetById?=' + selectedZone,
                async: false
            }).responseText;

                document.getElementById('typeName').value = JSON.parse(value).result.name;
                if(JSON.parse(value).result.zoneType == 0){
                    document.getElementById('radiusValue').value = JSON.parse(value).result.radius;
                }else {
                    $('.inputRadius').hide();    
            }

            var dataDto = [];
            $.each(JSON.parse(value).result.zoneItemDtos, function (index) {
                dataDto.push({
                    id: Math.random(),
                    latitude: JSON.parse(value).result.zoneItemDtos[index].latitude,
                    logitude: JSON.parse(value).result.zoneItemDtos[index].logitude
                });
            });

                document.getElementById('zoneType').value = JSON.parse(value).result.zoneType;
                var divStyle = document.getElementById("colorPickerZone");
                divStyle.value = JSON.parse(value).result.colour;
                divStyle.style.backgroundColor = JSON.parse(value).result.colour;
                divStyle.style.color = JSON.parse(value).result.colour;
                $("#zone-table").tabulator("setData", dataDto);
                $("#isenable").prop("checked", JSON.parse(value).result.isEnable);
                $("#isfill").prop("checked", JSON.parse(value).result.isFill);

                if (JSON.parse(value).result.zoneType == 2 || JSON.parse(value).result.zoneType == 3) {
                    document.getElementById("isfill").disabled = true;
                }
                else {
                    document.getElementById("isfill").disabled = false;
                }
        }
    });

    $("#zoneType").change(function() {
        var selectedZoneType = this.options[this.selectedIndex].value;
        if(selectedZoneType == 0) {
            $('.inputRadius').show();
        }
        else {
            $('.inputRadius').hide();
        }

        if(selectedZoneType == 2 || selectedZoneType == 3){
            document.getElementById("isfill").disabled = true;
            $("#isfill").prop("checked", false);
        }
        else {
            document.getElementById("isfill").disabled = false;
            $("#isfill").prop("checked", false);
        }      
    });
    
    //Add row on "Add Row" button click
    $("#add-zone-row").click(function () {
        var data = $("#zone-table").tabulator("getData");
        var e = document.getElementById("zoneType");
        var selectedZoneType = e.options[e.selectedIndex].value;
        if(selectedZoneType == 0 && data.length >= 1){
             swal("Warning!", "Zone type circle can only have 1 Latitude & Longitude!", "error");
        }
        else {
             $("#zone-table").tabulator("addRow", { id: Math.random() });
        }
    });

    //Delete row on "Delete Row" button click
    function deleteRow(row) {
        $("#zone-table").tabulator("deleteRow", row);
    }
    
    //Delete zone
    $("#delete-zone").click(function () {
        var e = document.getElementById("zoneSelect");
        var selectedZone = e.options[e.selectedIndex].value;
        if(selectedZone == "New") {
             swal("Error!", "Please select zone to delete!", "error").then(() => { $("#zoneModal").modal("show"); });
        }
        else {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: applicationHostUrl + '/api/services/app/ZoneService/Delete?id=' + selectedZone,
                            type: 'DELETE',
                            success: function (data) {
                                $("#zoneModal").modal("hide");
                                swal("Data successfully deleted!", {
                                    icon: "success",
                                }).then(() => { clearForm(); loadItemZone(); location.reload(); });
                            },
                            error: function (error) {
                                $("#zoneModal").modal("hide");
                                swal("Error!", "Cannot delete data!", "error");
                            }
                        });
                    }
                    else {
                       $("#zoneModal").modal("show");
                    }
                });
        }
    });

    //Save data on "Save" button click
    $("#save-zone").click(function () {
        var radius = 0;
        var fill = 0;
        var e = document.getElementById("zoneSelect");
        var selectedZone = e.options[e.selectedIndex].value;
        if(selectedZone == "New") {
            var typeName = document.getElementById("typeName").value;
            var isEnabled = $("#isenable").prop("checked");
            var e = document.getElementById("zoneType");
            var zoneType = e.options[e.selectedIndex].value;
            if(zoneType == 0) { 
                radius = document.getElementById("radiusValue").value; 
            }
            var color = document.getElementById("colorPickerZone").value;
            if(zoneType != 2 || zoneType != 3){
                fill = $("#isfill").prop("checked");
            }
            var data = $("#zone-table").tabulator("getData");
            var itemDto = [];


            for (var i = 0; i < data.length; i++) {
                if (typeof (data[i]) != "undefined") {
                    var groupId;
                    itemDto.push({
                        latitude: data[i].latitude,
                        logitude: data[i].logitude
                    });
                }
            };


            var objToSave = {
                "userId": userId,
                "zoneDto": {
                    "id": 0,
                    "name": typeName,
                    "isEnable": isEnabled,
                    "radius": radius,
                    "colour": color,
                    "isFill": fill,
                    "zoneType": zoneType,
                    "zoneItemDtos": itemDto
                }
            };

            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                'type': 'POST',
                'url': applicationHostUrl + '/api/services/app/ZoneService/Create',
                'data': JSON.stringify(objToSave),
                'dataType': 'json',
                success: function (data) {
                    swal("Good job!", "Data successfully saved!", "success").then(() => { clearForm(); loadItemZone(); $("#zoneModal").modal("show"); });;
                },
                error: function (error) {
                    swal("Error!", "Cannot save data!", "error");
                }
            })
        }
        else {
            // UPDATE
            var typeName = document.getElementById("typeName").value;
            var isEnabled = $("#isenable").prop("checked");
            var e = document.getElementById("zoneType");
            var zoneType = e.options[e.selectedIndex].value;
            if (zoneType == 0) {
                radius = document.getElementById("radiusValue").value;
            }
            var color = document.getElementById("colorPickerZone").value;
            if (zoneType != 2 || zoneType != 3) {
                fill = $("#isfill").prop("checked");
            }
            var data = $("#zone-table").tabulator("getData");
            var itemDto = [];


            for (var i = 0; i < data.length; i++) {
                if (typeof (data[i]) != "undefined") {
                    var groupId;
                    itemDto.push({
                        latitude: data[i].latitude,
                        logitude: data[i].logitude
                    });
                }
            };

            var objToSave = {
                "userId": userId,
                "zoneDto": {
                    "id": selectedZone,
                    "name": typeName,
                    "isEnable": isEnabled,
                    "radius": radius,
                    "colour": color,
                    "isFill": fill,
                    "zoneType": zoneType,
                    "zoneItemDtos": itemDto
                }
            };

            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                'type': 'PUT',
                'url': applicationHostUrl + '/api/services/app/ZoneService/Update',
                'data': JSON.stringify(objToSave),
                'dataType': 'json',
                success: function (data) {
                    swal("Good job!", "Data successfully saved!", "success").then(() => { clearForm(); loadItemZone(); location.reload(); });;
                },
                error: function (error) {
                    swal("Error!", "Cannot save data!", "error");
                }
            })
        }
    });

    function clearForm() {
        document.getElementById('zoneSelect').value = "New";
        var emptyTable = [{id:Math.random(), Latitude:"", Longitude:""}];
        document.getElementById('typeName').value = "";
        $('.inputRadius').show();
        document.getElementById('radiusValue').value = "";
        document.getElementById('zoneType').value = 0;
        divStyle.style.backgroundColor = "#fff";
        divStyle.style.color = "#fff";
        $("#zone-table").tabulator("setData", emptyTable);
        $("#isenable").prop("checked", true);
        $("#isfill").prop("checked", false);
    }

});