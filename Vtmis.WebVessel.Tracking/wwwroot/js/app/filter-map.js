﻿"use strict";

$(document).ready(function () {
    function get_filterGroupById(id) {
        var value = $.ajax({
            url: applicationHostUrl + '/api/services/app/FilterGroupService/GetById?' + $.param({ "id": id }),
            async: false
        }).responseText;
        return value;
    }

    function get_filterGroupIdByName(name) {
        var id = 0;
        var value = $.ajax({
            url: applicationHostUrl + '/api/services/app/FilterGroupService/GetAll',
            async: false
        }).responseText;

        $.each(JSON.parse(value).result, function (index) {
            if (name.localeCompare(JSON.parse(value).result[index].name) == 0) { id = JSON.parse(value).result[index].id; }
        });
        return id;
    }

    var filterGroupsData;
    loadFilterGroups(applicationHostUrl);
    function loadFilterGroups(url) {
        getAllFilterGroups(url).then(function (res) {
            var result = res.result;
            if (result.length > 0) {
                $.each(result, function (i, val) {
                    filterGroupsData += "<option value='" + val.name + "'>" + val.name + "</option>";
                });
            }
        });
    }

    //Create Group
    var groupEditor = function (cell, onRendered, success, cancel) {
        //cell - the cell component for the editable cell
        //onRendered - function to call when the editor has been rendered
        //success - function to call to pass the succesfully updated value to Tabulator
        //cancel - function to call to abort the edit and return to a normal cell
        var editor = $("<select id='groupFilter'>" + filterGroupsData + "</select>");
        //create and style editor
        //editor = $("<select><option value=''>" +
        //    "</option > <option value='[All Cyan]'>[All Cyan]</option>" +
        //    "<option value='[AtoN Only]'>[AtoN Only]</option>" +
        //    "<option value='[BaseStation Only]'>[BaseStation Only]</option>" +
        //    "<option value='[Color-Coded Types]'>[Color-Coded Types]</option>" +
        //    "<option value='[Filter All]'>[Filter All]</option>" +
        //    "<option value='[Fishing Vessel Only]'>[Fishing Vessel Only]</option>" +
        //    "<option value='[Passenger Vessel Only]'>[Passenger Vessel Only]</option>" +
        //    "</select>");
        editor.css({
            "padding": "3px",
            "width": "100%",
            "box-sizing": "border-box",
        });

        //Set value of editor to the current value of the cell
        editor.val(cell.getValue());

        //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
        onRendered(function () {
            editor.focus();
            editor.css("height", "100%");
        });

        //when the value has been set, trigger the cell to update
        editor.on("change blur", function (e) {
            success(editor.children("option").filter(":selected").text());
        });

        //return the editor element
        return editor;
    };

    //Ship Type
    var shipEditor = function (cell, onRendered, success, cancel) {
        //cell - the cell component for the editable cell
        //onRendered - function to call when the editor has been rendered
        //success - function to call to pass the succesfully updated value to Tabulator
        //cancel - function to call to abort the edit and return to a normal cell

        //create and style editor
        var editor = $("<select>" +
            "<option value='All'>All</option>" +
            "<option value='Anti Pollution'>Anti Pollution</option>" +
            "<option value='Cargo'>Cargo</option>" +
            "<option value='Diving'>Diving</option>" +
            "<option value='Dredg/Under'>Dredg/Under</option>" +
            "<option value='Fishing'>Fishing</option>" +
            "<option value='HSC'>HSC</option>" +
            "<option value='Law enforc'>Law enforc</option>" +
            "<option value='Local56'>Local56</option>" +
            "<option value='Local57'>Local57</option>" +
            "<option value='Medical'>Medical</option>" +
            "<option value='Military'>Military</option>" +
            "<option value='Other'>Other</option>" +
            "<option value='Passenger'>Passenger</option>" +
            "<option value='Pilot'>Pilot</option>" +
            "<option value='Pleasure Craft'>Pleasure Craft</option>" +
            "<option value='Port Tender'>Port Tender</option>" +
            "<option value='RRR#18'>RRR#18</option>" +
            "<option value='Sailing'>Sailing</option>" +
            "<option value='Search/Resc'>Search/Resc</option>" +
            "<option value='Tanker'>Tanker</option>" +
            "<option value='Towing'>Towing</option>" +
            "<option value='Towing 200/25'>Towing 200/25</option>" +
            "<option value='Tug'>Tug</option>" +
            "<option value='WIG'>WIG</option>" +
            "<option value='All DG,HS,MP'>All DG,HS,MP</option>" +
            "<option value='DG,HS,MP(A)'>DG,HS,MP(A)</option>" +
            "<option value='DG,HS,MP(B)'>DG,HS,MP(B)</option>" +
            "<option value='DG,HS,MP(C)'>DG,HS,MP(C)</option>" +
            "<option value='DG,HS,MP(D)'>DG,HS,MP(D)</option>" +
            "</select >");
        editor.css({
            "padding": "3px",
            "width": "100%",
            "box-sizing": "border-box",
        });

        //Set value of editor to the current value of the cell
        editor.val(cell.getValue());

        //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
        onRendered(function () {
            editor.focus();
            editor.css("height", "100%");
        });

        //when the value has been set, trigger the cell to update
        editor.on("change blur", function (e) {
            success(editor.val());
        });

        //return the editor element
        return editor;
    };

    //AIS Type
    var aisTypeEditor = function (cell, onRendered, success, cancel) {
        //cell - the cell component for the editable cell
        //onRendered - function to call when the editor has been rendered
        //success - function to call to pass the succesfully updated value to Tabulator
        //cancel - function to call to abort the edit and return to a normal cell

        //create and style editor
        var editor = $("<select>" +
            "<option value='All'>All</option>" +
            "<option value='Base St'>Base St</option>" +
            "<option value='Class A'>Class A</option>" +
            "<option value='Class B'>Class B</option>" +
            "<option value='AtoN'>AtoN</option>" +
            "<option value='SAR Air'>SAR Air</option>" +
            "<option value='SART'>SART</option>" +
            "</select >");
        editor.css({
            "padding": "3px",
            "width": "100%",
            "box-sizing": "border-box",
        });

        //Set value of editor to the current value of the cell
        editor.val(cell.getValue());

        //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
        onRendered(function () {
            editor.focus();
            editor.css("height", "100%");
        });

        //when the value has been set, trigger the cell to update
        editor.on("change blur", function (e) {
            success(editor.val());
        });

        //return the editor element
        return editor;
    };

    //Data Source
    var dataSourceEditor = function (cell, onRendered, success, cancel) {
        //cell - the cell component for the editable cell
        //onRendered - function to call when the editor has been rendered
        //success - function to call to pass the succesfully updated value to Tabulator
        //cancel - function to call to abort the edit and return to a normal cell

        //create and style editor
        var editor = $("<select>" +
            "<option value='All'>All</option>" +
            "<option value='Realtime'>Realtime</option>" +
            "<option value='Sat'>Sat</option>" +
            "<option value='LRIT'>LRIT</option>" +
            "<option value='Not Sat'>Not Sat</option>" +
            "<option value='Not LRIT'>Not LRIT</option>" +
            "</select >");
        editor.css({
            "padding": "3px",
            "width": "100%",
            "box-sizing": "border-box",
        });

        //Set value of editor to the current value of the cell
        editor.val(cell.getValue());

        //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
        onRendered(function () {
            editor.focus();
            editor.css("height", "100%");
        });

        //when the value has been set, trigger the cell to update
        editor.on("change blur", function (e) {
            success(editor.val());
        });

        //return the editor element
        return editor;
    };

    //Color
    var colorEditor = function (cell, onRendered, success, cancel) {
        //cell - the cell component for the editable cell
        //onRendered - function to call when the editor has been rendered
        //success - function to call to pass the succesfully updated value to Tabulator
        //cancel - function to call to abort the edit and return to a normal cell

        //create and style editor
        var editor = $("<select>" +
            "<option selected='selected' value='All'>Transparent</option>" +
            "<option value='Black'>Black</option>" +
            "<option value='Silver'>Silver</option>" +
            "<option value='White'>White</option>" +
            "<option value='Yellow'>Yellow</option>" +
            "<option value='Chocolate'>Chocolate</option>" +
            "<option value='Orange'>Orange</option>" +
            "<option value='Light Coral'>Light Coral</option>" +
            "<option value='Green'>Green</option>" +
            "<option value='Lime'>Lime</option>" +
            "<option value='Cyan'>Cyan</option>" +
            "<option value='Blue'>Blue</option>" +
            "<option value='Violet'>Violet</option>" +
            "<option value='Magenta'>Magenta</option>" +
            "<option value='Pink'>Pink</option>" +
            "</select >");
        editor.css({
            "padding": "3px",
            "width": "100%",
            "box-sizing": "border-box",
        });

        //Set value of editor to the current value of the cell
        editor.val(cell.getValue());

        //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
        onRendered(function () {
            editor.focus();
            editor.css("height", "100%");
        });

        //when the value has been set, trigger the cell to update
        editor.on("change blur", function (e) {
            success(editor.val());
        });

        //return the editor element
        return editor;
    };

    //Border Color
    var borderColorEditor = function (cell, onRendered, success, cancel) {
        //cell - the cell component for the editable cell
        //onRendered - function to call when the editor has been rendered
        //success - function to call to pass the succesfully updated value to Tabulator
        //cancel - function to call to abort the edit and return to a normal cell

        //create and style editor
        var editor = $("<select>" +
            "<option selected='selected' value='All'>Transparent</option>" +
            "<option value='Black'>Black</option>" +
            "<option value='Silver'>Silver</option>" +
            "<option value='White'>White</option>" +
            "<option value='Yellow'>Yellow</option>" +
            "<option value='Chocolate'>Chocolate</option>" +
            "<option value='Orange'>Orange</option>" +
            "<option value='Light Coral'>Light Coral</option>" +
            "<option value='Green'>Green</option>" +
            "<option value='Lime'>Lime</option>" +
            "<option value='Cyan'>Cyan</option>" +
            "<option value='Blue'>Blue</option>" +
            "<option value='Violet'>Violet</option>" +
            "<option value='Magenta'>Magenta</option>" +
            "<option value='Pink'>Pink</option>" +
            "</select >");
        editor.css({
            "padding": "3px",
            "width": "100%",
            "box-sizing": "border-box",
        });

        //Set value of editor to the current value of the cell
        editor.val(cell.getValue());

        //set focus on the select box when the editor is selected (timeout allows for editor to be added to DOM)
        onRendered(function () {
            editor.focus();
            editor.css("height", "100%");
        });

        //when the value has been set, trigger the cell to update
        editor.on("change blur", function (e) {
            success(editor.val());
        });

        //return the editor element
        return editor;
    };

    $("#modalFilter").iziModal({
        title: '',
        subtitle: '',
        headerColor: '#429cb7',
        background: null,
        theme: '',  // light
        icon: 'fa fa-filter',
        iconText: null,
        iconColor: '',
        rtl: false,
        width: '98%',
        top: '3%',
        bottom: '100px',
        borderBottom: true,
        padding: 10,
        radius: 3,
        zindex: 100000,
        iframe: false,
        iframeHeight: 400,
        iframeURL: null,
        focusInput: true,
        group: '',
        loop: false,
        arrowKeys: true,
        navigateCaption: true,
        navigateArrows: true, // Boolean, 'closeToModal', 'closeScreenEdge'
        history: false,
        restoreDefaultContent: true,
        autoOpen: 0, // Boolean, Number
        bodyOverflow: true,
        fullscreen: false,
        openFullscreen: false,
        closeOnEscape: true,
        closeButton: true,
        appendTo: false, // or false
        appendToOverlay: false, // or false
        overlay: true,
        overlayClose: false,
        overlayColor: 'rgba(0, 0, 0, 0.4)',
        timeout: false,
        timeoutProgressbar: false,
        pauseOnHover: false,
        timeoutProgressbarColor: 'rgba(255,255,255,0.5)',
        transitionIn: 'flipInX',
        transitionOut: 'flipOutX',
        transitionInOverlay: 'fadeIn',
        transitionOutOverlay: 'fadeOut',
        onFullscreen: function () { },
        onResize: function () { },
        onOpening: function () { },
        onOpened: function (modal, event) {
            modal.startLoading();

            var tabledata = [];
            var dataItems = [];

            document.getElementById('filterName').value = 'NewFilter';
            $("#selectFilter").change(function () {
                var selectedValue = $('#selectFilter option:selected').val();
                if (selectedValue == 'New') {
                    document.getElementById('filterName').value = 'NewFilter';
                }
                else {
                    document.getElementById('filterName').value = selectedValue;
                }

                var x = document.getElementById("download-csv");
                if (selectedValue == 'New') {
                    tabledata = [];
                    //load sample data into the table
                    $("#filter-table").tabulator("setData", tabledata);
                    x.style.display = "none";
                }
                else {
                    x.style.display = "inline-block";
                    var selectedId = $("#selectFilter option:selected").attr("id");

                    //load filter items by id
                    $.getJSON(applicationHostUrl + '/api/services/app/FilterService/GetById?' + $.param({ "id": selectedId }), function (data) {
                        var tabledata2 = [];

                        $.each(data.result.filterItemDtos, function (index) {
                            var filterGroupName = get_filterGroupById(data.result.filterItemDtos[index].groupTypeId);
                            tabledata2.push({
                                id: data.result.id,
                                enable: data.result.filterItemDtos[index].isEnable,
                                showShips: data.result.filterItemDtos[index].isShowShips,
                                group: JSON.parse(filterGroupName).result.name,
                                mmsi: data.result.filterItemDtos[index].mmsi,
                                name: data.result.filterItemDtos[index].shipName,
                                callSign: data.result.filterItemDtos[index].callSign,
                                shipType: data.result.filterItemDtos[index].shipType,
                                aisType: data.result.filterItemDtos[index].aisType,
                                minSpeed: data.result.filterItemDtos[index].minSpeed,
                                maxSpeed: data.result.filterItemDtos[index].maxSpeed,
                                minCourse: data.result.filterItemDtos[index].minCourse,
                                //maxCourse: data.result.personalizeFilterItems[index].minCourse,
                                minLength: data.result.filterItemDtos[index].minLength,
                                maxLength: data.result.filterItemDtos[index].maxLength,
                                dataSource: data.result.filterItemDtos[index].dataSource,
                                minAge: data.result.filterItemDtos[index].minAge,
                                maxAge: data.result.filterItemDtos[index].maxAge,
                                color: data.result.filterItemDtos[index].color,
                                borderColor: data.result.filterItemDtos[index].borderColor
                            });
                        });
                        //load sample data into the table
                        $("#filter-table").tabulator("setData", tabledata2);
                    });
                }
            });

            //create Tabulator on DOM element with id "filter-table"
            $("#filter-table").tabulator({
                height: '100%', // set height of table, this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
                layout: "fitColumns", //fit columns to width of table (optional)
                responsiveLayout: true,
                addRowPos: "bottom",
                columns: [ //Define Table Columns
                    { title: "Enable", width: 50, field: "enable", editor: true, formatter: "tickCross", align: 'center' },
                    { title: "Show Ships", width: 85, field: "showShips", editor: true, formatter: "tickCross", align: 'center' },
                    { title: "Group", field: "group", editor: groupEditor, validator: "required" },
                    { title: "MMSI", field: "mmsi", editor: true, validator: ["required", "numeric"] },
                    { title: "Name", field: "name", editor: true, validator: "required" },
                    { title: "Call Sign", field: "callSign", editor: true, validator: "required" },
                    { title: "Ship Type", field: "shipType", editor: shipEditor, validator: "required" },
                    { title: "AIS Type", field: "aisType", editor: aisTypeEditor, validator: "required" },
                    { title: "Min Speed", width: 83, field: "minSpeed", editor: true, validator: ["required", "numeric"] },
                    { title: "Max Speed", width: 83, field: "maxSpeed", editor: true, validator: ["required", "numeric"] },
                    { title: "Min Course", width: 83, field: "minCourse", editor: true, validator: ["required", "numeric"] },
                    //{ title: "Max Course", width: 83, field: "maxCourse", editor: true, validator: ["required", "numeric"] },
                    { title: "Min Length", width: 83, field: "minLength", editor: true, validator: ["required", "numeric"] },
                    { title: "Max Length", width: 83, field: "maxLength", editor: true, validator: ["required", "numeric"] },
                    { title: "Data Source", width: 83, field: "dataSource", editor: dataSourceEditor, validator: "required" },
                    { title: "Min Age", width: 62, field: "minAge", editor: true, validator: ["required", "numeric"] },
                    { title: "Max Age", width: 62, field: "maxAge", editor: true, validator: ["required", "numeric"] },
                    { title: "Color", field: "color", editor: colorEditor, validator: "required" },
                    { title: "Border Color", width: 90, field: "borderColor", editor: borderColorEditor, validator: "required" },
                    { title: "Remove", width: 62, formatter: "buttonCross", align: 'center', cellClick: function (e, cell) { deleteRow(cell.getRow().getData().id); } }
                ],
                validationFailed: function (cell, value, validators) {

                },
                dataEdited: function (data) {
                    //alert(JSON.stringify(data));

                }
            });

            //load sample data into the table
            $("#filter-table").tabulator("setData", tabledata);

            modal.stopLoading();

            //Add row on "Add Row" button click
            $("#add-row").click(function () {
                $("#filter-table").tabulator("addRow", { id: Math.random(), enable: true, showShips: true });
            });

            //Delete row on "Delete Row" button click
            function deleteRow(row) {
                $("#filter-table").tabulator("deleteRow", row);
            }

            //Delete Filter
            $("#delete-filter").click(function () {
                var selectId = $('#selectFilter option:selected').attr('id');
                if (selectId == "new") {
                    $('#modalFilter').iziModal('close');
                    swal("Error", "Please select filter to delete!", "error").then(() => { $('#modalFilter').iziModal('open'); });
                }
                else {
                    $('#modalFilter').iziModal('close');
                    swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this data!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                        .then((willDelete) => {
                            if (willDelete) {
                                $.ajax({
                                    url: applicationHostUrl + '/api/services/app/FilterService/Delete?' + $.param({ "id": selectId }),
                                    type: 'DELETE',
                                    success: function (data) {
                                        swal("Data successfully deleted!", {
                                            icon: "success",
                                        }).then(() => { $('#modalFilter').iziModal('open'); });
                                    },
                                    error: function (error) {
                                        $('#modalFilter').iziModal('close');
                                        swal("Error!", "Cannot delete data!", "error").then(() => { $('#modalFilter').iziModal('open'); });
                                        console.log("UserId: " + userId  + "\nURL: " + applicationHostUrl + "/api/services/app/FilterService/Delete?id=" + selectId + "\n\nError Message: " + error.message);
                                    }
                                });
                            }
                            else {
                                $('#modalFilter').iziModal('open');
                            }
                        });
                }
            });

            //load filter for selecting
            $.getJSON(applicationHostUrl + '/api/services/app/FilterService/GetAll?' + $.param({ "userId": userId }), function (data) {
                var div = document.getElementById('selectFilter');
                div.innerHTML += '<option id="new" selected>New</option>';

                $.each(data.result, function (index) {
                    dataItems.push(data.result[index].name);
                    div.innerHTML += '<option value="' + data.result[index].name + '" id="' + data.result[index].id + '">' + data.result[index].name + '</option>';
                });
            });

            //Save Filter
            $("#save-filter").click(function () {
                var newFilter = document.getElementById("filterName").value;
                var currentDateTime = new Date();

                var selectedValue = $('#selectFilter option:selected').val();
                if (selectedValue == 'New') {
                    // CREATE NEW
                    if (newFilter == "NewFilter" || newFilter == "") {
                        swal("Invalid Name", "Please change filter name!", "error");
                    }
                    else {
                        if (jQuery.inArray(newFilter, dataItems) != -1) {
                            swal("Invalid Name", "Filter name already exist!", "error");
                        }
                        else {
                            var data = $("#filter-table").tabulator("getData");
                            var itemDto = [];

                            for (var i = 0; i < data.length; i++) {
                                if (typeof (data[i]) != "undefined") {
                                    var groupId;
                                    if (typeof (data[i].group) == "string") { groupId = get_filterGroupIdByName(data[i].group); } else { groupId = data[i].group; }
                                    itemDto.push({
                                        "filterId": 0,
                                        "isEnable": data[i].enable,
                                        "isShowShips": data[i].showShips,
                                        "mmsi": data[i].mmsi,
                                        "shipName": data[i].name,
                                        "callSign": data[i].callSign,
                                        "groupTypeId": groupId,
                                        "shipType": data[i].shipType,
                                        "aisType": data[i].aisType,
                                        "minSpeed": data[i].minSpeed,
                                        "maxSpeed": data[i].maxSpeed,
                                        "minCourse": data[i].minCourse,
                                        "minLength": data[i].minLength,
                                        "maxLength": data[i].maxLength,
                                        "dataSource": data[i].dataSource,
                                        "minAge": data[i].minAge,
                                        "maxAge": data[i].maxAge,
                                        "color": data[i].color,
                                        "borderColor": data[i].borderColor
                                    });
                                }
                            };

                            var objToSave = {
                                "userId": userId,
                                "filterDto": {
                                    "id": 0,
                                    "name": newFilter,
                                    "filterItemDtos": itemDto
                                }
                            };

                            var emptyDataFlag = 0;
                            for (var key in objToSave.filterDto.filterItemDtos) {
                                var obj = objToSave.filterDto.filterItemDtos[key];
                                for (var item in obj) {
                                    if (obj[item] == undefined) { emptyDataFlag++; }
                                }
                            }

                            if (emptyDataFlag > 0) {
                                swal("Incomplete Data", "Please fill in all columns!", "error");
                            }
                            else {
                                $.ajax({
                                    headers: {
                                        'Accept': 'application/json',
                                        'Content-Type': 'application/json'
                                    },
                                    'type': 'POST',
                                    'url': applicationHostUrl + '/api/services/app/FilterService/Create',
                                    'data': JSON.stringify(objToSave),
                                    'dataType': 'json',
                                    success: function (data) {
                                        $('#modalFilter').iziModal('close');
                                        swal("Good job!", "Data successfully saved!", "success").then(() => { $('#modalFilter').iziModal('open'); });
                                    },
                                    error: function (error) {
                                        $('#modalFilter').iziModal('close');
                                        swal("Error!", "Cannot save data!", "error").then(() => { $('#modalFilter').iziModal('open'); });
                                        console.log("UserId: " + userId + "\nURL: " + applicationHostUrl + "/api/services/app/FilterService/Create\n\nData: " + JSON.stringify(objToSave) + "\n\nError Message: " + error.message);
                                    }
                                })
                            }
                        }
                    }
                }
                else {
                    // UPDATE DATA
                    if (newFilter == "NewFilter" || newFilter == "") {
                        swal("Invalid Name", "Please change filter name!", "error");
                    }
                    else {
                        var selectedId = $("#selectFilter option:selected").attr("id");
                        var data = $("#filter-table").tabulator("getData");
                        var itemDto = [];

                        for (var i = 0; i <= data.length; i++) {
                            if (typeof (data[i]) != "undefined") {
                                var groupId;
                                if (typeof (data[i].group) == "string") { groupId = get_filterGroupIdByName(data[i].group); } else { groupId = data[i].group; }
                                itemDto.push({
                                    "filterId": selectedId,
                                    "isEnable": data[i].enable,
                                    "isShowShips": data[i].showShips,
                                    "mmsi": data[i].mmsi,
                                    "shipName": data[i].name,
                                    "callSign": data[i].callSign,
                                    "groupTypeId": groupId,
                                    "shipType": data[i].shipType,
                                    "aisType": data[i].aisType,
                                    "minSpeed": data[i].minSpeed,
                                    "maxSpeed": data[i].maxSpeed,
                                    "minCourse": data[i].minCourse,
                                    "minLength": data[i].minLength,
                                    "maxLength": data[i].maxLength,
                                    "dataSource": data[i].dataSource,
                                    "minAge": data[i].minAge,
                                    "maxAge": data[i].maxAge,
                                    "color": data[i].color,
                                    "borderColor": data[i].borderColor
                                });
                            }   
                        };

                        var objToSave = {
                            "userId": userId,
                            "filterDto": {
                                "id": selectedId,
                                "name": newFilter,
                                "filterItemDtos": itemDto
                            }
                        };

                        var emptyDataFlagUpdate = 0;
                        for (var key in objToSave.filterDto.filterItemDtos) {
                            var obj = objToSave.filterDto.filterItemDtos[key];
                            for (var item in obj) {
                                if (obj[item] == undefined) { emptyDataFlagUpdate++; }
                            }
                        }

                        if (emptyDataFlagUpdate > 0) {
                            swal("Incomplete Data", "Please fill in all columns!", "error");
                        }
                        else {
                            $.ajax({
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                },
                                'type': 'PUT',
                                'url': applicationHostUrl + '/api/services/app/FilterService/Update',
                                'data': JSON.stringify(objToSave),
                                'dataType': 'json',
                                success: function (data) {
                                    $('#modalFilter').iziModal('close');
                                    swal("Good job!", "Data successfully updated!", "success").then(() => { $('#modalFilter').iziModal('open'); });
                                },
                                error: function (error) {
                                    $('#modalFilter').iziModal('close');
                                    swal("Error!", "Cannot update data!", "error").then(() => { $('#modalFilter').iziModal('open'); });
                                    console.log("UserId: " + userId + "\nURL: " + applicationHostUrl + "/api/services/app/FilterService/Update\n\nData: " + JSON.stringify(objToSave) + "\n\nError Message: " + error.message);
                                }
                            })
                        }
                    }
                }
            });

            //trigger download of data.csv file
            $("#download-csv").click(function () {
                $("#filter-table").tabulator("download", "csv", "Filter.csv");
            });
        },
        onClosing: function () { },
        onClosed: function () { },
        afterRender: function () { }
    });


});