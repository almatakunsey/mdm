﻿$(document).ready(function () {
    //create Tabulator on DOM element with id "example-table"
    $("#alarm-table").tabulator({
        height: '100%', // set height of table, this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
        layout: "fitColumns", //fit columns to width of table (optional)
        responsiveLayout: true,
        addRowPos: "bottom",
        columns: [ //Define Table Columns
            { title: "AisMessage", field: "AisMessage", editor: true, validator: "required" },
            { title: "Color", field: "Color", editor: true, validator: ["required"] },
            { title: "Message", field: "Message", editor: true, validator: ["required"] },
            { title: "Remove", width: 62, formatter: "buttonCross", align: 'center', cellClick: function (e, cell) { deleteRow(cell.getRow().getData().id); } }
        ],
        validationFailed: function (cell, value, validators) {
            //alert(cell + "\n" + value + "\n" + validators);
        },
        dataEdited: function (data) {
            //alert(JSON.stringify(data));
        },
    });

    $("#alarmModalId").click(function () { loadAlarmData(); });

    function loadAlarmData() {
        //load list alarm data
        var tabledata = [];

        /*oldData = [];
        $.getJSON(applicationHostUrl + '/api/services/app/Location/GetListAsync', function (data) {
            $.each(data.result.items, function (index) {
                tabledata.push({
                    id: data.result.items[index].id,
                    Name: data.result.items[index].name,
                    Longitude: data.result.items[index].longitude,
                    Latitude: data.result.items[index].latitude
                });

                oldData.push({
                    id: data.result.items[index].id,
                    Name: data.result.items[index].name,
                    Longitude: data.result.items[index].longitude,
                    Latitude: data.result.items[index].latitude
                })
            });*/

            //load sample data into the table
            $("#alarm-table").tabulator("setData", tabledata);
        //});
    }

    //Add row on "Add Row" button click
    $("#add-alarm-row").click(function () {
        $("#alarm-table").tabulator("addRow", { id: Math.random() });
    });

    //Delete row on "Delete Row" button click
    function deleteRow(row) {
        $("#alarm-table").tabulator("deleteRow", row);
    }

    //Save data on "Save" button click
    $("#save-alarm").click(function () {
        swal("Save",{
          buttons: false
        });
    });


    //trigger download of data.csv file
    $("#download-alarm-csv").click(function () {
        $("#alarm-table").tabulator("download", "csv", "Alarm.csv");
    });
});