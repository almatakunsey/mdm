﻿"use strict";
//1
/**************************************
 * Global Variables
**************************************** */
//var markerClusterGroup = L.markerClusterGroup();
var run = true;
var canvasRenderer = L.canvas();
var listOfMarkersInCurrentView = [];
var showTargetsName = true;

var showMarkers = true;
var toggleShowTargetNames = true;
var selectedTargetId = 0;
var isViewFilter = false;
var markers = {};
//var transport = signalR.TransportType.WebSockets;
//let connection = new signalR.HubConnection('/vesselgeolocationhub');
var connection = new signalR.HubConnectionBuilder()
    .withUrl("/vesselgeolocationhub")
    .configureLogging(signalR.LogLevel.Debug)
    .build();
//var startTime = new Date(demoTracks[0].properties.time[0]);
var targetData = [];
//var clusterCookieName = "isCluster";
var markersLayer = new L.LayerGroup({
    renderer: canvasRenderer
});
var routeLayer = new L.LayerGroup({
    renderer: canvasRenderer
});
//var tooltipLayer = new L.LayerGroup();
var markersRoute = {};


var iconRed = L.icon({
    iconUrl: '../../images/red.png'
});

var iconSmallRed = L.icon({
    iconUrl: '../../images/small_red.png'
});

var iconGreen = L.icon({
    iconUrl: '../../images/green.png'
});

var iconSmallGreen = L.icon({
    iconUrl: '../..images/small_green.png'
});

var iconYellow = L.icon({
    iconUrl: '../../images/yellow.png'
});

var iconSmallYellow = L.icon({
    iconUrl: '../../images/small_yellow.png'
});

/**************************************
 * Boat Icons
**************************************** */



//var boatIcon = new L.Icon({
//    iconUrl: '../../images/marker.png',
//    shadowUrl: null,
//    iconSize: new L.Point(19, 26),
//    iconAnchor: new L.Point(9, 13)
//});

//var boatIconRed = new L.Icon({
//    iconUrl: '../../images/red.png',
//    shadowUrl: null,
//    iconSize: new L.Point(19, 26),
//    iconAnchor: new L.Point(9, 13)
//});

//var boats = [boatIcon, boatIconRed]

/****************************************
 * Init Map & Context Menu Item
*****************************************/
var map = L.map('map', {
    renderer: canvasRenderer,
    contextmenu: true,
    contextmenuWidth: 140,
    contextmenuItems: [{
        text: 'Show coordinates',
        callback: showCoordinates
    }, {
        text: 'Center map here',
        callback: centerMap
    }, '-', {
        text: 'Zoom in',
        //icon: 'images/zoom-in.png',
        callback: zoomIn
    }, {
        text: 'Zoom out',
        //icon: 'images/zoom-out.png',
        callback: zoomOut
    }]
}).setView([2.87995107, 101.14700317], 10);





//var ciLayer = L.canvasIconLayer({}).addTo(map);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    maxZoom: 18,
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
    preferCanvas: true
}).addTo(map);

var openmaptilesUrl = "https://free-{s}.tilehosting.com/data/v3/{z}/{x}/{y}.pbf.pict?key={key}";
var openmaptilesAttribution = '<a href="https://openmaptiles.org/">&copy; OpenMapTiles</a>, <a href="http://www.openstreetmap.org/copyright">&copy; OpenStreetMap</a> contributors';
var openmaptilesKey = 'UmmATPUongHdDmIicgs7';
var vectorTileOptions = {
    rendererFactory: L.canvas.tile,
    attribution: openmaptilesAttribution,
    key: openmaptilesKey,
    subdomains: '0123',	// 01234 for openmaptiles, abcd for mapbox
    vectorTileLayerStyles: {
        // 				'poi_label': { icon: new L.Icon.Default() },
        //'poi': { icon: new L.Icon.Default() },
        housenumber: [],
        park: [],
        place: [],
        water: [],
        waterway: [],
        boundary: [],
        country_label: [],
        marine_label: [],
        state_label: [],
        place_label: [],
        waterway_label: [],
        water_name: [],
        landcover: [],
        landuse: [],
        landuse_overlay: [],
        road: [],
        transportation: [],
        aeroway: [],
        tunnel: [],
        bridge: [],
        barrier_line: [],
        building: [],
        road_label: [],
        road_name: [],
        transportation_name: [],
        housenum_label: [],
        mountain_peak: [],
    },
    interactive: true,	// Make sure that this VectorGrid fires mouse/pointer events
    getFeatureId: function (f) {
        return f.properties.osm_id;
    }
};
//L.tileLayer('http://tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

var pbfLayer = L.vectorGrid.protobuf(openmaptilesUrl, vectorTileOptions)
    .on('click', function (e) {	// The .on method attaches an event handler
        L.popup()
            .setContent(e.layer.properties.name || e.layer.properties.type)
            .setLatLng(e.latlng)
            .openOn(map);
        L.DomEvent.stop(e);
    })
    .addTo(map);
map.addLayer(markersLayer);
map.addLayer(routeLayer);

/****************************************
 * Map Event
*****************************************/
//map.on('moveend', placeMarkersInBounds);
//map.on('zoomend', placeMarkersInBounds);

//placeMarkersInBounds();

function placeMarkersInBounds() {
    if (run === true) {
        var markerArray = [];
        //console.log("placeMarkersInBounds");

        var mapBounds = map.getBounds();

        var length = Object.keys(markers).length;
        var keys = Object.keys(markers);

        var zoomLev = map.getZoom();
        console.log(zoomLev);

        markersLayer.clearLayers();

        for (var i = 0; i <= (length + 1); i++) {

            var m = markers[keys[i]];

            if (m !== null) {
                if (zoomLev <= 7) {
                    if (m.options.targetInfo.shipType <= 30)
                        m.setIcon(iconSmallGreen)
                    else if (m.options.targetInfo.shipType > 30 && m.options.targetInfo.shipType<=60)
                        m.setIcon(iconSmallYellow);
                    else
                        m.setIcon(iconSmallRed);
                }
                else {
                    if (m.options.targetInfo.shipType <= 30)
                        m.setIcon(iconGreen)
                    else if (m.options.targetInfo.shipType > 30 && m.options.targetInfo.shipType <= 60)
                        m.setIcon(iconYellow);
                    else
                        m.setIcon(iconSmallRed);
                }


                if (m.options.targetInfo.messageId === 18) {
                    var shipIcon = getTargetIcon((m.options.targetInfo.messageId));
                    m.setIcon(shipIcon);
                }

                var shouldBeVisible = mapBounds.contains(new L.latLng(m._latlng.lat, m._latlng.lng));
                if (!m._icon && shouldBeVisible) {
                    markerArray.push(m);
                    markersLayer.addLayer(m);
                }

            }
        }
    }

    //markersLayer = new L.LayerGroup(markerArray, { renderer: canvasRenderer }).addTo(map);
       
}

/****************************************
 * Context Menu Functions
*****************************************/
function replay() {
    alert('Replay')
}

function showCoordinates(e) {
    alert(e.latlng);
}

function centerMap(e) {
    map.panTo(e.latlng);
}

function zoomIn(e) {
    map.zoomIn();
}

function zoomOut(e) {
    map.zoomOut();
}

var getTargetInfo = function (e) {
    slideMenu.openMenu(e.relatedTarget.options.targetInfo);
}

