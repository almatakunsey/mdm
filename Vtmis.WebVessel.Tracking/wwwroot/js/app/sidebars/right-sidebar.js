﻿"use strict";
//2
/**************************************
 * Init Right Sidebar
**************************************** */

//Right SideBar (Refactor move to function)
var sidebar = L.control.sidebar('sidebar').addTo(map);

var right = '<div class="slider-header""><h3>Information Panel</h3></div>';
var menu = '<div class="tabs tabs-style-fillup"><nav><ul>' +
    '<li><a href="#section-circlefill-1" class="fa fa-info" data-toggle="tooltip" title="Details"></a></li>' +
    '<li><a href="#section-circlefill-2" class="fa fa-history" data-toggle="tooltip" title="History"></a></li>' +
    '<li><a href="#section-circlefill-3" class="fa fa-calendar" data-toggle="tooltip" title="Events"></a></li></ul></nav>' +
    '<div class="content-wrap">' +
    '<section id= "section-circlefill-1"><p>Details</p><div id="detailSection"></div></section>' +
    '<section id="section-circlefill-2"><p>History</p></section>' +
    '<section id="section-circlefill-3"><p>Events</p></section></div></div>';


var slideMenu = L.control.slideMenu('', { position: 'topright', menuposition: 'topright', width: '350px', height: '500px', delay: '30' }).addTo(map);
slideMenu.setContents(right + menu);

(function () {
    [].slice.call(document.querySelectorAll('.tabs')).forEach(function (el) {
        new CBPFWTabs(el);
    });

})();