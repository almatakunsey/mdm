﻿"use strict";
//4
var list = new L.Control.ListMarkers({ layer: markersLayer, itemIcon: null });

list.on('item-mouseover', function (e) {
    //e.layer.setIcon(L.icon({
    //    iconUrl: boatIcon.options.iconUrl.options.iconUrl
    //}))
}).on('item-mouseout', function (e) {
    //e.layer.setIcon(L.icon({
    //    iconUrl: boatIcon.options.iconUrl
    //}))
});
map.addControl(list);

/**************************************
 * Bind Markers
**************************************** */
var markerArray = [];

function bindMarker(obj) {
    var shipIcon = iconRed;
    if (!markers.hasOwnProperty(obj.vesselId)) {
        pushTargetInfo(obj);
        // instantiate canvas icon
        //console.log(obj);
        //var shipIcon = getTargetIcon(obj.messageId);
        // set icon colour
        //shipIcon.options.__proto__.fillStyle = "orange";
        var zoomLev = map.getZoom();

        if (zoomLev <= 7) {
            if (obj.shipType <= 30)
                shipIcon = iconSmallGreen;
            else if (obj.shipType > 30 && obj.shipType <= 60)
                shipIcon = iconSmallYellow;
            else
                shipIcon = iconSmallRed;
        }
        else {
            if (obj.shipType <= 30)
                shipIcon = iconGreen;
            else if (obj.shipType > 30 && obj.shipType <= 60)
                shipIcon = iconYellow;
            else
                shipIcon = iconRed;
        }

        if (obj.messageId === 18)
            shipIcon = getTargetIcon(obj.messageId);

        markers[obj.vesselId] = new L.marker([obj.lat, obj.lgt], {
            renderer: canvasRenderer,
            title: obj.vesselName,
            targetId: obj.vesselId, targetInfo: obj, icon: shipIcon,
            contextmenu: true,
            contextmenuItems: [{
                text: obj.vesselName,
                index: 0
            },
            { separator: true, index: 1 },
            {
                text: 'Details', callback: getTargetInfo, index: 2
            },
            { text: 'Replay', callback: replay, index: 3 },
            {
                separator: true,
                index: 4
            }]
        });

        //#1
        //markers[obj.vesselId].addTo(markersLayer);

        markers[obj.vesselId].setIconAngle(obj.cog);

        markers[obj.vesselId].previousLatLngs = [];
        //markers[obj.vesselId].bindTooltip(obj.vesselName, { renderer: canvasRenderer,permanent: true, direction: "top", className: "marker-label" });
        //hideShowMarker(showMarkers, markers[obj.vesselId], showTargetsName);
        //hideShowMarkerTooltip(showMarkers, markers[obj.vesselId], toggleShowTargetNames);
    }
    else {
        markers[obj.vesselId].previousLatLngs.push();
        var plat = markers[obj.vesselId]._latlng.lat;
        var plng = markers[obj.vesselId]._latlng.lng;

        //var bearing = L.GeometryUtil.bearing(new L.latLng(plat, plng), new L.latLng(obj.lat, obj.lgt));

        markers[obj.vesselId].setLatLng([obj.lat, obj.lgt]);

        markers[obj.vesselId].setIconAngle(obj.cog);
    }

    //
    var mapBounds = map.getBounds();

    if (mapBounds.contains(markers[obj.vesselId].getLatLng())) {
        //markerArray.push(markers[obj.vesselId]);
        if (run === true) {
            //markerArray.push(markers[obj.vesselId]);
            markersLayer.addLayer(markers[obj.vesselId]);
        }
    }
    

    markers[obj.vesselId].on('click', function (e) {
       
        $.when($.each(markers, function (i, obj) {           
            setCanvasMarkerColour(obj);
        })).then(function () {
            setCanvasMarkerColour(markers[obj.vesselId], "red");
        });
        slideMenu.openMenu(markers[obj.vesselId].options.targetInfo);
    });
}

function setCanvasMarkerColour(marker, colour = "orange") {
    var shipIcon = marker.options.icon;
    shipIcon.options.__proto__.fillStyle = colour;
    marker.setIcon(shipIcon);
}

//
function setMarkers(obj) {

    var i = 0;

    $.when(jQuery.each(obj.vesselPositions, function (i, obj) {
        
            markerArray = [];

            bindMarker(obj);
        
    })).then(function () {
        //ciLayer.addLayers(markerArray);
        //console.log("MarkerArray " + markerArray);
       
        //markersLayer = new L.LayerGroup(markerArray, { renderer: canvasRenderer }).addTo(map);
        //console.log(markersLayer);
        //markersLayer.eachLayer(function (layer) {
        //    currentMarkers.push(layer);
        //});
        toggleVesselNamesOnVesselLimitByMarker(markerArray);
    });
}

/**************************************
 * Focus on a marker
**************************************** */
function centerLeafletMapOnMarker(markerId) {
    var latLngs = [markers[markerId].getLatLng()];
    var markerBounds = L.latLngBounds(latLngs);
    map.fitBounds(markerBounds);
    $('#modalAdvancedSearch').iziModal('close');
}

/**************************************
 * Show Selected Marker
**************************************** */

var selectShip = function (el, vesselId) {
    selectedTargetId = vesselId;
    $("#listvessel>a>span.selected-ship-active").removeClass("selected-ship-active");
    if (vesselId === 0) {
        showMarkers = true;
    } else {
        showMarkers = false;
        $(el).children().addClass("selected-ship-active");
    }

    if (showMarkers === false) {
        $.each(markers, function (i, marker) {
            //hideShowMarkerTooltip(false, marker);
            hideShowMarker(false, marker);
        });
        hideShowMarker(true, markers[vesselId], true);
        setCanvasMarkerColour(markers[vesselId], "red");
     
    } else {
        var tempLayerCount = 0;
        $.when($.each(markers, function (i, marker) {
            tempLayerCount = i;
            hideShowMarker(true, marker, false);            
            //hideShowMarkerTooltip(true, marker);
        })).then(function () {
            toggleVesselNamesOnVesselLimit(tempLayerCount);
            });  
    }
}

/**************************************
 * Hide / Show Marker and Tooltip
**************************************** */
function hideShowMarkerTooltip(markerVisibility = true, target = null, tooltipVisibility = true, targetIcon = boatIcon) {
    //console.log(markerClusterGroup);
    if (target !== null && target._icon !== undefined && target._icon !== null) {
        if (markerVisibility) {
            target._icon.style.display = 'inherit';
            target.setIcon(targetIcon);
        } else {
            target._icon.style.display = 'none';
            target.setIcon(targetIcon);
            tooltipVisibility = false;
        }
    }
    tooltipVisibility === true ? target.openTooltip() : target.closeTooltip();
}

function hideShowMarker(targetVisibility = true, target = null, tooltipVisibility = true) {

    if (target !== null) {
        if (targetVisibility) {
            target.addTo(markersLayer);
            //setCanvasMarkerColour(target);
        } else {
            target.remove();
        }
    }
    //target.closeTooltip();
    tooltipVisibility === true ? target.openTooltip() : target.closeTooltip();
}

/**************************************
 * Toggle VesselName visibility
**************************************** */

var toggleVesselNames = function () {
    if (showTargetsName) {
        showTargetsName = false;
    } else {
        showTargetsName = true;
    }
    if (selectedTargetId !== 0) {
        //hideShowMarkerTooltip(true, markers[selectedTargetId], toggleShowTargetNames, boatIconRed);
        hideShowMarker(true, markers[selectedTargetId], showTargetsName);
    } else {
        $.each(listOfMarkersInCurrentView, function (i, val) {
            if (val.isTooltipOpen()) {
                //hideShowMarkerTooltip(true, val, toggleShowTargetNames);
                hideShowMarker(true, val, showTargetsName);
            } else {
                hideShowMarker(true, val, showTargetsName);
                //hideShowMarkerTooltip(true, val, toggleShowTargetNames);
            }
        })
    }
}

function toggleVesselNamesOnVesselLimitByMarker(mrkrs) {
var showTooltip = false;
    if (mrkrs.length > 0) {
              if (mrkrs.length > 20) {
                showTooltip = false;
            }
            else {
                showTooltip = showTargetsName;
            }

            $.each(mrkrs, function (i, val) {
                if (showMarkers) {
                    hideShowMarker(true, val, showTooltip);
                }            
        });
    }   
}

function toggleVesselNamesOnVesselLimit(vesselCount) {
    if (vesselCount > 20) {
        showTargetsName = false;
    }
    else {
        showTargetsName = true;
    }

    $.each(markers, function (i, val) {
        if (showMarkers) {
            hideShowMarker(true, val, showTargetsName);
        }

        //hideShowMarkerTooltip(true, val, toggleShowTargetNames);
    })
}


function toggleName(isTooltipVisible) {
    showTargetsName = isTooltipVisible;
    if (selectedTargetId !== 0) {
        //hideShowMarkerTooltip(true, markers[selectedTargetId], isTooltipVisible, boatIconRed);
        hideShowMarker(true, markers[selectedTargetId], showTargetsName);
    } else {
        $.each(listOfMarkersInCurrentView, function (i, val) {

            if (val.isTooltipOpen()) {
                hideShowMarker(true, val, showTargetsName);
                //hideShowMarkerTooltip(true, val, isTooltipVisible);
            } else {
                hideShowMarker(true, val, showTargetsName);
                //hideShowMarkerTooltip(true, val, isTooltipVisible);
            }
        })
    }
}
