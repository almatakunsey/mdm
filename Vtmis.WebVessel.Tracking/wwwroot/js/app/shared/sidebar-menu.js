﻿(function () {

    "use strict";

    $(document).ready(function () {

        $("#tools_filter").click(function () {
            var caret = $("#tools_filter_caret");
            var content = $("#tools_filter_content")
           
            if (caret.hasClass("fa-caret-down")) {
                caret.removeClass("fa-caret-down");
                caret.addClass("fa-caret-left");
            } else {
                caret.removeClass("fa-caret-left");
                caret.addClass("fa-caret-down");
            }
            
            content.toggle("slow");
        });

        $("#tools_names").click(function () {
            var caret = $("#tools_names_caret");
            var content = $("#tools_names_content");

            if (caret.hasClass("fa-caret-down")) {
                caret.removeClass("fa-caret-down");
                caret.addClass("fa-caret-left");
            } else {
                caret.removeClass("fa-caret-left");
                caret.addClass("fa-caret-down");
            }

            content.toggle("slow");
        });

        $("#tools_tracks").click(function () {
            var caret = $("#tools_tracks_caret");
            var content = $("#tools_tracks_content");

            if (caret.hasClass("fa-caret-down")) {
                caret.removeClass("fa-caret-down");
                caret.addClass("fa-caret-left");
            } else {
                caret.removeClass("fa-caret-left");
                caret.addClass("fa-caret-down");
            }

            content.toggle("slow");
        });

        $("#tools_infoPanel").click(function () {
            var caret = $("#tools_infoPanel_caret");
            var content = $("#tools_infoPanel_content");

            if (caret.hasClass("fa-caret-down")) {
                caret.removeClass("fa-caret-down");
                caret.addClass("fa-caret-left");
            } else {
                caret.removeClass("fa-caret-left");
                caret.addClass("fa-caret-down");
            }

            content.toggle("slow");
        });

        $("#tools_infoBallon").click(function () {
            var caret = $("#tools_infoBallon_caret");
            var content = $("#tools_infoBallon_content");

            if (caret.hasClass("fa-caret-down")) {
                caret.removeClass("fa-caret-down");
                caret.addClass("fa-caret-left");
            } else {
                caret.removeClass("fa-caret-left");
                caret.addClass("fa-caret-down");
            }

            content.toggle("slow");
        });

        $("#tools_follow").click(function () {
            var caret = $("#tools_follow_caret");
            var content = $("#tools_follow_content");

            if (caret.hasClass("fa-caret-down")) {
                caret.removeClass("fa-caret-down");
                caret.addClass("fa-caret-left");
            } else {
                caret.removeClass("fa-caret-left");
                caret.addClass("fa-caret-down");
            }

            content.toggle("slow");
        });

        $("#tools_selectedShip").click(function () {
            var caret = $("#tools_selectedShip_caret");
            var content = $("#tools_selectedShip_content");

            if (caret.hasClass("fa-caret-down")) {
                caret.removeClass("fa-caret-down");
                caret.addClass("fa-caret-left");
            } else {
                caret.removeClass("fa-caret-left");
                caret.addClass("fa-caret-down");
            }

            content.toggle("slow");
        });

        $("#tools_list").click(function () {
            var caret = $("#tools_list_caret");
            var content = $("#tools_list_content");

            if (caret.hasClass("fa-caret-down")) {
                caret.removeClass("fa-caret-down");
                caret.addClass("fa-caret-left");
            } else {
                caret.removeClass("fa-caret-left");
                caret.addClass("fa-caret-down");
            }

            content.toggle("slow");
        });
    });

})();