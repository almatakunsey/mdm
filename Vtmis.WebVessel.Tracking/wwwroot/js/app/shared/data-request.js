﻿
var dataRequest = function (requestMethod, url, data = null) {
    return $.ajax({
        method: requestMethod,
        url: url,
        data: data
    });    
}