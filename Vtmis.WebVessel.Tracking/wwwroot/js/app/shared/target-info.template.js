﻿"use strict";

var targetInfoTemplate = function (data, targetImage) {
    return `
        <div>
            <div class="panel panel-primary panel-heading">
                 <table class="table table-condensed">
                    <tr>
                        <td class="popup-flag"><div class="flag flag-my"></div></td>
                        <td><h4 style="margin-top: 7px; color: dimgray; text-align: center;">   `+ data.targetName + `</h4></td>
                    </tr>
                  </table>       
            </div>
        
            <div class="row">
               <div class="col-md-12">
                    <img class="img-responsive" src="`+ targetImage + `" style="max-width:30em !important;max-height:20em !important; margin-bottom:2px;"></img>
                </div>
                `+ getTemplate(data) + `
            </div>
        </div>   
    `;
}
//TODO: show IP if in role Admin
function getTemplate(data) {

    if (data.targetPositions[0].messageId == 18) {
        return classBTargetInfo(data);
    } else if (data.targetPositions[0].messageId == 1 || data.targetPositions[0].messageId == 2
        || data.targetPositions[0].messageId == 3) {
        return classATargetInfo(data);
    } else if (data.targetPositions[0].messageId == 9) {
        return sarAircraftTargetInfo(data);
    } else if (data.targetPositions[0].messageId == 21 || data.targetPositions[0].messageId == 6) {
        return atonTargetInfo(data);
    } else if (data.targetPositions[0].messageId == 4) {
        return baseStationTargetInfo(data);
    }
    else {
        return generalTargetInfo(data);
    }
}

function classATargetInfo(data) {
    
    return `<div>
                    <table class="table table-striped table-bordered text-black" style="margin-bottom: 0px;width: 26.7em !important; margin-left: 15px;">
                        <thead>
                            <tr class="info">
                                <th>IMO</th>
                                <th>MMSI</th>
                                <th>Call Sign</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.imo + `</td>
                                <td>`+ data.mmsi + `</td>
                                <td>` + data.callSign + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Type/Cargo</th>
                                <th>Length x Beam</th>
                                <th>Nearest MM</th>   
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.shipType_ToString + `</td>
                                <td>`+ data.lengthXbeam + `</td>
                                <td>?</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Draught</th>
                                <th>Last Seen UTC</th>
                                <th>Last Seen Local</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.maxDraught + `</td>
                                <td>`+ data.targetPositions[0].receivedTime + `</td>
                                <td>`+ data.targetPositions[0].localReceivedTime + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Latitude</th>
                                <th>Longitude</th>     
                                 <th>Nav. Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].latitude + `</td>
                               <td>`+ data.targetPositions[0].longitude + `</td>
                                <td>`+ data.targetPositions[0].navigationalStatus + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Speed</th>
                                <th>Course</th>
                                <th>Heading</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].sog + ` knots</td>
                                <td>`+ data.targetPositions[0].cog + `°</td>
                                <td>`+ data.targetPositions[0].trueHeading + `°</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">                                
                                <th>Rate of Turn</th>
                                <th>Destination</th>
                                 <th>ETA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].rot + `</td>
                               <td>`+ data.destination + `</td>
                                <td>`+ data.eta + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">                               
                                <th>Pos. Accuracy</th>
                                <th>Pos. Fix. Dev.</th>  
                                <th>IP</th> 
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].positionAccuracyToString + `</td>
                               <td>`+ data.positionFixingDeviceToString + `</td> 
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>`;
}

function classBTargetInfo(data) {
    return `<div>
                    <table class="table table-striped table-bordered text-black" style="margin-bottom: 0px;width: 26.7em !important; margin-left: 15px;">
                        <thead>
                            <tr class="info">
                                <th>IMO</th>
                                <th>MMSI</th>
                                <th>Call Sign</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.imo + `</td>
                                <td>`+ data.mmsi + `</td>
                                <td>` + data.callSign + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Type/Cargo</th>
                                <th>Length x Beam</th>
                                <th>Nearest MM</th>   
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.shipType_ToString + `</td>
                                <td>`+ data.lengthXbeam + `</td>
                                <td>?</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>RAIM</th>
                                <th>Last Seen UTC</th>
                                <th>Last Seen Local</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].raim + `</td>
                                <td>`+ data.targetPositions[0].receivedTime + `</td>
                                <td>`+ data.targetPositions[0].localReceivedTime + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Latitude</th>
                                <th>Longitude</th>     
                                 <th>IP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].latitude + `</td>
                               <td>`+ data.targetPositions[0].longitude + `</td>
                                <td></td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Speed</th>
                                <th>Course</th>
                                <th>Heading</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].sog + ` knots</td>
                                <td>`+ data.targetPositions[0].cog + `°</td>
                                <td>`+ data.targetPositions[0].trueHeading + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">                                
                                <th>Class B Unit</th>
                                <th>Class B Display</th>
                                 <th>Class B DSC</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>?</td>
                               <td>?</td>
                                <td>?</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">                               
                                <th>Pos. Accuracy</th>
                                <th colspan="2">Pos. Fix. Dev.</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].positionAccuracyToString + `</td>
                               <td colspan="2">`+ data.positionFixingDeviceToString + `</td>                          
                            </tr>
                        </tbody>
                    </table>
                </div>`;
}

function baseStationTargetInfo(data) {
    return `<div>
                    <table class="table table-striped table-bordered text-black" style="margin-bottom: 0px;width: 26.7em !important; margin-left: 15px;">
                        <thead>
                            <tr class="info">                              
                                <th>MMSI</th>
                                <th colspan="2">UTC Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.imo + `</td>
                                <td colspan="2">`+ data.targetPositions[0].receivedTime + `</td>
                            </tr>
                        </tbody>                       
                        <thead>
                            <tr class="info">
                                <th>Nearest MM</th>
                                <th>Last Seen UTC</th>
                                <th>Last Seen Local</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>?</td>
                                <td>`+ data.targetPositions[0].receivedTime + `</td>
                                <td>`+ data.targetPositions[0].localReceivedTime + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Latitude</th>
                                <th>Longitude</th>     
                                 <th>IP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].latitude + `</td>
                               <td>`+ data.targetPositions[0].longitude + `</td>
                                <td></td>
                            </tr>
                        </tbody>                       
                        <thead>
                            <tr class="info">                               
                                <th>Pos. Accuracy</th>
                                <th colspan="2">Pos. Fix. Dev.</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                              <td>`+ data.targetPositions[0].positionAccuracyToString + `</td>
                               <td colspan="2">`+ data.positionFixingDeviceToString + `</td>                             
                            </tr>
                        </tbody>
                    </table>
                </div>`;
}

function sarAircraftTargetInfo(data) {
    return `<div>
                    <table class="table table-striped table-bordered text-black" style="margin-bottom: 0px;width: 26.7em !important; margin-left: 15px;">
                        <thead>
                            <tr class="info">   
                                <th>MMSI</th>
                                <th colspan="2">Call Sign</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                                
                                <td>`+ data.mmsi + `</td>
                                <td colspan="2">`+ data.callSign + `</td>
                            </tr>
                        </tbody>                       
                        <thead>
                            <tr class="info">
                                <th>Altitude</th>
                                <th>Last Seen UTC</th>
                                <th>Last Seen Local</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].altitude + `</td>
                                <td>`+ data.targetPositions[0].receivedTime + `</td>
                                <td>`+ data.targetPositions[0].localReceivedTime + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Latitude</th>
                                <th>Longitude</th>     
                                 <th>IP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].latitude + `</td>
                               <td>`+ data.targetPositions[0].longitude + `</td>
                                <td></td>
                            </tr>
                        </tbody>                       
                        <thead>
                            <tr class="info">                               
                                <th>Pos. Accuracy</th>
                                <th colspan="2">Pos. Fix. Dev.</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                               <td>`+ data.targetPositions[0].positionAccuracyToString + `</td>
                               <td colspan="2">`+ data.positionFixingDeviceToString + `</td>                            
                            </tr>
                        </tbody>
                    </table>
                </div>`;
}

function atonTargetInfo(data) {
    return `<div>
                    <table class="table table-striped table-bordered text-black" style="margin-bottom: 0px;width: 26.7em !important; margin-left: 15px;">
                        <thead>
                            <tr class="info">   
                                <th>MMSI</th>
                                <th>Aid-to-Nav. Type</th>
                                <th>Dimension</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                                
                                <td>`+ data.mmsi + `</td>
                                <td>`+ data.atoNType_ToString + `</td>
                                <td>`+ data.lengthXbeam + `</td>
                            </tr>
                        </tbody>                       
                        <thead>
                            <tr class="info">
                                <th>Nearest MM</th>
                                <th>Last Seen UTC</th>
                                <th>Last Seen Local</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>?</td>
                                <td>`+ data.targetPositions[0].receivedTime + `</td>
                                <td>`+ data.targetPositions[0].localReceivedTime + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Latitude</th>
                                <th>Longitude</th>     
                                 <th>Virtual Aid-to-Nav</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].latitude + `</td>
                               <td>`+ data.targetPositions[0].longitude + `</td>
                                <td>` + data.virtualAtoN_ToString + `</td>
                            </tr>
                        </tbody>                       
                       <thead>
                            <tr class="info">
                                <th>Off Position (Msg. 21)</th>
                                <th colspan="2">AtoN Status (Msg. 21)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>` + data.offPosition21_ToString + `</td>
                               <td colspan="2">` + data.atoNStatus21 + `</td>
                            </tr>
                        </tbody>  
                        <thead>
                            <tr class="info">
                                <th>Off Position (Msg. 6)</th>
                                <th colspan="2">AtoN Status (Msg. 6)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>` + data.offPosition68_ToString + `</td>
                               <td colspan="2">` + data.atoNStatus68 + `</td>
                            </tr>
                        </tbody>  
                        <thead>
                            <tr class="info">
                                <th>Analogue Int/1/2</th>
                                <th colspan="2">Digital Input (7..0)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>` + data.analogueInt_1_2 + `</td>
                               <td colspan="2">?</td>
                            </tr>
                        </tbody>  
                        <thead>
                            <tr class="info">                               
                                <th>Pos. Accuracy</th>
                                <th>Pos. Fix. Dev.</th> 
                                <th>IP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                               <td>`+ data.targetPositions[0].positionAccuracyToString + `</td>
                               <td>`+ data.positionFixingDeviceToString + `</td> 
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>`;
}

function generalTargetInfo(data) {
    return `<div>
                    <table class="table table-striped table-bordered text-black" style="margin-bottom: 0px;width: 26.7em !important; margin-left: 15px;">
                        <thead>
                            <tr class="info">
                                <th colspan="3">MMSI</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3">`+ data.imo + `</td>
                            </tr>
                        </tbody>                      
                        <thead>
                            <tr class="info">
                                <th>Nearest MM</th>
                                <th>Last Seen UTC</th>
                                <th>Last Seen Local</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>?</td>
                                <td>`+ data.targetPositions[0].receivedTime + `</td>
                                <td>`+ data.targetPositions[0].localReceivedTime + `</td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr class="info">
                                <th>Latitude</th>
                                <th>Longitude</th>     
                                 <th>IP</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>`+ data.targetPositions[0].latitude + `</td>
                               <td>`+ data.targetPositions[0].longitude + `</td>
                                <td></td>
                            </tr>
                        </tbody>                        
                    </table>
                </div>`;
}