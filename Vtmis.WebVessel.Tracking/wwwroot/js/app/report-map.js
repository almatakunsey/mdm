﻿//7

function ViewReport() {
    var selectedReportTypeItem = $("#reportType option:selected").val();
    var selectedReportTypeNum = selectedReportTypeItem.split("|");

    $("#viewReportContainer").removeClass("hide");
    $("#reportFormContainer").addClass("hide");
    var tableReportContainerEl = $("#table-report-container");
    tableReportContainerEl.html("");

    if (selectedReportTypeNum[1] == "07") {
        var reportMinSpeed = $("#reportMinSpeed").val();
        var reportMaxSpeed = $("#reportMaxSpeed").val();

        if (parseInt(reportMinSpeed) > parseInt(reportMaxSpeed)) {
            alert("Min Speed cannot be higher than Max Speed");
        } else {

            $("#tableTitle").html("Speed Violation - Min Speed (" + reportMinSpeed + ") | Max Speed (" +
                reportMaxSpeed + ")");

            getSpeedViolationReportByMinMaxSpeed(reportMinSpeed, reportMaxSpeed).then(function (resp) {
                var tableData = "";
                if (resp.length > 0) {
                    $.each(resp, function (i, val) {
                        tableData += `<tr>
                                        <td>`+ val.localTime + `</td>
                                        <td>`+ val.vesselName + `</td>
                                        <td>`+ val.mmsi + `</td>
                                        <td>`+ val.sog + `</td>
                                        <td>`+ val.imo + `</td>
                                        <td>`+ val.destination + `</td>
                                    </tr>`;
                    });
                }
                tableReportContainerEl.html(`
                    <table class="table table-responsive table-bordered table-condensed table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Local Received Time</th>
                                <th>Name</th>
                                <th>MMSI</th>
                                <th>SOG</th>
                                <th>IMO</th>
                                <th>Destination</th>
                            </tr>
                        </thead>
                        <tbody>
                            `+ tableData + `
                        </tbody>
                    </table>
                `);
            });
        }
    }
}

function ViewForm() {
    $("#reportFormContainer").removeClass("hide");
    $("#viewReportContainer").addClass("hide");
}

function resetReportForm() {
    var reportName = $("#reportName");
    var isEnabled = $("#reportEnable");
    var reportType = $("#reportType");
    var reportTimeSpan = $("#reportTimeSpan");
    var reportTimeSpanType = $("#reportTimeSpanType");
    var reportMinSpeed = $("#reportMinSpeed");
    var reportMaxSpeed = $("#reportMaxSpeed");
    var reportZone = $("#reportZone");
    var reportFilter = $("#reportFilter");

    reportName.val("");
    reportTimeSpan.val("");
    reportMinSpeed.val(0);
    reportMaxSpeed.val(0);
    reportType.selectedIndex = 0;
    reportTimeSpanType.selectedIndex = 0;
    reportZone.val("");
    reportFilter.val("");
}

function populateForm(dataReportName, dataReportTypeId, dataReportTypeNum, dataIsReportEnabled,
    dataTimeSpanValue, dataTimeSpanType, dataReportItems) {
    var reportName = $("#reportName");
    var isEnabled = $("#reportEnable");
    var reportType = $("#reportType");
    var reportTimeSpan = $("#reportTimeSpan");
    var reportTimeSpanType = $("#reportTimeSpanType");
    var reportZone = $("#reportZone");

    reportName.val(dataReportName);
    reportTimeSpan.val(dataTimeSpanValue);
    reportType.val(dataReportTypeId + "|" + dataReportTypeNum);
    reportTimeSpanType.val(dataTimeSpanType);
    reportZone.val("");
    var selectedItem = $("#reportType option:selected").val();
    var selectedReportTypeNum = selectedItem.split("|");
    var elParent = $("#additionalReportItem");
    elParent.html("");
    var childItem = `
                                 <div class="form-group col-lg-2">          
                                    <label class="label-report" for="reportZone">Zone:</label>
                                    <input type="text" class="form-control" id="reportZone">
                                </div>
                                <div class="form-group col-lg-2">
                                    <label class="label-report" for="reportFilter">Filter:</label>
                                    <select class="form-control" id="reportFilter">
                                        
                                    </select>
                                </div>
                            `;
    if (selectedReportTypeNum[1] === "07") {
        childItem += `
                                <div class="form-group col-lg-2">
                                    <label class="label-report" for="reportMinSpeed">Min Speed:</label>
                                    <input type="number" class="form-control" id="reportMinSpeed" value="0" />
                                </div>
                                <div class="form-group col-lg-2">
                                    <label class="label-report" for="reportMaxSpeed">Max Speed:</label>
                                    <input type="number" class="form-control" id="reportMaxSpeed" value="0" />
                                </div>
                            `;
    }
    elParent.append(childItem);
    dataRequest("GET", applicationHostUrl + "/api/services/app/FilterGroupService/GetAll").then(function (res) {
        var resultDom = $("#reportFilter");
        resultDom.html("");
        var optionsValue = `<option value=""></option>`;
        var result = res.result;

        if (result.length > 0) {
            $.each(result, function (i, val) {
                optionsValue += `<option value="` + val.id + `|default">` + val.name + `</option>`;
            });
        }

        dataRequest("GET", applicationHostUrl + "/api/services/app/FilterService/GetAll?userId=" + userId).then(function (resFilter) {
            var resultFilter = resFilter.result;
            if (resultFilter.length > 0) {
                $.each(resultFilter, function (i, val) {
                    optionsValue += `<option value="` + val.id + `|custom">` + val.name + `</option>`;
                });
            }
            resultDom.append(optionsValue);
            var reportFilter = $("#reportFilter");
            var reportMinSpeed = $("#reportMinSpeed");
            var reportMaxSpeed = $("#reportMaxSpeed");
            if (dataReportItems[0].filterGroupId != null) {
                reportFilter.val(dataReportItems[0].filterGroupId + "|default");
            } else {
                reportFilter.val(dataReportItems[0].filterId + "|custom");
            }
            if (dataReportTypeNum == "07") {
                reportMinSpeed.val(dataReportItems[0].minSpeed);
                reportMaxSpeed.val(dataReportItems[0].maxSpeed);
            }
        });
    });
}

function SaveReport() {
    var selectReportType = $("#selectReportType option:selected");
    var reportName = $("#reportName");
    var isEnabled = $("#reportEnable");
    var reportType = $("#reportType");
    var reportTimeSpan = $("#reportTimeSpan");
    var reportTimeSpanType = $("#reportTimeSpanType");
    var reportMinSpeed = $("#reportMinSpeed");
    var reportMaxSpeed = $("#reportMaxSpeed");
    var selectedItem = $("#reportType option:selected").val();
    var selectedReportTypeNum = selectedItem.split("|");
    var isFormValid = false;
    var reportFilter = $("#reportFilter option:selected").val();
    if (reportName.val() == "" || reportTimeSpan.val() == "") {
        alert("Please enter all the provided fields");
    }
    else if (userId == "" || userId == undefined || userId == null) {
        alert("No user id. Please relogin");
    }
    else {
        var reportItemDtos = [];
        var reportItemToPush;
        var reportFilterData = reportFilter.split('|');
        if (reportFilterData[1] == "custom") {
            reportItemToPush = {
                filterId: reportFilterData[0]
            };
        } else {
            reportItemToPush = {
                filterGroupId: reportFilterData[0]
            };
        }
        reportItemToPush.filterType = reportFilterData[1];
        if (selectedReportTypeNum[1] === "07") {
            if (parseInt(reportMinSpeed.val()) > parseInt(reportMaxSpeed.val())) {
                alert("Min Speed cannot be higher than Max Speed");
            } else {
                isFormValid = true;

                reportItemToPush.minSpeed = reportMinSpeed.val();
                reportItemToPush.maxSpeed = reportMaxSpeed.val();
            }

        } else {
            isFormValid = true;
            //reportItemDtos = null;
        }
        reportItemDtos.push(reportItemToPush);
        if (isFormValid) {
            var objToSave = {
                name: reportName.val(),
                userId: userId,
                isEnabled: isEnabled.is(":checked"),
                timeSpan: reportTimeSpan.val(),
                timeSpanType: reportTimeSpanType.val(),
                reportTypeId: selectedReportTypeNum[0],
                reportItemDtos: reportItemDtos
            };
            //console.log(objToSave);
            var ajaxMethodType = "POST";
            var ajaxServiceType = "Create";

            if (selectReportType.val() != 0) {
                ajaxMethodType = "PUT";
                ajaxServiceType = "Update";
                objToSave.id = selectReportType.val();
            }
            $.ajax({
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                'type': ajaxMethodType,
                'url': applicationHostUrl + '/api/services/app/ReportService/' + ajaxServiceType,
                'data': JSON.stringify(objToSave),
                'dataType': 'json'
            }).error(function (err) {
                $('#modalNewReport').iziModal('close');
                swal("Error", "Error while saving report", "error").then(() => {
                    $('#modalNewReport').iziModal('open');
                });
            }).then(function (res) {
                $('#modalNewReport').iziModal('close');
                if (res.success == true) {
                    swal("Report successfully saved!", {
                        icon: "success",
                    }).then(() => {
                        $('#modalNewReport').iziModal('open');
                    });
                } else {
                    swal("Error", "Error while saving report", "error").then(() => {
                        $('#modalNewReport').iziModal('open');
                    });

                }

            });
        }
    }
}

function loadReports(url) {
    getAllReports(url).then(function (resp) {
        if (resp.success) {
            //var tableDom = $("#report-table");
            var selectReportType = $("#selectReportType");
            var report = resp.result;
            //tableDom.html("");
            selectReportType.html("");
            var reportOptions = "<option value='0'>New</option>";
            if (report.length > 0) {
                var obj;

                for (var i = 0; i < report.length; i++) {

                    reportOptions += `
                            <option value="`+ report[i].id + `">` + report[i].name + `</option>
                        `;
                }


                selectReportType.change(function () {
                    if (selectReportType.val() == 0) {
                        resetReportForm();
                        //$("#saveNewReport").removeClass("hide");
                        //$("#saveNewReport").addClass("show");
                        $("#viewReportBtn").addClass("hide");
                    } else {
                        dataRequest("GET", url + "/api/services/app/ReportService/GetById?id=" + selectReportType.val())
                            .then(function (resSelectReportType) {

                                var result = resSelectReportType.result;
                                //$("#saveNewReport").removeClass("show");
                                //$("#saveNewReport").addClass("hide");
                                $("#viewReportBtn").removeClass("hide");
                                populateForm(result.name, result.reportTypeId, result.reportType.reportTypeNumber, result.isEnabled,
                                    result.timeSpan, result.timeSpanType, result.reportItemDtos);
                            });
                    }
                });
            }
            selectReportType.append(reportOptions);
        }
    });
}

function loadReportType(url) {
    getAllReportTypes(url).then(function (res) {
        if (res.success) {
            var reportTypes = res.result;
            var reportTypeDom = $("#reportType");
            reportTypeDom.html("");
            if (reportTypes.length > 0) {
                var obj;
                for (var i = 0; i < reportTypes.length; i++) {
                    obj = `<option value="` + reportTypes[i].id + "|" + reportTypes[i].reportTypeNumber + `">`
                        + reportTypes[i].name + `</option>`;
                    reportTypeDom.append(obj);
                }

                reportTypeDom.change(function () {
                    var selectedItem = $("#reportType option:selected").val();
                    var selectedReportTypeNum = selectedItem.split("|");
                    var elParent = $("#additionalReportItem");
                    elParent.html("");
                    var childItem = `
                                 <div class="form-group col-lg-2">          
                                    <label class="label-report" for="reportZone">Zone:</label>
                                    <input type="text" class="form-control" id="reportZone">
                                </div>
                                <div class="form-group col-lg-2">
                                    <label class="label-report" for="reportFilter">Filter:</label>
                                    <select class="form-control" id="reportFilter">
                                        
                                    </select>
                                </div>
                            `;
                    //console.log(selectedReportTypeNum[1]);
                    if (selectedReportTypeNum[1] === "07") {
                        childItem += `
                                <div class="form-group col-lg-2">
                                    <label class="label-report" for="reportMinSpeed">Min Speed:</label>
                                    <input type="number" class="form-control" id="reportMinSpeed" value="0" />
                                </div>
                                <div class="form-group col-lg-2">
                                    <label class="label-report" for="reportMaxSpeed">Max Speed:</label>
                                    <input type="number" class="form-control" id="reportMaxSpeed" value="0" />
                                </div>
                            `;
                    }
                    elParent.append(childItem);
                    loadFilters(url);
                });

                reportTypeDom.change();
            }

        }
    });
}

function loadTimeSpan(url) {
    getAllTimeSpanTypes(url).then(function (res) {
        if (res.success) {
            var result = res.result;
            //console.log(result);
            var resultDom = $("#reportTimeSpanType");
            resultDom.html("");
            if (result.length > 0) {
                var obj;
                for (var i = 0; i < result.length; i++) {
                    obj = `<option value="` + result[i].value + `">` + result[i].name + `</option>`;
                    resultDom.append(obj);
                }

            }

        }
    });
}

function loadFilters(url) {
    dataRequest("GET", url + "/api/services/app/FilterGroupService/GetAll").then(function (res) {

        var resultDom = $("#reportFilter");
        resultDom.html("");
        var optionsValue = `<option value=""></option>`;
        var result = res.result;

        if (result.length > 0) {
            $.each(result, function (i, val) {
                optionsValue += `<option value="` + val.id + `|default">` + val.name + `</option>`;
            });
        }

        dataRequest("GET", url + "/api/services/app/FilterService/GetAll?userId=" + userId).then(function (resFilter) {

            var resultFilter = resFilter.result;
            //console.log(resultFilter);
            if (resultFilter.length > 0) {
                $.each(resultFilter, function (i, val) {
                    optionsValue += `<option value="` + val.id + `|custom">` + val.name + `</option>`;
                });
            }
            //console.log(optionsValue);
            resultDom.append(optionsValue);
        });
    });
}

var deleteReport = function (data) {
    //console.log(data);
    var x = confirm("Are you sure you want to delete?");
    if (x) {
        deleteReportById(applicationHostUrl, data).then(function (res) {
            if (res.success) {
                loadReports();
            }
            return true;
        });
    }
    else {
        return false;
    }

}

$(document).ready(function () {
    $("#modalNewReport").iziModal({
        title: '',
        subtitle: '',
        headerColor: '#429cb7',
        background: null,
        theme: '',  // light
        icon: 'fa fa-filter',
        iconText: null,
        iconColor: '',
        rtl: false,
        width: '98%',
        top: '3%',
        bottom: '100px',
        borderBottom: true,
        padding: 10,
        radius: 3,
        zindex: 100000,
        iframe: false,
        iframeHeight: 400,
        iframeURL: null,
        focusInput: true,
        group: '',
        loop: false,
        arrowKeys: true,
        navigateCaption: true,
        navigateArrows: true, // Boolean, 'closeToModal', 'closeScreenEdge'
        history: false,
        restoreDefaultContent: true,
        autoOpen: 0, // Boolean, Number
        bodyOverflow: true,
        fullscreen: false,
        openFullscreen: false,
        closeOnEscape: true,
        closeButton: true,
        appendTo: false, // or false
        appendToOverlay: false, // or false
        overlay: true,
        overlayClose: false,
        overlayColor: 'rgba(0, 0, 0, 0.4)',
        timeout: false,
        timeoutProgressbar: false,
        pauseOnHover: false,
        timeoutProgressbarColor: 'rgba(255,255,255,0.5)',
        transitionIn: 'flipInX',
        transitionOut: 'flipOutX',
        transitionInOverlay: 'fadeIn',
        transitionOutOverlay: 'fadeOut',
        onFullscreen: function () { },
        onResize: function () { },
        onOpening: function () {
            loadReports(applicationHostUrl);
            loadReportType(applicationHostUrl);
            loadTimeSpan(applicationHostUrl);

        },
        onOpened: function (modal, event) {
            //modal.startLoading();
        },
        onClosing: function () { },
        onClosed: function () { },
        afterRender: function () { }
    });
});
