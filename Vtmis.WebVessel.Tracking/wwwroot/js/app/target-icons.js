﻿"use strict";

function getTargetIcon(messageId) {
    if (messageId == 18) {
        return new L.icon({
            iconUrl: 'images/class_b.png'
        });
        //return new L.ClassBTargetIcon();
    } else if (messageId == 1 || messageId == 2
        || messageId == 3) {
        return new L.ClassATargetIcon();
    } else if (messageId == 9) {
        return new L.SarTargetIcon();
    } else if (messageId == 21 || messageId == 6) {
        return new L.AtonTargetIcon();
    } else if (messageId == 4) {
        return new L.BaseStationTargetIcon();
    } else if (messageId == 8) {
        return new L.MetHydroStation();
    }
    else {
        return new L.DefaultShipIcon();
    }
}

L.MetHydroStation = L.CanvasIcon.extend({
    options: {
        iconSize: new L.Point(60, 60),
        iconAnchor: new L.Point(30, 20),
        fillStyle: 'orange'
    },
    _setIconStyles: function (icon, type) {

        if (type == 'icon') {
            var ctx9 = icon.getContext('2d');
            var size = L.point(this.options.iconSize);
            var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));
            var ctx9Radius = size.x * 0.08;

            var ctx9Angle = 225;
            var ctx9Scale = size.x * 0.07;

            var x1 = center.x + ctx9Radius * Math.cos((ctx9Angle - 90) * Math.PI / 180) * 1;
            var y1 = center.y + ctx9Radius * Math.sin((ctx9Angle - 90) * Math.PI / 180) * 1;
            var x2 = x1 + ctx9Radius * Math.cos((ctx9Angle - 90) * Math.PI / 180) * 4;
            var y2 = y1 + ctx9Radius * Math.sin((ctx9Angle - 90) * Math.PI / 180) * 4;
            var x21 = x2 + ctx9Radius * Math.cos((ctx9Angle) * Math.PI / 180) * 1;
            var y21 = y2 + ctx9Radius * Math.sin((ctx9Angle) * Math.PI / 180) * 1;
            var x3 = x1 + ctx9Radius * Math.cos((ctx9Angle - 90) * Math.PI / 180) * 5;
            var y3 = y1 + ctx9Radius * Math.sin((ctx9Angle - 90) * Math.PI / 180) * 5;
            var x31 = x3 + ctx9Radius * Math.cos((ctx9Angle) * Math.PI / 180) * 2;
            var y31 = y3 + ctx9Radius * Math.sin((ctx9Angle) * Math.PI / 180) * 2;


            var textDirectionX = x1 + ctx9Radius * Math.cos((ctx9Angle - 90) * Math.PI / 180) * 2.5;
            var textDirectionY = y1 + ctx9Radius * Math.sin((ctx9Angle - 90) * Math.PI / 180) * 2.5;

            var textDirectionX1 = center.x + ctx9Radius * Math.cos((ctx9Angle - 45) * Math.PI / 180) * 1.5;
            var textDirectionY1 = center.y + ctx9Radius * Math.sin((ctx9Angle - 45) * Math.PI / 180) * 1.5;

            var textDirectionX2 = x1 + ctx9Radius * Math.cos((ctx9Angle - 270) * Math.PI / 180) * 2.5;
            var textDirectionY2 = y1 + ctx9Radius * Math.sin((ctx9Angle - 270) * Math.PI / 180) * 2.5;

            ctx9.beginPath();

            ctx9.lineWidth = 1;
            ctx9.arc(center.x, center.y, ctx9Radius, 0, 2 * Math.PI);
            ctx9.stroke();
            ctx9.closePath();

            ctx9.beginPath();
            /* ctx9.arc(x1,y1,5,0,2*Math.PI); */
            ctx9.moveTo(x1, y1);
            ctx9.lineTo(x3, y3);
            ctx9.moveTo(x2, y2);
            ctx9.lineTo(x21, y21);
            ctx9.moveTo(x3, y3);
            ctx9.lineTo(x31, y31);

            ctx9.font = "bold " + (ctx9Scale * 1.8) + "px arial";
            ctx9.textAlign = "start";
            ctx9.fillText("S", textDirectionX + ctx9Scale * 2, textDirectionY);
            ctx9.textAlign = "end";
            ctx9.fillText("90", textDirectionX1, textDirectionY1);
            ctx9.textAlign = "start";
            ctx9.fillText("1010", textDirectionX2, textDirectionY2);

            ctx9.stroke();
            ctx9.closePath();
        }
        L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
    }
});

L.DefaultShipIcon = L.CanvasIcon.extend({
    options: {
        iconSize: new L.Point(19, 26),
        iconAnchor: new L.Point(9.5, 9.5),
        fillStyle: 'orange'
    },
    _setIconStyles: function (icon, type) {
    
        if (type == 'icon') {
            var ctx = icon.getContext('2d');
            var size = L.point(this.options.iconSize);
            var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));
            ctx.beginPath();
            ctx.moveTo(center.x, 3);
            ctx.lineTo(center.x + center.x / 2, center.y);
            ctx.lineTo(center.x + center.x / 2, center.y * 3);

            ctx.lineTo(center.x - center.x / 2, center.y * 3);
            ctx.lineTo(center.x - center.x / 2, center.y);
            ctx.lineTo(center.x, 0);

            ctx.fillStyle = this.options.fillStyle;
            ctx.fill();

            ctx.strokeStyle = 'black';
            ctx.lineWidth = 1;
            ctx.stroke();
            ctx.closePath();
        }
        L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
    }
});

L.ClassATargetIcon = L.CanvasIcon.extend({
    options: {
        iconSize: new L.Point(19, 26),
        iconAnchor: new L.Point(9.5, 9.5),
        fillStyle: 'orange'
    },
    _setIconStyles: function (icon, type) {
    
        if (type == 'icon') {
            var ctx2 = icon.getContext('2d');
            var size = L.point(this.options.iconSize);
            var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));
            
            ctx2.beginPath();
            ctx2.lineWidth = 1.5;
            ctx2.moveTo(center.x,center.y-size.y*0.48);
            ctx2.lineTo(center.x+size.x*0.22,center.y+size.y*0.48);
            ctx2.lineTo(center.x-size.x*0.22,center.y+size.y*0.48);
            ctx2.lineTo(center.x,center.y-size.y/2);
            ctx2.lineTo(center.x + size.x * 0.22, center.y + size.y * 0.48);
            ctx2.fillStyle = this.options.fillStyle;
            ctx2.fill();
            ctx2.stroke();
            ctx2.closePath();
        }
        L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
    }
});

L.ClassBTargetIcon = L.CanvasIcon.extend({
    options: {
        iconSize: new L.Point(19, 26),
        iconAnchor: new L.Point(9.5, 9.5),
        fillStyle: 'orange'
    },
    _setIconStyles: function (icon, type) {

        if (type == 'icon') {
            var ctx3 = icon.getContext('2d');
            var size = L.point(this.options.iconSize);
            var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));

            ctx3.beginPath();
            ctx3.lineWidth = 2;
            ctx3.moveTo(center.x, center.y - size.y * 0.48);
            ctx3.lineTo(center.x + size.x * 0.22, center.y - size.y * 0.2);
            ctx3.lineTo(center.x + size.x * 0.22, center.y + size.y * 0.48);
            ctx3.lineTo(center.x - size.x * 0.22, center.y + size.y * 0.48);
            ctx3.lineTo(center.x - size.x * 0.22, center.y - size.y * 0.2);
            ctx3.lineTo(center.x, center.y - size.y * 0.48);
            ctx3.lineTo(center.x + size.x * 0.22, center.y - size.y * 0.2);
            ctx3.fillStyle = this.options.fillStyle;
            ctx3.fill();
            ctx3.stroke();
            ctx3.closePath();
            ctx3.beginPath();
            ctx3.arc(center.x, center.y, size.x / 2 * 0.1, 0, 2 * Math.PI);
            ctx3.stroke();
            ctx3.fillStyle = "black";
            ctx3.fill();
            ctx3.stroke();
            ctx3.closePath();
        }
        L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
    }
});

L.SarTargetIcon = L.CanvasIcon.extend({
    options: {
        iconSize: new L.Point(19, 26),
        iconAnchor: new L.Point(9.5, 9.5),
        fillStyle: 'orange'
    },
    _setIconStyles: function (icon, type) {

        if (type == 'icon') {
            var ctx4 = icon.getContext('2d');
            var size = L.point(this.options.iconSize);
            var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));
            ctx4.beginPath();
            ctx4.lineWidth = 2;
            for (var i = 1 * Math.PI; i < 2 * Math.PI; i += 0.01) {
                var xPos = center.x - (size.x * 0.11 * Math.sin(i)) * Math.sin(0 * Math.PI) + (size.x * 0.11 * Math.cos(i)) * Math.cos(0 * Math.PI);
               var yPos = center.y + (size.y * 0.32 * Math.cos(i)) * Math.sin(0 * Math.PI) + (size.y * 0.32 * Math.sin(i)) * Math.cos(0 * Math.PI) - size.y * 0.17;

                if (i == 0) {
                    ctx4.moveTo(xPos, yPos);
                } else {
                    ctx4.lineTo(xPos, yPos);
                }
            }

            ctx4.lineTo(center.x + size.x * 0.45, center.y + size.y * 0.1);
            ctx4.lineTo(center.x + size.x * 0.45, center.y + size.y * 0.23);
            ctx4.lineTo(center.x + size.x * 0.12, center.y + size.y * 0.02);

            ctx4.lineTo(center.x + size.x * 0.05, center.y + size.y * 0.38);
            ctx4.lineTo(center.x + size.x * 0.10, center.y + size.y * 0.43);
            ctx4.lineTo(center.x + size.x * 0.10, center.y + size.y * 0.5);


            ctx4.lineTo(center.x - size.x * 0.10, center.y + size.y * 0.5);
            ctx4.lineTo(center.x - size.x * 0.10, center.y + size.y * 0.43);
            ctx4.lineTo(center.x - size.x * 0.05, center.y + size.y * 0.38);


            ctx4.lineTo(center.x - size.x * 0.12, center.y + size.y * 0.02);
            ctx4.lineTo(center.x - size.x * 0.45, center.y + size.y * 0.23);
            ctx4.lineTo(center.x - size.x * 0.45, center.y + size.y * 0.1);
            ctx4.lineTo(center.x - (xPos - center.x), yPos);
            ctx4.fillStyle = this.options.fillStyle;
            ctx4.fill();
            ctx4.stroke();
            ctx4.closePath();
        }
        L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
    }
});

L.ArpaTargetIcon = L.CanvasIcon.extend({
    options: {
        iconSize: new L.Point(19, 26),
        iconAnchor: new L.Point(9.5, 9.5),
        fillStyle: 'orange'
    },
    _setIconStyles: function (icon, type) {

        if (type == 'icon') {
            var ctx1 = icon.getContext('2d');
            var size = L.point(this.options.iconSize);
            var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));

            ctx1.beginPath();
            ctx1.lineWidth = 2;
            ctx1.arc(center.x, center.y, size.x / 2 * 0.9, 0, 2 * Math.PI);
            ctx1.stroke();
            ctx1.fillStyle = this.options.fillStyle;
            ctx1.fill();
            ctx1.closePath();
            ctx1.beginPath();
            ctx1.arc(center.x, center.y, size.x / 2 * 0.1, 0, 2 * Math.PI);
            ctx1.stroke();
            ctx1.fillStyle = "black";
            ctx1.fill();
            ctx1.closePath();
        }
        L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
    }
});

L.AtonTargetIcon = L.CanvasIcon.extend({
    options: {
        iconSize: new L.Point(30, 30),
        iconAnchor: new L.Point(15, 15),
        fillStyle: 'orange'
    },
    _setIconStyles: function (icon, type) {

        if (type == 'icon') {
            var ctx7 = icon.getContext('2d');
            var size = L.point(this.options.iconSize);
            var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));
            ctx7.beginPath();
            ctx7.lineWidth = 1;
            ctx7.moveTo(center.x, center.y - size.y * 0.5);
            ctx7.lineTo(center.x + size.y * 0.5, center.y);
            ctx7.lineTo(center.x, center.y + size.y * 0.5);
            ctx7.lineTo(center.x - size.y * 0.5, center.y);
            ctx7.lineTo(center.x, center.y - size.y * 0.5);
            ctx7.lineTo(center.x + size.y * 0.5, center.y);
            ctx7.fillStyle = this.options.fillStyle;
            ctx7.fill();
            ctx7.stroke();
            ctx7.closePath();

            ctx7.beginPath();

            ctx7.moveTo(center.x, center.y - size.y * 0.5 / 3);
            ctx7.lineTo(center.x, center.y + size.y * 0.5 / 3);
            ctx7.moveTo(center.x - size.y * 0.5 / 3, center.y);
            ctx7.lineTo(center.x + size.y * 0.5 / 3, center.y);

            ctx7.stroke();
            ctx7.closePath();
        }
        L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
    }
});

L.BaseStationTargetIcon = L.CanvasIcon.extend({
    options: {
        iconSize: new L.Point(28, 28),
        iconAnchor: new L.Point(15, 15),
        fillStyle: 'orange'
    },
    _setIconStyles: function (icon, type) {

        if (type == 'icon') {
            var ctx8 = icon.getContext('2d');
            var size = L.point(this.options.iconSize);
            var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));
            ctx8.beginPath();
            ctx8.lineWidth = 2;
            ctx8.moveTo(center.x - size.x * 0.5, center.y - size.x * 0.5);
            ctx8.lineTo(center.x + size.x * 0.5, center.y - size.x * 0.5);
            ctx8.lineTo(center.x + size.x * 0.5, center.y + size.x * 0.5);
            ctx8.lineTo(center.x - size.x * 0.5, center.y + size.x * 0.5);
            ctx8.lineTo(center.x - size.x * 0.5, center.y - size.x * 0.5);
            ctx8.lineTo(center.x + size.x * 0.5, center.y - size.x * 0.5);
            ctx8.fillStyle = this.options.fillStyle;
            ctx8.fill();
            ctx8.stroke();
            ctx8.closePath();

            ctx8.beginPath();

            ctx8.lineWidth = 2;
            ctx8.arc(center.x, center.y, 2, 0, 2 * Math.PI);
            ctx8.fillStyle = "black";
            ctx8.fill();

            ctx8.stroke();
            ctx8.closePath();
        }
        L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
    }
});