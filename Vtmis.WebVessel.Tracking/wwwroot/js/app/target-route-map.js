﻿"use strict";

function drawPath(targetId, objs) {
    markersRoute = {};
    routeLayer.clearLayers();
    $.each(objs, function (i, obj) {
        //if (markersRoute[targetId + "|" + i] !== undefined) {
        //    delete markersRoute[targetId + "|" + i];
        //}

        markersRoute[targetId + "|" + i] =
            L.circle([obj.lat, obj.lgt], { title: "Lat: " + obj.lat + "Lgt: " + obj.lgt, radius: 2, renderer: canvasRenderer }).
                bindTooltip(`
                    <b>Name</b>: `+ obj.vesselName + `</br>
                    <b>MMSI</b>: `+ obj.vesselId + `</br>
                    <b>Latitude</b>: `+ obj.lat + `</br>
                    <b>Longitude</b>: `+ obj.lgt + `</br>
                    <b>Local</b>: `+ obj.receivedTime + `</br>
                `, { direction: "top", className: "marker-label" });
        markersRoute[targetId + "|" + i].addTo(routeLayer);
    });
    //console.log(markersRoute);
}

function drawRoute(obj, objIndex) {
    //console.log("Lat: " + obj.lat + "Lgt: " + obj.lgt);
    //console.log(markersRoute[obj.vesselId]);
    //if (markersRoute[obj.vesselId] === undefined) {
    //    markersRoute[obj.vesselId] = L.circle([obj.lat, obj.lgt], { title: "Lat: " + obj.lat + "Lgt: " + obj.lgt, radius: 2 });
    //}
    if (markersRoute[obj.vesselId + "|" + objIndex] === undefined) {
        markersRoute[obj.vesselId + "|" + objIndex] =
            L.circle([obj.lat, obj.lgt], { title: "Lat: " + obj.lat + "Lgt: " + obj.lgt, radius: 2 }).
                bindTooltip(`
                    <b>Name</b>: `+ obj.vesselName + `</br>
                    <b>MMSI</b>: `+ obj.vesselId + `</br>
                    <b>Latitude</b>: `+ obj.lat + `</br>
                    <b>Longitude</b>: `+ obj.lgt + `</br>
                    <b>Local</b>: `+ obj.receivedTime + `</br>
                `, { direction: "top", className: "marker-label" });
        markersRoute[obj.vesselId + "|" + objIndex].addTo(routeLayer);
    } else if (objIndex > 9) {

    } else {
        markersRoute[obj.vesselId + "|" + objIndex].setLatLng([obj.lat, obj.lgt]);
    }

    //var latlngs = [obj];
    //var polyline = L.polyline(obj, { color: 'red' });
    //polyline.addTo(routeLayer);
    //console.log(obj);
}

function toggleRoute() {
    if (map.hasLayer(routeLayer)) {
        routeLayer.remove();
    } else {
        routeLayer.addTo(map);
    }
}