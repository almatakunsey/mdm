﻿"use strict";
   //3
var controlSearch = new L.Control.Search({
            sourceData: localData,
            text: 'Target...',
            initial: false,	
            zoom: 12,
            position: 'topleft',
            marker: false
        });

map.addControl(controlSearch);

/**************************************
 * Push Target Info Searching Object
**************************************** */

var pushTargetInfo = function (obj) {
    if (obj.vesselName !== undefined && obj.vesselName !== null) {
        var targetName = { "loc": [obj.lat, obj.lgt], "title": obj.vesselName };
        targetData.push(targetName);
    }

    if (obj.vesselId !== undefined && obj.vesselId !== null) {
        var targetMMSI = { "loc": [obj.lat, obj.lgt], "title": obj.vesselId };
        targetData.push(targetMMSI);
    }

    if (obj.imo !== undefined && obj.imo !== null) {
        var targetIMO = { "loc": [obj.lat, obj.lgt], "title": obj.imo };
        targetData.push(targetIMO);
    }

    if (obj.callSign !== undefined && obj.callSign !== null) {
        var targetCallSign = { "loc": [obj.lat, obj.lgt], "title": obj.callSign };
        targetData.push(targetCallSign);
    }
}

function localData(text, callResponse) {
    //here can use custom criteria or merge data from multiple layers

    callResponse(targetData);

    return {	//called to stop previous requests on map move
        abort: function () {
            //console.log('aborted request:' + text);
        }
    };
}

/**************************************
 * Advanced Search Modal
**************************************** */
(function () {
    
    $(document).ready(function () {
        $("#modalAdvancedSearch").iziModal({
            title: '',
            subtitle: '',
            headerColor: '#429cb7',
            background: null,
            theme: '',  // light
            icon: 'fa fa-search',
            iconText: null,
            iconColor: '',
            rtl: false,
            width: '98%',
            top: '3%',
            bottom: '100px',
            borderBottom: true,
            padding: 10,
            radius: 3,
            zindex: 100000,
            iframe: false,
            iframeHeight: 400,
            iframeURL: null,
            focusInput: true,
            group: '',
            loop: false,
            arrowKeys: true,
            navigateCaption: true,
            navigateArrows: true, // Boolean, 'closeToModal', 'closeScreenEdge'
            history: false,
            restoreDefaultContent: false,
            autoOpen: 0, // Boolean, Number
            bodyOverflow: false,
            fullscreen: true,
            openFullscreen: false,
            closeOnEscape: true,
            closeButton: true,
            appendTo: false, // or false
            appendToOverlay: false, // or false
            overlay: true,
            overlayClose: false,
            overlayColor: 'rgba(0, 0, 0, 0.4)',
            timeout: false,
            timeoutProgressbar: false,
            pauseOnHover: false,
            timeoutProgressbarColor: 'rgba(255,255,255,0.5)',
            transitionIn: 'flipInX',
            transitionOut: 'flipOutX',
            transitionInOverlay: 'fadeIn',
            transitionOutOverlay: 'fadeOut',
            onFullscreen: function () { },
            onResize: function () { },
            onOpening: function () { },
            onOpened: function () { },
            onClosing: function () {
                $("#advancedName").val("");
                $("#advancedMMSI").val("");
                $("#advancedIMO").val("");
                $("#advancedCallSign").val("");
                $("#advancedSearchResult").html("");
            },
            onClosed: function () { },
            afterRender: function () { }
        });       

        $("#btnAdvancedSearch").click(function () {
            var searchObj = {
                advancedName: $("#advancedName").val(),
                advancedMMSI: $("#advancedMMSI").val(),
                advancedIMO: $("#advancedIMO").val(),
                advancedCallSign: $("#advancedCallSign").val()
            };
            var tableBody = $("#advancedSearchResult");
            tableBody.html("");
            dataRequest("GET", "/api/vessel/get", searchObj)
                .success(function (res) {
                    var result = res;
                    console.log(result);
                    if (result != null) {


                        if (result.length > 0) {

                            for (var i = 0; i < result.length; i++) {
                                var tr = `
             <tr>
                <td><button type="button" onclick="centerLeafletMapOnMarker(`+ result[i].vesselId + `)"><i class="fa fa-search"></i></button><button type="button"><i class="fa fa-camera"></i></button></td>
                <td>`+ result[i].vesselName + `</td>
                <td>`+ result[i].vesselId + `</td>
                <td>`+ result[i].imo + `</td>
                <td>`+ result[i].callSign + `</td>
                <td></td>
                <td></td>
                <td></td>
                <td>`+ result[i].lat + `</td>
                <td>`+ result[i].lgt + `</td>
                <td>`+ result[i].sog + `</td>
                <td>`+ result[i].cog + `</td>
                <td>`+ result[i].destination + `</td>
                <td></td>
            </tr>`
                                tableBody.append(tr);
                            }
                        }
                    }
                });
        });
    });
})();