﻿"use strict";
//5
//SignalR ---------------------------------------------------------


/**************************************
 * Timeline
*****************************************/

connection.on('setTimelineStartDate', data => {
    startTime = new Date(data);
    console.log(startTime);
});

/**************************************
 * Target Position
**************************************** */

connection.on('updatecurrentvessel', data => {
    //if (isViewFilter == false & restoreInProgress == false)
        setMarkers(data);
});

/**************************************
 *Clear All Markers
**************************************** */
connection.on('clearAllMarkers', data => {

    markers = {};
    markersLayer.clearLayers();
    console.log("clearAllMarkers");
});

//Start invoke server
//connection.start()
//    .then(() => connection.invoke('restoreHistory'));
start();

async function start() {
    try {
        await connection.start()
            .then(() => connection.invoke('restoreHistory'));
        console.log('connected');
    } catch (err) {
        console.log(err);
        setTimeout(() => start(), 5000);
    }
};

connection.onclose(async () => {
    await start();
});

/**************************************
 *Replay
**************************************** */

connection.on('updatereplayvessel', data => {
    var list = new L.Control.ListMarkers({ layer: markersLayer, itemIcon: null });
    map.addControl(list);
    //console.log(data);
    setMarkers(data);
});

$('#vesselReplay').on('click', function () {

    connection.invoke('replayServer', 'connect to cluster sever')
})

/**************************************
 *Restore Function
**************************************** */

var restoreInProgress = false;

connection.on('isRestoreProgress', data => {
    restoreInProgress = data;
    //console.log(restoreInProgress);
})

connection.on('updateRestoreVessel', data => {
    setMarkers(data);
});

function DoFilterTest() {

    isViewFilter = true;
    connection.invoke('personalizeFilter', '4');
    console.log('personalizeFilter');
    console.log('DoFilterTest isViewFilter ' + isViewFilter);
}

//End SignalR------------------------------------------------------------