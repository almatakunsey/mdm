﻿"use strict";

function getSpeedViolationReportByMinMaxSpeed(reportMinSpeed, reportMaxSpeed) {
    return dataRequest("GET", "/api/vessel/GetSpeedViolationReportById?minSpeed=" + reportMinSpeed +
        "&maxSpeed=" + reportMaxSpeed);
}

function getAllReports(url) {
    return dataRequest("GET", url + "/api/services/app/ReportService/Get");
}

function getAllReportTypes(url) {
    return dataRequest("GET", url + "/api/services/app/ReportTypeService/Get");
}

function getAllTimeSpanTypes(url) {
    return dataRequest("GET", url + "/api/services/app/ReportService/GetTimeSpanType");
}

function deleteReportById(url, id) {
    return dataRequest("DELETE", url + "/api/services/app/ReportService/Delete?id=" + id);
}