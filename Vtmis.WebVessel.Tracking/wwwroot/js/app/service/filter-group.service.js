﻿"use strict";

function getAllFilterGroups(url){
    return dataRequest("GET", url + "/api/services/app/FilterGroupService/GetAll");
}