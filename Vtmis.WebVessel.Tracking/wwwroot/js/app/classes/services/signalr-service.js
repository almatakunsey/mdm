﻿import { TargetsService } from "./targets-service.js";

export class SignalRService {
    constructor(renderer, layer, targetsLayer) {
        this.targetsService = new TargetsService();
        this.transport = signalR.TransportType.WebSockets;
        this.connection = new signalR.HubConnection('vesselgeolocationhub');
        this.connection.start()
            .then(() => this.connection.invoke('restoreHistory'));
        this.connection.on('updatecurrentvessel', data => {
            this.targetsService.createUpdateTargets(data, renderer, layer, targetsLayer);
        });
    }
}