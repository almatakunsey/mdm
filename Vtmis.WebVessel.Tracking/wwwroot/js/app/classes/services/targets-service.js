﻿import { TargetIconService } from "./target-icon-service.js";
import { MapService } from "./map-service.js";
import {
    isTargetToolTipVisible, canvasRenderer, numberOfTargetToShowToolTip,
    targetIdSelected, map, targetsLayer
} from "../../dashboard-maps.js";
import { allTargetCollection } from "../../dashboard-maps.js";

export class TargetsService {
    constructor() {

    }

    createUpdateTarget(target, renderer) {

        let targetIconService = new TargetIconService();
        if (!allTargetCollection.hasOwnProperty(target.vesselId)) {
            let targetIcon = targetIconService.getTargetIconByMessageId(target.messageId);
            allTargetCollection[target.vesselId] = new L.marker([target.lat, target.lgt], {
                renderer: renderer,
                title: target.vesselName,
                targetId: target.vesselId, targetInfo: target, icon: new targetIcon(),
                contextmenu: true
            });
            if (this.isTargetCoordinateInBound(target.lat, target.lgt)) {
                let mapService = new MapService();
                mapService.addToLayer(allTargetCollection[target.vesselId], targetsLayer);
                this.bindToolTip(allTargetCollection[target.vesselId], target.vesselName, renderer, true);
                if (this.countNumberOfTargetsInBound() > numberOfTargetToShowToolTip) {
                    this.hideAllTargetTooltipsInBound();
                } else {
                    
                }
            }
            allTargetCollection[target.vesselId].setIconAngle(target.cog);
            allTargetCollection[target.vesselId].previousLatLngs = [];
        } else {
            allTargetCollection[target.vesselId].previousLatLngs.push();
            allTargetCollection[target.vesselId].setLatLng([target.lat, target.lgt]);
            allTargetCollection[target.vesselId].setIconAngle(target.cog);
            if (this.isTargetCoordinateInBound(target.lat, target.lgt)) {
                if (this.countNumberOfTargetsInBound() > numberOfTargetToShowToolTip) {
                    this.hideAllTargetTooltipsInBound();
                } else {
                    this.hideShowTargetToolTip(allTargetCollection[target.vesselId], true);
                }
            }
        }
    }

    createUpdateTargets(targets, renderer) {
        for (let target of targets.vesselPositions) {
            this.createUpdateTarget(target, renderer);
        }
    }

    addTargetToCollection(target) {
        allTargetCollection.push(target);
    }

    bindToolTip(target, targetName, renderer, isPermanent, direction = "top", className = "marker-label") {
        target.bindTooltip(targetName, {
            renderer: renderer, permanent: isPermanent,
            direction: direction, className: className
        });
    }

    hideShowTargetToolTip(target, isShow) {
        isShow === true ? target.openTooltip() : target.closeTooltip();
    }

    hideAllTargetTooltipsInBound() {
        let that = this;
        targetsLayer.eachLayer(function (m) {
            if (m instanceof L.Marker) {
                if (map.getBounds().contains(m.getLatLng())) {

                    that.hideShowTargetToolTip(m, false);
                }
            }
        });
    }

    showAllTargetTooltipsInBound() {
        let that = this;
        targetsLayer.eachLayer(function (m) {
            if (m instanceof L.Marker) {
                if (map.getBounds().contains(m.getLatLng())) {
                    that.hideShowTargetToolTip(m, true);
                }
            }
        });
    }

    isTargetCoordinateInBound(latitude, longitude) {
        let layerBounds = map.getBounds();
        return layerBounds.contains(new L.latLng(latitude, longitude));
    }

    countNumberOfTargetsInBound() {
        let counter = 0;
        targetsLayer.eachLayer(function (m) {
            if (m instanceof L.Marker) {
                if (map.getBounds().contains(m.getLatLng())) {
                    counter += 1;
                }
            }
        });
        //console.log(counter);
        return counter;
    }

    updateMarkersInBound() {
        var layerBounds = map.getBounds();
        var length = Object.keys(allTargetCollection).length;
        var keys = Object.keys(allTargetCollection);

        for (var i = 0; i <= (length + 1); i++) {
            var m = allTargetCollection[keys[i]];
            if (m != null) {

                var shouldBeVisible = layerBounds.contains(new L.latLng(m._latlng.lat, m._latlng.lng));
                if (m._icon && !shouldBeVisible) {
                    targetsLayer.removeLayer(m);
                } else if (!m._icon && shouldBeVisible) {
                    targetsLayer.addLayer(m);                    
                }
                if (this.countNumberOfTargetsInBound() > numberOfTargetToShowToolTip) {
                    this.hideAllTargetTooltipsInBound();
                } else {
                    this.showAllTargetTooltipsInBound();
                }
            }
        }
        //targetsLayer.eachLayer(function (m) {
        //    if (m instanceof L.Marker) {
        //        var shouldBeVisible = map.getBounds().contains(m.getLatLng());
        //        if (m._icon && !shouldBeVisible) {
        //            targetsLayer.removeLayer(m);
        //        } else if (!m._icon && shouldBeVisible) {
        //            targetsLayer.addLayer(m);
        //            //this.bindToolTip(m, m.options.targetInfo.vesselName, canvasRenderer, true);
        //            if (this.countNumberOfTargetsInBound() > numberOfTargetToShowToolTip) {
        //                this.hideAllTargetTooltipsInBound();
        //            } else {
        //                this.bindToolTip(m, m.options.targetInfo.vesselName, canvasRenderer, true);
        //                //this.showAllTargetTooltipsInBound(layer);
        //            }
        //        }               
        //    }
        //});
        this.countNumberOfTargetsInBound();
    }
}