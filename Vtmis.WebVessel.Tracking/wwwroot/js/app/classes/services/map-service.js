﻿export class MapService {
    constructor() {
       
    }

    createOpenStreetMap(elementIdToAppend, latitude, longitude, zoomLevel, canvasRenderer) {
        let map = L.map(elementIdToAppend, {
            renderer: canvasRenderer,
            contextmenu: true }).setView([latitude, longitude], zoomLevel);

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
            preferCanvas: true
        }).addTo(map);
        return map;
    }

    addToLayer(layer, layerToAdd) {
        layer.addTo(layerToAdd);
    }

    addLayer(layer, layerToAdd) {
        layer.addLayer(layerToAdd);
    }

    removeLayer(layer) {
        layer.removeLayer(layerToAdd);
    }    
}