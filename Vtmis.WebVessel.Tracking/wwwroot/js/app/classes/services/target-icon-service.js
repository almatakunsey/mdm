﻿export class TargetIconService {
    constructor() {
        
    }

    getTargetIconByMessageId(messageId, targetFillColour = "orange", targetBorderColour = "black") {
        if (messageId == 18) {
            return this.getClassBTargetIcon(targetFillColour, targetBorderColour);
        } else if (messageId == 1 || messageId == 2
            || messageId == 3) {
            return this.getClassATargetIcon(targetFillColour, targetBorderColour);
        } else if (messageId == 9) {
            return this.getSarTargetIcon(targetFillColour, targetBorderColour);
        } else if (messageId == 21) {
            return this.getDefaultTargetIcon(targetFillColour, targetBorderColour);
        }
        else {
            return this.getDefaultTargetIcon(targetFillColour, targetBorderColour);
        }
    }

    getDefaultTargetIcon (targetFillColour = "orange", targetBorderColour = "black") {
        return L.CanvasIcon.extend({
            options: {
                iconSize: new L.Point(19, 26),
                iconAnchor: new L.Point(9.5, 9.5),
                fillStyle: targetFillColour,
                borderColour: targetBorderColour
            },
            _setIconStyles: function (icon, type) {
    
                if (type == 'icon') {
                    var ctx = icon.getContext('2d');
                    var size = L.point(this.options.iconSize);
                    var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));
                    ctx.beginPath();
                    ctx.moveTo(center.x, 3);
                    ctx.lineTo(center.x + center.x / 2, center.y);
                    ctx.lineTo(center.x + center.x / 2, center.y * 3);

                    ctx.lineTo(center.x - center.x / 2, center.y * 3);
                    ctx.lineTo(center.x - center.x / 2, center.y);
                    ctx.lineTo(center.x, 0);

                    ctx.fillStyle = this.options.fillStyle;
                    ctx.fill();

                    ctx.strokeStyle = this.options.borderColour;
                    ctx.lineWidth = 1;
                    ctx.stroke();
                    ctx.closePath();
                }
                L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
            }
        });
    }

    getClassATargetIcon(targetFillColour = "orange", targetBorderColour = "black") {
        return L.CanvasIcon.extend({
            options: {
                iconSize: new L.Point(19, 26),
                iconAnchor: new L.Point(9.5, 9.5),
                fillStyle: targetFillColour,
                borderColour: targetBorderColour
            },
            _setIconStyles: function (icon, type) {
    
                if (type == 'icon') {
                    var ctx2 = icon.getContext('2d');
                    var size = L.point(this.options.iconSize);
                    var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));
            
                    ctx2.beginPath();
                    ctx2.lineWidth = 1.5;
                    ctx2.moveTo(center.x,center.y-size.y*0.48);
                    ctx2.lineTo(center.x+size.x*0.22,center.y+size.y*0.48);
                    ctx2.lineTo(center.x-size.x*0.22,center.y+size.y*0.48);
                    ctx2.lineTo(center.x,center.y-size.y/2);
                    ctx2.lineTo(center.x + size.x * 0.22, center.y + size.y * 0.48);
                    ctx2.fillStyle = this.options.fillStyle;
                    ctx2.fill();
                    ctx2.strokeStyle = this.options.borderColour;
                    ctx2.stroke();
                    ctx2.closePath();
                }
                L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
            }
        });
    }

    getClassBTargetIcon(targetFillColour = "orange", targetBorderColour = "black") {
        return L.CanvasIcon.extend({
            options: {
                iconSize: new L.Point(19, 26),
                iconAnchor: new L.Point(9.5, 9.5),
                fillStyle: targetFillColour,
                borderColour: targetBorderColour
            },
            _setIconStyles: function (icon, type) {

                if (type == 'icon') {
                    var ctx3 = icon.getContext('2d');
                    var size = L.point(this.options.iconSize);
                    var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));

                    ctx3.beginPath();
                    ctx3.lineWidth = 2;
                    ctx3.moveTo(center.x, center.y - size.y * 0.48);
                    ctx3.lineTo(center.x + size.x * 0.22, center.y - size.y * 0.2);
                    ctx3.lineTo(center.x + size.x * 0.22, center.y + size.y * 0.48);
                    ctx3.lineTo(center.x - size.x * 0.22, center.y + size.y * 0.48);
                    ctx3.lineTo(center.x - size.x * 0.22, center.y - size.y * 0.2);
                    ctx3.lineTo(center.x, center.y - size.y * 0.48);
                    ctx3.lineTo(center.x + size.x * 0.22, center.y - size.y * 0.2);
                    ctx3.fillStyle = this.options.fillStyle;
                    ctx3.fill();
                    ctx3.strokeStyle = this.options.borderColour;
                    ctx3.stroke();
                    ctx3.closePath();
                    ctx3.beginPath();
                    ctx3.arc(center.x, center.y, size.x / 2 * 0.1, 0, 2 * Math.PI);
                    ctx3.stroke();
                    ctx3.fillStyle = "black";
                    ctx3.fill();
                    ctx3.stroke();
                    ctx3.closePath();
                }
                L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
            }
        });
    }

    getSarTargetIcon(targetFillColour = "orange", targetBorderColour = "black") {
        return L.CanvasIcon.extend({
            options: {
                iconSize: new L.Point(19, 26),
                iconAnchor: new L.Point(9.5, 9.5),
                fillStyle: targetFillColour,
                borderColour: targetBorderColour
            },
            _setIconStyles: function (icon, type) {

                if (type == 'icon') {
                    var ctx4 = icon.getContext('2d');
                    var size = L.point(this.options.iconSize);
                    var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));
                    ctx4.beginPath();
                    ctx4.lineWidth = 2;
                    for (var i = 1 * Math.PI; i < 2 * Math.PI; i += 0.01) {
                        var xPos = center.x - (size.x * 0.11 * Math.sin(i)) * Math.sin(0 * Math.PI) + (size.x * 0.11 * Math.cos(i)) * Math.cos(0 * Math.PI);
                       var yPos = center.y + (size.y * 0.32 * Math.cos(i)) * Math.sin(0 * Math.PI) + (size.y * 0.32 * Math.sin(i)) * Math.cos(0 * Math.PI) - size.y * 0.17;

                        if (i == 0) {
                            ctx4.moveTo(xPos, yPos);
                        } else {
                            ctx4.lineTo(xPos, yPos);
                        }
                    }

                    ctx4.lineTo(center.x + size.x * 0.45, center.y + size.y * 0.1);
                    ctx4.lineTo(center.x + size.x * 0.45, center.y + size.y * 0.23);
                    ctx4.lineTo(center.x + size.x * 0.12, center.y + size.y * 0.02);

                    ctx4.lineTo(center.x + size.x * 0.05, center.y + size.y * 0.38);
                    ctx4.lineTo(center.x + size.x * 0.10, center.y + size.y * 0.43);
                    ctx4.lineTo(center.x + size.x * 0.10, center.y + size.y * 0.5);


                    ctx4.lineTo(center.x - size.x * 0.10, center.y + size.y * 0.5);
                    ctx4.lineTo(center.x - size.x * 0.10, center.y + size.y * 0.43);
                    ctx4.lineTo(center.x - size.x * 0.05, center.y + size.y * 0.38);


                    ctx4.lineTo(center.x - size.x * 0.12, center.y + size.y * 0.02);
                    ctx4.lineTo(center.x - size.x * 0.45, center.y + size.y * 0.23);
                    ctx4.lineTo(center.x - size.x * 0.45, center.y + size.y * 0.1);
                    ctx4.lineTo(center.x - (xPos - center.x), yPos);
                    ctx4.fillStyle = this.options.fillStyle;
                    ctx4.fill();
                    ctx4.strokeStyle = this.options.borderColour;
                    ctx4.stroke();
                    ctx4.closePath();
                }
                L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
            }
        });
    }

    getArpaTargetIcon(targetFillColour = "orange", targetBorderColour = "black") {
        return L.CanvasIcon.extend({
            options: {
                iconSize: new L.Point(19, 26),
                iconAnchor: new L.Point(9.5, 9.5),
                fillStyle: targetFillColour,
                borderColour: targetBorderColour
            },
            _setIconStyles: function (icon, type) {

                if (type == 'icon') {
                    var ctx1 = icon.getContext('2d');
                    var size = L.point(this.options.iconSize);
                    var center = L.point(Math.floor(size.x / 2), Math.floor(size.y / 2));

                    ctx1.beginPath();
                    ctx1.lineWidth = 2;
                    ctx1.arc(center.x, center.y, size.x / 2 * 0.9, 0, 2 * Math.PI);
                    ctx1.strokeStyle = this.options.borderColour;
                    ctx1.stroke();
                    ctx1.fillStyle = this.options.fillStyle;
                    ctx1.fill();
                    ctx1.closePath();
                    ctx1.beginPath();
                    ctx1.arc(center.x, center.y, size.x / 2 * 0.1, 0, 2 * Math.PI);
                    ctx1.stroke();
                    ctx1.fillStyle = "black";
                    ctx1.fill();
                    ctx1.closePath();
                }
                L.CanvasIcon.prototype._setIconStyles.apply(this, arguments);
            }
        });
    }
}