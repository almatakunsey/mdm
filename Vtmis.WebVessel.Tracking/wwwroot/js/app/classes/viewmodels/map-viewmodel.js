﻿export class MapViewModel {
    constructor() {
        this._canvasRenderer = L.canvas();
    }    

    get canvasRenderer() {
        return this._canvasRenderer;
    }
}