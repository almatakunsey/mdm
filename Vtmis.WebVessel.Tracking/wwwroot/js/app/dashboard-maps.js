﻿import { MapService } from "./classes/services/map-service.js";
import { TargetsService } from "./classes/services/targets-service.js";
import { SignalRService } from "./classes/services/signalr-service.js";

class DashboardMaps {
    constructor() {
        
        this.listOfMarkersInCurrentView = [];
        this.targetsLayer = new L.LayerGroup({
            renderer: canvasRenderer
        });        
        this.mapService = new MapService();
        this.targetsService = new TargetsService();       
        this.map = this.mapService.createOpenStreetMap("map", 2.87995107, 101.14700317, 10, canvasRenderer);
        this.mapService.addLayer(this.map, this.targetsLayer);
        this.signalrService = new SignalRService(canvasRenderer,
            this.map, this.targetsLayer);
        
    }

    updateMarkersInBound() {
        this.targetsService.updateMarkersInBound(this.map, this.targetsLayer);
    }
}
export let allTargetCollection = [];
export let isTargetToolTipVisible = true;
export let canvasRenderer = L.canvas();
export let numberOfTargetToShowToolTip = 20;
export let targetIdSelected = 0;
let dashboardMaps = new DashboardMaps();
export let map = dashboardMaps.map;
export let targetsLayer = dashboardMaps.targetsLayer;

map.on('moveend', function () {
    dashboardMaps.updateMarkersInBound();
});
