﻿"use strict";

var startDate;
var endDate;
var startTime;
var endTime;
var playback;
var timeline;
var timelineData;
var polyline;
var decorator
var shipList = [];

function loadingScreen(startStop) {
    if (startStop == 'Start') {
        var html = '<div id="load"></div>';
        $('#loader').append(html);
    } else if (startStop == 'Stop'){
        document.getElementById('load').outerHTML = "";
    }
}

$(function () {
    $('#datetimes').dateRangePicker({
        startOfWeek: 'monday',
        separator: ' to ',
        format: 'YYYY-MM-DD HH:mm',
        autoClose: false,
        monthSelect: true,
        yearSelect: true,
        selectForward: true,
        customArrowPrevSymbol: '<i class="fa fa-arrow-circle-left"></i>',
        customArrowNextSymbol: '<i class="fa fa-arrow-circle-right"></i>',
        time: {
            enabled: true
        },
        endDate: endDate,
        defaultTime: startDate,
        defaultEndTime: endDate,
        getValue: function () {
            return $(this).val();
        }
    })
        .bind('datepicker-change', function (event, obj) {
            startDate = moment(obj.date1).format('YYYY-MM-DD HH:mm:ss');
            endDate = moment(obj.date2).format('YYYY-MM-DD HH:mm:ss');
        })

        .bind('datepicker-close', function () {
            if (startDate != undefined) {
                loadingScreen('Start');
            }
        })

        .bind('datepicker-closed', function () {
            shipList = [];

            $(".selectPlaybackShips").select2({
                data: shipList
            })
            
            console.log('Get available ships\nURL: ' + applicationApiUrl + "/api/playbacks/vessels/" + startDate);
            $.ajax({
                type: "GET",
                crossDomain: true,
                dataType: 'json',
                url: applicationApiUrl + "/api/playbacks/vessels/" + startDate,
                async: false,
                success: function (data) {
                    loadingScreen('Stop');
                    if (data != null) {
                        console.log('Get available ships SUCCESS');
                        $(".selectPlaybackShips").prop("disabled", false);
                        $.each(data.result.vesselDetails, function (index) {
                            shipList.push({
                                id: data.result.vesselDetails[index].mmsi,
                                text: data.result.vesselDetails[index].vesselName
                            });
                        });
                    }
                }
            });

            $(".selectPlaybackShips").select2({
                data: shipList
            })
        });
});

// Timeline sliding
$(document).ready(function () {
    var iconRed = L.icon({
        iconUrl: '../images/red.png',
        //iconAnchor: [-4, 30],
        iconSize: [15, 25], // size of the icon
        shadowSize: [0, 0], // size of the shadow
        shadowAnchor: [0, 0], // the same for the shadow
        popupAnchor: [0, -10]
    });

    timeline = new vis.Timeline(document.getElementById('timeline'), null, null);

    polyline = new L.polyline([]);
    decorator = L.polylineDecorator(polyline);

    // Playback options
    var playbackOptions = {
        playControl: true,
        orientIcons: true,
        marker: function (featureData) {
            return {
                icon: iconRed,
                getPopup: function (feature) {
                    return feature.properties.title;
                }
            };
        }
    };

    //  Initialize playback
    playback = new L.Playback(map, null, onPlaybackTimeChange, playbackOptions);

    $('.selectPlaybackShips').select2({ placeholder: 'Select a vessel' });
    $(".selectPlaybackShips").prop("disabled", true);
    $('.collapsible').collapsible({
        onOpen: function (el) {
            $("#timeline").show();
        }, onClose: function (el) { $("#timeline").hide(); }
    });
});

// A callback so timeline is set after changing playback time
function onPlaybackTimeChange(ms) {
    timeline.setCustomTime(new Date(ms));
};

$(".selectPlaybackShips").change(function () {
    var selectedPlaybackShips = $('#selectPlaybackShips').select2('data');
    console.log(applicationApiUrl + "/api/Playbacks?mmsi=" + selectedPlaybackShips[0].id + "&fromDateTime=" + startDate + "&toDateTime=" + endDate);
    loadingScreen('Start');
    $.ajax({
        type: "GET",
        crossDomain: true,
        dataType: 'json',
        url: applicationApiUrl + "/api/Playbacks?mmsi=" + selectedPlaybackShips[0].id + "&fromDateTime=" + startDate + "&toDateTime=" + endDate,

        contentType: "application/json",

        success: function (data) {
            loadingScreen('Stop');
            startTime = new Date(data.result.properties.time[0]);
            endTime = new Date(data.result.properties.time[data.result.properties.time.length - 1]);

            console.log(startTime);
            console.log(endTime);

            if (data.result.properties.time.length == 1) {
                alert('This vessel only has 1 data on ' + startDate + ' to ' + endDate)
            }

            var pointList = [];

            $.each(data.result.geometry.coordinates, function (index) {
                pointList.push([data.result.geometry.coordinates[index][1], data.result.geometry.coordinates[index][0]]);
            });

            window.map.removeLayer(window.polyline);
            window.map.removeLayer(window.decorator);

            polyline = new L.polyline(pointList);

            decorator = L.polylineDecorator(polyline, {
                patterns: [
                    // defines a pattern of 10px-wide dashes, repeated every 20px on the line
                    //{ offset: 12, repeat: 25, symbol: L.Symbol.dash({ pixelSize: 10, pathOptions: { weight: 2 } }) },
                    //{ offset: 0, repeat: 25, symbol: L.Symbol.dash({ pixelSize: 0, pathOptions: { color: '#f00' } }) },
                    //{ offset: '100%', repeat: 0, symbol: L.Symbol.arrowHead({ pixelSize: 15, polygon: false, pathOptions: { stroke: true } }) }

                    { offset: 25, repeat: 50, symbol: L.Symbol.arrowHead({ pixelSize: 15, pathOptions: { fillOpacity: 1, weight: 0 } }) },
                    {offset: 0, repeat: 10, symbol: L.Symbol.dash({pixelSize: 0})}
                ]
            }).addTo(map);

            var latlng = data.result.geometry.coordinates[0];
            if (latlng == null) { alert('No data available for this vessel on ' + startDate + ' to ' + endDate); }
            var lng = latlng[0];
            var lat = latlng[1];

            //center the map
            map.fitBounds(polyline.getBounds());

             //create a DataSet
            timelineData = new vis.DataSet([{ start: startTime, end: endTime, content: selectedPlaybackShips[0].text }]);

            //Set timeline options
            var timelineOptions = {
                "width": "100%",
                "height": "120px"
            };
            
            timeline.destroy()

            //Setup timeline
            timeline = new vis.Timeline(document.getElementById('timeline'), timelineData, timelineOptions);

            // Set custom time marker (blue)
            timeline.addCustomTime();

            // Set custom time marker (blue)
            timeline.setCustomTime(startTime);

            playback.setSpeed(5);

            playback.setData(data.result);

            // Set timeline time change event, so cursor is set after moving custom time (blue)
            timeline.on('timechange', onCustomTimeChange);

            function onCustomTimeChange(properties) {
                if (!playback.isPlaying()) {
                    playback.setCursor(properties.time.getTime());
                }
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
});
