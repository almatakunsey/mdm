﻿$(document).ready(function () {
    function hasValue(obj, value) {
        for (const k in obj) {
            if (obj[k].id === value) {
                return true;
            }
        }
        return false;
    }
    function emptyCellCheck(object) {
        for (var item in object) {
            var obj = object[item];
            for (var value in obj) {
                if (obj[value] === '') {
                    swal("Incomplete Data", "Please fill in all columns!", "error");
                    return false;
                }
            }
        }
        return true;
    }

    var oldData = [];
    $("#locationModal").click(function () { loadLocationData(); });

    //create Tabulator on DOM element with id "example-table"
    $("#location-table").tabulator({
        height: '100%', // set height of table, this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
        layout: "fitColumns", //fit columns to width of table (optional)
        responsiveLayout: true,
        addRowPos: "bottom",
        columns: [ //Define Table Columns
            { title: "Name", field: "Name", editor: true, validator: "required" },
            { title: "Longitude", field: "Longitude", editor: true, validator: ["required", "numeric"] },
            { title: "Latitude", field: "Latitude", editor: true, validator: ["required", "numeric"] },
            { title: "Remove", width: 62, formatter: "buttonCross", align: 'center', cellClick: function (e, cell) { deleteRow(cell.getRow().getData().id); } }
        ],
        validationFailed: function (cell, value, validators) {
            //alert(cell + "\n" + value + "\n" + validators);
        },
        dataEdited: function (data) {
            //alert(JSON.stringify(data));
        },
    });

    function loadLocationData() {
        //load list location data
        var tabledata = [];
        oldData = [];
        $.getJSON(applicationHostUrl + '/api/services/app/Location/GetListAsync', function (data) {
            $.each(data.result.items, function (index) {
                tabledata.push({
                    id: data.result.items[index].id,
                    Name: data.result.items[index].name,
                    Longitude: data.result.items[index].longitude,
                    Latitude: data.result.items[index].latitude
                });

                oldData.push({
                    id: data.result.items[index].id,
                    Name: data.result.items[index].name,
                    Longitude: data.result.items[index].longitude,
                    Latitude: data.result.items[index].latitude
                })
            });
            //load sample data into the table
            $("#location-table").tabulator("setData", tabledata);

            var value = $.ajax({
                url: applicationHostUrl + '/api/services/app/Location/GetListAsync',
                async: false
            }).responseText;

            const select = document.querySelector('.selectLocation');
            $('#selectLocation').find('option').remove();
            $.each(JSON.parse(value).result.items, function (index) {
                select.insertAdjacentHTML('beforeend', `<option data-lat="${JSON.parse(value).result.items[index].latitude}" data-lon="${JSON.parse(value).result.items[index].longitude}" value="${JSON.parse(value).result.items[index].id}">${JSON.parse(value).result.items[index].name}</option>`);
            });
        });
    }

    //On location apply button click
    $("#apply-location").click(function () {
        var selected = $("#selectLocation").find('option:selected');
        var lat = selected.data('lat');
        var lon = selected.data('lon');
        map.panTo(new L.LatLng(lon, lat));
    });

    //Add row on "Add Row" button click
    $("#add-location-row").click(function () {
        $("#location-table").tabulator("addRow", { id: Math.random(), Name: '', Longitude: '', Latitude: '' });
    });

    //Delete row on "Delete Row" button click
    function deleteRow(row) {
        $("#location-table").tabulator("deleteRow", row);
    }

    $("#save-location").click(function () {
        var data = $("#location-table").tabulator("getData");
        if (emptyCellCheck(data) === true) {
            for (var i = 0; i < oldData.length; i++) {
                // UPDATE ROW
                if (hasValue(data, oldData[i].id) === true) {
                    for (var x = 0; x < data.length; x++) {
                        var dataUpdate = 0;
                        if (oldData[i].id === data[x].id) {
                            if (oldData[i].Name !== data[x].Name) { dataUpdate++; }
                            else if (oldData[i].Longitude !== data[x].Longitude) { dataUpdate++; }
                            else if (oldData[i].Latitude !== data[x].Latitude) { dataUpdate++; }
                        }

                        if (dataUpdate > 0) {
                            var updatedData = {
                                "id": oldData[i].id,
                                "name": data[x].Name,
                                "longitude": data[x].Longitude,
                                "latitude": data[x].Latitude
                            }

                            $.ajax({
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json'
                                },
                                'type': 'PUT',
                                'url': applicationHostUrl + '/api/services/app/Location/UpdateAsync',
                                'data': JSON.stringify(updatedData),
                                'dataType': 'json',
                                error: function (error) {
                                    swal("Error!", "Cannot save data!", "error");
                                }
                            })
                        }
                    }
                }

                // DELETE ROW
                if (hasValue(data, oldData[i].id) === false) {
                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        'type': 'DELETE',
                        'url': applicationHostUrl + '/api/services/app/Location/DeleteAsync?id=' + oldData[i].id,
                        error: function (error) {
                            swal("Error!", "Cannot save data!", "error");
                        }
                    })
                }
            }

            for (var z = 0; z < data.length; z++) {
                if (hasValue(oldData, data[z].id) === false) {
                    var newLocation = {
                        "name": data[z].Name,
                        "longitude": data[z].Longitude,
                        "latitude": data[z].Latitude
                    }
                    $.ajax({
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + $.cookie("token")
                        },
                        'type': 'POST',
                        'url': applicationHostUrl + '/api/services/app/Location/CreateAsync',
                        'data': JSON.stringify(newLocation),
                        'dataType': 'json',
                        error: function (error) {
                            swal("Error!", "Cannot save data!", "error");
                        }
                    })
                }
            }
            $('#myModal').modal('hide');
            swal("Good job!", "Data successfully saved!", "success").then(() => { loadLocationData(); $('#myModal').modal('show'); });
        }
    });

    //trigger download of data.csv file
    $("#download-location-csv").click(function () {
        $("#location-table").tabulator("download", "csv", "Location.csv");
    });
});