
$(function() {
    // Get start/end times
    console.log('ready');

   
    var startTime;
    var endTime;
    var timeline;

    // Create a DataSet with data
    

    // Setup leaflet map
    var map = new L.Map('map');

    L.easyButton( 'fa-gbp', function(){
      
       $.ajax({
        type: "GET",
        crossDomain: true,
        dataType: 'json',
        url: "http://192.168.0.102/api/playbacks?url=192.168.0.102:8081/api/playbacks&mmsi=563007280&fromDateTime=2017-08-30 08:00:00&toDateTime=2017-08-31 09:02:00",

        contentType: "application/json",

        success: function (data) {
            var iconRed = L.icon({
                iconUrl: 'images/red.png',
                iconSize: [15,25], // size of the icon
                shadowSize: [0, 0], // size of the shadow
                iconAnchor: [7.5, 12.5], // point of the icon which will correspond to marker's location
                shadowAnchor: [0, 0], // the same for the shadow
                popupAnchor: [0, -10] 
            });

            startTime = new Date(data.result.properties.time[0]);
            endTime = new Date(data.result.properties.time[data.result.properties.time.length - 1]);

            console.log(startTime);
            console.log(endTime);

            var latlng = data.result.geometry.coordinates[0];
            var lng = latlng[0];
            var lat = latlng[1];

            map.setView([lat, lng], 10);

            var timelineData = new vis.DataSet([{ start: startTime, end: endTime, content: 'VTMIS playback' }]);

            // Set timeline options
            var timelineOptions = {
                "width": "100%",
                "height": "120px",
                "style": "box",
                "axisOnTop": true,
                "showCustomTime": true
            };

            // Setup timeline
            timeline = new vis.Timeline(document.getElementById('timeline'), timelineData, timelineOptions);

            // Set custom time marker (blue)
            timeline.setCustomTime(startTime);

            // =====================================================
            // =============== Playback ============================
            // =====================================================

            // Playback options
            var playbackOptions = {

                playControl: true,
                dateControl: true,
                orientIcons: true,
                // layer and marker options
                
               
                    marker: function (featureData) {
                        return {
                            icon: iconRed,
                            getPopup: function (feature) {
                                return feature.properties.title;
                            }
                        };
                    }



            };

            // Initialize playback
            var playback = new L.Playback(map, null, onPlaybackTimeChange, playbackOptions);

            playback.setData(data.result);
            //playback.addData(data.result);

            // Uncomment to test data reset;
            //playback.setData(blueMountain);    

            // Set timeline time change event, so cursor is set after moving custom time (blue)
            timeline.on('timechange', onCustomTimeChange);

            // A callback so timeline is set after changing playback time
            function onPlaybackTimeChange(ms) {
                timeline.setCustomTime(new Date(ms));
            };

            // 
            function onCustomTimeChange(properties) {
                if (!playback.isPlaying()) {
                    playback.setCursor(properties.time.getTime());
                }
            }       
        },
        failure: function (response) {
            alert(response.d);
        }
    });

    }).addTo(map);

   

    var basemapLayer = new L.TileLayer('http://{s}.tiles.mapbox.com/v3/github.map-xgq2svrz/{z}/{x}/{y}.png');

    // Center map and default zoom level
    map.setView([1.369715, 104.047755], 10);

    // Adds the background layer to the map
    map.addLayer(basemapLayer);

    
});
