const mix = require('laravel-mix');
const Dotenv = require('dotenv-webpack');

mix.setPublicPath('wwwroot/');

mix.webpackConfig({
    node: {
          fs: "empty"
    },
    output: {
        chunkFilename: 'js/chunks/[name].js?chunkId=[chunkhash]',
    },
    plugins: [
        new Dotenv({
            path: `wwwroot/.env.${process.env.NODE_ENV}`
        })
    ],    
    module: {
        rules: [
            {
                // only include svg that doesn't have font in the path or file name by using negative lookahead
                test: /(\.(png|jpe?g|gif)$|^((?!font).)*\.svg$)/,
                loaders: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: path => {
                                if (!/node_modules|bower_components/.test(path)) {
                                    console.log(Config.fileLoaderDirs.images)
                                    return (
                                        Config.fileLoaderDirs.images +
                                        '/bundle' +
                                        '/[name].[ext]?[hash]'
                                    );
                                }

                                return (
                                    Config.fileLoaderDirs.images +
                                    '/vendor/' +
                                    path
                                        .replace(/\\/g, '/')
                                        .replace(
                                            /((.*(node_modules|bower_components))|images|image|img|assets)\//g,
                                            ''
                                        ) +
                                    '?[hash]'
                                );
                            },
                            publicPath: Config.resourceRoot
                        }
                    },

                    {
                        loader: 'img-loader',
                        options: Config.imgLoaderOptions
                    },


                ]
            },
            {
                test: /\.worker\.js$/,
                use: { loader: 'worker-loader?name=./worker/[hash].worker.js' }                
            }
        ]
    }
});

mix.js('resources/assets/js/main.js', 'js/main.js')

// Theme related assets
// mix.sass('resources/assets/sass/app.scss', 'wwwroot/themes/inspinia/css');
// mix.copy('resources/assets/vendor/bootstrap/fonts', 'wwwroot/themes/inspinia/fonts');
// mix.copy('resources/assets/vendor/font-awesome/fonts', 'wwwroot/themes/inspinia/fonts');
// mix.copy('resources/assets/vendor/DataTables/datatables.min.js', 'wwwroot/dist/js')
// mix.copy('resources/assets/vendor/DataTables/datatables.min.css', 'wwwroot/dist/css')

// temporary, signal to prove bottleneck
// mix.copy('node_modules/@aspnet/signalr/dist/browser/signalr.js', 'wwwroot/themes/inspinia/js')

// mix.styles([
//     'resources/assets/vendor/bootstrap/css/bootstrap.css',
//     'resources/assets/vendor/animate/animate.css',
//     'resources/assets/vendor/font-awesome/css/font-awesome.css',    
// ], 'wwwroot/themes/inspinia/css/vendor.css', './');

// mix.scripts([            
//     // 'resources/assets/vendor/bootstrap/js/bootstrap.js',   
//     // 'resources/assets/vendor/metisMenu/jquery.metisMenu.js',     
//     // 'resources/assets/vendor/slimscroll/jquery.slimscroll.min.js',    
//     // 'resources/assets/vendor/pace/pace.min.js',     
//     'node_modules/@aspnet/signalr/dist/browser/signalr.min.js',    
//     'node_modules/msgpack5/dist/msgpack5.min.js',
//     'node_modules/@aspnet/signalr-protocol-msgpack/dist/browser/signalr-protocol-msgpack.min.js',    
//     // 'resources/assets/js/app.js'
// ], 'wwwroot/dist/js/signalr.js', './'); 