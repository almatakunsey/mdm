import '../vendor/bootstrap/css/bootstrap.css'
import '../vendor/animate/animate.css'
import '../vendor/font-awesome/css/font-awesome.css'    
import '../sass/app.scss'

import '../vendor/bootstrap/js/bootstrap.js'   
import '../vendor/metisMenu/jquery.metisMenu.js'     
import '../vendor/slimscroll/jquery.slimscroll.min.js'    
import '../vendor/pace/pace.min.js'     
import '../js/app.js'