export const fields = [
	{
		name: 'hide',
		title:'Hide',
		sortField: 'hide'
	},
	{
		name: 'time',
		title:'Time',
		sortField: 'time'
	},
	{
		name: 'priority',
		title:'Priority',
		sortField: 'priority'
	},
	{
		name: 'condition',
		title:'Condition',
		sortField: 'condition'
	},
	{
		name: 'message',
		title:'Msg',
		sortField: 'message'
	},
	{
		name: 'zone',
		title:'Zone',
		sortField: 'zone'
	},
	{
		name: 'mmsi',
		title:'MMSI',
		sortField: 'mmsi'
	},
	{
		name: 'alarmName',
		title:'Name',
		sortField: 'alarmName'
	},
	{
		name: 'callSign',
		title:'Call Sign',
		sortField: 'callSign'
	},
	{
		name: 'latitude',
		title:'Latitude',
		sortField: 'latitude'
	},
	{
		name: 'longitude',
		title:'Longitude',
		sortField: 'longitude'
	},
	{
		name: 'info',
		title:'Info',
		sortField: 'info'
	}
]