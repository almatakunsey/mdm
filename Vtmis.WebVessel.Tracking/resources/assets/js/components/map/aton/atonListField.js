export const fields = {
	aton : [
		{
			name:'mmsi', 
			title:'MMSI',
			sortField: 'mmsi',
			active:false
		},
		{
			name:'aisName', 
			title:'AIS Name',
			sortField: 'aisName',
			active:false
		},
		{
			name:'latitude', 
			title:'Latitude',
			sortField: 'latitude',
			active:false
		},
		{
			name:'longitude', 
			title:'Longitude',
			sortField: 'longitude',
			active:false
		},
		{
			name:'type', 
			title:'AtoN Type',
			sortField: 'type',
			active:true
		},
		{
			name:'virtualAton', 
			title:'Virtual AtoN',
			sortField: 'virtualAton',
			active:true
		},
		{
			name:'offPosition68', 
			title:'Off-Position 6/8',
			sortField: 'offPosition68',
			active:true
		},
		{
			name:'offPosition21', 
			title:'Off-Position 21',
			sortField: 'offPosition21',
			active:true
		},
		{
			name:'lastReport6', 
			title:'Last Report 6',
			sortField: 'lastReport6',
			active:true
		},
		{
			name:'lastReport8', 
			title:'Last Report 8',
			sortField: 'lastReport8',
			active:true
		},
		{
			name:'lastReport21', 
			title:'Last Report 21',
			sortField: 'lastReport21',
			active:true
		}

	],
	meteo: [
		{
			title: 'MMSI',
			name: 'mmsi',
			sortField: 'mmsi',
			active:true
		},
		{
			title: 'AIS Name',
			name: 'aisName',
			sortField: 'aisName',
			active:true
		},
		{
			title: 'Name',
			name: 'name',
			sortField: 'name',
			active:true
		},
		{
			title: 'Latitude',
			name: 'latitude',
			sortField: 'latitude',
			active:true
		},
		{
			title: 'Longitude',
			name: 'longitude',
			sortField: 'longitude',
			active:true
		},
		{
			title: 'AtoN Type',
			name: 'type',
			sortField: 'type',
			active:true
		},
		{
			title: 'Virtual AtoN',
			name: 'virtualAton',
			sortField: 'virtualAton',
			active:true
		},
		{
			title: 'Off-Position 6/8',
			name: 'offPosition68',
			sortField: 'offPosition68',
			active:true
		},
		{
			title: 'Off-Position 21',
			name: 'offPosition21',
			sortField: 'offPosition21',
			active:true
		},
		{
			title: 'Air Temperature',
			name: 'airTemp',
			sortField: 'airTemp',
			active:true
		},
		{
			title: 'Air Pressure',
			name: 'airPres',
			sortField: 'airPres',
			active:true
		},
		{
			title: 'Wind Speed',
			name: 'wndSpeed',
			sortField: 'wndSpeed',
			active:true
		},
		{
			title: 'Wind Gust',
			name: 'wndGust',
			sortField: 'wndGust',
			active:true
		},
		{
			title: 'Wind Direction',
			name: 'wndDir',
			sortField: 'wndDir',
			active:true
		},
		{
			title: 'Last Report 6',
			name: 'lastReport6',
			sortField: 'lastReport6',
			active:true
		},
		{
			title: 'Last Report 8',
			name: 'lastReport8',
			sortField: 'lastReport8',
			active:true
		},
		{
			title: 'Last Report 21',
			name: 'lastReport21',
			sortField: 'lastReport21',
			active:true
		}
	],
	monitoring: [
		{
			title:'MMSI',
			name:'mmsi',
			sortField:'mmsi',
			active:true
		},
		{
			title:'AIS Name',
			name:'aisName',
			sortField:'aisName',
			active:true
		},
		{
			title:'Name',
			name:'name',
			sortField:'name',
			active:true
		},
		{
			title:'Latitude',
			name:'latitude',
			sortField:'latitude',
			active:true
		},
		{
			title:'Longitude',
			name:'longitude',
			sortField:'longitude',
			active:true
		},
		{
			title:'AtoN Type',
			name:'type',
			sortField:'type',
			active:true
		},
		{
			title:'Virtual AtoN',
			name:'virtualAton',
			sortField:'virtualAton',
			active:true
		},
		{
			title:'Off-Position 6/8',
			name:'offPosition68',
			sortField:'offPosition68',
			active:true
		},
		{
			title:'Off-Position 21',
			name:'offPosition21',
			sortField:'offPosition21',
			active:true
		},
		{
			title:'RACON',
			name:'racon',
			sortField:'racon',
			active:true
		},
		{
			title:'RACON 21',
			name:'racoN21',
			sortField:'racoN21',
			active:true
		},
		{
			title:'Light',
			name:'light',
			sortField:'light',
			active:true
		},
		{
			title:'Light 21',
			name:'light21',
			sortField:'light21',
			active:true
		},
		{
			title:'Health',
			name:'health',
			sortField:'health',
			active:true
		},
		{
			title:'Health 21',
			name:'health21',
			sortField:'health21',
			active:true
		},
		{
			title:'Analogue Int.',
			name:'analogueInt',
			sortField:'analogueInt',
			active:true
		},
		{
			title:'Analogue Ext.1',
			name:'analogueExt1',
			sortField:'analogueExt1',
			active:true
		},
		{
			title:'Analogue Ext.2',
			name:'analogueExt2',
			sortField:'analogueExt2',
			active:true
		},
		{
			title:'7',
			name:'b7',
			sortField:'b7',
			active:true
		},
		{
			title:'6',
			name:'b6',
			sortField:'b6',
			active:true
		},
		{
			title:'5',
			name:'b5',
			sortField:'b5',
			active:true
		},
		{
			title:'4',
			name:'b4',
			sortField:'b4',
			active:true
		},
		{
			title:'3',
			name:'b3',
			sortField:'b3',
			active:true
		},
		{
			title:'2',
			name:'b2',
			sortField:'b2',
			active:true
		},
		{
			title:'1',
			name:'b1',
			sortField:'b1',
			active:true
		},
		{
			title:'0',
			name:'b0',
			sortField:'b0',
			active:true
		},
		{
			title:'Voltage Data',
			name:'voltageData',
			sortField:'voltageData',
			active:true
		},
		{
			title:'Current Data',
			name:'currentData',
			sortField:'currentData',
			active:true
		},
		{
			title:'Battery',
			name:'battery',
			sortField:'battery',
			active:true
		},
		{
			title:'Photocell',
			name:'photoCell',
			sortField:'photoCell',
			active:true
		},
		{
			title:'Last Report 6',
			name:'lastReport6',
			sortField:'lastReport6',
			active:true
		},
		{
			title:'Last Report 8',
			name:'lastReport8',
			sortField:'lastReport8',
			active:true
		},
		{
			title:'Last Report 21',
			name:'lastReport21',
			sortField:'lastReport21',
			active:true
		}
	]

}




