import SignalREventsMixin from './SignalREventsMixin'
import MainMapMixin from './MainMap'   
import ComponentControlMixin from './ComponentControlMixin' 
import ModeHandlerMixin from './Modes/ModeHandler' 

export default [
	SignalREventsMixin,
    MainMapMixin,
    ComponentControlMixin,
    ModeHandlerMixin
]