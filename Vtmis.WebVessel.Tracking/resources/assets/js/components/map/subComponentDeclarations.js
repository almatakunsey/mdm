export default (function(){
	 return {
        'map-layers' : ()=> import('./MapLayers'),
        'map-multi-components': ()=> import('./MapMultiComponents'),        
        'v-canvas-layer': ()=> import('@skinnyjames/vue2-leaflet-canvas'),        
        'vessel-details': ()=> import('./VesselDetail/VesselDetails'),
        'advanced-search-form': ()=> import('./search/AdvancedSearchForm'),
        'advanced-search-result':()=> import('./search/AdvancedSearchResult'),        
        'playback': ()=> import('./playback/PlaybackForm'),
        'wms-layers': ()=> import('./mapControl/enc/WMSLayers'),            

        
        // 'user-mainpane': ()=> import('./users/MainPane'),
                
        // bottompane
        'statusbar-bottompane' : ()=>import('./Status/StatusBar/BottomPane'),
        'app-notification': ()=> import('../common/AppNotification'),
        'alarm-notification': ()=> import('../common/AlarmNotification')

      }
})()