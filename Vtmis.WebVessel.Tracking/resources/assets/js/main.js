
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./asset')

// Vue init
window.Vue = require('vue');
Vue.config.productionTip = false

// Sessions
import VueSession from 'dwy-vue-session'
Vue.use(VueSession,{
  maxAge:1000*60*60*24
})


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import router from './router'
import { store } from './stores'

import './hooks'

// layouts
Vue.component('default-layout', ()=> import('./components/layouts/HomeLayout.vue'));
Vue.component('main-layout', ()=> import('./components/layouts/MainLayout.vue'));
Vue.component('admin-layout', ()=> import('./components/layouts/AdminLayout.vue'));
Vue.component('blank-layout', ()=> import('./components/layouts/BlankLayout.vue'));
Vue.component('App', ()=> import('./App.vue'));

window.vm = new Vue({
    el: '.app',
    router,
    store
});
