const random = require('random-string-generator');
const ipaddrJs = require('ipaddr.js');

function getNetworkType() {

    let networkType = ''
    const hostname = location.hostname

    try {
        networkType = ipaddrJs.parse(hostname).range()
    }
    catch (e) {
        if (e.message == 'ipaddr: the address has neither IPv6 nor IPv4 format') {
            if (hostname == "mdm.enav.my" || hostname == "staging.enav.my") { networkType == 'public' }
        }
    }

    return networkType
}

export default {
    // expose internal function
    getNetworkType,

	/*
	getWebAdminApiPort
	 */
    getWebAdminApiPort() {

        const networkType = getNetworkType()

        if (process.env.NODE_ENV == 'production' || process.env.NODE_ENV == 'productionraw') {
            return 50837
            //if(networkType == 'private'){
            //	return 50837
            //}
            //else{
            //	return 60837 // will be revised
            //}
        }

        else if (process.env.NODE_ENV == 'staging') {
            return 5022
            //if(networkType == 'private'){
            //	return 50837
            //}
            //else{
            //	return 60837
            //}
        }

        else {
            return 5012 // 5042
        }
    },
	/*
	getMapTrackingApiPort
	 */
    getMapTrackingApiPort() {

        const networkType = getNetworkType()

        if (process.env.NODE_ENV == 'production' || process.env.NODE_ENV == 'productionraw') {
            return 8081
            //if(networkType == 'private'){
            //	return 8081
            //}
            //else{
            //	return 9081 // will be revised
            //}
        }

        else if (process.env.NODE_ENV == 'staging') {
            return 5024
            //if(networkType == 'private'){
            //	return 8081
            //}
            //else{
            //	return 9081 // will be revised
            //}
        }

        else {
            return 5014 // 5044
        }
    },
	/*
	getMapPortalApiPort
	 */
    getMapPortalApiPort() {

        const networkType = getNetworkType()

        if (process.env.NODE_ENV == 'production' || process.env.NODE_ENV == 'productionraw') {
            return ''
            //if(networkType == 'private'){
            //	return ''
            //}
            //else{
            //	return 5002 // will be revised
            //}

        }
        else if (process.env.NODE_ENV == 'staging') {
            return 5021
            //if(networkType == 'private'){
            //	return ''
            //}
            //else{
            //	return 5002
            //}
        }
        else { 
            return 5011 // 5041           
            // if (networkType == 'private') {
            //     return ''
            // }
            // else {
            //     return 5011
            // }
        }
    },

	/*
	getProtocol
	 */
    getProtocol() {
        //TODO: SSLConfig
        return 'http'
        //if (process.env.NODE_ENV == 'staging') {
        //    return 'https'
        //}
        //else {
        //    return 'http'
        //}
    },

    getProcessEnv(){
        if (process.env.NODE_ENV == 'production' || process.env.NODE_ENV == 'productionraw') {
            return 'production'
            
        }
        else if (process.env.NODE_ENV == 'staging') {
            return 'staging'
            
        }
        else { 
            return 'dev'           
            
        }
    },

	/*
	isArrayObjectEqual
	 */
    isArrayObjectEqual(arr1, arr2) {
        const arr1length = arr1.length
        const arr2length = arr2.length

        let isEqual = true

        for (var i = 0; i < arr1length; i++) {

            let found = false

            for (var j = 0; j < arr2length; j++) {

                if (_.isEqual(arr1[i], arr2[j])) {
                    found = true
                    break;
                }
            }

            if (found == false) {
                isEqual = false;
                break;
            }
        }

        return isEqual;
    },

	/*
	getRndInteger
	 */
    getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    /*
	zip
	 */
    zip: (arr, ...arrs) => {
        return arr.map((val, i) => arrs.reduce((a, arr) => [...a, arr[i]], [val]));
    },
	/*
	genRandom
	 */
    genRandom: (length) => {
        const options = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890123456789012345678901234567890123456789'
        return random(length, options)
    },
	/*
	objectHasEmptyValue
	 */
    objectHasEmptyValue: (obj) => {
        let isEmpty = false

        for (let key of Object.keys(obj)) {
            if (obj.hasOwnProperty(key)) {

                // whitelist
                if (obj[key] == 0) {
                    continue;
                }

                if (typeof obj[key] == 'undefined' ||
                    !obj[key] ||
                    obj[key].length === 0 ||
                    obj[key] === "" ||
                    !/[^\s]/.test(obj[key]) ||
                    /^\s*$/.test(obj[key]) ||
                    (typeof obj[key] == 'string' && obj[key].replace(/\s/g, "") === "")
                ) {
                    isEmpty = true;
                    break;
                }

            }
        }

        return isEmpty
    }
}

