export default class EmptyResponseResultError extends Error {
	constructor(message){
		super(message)
		this.code = 602
		this.constructor = EmptyResponseResultError 
        this.__proto__   = EmptyResponseResultError.prototype
	}	
}