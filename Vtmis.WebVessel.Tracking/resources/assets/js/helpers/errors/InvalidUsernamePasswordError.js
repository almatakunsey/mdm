export default class InvalidUsernamePasswordError extends Error {
	constructor(message, response=''){
		super(message)
		this.code = 603
		this.response = response
		this.constructor = InvalidUsernamePasswordError 
        this.__proto__   = InvalidUsernamePasswordError.prototype
	}	
}