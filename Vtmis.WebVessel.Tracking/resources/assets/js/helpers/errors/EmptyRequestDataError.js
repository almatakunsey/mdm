export default class EmptyRequestDataError extends Error {
	constructor(message){
		super(message)
		this.code = 601
		this.constructor = EmptyRequestDataError 
        this.__proto__   = EmptyRequestDataError.prototype
	}	
}