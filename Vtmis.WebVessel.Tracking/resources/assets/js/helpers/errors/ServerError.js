export default class ServerError extends Error {
	constructor(message, response='', code){
		super(message)
		this.code = code
		this.response = response
		this.constructor = ServerError 
        this.__proto__   = ServerError.prototype

	}
	
}