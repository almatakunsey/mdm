export default class InvalidTenantError extends Error {
	constructor(message, response=''){
		super(message)
		this.code = 604
		this.response = response
		this.constructor = InvalidTenantError 
        this.__proto__   = InvalidTenantError.prototype
	}	
}