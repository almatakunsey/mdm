var store = require("store")

var HostPersonalizationStorage = function() {	
	
	this.colorSchemePristine = {
		host: ''
	}

	
	this.storePersonalizations = (data)=>{

		try{
			const {personalizations: persona} = data
			
			const colorScheme = _.find(persona, {"key":"colorScheme"})
			
			const localPersona = {				
				colorScheme: colorScheme ? JSON.parse(colorScheme.value): _.cloneDeep(this.colorSchemePristine)				
			}			

			store.set('personalizations.host', localPersona)			
										
		}
		catch(error){
			console.log(error)	
		}
			
	}
	
	this.getHostColorScheme = ()=>{ return store.get('personalizations.host').colorScheme.host }
	
}

export default (function(){
	return new HostPersonalizationStorage()
})()