var store = require("store")

var UserPersonalizationStorage = function() {

	this.appPristine = {
		activeModule: 'monitoring'
	}

	this.mapViewPristine = {		
		zoom: 6,
		location: {
			lat: "4.61",
			lng: "108.26"
		}			
	}

	this.selectedVesselPristine = {
		vesselId: ''
	}

	this.selectedFilterPristine = {
		selectedFilter: null,
		query: null
	}

	this.vesselListToolbarPristine = {
		vesselNameEnabled: false,
		vesselTrackEnabled: false,
		balloonInfoEnabled: false,
		vesselPinEnabled: false,
		vesselSelectedEnabled: false
	}

	this.colorSchemePristine = {
		user: ''
	}

	this.tableSchemaPristine = {
		aton: '',
		meteo: '',
		monitoring: ''
	}


	this.storePersonalizations = (data)=>{
		

		try{
			const {personalizations: persona} = data

			const app = _.find(persona, {"key":"app"})
			const mapView = _.find(persona, {"key":"mapView"})
			const selectedFilter = _.find(persona, {"key":"selectedFilter"})
			const selectedVessel = _.find(persona, {"key":"selectedVessel"})
			const vesselListToolbar = _.find(persona, {"key":"vesselListToolbar"})
			const colorScheme = _.find(persona, {"key":"colorScheme"})
			const table = _.find(persona, {"key":"table"})

			const localPersona = {
				app : app ? JSON.parse(app.value) : _.cloneDeep(this.appPristine),				
				mapView : mapView ? JSON.parse(mapView.value) : _.cloneDeep(this.mapViewPristine),				
				selectedFilter : selectedFilter ? JSON.parse(selectedFilter.value) : _.cloneDeep(this.selectedFilterPristine),
				selectedVessel : selectedVessel ? JSON.parse(selectedVessel.value) : _.cloneDeep(this.selectedVesselPristine),
				vesselListToolbar : vesselListToolbar ? JSON.parse(vesselListToolbar.value) : _.cloneDeep(this.vesselListToolbarPristine),
				colorScheme: colorScheme ? JSON.parse(colorScheme.value): _.cloneDeep(this.colorSchemePristine),
				table: table ? JSON.parse(table.value) : _.cloneDeep(this.tableSchemaPristine)
			}			

			store.set('personalizations.user', localPersona)			
										
		}
		catch(error){
			console.log(error)	
		}
			
	}


	this.getActiveModule = ()=>{return store.get('personalizations.user').app.activeModule }
	this.getZoomLevel = ()=>{ return store.get('personalizations.user').mapView.zoom }
	this.getLocation = ()=>{ return store.get('personalizations.user').mapView.location }
	this.getSelectedFilter = ()=>{ return store.get('personalizations.user').selectedFilter }
	this.getSelectedVessel = ()=>{ return store.get('personalizations.user').selectedVessel }	
	this.isVesselNameEnabled = ()=>{ return store.get('personalizations.user').vesselListToolbar.vesselNameEnabled }
	this.isVesselTrackEnabled = ()=>{ return store.get('personalizations.user').vesselListToolbar.vesselTrackEnabled }
	this.isBalloonInfoEnabled = ()=>{ return store.get('personalizations.user').vesselListToolbar.balloonInfoEnabled }	
	this.isVesselPinEnabled = ()=>{ return store.get('personalizations.user').vesselListToolbar.vesselPinEnabled }	
	this.isVesselSelectedEnabled =()=>{ return store.get('personalizations.user').vesselListToolbar.vesselSelectedEnabled }	
	this.getUserColorScheme = ()=>{ return store.get('personalizations.user').colorScheme.user }
	this.getTableFields = (name)=> { return store.get('personalizations.user').table[name].fields }
	this.getTableOrders = (name)=> { return store.get('personalizations.user').table[name].orders }
}

export default (function(){
	return new UserPersonalizationStorage()
})()