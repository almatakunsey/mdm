export default {
	getShipType: (ShipType)=>{	


        	let type = ''

        	switch(true) {
                case ((ShipType >= 10) && (ShipType <= 19)):
                {
                    type = "Reserved";
                }
                break;
                
                case ((ShipType >= 20) && (ShipType <= 28)):
                {
                    type = "Wing In Ground";
                }
                break;
                
                case (ShipType == 29):
                {
                    type = "SAR Aircraft";
                }
                break;
                
                case (ShipType == 30):
                {
                    type = "Fishing";
                }
                break;
                
                case (ShipType == 31):
                {
                    type = "Tug";
                }
                break;
                
                case (ShipType == 32):
                {
                    type = "Tug";
                }
                break;
                
                case (ShipType == 33):
                {
                    type = "Dredger";
                }
                break;
                
                case (ShipType == 34):
                {
                    type = "Dive Vessel";
                }
                break;
                
                case (ShipType == 35):
                {
                    type = "Military Ops";
                }
                break;
                
                case (ShipType == 36):
                {
                    type = "Sailing Vessel";
                }
                break;
                
                case (ShipType == 37):
                {
                    type = "Pleasure Craft";
                }
                break;
                
                case (ShipType == 38):
                {
                    type = "Reserved";
                }
                break;
                
                case (ShipType == 39):
                {
                    type = "Reserved";
                }
                break;
                
                case ((ShipType >= 40) && (ShipType <= 49)):
                {
                    type = "High-Speed Craft";
                }
                break;
                
                case (ShipType == 50):
                {
                    type = "Pilot Vessel";
                }
                break;
                
                case (ShipType == 51):
                {
                    type = "SAR";
                }
                break;
                
                case (ShipType == 52):
                {
                    type = "Tug";
                }
                break;
                
                case (ShipType == 53):
                {
                    type = "Port Tender";
                }
                break;
                
                case (ShipType == 54):
                {
                    type = "Anti-Pollution";
                }
                break;
                
                case (ShipType == 55):
                {
                    type = "Law Enforce";
                }
                break;
                
                case (ShipType == 56):
                {
                    type = "Local Vessel";
                }
                break;
                
                case (ShipType == 57):
                {
                    type = "Local Vessel";
                }
                break;
                
                case (ShipType == 58):
                {
                    type = "Medical Trans";
                }
                break;
                
                case (ShipType == 59):
                {
                    type = "Special Craft";
                }
                break;
                
                case ((ShipType >= 60) && (ShipType <= 69)):
                {
                    type = "Passenger";
                }
                break;
                
                case (ShipType == 70):
                {
                    type = "Cargo";
                }
                break;
                
                case (ShipType == 71):
                {
                    type = "Cargo - Hazard A (Major)";
                }
                break;
                
                case (ShipType == 72):
                {
                    type = "Cargo - Hazard B";
                }
                break;
                
                case (ShipType == 73):
                {
                    type = "Cargo - Hazard C (Minor)";
                }
                break;
                
                case (ShipType == 74):
                {
                    type = "Cargo - Hazard D (Recognizable)";
                }
                break;
                
                case ((ShipType >= 75) && (ShipType <= 79)):
                {
                    type = "Cargo";
                }
                break;
                
                case (ShipType == 80):
                {
                    type = "Tanker";
                }
                break;
                
                case (ShipType == 81):
                {
                    type = "Tanker - Hazard A (Major)";
                }
                break;
                
                case (ShipType == 82):
                {
                    type = "Tanker - Hazard B";
                }
                break;
                
                case (ShipType == 83):
                {
                    type = "Tanker - Hazard C (Minor)";
                }
                break;
                
                case (ShipType == 84):
                {
                    type = "Tanker - Hazard D (Recognizable)";
                }
                break;
                
                case ((ShipType >= 85) && (ShipType <= 89)):
                {
                    type = "Tanker";
                }
                break;
                
                case ((ShipType >= 90) && (ShipType <= 99)):
                {
                    type = "Other";
                }
                break;
                
                case (ShipType == 100):
                {
                    type = "Navigation Aid";
                }
                break;
                
                case (ShipType == 101):
                {
                    type = "Reference Point";
                }
                break;
                
                case (ShipType == 102):
                {
                    type = "RACON";
                }
                break;
                
                case (ShipType == 103):
                {
                    type = "OffShore Structure";
                }
                break;
                
                case (ShipType == 104):
                {
                    type = "Spare";
                }
                break;
                
                case (ShipType == 105):
                {
                    type = "Light, without Sectors";
                }
                break;
                
                case (ShipType == 106):
                {
                    type = "Light, with Sectors";
                }
                break;
                
                case (ShipType == 107):
                {
                    type = "Leading Light Front";
                }
                break;
                
                case (ShipType == 108):
                {
                    type = "Leading Light Rear";
                }
                break;
                
                case (ShipType == 109):
                {
                    type = "Beacon, Cardinal N";
                }
                break;
                
                case (ShipType == 110):
                {
                    type = "Beacon, Cardinal E";
                }
                break;
                
                case (ShipType == 111):
                {
                    type = "Beacon, Cardinal S";
                }
                break;
                
                case (ShipType == 112):
                {
                    type = "Beacon, Cardinal W";
                }
                break;
                
                case (ShipType == 113):
                {
                    type = "Beacon, Port Hand";
                }
                break;
                
                case (ShipType == 114):
                {
                    type = "Beacon, Starboard Hand";
                }
                break;
                
                case (ShipType == 115):
                {
                    type = "Beacon, Preferred Channel Port hand";
                }
                break;
                
                case (ShipType == 116):
                {
                    type = "Beacon, Preferred Channel Starboard hand";
                }
                break;
                
                case (ShipType == 117):
                {
                    type = "Beacon, Isolated danger";
                }
                break;
                
                case (ShipType == 118):
                {
                    type = "Beacon, Safe Water";
                }
                break;
                
                case (ShipType == 119):
                {
                    type = "Beacon, Special Mark";
                }
                break;
                
                case (ShipType == 120):
                {
                    type = "Cardinal Mark N";
                }
                break;
                
                case (ShipType == 121):
                {
                    type = "Cardinal Mark E";
                }
                break;
                
                case (ShipType == 122):
                {
                    type = "Cardinal Mark S";
                }
                break;
                
                case (ShipType == 123):
                {
                    type = "Cardinal Mark W";
                }
                break;
                
                case (ShipType == 124):
                {
                    type = "Port Hand Mark";
                }
                break;
                
                case (ShipType == 125):
                {
                    type = "Starboard Hand Mark";
                }
                break;
                
                case (ShipType == 126):
                {
                    type = "Preferred Channel Port Hand";
                }
                break;
                
                case (ShipType == 127):
                {
                    type = "Preferred Channel Starboard Hand";
                }
                break;
                
                case (ShipType == 128):
                {
                    type = "Isolated Danger";
                }
                break;
                
                case (ShipType == 129):
                {
                    type = "Safe Water";
                }
                break;
                
                case (ShipType == 130):
                {
                    type = "Manned VTS";
                }
                break;
                
                case (ShipType == 131):
                {
                    type = "Light Vessel";
                }
                break;
                
                default:                	
                    type = 'UNKNOWN';
                    break;
            }            

            return type
	},

    getVesselClass(vessel){
        
        const ClassA = [1,2,3]
        const ClassB = [18,19,24]
        const Sar = [9]
        const Sart = [14] 
        const Aton = [21]
        const BaseStation = [4]
        const MetHydro = [8]

        if (vessel.isSart){
            return 'Sart'
        }
        else if (Sart.includes(vessel.messageId)) {   
            return 'Sart'
        }
        else if (ClassA.includes(vessel.messageId)) {   
            return 'ClassA'
        }
        else if (ClassB.includes(vessel.messageId)) {
            return 'ClassB'
        }
        else if(Sar.includes(vessel.messageId)){
            return 'Sar'
        }
        
        else if(Aton.includes(vessel.messageId) || MetHydro.includes(vessel.messageId)){
            
            if(vessel.isMethydro){
                return 'MetHydro'
            }

            else if(vessel.msg21.virtualAtonStatus > 0){
                return 'VAton'
            }

            return 'Aton'            
        }
        else if(BaseStation.includes(vessel.messageId)){            
            return 'BaseStation'
        }       
        else if(_.has(vessel, 'wreckNumber')){
            return 'Wrem'
        } 
        
        else{
            return 'Other'
        }


    },

    formatMmsi(mmsi){
        mmsi = String(mmsi).substring(0,1) == '-' ? String(mmsi).substring(1) : mmsi
        return String(mmsi).padStart(9, "0")            
    },   

    formatFlag(mmsi){

            let flag = String(mmsi);
            let res = flag.match(/201|202|203|204|205|206|207|208|209|210|211|212|213|214|215|216|218|219|220|224|225|226|227|228|229|230|231|232|233|234|235|236|237|238|239|240|241|242|243|244|245|246|247|248|249|250|251|252|253|254|255|256|257|258|259|261|262|263|264|265|266|267|268|269|270|271|272|273|274|275|276|277|278|279|301|303|304|305|306|307|308|309|310|311|312|314|316|319|321|323|325|327|329|330|331|332|334|336|338|339|341|343|345|347|348|350|351|352|353|354|355|356|357|358|359|361|362|364|366|367|368|369|370|371|372|373|374|375|376|377|378|379|401|403|405|408|410|412|413|414|416|417|419|422|423|425|428|431|432|434|436|437|438|440|441|443|445|447|450|451|453|455|457|459|461|463|466|468|470|472|473|475|477|478|501|503|506|508|510|511|512|514|515|516|518|520|523|525|529|531|533|536|538|540|542|544|546|548|553|555|557|559|561|563|564|565|566|567|570|572|574|576|577|578|601|603|605|607|608|609|610|611|612|613|615|616|617|618|619|620|621|622|624|625|626|627|629|630|631|632|633|634|635|636|637|638|642|644|645|647|649|650|654|655|656|657|659|660|661|662|663|664|665|666|667|668|669|670|671|672|674|675|676|677|678|679|701|710|720|725|730|735|740|745|750|755|760|765|770|775/);

            const flagCountryMap = {
                '201':'AL', '202':'AD', '203':'AT', '204':'PT', '205':'BE', '206':'BY', '207':'BG', '208':'VA', '209':'CY', '210':'CY',
                '211':'DE', '212':'CY', '213':'GE', '214':'MD', '215':'MT', '216':'AM', '218':'DE', '219':'DK', '220':'DK', '224':'ES',
                '225':'ES', '226':'FR', '227':'FR', '228':'FR', '229':'MT', '230':'FI', '231':'FO', '232':'GB', '233':'GB', '234':'GB',
                '235':'GB', '236':'GI', '237':'GR', '238':'HR', '239':'GR', '240':'GR', '241':'GR', '242':'MA', '243':'HU', '244':'NL',
                '245':'NL', '246':'NL', '247':'IT', '248':'MT', '249':'MT', '250':'IE', '251':'IS', '252':'LI', '253':'LU', '254':'MC',
                '255':'PT', '256':'MT', '257':'NO', '258':'NO', '259':'NO', '261':'PL', '262':'ME', '263':'PT', '264':'RO', '265':'SE',
                '266':'SE', '267':'SK', '268':'SM', '269':'CH', '270':'CZ', '271':'TR', '272':'UA', '273':'RU', '274':'MK', '275':'LV',
                '276':'EE', '277':'LT', '278':'SI', '279':'RS', '301':'AI', '303':'US', '304':'AG', '305':'AG', '306':'CW', '307':'AW',
                '308':'BS', '309':'BS', '310':'BM', '311':'BS', '312':'BZ', '314':'BB', '316':'CA', '319':'KY', '321':'CR', '323':'CU',
                '325':'DM', '327':'DO', '329':'GP', '330':'GD', '331':'GL', '332':'GT', '334':'HN', '336':'HT', '338':'US', '339':'JM',
                '341':'KN', '343':'LC', '345':'MX', '347':'MQ', '348':'MS', '350':'NI', '351':'PA', '352':'PA', '353':'PA', '354':'PA',
                '355':'PA', '356':'PA', '357':'PA', '358':'PR', '359':'SV', '361':'PM', '362':'TT', '364':'TC', '366':'US', '367':'US',
                '368':'US', '369':'US', '370':'PA', '371':'PA', '372':'PA', '373':'PA', '374':'PA', '375':'VC', '376':'VC', '377':'VC', 
                '378':'VG', '379':'VI', '401':'AF', '403':'SA', '405':'BD', '408':'BH', '410':'BT', '412':'CN', '413':'CN', '414':'CN', 
                '416':'TW', '417':'LK', '419':'IN', '422':'IR', '423':'AZ', '425':'IQ', '428':'IL', '431':'JP', '432':'JP', '434':'TM', 
                '436':'KZ', '437':'UZ', '438':'JO', '440':'KR', '441':'KR', '443':'PS', '445':'KP', '447':'KW', '450':'LB', '451':'KG', 
                '453':'MO', '455':'MV', '457':'MN', '459':'NP', '461':'OM', '463':'PK', '466':'QA', '468':'SY', '470':'AE', '472':'TJ', 
                '473':'YE', '475':'YE', '477':'HK', '478':'BA', '501':'FR', '503':'AU', '506':'MM', '508':'BN', '510':'FM', '511':'PW', 
                '512':'NZ', '514':'KH', '515':'KH', '516':'CX', '518':'CK', '520':'FJ', '523':'CC', '525':'ID', '529':'KI', '531':'LA', 
                '533':'MY', '536':'MP', '538':'MH', '540':'NC', '542':'NU', '544':'NR', '546':'PF', '548':'PH', '553':'PG', '555':'PN', 
                '557':'SB', '559':'AS', '561':'WS', '563':'SG', '564':'SG', '565':'SG', '566':'SG', '567':'TH', '570':'TO', '572':'TV', 
                '574':'VN', '576':'VU', '577':'VU', '578':'WF', '601':'ZA', '603':'AO', '605':'DZ', '607':'FR', '608':'GB', '609':'BI',
                '610':'BJ', '611':'BW', '612':'CF', '613':'CM', '615':'CG', '616':'KM', '617':'CV', '618':'FR', '619':'CI', '620':'KM', 
                '621':'DJ', '622':'EG', '624':'ET', '625':'ER', '626':'GA', '627':'GH', '629':'GM', '630':'GW', '631':'GQ', '632':'GN', 
                '633':'BF', '634':'KE', '635':'FR', '636':'LR', '637':'LR', '638':'SS', '642':'LY', '644':'LS', '645':'MU', '647':'MG', 
                '649':'ML', '650':'MZ', '654':'MR', '655':'MW', '656':'NE', '657':'NG', '659':'NA', '660':'RE', '661':'RW', '662':'SD', 
                '663':'SN', '664':'SC', '665':'SH', '666':'SO', '667':'SL', '668':'ST', '669':'SZ', '670':'TD', '671':'TG', '672':'TN', 
                '674':'TZ', '675':'UG', '676':'CD', '677':'TZ', '678':'ZM', '679':'ZW', '701':'AR', '710':'BR', '720':'BO', '725':'CL', 
                '730':'CO', '735':'EC', '740':'FK', '745':'GF', '750':'GY', '755':'PY', '760':'PE', '765':'SR', '770':'UY', '775':'VE'
            }

            const country = flagCountryMap[res]

            return country === undefined ? 'undefined' : country

        },

        getCountryCodes(){
            return ['AL', 'AD', 'AT', 'PT', 'BE', 'BY', 'BG', 'VA', 'CY', 'CY', 'DE', 'CY', 'GE', 'MD', 'MT', 'AM', 'DE', 'DK', 'DK', 'ES', 'ES', 'FR', 'FR', 'FR', 'MT', 'FI', 'FO', 'GB', 'GB', 'GB', 'GB', 'GI', 'GR', 'HR', 'GR', 'GR', 'GR', 'MA', 'HU', 'NL', 'NL', 'NL', 'IT', 'MT', 'MT', 'IE', 'IS', 'LI', 'LU', 'MC', 'PT', 'MT', 'NO', 'NO', 'NO', 'PL', 'ME', 'PT', 'RO', 'SE', 'SE', 'SK', 'SM', 'CH', 'CZ', 'TR', 'UA', 'RU', 'MK', 'LV', 'EE', 'LT', 'SI', 'RS', 'AI', 'US', 'AG', 'AG', 'CW', 'AW', 'BS', 'BS', 'BM', 'BS', 'BZ', 'BB', 'CA', 'KY', 'CR', 'CU', 'DM', 'DO', 'GP', 'GD', 'GL', 'GT', 'HN', 'HT', 'US', 'JM', 'KN', 'LC', 'MX', 'MQ', 'MS', 'NI', 'PA', 'PA', 'PA', 'PA', 'PA', 'PA', 'PA', 'PR', 'SV', 'PM', 'TT', 'TC', 'US', 'US', 'US', 'US', 'PA', 'PA', 'PA', 'PA', 'PA', 'VC', 'VC', 'VC', 'VG', 'VI', 'AF', 'SA', 'BD', 'BH', 'BT', 'CN', 'CN', 'CN', 'TW', 'LK', 'IN', 'IR', 'AZ', 'IQ', 'IL', 'JP', 'JP', 'TM', 'KZ', 'UZ', 'JO', 'KR', 'KR', 'PS', 'KP', 'KW', 'LB', 'KG', 'MO', 'MV', 'MN', 'NP', 'OM', 'PK', 'QA', 'SY', 'AE', 'TJ', 'YE', 'YE', 'HK', 'BA', 'FR', 'AU', 'MM', 'BN', 'FM', 'PW', 'NZ', 'KH', 'KH', 'CX', 'CK', 'FJ', 'CC', 'ID', 'KI', 'LA', 'MY', 'MP', 'MH', 'NC', 'NU', 'NR', 'PF', 'PH', 'PG', 'PN', 'SB', 'AS', 'WS', 'SG', 'SG', 'SG', 'SG', 'TH', 'TO', 'TV', 'VN', 'VU', 'VU', 'WF', 'ZA', 'AO', 'DZ', 'FR', 'GB', 'BI', 'BJ', 'BW', 'CF', 'CM', 'CG', 'KM', 'CV', 'FR', 'CI', 'KM', 'DJ', 'EG', 'ET', 'ER', 'GA', 'GH', 'GM', 'GW', 'GQ', 'GN', 'BF', 'KE', 'FR', 'LR', 'LR', 'SS', 'LY', 'LS', 'MU', 'MG', 'ML', 'MZ', 'MR', 'MW', 'NE', 'NG', 'NA', 'RE', 'RW', 'SD', 'SN', 'SC', 'SH', 'SO', 'SL', 'ST', 'SZ', 'TD', 'TG', 'TN', 'TZ', 'UG', 'CD', 'TZ', 'ZM', 'ZW', 'AR', 'BR', 'BO', 'CL', 'CO', 'EC', 'FK', 'GF', 'GY', 'PY', 'PE', 'SR', 'UY', 'VE', 'undefined'] 
        }

}