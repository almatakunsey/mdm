var store = require("store")

var UserPersonalizationStorage = function() {	

	this.themePristine = "theme-default"
	this.colorSchemePristine = {
		tenant: ''
	}

	
	this.storePersonalizations = (data)=>{

		try{
			const {personalizations: persona} = data

			const theme = _.find(persona, {"key":"theme"})			
			const colorScheme = _.find(persona, {"key":"colorScheme"})			
			
			const localPersona = {
				theme : theme ? theme.value : this.themePristine,
				colorScheme: colorScheme ? JSON.parse(colorScheme.value): _.cloneDeep(this.colorSchemePristine)				
			}			

			store.set('personalizations.tenant', localPersona)			
										
		}
		catch(error){
			console.log(error)	
		}
			
	}


	this.getTheme = ()=>{ 
		return store.get('personalizations.tenant').theme 
	}

	this.getTenantColorScheme = ()=>{ return store.get('personalizations.tenant').colorScheme.tenant }
	
}

export default (function(){
	return new UserPersonalizationStorage()
})()