export default {
	has(errors, field, scope=""){		

		if(!errors.items.length) { return false }

		if(scope){ return _.some(errors.items, {scope, field}) }

		return _.some(errors.items, {field})
	},

	any(errors){
		return errors.items.length
	}
}