export default {
    rotate(cx, cy, x, y, angle) {
        var radians = -((Math.PI / 180) * angle),
        cos = Math.cos(radians),
        sin = Math.sin(radians),
        nx = (cos * (x - cx)) + (sin * (y - cy)) + cx,
        ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
        return {x:nx, y:ny};
    },    
    translate(xc, yc, x, y){
        // x changes, y changes
        return { x: x + xc, y: y + yc}
    },
    endPointByDegreeLength(x1, y1, degree, knotSpeed, seconds, metresPerPixel){        
            
        function getSector(){
            if( degree <= 90 ){ return 1 }
            else if( degree <= 180 ){ return 2 }
            else if( degree <= 270 ){ return 3 }
            else{ return 4 }
        }

        function getSectorEquation(){
            switch(getSector()){
                case 1:                         
                    
                    return function(pixelLength){

                        if(degree == 0){
                            return {x: x1, y: y1 - pixelLength}
                        }

                        else if(degree == 90){
                            return {x: x1 + pixelLength, y: y1}
                        }

                        else{
                            const rad = degree * (Math.PI / 180)

                            const x = (pixelLength * Math.sin(rad)) + x1
                            const y = y1 - ((pixelLength * Math.sin(rad)) / Math.tan(rad))

                            return {x, y}    
                        }                       
                        
                        
                    }

                    break;

                case 2:                

                    return function(pixelLength){ 

                        if(degree == 180){
                            return {x: x1, y: y1 + pixelLength}
                        }

                        else{
                            const rad = (degree - 90) * (Math.PI / 180)

                            const x = ((pixelLength * Math.sin(rad)) / Math.tan(rad)) + x1
                            const y = (pixelLength * Math.sin(rad)) + y1

                            return {x, y}    
                        }                      
                        
                        
                    }

                    break;

                case 3:                        

                    return function(pixelLength){   

                        if(degree == 270){
                            return {x: x1 - pixelLength, y: y1}
                        }

                        else{
                            const rad = (degree - 180) * (Math.PI / 180)

                            const x = x1 - (pixelLength * Math.sin(rad))
                            const y = ((pixelLength * Math.sin(rad)) / Math.tan(rad)) + y1
                            
                            return {x, y}    
                        }                    
                        
                        
                        
                    }

                    break;

                case 4:                                            

                    return function(pixelLength){

                        if(degree == 360){
                            return {x:x1, y: y1 - pixelLength}
                        }

                        else{
                            const rad = (degree - 270) * (Math.PI / 180)

                            const x = x1 - ((pixelLength * Math.sin(rad)) / Math.tan(rad))
                            const y = y1 - (pixelLength * Math.sin(rad))

                            return {x, y}    
                        }
                                       
                        

                        
                    }

                    break;
            }
        }
        
        // 1 knot = 0.51444 m/s
        // m/s
        // const mps = knotSpeed * 0.51444
        // const metres = mps * seconds
        // const pixelLength = metres / metresPerPixel
        
        const pixelLength = (knotSpeed * 0.51444 * seconds) / metresPerPixel        
        
        return (getSectorEquation())(pixelLength)


    }
}