const AuthorizationError = () => import(/* webpackChunkName: "auth-error" */'../components/pages/errors/AuthorizationError')

import { store } from '../stores'

export default [	
	{
		path: '/:tenant/errors/auth-error',
		component: AuthorizationError,
		name:'errors.authorization-error',
		meta: {
			layout: 'blank'			
		}
	}
]

