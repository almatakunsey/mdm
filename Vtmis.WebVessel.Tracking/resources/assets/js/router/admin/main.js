const AdminDashboard = ()=> import(/* webpackChunkName: "main-admin" */ '../../components/admin/Dashboard.vue')

export default [
	{
		path: '/admin',
		component: AdminDashboard,
		name:'admin.dashboard',
		meta: {
			layout: 'admin',
			requiresAuth: true
		}
	}
]