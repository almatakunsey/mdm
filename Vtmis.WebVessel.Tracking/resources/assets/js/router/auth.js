const Login = ()=> import(/* webpackChunkName: "login" */'../components/auth/Login.vue')
import { store } from '../stores'

export default [
  {
  	path: '/:tenant',    
    component: Login,
	name:'login',
	meta: {
	    layout: 'default', // name of the layout
	    requiresAuth: false
	},
	beforeEnter: (to, from, next) => {
		// if(from.name){ window.location.reload }
		next()
	}	
  }
]