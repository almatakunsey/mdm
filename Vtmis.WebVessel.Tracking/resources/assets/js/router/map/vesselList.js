const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const VesselListSummary = () => import(/* webpackChunkName: "vessel-list-summary" */'../../components/map/vessel-list/VesselListSummary')

import { store } from '../../stores'

export default [
	{
		path: 'vessel-list',
		components: {								
			sidePane: SidePane	
		},		
		children: [
			{
				path:'summary',
				components: {
					sidePaneContent: VesselListSummary
				},
				name: 'map.vessel-list.summary',
				beforeEnter: (to, from, next) => {	
					store.commit('vesselList/toggleVesselListEnabled', true)					
					store.commit('app/widgets/addActiveWidget', {name:'VesselList.Side.Summary'})
					next()									
				}
			}
		]
	}
]