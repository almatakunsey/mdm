const RightPane = () => import('../../components/common/WidgetContainers/map/RightPane')

const ToolsLegend = () => import('../../components/map/Tools/ToolsLegend')

const CableSummary = () => import('../../components/map/cable/CableSummary')
const CableCreate = () => import('../../components/map/cable/CableCreate')
const CableDesignerControl = () => import('../../components/map/cable/CableDesignerControl')
const CableEdit = () => import('../../components/map/cable/CableEdit')


import { store } from '../../stores'

export default [	

	// tools/legend 
	// {
	// 	path: 'tools',
	// 	components: {
	// 		rightPane: RightPane
	// 	},
	// 	children: [
	// 		{
	// 			path: 'legend',
	// 			components: {
	// 				rightPaneContent: ToolsLegend
	// 			},
	// 			name: 'map.tools.legend',
	// 			beforeEnter: (to, from, next) => {
	// 				store.commit('tools/legend/toggleLegendEnabled', true)
	// 				Event.fire('Map.ActivateWidget', 'Tools.Right.Legend')
	// 				next()									
	// 			}
	// 		}
	// 	]
	// }


]