const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')
const RightPane = () => import(/* webpackChunkName: "right-pane" */'../../components/common/WidgetContainers/map/RightPane')

const ZoneSummary = () => import(/* webpackChunkName: "zone-summary" */'../../components/map/zone/ZoneSummary')
const ZoneCreate = () => import(/* webpackChunkName: "zone-create" */'../../components/map/zone/ZoneCreate')
const ZoneEdit = () => import(/* webpackChunkName: "zone-edit" */'../../components/map/zone/ZoneEdit')
const ZoneDesignerControl = () => import(/* webpackChunkName: "zone-designer-control" */'../../components/map/zone/ZoneDesignerControl')

import { store } from '../../stores'
const storage = require("store")

export default [	
	
	// zone/summary/*

	{
		path: 'zone/summary',
		components: {
			sidePane: SidePane			
		},
		children : [
			{
				path: '/',
				components: {					
					sidePaneContent: ZoneSummary
				},
				name: 'map.zone.summary',
				beforeEnter: (to, from, next) => {	
					if(!_.includes(storage.get('perm'), 'Zone.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('map/addMapLayer', 'zone')	
						store.commit('zone/summary/toggleZoneSummary', true)
						store.commit('app/widgets/addActiveWidget', {name:'Zone.Side.Summary'})
						next()										
					}

					
				}	
			}			
		]
	},

	// zone/main/*

	{
		path: 'zone/main',
		components: {
			mainPane: MainPane
		},		
		children : [
			{
				path: 'create',
				components: {
					mainPaneContent: ZoneCreate
				},
				name: 'map.zone.main.create',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Zone.Create')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('zone/create/toggleZoneCreate', true)
						store.commit('app/widgets/addActiveWidget', {name:'Zone.Main.Create'})						
						next()									
					}					
					
				}

			},

			{
				path: 'edit/:id',
				components: {
					mainPaneContent: ZoneEdit
				},
				props: { mainPaneContent:true },
				name: 'map.zone.main.edit',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Zone.Update')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}					
					else{
						store.commit('zone/edit/toggleZoneEdit', true)
						store.commit('app/widgets/addActiveWidget', {name:'Zone.Main.Edit'})						
						next()									
					}
					
				}

			}
		]
	},

	// zone/tools/*

	{
		path: 'zone/tools',
		components: {
			rightPane: RightPane
		},
		children: [
			{
				path: 'designer',
				components: {
					rightPaneContent: ZoneDesignerControl
				},
				name: 'map.zone.tools.designer',
				beforeEnter: (to, from, next) => {					
					store.commit('map/removeMapLayer', 'zone')
					store.commit('zone/toggleZoneDesigner', true)
					store.commit('app/widgets/addActiveWidget', {name:'Zone.Right.Designer'})					
					next()									
				}
			}
		]
	}

]