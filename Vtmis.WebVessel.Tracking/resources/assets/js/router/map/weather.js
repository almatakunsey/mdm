const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')
const WeatherDashboard = () => import(/* webpackChunkName: "weather-dashboard" */'../../components/map/weather/WeatherDashboard')
// const LocationUpdate = () => import('../../components/map/weather/LocationUpdate')

import { store } from '../../stores'
const storage = require("store")

export default [	
	
	// weather/main/*

	{
		path: 'weather/main',
		components: {
			mainPane: MainPane
		},		
		children : [
			{
				path: 'stations',
				components: {
					mainPaneContent: WeatherDashboard
				},
				name: 'map.weather.main.dashboard',
				beforeEnter: (to, from, next) => {	
					if(!_.includes(storage.get('perm'), 'Methydro.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('weather/dashboard/toggleWeatherDashboard', true)
						store.commit('app/widgets/addActiveWidget', {name:'Weather.Main.Dashboard'})						
						next()
					}				
														
				}
			}
		]
	}	

]