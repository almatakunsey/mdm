// const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')

const RmsStatistic = () => import(/* webpackChunkName: "rms-statistic" */'../../components/map/statistic/RmsStatistic')
const WeatherHistorical = () => import(/* webpackChunkName: "weather-historical" */'../../components/map/statistic/WeatherHistorical')
const DdmsHistorical = () => import(/* webpackChunkName: "ddms-historical" */'../../components/map/statistic/DdmsHistorical')

// const DdmsStatistic = () => import('../../components/map/statistic/DdmsStatistic')
// const weatherStatistic = () => import('../../components/map/statistic/WeatherStatistic')
import { store } from '../../stores'
const storage = require("store")

export default [	

	// rms
	
	{
		path: 'rms/historical-data/:mmsi-:vesselName-:dac-:fi',
		components: {
			mainPane: MainPane
		},		

		children : [
			{
				path: '',
				components: {
					mainPaneContent: RmsStatistic
				},
				props: { mainPaneContent:true },
				name: 'map.rms.historicalData',
				beforeEnter: (to, from, next) => {	
					if(!_.includes(storage.get('perm'), 'RMS.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{				
						store.commit('statistic/statisticChart/toggleStatisticChart', true)
						store.commit('app/widgets/addActiveWidget', {name:'Rms.Main.Chart'})						
						next()									
					}
				}
			}
		]
	},
	// ddms
	{
		path: 'ddms/historical-data/:mmsi-:vesselName',
		components: {
			mainPane: MainPane
		},		

		children : [
			{
				path: '',
				components: {
					mainPaneContent: DdmsHistorical
				},
				props: { mainPaneContent:true },
				name: 'map.ddms.historicalData',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'DDMS.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}	
					}
					else{					
						store.commit('statistic/statisticChart/toggleStatisticChart', true)
						store.commit('app/widgets/addActiveWidget', {name:'Ddms.Main.Chart'})						
						next()									
					}
				}
			}
		]
	},
	// weather
	{
		path: 'methydro/historical-data/:mmsi',
		components: {
			mainPane: MainPane
		},		

		children : [
			{
				path: '',
				components: {
					mainPaneContent: WeatherHistorical
				},
				props: { mainPaneContent:true },
				name: 'map.weather.historicalData',
				beforeEnter: (to, from, next) => {	
					if(!_.includes(storage.get('perm'), 'Methydro.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{				
						store.commit('statistic/statisticChart/toggleStatisticChart', true)
						store.commit('app/widgets/addActiveWidget', {name:'Methydro.Main.Chart'})						
						next()									
					}
				}
			}
		]
	}
]