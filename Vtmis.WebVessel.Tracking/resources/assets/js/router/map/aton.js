const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')
// const RightPane = () => import('../../components/common/WidgetContainers/map/RightPane')

const AtonSummary = () => import(/* webpackChunkName: "aton-summary" */'../../components/map/aton/AtonSummary')
const AtonList = () => import(/* webpackChunkName: "aton-list" */'../../components/map/aton/AtonList')

const storage = require("store")

import { store } from '../../stores'

export default [	
	
	// aton/summary/*

	{
		path: 'aton/summary',
		components: {
			sidePane: SidePane			
		},
		children : [
			{
				path: '/',
				components: {					
					sidePaneContent: AtonSummary
				},
				name: 'map.aton.summary',
				beforeEnter: (to, from, next) => {						
					if(!_.includes(storage.get('perm'), 'Aton.View')){						
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('aton/summary/toggleAtonSummary', true)
						store.commit('app/widgets/addActiveWidget', {name:'Aton.Side.Summary'})
						next()									
					}
					
				}	
			}		
		]
	},

	// custom-route/main/*

	{
		path: 'aton/main',
		components: {
			mainPane: MainPane
		},		
		children : [
			{
				path: 'list',
				components: {
					mainPaneContent: AtonList
				},
				name: 'map.aton.main.list',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Aton.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('aton/atonList/toggleAtonList', true)
						store.commit('app/widgets/addActiveWidget', {name:'Aton.Main.List'})						
						next()										
					}			
					
				}

			}
		]
	}
]