const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const WremsSummary = () => import(/* webpackChunkName: "wrems-summary" */'../../components/map/wrems/WremsSummary')

import { store } from '../../stores'
const storage = require("store")

export default [	
	
	// location/summary/*

	{
		path: 'wrems/summary',
		components: {
			sidePane: SidePane			
		},
		children : [
			{
				path: '/',
				components: {					
					sidePaneContent: WremsSummary
				},
				name: 'map.wrems.summary',
				beforeEnter: (to, from, next) => {	

					if(!_.includes(storage.get('perm'), 'MyWREMS.View')){						
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}						
					}
					else{
						store.commit('wrems/summary/toggleWremsSummary', true)
						store.commit('app/widgets/addActiveWidget', {name:'Wrems.Side.Summary'})						
						next()	
					}						
								
				}	
			}		
		]
	}

]