const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')
const TenantFiltersAll = () => import(/* webpackChunkName: "main-pane" */'../../components/map/tenantFilter/TenantFiltersAll')
const TenantFilterCreate = () => import(/* webpackChunkName: "main-pane" */'../../components/map/tenantFilter/TenantFiltersCreate')
const TenantFilterAssign = () => import(/* webpackChunkName: "main-pane" */'../../components/map/tenantFilter/TenantFilterAssign')
const storage = require("store")

import { store } from '../../stores'

export default [
    {
        path: 'tenant',
        components: {
            mainPane: MainPane			
        },
        
        children: [
            {
                path: '/',
                components: {
                    mainPaneContent: TenantFiltersAll
                },
                name: 'map.tenantFilter.list',
                beforeEnter: (to, from, next) => {
                    if(!_.includes(storage.get('perm'), 'TenantFilter.View')){
                        if(from.fullPath == "/"){next({name:'mainmap'})}
                        else{next(false)}
                    }
                    else{
                        store.commit('tenantFilter/summary/toggleTenantFilterList', true)
                        store.commit('app/widgets/addActiveWidget', {name:'Tenant.Main.List'})						
                        next()										
                    }					
                    
                }

            },
            {
                path: 'create',
                components: {
                    mainPaneContent: TenantFilterCreate
                },
                name: 'map.tenantFilter.create',
                beforeEnter: (to, from, next) => {
                    if(!_.includes(storage.get('perm'), 'TenantFilter.Create')){
                        if(from.fullPath == "/"){next({name:'mainmap'})}
                        else{next(false)}
                    }
                    else{
                        store.commit('tenantFilter/create/toggleTenantFilterCreate', true)
                        store.commit('app/widgets/addActiveWidget', {name:'Tenant.Main.Create'})						
                        next()										
                    }					
                    
                }
            },
            {
                path: 'assign/:id',
                components: {
                    mainPaneContent: TenantFilterAssign
                },
                name: 'map.tenantFilter.assign',
                beforeEnter: (to, from, next) => {
                    if(!_.includes(storage.get('perm'), 'TenantFilter.Assign')){
                        if(from.fullPath == "/"){next({name:'mainmap'})}
                        else{next(false)}
                    }
                    else{
                        store.commit('tenantFilter/assign/toggleTenantFilterAssign', true)
                        store.commit('app/widgets/addActiveWidget', {name:'Tenant.Main.Assign'})						
                        next()										
                    }					
                    
                }
            }
        ]
    }
]