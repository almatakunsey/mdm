const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')

const FilterSummary = () => import(/* webpackChunkName: "filter-summary" */'../../components/map/filter/FilterSummary')
const FilterCreate = () => import(/* webpackChunkName: "filter-create" */'../../components/map/filter/FilterCreate')
const FilterEdit = () => import(/* webpackChunkName: "filter-edit" */'../../components/map/filter/FilterEdit')
const FilterVesselGroupList = () => import(/* webpackChunkName: "filter-vessel-group-list" */'../../components/map/filter/FilterVesselGroupList')
const FilterVesselGroupCreate = () => import(/* webpackChunkName: "filter-vessel-group-create" */'../../components/map/filter/FilterVesselGroupCreate')
const FilterVesselGroupEdit = () => import(/* webpackChunkName: "filter-vessel-group-edit" */'../../components/map/filter/FilterVesselGroupEdit')

import { store } from '../../stores'
const storage = require("store")

export default [	
	
	// filter/summary/*

	{
		path: 'filter/summary',
		components: {
			sidePane: SidePane			
		},
		children : [
			{
				path: '/',
				components: {					
					sidePaneContent: FilterSummary
				},
				name: 'map.filter.summary',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Filter.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('filters/summary/toggleFilterSummary', true)
						store.commit('app/widgets/addActiveWidget', {name:'Filter.Side.Summary'})						
						next()	
					}					
														
				}	
			}		
		]
	},

	// filter/main/*

	{
		path: 'filter/main',
		components: {
			mainPane: MainPane
		},		
		children : [
			{
				path: 'create',
				components: {
					mainPaneContent: FilterCreate
				},
				name: 'map.filter.main.create',
				beforeEnter: (to, from, next) => {

					if(!_.includes(storage.get('perm'), 'Filter.Create')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('filters/create/toggleFilterCreate', true)
						store.commit('app/widgets/addActiveWidget', {name:'Filter.Main.Create'})						
						next()									
					}										
				}

			},

			{
				path: 'edit/:id',
				components: {
					mainPaneContent: FilterEdit
				},
				props: { mainPaneContent:true },
				name: 'map.filter.main.edit',
				beforeEnter: (to, from, next) => {	

					if(!_.includes(storage.get('perm'), 'Filter.Update')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('filters/edit/toggleFilterEdit', true)
						store.commit('app/widgets/addActiveWidget', {name:'Filter.Main.Edit'})						
						next()										
					}			
					
				},

			}
		]
	},

	// vessel group
	{
		path: 'filter',
		components: {
			mainPane: MainPane			
		},
		children : [
			{
				path: 'vessel-group-list',
				components: {					
					mainPaneContent: FilterVesselGroupList
				},
				name: 'map.filter.vesselGroup.list',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'VesselGroup.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('filters/vesselGroup/vesselGroupList/toggleFilterVesselGroupListPane', true)
						store.commit('app/widgets/addActiveWidget', {name:'Filter.VesselGroup.List'})						
						next()	
					}					
														
				}	
			},

			{
				path: 'vessel-group/create',
				components: {
					mainPaneContent: FilterVesselGroupCreate
				},
				name: 'map.filter.vesselGroup.create',
				beforeEnter: (to, from, next) => {

					if(!_.includes(storage.get('perm'), 'VesselGroup.Create')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('filters/vesselGroup/vesselGroupCreate/toggleFilterVesselGroupCreate', true)
						store.commit('app/widgets/addActiveWidget', {name:'Filter.VesselGroup.Create'})						
						next()									
					}										
				}

			},
			{
				path: 'vessel-group/edit/:id',
				components: {
					mainPaneContent: FilterVesselGroupEdit
				},
				props: { mainPaneContent:true },
				name: 'map.filter.vesselGroup.edit',
				beforeEnter: (to, from, next) => {

					if(!_.includes(storage.get('perm'), 'VesselGroup.Update')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('filters/vesselGroup/edit/toggleFilterVesselGroupEdit', true)
						store.commit('app/widgets/addActiveWidget', {name:'Filter.VesselGroup.Edit'})						
						next()									
					}										
				}

			}	
		]
	},

]