const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')
const MainPaneNopad = () => import(/* webpackChunkName: "main-pane-nopad" */'../../components/common/WidgetContainers/map/MainPaneNopad')

const AlarmSummary = () => import(/* webpackChunkName: "alarm-summary" */'../../components/map/alarm/AlarmSummary')
const AlarmCreate = () => import(/* webpackChunkName: "alarm-create" */'../../components/map/alarm/AlarmCreate')
const AlarmUpdate = () => import(/* webpackChunkName: "alarm-update" */'../../components/map/alarm/AlarmUpdate')
const AlarmNotification = () => import(/* webpackChunkName: "alarm-notification" */'../../components/map/alarm/Notification/AlarmNotification')

import { store } from '../../stores'
const storage = require("store")

export default [	
	
	// alarm/summary/*

	{
		path: 'alarm/summary',
		components: {
			sidePane: SidePane			
		},
		children : [
			{
				path: '/',
				components: {					
					sidePaneContent: AlarmSummary
				},
				name: 'map.alarm.summary',
				beforeEnter: (to, from, next) => {	
					if(!_.includes(storage.get('perm'), 'Alarm.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('alarm/summary/toggleAlarmSummary', true)											
						store.commit('app/widgets/addActiveWidget', {name:'Alarm.Side.Summary'})            
						next()										
					}					
					
				}	
			}		
		]
	},

	// alarm/main/*

	{
		path: 'alarm/main',
		components: {
			mainPane: MainPane
		},		
		children : [
			{
				path: 'create',
				components: {
					mainPaneContent: AlarmCreate
				},
				name: 'map.alarm.main.create',
				beforeEnter: async (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Alarm.Create')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{						
						store.commit('alarm/create/toggleAlarmCreate', true)
						store.commit('app/widgets/addActiveWidget', {name:'Alarm.Main.Create'})						
						next()	
						
					}					
														
				}

			},

			{
				path: 'edit/:id',
				components: {
					mainPaneContent: AlarmUpdate
				},
				props: { mainPaneContent:true },
				name: 'map.alarm.main.edit',
				beforeEnter: (to, from, next) => {	
					if(!_.includes(storage.get('perm'), 'Alarm.Update')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('alarm/update/toggleAlarmUpdate', true)
						store.commit('app/widgets/addActiveWidget', {name:'Alarm.Main.Update'})						
						next()	
					}				
														
				}

			}			
		]
	},

	{
		path: 'alarm/main',
		components: {
			mainPane: MainPaneNopad
		},		
		children : [			
			{
				path: 'notifications',
				components: {
					mainPaneContent: AlarmNotification
				},
				name: 'map.alarm.main.notifications',
				beforeEnter: (to, from, next) => {					
					store.commit('alarm/notification/toggleAlarmNotification', true)
					store.commit('app/widgets/addActiveWidget', {name:'Alarm.Main.Notification'})					
					next()									
				}

			}
		]
	}	



]