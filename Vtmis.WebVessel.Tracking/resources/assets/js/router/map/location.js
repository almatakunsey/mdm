const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')
const RightPane = () => import(/* webpackChunkName: "right-pane" */'../../components/common/WidgetContainers/map/RightPane')

const LocationSummary = () => import(/* webpackChunkName: "location-summary" */'../../components/map/location/LocationSummary')
const LocationCreate = () => import(/* webpackChunkName: "location-create" */'../../components/map/location/LocationCreate')
const LocationUpdate = () => import(/* webpackChunkName: "location-update" */'../../components/map/location/LocationUpdate')
const LocationDesignerControl = () => import(/* webpackChunkName: "location-designer-control" */'../../components/map/location/LocationDesignerControl')

import { store } from '../../stores'
const storage = require("store")

export default [	
	
	// location/summary/*

	{
		path: 'location/summary',
		components: {
			sidePane: SidePane			
		},
		children : [
			{
				path: '/',
				components: {					
					sidePaneContent: LocationSummary
				},
				name: 'map.location.summary',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Location.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('locations/summary/toggleLocationSummary', true)
						store.commit('app/widgets/addActiveWidget', {name:'Location.Side.Summary'})						
						next()	
					}						
														
				}	
			}		
		]
	},

	// location/main/*

	{
		path: 'location/main',
		components: {
			mainPane: MainPane
		},		
		children : [
			{
				path: 'create',
				components: {
					mainPaneContent: LocationCreate
				},
				name: 'map.location.main.create',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Location.Create')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('locations/create/toggleLocationCreate', true)
						store.commit('app/widgets/addActiveWidget', {name:'Location.Main.Create'})						
						next()										
					}					
					
				}


			},

			{
				path: 'edit/:id',
				components: {
					mainPaneContent: LocationUpdate
				},
				props: { mainPaneContent:true },
				name: 'map.location.main.edit',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Location.Update')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('locations/update/toggleLocationUpdate', true)
						store.commit('app/widgets/addActiveWidget', {name:'Location.Main.Update'})						
						next()										
					}					
					
				}

			}
		]
	},

	// location/tools/*

	{
		path: 'location/tools',
		components: {
			rightPane: RightPane
		},
		children: [
			{
				path: 'designer',
				components: {
					rightPaneContent: LocationDesignerControl
				},
				name: 'map.location.tools.designer',
				beforeEnter: (to, from, next) => {					
					store.commit('locations/toggleLocationDesigner', true)
					store.commit('app/widgets/addActiveWidget', {name:'Location.Right.Designer'})					
					next()									
				}
			}
		]
	}	

]