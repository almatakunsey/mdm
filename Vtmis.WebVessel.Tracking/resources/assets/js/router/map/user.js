const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')
const UserList = () => import(/* webpackChunkName: "user-list" */'../../components/map/user/UserList')
const UserView = () => import(/* webpackChunkName: "user-view" */'../../components/map/user/View')
const UserCreate = () => import(/* webpackChunkName: "user-create" */'../../components/map/user/UserCreate')
const UserEdit = () => import(/* webpackChunkName: "user-edit" */'../../components/map/user/UserEdit')
const UserDetail = () => import(/* webpackChunkName: "user-detail" */'../../components/map/user/UserDetailForm')
const UserRole = () => import(/* webpackChunkName: "user-role" */'../../components/map/user/UserRoleForm')

const storage = require("store")

import { store } from '../../stores'

export default [
  {
  	path: 'users',    
    components: {
		mainPane: MainPane			
	},
	beforeEnter: (to, from, next) => {
		if(!_.includes(storage.get('perm'), 'User.View')){
			if(from.fullPath == "/"){next({name:'mainmap'})}
			else{next(false)}
		}
		else{
			store.commit('users/edit/toggleUserEdit', true)
			store.commit('app/widgets/addActiveWidget', {name:'Users.Main.Update'})						
			next()										
		}					
		
	},
	children : [
		{
			path: '/',
			components: {
				mainPaneContent: UserList
			},
			name: 'map.users.index',
			beforeEnter: (to, from, next) => {					
				store.commit('users/list/toggleUserList', true)
				store.commit('app/widgets/addActiveWidget', {name:'Users.Main.Index'})
				next()									
			}
		},
		{
			path: ':id/view',
			components: {
				mainPaneContent: UserView
			},
			name: 'map.users.view',
			props: { mainPaneContent:true },
			beforeEnter: (to, from, next) => {					
				store.commit('users/view/toggleUserView', true)
				store.commit('app/widgets/addActiveWidget', {name:'Users.Main.View'})					
				next()									
			}
		},
		{
			path: ':id/edit',
			components: {
				mainPaneContent: UserEdit
			},
			// name: 'map.users.edit',
			props: { mainPaneContent:true },
			beforeEnter: (to, from, next) => {
				if(!_.includes(storage.get('perm'), 'User.Update')){
					if(from.fullPath == "/"){next({name:'mainmap'})}
					else{next(false)}
				}
				else{
					store.commit('users/edit/toggleUserEdit', true)
					store.commit('app/widgets/addActiveWidget', {name:'Users.Main.Update'})						
					next()										
				}					
				
			},
			children:[
				{
					path:'user-details',
					name: 'map.users.edit.details',
					components:{
						userEditContent: UserDetail
					}
					
				},
				{
					path:'user-roles',
					name: 'map.users.edit.roles',
					components:{
						userEditContent: UserRole	
					}
					
				}
			]
		},
		{
			path: 'create',
			components: {
				mainPaneContent: UserCreate
			},
			// name: 'map.users.main.create',
			beforeEnter: (to, from, next) => {
				if(!_.includes(storage.get('perm'), 'User.Create')){
					if(from.fullPath == "/"){next({name:'mainmap'})}
					else{next(false)}
				}
				else{
					store.commit('users/create/toggleUserCreate', true)
					store.commit('app/widgets/addActiveWidget', {name:'Users.Main.Create'})						
					next()										
				}					
				
			},
			children:[
				{
					path:'user-details',
					name: 'map.users.create.details',
					components:{
						userCreateContent: UserDetail
					}
					
				},
				{
					path:'user-roles',
					name: 'map.users.create.roles',
					components:{
						userCreateContent: UserRole	
					}
					
				}
			]
		}
	]
  }
]