const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')

const AdvancedSearchResult = () => import(/* webpackChunkName: "advance-search-result" */'../../components/map/search/AdvancedSearchResult')

import { store } from '../../stores'

export default [	
	

	// search/advance/*

	{
		path: 'search/advance',
		components: {
			mainPane: MainPane
		},		
		children : [
			{
				path: 'result',
				components: {
					mainPaneContent: AdvancedSearchResult
				},
				name: 'map.search.advance.result',
				beforeEnter: (to, from, next) => {					
					// store.commit('search/vesselsearch/toggleAdvancedSearchResult', true)
					store.commit('app/widgets/addActiveWidget', {name:'AdvancedSearchResult'})					
					next()									
				}

			}
		]
	}

]