const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
// const MainPane = () => import('../../components/common/WidgetContainers/map/MainPane')
// const RightPane = () => import('../../components/common/WidgetContainers/map/RightPane')

// const CableSummary = () => import('../../components/map/cable/CableSummary')
const EicsSummary = () => import(/* webpackChunkName: "eics-summary" */'../../components/map/eics/EicsSummary')
// const RightEicsPanel = () => import('../../components/map/eics/RightEicsPanel')
// const CableCreate = () => import('../../components/map/cable/CableCreate')
// const CableDesignerControl = () => import('../../components/map/cable/CableDesignerControl')
// const CableEdit = () => import('../../components/map/cable/CableEdit')


import { store } from '../../stores'
const storage = require("store")

export default [	
	
	// cable/summary/*

	{
		path: 'eics/summary',
		components: {
			sidePane: SidePane
			// rightPane: RightPane			
		},
		children : [
			{
				path: '/',
				components: {					
					sidePaneContent: EicsSummary
					// rightPaneContent: RightEicsPanel
				},
				name: 'map.eics.summary',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'EICS_View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('eics/summary/toggleEicsSummary', true)
						store.commit('app/widgets/addActiveWidget', {name:'Eics.Side.Summary'})						
						next()	
					}					
					// store.commit('eics/summary/toggleEicsSummary', true)
					// store.commit('app/widgets/addActiveWidget', {name:'Eics.Side.Summary'})						
					// next()								
				}	
			}		
		]
	}
	// ,

	// {
	// 	path: 'eics/filter',
	// 	components: {
	// 		rightPane: RightPane
	// 	},
	// 	children: [
	// 		{
	// 			path: '/',
	// 			components: {
	// 				rightPaneContent: CableDesignerControl
	// 			},
	// 			name: 'map.eics.filter',
	// 			beforeEnter: (to, from, next) => {
	// 				// store.commit('cable/toggleCableDesigner', true)
	// 				// store.commit('app/widgets/addActiveWidget', {name:'Cable.Right.Designer'})					
	// 				// next()									
	// 			}
	// 		}
	// 	]
	// }

	// cable/main/*

	// {
	// 	path: 'cable/main',
	// 	components: {
	// 		mainPane: MainPane
	// 	},		
	// 	children : [
	// 		{
	// 			path: 'create',
	// 			components: {
	// 				mainPaneContent: CableCreate
	// 			},
	// 			name: 'map.cable.main.create',
	// 			beforeEnter: (to, from, next) => {
	// 				if(!_.includes(storage.get('perm'), 'Cable.Create')){
	// 					if(from.fullPath == "/"){next({name:'mainmap'})}
	// 					else{next(false)}
	// 				}
	// 				else{
	// 					store.commit('cable/create/toggleCableCreate', true)
	// 					store.commit('app/widgets/addActiveWidget', {name:'Cable.Main.Create'})						
	// 					next()										
	// 				}					
					
	// 			}

	// 		},

	// 		{
	// 			path: 'edit/:id',
	// 			components: {
	// 				mainPaneContent: CableEdit
	// 			},
	// 			props: { mainPaneContent:true },
	// 			name: 'map.cable.main.edit',
	// 			beforeEnter: (to, from, next) => {
	// 				if(!_.includes(storage.get('perm'), 'Cable.Update')){
	// 					if(from.fullPath == "/"){next({name:'mainmap'})}
	// 					else{next(false)}
	// 				}
	// 				else{
	// 					store.commit('cable/edit/toggleCableEdit', true)
	// 					store.commit('app/widgets/addActiveWidget', {name:'Cable.Main.Edit'})						
	// 					next()	
	// 				}					
														
	// 			}

	// 		}
	// 	]
	// }	
]