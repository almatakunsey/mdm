const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')
const RightPane = () => import(/* webpackChunkName: "right-pane" */'../../components/common/WidgetContainers/map/RightPane')

const CableSummary = () => import(/* webpackChunkName: "cable-summary" */'../../components/map/cable/CableSummary')
const CableCreate = () => import(/* webpackChunkName: "cable-create" */'../../components/map/cable/CableCreate')
const CableDesignerControl = () => import(/* webpackChunkName: "cable-designer-control" */'../../components/map/cable/CableDesignerControl')
const CableEdit = () => import(/* webpackChunkName: "cable-edit" */'../../components/map/cable/CableEdit')


import { store } from '../../stores'
const storage = require("store")

export default [	
	
	// cable/summary/*

	{
		path: 'cable/summary',
		components: {
			sidePane: SidePane			
		},
		children : [
			{
				path: '/',
				components: {					
					sidePaneContent: CableSummary
				},
				name: 'map.cable.summary',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Cable.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('cable/summary/toggleCableSummary', true)
						store.commit('app/widgets/addActiveWidget', {name:'Cable.Side.Summary'})						
						next()	
					}					
														
				}	
			}		
		]
	},

	// cable/main/*

	{
		path: 'cable/main',
		components: {
			mainPane: MainPane
		},		
		children : [
			{
				path: 'create',
				components: {
					mainPaneContent: CableCreate
				},
				name: 'map.cable.main.create',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Cable.Create')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('cable/create/toggleCableCreate', true)
						store.commit('app/widgets/addActiveWidget', {name:'Cable.Main.Create'})						
						next()										
					}					
					
				}

			},

			{
				path: 'edit/:id',
				components: {
					mainPaneContent: CableEdit
				},
				props: { mainPaneContent:true },
				name: 'map.cable.main.edit',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Cable.Update')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('cable/edit/toggleCableEdit', true)
						store.commit('app/widgets/addActiveWidget', {name:'Cable.Main.Edit'})						
						next()	
					}					
														
				}

			}
		]
	},

	// cable/tools/*

	{
		path: 'cable/tools',
		components: {
			rightPane: RightPane
		},
		children: [
			{
				path: 'designer',
				components: {
					rightPaneContent: CableDesignerControl
				},
				name: 'map.cable.tools.designer',
				beforeEnter: (to, from, next) => {
					store.commit('cable/toggleCableDesigner', true)
					store.commit('app/widgets/addActiveWidget', {name:'Cable.Right.Designer'})					
					next()									
				}
			}
		]
	}

]