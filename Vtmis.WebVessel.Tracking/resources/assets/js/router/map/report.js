const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')

const ReportSummary = () => import(/* webpackChunkName: "report-summary" */'../../components/map/report/ReportSummary')
const ReportCreate = () => import(/* webpackChunkName: "report-create" */'../../components/map/report/ReportCreate')
const ReportEdit = () => import(/* webpackChunkName: "report-edit" */'../../components/map/report/ReportEdit')
const ReportResult = () => import(/* webpackChunkName: "report-result" */'../../components/map/report/ReportResult')

import { store } from '../../stores'
const storage = require("store")

export default [	
	
	// report/summary/*

	{
		path: 'report/summary',
		components: {
			sidePane: SidePane			
		},
		children : [
			{
				path: '/',
				components: {					
					sidePaneContent: ReportSummary
				},
				name: 'map.report.summary',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Report.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('reports/summary/toggleReportSummary', true)
						store.commit('app/widgets/addActiveWidget', {name:'Report.Side.Summary'})
						next()	
					}						
														
				}	
			}		
		]
	},

	// report/main/*

	{
		path: 'report/main',
		components: {
			mainPane: MainPane
		},		
		children : [
			{
				path: 'create',
				components: {
					mainPaneContent: ReportCreate
				},
				name: 'map.report.main.create',
				beforeEnter: (to, from, next) => {	
					if(!_.includes(storage.get('perm'), 'Report.Create')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}				
					store.commit('reports/create/toggleReportCreate', true)
					store.commit('app/widgets/addActiveWidget', {name:'Report.Main.Create'})					
					next()									
				}

			},

			{
				path: 'edit/:id',
				components: {
					mainPaneContent: ReportEdit
				},
				props: { mainPaneContent:true },
				name: 'map.report.main.edit',
				beforeEnter: (to, from, next) => {	
					if(!_.includes(storage.get('perm'), 'Report.Update')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('reports/edit/toggleReportEdit', true)
						store.commit('app/widgets/addActiveWidget', {name:'Report.Main.Edit'})						
						next()							
					}				
							
				}

			},

			{
				path: 'result/',
				components: {
					mainPaneContent: ReportResult
				},
				props: { mainPaneContent:true },
				name: 'map.report.main.result',
				beforeEnter: (to, from, next) => {					
					store.commit('reports/result/toggleReportResult', true)
					store.commit('app/widgets/addActiveWidget', {name:'Report.Main.Result'})					
					next()									
				}

			}
		]
	}	

]