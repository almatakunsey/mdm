const SidePane = () => import(/* webpackChunkName: "side-pane" */'../../components/common/WidgetContainers/map/SidePane')
const MainPane = () => import(/* webpackChunkName: "main-pane" */'../../components/common/WidgetContainers/map/MainPane')
const RightPane = () => import(/* webpackChunkName: "right-pane" */'../../components/common/WidgetContainers/map/RightPane')
const BottomPane = () => import(/* webpackChunkName: "bottom-pane" */'../../components/common/WidgetContainers/map/BottomPane')

const CustomRouteSummary = () => import(/* webpackChunkName: "custom-route-summary" */'../../components/map/route/CustomRouteSummary')
const CustomRouteCreate = () => import(/* webpackChunkName: "custom-route-create" */'../../components/map/route/CustomRouteCreate')
const CustomRouteEdit = () => import(/* webpackChunkName: "custom-route-edit" */'../../components/map/route/CustomRouteEdit')
const CustomRouteDesignerControl = () => import(/* webpackChunkName: "custom-route-designer-control" */'../../components/map/route/RouteDesignerControl')
const CustomRouteEta = () => import(/* webpackChunkName: "custom-route-eta" */'../../components/map/route/CustomRouteEta')
const VesselEta = () => import(/* webpackChunkName: "vessel-eta" */'../../components/map/VesselDetail/VesselEta')

import { store } from '../../stores'
const storage = require("store")

export default [	
	
	// custom-route/summary/*

	{
		path: 'custom-route/summary',
		components: {
			sidePane: SidePane			
		},
		children : [
			{
				path: '/',
				components: {					
					sidePaneContent: CustomRouteSummary
				},
				name: 'map.custom-route.summary',
				beforeEnter: (to, from, next) => {	
					if(!_.includes(storage.get('perm'), 'Route.View')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('customRoute/summary/toggleCustomRouteSummary', true)
						store.commit('app/widgets/addActiveWidget', {name:'CustomRoute.Side.Summary'})
						next()										
					}					
					
				}	
			}		
		]
	},

	// custom-route/main/*

	{
		path: 'custom-route/main',
		components: {
			mainPane: MainPane
		},		
		children : [
			{
				path: 'create',
				components: {
					mainPaneContent: CustomRouteCreate
				},
				name: 'map.custom-route.main.create',
				beforeEnter: (to, from, next) => {		
					if(!_.includes(storage.get('perm'), 'Route.Create')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('customRoute/create/toggleCustomRouteCreate', true)
						store.commit('app/widgets/addActiveWidget', {name:'CustomRoute.Main.Create'})						
						next()										
					}			
					
				}

			},

			{
				path: 'edit/:id',
				components: {
					mainPaneContent: CustomRouteEdit
				},
				props: { mainPaneContent:true },
				name: 'map.custom-route.main.edit',
				beforeEnter: (to, from, next) => {
					if(!_.includes(storage.get('perm'), 'Route.Update')){
						if(from.fullPath == "/"){next({name:'mainmap'})}
						else{next(false)}
					}
					else{
						store.commit('customRoute/edit/toggleCustomRouteEdit', true)
						store.commit('app/widgets/addActiveWidget', {name:'CustomRoute.Main.Edit'})						
						next()									
					}					
					
				}

			},			

			{
				path: 'vessel-eta/:vesselId',
				components: {
					mainPaneContent: VesselEta
				},	
				props: { mainPaneContent:true },			
				name: 'map.custom-route.main.vessel-eta',
				beforeEnter: (to, from, next) => {					
					store.commit('customRoute/eta/toggleVesselEta', true)
					store.commit('app/widgets/addActiveWidget', {name:'Vessel.Main.Eta'})					
					next()									
				}

			},


		]
	},

	{
		path: 'custom-route/main',
		components: {
			mainPane: BottomPane
		},		
		children : [			

			{
				path: 'eta',
				components: {
					mainPaneContent: CustomRouteEta
				},				
				name: 'map.custom-route.main.eta',
				beforeEnter: (to, from, next) => {					
					store.commit('customRoute/eta/toggleCustomRouteEta', true)
					store.commit('app/widgets/addActiveWidget', {name:'CustomRoute.Main.Eta'})					
					next()									
				}

			}


		]
	},

	// custom-route/tools/*

	{
		path: 'custom-route/tools',
		components: {
			rightPane: RightPane
		},
		children: [
			{
				path: 'designer',
				components: {
					rightPaneContent: CustomRouteDesignerControl
				},
				name: 'map.custom-route.tools.designer',
				beforeEnter: (to, from, next) => {
					store.commit('customRoute/toggleRouteDesigner', true)
					store.commit('app/widgets/addActiveWidget', {name:'CustomRoute.Right.Designer'})					
					next()									
				}
			}
		]
	}	

]