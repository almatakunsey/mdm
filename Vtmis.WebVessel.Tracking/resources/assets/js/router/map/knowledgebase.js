const MiniHelperPanel = () => import(/* webpackChunkName: "mini-helper-panel" */'../../components/map/knowledgebase/MiniHelperPanel')
const Topic = () => import(/* webpackChunkName: "topic" */'../../components/map/knowledgebase/Topic')
const KnowledgeBaseRightPane = () => import(/* webpackChunkName: "knowledge-base-right-pane" */'../../components/map/knowledgebase/RightPane')
const InformationPanelContent = () => import(/* webpackChunkName: "information-panel-content" */'../../components/map/knowledgebase/infoPanelHelper/InfoPanelContent')
const UploadDeleteHelper = () => import(/* webpackChunkName: "upload-delete-helper" */'../../components/map/knowledgebase/infoPanelHelper/UploadDeleteImage')
const HistoricalDataHelper = () => import(/* webpackChunkName: "historical-data-helper" */'../../components/map/knowledgebase/infoPanelHelper/HistoricalData')
const VesselContent = () => import(/* webpackChunkName: "vessel-content" */'../../components/map/knowledgebase/vesselListHelper/VesselPanel')
const VesselList = () => import(/* webpackChunkName: "vessel-list" */'../../components/map/knowledgebase/vesselListHelper/VesselList')
const ShowVesselName = () => import(/* webpackChunkName: "show-vessel-name" */'../../components/map/knowledgebase/vesselListHelper/ShowVesselName')
const ShowVesselTrack = () => import(/* webpackChunkName: "show-vessel-track" */'../../components/map/knowledgebase/vesselListHelper/ShowVesselTrack')
const ShowInformationBalloon = () => import(/* webpackChunkName: "show-information-balloon" */'../../components/map/knowledgebase/vesselListHelper/ShowInformationBalloon')
const FollowSelectedVessel = () => import(/* webpackChunkName: "follow-selected-vessel" */'../../components/map/knowledgebase/vesselListHelper/FollowSelectedVessel')
const ShowOnlySelectedVessel = () => import(/* webpackChunkName: "show-only-selected-vessel" */'../../components/map/knowledgebase/vesselListHelper/ShowOnlySelectedVessel')
const FilterContent = () => import(/* webpackChunkName: "filter-content" */'../../components/map/knowledgebase/filterHelper/FilterContent')
const CreateFilterHelper = () => import(/* webpackChunkName: "create-filter-helper" */'../../components/map/knowledgebase/filterHelper/CreateFilter')
const EditFilterHelper = () => import(/* webpackChunkName: "edit-filter-helper" */'../../components/map/knowledgebase/filterHelper/EditFilter')
const DeleteFilterHelper = () => import(/* webpackChunkName: "delete-filter-helper" */'../../components/map/knowledgebase/filterHelper/DeleteFilter')
const FilterDesignerHelper = () => import(/* webpackChunkName: "filter-designer-helper" */'../../components/map/knowledgebase/filterHelper/FilterDesigner')
const ZoneContent = () => import(/* webpackChunkName: "zone-content" */'../../components/map/knowledgebase/zoneHelper/ZoneContent')
const ZoneDesignerHelper = () => import(/* webpackChunkName: "zone-designer-helper" */'../../components/map/knowledgebase/zoneHelper/ZoneDesigner')
const ZoneCreateHelper = () => import(/* webpackChunkName: "zone-create-helper" */'../../components/map/knowledgebase/zoneHelper/ZoneCreate')
const ZoneEditHelper = () => import(/* webpackChunkName: "zone-delete-helper" */'../../components/map/knowledgebase/zoneHelper/ZoneEdit')
const ZoneDeleteHelper = () => import(/* webpackChunkName: "vessel-content" */'../../components/map/knowledgebase/zoneHelper/ZoneDelete')
const LocationContent = () => import(/* webpackChunkName: "location-content" */'../../components/map/knowledgebase/locationHelper/LocationContent')
const LocationCreateHelper = () => import(/* webpackChunkName: "location-create-helper" */'../../components/map/knowledgebase/locationHelper/LocationCreate')
const LocationEditHelper = () => import(/* webpackChunkName: "location-edit-helper" */'../../components/map/knowledgebase/locationHelper/LocationEdit')
// const ReportContent = () => import('../../components/map/knowledgebase/reportHelper/ReportContent')
// const ReportDesignerHelper = () => import('../../components/map/knowledgebase/reportHelper/ReportDesigner')
// const ReportCreateHelper = () => import('../../components/map/knowledgebase/reportHelper/ReportCreate')
// const ReportGenerateHelper = () => import('../../components/map/knowledgebase/reportHelper/ReportGenerate')
const RouteContent = () => import(/* webpackChunkName: "route-content" */'../../components/map/knowledgebase/routeHelper/RouteContent')
const RouteDesignerHelper = () => import(/* webpackChunkName: "route-designer-helper" */'../../components/map/knowledgebase/routeHelper/RouteDesigner')
const RouteCreateHelper = () => import(/* webpackChunkName: "route-create-helper" */'../../components/map/knowledgebase/routeHelper/RouteCreate')
const RouteEtaHelper = () => import(/* webpackChunkName: "route-eta-helper" */'../../components/map/knowledgebase/routeHelper/RouteEta')
const RouteVesselEtaHelper = () => import(/* webpackChunkName: "route-vessel-eta-helper" */'../../components/map/knowledgebase/routeHelper/VesselEta')
const WeatherContent = () => import(/* webpackChunkName: "weather-content" */'../../components/map/knowledgebase/weatherHelper/weatherContent')
const WeatherInstrument = () => import(/* webpackChunkName: "weather-instrument-content" */'../../components/map/knowledgebase/weatherHelper/InstrumentPanel')
const WeatherHistorical = () => import(/* webpackChunkName: "weather-historical-content" */'../../components/map/knowledgebase/weatherHelper/HistoricalData')
const SearchContent = () => import(/* webpackChunkName: "search-content" */'../../components/map/knowledgebase/searchHelper/SearchContent')
const ToolsContent = () => import(/* webpackChunkName: "tools-content" */'../../components/map/knowledgebase/toolsHelper/ToolsContent')
const FooterContent = () => import(/* webpackChunkName: "footer-content" */'../../components/map/knowledgebase/footerHelper/FooterContent')
const EicsContent = () => import(/* webpackChunkName: "eics-content" */'../../components/map/knowledgebase/eicsHelper/EicsContent')
const WremsContent = () => import(/* webpackChunkName: "wrems-content" */'../../components/map/knowledgebase/wremsHelper/WremsContent')
const AtonContent = () => import(/* webpackChunkName: "aton-content" */'../../components/map/knowledgebase/atonHelper/AtonContent')
const UserContent = () => import(/* webpackChunkName: "user-content" */'../../components/map/knowledgebase/userHelper/UserContent')
const SearchResult = () => import(/* webpackChunkName: "search-result" */'../../components/map/knowledgebase/SearchContent')

import { store } from '../../stores'

export default [
	{
		path: 'knowledgebase',
		components: {											
			rightPane: KnowledgeBaseRightPane			
		},
		beforeEnter: (to, from, next) => {
			store.commit('knowledgebase/minihelper/toggleMiniHelperEnabled', true)
			store.commit('app/widgets/addActiveWidget', {name:'KnowledgeBase.Right.Home'})
			next()									
		},	
		children:[
			{
				path: 'topics',				
				components: {
					rightPaneContent: MiniHelperPanel
				},
				children:[
					{
						path:'/',
						name: 'knowledgebase.right.topics',
						components:{
							miniHelperPanelContent: Topic	
						}
						
					},

					// Information Panel 
					{
						path: 'information-panel',
						name: 'knowledgebase.right.informationPanel',
						components: {
							miniHelperPanelContent: InformationPanelContent
						},
					},

					{
						path: 'information-panel/upload-delete-image',
						name: 'knowledgebase.right.uploadDeleteImage',
						components: {
							miniHelperPanelContent: UploadDeleteHelper
						},
					},

					{
						path: 'information-panel/historical-data',
						name: 'knowledgebase.right.historicalData',
						components: {
							miniHelperPanelContent: HistoricalDataHelper
						},
					},
					// Vessel List 
					{
						path: 'vessel',
						name: 'knowledgebase.right.vessel',
						components: {
							miniHelperPanelContent: VesselContent
						},
					},

					{
						path: 'vessel/vessel-list',
						name: 'knowledgebase.right.vesselList',
						components: {
							miniHelperPanelContent: VesselList
						},
					},

					{
						path: 'vessel/show-vessel-names',
						name: 'knowledgebase.right.showVesselNames',
						components: {
							miniHelperPanelContent: ShowVesselName
						}			
					},

					{
						path: 'vessel/show-vessel-tracks',
						name: 'knowledgebase.right.showVesselTracks',
						components: {
							miniHelperPanelContent: ShowVesselTrack
						}			
					},

					{
						path: 'vessel/show-information-balloon',
						name: 'knowledgebase.right.showInformationBalloon',
						components: {
							miniHelperPanelContent: ShowInformationBalloon
						}			
					},

					{
						path: 'vessel/follow-selected-vessel',
						name: 'knowledgebase.right.followSelectedVessel',
						components: {
							miniHelperPanelContent: FollowSelectedVessel
						}			
					},

					{
						path: 'vessel/show-only-selected-vessel',
						name: 'knowledgebase.right.showOnlySelectedVessel',
						components: {
							miniHelperPanelContent: ShowOnlySelectedVessel
						}			
					},

					// Filter
					
					{
						path: 'filter',
						name: 'knowledgebase.right.filter',
						components: {
							miniHelperPanelContent: FilterContent
						},
					},

					{
						path: 'filter/create-filter',
						name: 'knowledgebase.right.createFilter',
						components: {
							miniHelperPanelContent: CreateFilterHelper
						},
					},
					{
						path: 'filter/edit-filter',
						name: 'knowledgebase.right.editFilter',
						components: {
							miniHelperPanelContent: EditFilterHelper
						},
					},
					{
						path: 'filter/delete-filter',
						name: 'knowledgebase.right.deleteFilter',
						components: {
							miniHelperPanelContent: DeleteFilterHelper
						},
					},
					{
						path: 'filter/filter-designer',
						name: 'knowledgebase.right.filterDesigner',
						components: {
							miniHelperPanelContent: FilterDesignerHelper
						},
					},

					// Zone
					
					{
						path: 'zone',
						name: 'knowledgebase.right.zone',
						components: {
							miniHelperPanelContent: ZoneContent
						},
					},

					{
						path: 'zone/zone-designer',
						name: 'knowledgebase.right.zoneDesigner',
						components: {
							miniHelperPanelContent: ZoneDesignerHelper
						},
					},

					{
						path: 'zone/zone-create',
						name: 'knowledgebase.right.zoneCreate',
						components: {
							miniHelperPanelContent: ZoneCreateHelper
						},
					},

					{
						path: 'zone/zone-edit',
						name: 'knowledgebase.right.zoneEdit',
						components: {
							miniHelperPanelContent: ZoneEditHelper
						},
					},

					{
						path: 'zone/zone-Delete',
						name: 'knowledgebase.right.zoneDelete',
						components: {
							miniHelperPanelContent: ZoneDeleteHelper
						},
					},

					// Location
					
					{
						path: 'location',
						name: 'knowledgebase.right.location',
						components: {
							miniHelperPanelContent: LocationContent
						},
					},

					{
						path: 'location/location-create',
						name: 'knowledgebase.right.locationCreate',
						components: {
							miniHelperPanelContent: LocationCreateHelper
						},
					},

					{
						path: 'location/location-edit',
						name: 'knowledgebase.right.locationEdit',
						components: {
							miniHelperPanelContent: LocationEditHelper
						},
					},

					// Report
					
					// {
					// 	path: 'report',
					// 	name: 'knowledgebase.right.report',
					// 	components: {
					// 		miniHelperPanelContent: ReportContent
					// 	},
					// },

					// {
					// 	path: 'report/report-designer',
					// 	name: 'knowledgebase.right.reportDesigner',
					// 	components: {
					// 		miniHelperPanelContent: ReportDesignerHelper
					// 	},
					// },

					// {
					// 	path: 'report/report-create',
					// 	name: 'knowledgebase.right.reportCreate',
					// 	components: {
					// 		miniHelperPanelContent: ReportCreateHelper
					// 	},
					// },

					// {
					// 	path: 'report/report-generate',
					// 	name: 'knowledgebase.right.runReport',
					// 	components: {
					// 		miniHelperPanelContent: ReportGenerateHelper
					// 	},
					// },

					// Route
					
					{
						path: 'route',
						name: 'knowledgebase.right.route',
						components: {
							miniHelperPanelContent: RouteContent
						},
					},

					{
						path: 'route/route-designer',
						name: 'knowledgebase.right.routeDesigner',
						components: {
							miniHelperPanelContent: RouteDesignerHelper
						},
					},
					{
						path: 'route/route-create',
						name: 'knowledgebase.right.routeCreate',
						components: {
							miniHelperPanelContent: RouteCreateHelper
						},
					},
					{
						path: 'route/route-eta',
						name: 'knowledgebase.right.routeEta',
						components: {
							miniHelperPanelContent: RouteEtaHelper
						},
					},
					{
						path: 'route/route-vessel-eta',
						name: 'knowledgebase.right.routeVesselEta',
						components: {
							miniHelperPanelContent: RouteVesselEtaHelper
						},
					},

					// Weather
					
					{
						path: 'weather',
						name: 'knowledgebase.right.weather',
						components: {
							miniHelperPanelContent: WeatherContent
						},
					},
					{
						path: 'weather/instrument-panel',
						name: 'knowledgebase.right.weatherInstrument',
						components: {
							miniHelperPanelContent: WeatherInstrument
						},
					},
					{
						path: 'weather/historical-data',
						name: 'knowledgebase.right.weatherHistorical',
						components: {
							miniHelperPanelContent: WeatherHistorical
						},
					},

					// User
					
					{
						path: 'user',
						name: 'knowledgebase.right.user',
						components: {
							miniHelperPanelContent: UserContent
						},
					},

					// Search Helper
					
					{
						path: 'search',
						name: 'knowledgebase.right.search',
						components: {
							miniHelperPanelContent: SearchContent
						},
					},

					// Tools Helper
					
					{
						path: 'tools',
						name: 'knowledgebase.right.tools',
						components: {
							miniHelperPanelContent: ToolsContent
						},
					},

					// Footer Helper
					
					{
						path: 'footer',
						name: 'knowledgebase.right.footer',
						components: {
							miniHelperPanelContent: FooterContent
						},
					},

					// AtoN Helper
					
					{
						path: 'aton',
						name: 'knowledgebase.right.aton',
						components: {
							miniHelperPanelContent: AtonContent
						},
					},

					// EICS Helper
					
					{
						path: 'eics',
						name: 'knowledgebase.right.eics',
						components: {
							miniHelperPanelContent: EicsContent
						},
					},

					// Wrems Helper
					
					{
						path: 'wrems',
						name: 'knowledgebase.right.wrems',
						components: {
							miniHelperPanelContent: WremsContent
						},
					},
					// Search Function
					
					{
						path: 'search/:searchTerm',
						name: 'knowledgebase.right.searchContent',
						props: { miniHelperPanelContent:true },
						components: {
							miniHelperPanelContent: SearchResult
						},
					}
				]			
			},
				
		]
		
	}
]