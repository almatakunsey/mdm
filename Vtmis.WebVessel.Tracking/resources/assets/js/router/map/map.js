const MainMap = () => import('../../components/map/MainMap')

import knowledgebaseRoute from './knowledgebase'
import filterRoute from './filter'
import vesselListRoute from './vesselList'
import zoneRoute from './zone'
import locationRoute from './location'
import reportRoute from './report'
import customRouteRoute from './customRoute'
import alarmRoute from './alarm'
import userRoute from './user'
import weatherRoute from './weather'
import cableRoute from './cable'
import atonRoute from './aton'
import statisticRoute from './statistic'
import searchRoute from './search'
import toolsRoute from './tools'
import wremsRoute from './wrems'
import eicsRoute from './eics'
import tenantFilter from './tenantfilter'

import { store } from '../../stores'

export default [	
	{
		path: '/:tenant/map',
		component: MainMap,
		name:'mainmap',
		meta: {
			layout: 'main',
			requiresAuth: true
		},
		beforeEnter: (to, from, next) => {			
			Event.fire('Map.ClearWidget')
			next()									
		},			
		children: [	
			...knowledgebaseRoute,
			...filterRoute,
			...vesselListRoute,
			...zoneRoute,
			...locationRoute,
			...reportRoute,
			...customRouteRoute,
			...alarmRoute,
			...userRoute,			
			...weatherRoute,
			...cableRoute,
			...atonRoute,
			...statisticRoute,
			...searchRoute,
			...toolsRoute,
			...wremsRoute,
			...eicsRoute,
			...tenantFilter
		]
	}
]

