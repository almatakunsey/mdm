const InstrumentPanelSinglePage = () => import(/* webpackChunkName: "instrument-panel-single-page" */'../components/pages/instrumentPanel/InstrumentPanelSinglePage')
const WeatherHistoricalPublic = () => import(/* webpackChunkName: "weather-historical-public" */'../components/pages/instrumentPanel/WeatherHistoricalPublic')

import { store } from '../stores'

export default [	
	{
		path: '/MEH/instrument-panel/details',
		component: InstrumentPanelSinglePage,
		name:'public.instrument-panel',
		meta: {
			layout: 'blank'			
		}
	},

	{
		path: '/MEH/instrument-panel/historical',
		component: WeatherHistoricalPublic,
		props:true,
		name:'public.weather.historical',
		meta: {
			layout: 'blank'			
		}
	}
]

