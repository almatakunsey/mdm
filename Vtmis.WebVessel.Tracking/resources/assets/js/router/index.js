import Vue from 'vue'
import VueSession from 'dwy-vue-session'

// other routing
import Router from 'vue-router'
import Auth from './auth'
import Maps from './map/map'
import AdminDashboard from './admin/main'
import Errors from './errors'
import InstrumentPanel from './instrumentPanel'

Vue.use(Router)

// layouts
Vue.component('default-layout', ()=> import('../components/layouts/HomeLayout.vue'));
Vue.component('main-layout', ()=> import('../components/layouts/MainLayout.vue'));
Vue.component('admin-layout', ()=> import('../components/layouts/AdminLayout.vue'));

const router = new Router({
	mode:'hash'	
})

// dynamically add routes
const routes = [	
	...Auth,
	...Maps,
	...AdminDashboard,
	...Errors,
	...InstrumentPanel,
]
router.addRoutes(routes)

// router guard
router.beforeEach((to, from, next) => {

	const session=VueSession.getInstance();	

	// logged in page, if not signed in, redirect to login page
	if (to.matched.some(record => record.meta.requiresAuth)) {			
		if (!session.signedin) {			
			next({
				path: `/${to.params.tenant}`
				// query: {
				// 	redirect: to.fullPath,
				// }
			});
		} else {
			next();
		}
	} 
	// in login page, if signed in, redirect to main page
	else if(to.name == 'login' || to.name == undefined) {	
		
		if(session.signedin){				
			next({
				path: `/${localStorage.tenancyName}/map`
			});
		}
		else{			
			if(to.path == '/'){
				next({
					path: '/admin'
				});	
			}			
			else{
				next()
			}
		}
	}

	else {
		console.log("%cGENERAL PAGES", conStyle.boldRed)
		next()
	}

})

// This will be global guard later
// router.beforeEach((to, from, next)=>{});

export default router