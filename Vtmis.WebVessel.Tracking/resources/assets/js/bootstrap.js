/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */
try {    
    window.$ = window.jQuery = require('jquery');
    // inspinia had already override the global bootstrap
    // require('bootstrap');        
} catch (e) {}

window._ = require('lodash');
window.Popper = require('popper.js').default;

import Vue from 'vue'

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.axios.defaults.headers.common['Authorization'] = `Bearer ${localStorage.token}`
window.axios.defaults.headers.common['Content-Type'] = 'application/json' 

/**
 * Some prototype override for leaflet
 */
import L from 'leaflet'

L.LatLng.prototype.bearingTo = function(other) {
	L.LatLng.DEG_TO_RAD = Math.PI / 180
	L.LatLng.RAD_TO_DEG = 180 / Math.PI

    var d2r  = L.LatLng.DEG_TO_RAD;    
    var r2d  = L.LatLng.RAD_TO_DEG;
    var lat1 = this.lat * d2r;
    var lat2 = other.lat * d2r;
    var dLon = (other.lng-this.lng) * d2r;
    var y    = Math.sin(dLon) * Math.cos(lat2);
    var x    = Math.cos(lat1)*Math.sin(lat2) - Math.sin(lat1)*Math.cos(lat2)*Math.cos(dLon);
    var brng = Math.atan2(y, x);
    // brng = parseInt( brng * r2d );
    brng = brng * r2d
    brng = (brng + 360) % 360;    
    return _.round(brng, 1);
};

L.LatLng.prototype.bearingWordTo = function(other) {
    var bearing = this.bearingTo(other);
    var bearingword = '';
    if      (bearing >=  22 && bearing <=  67) bearingword = 'NE';
    else if (bearing >=  67 && bearing <= 112) bearingword =  'E';
    else if (bearing >= 112 && bearing <= 157) bearingword = 'SE';
    else if (bearing >= 157 && bearing <= 202) bearingword =  'S';
    else if (bearing >= 202 && bearing <= 247) bearingword = 'SW';
    else if (bearing >= 247 && bearing <= 292) bearingword =  'W';
    else if (bearing >= 292 && bearing <= 337) bearingword = 'NW';
    else if (bearing >= 337 || bearing <=  22) bearingword =  'N';
    return bearingword;
};


/**
 * Mock
 */

// comment/uncomment this to enable/disable mockups
const MockAdapter = require('axios-mock-adapter');
window.mock = new MockAdapter(axios);
require('./data');	

/**
 * Moment global lib
 */

// these deserve its global
window.moment = require('moment-timezone');

// color helpers for console.log
window.conStyle = {
	boldWhite : "font-weight: bold; color: #fff; background-color: #404040; padding: 2px",
	boldRed : "font-weight: bold; color: #fff; background-color: #ff0000; padding: 2px",
    boldBlue : "font-weight: bold; color: #fff; background-color: #66ccff; padding: 2px",
    boldGreen : "font-weight: bold; color: #fff; background-color: #00cc99; padding: 2px"
}


/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

// global events
window.Event = new class {
	constructor(){
		this.vue = new Vue();
	}

	fire(event, data=null){
		this.vue.$emit(event, data);
	}

	listen(event, callback){
		this.vue.$on(event, callback);
	}

    remove(event){
        this.vue.$off(event)
    }	
	
}


// font awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { 
	faUserTie, faMapMarkedAlt, faUser, 
	faCompass, faInfo, faComments, 
	faLocationArrow, faShip, faHistory,
    faCalendar, faTimes, faMapMarker, faMapMarkerAlt,
	faSearch, faBackward, faForward,
	faFilter, faObjectUngroup, faFileAlt,
	faBell, faTachometerAlt, faTimesCircle, 
	faPlusCircle, faPlus, faPencilAlt,
	faTrashAlt,faInfoCircle, faRoute,
	faArrowLeft, faArrowCircleLeft, faSave, faRedoAlt,
    faChevronDown, faChevronLeft, faCog, faChartBar,
    faFileExcel, faFilePdf, faFileCsv, faEye,
    faEllipsisH, faChevronUp
}
from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'	

library.add(faSearch)
library.add(faUser)
library.add(faUserTie)
library.add(faCompass)
library.add(faMapMarkedAlt)
library.add(faMapMarkerAlt)
library.add(faInfo)
library.add(faComments)
library.add(faLocationArrow)
library.add(faShip)
library.add(faHistory)
library.add(faCalendar)
library.add(faTimes)
library.add(faBackward)
library.add(faForward)
library.add(faFilter)
library.add(faObjectUngroup)
library.add(faFileAlt)
library.add(faBell)
library.add(faTachometerAlt)
library.add(faTrashAlt)
library.add(faPencilAlt)
library.add(faTimesCircle)
library.add(faPlusCircle)
library.add(faInfoCircle)
library.add(faRoute)
library.add(faArrowLeft)
library.add(faArrowCircleLeft)
library.add(faSave)
library.add(faRedoAlt)
library.add(faChevronLeft)
library.add(faChevronDown)
library.add(faCog)
library.add(faChartBar)
library.add(faFileExcel)
library.add(faFilePdf)
library.add(faFileCsv)
library.add(faEye)
library.add(faEllipsisH)
library.add(faChevronUp)


Vue.component('font-awesome-icon', FontAwesomeIcon)

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

/* This is a comment to trigger changes in git */
