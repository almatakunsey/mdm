const VesselRouteDb = {
	
	getRoutesByVesselId(vesselId){		

		return new Promise((resolve, reject)=>{
			let req = indexedDB.open("mdm");
	        let db;

	        req.onsuccess = function(){
	        	
	        	db = req.result;
	        	let tx = db.transaction("routes", "readwrite");                
				let store = tx.objectStore("routes")

				let storeRequest = store.get(vesselId)
				storeRequest.onsuccess = function(){
					resolve(storeRequest.result)
				}			
	        }

	        req.onerror = function(error){
	        	reject(error)
	        }
		})			
	}
}

export {
	VesselRouteDb
}