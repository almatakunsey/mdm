import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}`

export default {

	/*
	getAtons
	 */
	getAtons(type){
		// param mmsi, fromDate, toDate
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/targets/aton?type=${type}&pageIndex=1&pageSize=1000`)
			.then((result)=>{
				// console.log(result)
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	}

}
