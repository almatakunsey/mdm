import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`


export default {

    createTenantFilter(data) {
        return new Promise((resolve, reject) => {
            axios.post(`${host}/api/TenantFilter`, data)
            .then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				console.log(error)
				reject(error)
			})
        })
    },

    getTenantFilters(page, size){		
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/TenantFilter`, {
				params: {
					pageIndex: page,
					pageSize: size
				}
			})
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
    },
    
    getTenantFilter(id) {
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/TenantFilter/${id}`)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	},

	assignTenantFilter(data){
        return new Promise((resolve, reject) => {
            axios.post(`${host}/api/TenantFilter/assign`, data)
            .then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				console.log(error)
				reject(error)
			})
        })
	}
}