import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {

	getUserPersonalization(token, tenantId){		
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Personalization`, 
				{
					params: {
						tenantId: tenantId ? tenantId : null,
						userLevel:'TenantUser'
					},
					headers: { Authorization: `Bearer ${token}` } 
				}				
			)
	        .then(({data:{result}})=>{            
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})
	},

	getTenantPersonalization(token, tenantId){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Personalization`, 
			{
				params:{
					tenantId: tenantId ? tenantId : null,
					userLevel:'TenantAdmin'
				},
				headers: { Authorization: `Bearer ${token}` } 
			})
	        .then(({data:{result}})=>{            
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})		
	},

	getHostPersonalization(token, tenantId){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Personalization`, 
			{
				params:{
					tenantId: tenantId ? tenantId : null,
					userLevel: 'HostAdmin'
				},
				headers: { Authorization: `Bearer ${token}` } 
			})
	        .then(({data:{result}})=>{            
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})		
	},

	getTenantKeyByTenantId(key, tenantId){

	},

	getTenantKeyByTenantName(key, tenantName){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Personalization/getKey`, 
			{
				params:{
					key,
					tenantName,
					userLevel:'TenantAdmin'					
				}				
			})
	        .then(({data:{result}})=>{            
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})		
	},

	getUserKeyByUserId(key, userId){
	
	},

	getHostKey(key){

	}


}