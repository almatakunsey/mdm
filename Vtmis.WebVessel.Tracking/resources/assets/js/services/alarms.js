import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {

	GetMyAlarms(){
		return new Promise((resolve, reject)=>{            
			axios.get(`${host}/api/services/app/Alarm/GetAll`)
            .then(({data:{result}})=>{                   
                resolve(result)                
            })            
            .catch((error)=>{
                reject(error)
            })
        })
	},

	deleteAlarm(id){
		return new Promise((resolve, reject)=>{
			axios.delete(`${host}/api/services/app/Alarm/DeleteAsync?id=${id}`)			
			.then( ({data:{result}}) =>{													
                resolve(result)
			})			
			.catch(( {response} )=>{							
				reject(response)				
			})
		})
		
	},

	GetAlarmById(id){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/app/Alarm/GetByIdAsync?id=${id}`)			
			.then( ({data:{result}} ) =>{													
	            resolve(result)
			})			
			.catch(( {response} )=>{							
				reject(response)				
			})
		})		
		
	},

	UpdateAlarm(data){
		 return new Promise((resolve, reject)=>{            
            axios.put(`${host}/api/services/app/Alarm/UpdateAsync`, data).then(
                ({data:{result}})=>{
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
        })
	},

	saveAlarm(data){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/services/app/Alarm/CreateAsync`, data).then(
                (result)=>{                    
                	resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
		})
	},

	getNotifications(page){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/app/Alarm/GetAllByUserIdAsync?page=1`).then(
				(result)=>{
					resolve(result)
				},
				(error)=>{
					reject(error)
				}
			)
		})
		
	}

}