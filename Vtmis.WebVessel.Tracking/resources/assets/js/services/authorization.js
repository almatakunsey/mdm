import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {

	/*
	getGrantedPermissions
	 */
	getGrantedPermissions(){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Permission`, {
				params: {
					isGranted: true
				}
			})			
			.then( ({data:{result}}) =>{													
                resolve(result)
			})			
			.catch(( {response} )=>{							
				reject(response)				
			})
		})		
	},

	/*
	getAllPermissions
	 */
	getAllPermissions(){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Permission`, {
				params: {
					isGranted: false
				}
			})			
			.then( ({data:{result}}) =>{													
                resolve(result)
			})			
			.catch(( {response} )=>{							
				reject(response)				
			})
		})		
	},

	/*
	getPermission
	 */
	getPermission(permissionName){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/Permission`, {
				permissionName, 
				fullname: false
			})			
			.then( ({data:{result}}) =>{													
                resolve(result)
			})			
			.catch(( {response} )=>{							
				reject(response)				
			})
		})		
	}

}