import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`
const mapTrackingApi = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}`
export default {

	/*
	GetFilterByUserId
	 */
	GetFilterByUserId(userId){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/filters`)			
			.then( ({data:{result}}) =>{													
                resolve(result)
			})			
			.catch(( {response} )=>{							
				reject(response)				
			})
		})
		
	},

	/*
	deleteFilter
	 */
	deleteFilter(filterId){
		return new Promise((resolve, reject)=>{
			 axios.delete(`${host}/api/Filters/${filterId}`).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{                    
                    reject(error)
                }
            )
		})
	},

	/*
	getFilters
	 */
	getFilters(
		includingFilterItems=true , 
		includingFilterDetails=true, 
		pageIndex=1, 
		pageSize=1000){	

		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Filters`, {
				params:{
					includingFilterItems,
					includingFilterDetails,
					pageIndex,
					pageSize	
				}
				
			}).then(
				(result)=>{
					resolve(result)
				},
				(error)=>{
					reject(error)
				}
			)
		})
	},

	/*
	getFilter
	 */
	getFilter(
		filterId,
		includingFilterItems=true , 
		includingFilterDetails=true
	){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Filters/${filterId}`, {
				params:{
					filterId,
					includingFilterItems,
					includingFilterDetails
				}
			})
		})
	},

	/*
	storeFilter
	 */
	storeFilter(data){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/Filters`, data).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{                	
                    reject(error)
                }
            )
		})
	},

	/*
	updateFilter
	 */
	updateFilter(data, filterId){		
		return new Promise((resolve, reject)=>{
			data.id = filterId
			axios.put(`${host}/api/Filters/${filterId}`, data).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
		})
	},

	/*
	getShipTypes
	 */
	getShipTypes(){
		return new Promise((resolve, reject)=>{
			axios.get(`${mapTrackingApi}/api/shipType`)
	        .then((result)=>{
	        	// console.log(result)  
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})
		
	},

	/*
	getVesselGroupList
	 */
	getVesselGroupList(){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/vesselGroup`)
	        .then((result)=>{
	        	console.log(result)  
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})
		
	},

	/*
	getVesselGroupList
	 */
	getVesselGroupDetails(id){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/vesselGroup/${id}`)
			.then( ({data:{result}}) =>{												
	            resolve(result)
	            // console.log("%cresult:", conStyle.boldRed, res)
			})			
			.catch(( {response} )=>{							
				reject(response)				
			})
		})
		
	},

	/*
	updateFilterGroup
	 */
	updateFilterGroup(data, id){		
		return new Promise((resolve, reject)=>{
			
			axios.put(`${host}/api/vesselGroup/${id}`, data).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
		})
	},

	/*
	createFilterGroup
	 */
	createFilterGroup(data){		
		return new Promise((resolve, reject)=>{
			
			axios.post(`${host}/api/vesselGroup/`, data).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
		})
	},

	/*
	deleteFilterGroup
	 */
	deleteFilterGroup(id){
		return new Promise((resolve, reject)=>{
			 axios.delete(`${host}/api/vesselGroup/${id}`).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{                    
                    reject(error)
                }
            )
		})
	},


}