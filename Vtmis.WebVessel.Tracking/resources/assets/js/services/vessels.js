import utilHelper from '../helpers/util'
const mapTrackingHost = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}`

const mapPortalApiPort = utilHelper.getMapPortalApiPort()
const mapPortalApiHost = `${utilHelper.getProtocol()}://${window.location.hostname}${ mapPortalApiPort ? ':' + mapPortalApiPort : '' }`
const webAdminApiPort = utilHelper.getWebAdminApiPort()
const webAdminApi = `${utilHelper.getProtocol()}://${window.location.hostname}:${webAdminApiPort}`

export default {

	getTenantVesselsList(){
		return new Promise((resolve, reject)=>{
			axios.get(`${webAdminApi}/api/services/app/vesselownedservice/GetVesselToTenantAssignedListByUserIdAsync`)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})
		
	},

	getInfoByVesselId(vesselId){
		return new Promise((resolve, reject)=>{
			axios.post(`${mapTrackingHost}/api/VesselFilters/GetTargetInfoByVesselId`, {vesselId})
	        .then(({data})=>{            	            
	            resolve(data)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})				
	},

	getImageUrl(vesselId){
		return new Promise((resolve, reject)=>{
			axios.get(`${mapTrackingHost}/api/VesselFilters/GetVesselImage/?mmsi=${vesselId}`)
			.then(({data})=>{				
				resolve(data)
			})
			.catch((error)=>{
				reject(error)
			})
		})
	},

	getInfoByImoCallSign(imo,callSign){
		return new Promise((resolve, reject)=>{
			axios.post(`${webAdminApi}/api/lloyds/details`, {imo,callSign})
	        .then(({data})=>{            	            
	            resolve(data)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})				
	},

	getRmsInfoByVesselId(vesselId){
		return new Promise((resolve, reject)=>{
			axios.post(`${mapTrackingHost}/api/targets/rms/${vesselId}`)
	        .then(({data})=>{            	            
	            resolve(data)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})		
	},

	getDdmsInfoByVesselId(vesselId){
		return new Promise((resolve, reject)=>{
			axios.post(`${mapTrackingHost}/api/targets/ddms/${vesselId}`)
	        .then(({data})=>{            	            
	            resolve(data)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})		
	},

	getTimelineByVesselId(){
		return new Promise((resolve, reject)=>{
			axios.post(`${mapTrackingHost}/api/targets/timeline/details`)
	        .then(({data})=>{            	            
	            resolve(data)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})		
	},

	getLightHouseInfoByVesselId(vesselId){
		return new Promise((resolve, reject)=>{
			axios.post(`${mapTrackingHost}/api/targets/lighthouse/${vesselId}`)
	        .then(({data})=>{            	            
	            resolve(data)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})		
	},

	advancedSearch(queryData){
		return new Promise((resolve, reject)=>{
			axios.post(`${mapPortalApiHost}/api/vessel/search`, queryData).then(
				({data})=>{										
					resolve(data)
				},
				(error)=>{
					reject(error)
				}
			)
		})
	},

	uploadImage(imgFormData){
		return new Promise((resolve, reject)=>{
			axios.post(`${webAdminApi}/api/Image/Upload`, imgFormData).then(
				({data})=>{										
					resolve(data)
				},
				(error)=>{
					reject(error)
				}
			)
		})	
	},

	loadImages(id){
		return new Promise((resolve, reject)=>{
			axios.get(`${webAdminApi}/api/Image`, {
				params:{
					imageId:id,
					imageType:'Target'
				}
			}).then(
				({data})=>{										
					resolve(data)
				},
				(error)=>{
					reject(error)
				}
			)
		})
	},

	deleteImage(id, name, path, type){
		return new Promise((resolve, reject)=>{
			axios.delete(`${webAdminApi}/api/Image/Delete`, {
				data:{
					imageId:id,
					imageType:type,
					name:[name]								
				}				
			}).then(
				({data})=>{										
					resolve(data)
				},
				(error)=>{
					reject(error)
				}
			)
		})	
	}

}