import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`
const genReport = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}`
export default {

	GetReportsTypeByUserId(){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/app/ReportTypeService/Get`)
	        .then(({data:{result}})=>{
	        // console.log(result)  
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})
		
		
	},

	saveReport(data){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/reports`, data).then(
                (result)=>{                    
                	resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
		})
	},

	getReports( 
		pageIndex=1, 
		pageSize=10){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Reports`, {
				params:{
					pageIndex,
					pageSize	
				}
				
			}).then(
				(result)=>{
					resolve(result)
				},
				(error)=>{
					reject(error)
				}
			)
		})
	},

	deleteReport(reportId){
		return new Promise((resolve, reject)=>{
			 axios.delete(`${host}/api/Reports/${reportId}`).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{                    
                    reject(error)
                }
            )
		})
	},

	updateReport(data, reportId){		
		return new Promise((resolve, reject)=>{
			// data.id = reportId
			axios.put(`${host}/api/Reports/${reportId}`, data).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
		})
	},

	getReportsById(reportId, isDetail=true){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Reports/${reportId}`, {
				params:{
					id:reportId,
					isDetail
				}
			})
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
	},

	generateReport(reportId,timeSpan,startDate,endDate){
		return new Promise((resolve, reject)=>{
			axios.get(`${genReport}/api/reports/generate/${reportId}?timeSpan=${timeSpan}&from=${startDate}&to=${endDate}`, {
			})
			.then((result)=>{
				resolve(result)
				// console.log(result)

			})	
			.catch((error)=>{
				reject(error)
			})
		})
	},
	generateReport1(reportId,enterTime,exitTime,pageIndex=1, pageSize=10){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/reports/generate`, 
				{
					reportId:reportId,
					enterTime:enterTime,
					exitTime:exitTime,
					pageIndex:pageIndex,
					pageSize:pageSize

				}).then((result)=>{                    
                	resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
		})
	},
	// generateReport1(reportId,enterTime,exitTime,pageIndex=1, pageSize=10){
	// 	return new Promise((resolve, reject)=>{
	// 		axios.post(`${genReport}/api/reports/generate/`, reportId,enterTime,exitTime,pageIndex,pageSize {
	// 		})
	// 		.then((result)=>{
	// 			resolve(result)
	// 			console.log(result)

	// 		})	
	// 		.catch((error)=>{
	// 			reject(error)
	// 		})
	// 	})
	// }
}