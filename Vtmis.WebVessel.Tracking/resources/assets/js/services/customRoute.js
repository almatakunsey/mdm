import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {	

	storeRoute(data){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/Routes`, data)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
		
	},

	updateRoute(data){
		return new Promise((resolve, reject)=>{
			axios.put(`${host}/api/Routes/${data.id}`, data)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
		
	},

	getRoute(id){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Routes/${id}`)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	},

	getRoutes(page, size){		
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Routes`, {
				params: {
					pageIndex: page,
					pageSize: size
				}
			})
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
	},

	removeRoute(id){
		return new Promise((resolve, reject)=>{
			axios.delete(`${host}/api/Routes/${id}`)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})
	},

	getRouteEta(routeId){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/routes/${routeId}/eta`)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	},

	getVesselEta(routeId,mmsi){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/routes/${routeId}/eta/${mmsi}`)
			.then((result)=>{
				resolve(result)
				// console.log(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	},

}