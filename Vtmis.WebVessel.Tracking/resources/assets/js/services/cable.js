import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {	

	storeCable(data){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/Cables`, data)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
		
	},

	updateCable(data){
		return new Promise((resolve, reject)=>{
			axios.put(`${host}/api/Cables/${data.id}`, data)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
		
	},

	getCable(id){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Cables/${id}`)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	},

	getCables(page, size){		
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/Cables`, {
				params: {
					pageIndex: page,
					pageSize: size
				}
			})
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
	},

	removeCable(id){		
		return new Promise((resolve, reject)=>{
			axios.delete(`${host}/api/Cables/${id}`)
			.then((result)=>{
				console.log(result)
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})
	}	

}