import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {

	storeLocation(data){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/services/app/Location/CreateAsync`, data)
            .then((result) => {
                resolve(result)
            })
            .catch((error) => {
                reject(error)
            })
		})
	},

    updateLocation(data){        
        return new Promise((resolve, reject)=>{
            axios.put(`${host}/api/services/app/Location/UpdateAsync`, data)
            .then((result) => {
                resolve(result)
            })
            .catch((error) => {
                reject(error)
            })
        })
    }, 

	getLocations(){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/app/Location/GetListAsync`)
            .then((result) => {
                resolve(result)
            })
            .catch((error) => {
                reject(error)
            })
		})
	},

    deleteLocation(id){
        return new Promise((resolve, reject)=>{
            axios.delete(`${host}/api/services/app/Location/DeleteAsync?id=${ id }`)
            .then((result) => {
                resolve(result)
            })
            .catch((error) => {
                reject(error)
            })
        })
        
    }

}