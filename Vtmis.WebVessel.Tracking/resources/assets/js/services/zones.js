import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {

	GetZonesByUserId(userId){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/app/ZoneService/GetAllAsync`, {
				params:{userId}
			})
	        .then(({data:{result}})=>{            
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})
		
		
	},	

	deleteZone(zoneId){
		return new Promise((resolve, reject)=>{
			 axios.delete(`${host}/api/services/app/ZoneService/Delete?id=${zoneId}`).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{                    
                    reject(error)
                }
            )
		})
	},

	storeZoneDef(data){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/services/app/ZoneService/Create`, data)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
		
	},

	updateZoneDef(data){
		return new Promise((resolve, reject)=>{
			axios.put(`${host}/api/services/app/ZoneService/Update`, data)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
	},

	
	getZoneDef(zoneId){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/app/ZoneService/GetById`, {params:{id:zoneId}})
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
	}




}