import utilHelper from '../helpers/util'
const mapTrackingApi = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}`
// const get = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}`

export default {

	getWeatherStationList(){
		return new Promise((resolve, reject)=>{
			axios.get(`${mapTrackingApi}/api/methydro/station`)
	        .then(({data:{result}})=>{
	        	// console.log(result)  
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})		
	},

	getGaugesList(mmsi){
		// param mmsi, fromDate, toDate
		return new Promise((resolve, reject)=>{
			axios.get(`${mapTrackingApi}/api/methydro/${mmsi}`)
			.then((result)=>{
				// console.log(result)
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	},

	getPublicWeatherStationList(token){
		return new Promise((resolve, reject)=>{
			axios.get(`${mapTrackingApi}/api/methydro/station`,
			{
    			headers: {
        			"Authorization" : `Bearer ${token}`
      			}
    		})
	        .then(({data:{result}})=>{
	        	// console.log(result)  
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})		
	},

	getPublicGaugesList(mmsi,token){
		
		return new Promise((resolve, reject)=>{
			axios.get(`${mapTrackingApi}/api/methydro/${mmsi}`,
    		{
    			headers: {
        			"Authorization" : `Bearer ${token}`
      			}
    		})
	        .then((result)=>{
	        	// console.log(result)  
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})
	}
}