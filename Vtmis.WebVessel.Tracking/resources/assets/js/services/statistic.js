import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}`

export default {
	
	getRmsHistorical(mmsi,startDate,endDate){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/targets/rms/${mmsi}/historical?startDate=${startDate}&endDate=${endDate}`)
			.then((result)=>{
				// console.log(result)
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	},

	getWeatherHistorical(mmsi,startDate,endDate){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/methydro/${mmsi}/historical?startDate=${startDate}&endDate=${endDate}`)
			.then((result)=>{
				// console.log(result)
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	},

	getPublicWeatherHistorical(mmsi,token,startDate,endDate){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/methydro/${mmsi}/historical?startDate=${startDate}&endDate=${endDate}`,
			{
    			headers: {
        			"Authorization" : `Bearer ${token}`
      			}
    		})
			.then((result)=>{
				// console.log(result)
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	},

	getDdmsHistorical(mmsi,startDate,endDate){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/targets/ddms/${mmsi}/historical?startDate=${startDate}&endDate=${endDate}`)
			.then((result)=>{
				// console.log(result)
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	},

}
