import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {

	login(userNameOrEmailAddress, password, tenantName){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/TokenAuth/Authenticate`, {
				userNameOrEmailAddress,
				password,
				tenantName
			})
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})
		
	},

	logout(){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/account/logout`)
			.then((result)=>{										
				resolve(result)
			})
			.catch( (error)=>{				
				reject(error)
			})
		})
	},

	checkSession(){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/app/Session/CheckSession`)
			.then((result)=>{										
				resolve(result)
			})
			.catch( (error)=>{				
				reject(error)
			})
		})
	}

}