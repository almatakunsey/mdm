import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {

	/*
	loadWrems
	 */
	loadWrems(){		
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/wrems/details`)
			.then(({data})=>{				
				resolve(data)
			})	
			.catch((error)=>{
				reject(error)
			})	
		})
	}

}
