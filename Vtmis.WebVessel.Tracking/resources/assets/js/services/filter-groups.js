import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {
	getAllGroupType(){
		return new Promise( (resolve, reject)=>{

			axios.get(`${host}/api/services/app/FilterGroupService/GetAll`)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})

		} )
	},
	getGroupById(id){
		return new Promise( (resolve, reject)=>{

			axios.get(`${host}/api/services/app/FilterGroupService/GetById`, {
				params:{id}
			})
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})

		} )	
	}
}