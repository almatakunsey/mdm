import utilHelper from '../helpers/util'
const host = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

export default {

	getAllUser(){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/app/User/GetAll`)			
			.then( ({data:{result}}) =>{													
                resolve(result)
			})			
			.catch(( {response} )=>{							
				reject(response)				
			})
		})		
	},

	getUserDetails(id){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/app/User/Get`, {
				params:{id}
			})
			.then( ({data:{result}}) =>{													
	            resolve(result)
			})			
			.catch(( {response} )=>{							
				reject(response)				
			})
		})

	},

	storeNewUser(data){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/services/app/User/Create`, data)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
		
	},

	updateUser(data){
		return new Promise((resolve, reject)=>{
			axios.put(`${host}/api/services/app/User/Update`, data)
			.then((result)=>{
				resolve(result)
			})	
			.catch((error)=>{
				reject(error)
			})
		})
		
	},

	removeUser(id){
		return new Promise((resolve, reject)=>{
			axios.delete(`${host}/api/services/app/User/Delete?id=${id}`)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})
	},

	deactivateUser(id){
		return new Promise((resolve, reject)=>{
			axios.post(`${host}/api/services/app/User/DeactivateUser?id=${id}`)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})
	},
	

	getListOfRoleByUserId(){
		return new Promise((resolve, reject)=>{
			axios.get(`${host}/api/services/app/User/GetRoles`)
	        .then((result)=>{
	        // console.log(result)  
	            resolve(result)
	        })            
	        .catch((error)=>{
	            reject(error)
	        })
		})
	}
	

}