import utilHelper from '../helpers/util'
const mapTrackingApi = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}`

export default {
	getPlayback(vesselId, startDateFormatted, endDateFormatted){
		return new Promise((resolve, reject)=>{
			axios.get(`${mapTrackingApi}/api/Playbacks`, {
				params:{
					mmsi: vesselId,
					fromDateTime: startDateFormatted,
					toDateTime: endDateFormatted
				}
			})
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})
	}
}