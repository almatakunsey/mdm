import utilHelper from '../helpers/util'
const mapPortalApi = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getMapPortalApiPort()}`

export default {
	
	updateRoutes(vesselIds){
		return new Promise((resolve, reject)=>{
			axios.post(`${mapPortalApi}/api/VesselRouteHistory`, vesselIds)
			.then(({data})=>{			
				resolve(data)
			})
			.catch((error)=>{
				reject(error)
			})
		})
	}
	

}