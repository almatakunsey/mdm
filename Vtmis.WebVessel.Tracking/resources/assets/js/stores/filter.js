const state = {
	enabled: false
}

const getters = {
	filterEnabled: state => state.enabled
}

const mutations = {
	toggleFilter(state, condition){ 
		state.enabled = condition;		
	}
}

export const filter = {
	state,
	getters,
	mutations,
	namespaced: true
}