export const encData = {
	baseUrl: `https://data.hydro.gov.my/server/rest/services/NauticalProducts/MyENC_WebChartService/MapServer/exts/MaritimeChartService/WMSServer?token=${localStorage.encToken}`,	
    layers: {
        all: 
        [
            {        
                name: 0,
                title: "Information about the chart display",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 1,
                title: "Natural and man-made features, port features",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 2,
                title: "Depths, currents, etc",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 3,
                title: "Seabed, obstructions, pipelines",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 4,
                title: "Traffic routes",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 5,
                title: "Special areas",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 6,
                title: "Buoys, beacons, lights, fog signals, radar",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 7,
                title: "Services and small craft facilities",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            }
        ],

        base:
        [
            {
                name: 1,
                title: "Natural and man-made features, port features",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 3,
                title: "Seabed, obstructions, pipelines",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 4,
                title: "Traffic routes",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            }
        ],

        standard:
        [
            {
                name: 1,
                title: "Natural and man-made features, port features",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 3,
                title: "Seabed, obstructions, pipelines",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 4,
                title: "Traffic routes",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 5,
                title: "Special areas",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 6,
                title: "Buoys, beacons, lights, fog signals, radar",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            },
            {
                name: 7,
                title: "Services and small craft facilities",
                defaultVisibility: true,
                parentLayerId: -1,
                subLayerIds: "N/A",
                minScale: 0,
                maxScale: 0
            }
        ],

        // waterOnly:
        // [
        //     {
        //         name: 1,
        //         title: "Natural and man-made features, port features",
        //         defaultVisibility: true,
        //         parentLayerId: -1,
        //         subLayerIds: "N/A",
        //         minScale: 0,
        //         maxScale: 0
        //     }
        // ]

    }
}