import { encData } from './data'

const state = {
	baseUrl: encData.baseUrl,
	layers: encData.layers,
	selectedLayer: '', // base, standard, all, waterOnly
	waterOnlySelected: false
}

const getters = {
	baseUrl : state => state.baseUrl,	
	selectedLayer: state => state.selectedLayer,
	layers(state) {
		const layers = state.layers[state.selectedLayer];
		if (state.waterOnlySelected) return layers.filter(layer => layer.name != 1);
		return layers;
	},
	isWaterOnlySelected(state) {
		return state.waterOnlySelected;
	}
}

const mutations = {
	setSelectedLayer(state, layer){		
		state.selectedLayer = layer
	},
	clearSelectedLayer(state){		
		state.selectedLayer = ''
	},
	setSelectedWaterOnly(state) {
		state.waterOnlySelected = !state.waterOnlySelected;
	}	
}

export const enc = {
	state,
	getters,
	mutations,
	namespaced: true
}
