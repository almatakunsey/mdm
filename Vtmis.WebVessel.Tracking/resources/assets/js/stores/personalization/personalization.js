const dataPristine = {
	table:{
		atonList:{
			columns: ['mmsi', 'aisName', 'lastReport6', 'lastReport8', 'lastReport21', 'type', 'virtualAton', 'offPosition68', 'offPosition21', 'latitude', 'longitude'],
			sort: {order:'', field:''}	
		},
		atonListMeteo:{
			columns: ['mmsi', 'aisName', 'name', 'latitude', 'longitude', 'type', 'virtualAton', 'offPosition68', 'offPosition21', 'airTemp', 'airPres', 'wndSpeed', 'wndGust', 'wndDir', 'lastReport6', 'lastReport8', 'lastReport21'],
			sort: {order:'', field:''}	
		},
		atonListMonitoring:{
			columns: ['mmsi', 'aisName', 'name', 'latitude', 'longitude', 'type', 'virtualAton', 'offPosition68', 'offPosition21', 'racon', 'racoN21', 'light', 'light21', 'health', 'health21', 'analogueInt', 'analogueExt1', 'analogueExt2', 'b7', 'b6', 'b5', 'b4', 'b3', 'b2', 'b1', 'b0', 'voltageData', 'currentData', 'battery', 'photoCell', 'lastReport6', 'lastReport8', 'lastReport21'],
			sort: {order:'', field:''}	
		}
	}
}

const state = {
	data: _.cloneDeep(dataPristine)
}

const getters = {
	personalizationData: state => state.data
}

const mutations = {
	setData(state, data){ state.data = data },
	setTableColumns(state, {tableName, columns}){
		state.data.table[tableName].columns = columns
	},
	setTableSort(state, {tableName, order, field}){
		state.data.table[tableName].sort = {order, field}
	} 
}

export const personalization = {
	state,
	getters,
	mutations,
	namespaced: true
}