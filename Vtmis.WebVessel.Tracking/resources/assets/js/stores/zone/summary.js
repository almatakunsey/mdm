const state = {
    enabled: false,
    zoneNameEnabled: false    
}

const mutations = {        
    toggleZoneSummary(state, condition){state.enabled = condition},
    toggleZoneNameEnabled(state, condition){state.zoneNameEnabled = condition}
}

const getters = {    
    enabled: state => state.enabled,
    zoneNameEnabled: state => state.zoneNameEnabled
}

export const summary = {
    state,
    getters,  
    mutations,
    namespaced: true
}