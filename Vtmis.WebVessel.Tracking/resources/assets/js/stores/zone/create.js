import utilHelper from '../../helpers/util'
import async from 'async'
import _ from 'lodash'

import zoneService from '../../services/zones'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

/*
state
 */
let state = {
    enabled: false
}

/*
mutations
 */
let mutations = {
    toggleZoneCreate(state, condition){state.enabled = condition}    
}

/*
getters
 */
let getters = {
    enabled: state => state.enabled    
}

/*
actions
 */
let actions = {
	storeZoneDef({commit, getters, rootGetters}){     	

        return new Promise((resolve, reject)=>{

            async.waterfall([
                (callback)=>{
                    // get a clone of the object
                    const formData = _.cloneDeep(rootGetters['zone/form/formData'])                    

                    // const zoneItemDtosLength = formData.zoneItemDtos.length
                    // for(let i=0; i<zoneItemDtosLength; i++){
                    //     formData.zoneItemDtos[i].order = i
                    // }
                    
                    // clean up the formData to properly reflect the api
                    formData.zoneItemDtos.forEach((item)=>{
                        delete item.id
                        item.logitude = item.longitude
                        delete item.longitude            
                    })

                    formData.zoneItems = _.cloneDeep(formData.zoneItemDtos)
                    delete formData.zoneItemDtos

                    // prepare data according to request
                    const data = {
                        userId: localStorage.userId,
                        zoneDto: formData
                    }  

                    // console.log(data)                   

                    callback(null, data)

                },
                (data, callback)=>{

                    zoneService.storeZoneDef(data).then(
                        (result)=>{                              
                            resolve(result)
                        },
                        (error)=>{                            
                            reject(error)
                        }
                    )            
                }
            ]) // end waterfall
        }) // promise return
    }
}

/*
export
 */
export const create = {
    state,
    getters,      
    mutations,  
    actions,  
    namespaced: true
}