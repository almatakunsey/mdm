import utilHelper from '../../helpers/util'
import async from 'async'
import _ from 'lodash'

const zoneItemDtosPristine = {
    id: utilHelper.genRandom(32),
    order: 0,
    latitude:'',
    longitude:'',
    highlight: false

}

const formDataPristine = {
    name:'',
    isEnable: true,
    radius: 1,
    bulkEdit: false,
    bulkEditData: '',
    colour: '#ff8080',
    isFill: true,
    zoneType: 0,
    zoneItemDtos: [_.cloneDeep(zoneItemDtosPristine)]
}

/*
state
 */
const state = {    
    error: null,    
    formData : _.cloneDeep(formDataPristine),     
    zoneTypeOptions: [
        { text: 'Circle', id: 0 },
        { text: 'Polygon', id: 1 },
        { text: 'Line L', id: 2 },
        { text: 'Line R', id: 3 }
    ],
    latLngFormat: [{text: 'Decimal Degrees', id: 1}]
}

/*
getters
 */
const getters = {    
    formData: state => state.formData,
    zoneTypeOptions: state => state.zoneTypeOptions,
    latLngFormat: state => state.latLngFormat    
}

/*
mutations
 */
const mutations = {
	setFormData(state, {key, value}){                
        state.formData[key] = value
    },
	replaceFormData(state, data){state.formData = data},
	updateDto(state, {type, value, id}){ 
        const dataIndex = _.findIndex(state.formData.zoneItemDtos, {id})       
        state.formData.zoneItemDtos[dataIndex][type] = value        
    },
    addNewDtoItem(state, {latitude = '', longitude = ''}){        

        let newDtoItem = _.cloneDeep(zoneItemDtosPristine)

        // get next order to assign       
        let nextOrder = 0        
        if(state.formData.zoneItemDtos.length){
            nextOrder = _.maxBy(state.formData.zoneItemDtos, 'order').order + 1
        }           

        newDtoItem.id = utilHelper.genRandom(32)
        newDtoItem.latitude = latitude
        newDtoItem.longitude = longitude
        newDtoItem.order = nextOrder                
        state.formData.zoneItemDtos.push(newDtoItem)        
    },
    setZoneItemDtos(state, itemDtos){        
        state.formData.zoneItemDtos = itemDtos.map( ({latitude, longitude, order}) => {
            return {id: utilHelper.genRandom(32), latitude, longitude, order, highlight:false}
        })                
    }, 
    clearZoneItemDtos(state){
        state.formData.zoneItemDtos = []
    },   
    removeDtoItem(state, id){
        const dataIndex = _.findIndex(state.formData.zoneItemDtos, {id})       
        state.formData.zoneItemDtos.splice(dataIndex, 1)    
    },
    resetFormData(state){         
        state.formData = _.cloneDeep(formDataPristine)
    },
    resetBulkEditDataOnZoneTypeChange(state){         
        const coords = state.formData.bulkEditData.split(/\r?\n/g).map(coord => coord.replace(/\s/g, ""))
        function stichCoords(coords){
            let itemDtos = '';            
            _.each(coords, (c, index)=>{itemDtos = `${itemDtos}${index ? '\n' : ''}${c}`})
            return itemDtos
        }
        if(state.formData.zoneType == 0 && coords.length > 1){
            state.formData.bulkEditData = stichCoords(coords.splice(0,1))
        }
        else if(state.formData.zoneType == 1 && coords.length > 3){     
            state.formData.bulkEditData = stichCoords(coords.splice(0,3))
        }
        else if((state.formData.zoneType == 2 || state.formData.zoneType == 3) && coords.length > 2){
            state.formData.bulkEditData = stichCoords(coords.splice(0,2))
        }
    },
    resetDtoItemOnZoneTypeChange(state){
        function addNewDtoItem(){
            let newDtoItem = _.cloneDeep(zoneItemDtosPristine)
            newDtoItem.id = utilHelper.genRandom(32)
            newDtoItem.latitude = ''
            newDtoItem.longitude = ''
            state.formData.zoneItemDtos.push(newDtoItem) 
        }
        // circle
        if(state.formData.zoneType == 0 && state.formData.zoneItemDtos.length > 1){
            state.formData.zoneItemDtos = state.formData.zoneItemDtos.splice(0, 1)
        }
        // polygon
        else if(state.formData.zoneType == 1){            
            if(state.formData.zoneItemDtos.length > 3){
                state.formData.zoneItemDtos = state.formData.zoneItemDtos.splice(0,3)    
            }
            else if(state.formData.zoneItemDtos.length <= 2){                
                const zoneItemDtosLength = state.formData.zoneItemDtos.length
                for(let i=0; i < 3 - zoneItemDtosLength; i++){ addNewDtoItem() }
            }            
        }
        // line
        else if(state.formData.zoneType == 2 || state.formData.zoneType == 3){
            if(state.formData.zoneItemDtos.length > 2){                
                state.formData.zoneItemDtos = state.formData.zoneItemDtos.splice(0, 2)    
            }
            else if(state.formData.zoneItemDtos.length == 1){                
                addNewDtoItem()
            }
        }
    },    

    //
    // zoneDesignerRelated    
    // 
    
    removeFaultyDtoItems(state){                       
        state.formData.zoneItemDtos = state.formData.zoneItemDtos.filter((item) => {
            return item.latitude && item.longitude
        })

    },

    toggleDtaItemHighlight(state, {id, value}){        
        state.formData.zoneItemDtos[_.findIndex(state.formData.zoneItemDtos, {id})].highlight = value               
    },
    clearDtaItemHighlight(state){
        if(state.formData.zoneItemDtos.length){
            state.formData.zoneItemDtos.forEach((item)=>{
                item.highlight = false
            })
        }
    }
}

/*
actions
 */
const actions = {
    convertBulkEditToDtoItem({commit, getters}){
        let coords = getters.formData.bulkEditData
        // split by newline
        .split(/\r?\n/g)         
        // remove space and collect into object
        .map((coord, i) => {
            coord.replace(/\s/g, "")
            return {
                order: i+1,
                latitude: coord.split(',')[0], 
                longitude: coord.split(',')[1]
            }
        } )                 

        // if user change to circle
        if(getters.formData.zoneType == 0){coords = coords.splice(0,1)}

        // if user change to line
        else if(getters.formData.zoneType == 2 || getters.formData.zoneType == 3){coords = coords.splice(0,2)}

        commit('setZoneItemDtos', coords)        
    },
    convertDtoItemToBulkEdit({commit, getters}){
        // just simple check to make sure the data is properly formatted
        if(!getters.formData.zoneItemDtos.length || 
            (
                getters.formData.zoneItemDtos.length == 1 &&
                (
                    getters.formData.zoneItemDtos[0].latitude == '' || 
                    getters.formData.zoneItemDtos[0].longitude == ''   
                )
            )
        ){return false}
        // we going to replicate zoneItemDtos data into bulkEdit data        
        let itemDtos = getters.formData.zoneItemDtos
        .filter( i => i.latitude || i.longitude)

        itemDtos = _.sortBy(itemDtos, ['order'])
        .map( ({latitude, longitude}, index) => `${index ? '\n' : ''}${latitude}, ${longitude}`)
        .join("")              

        commit('setFormData', {key:'bulkEditData', value: itemDtos})        
    },

    cleanResetForm({commit, getters}){
        commit('removeFaultyDtoItems')
        // if dto items cleaned to the point it get empty, reset the form
        if(!getters.formData.zoneItemDtos.length){
            commit('resetFormData')
        }
    }    

}

/*
export
 */
export const form = {
    state,
    getters,    
    mutations,
    actions,
    namespaced:true
}





