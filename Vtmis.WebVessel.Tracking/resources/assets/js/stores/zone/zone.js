import {summary} from './summary'
import {create} from './create'
import {edit} from './edit'
import {form} from './form'
import _ from 'lodash'

import zoneService from '../../services/zones'

import async from 'async'
import utilHelper from '../../helpers/util' 
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const zonesPristine = {
    circles: [],
    polygons: [],
    lines: []
}

/*
state
 */
const state = {     
    zonesDef: [],
    zones: _.cloneDeep(zonesPristine),    
    designer:{
        enabled:false,
        polygonClosed: false,
        fromDesigner: false        
    }
}

/*
getters
 */
const getters = {     
    zoneDesigner: state => state.designer,  
    zoneDesignerEnabled: state => state.designer.enabled,
    fromDesigner: state => state.designer.fromDesigner,     
    circles : state => _.filter(state.zonesDef, def => def.zoneType == 0 ), 
    polygons : state => {
        
        const polygons = _.cloneDeep( _.filter(state.zonesDef, def => def.zoneType == 1 ) )        

        return _.map(polygons, p => {            
            p.zoneItemDtos = _.map(_.sortBy(p.zoneItemDtos, ["order"]), ({latitude, logitude})=> [latitude, logitude] )
            return p
        })
    },
    lines : state => {    
        const lines = _.cloneDeep(_.filter(state.zonesDef, def => def.zoneType == 2 || def.zoneType == 3))         
        return _.map(lines, p => {
            p.zoneItemDtos = _.map(_.sortBy(p.zoneItemDtos, ["order"]), ({latitude, logitude})=> [latitude, logitude] )
            return p
        })
    },
    zonesDef : state => state.zonesDef,
    zones: state => state.zones
}

/*
mutations
 */
const mutations = {        
    setZonesDef(state, zones){state.zonesDef = zones},
    clearZonesDef(state){state.zonesDef = []},
    clearZones(state){state.zones = _.cloneDeep(zonesPristine)},        
    addZone(state, {type, zone}){ state.zones[type].push(zone)},
    
    // designer
    toggleZoneDesigner(state, condition){state.designer.enabled = condition},       
    zoneDesignerToggleClosePolygon(state){ state.designer.polygonClosed = !state.designer.polygonClosed },
    toggleFromDesigner(state, condition){state.designer.fromDesigner = condition }

}

/*
actions
 */
const actions = {    
    getMyZonesList({commit}){     
        return new Promise((resolve, reject)=>{            
            zoneService.GetZonesByUserId(localStorage.userId)
            .then((result)=>{                
                resolve(result)
            })            
            .catch((error)=>{
                reject(error)
            })
        })
    },
    updateMyZones({commit}, {zoneIds}){        

        const zoneDetailGetterFunc = zoneIds.map((id)=>{
            return (callback)=>{                
                zoneService.getZoneDef(id).then(
                    ({data})=>{
                       callback(null, data)
                    },
                    (error)=>{
                       callback(error)
                    }
                )
            }
        })

        return new Promise((resolve, reject)=>{
            async.parallel(zoneDetailGetterFunc, (error, result) => {
                if(error){                    
                    reject(error)
                }
                else{                                        
                    commit('setZonesDef', result.map( re => re.result  ))
                }
            })    
        })        
    },
    refreshMyZonesDef({commit, dispatch}){          
        return new Promise((resolve, reject)=>{
            async.waterfall([
                // get list of zone ids
                (callback)=>{                
                    dispatch('getMyZonesList').then(
                        (result)=>{                            
                            callback(null, result)
                        },
                        (error)=>{                            
                            callback(error)
                        }
                    )
                },
                // foreach zone ids, get the details
                (result, callback)=>{                   
                    if(result.length){                        
                        const zoneIds = _.map(result, (re) => re.id )
                        dispatch('updateMyZones', {zoneIds})
                        .catch((error)=>{
                            if(error){callback(error)}
                            else{callback()}
                        })
                    }                   
                }
            ], (error, result)=>{                
                if(error){console.error(error); reject(error)}
                else{resolve(result)}                 
            })
        })
        
    },
   
    deleteZone({commit}, zoneId){        
        return new Promise((resolve, reject)=>{ 
            zoneService.deleteZone(zoneId).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{                    
                    reject(error)
                }
            )
        })        
    }


}

export const zone = {   
    state,
    getters,
    mutations,
    actions,
    namespaced: true,
    modules: {
        summary,
        create,
        edit,
        form
    }   
}