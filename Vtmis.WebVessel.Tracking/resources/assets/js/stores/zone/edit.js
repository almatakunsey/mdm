import utilHelper from '../../helpers/util'
import async from 'async'
import * as form from './form'

import zoneService from '../../services/zones'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const state = {
    editedZoneId: null,
    enabled: false,        
}

const mutations = {
    setEditedZoneId(state, id){state.editedZoneId = id},
    toggleZoneEdit(state, condition){state.enabled = condition}        
}

const getters = {
    enabled: state => state.enabled,    
    editedZoneId: state => state.editedZoneId 
}

const actions = {
    loadZone({commit, getters}){
        return new Promise((resolve, reject)=>{            
            zoneService.getZoneDef(getters.editedZoneId).then(
                ({data:{result}})=>{                    
                    
                    // format the result to fit in the formData\
                    const formattedData = {
                        name:result.name,
                        id: getters.editedZoneId,
                        isEnable: result.isEnable,
                        radius: result.radius,
                        bulkEdit: false,
                        bulkEditData: '',
                        colour: result.colour,
                        isFill: result.isFill,
                        zoneType: result.zoneType,
                        zoneItemDtos: result.zoneItemDtos.map(({latitude, logitude, order})=>{
                            return {
                                id: utilHelper.genRandom(32),
                                order,
                                latitude,
                                longitude:logitude
                            }
                        })
                    }                     

                    commit('zone/form/replaceFormData', formattedData, {root:true})                     

                    resolve(formattedData)                 
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    },
    updateZone({commit, getters, rootGetters}){
        
        return new Promise((resolve, reject)=>{            
            
            async.waterfall([
                (callback)=>{
                    // get a clone of the object
                    const formData = _.cloneDeep(rootGetters['zone/form/formData'])

                    // const zoneItemDtosLength = formData.zoneItemDtos.length
                    // for(let i=0; i<zoneItemDtosLength; i++){
                    //     formData.zoneItemDtos[i].order = i
                    // }

                    // clean up the formData to properly reflect the api
                    formData.zoneItemDtos.forEach((item)=>{
                        delete item.id                        
                        item.logitude = item.longitude
                        delete item.longitude            
                    })
                    formData.zoneItems = formData.zoneItemDtos;
                    // prepare data according to request
                    const data = {
                        userId: localStorage.userId,
                        zoneDto: formData
                    } 

                    callback(null, data)
                },
                (data, callback)=>{                                     
                    zoneService.updateZoneDef(data).then(
                        (result)=>{
                            resolve(result)
                        },
                        (error)=>{
                            reject(error)
                        }
                    )
                }
            ])           

        })
    }
}

export const edit = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}