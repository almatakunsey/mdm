import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

import { app } from './App/app'
import { auth } from './auth/auth'
import { map } from './map'
import { vessel } from './vessel'
import { vesselDetails } from './vesselDetails/vesselDetails'
import { vesselsearch } from './search/vesselsearch'
import { locations } from './locations/locations'
import { signalr } from './signalr'
import { playback } from './playback'
import { filter } from './filter'
import { filters } from './filters/filters'
import { zone } from './zone/zone'
import { alarm } from './alarm/alarm'
import { vesselList } from './vesselList/vesselList'
import { status } from './status/status'
import { reports } from './reports/reports'
import { users } from './users/users'
import { customRoute } from './customRoute/customRoute'
import { tools } from './tools/tools'
import { knowledgebase } from './knowledgebase/knowledgebase'
import { weather } from './weather/weather'
import { cable } from './cable/cable'
import { aton } from './aton/aton'
import { enc } from './enc/enc'
import { statistic } from './statistic/statistic'
import { personalization } from './personalization/personalization'
import { wrems } from './wrems/wrems'
import { eics } from './eics/eics'
import { tenantFilter } from './tenantFilter/tenantFilter'

export const store = new Vuex.Store({
	modules: {
    	app,
    	auth,
    	map,
    	// vessel,
        vesselDetails,
        vesselsearch,
        locations,
        signalr,
        playback,
        filter,
        filters,
        zone,
        alarm,
        vesselList,
        status,
        reports,
        users,
        customRoute,
		tools,
        knowledgebase,
        weather,
        cable,
        aton,
        enc,
        statistic,
        personalization,
        wrems,
        eics,
        tenantFilter
	}
})
