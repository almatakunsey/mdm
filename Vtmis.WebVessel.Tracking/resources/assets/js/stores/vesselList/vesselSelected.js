const state = {
	enabled: false	
}

const getters = {	
	enabled: state => state.enabled	
}

const mutations = {	
	toggleEnable(state, condition){
		state.enabled = !state.enabled
	}	
}

export const vesselSelected = {	
	state,
	getters,
	mutations,	
	namespaced: true
}