const state = {		
	enabled: false,
	nameBoxesDefinition: new Map(),
	nameBoxes: new Map(),
	newNameBoxesDefinition: new Map(),	
	stateChangeTracker: 0,
	flags: new Map()	
}

const getters = {
	enabled: state => state.enabled,
	inBoundVesselIds: state => state.inBoundVesselIds,
	nameBoxes: state => state.stateChangeTracker && state.nameBoxes,	
	nameBoxesDefinition: state => state.stateChangeTracker && state.nameBoxesDefinition,	
	nameBoxDefinition: (state) => (id) => {		
		return state.stateChangeTracker && state.nameBoxesDefinition.get(id)
	},	
	newNameBoxesDefinition: state => state.stateChangeTracker && state.newNameBoxesDefinition,	
	newNameBoxDefinition: (state) => (id) => {
		return state.stateChangeTracker && state.newNameBoxesDefinition.get(id)
	},
	flags: state => state.stateChangeTracker && state.flags

}

const mutations = {	
	toggleEnable(state){state.enabled = !state.enabled},		
	clearNameBoxes(state){state.nameBoxes.clear(); state.stateChangeTracker += 1},
	clearNameBoxesDefinition(state){state.nameBoxesDefinition.clear(); state.stateChangeTracker += 1},
	addNameBox(state, {vesselId, box}){
		state.nameBoxes.set(vesselId, box)
		state.stateChangeTracker += 1
	},
	addNameBoxDefinition(state, {vesselId, boxDefinition}){		
		state.nameBoxesDefinition.set(vesselId, boxDefinition)
		state.stateChangeTracker += 1
	},
	clearNewNameBoxesDefinition(state){
		state.newNameBoxesDefinition.clear(); 
		state.stateChangeTracker += 1
	},	
	addNewNameBoxDefinition(state, {vesselId, boxDefinition}){			
		state.newNameBoxesDefinition.set(vesselId, boxDefinition)
		state.stateChangeTracker += 1
	},
	updateLatestNameBoxDefinition(state){

		state.nameBoxesDefinition.clear()		
		for( let [vesselId, definition] of state.newNameBoxesDefinition ){
			state.nameBoxesDefinition.set(vesselId, definition)
		}		
		state.stateChangeTracker += 1
	},
	addFlag(state, {country, flag}){
		state.flags.set(country, flag)
		state.stateChangeTracker += 1
	}
}

export const vesselName = {
	state,
	getters,
	mutations,
	namespaced: true
}