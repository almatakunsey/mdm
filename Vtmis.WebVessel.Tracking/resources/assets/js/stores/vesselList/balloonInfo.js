import utilHelper from '../../helpers/util'
import vesselService from '../../services/vessels'

const state = {
	enabled: false,
	info: {
		base: null,
		extra: null,
		imageUrl: ''		
	}	
}

const getters = {
	extraInfo: (state) => state.info.extra,
	baseInfo: (state) => state.info.base,
	info: (state) => state.info,
	enabled: state => state.enabled
}

const mutations = {
	setExtraInfo(state, extraInfo){		
		state.info.extra = extraInfo
	},
	setBaseInfo(state, info){
		// console.log('%cBalloonBaseInfo', conStyle.boldWhite, info)		
		state.info.base = info
	},
	resetInfo(state){
		state.info.base = null
		state.info.extra = null
		state.info.imageUrl = ''
	},
	toggleEnable(state, condition){
		state.enabled = !state.enabled
	},
	setImageUrl(state, url){
		state.info.imageUrl = `http://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}${url}`
	}
}

const actions = {
	updateExtraInfo({commit}, {id}){		

		return new Promise((resolve, reject)=>{
			vesselService.getInfoByVesselId(id)
			.then((data)=>{
				// console.log('%cExtra Balloon Info', conStyle.boldWhite, data)
				setTimeout(()=>{
					commit('setExtraInfo', data)					
					resolve(data)
				}, 500)
			})
			.catch(()=>{
				reject(error)
			})
		})	
		
	},	

	updateImageUrl({commit}, {id}){
		vesselService.getImageUrl(id)
		.then((url)=>{
			commit('setImageUrl', url)
		})		
	}

}


export const balloonInfo = {	
	state,
	getters,
	mutations,
	actions,
	namespaced: true
}