import {toolbar} from './toolbar'

const state = {	      
    vessels: new Map(),
    stateChangeTracker: 0, 
    enabled: false   
}

const getters = {     
    vessels: state => state.stateChangeTracker && state.vessels,
    vesselsArr: state => state.stateChangeTracker && [...state.vessels.values()],
    vesselListEnabled: state => state.enabled 
}

const mutations = {            
    addVessel(state, vessel){
        state.vessels.set(vessel.vesselId, vessel)
        state.stateChangeTracker += 1           
    },
    clearVessels(state, vessels){ state.vessels.clear(); state.stateChangeTracker += 1},
    toggleVesselListEnabled(state, condition){state.enabled = condition}    
}

export const vesselList = {	
	state,
	getters,
    mutations,    
    namespaced: true,
    modules: {        
        toolbar
    }	
}