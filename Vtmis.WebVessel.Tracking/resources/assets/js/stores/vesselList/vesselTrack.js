const state = {	
	inBoundVesselIds: [],
	enabled: false,
	show: false
}

const getters = {
	enabled: state => state.enabled,
	inBoundVesselIds: state => state.inBoundVesselIds	
}

const mutations = {
	addVessel(state, vessel){state.inBoundVesselIds.push(vessel)},
	toggleEnable(state){state.enabled = !state.enabled},	
	clearInBoundVesselIds(state){state.inBoundVesselIds = []}
}

export const vesselTrack = {
	state,
	getters,
	mutations,
	namespaced: true
}