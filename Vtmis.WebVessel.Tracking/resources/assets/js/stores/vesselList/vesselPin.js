const state = {
	enabled: false
}

const getters = {	
	enabled: state => state.enabled
}

const mutations = {	
	toggleEnable(state, condition){
		state.enabled = !state.enabled
	}
}

export const vesselPin = {	
	state,
	getters,
	mutations,	
	namespaced: true
}