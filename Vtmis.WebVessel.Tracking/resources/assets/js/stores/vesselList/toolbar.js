import { vesselName } from './vesselName'
import { vesselTrack } from './vesselTrack'
import { balloonInfo } from './balloonInfo'
import { vesselPin } from './vesselPin'
import { vesselSelected } from './vesselSelected'

const state = {
	floatPane: {
		enabled: false
	}
}

const mutations = {
	toggleFloatPane(state, condition){state.floatPane.enabled = condition}
}

const getters = {
	floatPaneEnabled: state => state.floatPane.enabled
}

export const toolbar = {
	state,
	getters,
	mutations,
	namespaced: true,
	modules: {
		vesselName,
		vesselTrack,
		balloonInfo,
		vesselPin,
		vesselSelected
	}
}