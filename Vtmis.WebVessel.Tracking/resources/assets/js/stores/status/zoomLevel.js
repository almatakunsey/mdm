const state = {
	enabled: true
}

const getters = {
	enabled: state => state.enabled
}

export const zoomLevel = {
	state,
	getters,	
	namespaced: true	
}