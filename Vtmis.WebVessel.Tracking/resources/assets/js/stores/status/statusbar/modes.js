/*
state
 */
const state = {
	filter: {
		enabled: false,
		name: ''
	},
	zone: {
		designer:{
			enabled: false
		}
	}
}

/*
getters
 */
const getters = {
	filterEnabled: state => state.filter.enabled,
	filterName: state => state.filter.name,
	zoneDesignerEnabled: state => state.zone.designer.enabled
}

/*
mutations
 */
const mutations = {
	toggleModeFilterStatus(state, condition){ state.filter.enabled = condition },
	toggleModeZoneDesigner(state, condition){ state.zone.designer.enabled = condition }
}


/*
export
 */
export const modes = {
	state,
	getters,
	mutations,
	namespaced:true
}