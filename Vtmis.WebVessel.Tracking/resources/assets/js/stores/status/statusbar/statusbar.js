import {vesselTypeCount} from './vesselTypeCount'
import {vesselClassCount} from './vesselClassCount'
import {modes} from './modes' 

const state = { 
    bottomPane: {
        enabled: true
    },
    topPane: {
        enabled: true
    }   
}

const getters = { 
    bottomPaneEnabled : state => state.bottomPane.enabled,
    topPaneEnabled: state => state.topPane.enabled
}

const mutations = {            
    toggleStatusBarBottomPane(state, condition){ state.bottomPane.enabled = condition },
    toggleStatusBarTopPane(state, condition){ state.topPane.enabled = condition }    
}

export const statusbar = {   
    state,
    getters,
    mutations,   
    namespaced: true,
    modules: {
        vesselTypeCount,
        vesselClassCount,
        modes
    }
}