const state = { 
    enabled: false,
    error: null,
    stateChangeTracker: 0,
    vesselCount: new Map() 
}

const getters = { 
    enabled : state => state.enabled,
    error: state => state.error,
    vesselCountArr: state => state.stateChangeTracker && _.zip([...state.vesselCount.keys()], [...state.vesselCount.values()]),
    vesselCount: state => state.stateChangeTracker && state.vesselCount,
    totalVessel: state => {
        return state.stateChangeTracker && [...state.vesselCount.values()].reduce((a, b) => a + b, 0);
    }
}

const mutations = {            
    toggleMapStatus(state, condition){ state.enabled = condition },
    incrementVesselCount(state, type){    	
    	if(state.vesselCount.has(type)){state.vesselCount.set(type, (state.vesselCount.get(type)) + 1)}
    	else{state.vesselCount.set(type, 1)}
    	state.stateChangeTracker += 1
    },
    clearVesselCount(state){state.vesselCount.clear(); state.stateChangeTracker += 1}    
}

export const vesselTypeCount = {   
    state,
    getters,
    mutations,    
    namespaced: true
}