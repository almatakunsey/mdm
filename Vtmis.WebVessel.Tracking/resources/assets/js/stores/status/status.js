import { statusbar } from './statusbar/statusbar'
import { zoomLevel } from './zoomLevel'

export const status = {
	namespaced:true,
	modules: {
		statusbar,
		zoomLevel
	}
}