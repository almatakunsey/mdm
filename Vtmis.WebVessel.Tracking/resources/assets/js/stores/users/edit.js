import userService from '../../services/users'

const state = {
	enabled: false
}

const getters = {
	enabled: state =>state.enabled
}

const mutations = {
	toggleUserEdit(state, condition){
		state.enabled = condition
	}
}

const actions = {
	updatesUser({commit, getters, rootGetters}, id){
            
        return new Promise((resolve, reject) => {            
            // console.log("%cFormData:", conStyle.boldRed, rootGetters['users/form/formData'])
            userService.updateUser(rootGetters['users/form/formData'], id)
            .then((result)=>{
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
            
        })  
    }
}

export const edit = {
	state,
	getters,
	mutations,
    actions,
	namespaced:true
}
