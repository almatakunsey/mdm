import userService from '../../services/users'

const formDataPristine = {
	name: "",
	surname: "",
	userName: "",
    emailAddress: "",
	isActive: true,
	roleNames: [],
  	password: "",
  	confirmPassword: "",
  	tenantId: Number(localStorage.tenantId),
  	enabled: false
	
}

const state = {
	formData : _.cloneDeep(formDataPristine),
	listOfRoles: []
}

const getters = {
	formData: state => state.formData,
	listOfRoles: state => state.listOfRoles
}

const mutations = {
	setFormData(state, data){state.formData = data},
	resetFormData(state){state.formData = _.cloneDeep(formDataPristine)},
	setFormDataValue(state, {key, value}){state.formData[key]=value},
	setListOfRoles(state, roles){   
        _.each(roles, ({ id, displayName }) => { 
            state.listOfRoles.push({ text: displayName, id:id })
        })
    },
    clearListOfRoles(state){        
        state.listOfRoles = []
    },
}

const actions = {
	loadFormData({commit}, userId){
		return new Promise((resolve, reject)=>{
			userService.getUserDetails(userId)
			.then((result)=>{
				commit('setFormData', result)
				resolve(result)
			})	
			.catch((error)=>{
				console.error(error)
			})
		})
		
	},

	getListOfRoles({commit}){
		commit('clearListOfRoles')
        return new Promise((resolve, reject)=>{
            userService.getListOfRoleByUserId()
            .then(({data:{result:{items}}})=>{
            	// console.log("%cResult:", conStyle.boldRed, items)
                commit('setListOfRoles', items)
                resolve(items)
            })
            .catch((error)=>{
                reject(error)
            })
        })
    },

    removeUser({commit, getters}, id){
		return new Promise((resolve, reject)=>{
			userService.removeUser(id)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})		
	},

	deactivateUser({commit, getters}, id){
		return new Promise((resolve, reject)=>{
			userService.deactivateUser(id)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})		
	}
}

export const form = {
	state,
	getters,
	mutations,
	actions,
	namespaced: true
}