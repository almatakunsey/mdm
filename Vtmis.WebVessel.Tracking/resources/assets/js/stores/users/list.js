import userService from '../../services/users'
import { UserListWorker } from '../../workers/users'

const userListItemPristine = {
	tenantName: '',
	tenantId: null,
	users: []
}

const state = {	
	userList: [],
	enabled: false
}

const getters = {
	userList(state){ return state.userList },
	enabled(state){ return state.enabled }
}

// helpers functions
const flatUsers = (userListCopy)=>{
	let users = []
	
	userListCopy.forEach((tenantUsers)=>{
		tenantUsers.users.forEach((user)=>{
			users.push(user)
		})
	})

	// remove duplicate users
	users = _.uniqWith(users, (a, b)=>{
		return a.id == b.id
	})

	return users
}


// mutations
const mutations = {
	toggleUserList(state, condition){state.enabled = condition},
	setUserList(state, userList){ state.userList = userList },
	addUsers(state, {items}){

		let data = []
		
		_.each(items, (item)=>{			

			if(item.tenantName == null){item.tenantName = 'MDM'}

			let index = _.findIndex(data, {'tenantName':item.tenantName})

			// not found in list, create new tenant
			if(index == -1){	

				let userListItem = _.cloneDeep(userListItemPristine)
				
				userListItem.tenantName = item.tenantName
				userListItem.tenantId = item.tenantId
				userListItem.users = [item]

				data.push(userListItem)
			}
			// found in list, we just update the users
			else{				
				data[index].users.push(item)
			}			
		})		

		state.userList = data		

	},
	groupUserList(state, group){		

		// flat out the array and collect users
		const users = flatUsers(_.cloneDeep(state.userList))

		// empty the list
		state.userList = []

		// coming from tenant, there wont be any duplicate in users
		if(group == 'vessel'){	
			UserListWorker.groupByVessel(users)									
		}

		// back to tenant list
		else{			
			UserListWorker.groupByTenant(users)															
		}

	}
}

const actions = {
	loadUserList({commit, getters}){
		return new Promise((resolve, reject)=>{
			userService.getAllUser().then(
				(result)=>{					

					commit('addUsers', result)	

					setTimeout(()=>{							
						resolve(result)					
					}, 1000)					
					
				},
				(error)=>{
					reject(error)
				}
			)
		})
	}	
}

export const list = {
	state,
	getters,
	mutations,
	actions,
	namespaced:true
}