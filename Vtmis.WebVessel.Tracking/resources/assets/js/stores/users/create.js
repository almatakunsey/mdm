import userService from '../../services/users'

const state = {
    enabled: false    

} // end state

const getters = {
    enabled: state => state.enabled    
}

const mutations = {
    toggleUserCreate(state, condition){state.enabled = condition}    
}

const actions = {
    createNewUser({ commit, getters, rootGetters }){
            
        return new Promise((resolve, reject) => {            
            // console.log("%cFormData:", conStyle.boldRed, rootGetters['users/form/formData'])
            userService.storeNewUser(rootGetters['users/form/formData'])
            .then((result)=>{
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
            
        })  
    }
}

export const create = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}