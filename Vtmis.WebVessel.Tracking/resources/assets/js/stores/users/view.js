const state = {
	enabled: false,
	userData: null
}

const getters = {
	userData: state => state.userData
}

const mutations = {
	toggleUserView(state, condition){
		state.enabled = condition
	},
	setUserData(state, data){state.userData=data}
}

const actions = {
	loadUserData({commit}){
		
	}
}

export const view = {
	state,
	getters,
	mutations,
	namespaced:true
}
