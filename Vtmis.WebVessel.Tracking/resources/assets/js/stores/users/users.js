import { list } from './list'
import { view } from './view'
import { form } from './form'
import {create} from './create'
import {edit} from './edit'

export const users = {	
	namespaced:true,
	modules: {
		list,
		view, 
		form,
		create,
		edit
	}
}