import { minihelper } from './minihelper'

const state = {
	rightPane: {
		enabled: true
	},
	actionButton: {
		enabled: true
	}
}

const getters = {
	rightPaneEnabled : state => state.rightPane.enabled
}

const mutations = {
	toggleRightPaneEnabled(state, condition){ state.rightPane.enabled = condition },
	toggleActionButtonEnabled(state, condition){ state.actionButton.enabled = condition }
}

export const knowledgebase = {
	state,
	getters,
	mutations,
	namespaced: true,
	modules: {
		minihelper
	}
}