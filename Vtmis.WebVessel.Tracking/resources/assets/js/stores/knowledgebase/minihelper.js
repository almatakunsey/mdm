const formDataOrigin = {
	name: ''
}
const state = {
	enabled: false,
	formData: _.cloneDeep(formDataOrigin)
}

const getters = {
	miniHelperEnabled : state => state.enabled,
	formData: state => state.formData
}

const mutations = {
	toggleMiniHelperEnabled(state, condition){ state.enabled = condition },
	clearFormData(state){ state.formData = _.cloneDeep(formDataOrigin) },
	setFormData(state, {key, value}){
        state.formData[key] = value        
    }
}

export const minihelper = {
	state,
	getters,
	mutations,
	namespaced: true
}