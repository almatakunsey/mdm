const state = {
	enabled: false,
	panelShown: true,
	vessels: new Map(),
    stateChangeTracker: 0,
}

const getters = {
	wremsSummaryEnabled: state => state.enabled,
	panelShown: state => state.panelShown,
	vessels: state => state.stateChangeTracker && state.vessels,
    vesselsArr: state => state.stateChangeTracker && [...state.vessels.values()].filter(v => v.isDrawn && v.wreckType == "WRECKS")
}

const mutations = {
	toggleWremsSummary(state, condition){		
		state.enabled = condition
	},
	togglePanelShown(state, condition){ state.panelShown = condition },
	addVessel(state, vessel){
        state.vessels.set(vessel.vesselId, vessel)
        state.stateChangeTracker += 1           
    },
    clearVessels(state, vessels){ state.vessels.clear(); state.stateChangeTracker += 1}
}

export const summary = {
	state,
	getters,
	mutations,
	namespaced:true
}