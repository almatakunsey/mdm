import wremsService from '../../services/wrems'
import { summary } from './summary'

const state = {
    showVessels: false
}

const getters = {
    showVessels: state => state.showVessels
}

const mutations = {
    toggleShowVessels(state, condition){state.showVessels = condition}
}

const actions = {
	async loadWrems({commit}){
		return new Promise((resolve, reject)=>{
            wremsService.loadWrems()
            // .then(({data:{result:{items}}}) =>{             
            //     commit('setAtonData', items)
            //     resolve(items)
            //     // console.log("%cAton from api:", conStyle.boldWhite, items)
            // })
            .then((result) =>{                
                
                const {wreckDetails} = result            
                
                wreckDetails.forEach(w => {

                    const [{latitude:lat, longitude:lgt}] = w.locations                   

                    const newInfo = {
                        vesselId : `w${w.id}`,
                        mmsi: `w${w.id}`,
                        vesselName: w.wreckName,
                        trueHeading: 1, // wont be use anyway
                        lat,
                        lgt
                    }

                    commit( 'map/setVessel', {...newInfo, ...w}, {root:true})                    
                })
                
                resolve(result)                
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })  
	}
}

export const wrems = {
    state,
    getters,
    mutations,
    actions,	
	namespaced:true,
    modules:{
        summary
    }
}