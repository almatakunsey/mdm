const state = {
	connections : {
		vesselGeolocation: null,
		alarm: null
	}
}

const getters = {
	connections: (state)=> state.connections
}

const mutations = {
	setConnection(state, {type, connection}){
		state.connections[type] = connection
	}
}

export const signalr = {	
	state,
	getters,
	mutations,	
	namespaced: true
}