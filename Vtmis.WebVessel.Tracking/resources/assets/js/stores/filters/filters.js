import {summary} from './summary'
import {create} from './create'
import {edit} from './edit'
import {form} from './form'
import {vesselGroup} from './vesselGroup/vesselGroup'

import filterService from '../../services/filters'

import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

/*
state
 */
const state = {		
	filters: [],
	selectedFilter: null,
	selectedFilterDetails: null,
	query: '',
	status: {
		enabled: false
	}	
}

/*
getters
 */
const getters = {		
	filterStatusEnabled: state => state.status.enabled,
	filters : state => state.filters,
	selectedFilter : state => state.selectedFilter,
	selectedFilterDetails : state => state.selectedFilterDetails,
	query : state => state.query
}

/*
mutations
 */
const mutations = {
	setFilters(state, filters){state.filters = filters},	
	setSelectedFilter(state, filter){ state.selectedFilter = filter },	
	toggleModeFilterStatus(state, condition){ state.status.enabled = condition },	
	setQuery(state, query){
		return new Promise((resolve, reject)=>{
			state.query = query
			resolve(query)
		})
		
	}
}

/*
actions
 */
const actions = {
	loadFilters : ({commit}) => {

		return new Promise((resolve, reject)=>{
			filterService.getFilters()
			.then( ({data:{result}}) =>{				
				commit('setFilters', result.items)
				resolve(result)
			})			
			.catch(( {response} )=>{							
				reject(response)				
			})
		})								
	},

	
	deleteFilter({commit, getters}, id){		

		return new Promise((resolve, reject)=>{
			filterService.deleteFilter(id).then(
				(result)=>{                    
                    resolve(result)
                },
                (error)=>{                    
                    reject(error)
                }
			)
		})        
    }   
}

/*
export
 */
export const filters = {
	state,
	getters,
	mutations,
	actions,	
	namespaced: true,
	modules:{
		summary,
		create,
		edit,
		form,
		vesselGroup	
	}
}