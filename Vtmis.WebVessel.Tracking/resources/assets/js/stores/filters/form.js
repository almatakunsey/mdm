import utilHelper from '../../helpers/util'
import data from './data'
import filterGroupService from '../../services/filter-groups'
import filtersService from '../../services/filters'

const filterItemsDetailsPristine = {
	rid: utilHelper.genRandom(32),
	condition: 0,
	columnName: 'shipName',
	operation: '=',
	columnValue: ''
}

const filterItemsPristine = {
	rid: utilHelper.genRandom(32),
	colour: '#ff8080',
	borderColour: '#ff8080',
	filterDetails: [_.cloneDeep(filterItemsDetailsPristine)]
}

const formDataPristine =  {
    name: '',
	isEnable: true,
	isShowShips: true,
	filterItems: [_.cloneDeep(filterItemsPristine)]
}

/*
state
 */
const state = {
	formData: _.cloneDeep(formDataPristine),
	andOrOptions: data.andOrOptions,    
	fieldOptions: data.fieldOptions,
	operatorOptions: data.operatorOptions,
	shipTypeOptions: data.shipTypeOptions,
	navStatusOptions: data.navStatusOptions,
	AISTypeOptions: data.AISTypeOptions,
	dataSourceOptions: data.dataSourceOptions,
	colorOptions: data.colorOptions,
	lloydsTypeOptions: data.lloydsTypeOptions,
	groupTypeOptions: null,
	shipTypeListOptions: [],
	filterGroupListOptions: []
}


/*
getters
 */
const getters = {
	formData: state => state.formData,
	filterItems: state => state.formData.filterItems,
    andOrOptions: state => state.andOrOptions,
    fieldOptions: state => state.fieldOptions,
    operatorOptions: state => state.operatorOptions,
    shipTypeOptions: state => state.shipTypeOptions,
    navStatusOptions: state => state.navStatusOptions,
    AISTypeOptions: state => state.AISTypeOptions,
    dataSourceOptions: state => state.dataSourceOptions,
    colorOptions: state => state.colorOptions,
    lloydsTypeOptions: state => state.lloydsTypeOptions,
    groupTypeOptions: state=> state.groupTypeOptions,
    shipTypeListOptions: state => state.shipTypeListOptions,
    filterGroupListOptions: state => state.filterGroupListOptions
}

const getItemIndex = (state, rid)=>{
	return _.findIndex(state.formData.filterItems, {rid})
}

const getDetailIndex = (state, itemIndex, detailRid)=>{
	return _.findIndex(state.formData.filterItems[itemIndex].filterDetails, {rid:detailRid})
}


/*
mutations
 */
const mutations = {

	setFormMainData(state, {key, value}){state.formData[key] = value},
	setFormFilterItem(state, {key, value, rid}){
		const itemIndex = getItemIndex(state, rid)
		state.formData.filterItems[itemIndex][key] = value	
	},
	setFormFilterItemDetails(state, { key, value, itemRid, detailRid }){

		const itemIndex = getItemIndex(state, itemRid)
		const detailIndex = getDetailIndex(state, itemIndex, detailRid)		
		state.formData.filterItems[itemIndex].filterDetails[detailIndex][key] = value
	},
	removeFilterDetail(state, {itemRid, detailRid}){		
		
		const itemIndex = getItemIndex(state, itemRid)
		const detailIndex = getDetailIndex(state, itemIndex, detailRid)		
	
		state.formData.filterItems[itemIndex].filterDetails.splice(detailIndex, 1)
	},
	addFilterDetail(state, itemRid){		
		const itemIndex = getItemIndex(state, itemRid)
		const newDetail = _.cloneDeep(filterItemsDetailsPristine)
		newDetail.rid = utilHelper.genRandom(32)
		state.formData.filterItems[itemIndex].filterDetails.push(newDetail)		
	},
	addFilterItem(state){
		const newItem = _.cloneDeep(filterItemsPristine)
		newItem.rid = utilHelper.genRandom(32)
		state.formData.filterItems.push(newItem)			
	},
	removeFilterItem(state, itemRid){
		const itemIndex = getItemIndex(state, itemRid)
		state.formData.filterItems.splice(itemIndex, 1)
	},
	resetForm(state){
		 state.formData = _.cloneDeep(formDataPristine)		 
	},
	replaceFormData(state, formData){
		state.formData = formData
	},
	setGroupTypeOptions(state, groups){
		state.groupTypeOptions = groups.map( g => {
			return {text: g.name, id:g.id }
		} )
	},
	setShipTypesList(state, shipType){		
        // state.stationListOption.push( {text:'Select Weather Station', id:0} )    
        _.each(shipType, ({ id, shipTypeName }) => { 
            state.shipTypeListOptions.push({ text: shipTypeName, id:shipTypeName })
        })        
    },

    clearShipTypesList(state){        
        state.shipTypeListOptions = []
    },

    setFilterGroupListOptions(state, value){		
        // state.stationListOption.push( {text:'Select Weather Station', id:0} )    
        _.each(value, ({ id, name }) => { 
            state.filterGroupListOptions.push({ text: name, id })
        })        
    },

    clearFilterGroupListOptions(state){        
        state.filterGroupListOptions = []
    },
}

const actions = {
	getShipTypeList({commit}){		
        commit('clearShipTypesList')
        return new Promise((resolve, reject)=>{
            filtersService.getShipTypes()
            .then(({data:{result}})=>{
                commit('setShipTypesList', result)                
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
        })
    },

    getFilterGroupList({commit}){		
        commit('clearFilterGroupListOptions')
        return new Promise((resolve, reject)=>{

            filtersService.getVesselGroupList()
            .then(({data:{result}}) =>{           
                commit('setFilterGroupListOptions', result)
                resolve(result)
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })
    },

	loadAllGroupTypeOptions({commit}){
		return new Promise((resolve, reject)=>{
			filterGroupService.getAllGroupType()
			.then(( {data:{result}} )=>{
				commit('setGroupTypeOptions', result)				
				resolve(result)
			})
			.catch((error)=>{
				console.error(error)
				reject(error)
			})
		})
	}
}

export const form = {
	state,
	getters,
	mutations,	
	actions,
	namespaced:true	
}
