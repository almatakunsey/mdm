import utilHelper from '../../helpers/util'
import data from './data'
import filterService from '../../services/filters'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const state = {
    enabled: false    
} // end state

const mutations = {
    toggleFilterEdit(state, condition){state.enabled = condition}    
}

const actions = {    

    updateFilter({commit, getters, rootGetters}){        

        return new Promise((resolve, reject)=>{                                    
            filterService.updateFilter(
                rootGetters['filters/form/formData'], 
                rootGetters['filters/selectedFilter'].id
            ).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    }
}

const getters = {
    enabled: state => state.enabled    
}

export const edit = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}