import utilHelper from '../../helpers/util'
import data from './data'

import filterService from '../../services/filters'

const state = {
    enabled: false    

} // end state

const getters = {
    enabled: state => state.enabled    
}

const mutations = {
    toggleFilterCreate(state, condition){state.enabled = condition}    
}

const actions = {
    storeFilter({commit, getters, rootGetters}){        

        return new Promise((resolve, reject)=>{                                    
            filterService.storeFilter(rootGetters['filters/form/formData']).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    }
}

export const create = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}