import { vesselGroupList} from './vesselGroupList'
import { vesselGroupCreate } from './vesselGroupCreate'
import { form } from './form'
import { edit } from './edit'

export const vesselGroup = {
	namespaced: true,
	modules: {
        vesselGroupList,
        vesselGroupCreate,
        form,
        edit
    }
}