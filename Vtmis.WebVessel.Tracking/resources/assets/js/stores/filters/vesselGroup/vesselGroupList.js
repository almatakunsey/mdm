import filterService from '../../../services/filters'
import { fields } from './vesselGroupListField'

const state = {
    fields: fields,
    enabled: false,
    vesselGroupList: []
}

const getters = {
    field: state => state.fields,
	enabled: state => state.enabled,
    vesselGroupList: state => state.vesselGroupList
}

const mutations = {
    setField(state, field){state.fields = field},
	toggleFilterVesselGroupListPane(state, condition){state.enabled = condition},
    setVesselGroupList(state, vesselGroupList){ state.vesselGroupList = vesselGroupList }
}

const actions = {
    getVesselsGroupList : ({commit}) => {

        return new Promise((resolve, reject)=>{

            filterService.getVesselGroupList()
            .then(({data:{result}}) =>{           
                commit('setVesselGroupList', result)
                resolve(result)
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })                              
    }

}

export const vesselGroupList = {
    state,
    getters,    
    mutations,
    actions,
    namespaced: true
}