import filterService from '../../../services/filters'

const state = {
    enabled: false    

} // end state

const getters = {
    enabled: state => state.enabled    
}

const mutations = {
    toggleFilterVesselGroupCreate(state, condition){state.enabled = condition}    
}

const actions = {
    storeFilterVesselGroup({rootGetters}){
        return new Promise((resolve, reject)=>{
            filterService.createFilterGroup(rootGetters['filters/vesselGroup/form/formData']).then((result)=>{
                resolve(result)
            })  
            .catch((error)=>{
                console.error(error)
            })
        })
        
    },
}

export const vesselGroupCreate = {
   	state,
    getters,      
    mutations,
    actions,
    namespaced: true
}