import utilHelper from '../../../helpers/util'

import filterService from '../../../services/filters'

const vesselGroupItemPristine = {
    rid: utilHelper.genRandom(32),
    condition: 0,
    columnName: 'mmsi',
    operation: '=',
    columnValue: ''
}

const formDataPristine = {
        name: '',
        searchTerm: '',
        vesselGroupDetails: [_.cloneDeep(vesselGroupItemPristine)]
}

const state = {
    filterGroup: [],
    formData : _.cloneDeep(formDataPristine),
    andOrOptions: [
        {text: 'Or', id: 0},
        {text: 'And', id: 1}
    ],
    fieldOptions: [    
        {text: 'MMSI', id: 'mmsi'},
        {text: 'SHIP NAME', id: 'shipName'},
        {text: 'CALL SIGN', id: 'callSign'}, 
    ],
    operatorOptions: [
        {text: '=', id: '='},       
        {text: '!=', id: '!='},
        {text: 'Start With', id: 'startWith'},
        {text: 'IN', id: 'in'},
        {text: 'NOT IN', id: 'notIn'}
    ],
}

const getters = {
    filterGroup : state => state.filterGroup,
    formData: state => state.formData,
    vesselGroupDetails: state => state.formData.vesselGroupDetails,
    andOrOptions: state => state.andOrOptions,
    fieldOptions: state => state.fieldOptions,
    operatorOptions: state => state.operatorOptions,    
}

const mutations = {
    setFilterGroup(state, filterGroup){state.filterGroup = filterGroup},
    clearData(state) {state.formData = _.cloneDeep(formDataPristine)},
    setFormData(state, {key, value}){
        state.formData[key] = value        
    },
    addVesselGroupItem(state){
        const newItem = _.cloneDeep(vesselGroupItemPristine)
        newItem.rid = utilHelper.genRandom(32)
        state.formData.vesselGroupDetails.push(newItem)            
    },
    setVesselGroupItem(state, {type, value, rid}){ 
        const dataIndex = _.findIndex(state.formData.vesselGroupDetails, {rid})
        // console.log("%cdataindex:", conStyle.boldRed, dataIndex)
        state.formData.vesselGroupDetails[dataIndex][type] = value

    },
    removeVesselGroupItem(state, index){   
        state.formData.vesselGroupDetails.splice(index, 1)
        // if(index == 0) {
        //     state.formData.vesselGroupDetails[0].condition = '-1'
        // }  
    },
    removeFilterItem(state, itemRid){
        const itemIndex = getItemIndex(state, itemRid)
        state.formData.vesselGroupDetails.splice(itemIndex, 1)
    },
    replaceFormData(state, formData){
        state.formData = formData
    }

}

const actions = {

    loadFormData({commit}, userId){
        return new Promise((resolve, reject)=>{
            filterService.getVesselGroupDetails(userId)
            .then((result)=>{
                commit('setFilterGroup', result)
                console.log("%cresult:", conStyle.boldRed, result)
                resolve(result)
            })  
            .catch((error)=>{
                console.error(error)
            })
        })
        
    },

    deleteFilterGroup({commit, getters}, id){        

        return new Promise((resolve, reject)=>{
            filterService.deleteFilterGroup(id).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{                    
                    reject(error)
                }
            )
        })        
    }

   
}

export const form = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}