export const fields = [
	{
		name:'name', 
		title:'Name',
		sortField: 'name',
		active:true,
		inReport: true
	},

	{
        name: '__slot:actions',
        title: 'Actions',
        active:true,
        width: '50px',
        titleClass: 'center-aligned',
        inReport: false
    }
	]