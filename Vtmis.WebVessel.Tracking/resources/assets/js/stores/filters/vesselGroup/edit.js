import utilHelper from '../../../helpers/util'

import filterService from '../../../services/filters'

const state = {
    enabled: false    
}

const getters = {
    enabled: state => state.enabled    
}

const mutations = {
    toggleFilterVesselGroupEdit(state, condition){state.enabled = condition}    
}

const actions = {    

    updateFilterVesselGroup({commit, getters, rootGetters}, id){        

        return new Promise((resolve, reject)=>{                                    
            filterService.updateFilterGroup(rootGetters['filters/vesselGroup/form/formData'],id)
            .then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    }
}



export const edit = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}