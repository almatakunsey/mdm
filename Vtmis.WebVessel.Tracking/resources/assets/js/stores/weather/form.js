import weatherService from '../../services/weather'

const formDataOrigin = {
    stationId: 0,
    area: 'all',
    isEnable: false,
    refreshRate: 60
    //fromDate: '',
    //toDate: '',
}

const state = {
	formData: _.cloneDeep(formDataOrigin),
    areaOptions: [
        {text: 'All', id: 'all'},
        // {text: 'Screen', id: 'screen'}
    ],
    stationListOption: [],
    gaugesList:[]
}

const getters = {    
    formData: state => state.formData,
    areaOptions: state => state.areaOptions,
    stationListOption: state => state.stationListOption,
    gaugesList: state => state.gaugesList
}

const mutations = { 
    clearFormData(state){ state.formData = _.cloneDeep(formDataOrigin) },
    setFormData(state, {key, value}){
        state.formData[key] = value        
    },
    setStationList(state, station){    
        _.each(station, ({ id, stationName, mmsi }) => { 
            state.stationListOption.push({ text: stationName, id: mmsi })
        })
    },
    clearStationList(state){        
        state.stationListOption = []
    },
    setGaugesList(state, gaugesList){ state.gaugesList = gaugesList }
}

const actions = {
    getWeatherStationList({commit}){
        commit('clearStationList')
        return new Promise((resolve, reject)=>{
            weatherService.getWeatherStationList()
            .then((result)=>{
                commit('setStationList', result.items)
                // console.log(result.items)
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
        })
    },

    getGaugesList({commit, getters}, mmsi){        

        return new Promise((resolve, reject)=>{                                    
            weatherService.getGaugesList(mmsi)
                .then( ({data:{result}}) =>{    
                    commit('setGaugesList', result)
                    // console.log(data)
                    resolve(result)
                    
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    },

    getPublicWeatherStationList({commit}, token){
        commit('clearStationList')
        return new Promise((resolve, reject)=>{
            weatherService.getPublicWeatherStationList(token)
            .then((result)=>{
                commit('setStationList', result.items)
                // console.log(result.items)
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
        })
    },

    getPublicGaugesList({commit}, {mmsi, token}){   
        return new Promise((resolve, reject)=>{                                    
            weatherService.getPublicGaugesList(mmsi,token)
                .then( ({data:{result}}) =>{    
                    commit('setGaugesList', result)
                    // console.log(data)
                    resolve(result)
                    
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    }
}

export const form = {
	state,
	getters,
	mutations,
    actions,
	namespaced: true
}