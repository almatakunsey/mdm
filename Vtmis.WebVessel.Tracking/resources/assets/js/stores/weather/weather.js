import {summary} from './summary'
import { dashboard } from './dashboard'
import { form } from './form'

const state = {	
	mainPane: {
		enabled: false
	}
}

const getters = {	
	mainPaneEnabled : state => state.mainPane.enabled
}

const mutations = {	
	toggleReportMainPane(state, condition){ state.mainPane.enabled = condition }
}

const actions = {
    
}

export const weather = {	
	state,
	getters,
	mutations,
    actions,
	namespaced: true,
	modules:{
		summary,
        dashboard,
        form
	}
}

