const state = {
    enabled: false    
}

const getters = {
    enabled: state => state.enabled
}

const mutations = {
    toggleWeatherDashboard(state, condition){ state.enabled = condition },
}

const actions = {
   
}

export const dashboard = {
    state,
    getters,  
    actions,  
    mutations,
    namespaced: true
}

