﻿import moment from 'moment'
import async from 'async'

import utilHelper from '../helpers/util'
import playbackService from '../services/playbacks'   

import { fields } from './playbackListField'

/*
########   ########    #######   ########    ######    
##     ##  ##     ##  ##     ##  ##     ##  ##    ##   
##     ##  ##     ##  ##     ##  ##     ##  ##         
########   ########   ##     ##  ########    ######    
##         ##   ##    ##     ##  ##               ##   
##         ##    ##   ##     ##  ##         ##    ##   
##         ##     ##   #######   ##          ######    
*/
let startDate = null
let endDate = null

// local pc
// process.env.NODE_ENV == 'development' && 
// utilHelper.getNetworkType() == 'private' &&
// window.location.hostname != '10.1.2.14'

// if(
//     window.location.hostname == 'localhost'
// ){            
//     startDate = moment('2020-02-10 10:00:00').subtract(1, 'hours') 
//     endDate = moment('2020-02-10 10:00:00')
// }
// // others, including server
// else{         
//     startDate = moment().subtract(1, 'hours')
//     endDate = moment()    
// }

// we're using mongodb right now, so we can use 
// current datetime right away
startDate = moment().subtract(1, 'hours')
endDate = moment()    

/*
 ######    ########      ###     ########   ########   
##    ##      ##        ## ##       ##      ##         
##            ##       ##   ##      ##      ##         
 ######       ##      ##     ##     ##      ######     
      ##      ##      #########     ##      ##         
##    ##      ##      ##     ##     ##      ##         
 ######       ##      ##     ##     ##      ########   
*/
const state = {
    enabled: false,
    dateRange: {
        startDate,
        endDate
    },
    selectedIds: [],
    selectedNames: [],
    selectedVessels: [],
    data : [],    
    multipleVesselsPoint : [],
    fields: fields
}

/*
 ######    ########   ########   ########   ########   ########    ######    
##    ##   ##            ##         ##      ##         ##     ##  ##    ##   
##         ##            ##         ##      ##         ##     ##  ##         
##   ####  ######        ##         ##      ######     ########    ######    
##    ##   ##            ##         ##      ##         ##   ##          ##   
##    ##   ##            ##         ##      ##         ##    ##   ##    ##   
 ######    ########      ##         ##      ########   ##     ##   ######    
*/
const getters = {
    playbackEnabled: state => state.enabled,        
    selectedIds: (state, getters, rootState, rootGetters) => rootGetters['map/selectedVesselIds'],
    selectedNames: (state, getters, rootState, rootGetters) => getters.selectedIds.map( id => rootGetters['map/vessel'](id).vesselName ),
    selectedVessels: (state, getters, rootState, rootGetters) => getters.selectedIds.map( id => rootGetters['map/vessel'](id) ),
    startDate: state => state.dateRange.startDate,
    endDate: state => state.dateRange.endDate,
    data: state => state.data,    
    multipleVesselsPoint: state => state.multipleVesselsPoint,
    pointExist: (state) => {
        let exist = false
        for(const list of state.multipleVesselsPoint){            
            if(list.data.length){
                exist = true
                break
            }
        }
        return exist
    },
    field: state => state.fields,
}

/*
##     ##  ##     ##  ########      ###     ########   ####   #######   ##    ##    ######    
###   ###  ##     ##     ##        ## ##       ##       ##   ##     ##  ###   ##   ##    ##   
#### ####  ##     ##     ##       ##   ##      ##       ##   ##     ##  ####  ##   ##         
## ### ##  ##     ##     ##      ##     ##     ##       ##   ##     ##  ## ## ##    ######    
##     ##  ##     ##     ##      #########     ##       ##   ##     ##  ##  ####         ##   
##     ##  ##     ##     ##      ##     ##     ##       ##   ##     ##  ##   ###   ##    ##   
##     ##   #######      ##      ##     ##     ##      ####   #######   ##    ##    ######    
*/
const mutations = {
    togglePlaybackEnabled(state, condition) { state.enabled = condition },
    addSelectedIds(state, {id, ctrlKey}) {
        if(ctrlKey && !_.includes(state.selectedIds, id)){            
            state.selectedIds.push(id) 
        }
        else{
            state.selectedIds = [id]   
        }        
    },
    setSelectedIds(state, ids){ state.selectedIds = ids },
    clearSelectedIds(state) {state.selectedIds = [],state.selectedNames = []},
    setSelectedNames(state, {name, ctrlKey}) {                        
        if(ctrlKey && !_.includes(state.selectedNames, name)){
            state.selectedNames.length == 0 ? state.selectedNames.push(name) : state.selectedNames.push(' ' + name) 
        }
        else{
            state.selectedNames = [name]
        }
    },
    setSelectedNamesArr(state, names){ state.selectedNames = names },
    setSelectedVessel(state, vessels){ state.selectedVessels = vessels },
    setDateRange(state, {startDate, endDate}) {state.dateRange = {startDate, endDate}},
    getDate(state, {start,end}){
        state.dateRange.startDate = start,
        state.dateRange.endDate = end
    },
    setData(state, geojsonArray) {
        state.data = geojsonArray
    },
    setPointList(state, {data, vesselId}) {
        // console.log("%c data: ", conStyle.boldRed, data)
        // console.log("%cvesselId:", conStyle.boldRed, vesselId)

        // data.result = null

        if(data.result){

            let pointList = []
            
            _.each(data.result.geometry.coordinates, function (val, index) {
                pointList.push([data.result.geometry.coordinates[index][1], data.result.geometry.coordinates[index][0]]);
            });
            
            state.multipleVesselsPoint.push({ data: pointList, mmsi: vesselId})
        }
    },
    clearData(state) {state.data = []},    
    clearMultipleVesselPoint(state) { state.multipleVesselsPoint = [] },
    clearSelectedNames(state) { state.selectedNames = [] },
    setPlaybackField(state, field){state.fields = field},
}

/*
   ###      ######    ########   ####   #######   ##    ##    ######    
  ## ##    ##    ##      ##       ##   ##     ##  ###   ##   ##    ##   
 ##   ##   ##            ##       ##   ##     ##  ####  ##   ##         
##     ##  ##            ##       ##   ##     ##  ## ## ##    ######    
#########  ##            ##       ##   ##     ##  ##  ####         ##   
##     ##  ##    ##      ##       ##   ##     ##  ##   ###   ##    ##   
##     ##   ######       ##      ####   #######   ##    ##    ######    
*/
const actions = {
    updatePlaybackData({commit, getters},{startDate, endDate}){        
            
        return new Promise((resolve, reject)=>{            
            const startDateFormatted = startDate.format('YYYY-MM-DD HH:mm:ss')
            const endDateFormatted = endDate.format('YYYY-MM-DD HH:mm:ss')
            
            const playbackFunctions = getters.selectedIds.map((vesselId)=>{
                return (callback)=>{
                    playbackService.getPlayback(vesselId, startDateFormatted, endDateFormatted)
                    .then(
                        ({data})=>{                             
                            callback(null, data)                            
                            commit('setPointList', {data , vesselId})
                        },
                        (error)=>{
                            callback(error)
                        }
                    )
                }
            })

            async.parallel(playbackFunctions, (error, result) => {
                if(error){
                    reject(error)
                }
                else{                                        
                    commit('clearData')
                    const geojsonArray = result.map(({result}) => result ).filter( (re) => re && re.geometry.coordinates.length )                    
                    commit('setData', geojsonArray)
                    resolve('OK')
                }
            })            

        })

        
    }
}

export const playback = {
    state,	
	getters, 
	mutations,
    actions,
	namespaced: true
}