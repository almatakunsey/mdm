export const routeEtaFields = [
	{
		name:'name', 
		title:'Name',
		sortField: 'name',
		active:true,
		inReport: true
	},
	{
		name:'mmsi', 
		title:'MMSI',
		sortField: 'mmsi',
		active:true,
		inReport: true
	},
	{
		name:'cog', 
		title:'COG',
		sortField: 'cog',
		active:true,
		inReport: true
	}, 
	{
		name:'sog', 
		title:'SOG',
		sortField: 'sog',
		active:true,
		inReport: true
	},
	{
		name:'shipType', 
		title:'Ship Type',
		sortField: 'shipType',
		active:true,
		inReport: true
	}
	
]