import utilHelper from '../../helpers/util'
import customRouteService from '../../services/customRoute'
import { vesselFields } from './vesselEtaListField'
import { routeEtaFields } from './routeEtaListField'

const formDataOrigin = {
    refreshEnabled: false,
    isPassenger: false,
    refreshRate: 1,
    searchTerm: '',
    editColumn: false
}

const state = {
    formData: _.cloneDeep(formDataOrigin),
    vesselFields: vesselFields,
    routeEtaField: routeEtaFields,
    routeEtaPane: {
        enabled: false
    },
    vesselEtaPane: {
        enabled: false
    },
    routeOptions: [],    
    routeEta: [],
    
    vesselEta:[],
    vesselInfo:[],
}

const getters = {
	routeEtaPane: state => state.routeEtaPane.enabled,
    vesselEtaPane: state => state.vesselEtaPane.enabled, 
    vesselField: state => state.vesselFields,
    routeEtaField: state => state.routeEtaField,
	routeOptions: state => state.routeOptions,
	etaData: state => state.etaData,
    routeEta: state => state.routeEta,
    formData: state => state.formData,
    vesselEta: state => state.vesselEta,
    vesselInfo: state => state.vesselInfo,
}

const mutations = {
    toggleCustomRouteEta(state, condition){state.routeEtaPane.enabled = condition},
    toggleVesselEta(state, condition){state.vesselEtaPane.enabled = condition},
    setVesselField(state, vesselField){state.vesselFields = vesselField},
    setRouteEtaField(state, routeEtaField){state.routeEtaField = routeEtaField},
    resetRouteEtaField(state){ 
        state.routeEtaField = [
        {
            name:'name', 
            title:'Name',
            sortField: 'name',
            active:true,
            inReport: true
        },
        {
            name:'mmsi', 
            title:'MMSI',
            sortField: 'mmsi',
            active:true,
            inReport: true
        },
        {
            name:'cog', 
            title:'COG',
            sortField: 'cog',
            active:true,
            inReport: true
        }, 
        {
            name:'sog', 
            title:'SOG',
            sortField: 'sog',
            active:true,
            inReport: true
        },
        {
            name:'shipType', 
            title:'Ship Type',
            sortField: 'shipType',
            active:true,
            inReport: true
        }
    ]},
    setVesselInfo(state, vesselInfo){state.vesselInfo = vesselInfo},
    setRouteOptions(state, routes){        

        _.each(routes, ({ id, name }) => { 
            state.routeOptions.push({ text: name, id })
        })        
    },
    clearRouteOptions(state)
    {
        state.routes = []
        state.routeOptions = []
    },
    setRouteEta(state, routeEta){ state.routeEta = routeEta },
    clearRouteEta(state){ state.routeEta = []},

    setVesselEta(state, vesselEta){ state.vesselEta = vesselEta },
    clearVesselEta(state){ state.vesselEta = []},
    clearFormData(state) { 
            state.formData = {
                refreshEnabled: false,
                isPassenger: false,
                refreshRate: 1,
                searchTerm: '',
                editColumn: false
            } 
    },
    setFormData(state, {key, value}){
        state.formData[key] = value        
    },
}

const actions = {
	
	loadRouteOptions({commit, getters}){		
		commit('clearRouteOptions')
		return new Promise((resolve, reject)=>{			
			customRouteService.getRoutes(1, 1000)
			.then(({data:{result:{items}}})=>{	   
            // console.log(items)
				commit('setRouteOptions', items)
				resolve(items)				
			})
			.catch((error)=>{
				console.log(error)
				reject(error)
			})
		})		
	},

	loadRoutesEta({commit, getters, rootGetters}, id){

        if(!id){ return false }
		// commit('clearRouteEta')
        return new Promise((resolve, reject)=>{     
        
            customRouteService.getRouteEta(id)
            .then(({data:{result:{finalResults}}})=>{                    
                    commit('setRouteEta', finalResults)
                    // console.log(finalResults)
                    resolve(finalResults)
                },
                (error)=>{
                    reject(error)
                }
            )
        })
	},

    loadVesselEta({commit, getters}, id){

        if(!id){ return false }
        const mmsi = getters.vesselInfo.mmsi
        return new Promise((resolve, reject)=>{     
        
            customRouteService.getVesselEta(id,mmsi)
            .then(({data:{result:{result:{waypoints}}}})=>{                    
                    commit('setVesselEta', waypoints)
                    // console.log(waypoints)
                    resolve(waypoints)                    
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    }

}

export const eta = {
    state,
    getters,      
    mutations,
    actions,    
    namespaced: true
}