import { summary } from './summary'
import { create } from './create'
import { form } from './form'
import { edit } from './edit'
import { eta } from './eta'

import customRouteService from '../../services/customRoute'

const routeInstancePristine = {
	lines: [],
	points: []
}

const state = {		
	routes: [],
	routesReady: false,
	routeInstances: _.cloneDeep(routeInstancePristine),
	designer:{
        enabled:false        
    },
    fromDesigner: false
}

const getters = {		
	routes: state => {		
		return _.map(state.routes, p => {            
            p.waypoints = _.sortBy(p.waypoints, ["order"])
            return p
        })		
	},
	routeInstances: state => state.routeInstances,
	routesReady: state => state.routesReady,
	routeDesignerEnabled: state => state.designer.enabled,
	fromDesigner: state => state.fromDesigner
}

const mutations = {		
	toggleRouteDesigner(state, condition){state.designer.enabled = condition},    
	setRoutes(state, routes){ state.routes = routes },
	clearRoutes(state){ state.routes = [] },
	toggleRoutesReady(state, condition){ state.routesReady = condition },
	addRouteInstance(state, {type, instance}){ state.routeInstances[type].push(instance) },
	clearRouteInstances(state){ state.routeInstances = _.cloneDeep(routeInstancePristine) },
	toggleFromDesigner(state, condition){ state.fromDesigner = condition }        
}

const actions = {
	loadAllRoutes({commit, getters}){		
		commit('toggleRoutesReady', false)
		commit('clearRoutes')
		return new Promise((resolve, reject)=>{			
			customRouteService.getRoutes(1, 1000)
			.then(({data:{result:{items}}})=>{				
				commit('setRoutes', items)
				commit('toggleRoutesReady', true)
				resolve(items)
			})
			.catch((error)=>{
				console.log(error)
				reject(error)
			})
		})		
	},
	removeRoute({commit, getters}, id){
		return new Promise((resolve, reject)=>{
			customRouteService.removeRoute(id)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})		
	}
}

export const customRoute = {
	state,
	getters,
	mutations,
	actions, 
	namespaced:true,
	modules: {
		summary,
		create,
		form,
		edit,
		eta
	}
}