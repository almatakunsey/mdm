import utilHelper from '../../helpers/util'
import async from 'async'
import customRouteService from '../../services/customRoute'

const state = {
    editedCustomRouteId: null,
    enabled: false    
}

const getters = {
    enabled: state => state.enabled,    
    editedCustomRouteId: state => state.editedCustomRouteId,
    fromDesigner: state => state.fromDesigner
}

const mutations = {
    setEditedCustomRouteId(state, id){state.editedCustomRouteId = id},
    toggleCustomRouteEdit(state, condition){state.enabled = condition}    
}

const actions = {
    loadRoute({commit, getters, rootGetters}){
        return new Promise((resolve, reject)=>{
            customRouteService.getRoute(getters.editedCustomRouteId)
            .then((result)=>{
                let {data:{result:formattedData}} = result                                
                
                formattedData.waypoints = _.sortBy(formattedData.waypoints, ['order']).map((w)=>{
                    w.highlight = false
                    return w
                })

                
                commit('customRoute/form/replaceFormData', formattedData, {root:true})
                resolve(formattedData) 
            })
            .catch((error)=>{
                reject(error)
            })  
        })
    },

    updateRoute({commit, getters, rootGetters}){     
        return new Promise((resolve, reject)=>{
            // get a clone of the formdata
            const formData = _.cloneDeep(rootGetters['customRoute/form/formData'])            

            // remove some unneeded properties from waypoints
            formData.waypoints.forEach((wp)=>{                
                delete wp.highlight
                if(typeof wp.id === 'string'){
                    delete wp.id
                }
            })

            customRouteService.updateRoute(formData)
            .then((result)=>{
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })          

        })
        
    }
}

export const edit = {
    state,
    getters,      
    mutations,
    actions,    
    namespaced: true
}