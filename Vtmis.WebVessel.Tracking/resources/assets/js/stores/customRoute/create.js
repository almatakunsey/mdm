import async from 'async'
import customRouteService from '../../services/customRoute'

const state = {
    enabled: false    
}

const getters = {
    enabled: state => state.enabled    
}

const mutations = {
    toggleCustomRouteCreate(state, condition){state.enabled = condition}    
}

const actions = {
	storeRoute({commit, getters, rootGetters}){		
		return new Promise((resolve, reject)=>{
			// get a clone of the formdata
            const formData = _.cloneDeep(rootGetters['customRoute/form/formData'])

            // remove some unneeded properties from waypoints
			formData.waypoints.forEach((wp)=>{
				delete wp.id
				delete wp.highlight
			})

			customRouteService.storeRoute(formData)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})			

		})
		
	}
}

export const create = {
	state,
	getters,
	mutations,
	actions,
	namespaced: true
}