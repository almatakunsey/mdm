export const vesselFields = [	
	{
		name:'name', 
		title:'Name',
		// sortField: 'name',
		active:true,
		inReport: true
	},
	{
		name:'distance', 
		title:'Distance',
		// sortField: 'distance',
		active:true,
		inReport: true
	},
	{
		name:'eta', 
		title:'ETA',
		// sortField: 'eta',
		active:true,
		inReport: true
	}
]