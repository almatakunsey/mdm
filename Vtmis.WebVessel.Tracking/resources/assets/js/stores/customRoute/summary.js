const state = {
	enabled: false,
	routeNameEnabled: false
}

const getters = {
	customRouteSummaryEnabled: state => state.enabled,
	routeNameEnabled: state => state.routeNameEnabled
}

const mutations = {
	toggleCustomRouteSummary(state, condition){
		state.enabled = condition
	},
	toggleRouteNameEnabled(state, condition){state.routeNameEnabled = condition}
}

export const summary = {
	state,
	getters,
	mutations,
	namespaced: true
}