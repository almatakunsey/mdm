import L from 'leaflet';
import vesselInfoHelper from '../helpers/vesselinfo'
import utilHelper from '../helpers/util'
import vesselService from '../services/vessels'
import vesselRouteService from '../services/vessel-route'
import _ from 'lodash'

// const defaultLayers = ['defaultCanvas', 'zoomlevel-status', 'customRoutes']
const defaultLayers = ['defaultCanvas', 'customRoutes', 'cables', 'zone']

/*
 ######    ########      ###     ########   ########   
##    ##      ##        ## ##       ##      ##         
##            ##       ##   ##      ##      ##         
 ######       ##      ##     ##     ##      ######     
      ##      ##      #########     ##      ##         
##    ##      ##      ##     ##     ##      ##         
 ######       ##      ##     ##     ##      ########   
*/
const state = {
	map: null,
	center: L.latLng(4.61, 108.26),
	mapLayerGroups: new Map(),
	vessels: new Map(),
	vesselHitBoxes: new Map(),
	stateChangeTracker: 0,
	isMoving: false,	
	selectedVesselId: null,	
	selectedVesselIds: [],	
	zoom: 6,
	layers: _.cloneDeep(defaultLayers),
	tenantVesselMMSI: [],
	routes: new Map(),
	getRouteMemoized: null,	
	metresPerPixel: null,
	offscreenVesselCanvas: null,
	mapType: 'osm',
	selectedMapType: ''
}

/*
 ######    ########   ########   ########   ########   ########    ######    
##    ##   ##            ##         ##      ##         ##     ##  ##    ##   
##         ##            ##         ##      ##         ##     ##  ##         
##   ####  ######        ##         ##      ######     ########    ######    
##    ##   ##            ##         ##      ##         ##   ##          ##   
##    ##   ##            ##         ##      ##         ##    ##   ##    ##   
 ######    ########      ##         ##      ########   ##     ##   ######    
*/
const getters = {	
	tenantVesselMMSI: state => state.tenantVesselMMSI,
	map: state => state.map,
	center: state => state.center,
	mapLayerGroups: state => state.stateChangeTracker && state.mapLayerGroups,
	mapLayers: state => state.layers,
	latlnglist: (state)=>{		
		return state.stateChangeTracker && [...state.vessels.values()].map(({latlng, vesselId})=>{
			return {latlng, vesselId}
		})
	},
	vessels: (state) => state.stateChangeTracker && state.vessels,
	vesselHitBoxes: (state) => state.stateChangeTracker && state.vesselHitBoxes,
	vesselHitBox: (state) => (id) => state.vesselHitBoxes.get(id),	
	vesselHitBoxesArr: (state) => state.stateChangeTracker && [...state.vesselHitBoxes.values()],	
	vesselsArr: (state) => state.stateChangeTracker && [...state.vessels.values()],
	vessel: (state) => (id) => state.vessels.get(id),
	isMoving: (state) => state.isMoving,	
	selectedVesselId: (state) => state.selectedVesselId,	
	selectedVesselIds: (state) => state.selectedVesselIds,	
	zoom: (state)=> state.zoom,
	routes: (state) => state.stateChangeTracker && state.routes,
	getRouteMemoized: (state) => state.getRouteMemoized,
	metresPerPixel: (state) => state.metresPerPixel,
	offscreenVesselCanvas: state => state.offscreenVesselCanvas,
	mapType: state => state.mapType,
	selectedMapType: state => state.selectedMapType,
	vesselDrawnCount: state => {		
		let count = 0		
		state.vessels.forEach((v)=>{
			if(v.isDrawn){
				console.log("drawn")
				count++
			}
		})
		return count
	}	

}

/*
##     ##  ##     ##  ########      ###     ########   ####   #######   ##    ##    ######    
###   ###  ##     ##     ##        ## ##       ##       ##   ##     ##  ###   ##   ##    ##   
#### ####  ##     ##     ##       ##   ##      ##       ##   ##     ##  ####  ##   ##         
## ### ##  ##     ##     ##      ##     ##     ##       ##   ##     ##  ## ## ##    ######    
##     ##  ##     ##     ##      #########     ##       ##   ##     ##  ##  ####         ##   
##     ##  ##     ##     ##      ##     ##     ##       ##   ##     ##  ##   ###   ##    ##   
##     ##   #######      ##      ##     ##     ##      ####   #######   ##    ##    ######    
*/
const mutations = {
	setMap(state, map){state.map = map},
	setMapLayerGroup(state, {name, layerGroup}){
		state.mapLayerGroups.set(name, layerGroup)
		state.stateChangeTracker += 1
	},
	removeMapLayerGroup(state, name){
		state.mapLayerGroups.delete(name)
		state.stateChangeTracker += 1	
	},
	setCenter(state, {lat, lng}){
		state.center = L.latLng(lat, lng)
	},
	setVessel(state, data){	
		data['latlng'] = L.latLng(data.lat, data.lgt)							
		data['isDrawn'] = false
		state.vessels.set(data.vesselId, data)
		state.stateChangeTracker += 1
	},
	setVesselDrawnStatus(state, {vesselId, status, color}){		
		let vessel = state.vessels.get(vesselId)
		vessel['isDrawn'] = status
		vessel['color'] = color
		state.vessels.set(vesselId, vessel)
		state.stateChangeTracker += 1
	},
	clearAllVesselsDrawnStatus(state){
		state.vessels.forEach((vessel, vesselId)=>{
			vessel['isDrawn'] = false
			state.vessels.set(vesselId, vessel)						
		})
		state.stateChangeTracker += 1
	},
	setVesselHitBox(state, {vesselId, poly}){		
		state.vesselHitBoxes.set(vesselId, {polygon: poly.polygon, ctr: poly.ctr})
		state.stateChangeTracker += 1
	},
	clearVesselHitBoxes(state){state.vesselHitBoxes.clear(); state.stateChangeTracker += 1 },	
	toggleMapMove(state, condition){state.isMoving = condition},	
	setSelectedVesselId(state, id){state.selectedVesselId = id},	
	addSelectedVesselId(state, id){	
		if(!_.includes(state.selectedVesselIds, id)){
			state.selectedVesselIds.push(id)	
		}
		// else{
		// 	_.remove(state.selectedVesselIds, n => n == id )
		// }			
	},
	setSelectedVesselIds(state, ids){
		state.selectedVesselIds = ids
	},	
	resetSelectedVesselId(state){state.selectedVesselId = null},	
	resetSelectedVesselIds(state){state.selectedVesselIds = []},	
	removeSelectedVesselId(state, id){_.remove(state.selectedVesselIds, (n)=>{ return n == id })},
	setZoom(state, zoom){state.zoom = zoom},
	addMapLayer(state, layer){ if(!_.includes(state.layers, layer)){ state.layers.push(layer) } },
	setMapLayers(state, layers){state.layers = layers},
	clearMapLayers(state){state.layers = []},
	removeMapLayer(state, layer){state.layers = state.layers.filter( l => l !== layer ) },
	setDefaultMapLayers(state){state.layers = _.cloneDeep(defaultLayers) },
	setTenantVessels(state, data){		
		state.tenantVesselMMSI = []
      
        _.each(data, ({ mmsi }) => { 
            state.tenantVesselMMSI.push(mmsi)
        })
	},
	setRoute(state, data){				
		_.each(data, ({vesselId, vesselRoutePosition})=>{

			const arrRoute = _.map(vesselRoutePosition, ({lat, lgt})=>{
				return {latlng: L.latLng(lat, lgt)}
			})

			state.routes.set(vesselId, arrRoute)
			state.stateChangeTracker += 1						
		})
				
	},
	setGetRouteMemoized(state, fn){state.getRouteMemoized = fn},
	setMetresPerPixel(state, values){state.metresPerPixel = values},
	setOffscreenVesselCanvas(state, canvas){ state.offscreenVesselCanvas = canvas },
	setMapType(state, type){state.mapType = type},
	setSelectedMapType(state, layer){		
		state.selectedMapType = layer
	},
	clearSelectedMapType(state){		
		state.selectedMapType = ''
	}	
}

/*
   ###      ######    ########   ####   #######   ##    ##    ######    
  ## ##    ##    ##      ##       ##   ##     ##  ###   ##   ##    ##   
 ##   ##   ##            ##       ##   ##     ##  ####  ##   ##         
##     ##  ##            ##       ##   ##     ##  ## ## ##    ######    
#########  ##            ##       ##   ##     ##  ##  ####         ##   
##     ##  ##    ##      ##       ##   ##     ##  ##   ###   ##    ##   
##     ##   ######       ##      ####   #######   ##    ##    ######    
*/
const actions = {
	loadTenantVessels({ commit }){		
		
		return new Promise((resolve, reject) => {
			vesselService.getTenantVesselsList()
			.then(( {data:{result:{items}}} )=>{				
				commit('setTenantVessels', items)
                resolve('Get tenant vessel: OK')
			})
			.catch((error)=>{
				reject('Get tenant vessel ERROR: ' + error)
			})    
        })
	},
	updateRoutes({commit}, {vesselIds}){		
		return new Promise((resolve, reject)=>{			
			vesselRouteService.updateRoutes(vesselIds)
			.then(({data})=>{
				commit('setRoute', data)			
				resolve(data)
			})
			.catch((error)=>{
				reject(error)
			})
		})
	},

	addSelectedVesselId({commit, getters, rootGetters}, {id, ctrlKey}){				

		// limit number of vessel info panel
		// if( rootGetters['app/modules/activeModule'] == 'analytic' && 
		// 	getters.selectedVesselIds.length  >= 5
		// ){
		if(getters.selectedVesselIds.length  >= 5){
			return false;
		}

		// if( rootGetters['app/modules/activeModule'] == 'monitoring' && !ctrlKey ){
		if(!ctrlKey ){
			commit('resetSelectedVesselIds')
		}
		
		commit('addSelectedVesselId', id)
	}
}

/*
export
 */
export const map = {
	state,	
	getters,
	mutations,
	actions,
	namespaced: true
}