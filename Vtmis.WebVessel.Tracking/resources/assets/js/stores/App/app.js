import {screensize} from './screensize'
import {modules} from './modules'
import {session} from './session'
import {widgets} from './widgets'

const state = {
	token: '',
	theme: 'theme-four',
	tenantId: ''
}

const getters = {
	token: state => state.token,
	theme: state => state.theme,
	tenantId: state => state.tenantId
}

const mutations = {
	setToken(state, token){
		state.token = token
	},
	setTheme(state, theme){
		state.theme = theme
	},
	setTenantId(state, id){
		state.tenantId = id
	}
}


export const app = {
	state,
	getters,
	mutations,	
	namespaced: true,
	modules:{
		screensize,
		modules,
		session,
		widgets		
	}
}