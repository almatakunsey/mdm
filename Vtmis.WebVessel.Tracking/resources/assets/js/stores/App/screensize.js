const state = {	
	screenSize: {
		current : 0,
		xs: false,
		sm: false,
		md: false,
		lg: false,
		xl: false		
	}
}

const getters = {
	screenSize(state){return state.screenSize},
	isXs(state){return state.screenSize.xs},
	isSm(state){return state.screenSize.sm},
	isMd(state){return state.screenSize.md},
	isLg(state){return state.screenSize.lg},
	isXl(state){return state.screenSize.xl},

	isXsAndLarger(state){return state.screenSize.xs || state.screenSize.sm || state.screenSize.md || state.screenSize.lg || state.screenSize.xl},
	isSmAndLarger(state){return state.screenSize.sm || state.screenSize.md || state.screenSize.lg || state.screenSize.xl},
	isMdAndLarger(state){return state.screenSize.md || state.screenSize.lg || state.screenSize.xl},	
	isLgAndLarger(state){return state.screenSize.lg || state.screenSize.xl},

	isXlAndSmaller(state){return state.screenSize.xs || state.screenSize.sm || state.screenSize.md || state.screenSize.lg || state.screenSize.xl},
	isLgAndSmaller(state){return state.screenSize.xs || state.screenSize.sm || state.screenSize.md || state.screenSize.lg},
	isMdAndSmaller(state){return state.screenSize.xs || state.screenSize.sm || state.screenSize.md},
	isSmAndSmaller(state){return state.screenSize.xs || state.screenSize.sm}	
}

const mutations = {	
	setScreenSize(state, size){	
		state.screenSize.current = size		
		state.screenSize.xs = size < 768
		state.screenSize.sm = size >= 768 && size < 992
		state.screenSize.md = size >= 992 && size < 1200
		state.screenSize.lg = size >= 1200 && size < 1900
		state.screenSize.xl = size >= 1900
	}
}

export const screensize = {
	state,	
	getters,
	mutations,	
	namespaced: true
}