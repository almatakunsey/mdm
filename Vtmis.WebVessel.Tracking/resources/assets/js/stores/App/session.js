const state = {
	user: {
		tenantName: ''
	}
}

const getters = {
	user: state => state.user	
}

const mutations = {
	setUserState(state, {key, value}){		
		state.user[key] = value		
	}
}

export const session = {
	state,	
	getters,
	mutations,
	namespaced: true	
}