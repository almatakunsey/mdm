const widget = {
	name: ''
}

const state = {
	widgets: {
		active: [],
		lgth: 0
	}
}

const getters = {
	activeWidgets: state => state.widgets.active,
	widgetLength: state => state.widgets.lgth
}

const mutations = {
	addActiveWidget(state, {name}){		
		const newWidget = _.cloneDeep(widget)
		newWidget.name = name
		state.widgets.active.push(newWidget)
		state.widgets.lgth = state.widgets.active.length
	},
	removeActiveWidget(state, {name}){		
		_.remove(state.widgets.active, {name})		
		state.widgets.lgth = state.widgets.active.length
	},
	clearActiveWidgets(state){				
		state.widgets.active = []
		state.widgets.lgth = 0
		state.widgets.lgth = state.widgets.active.length
	}

}

export const widgets = {
	state,
	getters,
	mutations,
	namespaced:true
}