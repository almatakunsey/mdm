const state = {
	// other state include 'analytic'
	active: 'monitoring'
}

const getters = {
	activeModule: state => state.active
}

const mutations = {	
	setActiveModule(state, module){ state.active = module }
}

export const modules = {
	state,
	getters,
	mutations,
	namespaced: true
}