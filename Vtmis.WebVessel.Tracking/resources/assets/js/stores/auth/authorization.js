import authService from '../../services/authorization'

const state = {
	userLevel: '',
	permissions: []
}

const getters = {
	permissions: state => state.permissions,
	permission: (state) => (name) => _.indexOf(state.permissions, name) != -1,
	userLevel: state => state.userLevel
}

const mutations = {
	setPermissions(state, permissions){		
		state.permissions = [...permissions]

	},
	setUserLevel(state, level){state.userLevel = level}
}

const actions = {
	loadPermissions({commit, getters}){
		authService.getGrantedPermissions()
		.then((result)=>{			
			commit('setPermissions', _.isArray(result) ? result : [])			
		})
		.catch((error)=>{
			console.error(error)
		})
	},
	requestPermission({commit}, permissionName){
		return new Promise((resolve, reject)=>{
			authService.getPermission(permissionName)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})
	}
}

export const authorization = {
	state,
	getters,
	mutations,
	actions,
	namespaced: true	
}