import {InvalidUsernamePasswordError, InvalidTenantError} from '../../helpers/errors'
import utilHelper from '../../helpers/util'

import authService from '../../services/authentication'

const state = {
	login: {		
		error: null
	},
}

const mutations = {	
	setCheckSession(state, condition){state.checkSession = condition},
	setLoginError: (state, error) => {state.login.error = error},
	clearLoginError: (state) => { state.login.error = null }
}

const getters = {	
	checkSession: state => state.checkSession,
	loginError: state => state.login.error
}

const actions = {	
	login: ({commit, dispatch, rootState, getters}, {username, password, tenantName})=>{		
					
		return new Promise((resolve, reject)=>{			
			// axios.post(`${process.env.WEBADMIN_API_BASE_URL}/api/TokenAuth/Authenticate`, {								
			authService.login(username, password, tenantName)
			.then( ( { data } )=>{						
				resolve(data)	
			})			
			.catch(( {response} )=>{											

				if(_.has(response.data.error, 'validationErrors')){	
					
					const validationErrors = response.data.error.validationErrors					
					if(validationErrors && 
						( 
							(validationErrors[0].members[0]) === "password" ||
							(validationErrors[0].members[0]) === "userNameOrEmailAddress"
						)
					){						
						commit('setLoginError', new InvalidUsernamePasswordError("User Name And Password Is Required", response))
					} 
				}

				if(_.has(response.data.error, 'details')){
					
					const details = response.data.error.details
					if(details === 'Invalid user name or password'){						
						commit('setLoginError', new InvalidUsernamePasswordError("Invalid UserName Or Password", response))
					} 
					else if(details === `User ${username} is not active and can not log in.`){						
						commit('setLoginError', new InvalidUsernamePasswordError(`User ${username} is not active and can not log in.`, response))
					}
				}

				reject(response)

			})


		})		
	},
	logout: ({commit, dispatch, rootState})=>{
		return new Promise((resolve, reject)=>{
			authService.logout()
			.then((data)=>{										
				resolve(data)
			})
			.catch( ({message, response})=>{				
				reject(response)
			})
		})
	},
	checkSession: ({commit})=>{		
		return new Promise((resolve, reject)=>{
			authService.checkSession()
			.then(
				({data:{result}})=>{
					resolve(result)
					commit('setCheckSession', false)
				},
				(error)=>{
					reject(error)
				}
			)

		})
	}
}

export const authentication = {
	state,
	mutations,
	getters,
	actions,
	namespaced: true
}