import {authentication} from './authentication'
import {authorization} from './authorization'

export const auth = {
	namespaced: true,
	modules: {
		authentication,
		authorization
	}
}