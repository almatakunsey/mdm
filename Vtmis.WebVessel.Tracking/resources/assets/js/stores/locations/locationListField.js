export const fields = [
	{
		name:'Name', 
		title:'Name',
		sortField: 'Name',
		active:true,
		inReport: true
	},
	{
		name:'Latitude', 
		title:'Latitude',
		// sortField: 'distance',
		active:true,
		inReport: true
	},
	{
		name:'Longitude', 
		title:'Longitude',
		// sortField: 'eta',
		active:true,
		inReport: true
	},
	{
		name:'zoomLevel', 
		title:'Zoom Level',
		// sortField: 'eta',
		active:true,
		inReport: true
	},
	// {
 //        name: '__slot:actions',
 //        title: 'Actions',
 //        active:true,
 //        inReport: false
 //    }
	]