import {summary} from './summary'
import {create} from './create'
import {update} from './update'
import {form} from './form'

import utilHelper from '../../helpers/util'
import locationService from '../../services/locations'


const state = {	
	mainPane: {
		enabled: false
	},
    locations: {},
    tableData: [],
    tableColumn: [
        { data: 'Name' },
        { data: 'Latitude' },
        { data: 'Longitude' },
        { data: 'zoomLevel' },
        { data: null, "defaultContent": 
            `
            <button class="btn btn-md btn-warning" data-action="edit">
                <span class="fa fa-pencil-square-o" aria-hidden="true"></span>
            </button>

            <button class="btn btn-md btn-danger" data-action="delete">
                <span class="fa fa-trash-o" aria-hidden="true"></span>
            </button>
            
            ` 
        }
    ],
    selectedLocation: null,
    designer:{
        enabled:false        
    },
    fromDesigner: false

}

const getters = {		
    locations: state => state.locations,
    tableData: state => state.tableData,
    tableColumn: state => state.tableColumn,
    selectedLocation: state => state.selectedLocation,
    locationDesignerEnabled: state => state.designer.enabled,
    fromDesigner: state => state.fromDesigner
}

const mutations = {		
    toggleLocationDesigner(state, condition){state.designer.enabled = condition},
    toggleFromDesigner(state, condition){ state.fromDesigner = condition },
    setLocations(state, locationInfo) {
        const formattedLocations = locationInfo.result.items.map((l)=>{
            return {
                id: l.id, 
                Name: l.name, 
                Longitude: l.longitude, 
                Latitude: l.latitude, 
                TenantId: l.tenantId,
                zoomLevel: l.zoomLevel,
            //     action: `
            // <button class="btn btn-md btn-warning" data-action="edit">
            //     <span class="fa fa-pencil-square-o" aria-hidden="true"></span>
            // </button>

            // <button class="btn btn-md btn-danger" data-action="delete">
            //     <span class="fa fa-trash-o" aria-hidden="true"></span>
            // </button>
            
            // `
            }
        })

        state.tableData = []

        state.locations = state.tableData = formattedLocations        
    },
    setSelectedLocation(state, location){ state.selectedLocation = location }
}

const actions = {
    loadLocations({ commit }) {        
        return new Promise((resolve, reject) => {
            locationService.getLocations()
            .then(({data})=>{                
                commit('setLocations', data)
                resolve(data)
            })
            .catch((error)=>{
                reject(error)
            })            
        })
    },

    deleteLocation({ commit, getters }, id){
        return new Promise((resolve, reject)=>{
            locationService.deleteLocation(id)
            .then((result)=>{
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
        })
    }
}

export const locations = {	
	state,
	getters,
	mutations,
    actions,
	namespaced: true,
	modules:{
		summary,
        create,
        update,
        form
	}
}

