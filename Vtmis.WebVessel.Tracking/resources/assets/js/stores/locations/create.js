import utilHelper from '../../helpers/util'
import locationService from '../../services/locations'
import _ from 'lodash'

const state = {
    enabled: false    
}

const getters = {
    enabled: state => state.enabled
}

const mutations = {
    toggleLocationCreate(state, condition){ state.enabled = condition },
    updateFormData(state, {key, value}){state.formData[key] = value},
    clearFormData(state){ state.formData = _.cloneDeep(formDataOrigin) }
}

const actions = {
    createLocations({ commit, getters, rootGetters }){
            
        return new Promise((resolve, reject) => {            

            locationService.storeLocation(rootGetters['locations/form/formData'])
            .then((result)=>{
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
            
        })  
    }
}

export const create = {
    state,
    getters,  
    actions,  
    mutations,
    namespaced: true
}

