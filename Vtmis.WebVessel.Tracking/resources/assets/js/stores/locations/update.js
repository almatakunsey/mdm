import utilHelper from '../../helpers/util'
import locationService from '../../services/locations'
import _ from 'lodash'

const state = {
    enabled: false
}

const getters = {
    enabled: state => state.enabled    
}

const mutations = {
    toggleLocationUpdate(state, condition){ state.enabled = condition }    
}

const actions = {
    loadLocationFormData({commit, rootGetters})
    {               
        commit(
            'locations/form/setFormData', 
            rootGetters['locations/selectedLocation'], 
            {root:true}
        ) 
    },
    updateLocations({ commit, getters, rootGetters }){
                   
        return new Promise((resolve, reject) => {

            locationService.updateLocation(rootGetters['locations/form/formData'])
            .then((result)=>{
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
            
        })  
    }
}

export const update = {
    state,
    getters,  
    actions,  
    mutations,
    namespaced: true
}

// Comment to trigger changes in git