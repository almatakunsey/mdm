import utilHelper from '../../helpers/util'
import { fields } from './locationListField'

const formDataOrigin = {
    id:'',
    uid: utilHelper.genRandom(32),
    name: '',
    longitude: '',
    latitude: '',
    zoomLevel: 8,
    highlight:false,
    searchTerm: ''
}

const state = {
	formData: _.cloneDeep(formDataOrigin),
    fields: fields 
}

const getters = {    
    formData: state => state.formData,
    field: state => state.fields
}

const mutations = { 
    updateFormData(state, {key, value}){state.formData[key] = value},
    clearFormData(state){ state.formData = _.cloneDeep(formDataOrigin) },
    setFormData(state, {id, Name, Latitude, Longitude, zoomLevel}){
        state.formData = {
            id,
            name:Name,
            latitude:Latitude,
            longitude:Longitude,
            zoomLevel
        }
    },
    setField(state, field){state.fields = field}, 
    addNewWaypoint(state, data){        
        
        if( typeof data !== 'undefined' && 
            typeof data.latitude !== 'undefined'){          
            state.formData.latitude = data.latitude
        }
        if( typeof data !== 'undefined' && 
            typeof data.longitude !== 'undefined'){
            state.formData.longitude = data.longitude
        }

    },
    replaceFormData(state, formData){
        state.formData = formData
    },
    setWaypointData(state, {key, value}){       
        state.formData[key] = value
    },
    removeWaypoint(state){      
        state.formData.latitude = '',
        state.formData.longitude = ''
    },
    clearWaypoints(state){
        state.formData.latitude = '',
        state.formData.longitude = ''
    },
    togglePointHighlight(state, {uid, value}){        
        state.formData.highlight = value               
    },
    clearPointHighlight(state){
        if(state.formData.latitude && state.formData.longitude){
            state.formData.highlight = false
        }
    }
}

export const form = {
	state,
	getters,
	mutations,
	namespaced: true
}