const state = {
    enabled: false,
    error: null
}

const mutations = {
    setSummaryError(state, error){state.error = error},
    toggleAlarmSummary(state, condition){state.enabled = condition}
}

const getters = {
    summaryError : state => state.error,
    enabled: state => state.enabled
}

export const summary = {
    state,
    getters,    
    mutations,
    namespaced: true
}