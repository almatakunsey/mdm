﻿import utilHelper from '../../helpers/util'
import alarmService from '../../services/alarms'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const state = {
    enabled: false,
    alarmData: {}
}

const getters = {
    enabled: state => state.enabled,
}

const mutations = {
    toggleAlarmUpdate(state, condition) { state.enabled = condition },
}

const actions = {
    
    updateAlarm({ commit, getters, rootGetters }){
        return new Promise((resolve, reject) => {
            alarmService.UpdateAlarm(rootGetters['alarm/form/alarmData'])
            .then((result)=>{
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })            
        })
    },
}

export const update = {
    state,
    getters,
    actions,
    mutations,
    namespaced: true
}
