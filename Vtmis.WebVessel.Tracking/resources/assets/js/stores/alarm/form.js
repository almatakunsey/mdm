import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

import filterService from '../../services/filters'
import zoneService from '../../services/zones'
import alarmService from '../../services/alarms'

import options from './data/options'

const alarmItemDtosPristine = {
    rid: utilHelper.genRandom(32),
    condition:0,
    columnType:0,
    columnItem: 0,
    operation: 0,
    columnValue: 0
}

const formDataPristine = {
        name: '',
        isEnabled:true,
        priority: 0,
        message: '',
        color: '#004080',
        repeatInterval: 60,
        isSendAISSafetyMessage: true,
        aisMessage: '',
        emailAddress: '',
        isSendEmail: true,
        alarmItems: [_.cloneDeep(alarmItemDtosPristine)]
}

const state = {
    alarmData : _.cloneDeep(formDataPristine),
    alarmPriorityOptions: options.priorityOptions,
    conditionTypesOptions: options.conditionTypesOptions,
    logicalOptions: options.logicalOptions,
    operationOptions: options.operationOptions,
    conditionOneOptions: options.conditionOneOptions,
    conditionTwoOptions: options.conditionTwoOptions,
    filtersOptions: [],
    zonesOptions: []
}
const getItemIndex = (state, rid)=>{
    return _.findIndex(state.alarmData.alarmItems, {rid})
}
const getters = {
    alarmData: state => state.alarmData,
    alarmItems: state => state.alarmData.alarmItems,
    priorityOptions: state => state.alarmPriorityOptions,
    conditionTypesOptions: state => state.conditionTypesOptions,
    logicalOptions: state => state.logicalOptions,
    operationOptions: state => state.operationOptions,
    conditionOneOptions: state => state.conditionOneOptions,
    conditionTwoOptions: state => state.conditionTwoOptions,
    filtersOptions: state => state.filtersOptions,
    zonesOptions: state => state.zonesOptions
}

const mutations = {
    clearData(state) {state.alarmData = _.cloneDeep(formDataPristine)},
    setAlarmData(state, {key, value}){
        state.alarmData[key] = value        
    },
    addAlarmConditionItem(state){
        const newDtoItem = _.cloneDeep(alarmItemDtosPristine)
        newDtoItem.rid = utilHelper.genRandom(32)
        state.alarmData.alarmItems.push(newDtoItem)
    },
    removeAlarmConditionItem(state, index){   
        state.alarmData.alarmItems.splice(index, 1)
        if(index == 0) {
            state.alarmData.alarmItems[0].condition = '-1'
        }  
    },
    setFormAlarmItem(state, {key, value, rid}){
        const itemIndex = getItemIndex(state, rid)
        state.alarmData.alarmItems[itemIndex][key] = value  
    },
    setFilters(state, filters){        
       state.filtersOptions.push( {text:'Select Filter', id:0 } )
        _.each(filters, ({ id, name }) => { 
            state.filtersOptions.push({ text: name, id })
        })
    },
    clearFilters(state)
    {
        state.filters = []
        state.filtersOptions = []
    },
    setZones(state, zones){    
        state.zonesOptions.push( {text:'Select Zone', id:0 } )    
        _.each(zones, ({ id, name }) => { 
            state.zonesOptions.push({ text: name, id })
        })
    },
    clearZones(state){
        state.zones = []
        state.zonesOptions = []
    },
    replaceAlarmData(state, alarmData){
        state.alarmData = alarmData
    }
}

const actions = {
    getFilterList({commit}){
        commit('clearFilters')
        return new Promise((resolve, reject)=>{
            filterService.getFilters()
            .then( ({data:{result}}) =>{                
                commit('setFilters', result.items)
                resolve(result)
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })  
    },
    getZoneList({commit}){
        commit('clearZones')        
        return new Promise((resolve, reject)=>{ 
            zoneService.GetZonesByUserId(localStorage.userId)           
             .then((result)=>{
                commit('setZones', result)                
                resolve(result)
            })            
            .catch((error)=>{
                reject(error)
            })
           
        })
    }
}

export const form = {
    state,
    getters,
    actions,
    mutations,
    namespaced: true
}