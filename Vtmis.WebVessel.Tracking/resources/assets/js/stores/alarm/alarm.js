import {summary} from './summary'
import {create} from './create'
import {update} from './update'
import {form} from './form'
import {notification} from './notification'

import utilHelper from '../../helpers/util'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

import filterService from '../../services/filters'
import zoneService from '../../services/zones'
import alarmService from '../../services/alarms'

const state = {	    
    selectedAlarm: null,   
    alarms: []
}

const getters = {     
    alarms : state => state.alarms,
    selectedAlarm: state => state.selectedAlarm
}

const mutations = {        
    setAlarms(state, alarms){state.alarms = alarms},
    clearAlarms(state){state.alarms = []},    
    setSelectedAlarm(state, alarm){state.selectedAlarm = alarm},
    removeSelectedAlarm(state){_.remove(state.alarms, alarm => alarm.id == state.selectedAlarm.id )},
    updateAlarmConditionItem(state, {value, type, id}){
        const dataIndex = _.findIndex(state.alarms.alarmItems, {id})       
        state.alarms.alarmItems[dataIndex][type] = value        
    }
}

const actions = {
    loadAlarms({commit}){
        return new Promise((resolve, reject)=>{
            commit('clearAlarms')
            alarmService.GetMyAlarms()
            .then((result)=>{
                    resolve(result)
                    commit('setAlarms', result)
            })
            .catch((error)=>{
                reject(error)
            })
        })        
    },
    deleteAlarm({commit, getters}, id){
        return new Promise((resolve, reject)=>{
            alarmService.deleteAlarm(id)
            .then((result)=>{
                // commit('removeSelectedAlarm')
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
        })
    }
}

export const alarm = {	
	state,
	getters,
    mutations,
    actions,
    namespaced: true,
    modules: {
        summary,
        create,
        update,
        form,
        notification
    }	
}