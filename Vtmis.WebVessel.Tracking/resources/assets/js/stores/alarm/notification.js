import alarmService from '../../services/alarms'

const state = {
	enabled: false,
	notifications: [],
	alerts: []
}

const getters = {
	alarmNotificationEnabled: state => state.enabled,
	notifications: state => state.notifications,
	alerts: state => state.alerts
}

const mutations = {
	toggleAlarmNotification(state, condition){
		state.enabled = condition
	},
	setNotifications(state, notifications){
		state.notifications = notifications
	},
	clearNotifications(state){
		state.notifications = []
	},
	addAlert(state, alert){
		if( !_.find(state.alerts, {alarmId:alert.alarmId}) ){			
			alert['read'] = false			
			state.alerts.push(alert)						
		}		
	},
	setAllAlertsAsRead(state){
		state.alerts.forEach((a)=>{
			a.read = true
		})
	}
}

const actions = {
	loadNotifications({commit}, {page}){
		commit('clearNotifications')
		alarmService.getNotifications(page).then((result)=>{
			console.log(result)
		})

	}
}

export const notification = {
	state,
	getters,
	mutations,
	actions,
	namespaced:true
}