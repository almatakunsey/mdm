﻿import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

import alarmService from '../../services/alarms'

const state = {
    enabled: false
}

const getters = {
    enabled: state => state.enabled,
}

const mutations = {
    toggleAlarmCreate(state, condition) { state.enabled = condition },
}

const actions = {
    saveAlarm({commit, getters, rootGetters}) {
        return new Promise((resolve, reject)=>{
            alarmService.saveAlarm(rootGetters['alarm/form/alarmData'])
            .then((result)=>{
                resolve("Data successfully saved!")
            })
            .catch((error)=>{
                reject(error)
            })
            
        })
    }
}

export const create = {
    state,
    getters,
    actions,
    mutations,
    namespaced: true
}