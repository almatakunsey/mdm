import statisticService from '../../services/statistic'
import moment from 'moment'
import { rmsFields } from './rmsListField'
import { ddmsFields } from './ddmsListField'
import { weatherFields } from './weatherListField'

const formDataOrigin = {
    startDate: moment().subtract(48, 'hours').format('YYYY-MM-DDTHH:mm:ss'),
    endDate: moment().format('YYYY-MM-DDTHH:mm:ss'),
    timeSpan: 'user defined',
    searchTerm: '',
    // startDate: moment('2018-10-25T08:00:00').format('YYYY-MM-DDTHH:mm:ss'),
    // endDate: moment('2018-10-25T23:00:00').format('YYYY-MM-DDTHH:mm:ss')
}

const state = {
    enabled: false,
    formData : _.cloneDeep(formDataOrigin),
    rmsFields:rmsFields,
    ddmsFields:ddmsFields,
    weatherFields:weatherFields,
    chartData: [],
    timeSpanOptions: [
        {text: 'User Defined', id: 'user defined'},
        {text: 'Last 24 Hours', id: '24 hours'},
        {text: 'Last 48 Hours', id: '48 hours'},
        {text: 'Last 10 Days', id: 'last 10 days'},
        {text: 'Last 30 Days', id: 'last 30 days'},
        {text: 'Today', id: 'today'},
        {text: 'Yesterday', id: 'yesterday'},
        {text: 'This Week', id: 'this week'},
        {text: 'Last Week', id: 'last week'},
        {text: 'This Month', id: 'this month'},
        {text: 'Last Month', id: 'last month'},
    ],
}

const getters = {
    enabled: state => state.enabled,
    formData: state => state.formData,
    rmsField: state => (type) => {        
        return state.rmsFields[type]        
    },
    // rmsField: state => state.rmsFields,
    ddmsField: state => state.ddmsFields,
    weatherField: state => state.weatherFields,
    chartData: state => state.chartData,
    timeSpanOptions: state => state.timeSpanOptions
}

const mutations = {
    toggleStatisticChart(state, condition){state.enabled = condition},
    setFormData(state, {key, value}){
        state.formData[key] = value        
    },
    setRmsField(state, {type, rmsField}){state.rmsFields[type] = rmsField},  
    // setRmsField(state, rmsField){state.rmsFields = rmsField},
    setDdmsField(state, ddmsField){state.ddmsFields = ddmsField},
    setWeatherField(state, weatherField){state.weatherFields = weatherField},
    setChartData(state, chartData){ state.chartData = chartData },
    clearFormData(state){ state.formData = _.cloneDeep(formDataOrigin)}
}

const actions = {

    getRmsHistoricalByMmsi : ({commit, getters}, mmsi) => {

        const startDate = getters.formData.startDate
        const endDate = getters.formData.endDate
        
        return new Promise((resolve, reject)=>{

            statisticService.getRmsHistorical(mmsi,startDate,endDate)
            .then(({data:{result}}) =>{             
                commit('setChartData', result)
                console.log("%cArray:", conStyle.boldWhite, result)
                resolve(result)
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })                              
    },

    getWeatherHistoricalByMmsi : ({commit, getters}, mmsi) => {

        const startDate = getters.formData.startDate
        const endDate = getters.formData.endDate
        
        return new Promise((resolve, reject)=>{

            statisticService.getWeatherHistorical(mmsi,startDate,endDate)
            .then(({data:{result}}) =>{             
                commit('setChartData', result)
                console.log("%cWeather Arr:", conStyle.boldWhite, result)
                resolve(result)
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })                              
    },

    getPublicWeatherHistoricalByMmsi : ({commit, getters}, {mmsi, token}) => {

        const startDate = getters.formData.startDate
        const endDate = getters.formData.endDate
        
        return new Promise((resolve, reject)=>{

            statisticService.getPublicWeatherHistorical(mmsi,token,startDate,endDate)
            .then(({data:{result}}) =>{             
                commit('setChartData', result)
                console.log("%cWeather Arr:", conStyle.boldWhite, result)
                resolve(result)
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })                              
    },

    getDdmsHistoricalByMmsi : ({commit, getters}, mmsi) => {

        const startDate = getters.formData.startDate
        const endDate = getters.formData.endDate
        
        return new Promise((resolve, reject)=>{

            statisticService.getDdmsHistorical(mmsi,startDate,endDate)
            .then(({data:{result}}) =>{             
                commit('setChartData', result)
                console.log("%cDdms Arr:", conStyle.boldWhite, result)
                resolve(result)
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })                              
    }
    


}

export const statisticChart = {
    state,
    getters,    
    mutations,
    actions,
    namespaced: true
}