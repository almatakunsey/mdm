export const weatherFields = [
	{
		name: "name",
		title:'Name',
		sortField: 'name',
		active:true,
		inReport: true
	},
	{
		name:"airPressure",
		title:'Air Pressure',
		sortField: 'airPressure',
		active:true,
		inReport: true
	}, 
	{
		name:"airTemperature",
		title:'Air Temperature',
		sortField: 'airTemperature',
		active:true,
		inReport: true
	}, 
	{
		name:"currentDirection2",
		title:'Current Direction 2',
		sortField: 'currentDirection2',
		active:true,
		inReport: true
	}, 
	{
		name:"currentDirection3",
		title:'Current Direction 3',
		sortField: 'currentDirection3',
		active:true,
		inReport: true
	}, 
	{
		name: "currentSpeed2",
		title:'Current Speed 2',
		sortField: 'currentSpeed2',
		active:true,
		inReport: true
	}, 
	{
		name: "currentSpeed3",
		title:'Current Speed 3',
		sortField: 'currentSpeed3',
		active:true,
		inReport: true
	},  
	{
		name: "relHumidity",
		title:'Humidity',
		sortField: 'relHumidity',
		active:true,
		inReport: true
	}, 
	{
		name: "surfCurrentDirection",
		title:'Surface Current Direction',
		sortField: 'surfCurrentDirection',
		active:true,
		inReport: true
	}, 
	{
		name: "surfCurrentSpeed",
		title:'Surface Current Speed',
		sortField: 'surfCurrentSpeed',
		active:true,
		inReport: true
	}, 
	{
		name: "visibility",
		title:'Visibility',
		sortField: 'visibility',
		active:true,
		inReport: true
	}, 
	{
		name: "waterLevel",
		title:'Water Level',
		sortField: 'waterLevel',
		active:true,
		inReport: true
	}, 
	{
		name: "waterTemp",
		title:'Water Temperature',
		sortField: 'waterTemp',
		active:true,
		inReport: true
	}, 
	{
		name: "waveDirection",
		title:'Wave Direction',
		sortField: 'waveDirection',
		active:true,
		inReport: true
	}, 
	{
		name: "waveHeight",
		title:'Wave Height',
		sortField: 'waveHeight',
		active:true,
		inReport: true
	}, 
	{
		name: "wavePeriod",
		title:'Wave Period',
		sortField: 'wavePeriod',
		active:true,
		inReport: true
	}, 
	{
		name: "windDirection",
		title:'Wind Direction',
		sortField: 'windDirection',
		active:true,
		inReport: true
	}, 
	{
		name: "windGust",
		title:'Wind Gust',
		sortField: 'windGust',
		active:true,
		inReport: true
	}, 
	{
		name: "windGustDirection",
		title:'Wind Gust Direction',
		sortField: 'windGustDirection',
		active:true,
		inReport: true
	}, 
	{
		name: "windSpeed",
		title:'Wind Speed',
		sortField: 'windSpeed',
		active:true,
		inReport: true
	},
	{
		name:'utcDate', 
		title:'Received Time (UTC)',
		sortField: 'utcDate',
		active:true,
		inReport: true
	},
	{
		name:'localDate', 
		title:'Received Time (Local)',
		sortField: 'localDate',
		active:true,
		inReport: true
	}
]
