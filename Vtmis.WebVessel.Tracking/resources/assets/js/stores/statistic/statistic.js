import {statisticChart} from './statisticChart'

export const statistic = {		
    namespaced: true,
    modules: {
        statisticChart
    }	
}