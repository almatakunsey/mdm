export const rmsFields = {
	ZeniLite: [
		{
			name:'name', 
			title:'Name',
			sortField: 'name',
			active:true,
			inReport: true
		},
		{
			name:'mmsi', 
			title:'MMSI',
			sortField: 'mmsi',
			active:true,
			inReport: true
		},
		{
			name:'voltageData', 
			title:'Voltage',
			sortField: 'voltageData',
			active:true,
			inReport: true
		},
		
		{
			name:'powerSupplyType', 
			title:'Power Supply Type',
			sortField: 'powerSupplyType',
			active:true,
			inReport: true
		},
		{
			name:'currentData', 
			title:'Current',
			sortField: 'currentData',
			active:true,
			inReport: true
		},
		{
			name:'lightStatus', 
			title:'Light',
			sortField: 'lightStatus',
			active:true,
			inReport: true
		},
		{
			name:'batteryStatus', 
			title:'Battery Status',
			sortField: 'batteryStatus',
			active:true,
			inReport: true
		},
		{
			name:'utcDate', 
			title:'Received Time (UTC)',
			sortField: 'utcDate',
			active:true,
			inReport: true
		},
		{
			name:'localDate', 
			title:'Received Time (Local)',
			sortField: 'localDate',
			active:true,
			inReport: true
		}
	],
	IALA : [
		{
			name:'name', 
			title:'Name',
			sortField: 'name',
			active:true,
			inReport: true
		},
		{
			name:'mmsi', 
			title:'MMSI',
			sortField: 'mmsi',
			active:true,
			inReport: true
		},
		{
			name:'statusSpecific.health', 
			title:'Health',
			sortField: 'statusSpecific.health',
			active:true,
			inReport: true
		},
		{
			name:'statusSpecific.light', 
			title:'Light',
			sortField: 'statusSpecific.light',
			active:true,
			inReport: true
		},
		{
			name:'statusSpecific.racon', 
			title:'RACON',
			sortField: 'statusSpecific.racon',
			active:true,
			inReport: true
		},
		{
			name:'supplyVoltageInt', 
			title:'Analogue Internal',
			sortField: 'supplyVoltageInt',
			active:true,
			inReport: true
		},
		{
			name:'supplyVoltageExt1', 
			title:'Analogue External 1',
			sortField: 'supplyVoltageExt1',
			active:true,
			inReport: true
		},
		{
			name:'supplyVoltageExt2', 
			title:'Analogue External 2',
			sortField: 'supplyVoltageExt2',
			active:true,
			inReport: true
		},
		{
			name:'utcDate', 
			title:'Received Time (UTC)',
			sortField: 'utcDate',
			active:true,
			inReport: true
		},
		{
			name:'localDate', 
			title:'Received Time (Local)',
			sortField: 'localDate',
			active:true,
			inReport: true
		}
	],
	LightBeacon : [
		{
			name:'name', 
			title:'Name',
			sortField: 'name',
			active:true,
			inReport: true
		},
		{
			name:'mmsi', 
			title:'MMSI',
			sortField: 'mmsi',
			active:true,
			inReport: true
		},
		{
			name:'statusSpecific.health', 
			title:'Health',
			sortField: 'statusSpecific.health',
			active:true,
			inReport: true
		},
		{
			name:'statusSpecific.light', 
			title:'Light',
			sortField: 'statusSpecific.light',
			active:true,
			inReport: true
		},
		{
			name:'statusSpecific.racon', 
			title:'RACON',
			sortField: 'statusSpecific.racon',
			active:true,
			inReport: true
		},
		{
			name:'beat', 
			title:'Beat',
			sortField: 'beat',
			active:true,
			inReport: true
		},
		{
			name:'lanternBatt', 
			title:'Lantern Battery',
			sortField: 'lanternBatt',
			active:true,
			inReport: true
		},
		{
			name:'lantern', 
			title:'Lantern',
			sortField: 'lantern',
			active:true,
			inReport: true
		},
		{
			name:'ambient', 
			title:'Ambient',
			sortField: 'ambient',
			active:true,
			inReport: true
		},
		{
			name:'door', 
			title:'Door',
			sortField: 'door',
			active:true,
			inReport: true
		},
		{
			name:'supplyVoltageInt', 
			title:'Voltage Internal',
			sortField: 'supplyVoltageInt',
			active:true,
			inReport: true
		},
		{
			name:'supplyVoltageExt1', 
			title:'Voltage External 1',
			sortField: 'supplyVoltageExt1',
			active:true,
			inReport: true
		},
		{
			name:'supplyVoltageExt2', 
			title:'Voltage External 2',
			sortField: 'supplyVoltageExt2',
			active:true,
			inReport: true
		},

		{
			name:'utcDate', 
			title:'Received Time (UTC)',
			sortField: 'utcDate',
			active:true,
			inReport: true
		},

		
	],
	LightHouse : [
		{
			name:'name', 
			title:'Name',
			sortField: 'name',
			active:true,
			inReport: true
		},
		{
			name:'mmsi', 
			title:'MMSI',
			sortField: 'mmsi',
			active:true,
			inReport: true
		},
		{
			name:'statusSpecific.health', 
			title:'Health',
			sortField: 'statusSpecific.health',
			active:true,
			inReport: true
		},
		{
			name:'statusSpecific.light', 
			title:'Light',
			sortField: 'statusSpecific.light',
			active:true,
			inReport: true
		},
		{
			name:'statusSpecific.racon', 
			title:'RACON',
			sortField: 'statusSpecific.racon',
			active:true,
			inReport: true
		},
		{
			name:'supplyVoltageInt', 
			title:'Voltage Internal',
			sortField: 'supplyVoltageInt',
			active:true,
			inReport: true
		},
		{
			name:'supplyVoltageExt1', 
			title:'Voltage External 1',
			sortField: 'supplyVoltageExt1',
			active:true,
			inReport: true
		},
		{
			name:'supplyVoltageExt2', 
			title:'Voltage External 2',
			sortField: 'supplyVoltageExt2',
			active:true,
			inReport: true
		},
		{
			name:'lightSensor', 
			title:'Ambient',
			sortField: 'lightSensor',
			active:true,
			inReport: true
		},
		{
			name:'beat', 
			title:'Beat',
			sortField: 'beat',
			active:true,
			inReport: true
		},
		{
			name:'hatchDoor', 
			title:'Hatch Door',
			sortField: 'hatchDoor',
			active:true,
			inReport: true
		},
		{
			name:'utcDate', 
			title:'Received Time (UTC)',
			sortField: 'utcDate',
			active:true,
			inReport: true
		}
		
	],	
	SelfContainedLantern: [
		{
			name:'mmsi', 
			title:'MMSI',
			sortField: 'mmsi',
			active:true,
			inReport: true
		},

		{
			name:'utcDate', 
			title:'Received Time (UTC)',
			sortField: 'utcDate',
			active:true,
			inReport: true
		}
		
	]
	
	
}




