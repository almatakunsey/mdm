export const ddmsFields = [
	{
		name:'name', 
		title:'Name',
		sortField: 'name',
		width: '50px',
		active:true,
		inReport: true
	},
	{
		name:'speed', 
		title:'Speed',
		sortField: 'speed',
		active:true,
		inReport: true
	},	
	{
		name:'actual', 
		title:'Actual',
		sortField: 'actual',
		active:true,
		inReport: true
	},
	{
		name:'empty', 
		title:'Empty',
		sortField: 'empty',
		active:true,
		inReport: true
	},
	{
		name:'full', 
		title:'Full',
		sortField: 'full',
		active:true,
		inReport: true
	},
	{
		name:'half', 
		title:'Half',
		sortField: 'half',
		active:true,
		inReport: true
	},
	{
		name:'supplyDesc', 
		title:'Supply',
		sortField: 'supplyDesc',
		active:true,
		inReport: true
	},
	{
		name:'coverDesc', 
		title:'Cover',
		sortField: 'coverDesc',
		active:true,
		inReport: true
	},
	{
		name:'battery', 
		title:'Battery',
		sortField: 'battery',
		active:true,
		inReport: true
	},
	{
		name:'sonarDesc', 
		title:'Sonar',
		sortField: 'sonarDesc',
		active:true,
		inReport: true
	},
	{
		name:'hopperDesc', 
		title:'Hoppers',
		sortField: 'hopperDesc',
		active:true,
		inReport: true
	},
	{
		name:'hopperFlag', 
		title:'Hoppers Flag',
		sortField: 'hopperFlag',
		active:true,
		inReport: true
	},
	{
		name:'lat', 
		title:'Latitude',
		sortField: 'lat',
		active:true,
		inReport: true
	},
	{
		name:'lng', 
		title:'Longitude',
		sortField: 'lng',
		active:true,
		inReport: true
	},
	{
		name:'middleValue', 
		title:'Middle',
		sortField: 'middleValue',
		active:true,
		inReport: true
	},
	{
		name:'utcDate', 
		title:'Received Time (UTC)',
		sortField: 'utcDate',
		active:true,
		inReport: true
	},
	{
		name:'localDate', 
		title:'Received Time (Local)',
		sortField: 'localDate',
		active:true,
		inReport: true
	}
]
