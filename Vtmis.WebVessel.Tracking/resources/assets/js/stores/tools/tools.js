import {ruler} from './ruler'
import {weatherStation} from './weatherstation'
import {rightmenu} from './rightmenu'
import {legend} from './legend'

const state = {
	rightMenuPane: {
		enabled : true
	}
}

const getters = {
	toolsRightMenuPaneEnabled: state => state.rightMenuPane.enabled
}

const mutations = {
	toggleToolsRightMenuPane(state, condition){
		state.rightMenuPane.enabled = condition
	}
}

export const tools = {
	state,
	getters,
	mutations,
    namespaced: true,
    modules: {
        ruler,
        rightmenu,
        weatherStation,
        legend
    }
}
