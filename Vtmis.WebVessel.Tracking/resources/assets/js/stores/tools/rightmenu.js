const state = {
	enabled: true
}

const getters = {
	enabled : state => state.enabled
}

const mutations = {
	toggleToolsRightMenu(state, condition){
		state.enabled = condition
	}
}

export const rightmenu = {
	state,
	getters,
	mutations,
	namespaced: true
}