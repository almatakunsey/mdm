import utilHelper from '../../helpers/util'

const state = {
    enabled: false,
    points: [],
    distance: 0,
    bearing: 0
}

const getters = {
    enabled: state => state.enabled,
    points: state => state.points,
    distance: state => state.distance,
    bearing: state => state.bearing    
}

const mutations = {
    toggleRulerEnabled(state, condition){ state.enabled = condition },
    addPoint(state, latlng){    	
        state.points.push({
            id: utilHelper.genRandom(32),
            latlng,
            highlight: false
        })
    },
    removePoint(state, id){state.points.splice(_.findIndex(state.points, {id}), 1)},
    clearPoints(state){
        state.points = []
    },
    togglePointHighlight(state, {id, value}){        
        state.points[_.findIndex(state.points, {id})].highlight = value               
    },
    clearPointHighlight(state){
        if(state.points.length){
            state.points.forEach((point)=>{
                point.highlight = false
            })
        }
    },
    setDistance(state, distance){state.distance = distance},
    setBearing(state, bearing){state.bearing = bearing},
    reset(state){
        state.points = []
        state.distance = 0
        state.bearing = 0
    }
}

export const ruler = {
    state,
    getters,
    mutations,
    namespaced: true
}
