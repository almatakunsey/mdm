const state = {
	enabled: false,	
    shipTypeColorArr: [],
    viewAs: 'user'       
}

const getters = {
	enabled: state => state.enabled,	
    shipTypeColorArr: state => state.shipTypeColorArr,
    viewAs: state => state.viewAs
}

const mutations = {
	toggleLegendEnabled(state, condition){
		state.enabled = condition
	},	
    collectShipTypeColor(state, shipTypeColor){
    	state.shipTypeColorArr.push(shipTypeColor)
    },
    clearShipTypeColorArr(state){ state.shipTypeColorArr = [] },
    setViewAs(state, level){ state.viewAs = level }
}

const actions = {
	addShipTypeColor({commit, getters, rootGetters}, {type, level}){
		if( !_.find(getters.shipTypeColorArr, {type}) ){			
	    	commit('collectShipTypeColor', {
	    		type,
				color:rootGetters['vesselDetails/colors/colorByShipTypeName'](type, level)	    		
	    	})
    	} 	
	}
}

export const legend = {
	state,
	getters,
	mutations,
	actions,
	namespaced:true
}