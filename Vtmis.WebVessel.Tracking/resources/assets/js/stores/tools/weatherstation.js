const state = {
	enabled: false
}

const getters = {
	enabled: state => state.enabled
}

const mutations = {
	toggleWeatherStationEnabled(state, condition){ state.enabled = condition}
}

export const weatherStation = {
	state,
	getters,
	mutations,
	namespaced:true
}