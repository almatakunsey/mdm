import {summary} from './summary'

export const eics = {
	namespaced:true,
	modules:{
		summary
	}
}