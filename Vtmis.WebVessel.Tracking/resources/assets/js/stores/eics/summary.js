const state = {
	enabled: false,
	panelShown: true,
	vessels: new Map(),
    stateChangeTracker: 0,
    equipments: ["oil", "towing", "salvage", "firefight", "sar"],
    anchoredVesselIds: []
}

const getters = {
	eicsSummaryEnabled: state => state.enabled,
	panelShown: state => state.panelShown,
	vessels: state => state.stateChangeTracker && state.vessels,
    vesselsArr: state => state.stateChangeTracker && [...state.vessels.values()],
    equipments: state => state.equipments,
    anchoredVesselIds: state => state.anchoredVesselIds
}

const mutations = {
	toggleEicsSummary(state, condition){ state.enabled = condition },
	togglePanelShown(state, condition){ state.panelShown = condition },	
    addVessel(state, vessel){    	
        state.vessels.set(vessel.vesselId, vessel)
        state.stateChangeTracker += 1           
    },
    clearVessels(state, vessels){ state.vessels.clear(); state.stateChangeTracker += 1},
    removeEquipment(state, equipment){state.equipments.splice(state.equipments.indexOf(equipment), 1)},
    addEquipment(state, equipment){state.equipments.push(equipment)},
    clearAnchoredVesselIds(state){state.anchoredVesselIds = []},
    setAnchoredVesselIds(state, vesselIds){state.anchoredVesselIds = vesselIds}
}

export const summary = {
	state,
	getters,
	mutations,
	namespaced:true
}