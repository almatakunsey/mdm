import L from 'leaflet';
import vesselInfoHelper from '../helpers/vesselinfo'
import utilHelper from '../helpers/util'

import vesselService from '../services/vessels'


/*
 ######    ########      ###     ########   ########   
##    ##      ##        ## ##       ##      ##         
##            ##       ##   ##      ##      ##         
 ######       ##      ##     ##     ##      ######     
      ##      ##      #########     ##      ##         
##    ##      ##      ##     ##     ##      ##         
 ######       ##      ##     ##     ##      ########   
*/
const state = {
	details:{
		enabled: false
	},	
	info: {
		base: null,
		extra: null,
		lloyd: null,
		rms: null,
		imageUrl: ''
	}	
}

/*
 ######    ########   ########   ########   ########   ########    ######    
##    ##   ##            ##         ##      ##         ##     ##  ##    ##   
##         ##            ##         ##      ##         ##     ##  ##         
##   ####  ######        ##         ##      ######     ########    ######    
##    ##   ##            ##         ##      ##         ##   ##          ##   
##    ##   ##            ##         ##      ##         ##    ##   ##    ##   
 ######    ########      ##         ##      ########   ##     ##   ######    
*/
const getters = {
	baseInfo: (state) => state.info.base,
	extraInfo: (state) => state.info.extra,
	lloydInfo: (state) => state.info.lloyd,
	rmsInfo: (state) => state.info.rms,
	info: (state) => state.info,
	vesselClass: (state) => state.info.base && vesselInfoHelper.getVesselClass(state.info.base),
	targetPosLastIndex: (state) => state.info.extra && state.info.extra.targetPositions.items.length - 1,
	imageUrl: (state) => state.info.imageUrl,
	detailsEnabled: (state) => state.details.enabled,	
}

/*
##     ##  ##     ##  ########      ###     ########   ####   #######   ##    ##    ######    
###   ###  ##     ##     ##        ## ##       ##       ##   ##     ##  ###   ##   ##    ##   
#### ####  ##     ##     ##       ##   ##      ##       ##   ##     ##  ####  ##   ##         
## ### ##  ##     ##     ##      ##     ##     ##       ##   ##     ##  ## ## ##    ######    
##     ##  ##     ##     ##      #########     ##       ##   ##     ##  ##  ####         ##   
##     ##  ##     ##     ##      ##     ##     ##       ##   ##     ##  ##   ###   ##    ##   
##     ##   #######      ##      ##     ##     ##      ####   #######   ##    ##    ######    
*/
const mutations = {
	setBaseInfo(state, info){
		state.info.base = info
	},
	setExtraInfo(state, extraInfo){		
		state.info.extra = extraInfo
	},
	setLloydInfo(state, lloydInfo){
		//console.log(lloydInfo)
		state.info.lloyd = lloydInfo
	},
	setRmsInfo(state, rmsInfo){
		//console.log(lloydInfo)
		state.info.rms = rmsInfo
	},
	setImageUrl(state, url){
		state.info.imageUrl = `http://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}${url.result}`
	},
	resetInfo(state){
		state.info.base = null
		state.info.extra = null
		state.info.lloyd = null
	},
	toggleVesselDetails(state, condition){
		state.details.enabled = condition
	}
}

/*
   ###      ######    ########   ####   #######   ##    ##    ######    
  ## ##    ##    ##      ##       ##   ##     ##  ###   ##   ##    ##   
 ##   ##   ##            ##       ##   ##     ##  ####  ##   ##         
##     ##  ##            ##       ##   ##     ##  ## ## ##    ######    
#########  ##            ##       ##   ##     ##  ##  ####         ##   
##     ##  ##    ##      ##       ##   ##     ##  ##   ###   ##    ##   
##     ##   ######       ##      ####   #######   ##    ##    ######    
*/
const actions = {
	updateExtraInfo({commit, rootState}, {id}){		

		return new Promise((resolve, reject)=>{
			vesselService.getInfoByVesselId(id)
			.then((data)=>{
				console.log("%cExtra Info:", conStyle.boldWhite, data)
				setTimeout(()=>{
					commit('setExtraInfo', data)					
					resolve(data)
				}, 500)
			})
			.catch((error)=>{
				reject(error)
			})
		})	
	},

	updateLloydInfo({commit}, {imo,callSign}){		
		return new Promise((resolve, reject)=>{
			// console.log(callSign)
			vesselService.getInfoByImoCallSign(imo,callSign)
			.then((data)=>{
				console.log(data)
				setTimeout(()=>{
					commit('setLloydInfo', data.result)					
					resolve(data)
				}, 500)
			})
			.catch((error)=>{
				reject(error)
			})
		})	
	},

	updateRmsInfo({commit}, {id}){		
		return new Promise((resolve, reject)=>{
			// console.log(callSign)
			vesselService.getRmsInfoByVesselId(id)
			.then((data)=>{
				//console.log(data)
				setTimeout(()=>{
					commit('setRmsInfo', data.result)					
					resolve(data)
				}, 500)
			})
			.catch((error)=>{
				reject(error)
			})
		})	
	},

	updateImageUrl({commit}, {id}){

		vesselService.getImageUrl(id)
		.then((url)=>{
			commit('setImageUrl', url)
		})
		
	}

}


export const vessel = {	
	state,
	getters,
	mutations,
	actions,
	namespaced: true
}