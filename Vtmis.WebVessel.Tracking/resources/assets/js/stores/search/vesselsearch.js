import Fuse from 'fuse.js'
import utilHelper from '../../helpers/util'

const port = utilHelper.getMapPortalApiPort()
const host = `http://${window.location.hostname}${ port ? ':'+port : '' }`

// service
import vesselService from '../../services/vessels'

// data
import searchOptions from './options'
import { fields } from './vesselSearchListField'

const formDataOrigin = {
    searchTerm: ''
}

const state = {	
	formData: _.cloneDeep(formDataOrigin),
	keyword: '',
	suggestions: [],
	fields: fields,	
	advancedSearch: {
		enabled: false,
		searchOptions,		
		query: {
			targetName: '',
			targetMmsi: '',
			targetImo: '',
			targetCallSign: '',
			area: 'all',
			timeSpan: '1',
			refreshRate: ''
		},
		result: {
			enabled: false,
			data: []
		},
		error: null
	}	
}

const getters = {
	formData: state => state.formData,
	field: state => state.fields,
	keyword: state => state.keyword,
	suggestions: state => state.suggestions,
	advancedSearchEnabled: state => state.advancedSearch.enabled,
	advancedSearchResultEnabled: state => state.advancedSearch.result.enabled,
	advancedSearchQuery: state => state.advancedSearch.query,	
	advancedSearchResult: state => state.advancedSearch.result.data,
	advancedSearchError: state => state.advancedSearch.error,
	searchOptions: state => state.advancedSearch.searchOptions
}

const mutations = {
	setFormData(state, {key, value}){state.formData[key] = value},
	clearFormData(state){state.formData = _.cloneDeep(formDataOrigin)},
	setField(state, field){state.fields = field}, 
	setKeyword(state, keyword){state.keyword = keyword},
	resetKeyword(state){state.keyword = ''},
	setSuggestions(state, data){state.suggestions = data.slice(0, 5)},	
	resetSuggestions(state){state.suggestions = []},
	toggleAdvancedSearch(state, condition){ state.advancedSearch.enabled = condition },
	toggleAdvancedSearchResult(state, condition){ state.advancedSearch.result.enabled = condition },
	setAdvancedSearchQuery(state, {key, value}){		
		state.advancedSearch.query[key]=value		
	},
	clearAdvancedSearchQuery(state){
		state.advancedSearch.query = {
			targetName: '',
			targetMmsi: '',
			targetImo: '',
			targetCallSign: '',
			area: 'all',
			timeSpan: '1',
			refreshRate: ''
		}	
	},
	setAdvancedSearchResult : (state, values) => {state.advancedSearch.result.data = values},
	setAdvancedSearchError: (state, error) => {state.advancedSearch.error = error},
	clearAdvancedSearchError: (state) => {state.advancedSearch.error = null}
}

const actions = {
	async getSuggestions({commit, getters}, {vessels, keys}){		
		
		var options = {
			shouldSort: true,
			threshold: 0.6,
			location: 0,
			distance: 100,
			maxPatternLength: 32,
			minMatchCharLength: 1,
			keys
		};

		const fuse = new Fuse(vessels, options); // "list" is the item array		
		
		return fuse.search(getters.keyword)		
		
	},
	advancedSearch({commit, getters}){
		
		function formatData(){			
			
			const data = _.clone(getters.advancedSearchQuery)
			
			_.each(Object.keys(data), (key)=>{if(!data[key]){delete data[key]}})
			delete data.area
			delete data.refreshRate			
			return data			
		}

		return new Promise((resolve, reject)=>{				

			vesselService.advancedSearch(formatData()).then(
				(result)=>{										
					resolve(result)
				},
				(error)=>{
					reject(error)
				}
			)			
		})
		
	}
}

export const vesselsearch = {	
	state,	
	getters, 
	actions,
	mutations,
	namespaced: true
}