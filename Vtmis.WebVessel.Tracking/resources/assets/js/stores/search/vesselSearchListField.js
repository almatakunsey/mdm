export const fields = [		
	{
	    name: '__slot:actions',
	    title: 'Actions',
	    active:true,
	    titleClass: 'center-aligned',
	    inReport: false
	},
	{
		name:'vesselName', 
		title:'Name',
		sortField: 'vesselName',
		active:true,
		inReport: true
	},
	{
		name:'mmsi', 
		title:'MMSI',
		sortField: 'mmsi',
		active:true,
		inReport: true
	},
	{
		name:'imo', 
		title:'IMO',
		sortField: 'imo',
		active:true,
		inReport: true
	},
	{
		name:'callSign', 
		title:'Call Sign',
		sortField: 'callSign',
		active:true,
		inReport: true
	},
	{
		name:'shipType_ToString', 
		title:'Type',
		sortField: 'shipType_ToString',
		active:true,
		inReport: true
	},
	{
		name:'lastSeen', 
		title:'Last Seen',
		sortField: 'lastSeen',
		active:true,
		inReport: true
	},
	{
		name:'navStatus', 
		title:'Nav. Status',
		sortField: 'navStatus',
		active:true,
		inReport: true
	},
	{
		name:'coord.lat', 
		title:'Latitude',
		sortField: 'coord.lat',
		active:true,
		inReport: true
	},
	{
		name:'coord.lgt', 
		title:'Longitude',
		sortField: 'coord.lgt',
		active:true,
		inReport: true
	},
	{
		name:'sog', 
		title:'SOG',
		sortField: 'sog',
		active:true,
		inReport: true
	},
	{
		name:'cog', 
		title:'COG',
		sortField: 'cog',
		active:true,
		inReport: true
	},
	{
		name:'destination', 
		title:'Destination',
		sortField: 'destination',
		active:true,
		inReport: true
	},
	{
		name:'eta', 
		title:'ETA',
		sortField: 'eta',
		active:true,
		inReport: true
	}	
]