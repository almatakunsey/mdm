export default {
	areaOptions : [
    	{text: 'all', id: 'all'},
    	{text: 'screen', id: 'screen'}
    ],

    timeSpanOptions : [
    	{text: '24 Hours', id: 1},
    	{text: '48 Hours', id: 2},
    	{text: '10 Days', id: 3},
    	{text: '30 Days', id: 4},
    	{text: '90 Days', id: 5},
    	{text: '365 Days', id: 6}
    ]
}