import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

import reportService from '../../services/reports'
import filterService from '../../services/filters'
import zoneService from '../../services/zones'

const reportItemDtosPristine = {
    id: utilHelper.genRandom(32),
    filterId:0,
    zoneId:0,
    fee: 0,
    minSpeed: 0,
    maxSpeed: 0
}

const formDataPristine = {
        name: '',
        timeSpan: 1,
        timeSpanType: 0,
        isEnabled:true,
        reportTypeId: 0,
        reportTypeNumber: "",
        reportItems: [_.cloneDeep(reportItemDtosPristine)]
}

const state = {
    reportData : _.cloneDeep(formDataPristine),
    timeSpanOptions: [
        {text: 'Minutes', id: 0},
        {text: 'Hours', id: 1},
        {text: 'Days', id: 2},
        {text: 'Months', id: 3}  
    ],
    filtersOptions: [],
    zonesOptions: [],
    reportTypeOptions: []
}

const getters = {
    reportData: state => state.reportData,
    timeSpanOptions: state => state.timeSpanOptions,
    filtersOptions: state => state.filtersOptions,
    zonesOptions: state => state.zonesOptions,
    reportTypeOptions: state => state.reportTypeOptions
    
}

const mutations = {
    setFilters(state, filters){        
       state.filtersOptions.push( {text:'Select Filter', id:0 } )
        _.each(filters, ({ id, name }) => { 
            state.filtersOptions.push({ text: name, id })
        })
    },
    clearFilters(state)
    {
        state.filters = []
        state.filtersOptions = []
    },
    setZones(state, zones){    
        state.zonesOptions.push( {text:'Select Zone', id:0 } )    
        _.each(zones, ({ id, name }) => { 
            state.zonesOptions.push({ text: name, id })
        })
    },
    clearZones(state){
        state.zones = []
        state.zonesOptions = []
    },
    setReportsType(state, reports){   
        _.each(reports, ({ id, name, reportTypeNumber }) => { 
            state.reportTypeOptions.push({ text: name, id, typeNumber: reportTypeNumber })
        })
    },
    clearReportsType(state){        
        state.reportTypeOptions = []
    },
    clearData(state) {state.reportData = _.cloneDeep(formDataPristine)},
    setReportData(state, {key, value}){
        state.reportData[key] = value        
    },
    addReportConditionItem(state, {filterId = 0, zoneId = 0 , fee = 0, minSpeed = 0, maxSpeed = 0}){
        // state.reportData.reportItems.push({ filterId: state.filtersOptions[0].id , zoneId: state.zonesOptions[0].id, fee: 0, minSpeed: 0, maxSpeed: 0})
        let newDtoItem = _.cloneDeep(reportItemDtosPristine)
        newDtoItem.id = utilHelper.genRandom(32)
        newDtoItem.filterId = filterId
        newDtoItem.zoneId = zoneId
        newDtoItem.fee = fee
        newDtoItem.minSpeed = minSpeed
        newDtoItem.maxSpeed = maxSpeed
        state.reportData.reportItems.push(newDtoItem)
        state.reportData.reportTypeId = state.reportTypeOptions[0].id
    },
    removeReportConditionItem(state, index){   
        state.reportData.reportItems.splice(index, 1)
        if(index == 0) {
            state.reportData.reportItems[0].condition = '-1'
        }  
    },
    updateReportConditionItem(state, {type, value, id}){ 
        const dataIndex = _.findIndex(state.reportData.reportItems, {id})
        state.reportData.reportItems[dataIndex][type] = value

    },
    replaceReportData(state, reportData){
        state.reportData = reportData
    }

}

const actions = {

    getFilterOptions({commit}){
        commit('clearFilters')
        return new Promise((resolve, reject)=>{
            filterService.GetFilterByUserId(localStorage.userId)
            .then((result)=>{                
                commit('setFilters', result.items)
                resolve(result)
            })
            .catch((response)=>{                            
                reject(response)                
            })                                          
        })
    },
    getZoneOptions({commit}){
        commit('clearZones')        
        return new Promise((resolve, reject)=>{ 
            zoneService.GetZonesByUserId(localStorage.userId)           
             .then((result)=>{
                commit('setZones', result)                
                resolve(result)
            })            
            .catch((error)=>{
                reject(error)
            })
           
        })
    },
    getReportsTypeOptions({commit}){
        commit('clearReportsType')
        return new Promise((resolve, reject)=>{
            reportService.GetReportsTypeByUserId()
            .then((result)=>{
                commit('setReportsType', result)
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
        })
    }
}

export const form = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}