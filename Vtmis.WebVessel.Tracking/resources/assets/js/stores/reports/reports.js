import {summary} from './summary'
import {create} from './create'
import {edit} from './edit'
import {form} from './form'
import {result} from './result'

import reportService from '../../services/reports'

const state = {	
	mainPane: {
		enabled: false
	},
    reports: [],
    selectedReport: null,
}

const getters = {	
	mainPaneEnabled : state => state.mainPane.enabled,
    reports : state => state.reports,
    selectedReport : state => state.selectedReport,
}

const mutations = {	
	toggleReportMainPane(state, condition){ state.mainPane.enabled = condition },
    setReports(state, reports){state.reports = reports},
    setSelectedReport(state, reports){ state.selectedReport = reports }
}

const actions = {
    getReports : ({commit}) => {

        return new Promise((resolve, reject)=>{
            reportService.getReports()
            .then( ({data:{result}}) =>{                
                commit('setReports', result.items)
                // console.log(result.items)
                resolve(result)
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })                              
    },

    deleteReport({commit, getters}, id){
        
        return new Promise((resolve, reject)=>{
            reportService.deleteReport(id).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{                    
                    reject(error)
                }
            )
        })        
    }
}

export const reports = {	
	state,
	getters,
	mutations,
    actions,
	namespaced: true,
	modules:{
		summary,
		create,
        edit,
        form,
        result
	}
}

