import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

import reportService from '../../services/reports'

const state = {
    enabled: false
}

const getters = {
    enabled: state => state.enabled
}

const mutations = {
    toggleReportCreate(state, condition){state.enabled = condition},
}

const actions = {

    saveReport({commit, getters, rootGetters}) {

        return new Promise((resolve, reject)=>{
            reportService.saveReport(rootGetters['reports/form/reportData'])
            .then((result)=>{
                resolve("Data successfully saved!")
            })
            .catch((error)=>{
                reject(error)
            })
            
        })
    }
}

export const create = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}