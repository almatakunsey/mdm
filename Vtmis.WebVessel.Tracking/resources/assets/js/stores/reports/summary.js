const state = {
    enabled: false
}

const getters = {
    enabled: state => state.enabled
}

const mutations = {
    toggleReportSummary(state, condition){ state.enabled = condition }
}

export const summary = {
    state,
    getters,    
    mutations,
    namespaced: true
}