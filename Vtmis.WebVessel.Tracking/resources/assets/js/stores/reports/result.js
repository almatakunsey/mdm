import utilHelper from '../../helpers/util'
import reportService from '../../services/reports'
import moment from 'moment'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const state = {
    enabled: false,
    resultData : {
        timeSpan: 0,
        startDate: moment().subtract(24, 'hours').format('YYYY-MM-DDTHH:mm'),
        endDate: moment().format('YYYY-MM-DDTHH:mm')
    },
    timeSpanOptions: [
        {text: 'User Defined', id: 0},
        {text: 'Last 24 Hours', id: 1},
        {text: 'Last 48 Hours', id: 2},
        {text: 'Last 10 Days', id: 3},
        {text: 'Last 30 Days', id: 4},
        {text: 'Last 90 Days', id: 5},
        {text: 'Last 365 Days', id: 6},
        {text: 'Today', id: 7},
        {text: 'Yesterday', id: 8},
        {text: 'This Week', id: 9},
        {text: 'Last Week', id: 10},
        {text: 'This Month', id: 11},
        {text: 'Last Month', id: 12},
        {text: 'This Quarter', id: 13},
        {text: 'Last Quater', id: 14},
        {text: 'This Year', id: 15},
        {text: 'Last Year', id: 16},
        {text: 'All', id: 17},
    ],
    columns: [
            {data: 'arrivalTime'},
            {data: 'departureTime'},
            {data: 'name'},
            {data: 'mmsi'},
            {data: 'callSign'},
            {data: 'callType'},
            {data: 'length'},
            {data: 'destination'},
            {data: 'eta'}

        ],
    reports: [],
    selectedReport: null,
    results: {},
    tableData: [],
    speedVioData: [],
    enterExitData:[]
} // end state

const getters = {
    enabled: state => state.enabled,
    resultData: state => state.resultData,
    timeSpanOptions: state => state.timeSpanOptions,
    columns: state => state.columns,
    selectedReport : state => state.selectedReport,
    reports : state => state.reports,
    results: state => state.results,
    tableData: state => state.tableData,
    speedVioData: state => state.speedVioData,
    enterExitData: state => state.enterExitData
}

const mutations = {
    toggleReportResult(state, condition){state.enabled = condition},
    setResultData(state, {key, value}){
        state.resultData[key] = value        
    },   
    setSelectedReport(state, reports){ state.selectedReport = reports },
    setResults(state, reportInfo) {
        state.results = reportInfo.data.result.data
        state.tableData = []
      
        _.each(reportInfo.data.result.data, ({  arrivalTime, departureTime, name, mmsi, callSign, callType, length, destination, eta }) => { 
            state.tableData.push(
                { 
                    name:name, 
                    arrivalTime:arrivalTime, 
                    departureTime:departureTime, 
                    callSign:callSign,
                    callType:callType,
                    destination:destination,
                    eta:eta,
                    length:length,
                    mmsi:mmsi
                })
        })
    },
    clearResults(state){
        state.tableData = []
    },
    setSpeedVioData(state, speedVioData){ state.speedVioData = speedVioData },
    setEnterExitData(state, enterExitData){ state.enterExitData = enterExitData }
}
const actions = {

    generateReport({commit, getters, rootGetters}){        

        const reportId = rootGetters['reports/selectedReport'].id
        const timeSpan = getters.resultData.timeSpan
        const startDate = getters.resultData.startDate
        const endDate = getters.resultData.endDate

        return new Promise((resolve, reject)=>{                                    
            reportService.generateReport(reportId,timeSpan,startDate,endDate).then(
                (data)=>{
                commit('setResults', data)                    
                    resolve(data)
                    console.log(data)
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    },

    generateSpeedViolationReport({commit, getters, rootGetters}){        

        const reportId = rootGetters['reports/selectedReport'].id
        const timeSpan = getters.resultData.timeSpan
        const startDate = getters.resultData.startDate
        const endDate = getters.resultData.endDate

        return new Promise((resolve, reject)=>{                                    
            reportService.generateReport(reportId,timeSpan,startDate,endDate).then(
                (({data:{result}})=>{
                commit('setSpeedVioData', result)                    
                    resolve(result)
                    // console.log(result.max)
                }),
                (error)=>{
                    reject(error)
                }
            )
        })
    },
    generateEnterExitReport({commit, getters, rootGetters}){        

        const reportId = rootGetters['reports/selectedReport'].id
        // const timeSpan = getters.resultData.timeSpan
        const enterTime = getters.resultData.startDate
        const exitTime = getters.resultData.endDate

        return new Promise((resolve, reject)=>{                                    
            reportService.generateReport1(reportId,enterTime,exitTime).then(
                ((result)=>{
                commit('setEnterExitData', result)                    
                    resolve(result)
                    // console.log(result)
                }),
                (error)=>{
                    reject(error)
                }
            )
        })
    },
    getReportsById : ({commit, getters, rootGetters}) => {
        const reportId = rootGetters['reports/selectedReport'].id
        return new Promise((resolve, reject)=>{
            reportService.getReportsById(reportId)
            .then( ({data:{result}}) =>{                
                commit('setSelectedReport', result)
                // console.log(result)
                resolve(result)
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })                              
    }
}

export const result = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}