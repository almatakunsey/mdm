import utilHelper from '../../helpers/util'
import reportService from '../../services/reports'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const state = {
    editedReportId: null,
    enabled: false    
} // end state

const mutations = {
    setEditedReportId(state, id){
        state.editedReportId = id
    },
    toggleReportEdit(state, condition){state.enabled = condition}    
}

const getters = {
    enabled: state => state.enabled,    
    editedReportId: state => state.editedReportId 
}

const actions = {

    loadReport({commit, getters}){
        return new Promise((resolve, reject)=>{
            reportService.getReportsById(getters.editedReportId).then(
                ({data:{result}})=>{

                    // format the result to fit in the formData\
                    const formattedData = {
                        id: getters.editedReportId,
                        isEnabled: result.isEnabled,
                        name:result.name,
                        reportTypeId: result.reportTypeId,
                        timeSpan: result.timeSpan,
                        timeSpanType: result.timeSpanType,
                        reportItems: result.reportItems
                    }
                
                commit('reports/form/replaceReportData', formattedData, {root:true})
                // console.log(formattedData.isEnabled)
                resolve(formattedData) 
            })
            .catch((error)=>{
                reject(error)
            })  
        })
    },

    updateReport({commit, getters, rootGetters}){        

        return new Promise((resolve, reject)=>{                                    
            reportService.updateReport(
                rootGetters['reports/form/reportData'], 
                getters.editedReportId
            ).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    }
}

export const edit = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}