import { createImage }  from './createImage'
import vesselService from '../../../services/vessels'

const imgData = {
	name: '',
	path: '',
	original: '',
	thumbnail: ''

}

const imagePristine = {
	id: '',
	type: 'Target',
	imgDatas:[]
}

const state = {
	// images of vessel(s) separated by vessel id/mmsi
	images : []
}

const getters = {
	// image(s) of a single vessel id/mmsi
	// the image is in properties imgDatas	
	image: (state) => (id) => state.images[_.findIndex(state.images, {id})]	
}

const mutations = {
	addNewImage(state, id){

		if(state.images[_.findIndex(state.images, {id})]){	return false }		
		
		const newImg = _.cloneDeep(imagePristine)		
		newImg.id = id
		
		state.images.push(newImg)

	},

	addNewImageData(state, {id, data}){
		
		if(_.filter(state.images, { imgDatas: [ { name: data.name } ]}).length){return false}
		
		const newImgData = _.cloneDeep(imgData)
		
		newImgData.name = data.name
		newImgData.path = data.path
		newImgData.original = data.original
		newImgData.thumbnail = data.thumbnail

		state.images[_.findIndex(state.images, {id})].imgDatas.push(newImgData)
	},

	removeImgData(state, { vesselId, name, path }) {
		
		const vesselImgDatas = state.images.find(x => x.id == vesselId).imgDatas

		_.remove(vesselImgDatas, {name, path})		
	}



}

const actions = {
	async loadImages({commit, getters}, id){
		
		try{
			
			const {result} = await vesselService.loadImages(id)
			
			if(result && result.length){
				result.forEach((re)=>{
					commit('addNewImageData', {id,data:re})
				})
			}

			return true

		}
		catch(error){
			throw(error)	
		}
	},

	async deleteImage({commit}, {vesselId, name, path, type}){
		try{
			
			const {result} = await vesselService.deleteImage(vesselId, name, path, type)
					
			commit('removeImgData', {vesselId, name, path})

			return true

		}
		catch(error){
			throw(error)	
		}
	}


}

export const images = {
	state,
	getters,
	mutations,
	actions,
	namespaced:true,
	modules : {
		createImage
	}
}