import vesselService from '../../../services/vessels'

const formDataPristine = {
	id:'',
	imgSrc: '',
	cropImg: {
		src: '',
		option: ''
	},
	crop: false	
}

const state = {
	formDatas: []
}

const getters = {	
	formData: (state) => (id) => state.formDatas[_.findIndex(state.formDatas, {id})]	
}

const mutations = {
	// id is the id obtained from vessel.info
	addNewImageFormData(state, id){
		const newFormData = _.cloneDeep(formDataPristine)
		newFormData.id = id;			
		state.formDatas.push(newFormData)		
	},

	setImgSrc(state, {id, imgSrc}){
		state.formDatas[_.findIndex(state.formDatas, {id})].imgSrc = imgSrc
	},

	setCropImg(state, {id, cropImg}){
		state.formDatas[_.findIndex(state.formDatas, {id})].cropImg = cropImg
	},

	removeFormData(state, id){
		state.formDatas.splice( _.findIndex(state.formDatas, {id}), 1 )
	},

	toggleCrop(state, {id, condition}){
		state.formDatas[_.findIndex(state.formDatas, {id})].crop = condition
	},

	resetImageFormData(state, id){
		_.remove(state.formDatas, {id})
		const newFormData = _.cloneDeep(formDataPristine)
		newFormData.id = id;
		state.formDatas.push(newFormData)
	}
}


const beforeSendFormat = (formData, target)=>{

	const formDataCopy = _.cloneDeep(formData)

	const imgSrc = formDataCopy.crop ? formDataCopy.cropImg.src : formDataCopy.imgSrc

	if(target == 'Wrem'){formDataCopy.id = formDataCopy.id.replace('w', '')}

	return {
		imageId:formDataCopy.id,
		imageType:target,
		images:[imgSrc]
	}
}

const actions = {
	async uploadImage({commit, getters}, {id, target}){
						
		try{						
			// result is array of object
			const {result} = await vesselService.uploadImage( beforeSendFormat(getters.formData(id), target) )
			return result			
		}
		catch(error){
			throw(error)
		}
	}
}

export const createImage = {
	state,
	getters,
	mutations,
	actions,
	namespaced:true
}
