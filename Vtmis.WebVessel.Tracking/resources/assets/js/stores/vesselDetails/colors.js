import vesselInfoHelper from '../../helpers/vesselinfo'

const colorsOrigin = {
    default: {
		"Anti-Pollution":"#808080",
		"Cargo":"#8b4513",
		"Cargo - Hazard A (Major)":"#006400",
		"Cargo - Hazard B":"#808000",
		"Cargo - Hazard C (Minor)":"#483d8b",
		"Cargo - Hazard D (Recognizable)":"#3cb371",
		"Dive Vessel":"#008b8b",
		"Dredger":"#00008b",
		"Fishing":"#32cd32",
		"High-Speed Craft":"#800080",
		"Law Enforce":"#b03060",
		"Local Vessel":"#ff0000",
		"Medical Trans":"#ff8c00",
		"Military Ops":"#ffd700",
		"Other":"#ff4500",
		"Passenger":"#00fa9a",
		"Pilot Vessel":"#8a2be2",
		"Pleasure Craft":"#dc143c",
		"Port Tender":"#00ffff",
		"Reserved":"#00bfff",
		"Sailing Vessel":"#f4a460",
		"SAR":"#0000ff",
		"Special Craft":"#f08080",
		"Tanker":"#ff00ff",
		"Tanker - Hazard A (Major)":"#1e90ff",
		"Tanker - Hazard B":"#add8e6",
		"Tanker - Hazard C (Minor)":"#ff1493",
		"Tanker - Hazard D (Recognizable)":"#7b68ee",
		"Tug":"#ee82ee",
		"Wing In Ground":"#ffdab9",
		// special
		"Unknown":"#808080", //gray
		"Sar":"#adff2f",
		"Aton":"#daa520",
		"VAton":"#66ccff",
		"Sart":"#556b2f",
		"MetHydro":"#00ced1",
		"BaseStation":"#f0e68c"

	},
	host:{},
	tenant:{},
	user:{}
}

const state = {
	colors:  _.cloneDeep(colorsOrigin),
}

const resolveShipTypeName = (vessel)=>{

	// if shipType_ToString == null
	if(!vessel.shipType_ToString){
		// get vesselCLass using messageId
		const className = vesselInfoHelper.getVesselClass(vessel)		
		if(className == 'Other'){
			return 'Unknown'
		}

		return className
	}

	return vessel.shipType_ToString
}

const resolveColorByShipTypeName = (state, shipTypeName, level = '')=>{		

	const levels = ['user', 'tenant', 'host', 'default']
	const levelsLength = levels.length

	let levelStart = 0

	if(level){levelStart = levels.indexOf(level)}

	let color = ''

	for(let i=levelStart; i < levelsLength; i++){		
		if( _.has(state.colors[levels[i]], shipTypeName) && state.colors[levels[i]][shipTypeName]){			
			color = state.colors[levels[i]][shipTypeName]
			break;
		}
	}	

	return color ? color : 'black'	
	
}

const getters = {
	colors: state => state.colors,
	color: state => (vessel, level='') => resolveColorByShipTypeName(state, resolveShipTypeName(vessel), level),		
	colorByShipTypeName: state => (shipTypeName, level='') => resolveColorByShipTypeName(state, shipTypeName, level)
}

const mutations = {
	setColors(state, {role, colors}){state.colors[role]=colors},	
	setupColors(state, {role, colors}){		
		_.forOwn(colors, (color, shipTypeName)=>{
			if(color){
				state.colors[role][shipTypeName] = color
			}
		})
	},
	// KIV will refactor to this instead of set[role]Color
	setColor(state, {role, color, shipTypeName}){
		state.colors[role][shipTypeName] = color
	},
	resetColors(state){ 		
		state.colors = _.cloneDeep(colorsOrigin) 
	},			
	resetRoleColors(state, role){			

		state.colors.user = _.cloneDeep(colorsOrigin.user)	
		
		if(role !== 'user'){
			console.log("%c Called ", conStyle.boldRed)
			state.colors[role] = _.cloneDeep(colorsOrigin[role]) 
		}
		
	},
	copyUserColorTo(state, role){state.colors[role] = state.colors.user}
}

export const colors = {
	state,
	getters,
	mutations,
	namespaced: true
}