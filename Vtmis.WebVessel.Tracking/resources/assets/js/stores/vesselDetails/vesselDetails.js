import L from 'leaflet';
import vesselInfoHelper from '../../helpers/vesselinfo'
import utilHelper from '../../helpers/util'

import vesselService from '../../services/vessels'

import { images } from './images/images'
import { colors } from './colors'


// props

const vesselInfoPristine = {
	id: 0,
	base: null,		
	lloyd: null,
	rms: null,
	ddms: null,
	timeline: null,
	lightHouse: null,
	imageUrl: '',
	minimized: false
}


/*
 ######    ########      ###     ########   ########   
##    ##      ##        ## ##       ##      ##         
##            ##       ##   ##      ##      ##         
 ######       ##      ##     ##     ##      ######     
      ##      ##      #########     ##      ##         
##    ##      ##      ##     ##     ##      ##         
 ######       ##      ##     ##     ##      ########   
*/
// store main functions and props
const state = {
	details:{
		enabled: false
	},	
	infos: [] 	
}

/*
 ######    ########   ########   ########   ########   ########    ######    
##    ##   ##            ##         ##      ##         ##     ##  ##    ##   
##         ##            ##         ##      ##         ##     ##  ##         
##   ####  ######        ##         ##      ######     ########    ######    
##    ##   ##            ##         ##      ##         ##   ##          ##   
##    ##   ##            ##         ##      ##         ##    ##   ##    ##   
 ######    ########      ##         ##      ########   ##     ##   ######    
*/
const getters = {
	infos: state => state.infos,
	detailsEnabled: (state) => state.details.enabled	
}

/*
##     ##  ##     ##  ########      ###     ########   ####   #######   ##    ##    ######    
###   ###  ##     ##     ##        ## ##       ##       ##   ##     ##  ###   ##   ##    ##   
#### ####  ##     ##     ##       ##   ##      ##       ##   ##     ##  ####  ##   ##         
## ### ##  ##     ##     ##      ##     ##     ##       ##   ##     ##  ## ## ##    ######    
##     ##  ##     ##     ##      #########     ##       ##   ##     ##  ##  ####         ##   
##     ##  ##     ##     ##      ##     ##     ##       ##   ##     ##  ##   ###   ##    ##   
##     ##   #######      ##      ##     ##     ##      ####   #######   ##    ##    ######    
*/
const mutations = {
	setInfos(state, infos){
		state.infos = infos
	},
	addNewVesselInfo(state, vesselInfo){
			
		// if(_.find(state.infos, {id:vesselInfo.id})){
		// 	_.remove(state.infos, n => n.id == vesselInfo.id)			
		// }
		// else{
		// 	state.infos.push(vesselInfo)	
		// }

		state.infos.push(vesselInfo)
		
		// maximized infos + minimized infos
		state.infos = _.filter(state.infos, i => !i.minimized).concat(_.filter(state.infos, i => i.minimized))
	},
	updateLloydInfo(state, {id, lloydInfo}){	
		state.infos[_.findIndex(state.infos, {id})].lloyd = lloydInfo
	},
	updateRmsInfo(state, {id, rmsInfo}){
		state.infos[_.findIndex(state.infos, {id})].rms = rmsInfo
	},
	updateDdmsInfo(state, {id, ddmsInfo}){
		state.infos[_.findIndex(state.infos, {id})].ddms = ddmsInfo
	},
	updateTimelineInfo(state, {id, timelineInfo}){
		state.infos[_.findIndex(state.infos, {id})].timeline = timelineInfo
	},
	updateLightHouseInfo(state, {id, lightHouseInfo}){
		state.infos[_.findIndex(state.infos, {id})].lightHouse = lightHouseInfo
	},
	setImageUrl(state, {id, url}){
		const imageUrl = `http://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}${url.result}`
		state.infos[_.findIndex(state.infos, {id})].imageUrl = imageUrl
	},
	resetInfos(state){		
		state.infos = []
	},
	removeVesselInfo(state, id){
		state.infos.splice( _.findIndex(state.infos, {id}), 1 )	

		// maximized infos + minimized infos
		state.infos = _.filter(state.infos, i => !i.minimized).concat(_.filter(state.infos, i => i.minimized))	
	},
	toggleVesselDetails(state, condition){state.details.enabled = condition},
	toggleMinimize(state, id){

		// copy the infos to avoid jittery changes
		const copyInfos = _.cloneDeep(state.infos)
		const itemIndex = _.findIndex(copyInfos, {id})

		copyInfos[itemIndex].minimized = !copyInfos[itemIndex].minimized
		
		// maximized infos + minimized infos
		state.infos = _.filter(copyInfos, i => !i.minimized).concat(_.filter(copyInfos, i => i.minimized))

	}
}

/*
   ###      ######    ########   ####   #######   ##    ##    ######    
  ## ##    ##    ##      ##       ##   ##     ##  ###   ##   ##    ##   
 ##   ##   ##            ##       ##   ##     ##  ####  ##   ##         
##     ##  ##            ##       ##   ##     ##  ## ## ##    ######    
#########  ##            ##       ##   ##     ##  ##  ####         ##   
##     ##  ##    ##      ##       ##   ##     ##  ##   ###   ##    ##   
##     ##   ######       ##      ####   #######   ##    ##    ######    
*/
const actions = {

	addNewSelectedVesselInfo({commit, getters, rootGetters}, {vesselId, ctrlKey}){		

		// // prevent adding same vessel twice if in multiple mode
		if( _.findIndex(getters.infos, {id:vesselId}) != -1 && ctrlKey ){return false}

		// limit number of vessel info panel		
		// if( rootGetters['app/modules/activeModule'] == 'analytic' && 
		// 	getters.infos.length  >= 5
		// ){
		if(getters.infos.length  >= 5){
			return false;
		}
	
		const baseInfo = rootGetters['map/vessel'](vesselId)			

		let newVesselInfo = _.cloneDeep(vesselInfoPristine)
		
		newVesselInfo.id = vesselId
		newVesselInfo.base = baseInfo

		// updated: both monitoring and analytic will replace 
		// the details if ctrl key is not clicked
		// if(
		// 	rootGetters['app/modules/activeModule'] == 'monitoring'
		// 	&& !ctrlKey
		// ){ 
		if(!ctrlKey){ 
			commit('resetInfos') 
		}
				
		commit('addNewVesselInfo', newVesselInfo)		

		// lloyd handler
		const shouldLoadLloyd = () => {						
			return (baseInfo.imo && baseInfo.imo != -1) && baseInfo.callSign ? true : false			
		}

		// rms handler
		const shouldLoadRms = () => {
			return baseInfo.messageId == 21 && baseInfo.isSart == false && baseInfo.isMethydro == false
		}

		// ddms handler
		const shouldLoadDdms = () => {
			return baseInfo.messageId != 21
		}

		// timeline handler
		const shouldLoadTimeline = () => {
			return baseInfo.messageId == 1 || baseInfo.messageId == 2 || baseInfo.messageId == 3 || baseInfo.messageId == 18 || baseInfo.messageId == 19 || baseInfo.messageId == 24
		}

		// lightHouse handler
		const shouldLoadLightHouse = () => {
			return (baseInfo.hasOwnProperty('msg21') && baseInfo.msg21.isLighthouse ? true : false)
		}

		// image handler
		vesselService.getImageUrl(vesselId)
		.then((url)=>{
			commit('setImageUrl', {id:vesselId, url})
		})

		if(shouldLoadLloyd()){				
			vesselService.getInfoByImoCallSign(baseInfo.imo, baseInfo.callSign)
			.then( ({result}) =>{				

				commit('updateLloydInfo', {
					id:vesselId,
					lloydInfo:result
				})
			})
			.catch((error)=>{
				// console.error(error)
			})
		}

		if(shouldLoadRms()){
			vesselService.getRmsInfoByVesselId(baseInfo.vesselId)
			.then( ({result}) =>{				

				commit('updateRmsInfo', {
					id:vesselId,
					rmsInfo:result
				})
				// console.log(result)
			})
			.catch((error)=>{
				// console.error(error)
			})
		}

		if(shouldLoadDdms()){
			vesselService.getDdmsInfoByVesselId(baseInfo.vesselId)
			.then( ({result}) =>{				

				commit('updateDdmsInfo', {
					id:vesselId,
					ddmsInfo:result
				})
				console.log(result)
			})
			.catch((error)=>{
				// console.error(error)
			})
		}

		if(shouldLoadTimeline()){
			vesselService.getTimelineByVesselId()
			.then( ({result}) =>{				
				console.log("%cTimeline Info:", conStyle.boldRed, result)
				commit('updateTimelineInfo', {
					id:vesselId,
					timelineInfo:result
				})
				
			})
			.catch((error)=>{
				// console.error(error)
			})
		}
		if(shouldLoadLightHouse()){
			vesselService.getLightHouseInfoByVesselId(baseInfo.vesselId)
			.then( ({result}) =>{				

				commit('updateLightHouseInfo', {
					id:vesselId,
					lightHouseInfo:result
				})
				console.log(result)
			})
			.catch((error)=>{
				// console.error(error)
			})
		}

	}	
}

export const vesselDetails = {
	state,
	getters,
	mutations,
	actions,
	namespaced:true,
	modules: {
		images,
		colors
	}
}