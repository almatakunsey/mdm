export const fields = [
	{
		name:'name', 
		title:'Name',
		active:true,
		inReport: true
	},
	{
		name:'mmsi', 
		title:'MMSI',
		active:true,
		inReport: true
	},
	{
		name:'draught', 
		title:'Draught',
		active:true,
		inReport: true
	},
	{
		name:'lengthXbeam', 
		title:'Length x Beam',
		active:true,
		inReport: true
	},
	{
		name:'latitude', 
		title:'Latitude',
		active:true,
		inReport: true
	},
	{
		name:'longitude', 
		title:'Longitude',
		active:true,
		inReport: true
	},
	{
		name:'sog', 
		title:'SOG',
		active:true,
		inReport: true
	},
	{
		name:'navStatus', 
		title:'Navigation Status',
		active:true,
		inReport: true
	},
	{
		name:'destination', 
		title:'Destination',
		active:true,
		inReport: true
	},
	{
		name:'orientations', 
		title:'COG',
		active:true,
		inReport: true
	},
	{
		name:'localTime', 
		title:'Local Received Time',
		active:true,
		inReport: true
	},
	{
		name:'UTCTime', 
		title:'UTC Received Time',
		active:true,
		inReport: true
	},
]




