const state = {
    enabled: false,
    showVessels: false,
    panelShown: true,
    error: null,
    vessels: new Map(),
    stateChangeTracker: 0,
}

const getters = {
    summaryError : state => state.error,
    enabled: state => state.enabled,
    showVessels: state => state.showVessels,
    panelShown: state => state.panelShown,
    vessels: state => state.stateChangeTracker && state.vessels,
    vesselsArr: state => state.stateChangeTracker && [...state.vessels.values()].filter(v => v.isDrawn && v.messageId == 21 && v.isSart == false && v.isMethydro == false)
}

const mutations = {
    setSummaryError(state, error){state.error = error},
    toggleAtonSummary(state, condition){state.enabled = condition},
    toggleShowVessels(state, condition){state.showVessels = condition},
    togglePanelShown(state, condition){ state.panelShown = condition }, 
    addVessel(state, vessel){
        state.vessels.set(vessel.vesselId, vessel)
        state.stateChangeTracker += 1           
    },
    clearVessels(state, vessels){ state.vessels.clear(); state.stateChangeTracker += 1},
}

export const summary = {
    state,
    getters,    
    mutations,
    namespaced: true
}