export const fields = {
	
	default:{
		aton : [
			{
		        name: '__slot:actions',
		        title: 'Actions',
		        active:true,
		        titleClass: 'center-aligned',
		        inReport: false
	        },
			{
				name:'health68', 
				title:'Status',
				// sortField: 'health68',
				width: '50px',
				active:true,
				inReport: false
			},
			{
				name:'mmsi', 
				title:'MMSI',
				sortField: 'mmsi',
				active:true,
				inReport: true
			},
			{
				name:'aisName', 
				title:'AIS Name',
				sortField: 'aisName',
				active:true,
				inReport: true
			},
			{
				name:'lastReport6', 
				title:'Last Report 6',
				sortField: 'lastReport6',
				active:true,
				inReport: true
			},
			{
				name:'lastReport8', 
				title:'Last Report 8',
				sortField: 'lastReport8',
				active:true,
				inReport: true
			},
			{
				name:'lastReport21', 
				title:'Last Report 21',
				sortField: 'lastReport21',
				active:true,
				inReport: true
			},
			{
				name:'type', 
				title:'AtoN Type',
				sortField: 'type',
				active:true,
				inReport: true
			},
			{
				name:'virtualAton', 
				title:'Virtual AtoN',
				sortField: 'virtualAton',
				active:true,
				inReport: true
			},
			{
				name:'offPosition68', 
				title:'Off-Position 6/8',
				sortField: 'offPosition68',
				active:true,
				inReport: true
			},
			{
				name:'offPosition21', 
				title:'Off-Position 21',
				sortField: 'offPosition21',
				active:true,
				inReport: true
			},
			{
				name:'latitude', 
				title:'Latitude',
				sortField: 'latitude',
				active:true,
				inReport: true
			},
			{
				name:'longitude', 
				title:'Longitude',
				sortField: 'longitude',
				active:true,
				inReport: true
			}
			
		],
		meteo: [
			{
		        name: '__slot:actions',
		        title: 'Actions',
		        active:true,
		        titleClass: 'center-aligned',
		        inReport: false
	        },
			{
				title: 'MMSI',
				name: 'mmsi',
				sortField: 'mmsi',
				active:true,
				inReport: true
			},
			{
				title: 'AIS Name',
				name: 'aisName',
				sortField: 'aisName',
				active:true,
				inReport: true
			},
			{
				title: 'Name',
				name: 'name',
				sortField: 'name',
				active:true,
				inReport: true
			},
			{
				title: 'Latitude',
				name: 'latitude',
				sortField: 'latitude',
				active:true,
				inReport: true
			},
			{
				title: 'Longitude',
				name: 'longitude',
				sortField: 'longitude',
				active:true,
				inReport: true
			},
			{
				title: 'AtoN Type',
				name: 'type',
				sortField: 'type',
				active:true,
				inReport: true
			},
			{
				title: 'Virtual AtoN',
				name: 'virtualAton',
				sortField: 'virtualAton',
				active:true,
				inReport: true
			},
			{
				title: 'Off-Position 6/8',
				name: 'offPosition68',
				sortField: 'offPosition68',
				active:true,
				inReport: true
			},
			{
				title: 'Off-Position 21',
				name: 'offPosition21',
				sortField: 'offPosition21',
				active:true,
				inReport: true
			},
			{
				title: 'Air Temperature',
				name: 'airTemp',
				sortField: 'airTemp',
				active:true,
				inReport: true
			},
			{
				title: 'Air Pressure',
				name: 'airPres',
				sortField: 'airPres',
				active:true,
				inReport: true
			},
			{
				title: 'Wind Speed',
				name: 'wndSpeed',
				sortField: 'wndSpeed',
				active:true,
				inReport: true
			},
			{
				title: 'Wind Gust',
				name: 'wndGust',
				sortField: 'wndGust',
				active:true,
				inReport: true
			},
			{
				title: 'Wind Direction',
				name: 'wndDir',
				sortField: 'wndDir',
				active:true,
				inReport: true
			},
			{
				title: 'Last Report 6',
				name: 'lastReport6',
				sortField: 'lastReport6',
				active:true,
				inReport: true
			},
			{
				title: 'Last Report 8',
				name: 'lastReport8',
				sortField: 'lastReport8',
				active:true,
				inReport: true
			},
			{
				title: 'Last Report 21',
				name: 'lastReport21',
				sortField: 'lastReport21',
				active:true,
				inReport: true
			}
		],
		monitoring: [
			{
		        name: '__slot:actions',
		        title: 'Actions',
		        active:true,
		        titleClass: 'center-aligned',
		        inReport: false
	        },
			{
				title:'MMSI',
				name:'mmsi',
				sortField:'mmsi',
				active:true,
				inReport: true
			},
			{
				title:'AIS Name',
				name:'aisName',
				sortField:'aisName',
				active:true,
				inReport: true
			},
			{
				title:'Name',
				name:'name',
				sortField:'name',
				active:true,
				inReport: true
			},
			{
				title:'Latitude',
				name:'latitude',
				sortField:'latitude',
				active:true,
				inReport: true
			},
			{
				title:'Longitude',
				name:'longitude',
				sortField:'longitude',
				active:true,
				inReport: true
			},
			{
				title:'AtoN Type',
				name:'type',
				sortField:'type',
				active:true,
				inReport: true
			},
			{
				title:'Virtual AtoN',
				name:'virtualAton',
				sortField:'virtualAton',
				active:true,
				inReport: true
			},
			{
				title:'Off-Position 6/8',
				name:'offPosition68',
				sortField:'offPosition68',
				active:true,
				inReport: true
			},
			{
				title:'Off-Position 21',
				name:'offPosition21',
				sortField:'offPosition21',
				active:true,
				inReport: true
			},
			{
				title:'RACON',
				name:'racon68',
				sortField:'racon68',
				active:true,
				inReport: true
			},
			{
				title:'RACON 21',
				name:'racon21',
				sortField:'racon21',
				active:true,
				inReport: true
			},
			{
				title:'Light',
				name:'light68',
				sortField:'light68',
				active:true,
				inReport: true
			},
			{
				title:'Light 21',
				name:'light21',
				sortField:'light21',
				active:true,
				inReport: true
			},
			{
				title:'Health',
				name:'health68',
				sortField:'health68',
				active:true,
				inReport: true
			},
			{
				title:'Health 21',
				name:'health21',
				sortField:'health21',
				active:true,
				inReport: true
			},
			{
				title:'Analogue Int.',
				name:'analogueInt',
				sortField:'analogueInt',
				active:true,
				inReport: true
			},
			{
				title:'Analogue Ext.1',
				name:'analogueExt1',
				sortField:'analogueExt1',
				active:true,
				inReport: true
			},
			{
				title:'Analogue Ext.2',
				name:'analogueExt2',
				sortField:'analogueExt2',
				active:true,
				inReport: true
			},
			{
				title:'Voltage Data',
				name:'voltageData',
				sortField:'voltageData',
				active:true,
				inReport: true
			},
			{
				title:'Current Data',
				name:'currentData',
				sortField:'currentData',
				active:true,
				inReport: true
			},
			{
				title:'Battery',
				name:'battery',
				sortField:'battery',
				active:true,
				inReport: true
			},
			{
				title:'Photocell',
				name:'photoCell',
				sortField:'photoCell',
				active:true,
				inReport: true
			},
			{
				title:'Last Report 6',
				name:'lastReport6',
				sortField:'lastReport6',
				active:true,
				inReport: true
			},
			{
				title:'Last Report 8',
				name:'lastReport8',
				sortField:'lastReport8',
				active:true,
				inReport: true
			},
			{
				title:'Last Report 21',
				name:'lastReport21',
				sortField:'lastReport21',
				active:true,
				inReport: true
			}
		]
	}, // default

	host: {
		aton:'',
		meteo:'',
		monitoring:''
	},
	tenant:  {
		aton:'',
		meteo:'',
		monitoring:''
	},
	user:  {
		aton:'',
		meteo:'',
		monitoring:''
	}
	
}




