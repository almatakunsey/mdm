import {summary} from './summary'
import {atonList} from './atonList'
import atonService from '../../services/aton'

export const aton = {		
    namespaced: true,
    modules: {
        summary,
        atonList
    }	
}