export const sortFields = {
	default: {
		
        aton:[
            {
                direction: "asc",
                field: "mmsi",
                sortField: "mmsi"
            }
        ],
        meteo:[
            {
                direction: "asc",
                field: "mmsi",
                sortField: "mmsi"
            }
        ],
        monitoring:[
            {
                direction: "asc",
                field: "mmsi",
                sortField: "mmsi"
            }
        ]
	    
	},

	host: {
		aton: '',
		meteo: '',
		monitoring: ''
	},

	tenant: {
		aton: '',
		meteo: '',
		monitoring: ''
	},

	user: {
		aton: '',
		meteo: '',
		monitoring: ''
	}
}