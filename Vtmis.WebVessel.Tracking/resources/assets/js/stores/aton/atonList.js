import atonService from '../../services/aton'

import { fields } from './atonListField'
import { sortFields } from './atonListSortField'

const formDataOrigin = {
    area: 'all',
    type: 2,
    // timeSpan: 1,
    refreshEnabled: false,
    refreshRate: 60,
    searchTerm: ''
}

const state = {
	formData: _.cloneDeep(formDataOrigin),
    enabled: false,
    areaOptions: [
        {text: 'All', id: 'all'},
        {text: 'Screen', id: 'screen'}
    ],
    typeOptions: [
        // {text: 'Select One', id: '0'},
        {text: 'AtoN', id: '2'},
        {text: 'Meteo Data', id: '7'},
        {text: 'Monitoring Data', id: '9'}
    ],
    atonData: [],
    fields: fields,
    sortFields: sortFields   
}

const getters = {
	formData: state => state.formData,
    areaOptions: state => state.areaOptions,
    typeOptions: state => state.typeOptions,
    enabled: state => state.enabled,
    atonData: state => state.atonData,   
    field: state => (type, level='') => {        
        if(!level){
            if(state.fields.user[type]){ return state.fields.user[type]}
            else if(state.fields.tenant[type]){ return state.fields.tenant[type]}
            else if(state.fields.host[type]){ return state.fields.host[type]}
            return state.fields.default[type]
        }
        return state.fields[level][type]        
    },
    sortField: state => (type, level='') => {        
        if(!level){
            if(state.sortFields.user[type]){ return state.sortFields.user[type]}
            else if(state.sortFields.tenant[type]){ return state.sortFields.tenant[type]}
            else if(state.sortFields.host[type]){ return state.sortFields.host[type]}
            else if(state.sortFields.default[type]){ return state.sortFields.default[type]}
        }   
        return state.sortFields[level][type]
    }
}

const mutations = {
	clearFormData(state){ state.formData = _.cloneDeep(formDataOrigin) },
    setFormData(state, {key, value}){
        state.formData[key] = value        
    },
    toggleAtonList(state, condition){state.enabled = condition},
    setAtonData(state, atonData){ state.atonData = atonData },
    clearAtonData(state){ state.atonData = []},
    setField(state, {level, type, field}){state.fields[level][type] = field},    
    setSortField(state, {level, type, sortField}){        
        state.sortFields[level][type] = sortField        
    }    
}

const actions = {

    getAtonByType : ({commit}, type) => {

        return new Promise((resolve, reject)=>{

            atonService.getAtons(type)
            .then(({data:{result:{items}}}) =>{             
                commit('setAtonData', items)
                resolve(items)
                // console.log("%cAton from api:", conStyle.boldWhite, items)
            })          
            .catch(( {response} )=>{                            
                reject(response)                
            })
        })                              
    }
}



export const atonList = {
    state,
    getters,    
    mutations,
    actions,
    namespaced: true
}