const state = {
    enabled: false
}

const mutations = {
    toggleTenantFilterList: (state, condition) => state.enabled = condition
}

const getters = {
    enabled: state => state.enabled
}

export const summary = {
    state,
    getters,  
    mutations,
    namespaced: true
}