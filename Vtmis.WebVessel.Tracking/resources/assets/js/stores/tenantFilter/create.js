import tenantFilterService from '../../services/tenantFilter'

const state = {
    enabled: false    

}

const getters = {
    enabled: state => state.enabled    
}

const mutations = {
    toggleTenantFilterCreate(state, condition){state.enabled = condition}    
}

const actions = {
    storeTenantFilter({commit, getters, rootGetters}){        

        return new Promise((resolve, reject)=>{                                    
            tenantFilterService.createTenantFilter(rootGetters['tenantFilter/form/formData']).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    }
}

export const create = {
    state,
    getters,      
    mutations,
    actions,
    namespaced: true
}