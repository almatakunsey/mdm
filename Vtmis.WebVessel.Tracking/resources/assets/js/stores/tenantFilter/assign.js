import tenantFilterService from '../../services/tenantFilter'

const state = {
    enabled: false
}

const mutations = {
    toggleTenantFilterAssign: (state, condition) => state.enabled = condition
}

const getters = {
    enabled: state => state.enabled
}

const actions = {
    assignUsersToFilter({commit}, data) {
        return new Promise((resolve, reject) => {
            tenantFilterService.assignTenantFilter(data).then(
                (result)=>{                    
                    resolve(result)
                },
                (error)=>{
                    reject(error)
                }
            )
        })
    }
}
export const assign = {
    state,
    getters,  
    mutations,
    actions,
    namespaced: true
}