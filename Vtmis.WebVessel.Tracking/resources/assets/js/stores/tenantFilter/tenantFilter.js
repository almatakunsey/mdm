import { summary } from './summary'
import { assign } from './assign'
import { create } from './create'
import {form} from './form'

import tenantFilterService from '../../services/tenantFilter'


const state = {
    listFilters : [],
    filter: {},
    filterId: 0
}

const mutations = {
    setListTenantFilters(state, listFilters) {
        state.listFilters = listFilters
    },
    setTenantFilter(state, filter){
        state.filter = filter
    },
    setFilterId(state, id) {
        state.filterId = +id
    }
}

const actions = {
    loadTenantFilters({commit}) {
        return new Promise((resolve, reject) => {
            tenantFilterService.getTenantFilters(1,1000)
            .then(({data: {result: {items}}}) => {
                commit('setListTenantFilters', items);
                resolve(items)
            })
            .catch((error) => {
                console.log(error)
                reject(error)
            })
        })
    },
    loadTenantFilterById({commit}, id) {
        return new Promise((resolve, reject) => {
            tenantFilterService.getTenantFilter(id)
            .then(({data: {result: {item}}}) => {
                commit('setTenantFilter', item);
                resolve(item)
            })
            .catch((error) => {
                console.log(error)
                reject(error)
            })
        })
    }
}

const getters = {
    listTenantFilters(state) {
        return state.listFilters;
    },
    searchTerm(state){
        return state.searchTerm
    },
    tenantFilterById(state){
        return _.keyBy(state.listFilters, 'id')[state.filterId]
    }

}

export const tenantFilter = {
    state,
    getters,
    actions,
    mutations,
    namespaced: true,
    modules: {
        summary,
        create,
        form,
        assign
    }
}