export default {
    condition: [
        {text: 'OR', id: 0},
        {text: 'AND', id:1}
    ],
    type: [
        {text: 'MMSI', id: 'mmsi'},
        {text: 'Ship Type', id: 'shipType'},
        {text: 'AIS Type', id: 'aisType'},
    ],
    operation: [
        {text: '=', id: '='},
        {text: '!=', id: '!='}
    ],
    aisTypeOptions : [
        { text: "Base Station", id: "BaseStation" },
        { text: "Class A", id: "ClassA" },
        { text: "Class B", id: "ClassB" },
        { text: "AtoN", id: "Aton" },
        { text: "SAR", id: "SAR" }
    ]
}