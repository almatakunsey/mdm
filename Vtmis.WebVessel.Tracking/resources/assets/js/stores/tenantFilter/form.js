import utilHelper from '../../helpers/util'
import fields from './formFields'
import filtersService from '../../services/filters'

const tenantFilterItemsPristine = {
	rid: utilHelper.genRandom(32),
	condition: 0,
	columnName: 'mmsi',
	operation: '=',
	columnValue: '',
    // rowIndex: 0
}

const formDataPristine =  {
    filterName: '',
	filterItems: [_.cloneDeep(tenantFilterItemsPristine)]
} 
const state = {
    formData: _.cloneDeep(formDataPristine),
    condition: fields.condition,
    type: fields.type,
    operation: fields.operation,
    shipTypeOptions: [],
    aisTypeOptions: fields.aisTypeOptions
}

const getters = {
    formData: state => state.formData,
    tenantFilterItems: state => state.formData.filterItems,
    condition: state => state.condition,
    type: state => state.type,
    operation: state => state.operation,
    shipTypeOptions: state => state.shipTypeOptions,
    aisTypeOptions: state => state.aisTypeOptions
}

const getItemIndex = (state, rid)=>{
	return _.findIndex(state.formData.filterItems, {rid})
}

const mutations = {
    setShipTypesList(state, shipType){	
        let shipTypes = [];	
        _.each(shipType, ({ id, shipTypeName }) => { 
            shipTypes.push({ text: shipTypeName, id:shipTypeName })
        })        

        state.shipTypeOptions = shipTypes
    },
    clearData(state) {state.formData = _.cloneDeep(formDataPristine)},
    setFormData(state, {key, value}){
        state.formData[key] = value        
    },
    addTenantFilterItem(state){
        const newItem = _.cloneDeep(tenantFilterItemsPristine)
        newItem.rid = utilHelper.genRandom(32)
        state.formData.filterItems.push(newItem)            
    },
    setTenantFilterItem(state, {type, value, rid}){ 
        const dataIndex = _.findIndex(state.formData.filterItems, {rid})
        state.formData.filterItems[dataIndex][type] = value

    },
    removeTenantFilterItem(state, index){   
        state.formData.filterItems.splice(index, 1)
    },

}

const actions = {
    loadShipTypeList({commit}){		
        
        return new Promise((resolve, reject)=>{
            filtersService.getShipTypes()
            .then(({data:{result}})=>{
                commit('setShipTypesList', result)                
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })
        })
    },
}

export const form = {
	state,
    getters,
    mutations,
    actions,
	namespaced:true	
}
