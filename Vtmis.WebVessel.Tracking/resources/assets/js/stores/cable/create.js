import cableService from '../../services/cable'

const state = {
	enabled: false
}

const getters = {
	enabled: state => state.enabled
}

const mutations = {
	toggleCableCreate(state, condition){state.enabled = condition}
}

const actions = {
	storeCable({commit, getters, rootGetters}){		
		return new Promise((resolve, reject)=>{
			// get a clone of the formdata
            const formData = _.cloneDeep(rootGetters['cable/form/formData'])

            // remove some unneeded properties from waypoints
			formData.waypoints.forEach((wp)=>{
				delete wp.id
				delete wp.highlight
			})

			// change the key name to follow api
			formData['cableDetails'] = formData.waypoints
			delete formData.waypoints

			cableService.storeCable(formData)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})			

		})
		
	}
}

export const create = {
	state,
	getters,
	mutations,
	actions,
	namespaced:true
}