import utilHelper from '../../helpers/util'
import moment from 'moment'

const waypointDataPristine = {
	id: utilHelper.genRandom(32),
	// isShown: true,
	// name: "C1",
	latitude: "",
	longitude: "",	
	highlight:false,
    order:0
}

const formDataPristine = {
	name: "",
    description: "",
    location: "",
	isEnable: true,
	color: "#ff0000",
    date: moment().format('YYYY-MM-DDTHH:mm:ss.SSS[Z]'),
    // the api key for waypoints is cableDetails
    // we're going to reformat before load, store and edit
    waypoints:[_.cloneDeep(waypointDataPristine)]
	
}

/*
state
 */
const state = {
	formData : _.cloneDeep(formDataPristine)
}

/*
getters
 */
const getters = {
	formData : state => state.formData    
}


/*
mutations
 */

const getNextBiggerNumber = (waypoints)=>{
    let numArr = []
    waypoints.forEach((w)=>{
        if(w.name.match(/(C)\d+/g)){
            numArr.push(parseInt(w.name.match(/\d+/g)[0])) 
        }        
    })
    numArr.sort()   
    let i = 1
    while( numArr.indexOf(i) >= 0 ) {
        i++;
    }
    return i;
}

const mutations = {
	setFormData(state, {key, value}){                
        state.formData[key] = value
    },
    replaceFormData(state, data){state.formData = data},
    resetFormData(state){state.formData = _.cloneDeep(formDataPristine)},
    addNewWaypoint(state, data){    	
    	
    	let newWaypoint = _.cloneDeep(waypointDataPristine)    	
        
        newWaypoint.id = utilHelper.genRandom(32)

        // get next order to assign       
        let nextOrder = 0        
        if(state.formData.waypoints.length){
            nextOrder = _.maxBy(state.formData.waypoints, 'order').order + 1
        }   

        newWaypoint.order = nextOrder             
                	
    	if(	typeof data !== 'undefined' && 
    		typeof data.latitude !== 'undefined'){    		
    		newWaypoint.latitude = data.latitude
    	}
    	if(	typeof data !== 'undefined' && 
    		typeof data.longitude !== 'undefined'){
    		newWaypoint.longitude = data.longitude
    	}

    	state.formData.waypoints.push(newWaypoint)
    },
    setWaypointData(state, {id, key, value}){    	
    	const dataIndex = _.findIndex(state.formData.waypoints, {id})
    	state.formData.waypoints[dataIndex][key] = value
    },
    removeWaypoint(state, id){    	
        state.formData.waypoints.splice(_.findIndex(state.formData.waypoints, {id}), 1)    
    },
    clearWaypoints(state){
    	state.formData.waypoints = []
    },
    togglePointHighlight(state, {id, value}){        
        state.formData.waypoints[_.findIndex(state.formData.waypoints, {id})].highlight = value               
    },
    clearPointHighlight(state){
        if(state.formData.waypoints.length){
            state.formData.waypoints.forEach((point)=>{
                point.highlight = false
            })
        }
    }
}

export const form = {
	state,
	getters,
	mutations,
	namespaced: true
}