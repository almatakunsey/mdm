import { summary } from './summary'
import { create } from './create'
import { form } from './form'
import { edit } from './edit'

import cableService from '../../services/cable'

const cableInstancePristine = {	
	lines: [],
	points: []
}

const state = {			
	cables: [],
	cablesReady: false,
	cableInstances: _.cloneDeep(cableInstancePristine),
	designer:{
        enabled:false        
    },
    fromDesigner: false
}

const getters = {
	cables: state => {		
		return _.map(state.cables, c => {            
            c.waypoints = _.sortBy(c.waypoints, ["order"])
            return c
        })
	},
	cableInstances: state => state.cableInstances,
	cablesReady: state => state.cablesReady,			
	cableDesignerEnabled: state => state.designer.enabled,
	fromDesigner: state => state.fromDesigner
}

const mutations = {		
	toggleCableDesigner(state, condition){state.designer.enabled = condition},    				
	toggleFromDesigner(state, condition){ state.fromDesigner = condition },
	setCables(state, cables){ 
		state.cables = cables 
	},
	clearCables(state){ state.cables = [] },
	toggleCablesReady(state, condition){ state.cablesReady = condition },
	addCableInstance(state, {type, instance}){ state.cableInstances[type].push(instance) },
	clearCableInstances(state){ state.cableInstances = _.cloneDeep(cableInstancePristine) },        
}

const actions = {
	loadAllCables({commit, getters}){		
		commit('toggleCablesReady', false)
		commit('clearCables')
		return new Promise((resolve, reject)=>{			
			cableService.getCables(1, 1000)
			.then(({data:{result:{items}}})=>{

				items.forEach((item)=>{
					item['waypoints'] = item.cableDetails
					delete item['cableDetails']	
				})
			
				commit('setCables', items)
				commit('toggleCablesReady', true)
				resolve(items)
			})
			.catch((error)=>{
				console.log(error)
				reject(error)
			})
		})		
	},
	removeCable({commit, getters}, id){
		return new Promise((resolve, reject)=>{
			cableService.removeCable(id)
			.then((result)=>{
				resolve(result)
			})
			.catch((error)=>{
				reject(error)
			})
		})		
	}
}

export const cable = {
	state,
	getters,
	mutations,
	actions,
	namespaced: true,
	modules: {
		create,
		summary,
		form,
		edit
	}
}