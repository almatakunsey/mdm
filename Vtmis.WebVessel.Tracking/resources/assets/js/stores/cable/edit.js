import utilHelper from '../../helpers/util'
import async from 'async'
import cableService from '../../services/cable'

const state = {
    editedCableId: null,
    enabled: false    
}

const getters = {
    enabled: state => state.enabled,    
    editedCableId: state => state.editedCableId,
    fromDesigner: state => state.fromDesigner
}

const mutations = {
    setEditedCableId(state, id){state.editedCableId = id},
    toggleCableEdit(state, condition){state.enabled = condition}    
}

const actions = {
    loadCable({commit, getters, rootGetters}){
        return new Promise((resolve, reject)=>{
            cableService.getCable(getters.editedCableId)
            .then(({data:{result}})=>{                               

                result.cableDetails = _.sortBy(result.cableDetails, ['order']).map((c)=>{
                    c.highlight = false
                    return c
                })

                result['waypoints'] = result.cableDetails
                delete result['cableDetails']

                commit('cable/form/replaceFormData', result, {root:true})
                resolve(result) 
            })
            .catch((error)=>{
                reject(error)
            })  
        })
    },

    updateCable({commit, getters, rootGetters}){     
        return new Promise((resolve, reject)=>{
            // get a clone of the formdata
            const formData = _.cloneDeep(rootGetters['cable/form/formData'])            

            // remove some unneeded properties from waypoints
            formData.waypoints.forEach((wp)=>{                
                delete wp.highlight
                if(typeof wp.id === 'string'){
                    delete wp.id
                }
            })

            formData['cableDetails'] = formData.waypoints
            delete formData['waypoints']

            cableService.updateCable(formData)
            .then((result)=>{
                resolve(result)
            })
            .catch((error)=>{
                reject(error)
            })          

        })
        
    }
}

export const edit = {
    state,
    getters,      
    mutations,
    actions,    
    namespaced: true
}