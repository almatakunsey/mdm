import zonetypeMaxlength from './zonetype-maxlength'
import coordlist from './coordlist'

export const customValidations = {
	zonetypeMaxlength,
	coordlist
}