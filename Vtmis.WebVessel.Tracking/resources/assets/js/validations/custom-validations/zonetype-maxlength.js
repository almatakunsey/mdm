import { Validator } from 'vee-validate'
import validator from 'validator'	

export default (() => {
	Validator.extend('zonetype-maxlength', {
		getMessage: field => `The ${field} length not correct for chosen zone type`,
		validate: (value, {zoneType}) => {			

			const coords = value.split(/\r?\n/g)
			
			if( zoneType == 0 && coords.length > 1){ return false }
			else if( zoneType == 1 && coords.length < 3){ return false }
			else if( (zoneType == 2 || zoneType == 3) && coords.length != 2 ){ return false }
			
			return true	
		}
	}, {paramNames: ['zoneType']})
})()