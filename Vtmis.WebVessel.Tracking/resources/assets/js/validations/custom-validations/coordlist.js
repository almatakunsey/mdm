import { Validator } from 'vee-validate'
import validator from 'validator'	

export default ( () => {
	Validator.extend('coordlist', {
		getMessage: (field) => {			
			return `The ${field} is not in the right format`
		},
		validate: value => {
			const coords = value.split(/\r?\n/g).map(coord => coord.replace(/\s/g, ""))
			
			// if some of them dont have exactly 1 comma
			if(_.some(coords, coord => coord.split(",").length - 1 !== 1)){return false}

			for(let coord of coords){			
				if( _.some(coord.split(","), c => validator.isNumeric(String(c)) == false ) ){	
					return false;
					break;
				}
			}							

			return true
		}
	}, {
		immediate:false
	});
})()