import { Validator } from 'vee-validate';

export default (()=>{
    const dictionary = {
        en: {
            attributes:{
                wpname: 'Waypoint Name'
            }
        }        
    };

    // Override and merge the dictionaries
    Validator.localize(dictionary);  
})()