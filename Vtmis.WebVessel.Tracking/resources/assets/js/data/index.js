require('./zone')
require('./alarm')
require('./filter')
require('./cable')
require('./wrems')
require('./vesselDetails')
require('./image')

// default
mock.onAny().passThrough();