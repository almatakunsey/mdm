import utilHelper from '../../helpers/util'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const alarmListMocks = (()=>{
    
    mock.onGet(`${host}/api/services/app/Alarm/GetAllByUserIdAsync?userId=${localStorage.userId}`)
    .reply(200, 
    {
        result : [
            {
              "tenantId": 1,
              "name": "Mock Alarm 1",
              "isEnabled": false,
              "priority": '1',
              "message": "Mock Alarm 1 Message",
              "color": "#1e3c63",
              "repeatInterval": 120,
              "alarmItems": [
                  { 
                    "id": 123,
                    "condition": '',
                    "columnName": 'Condition 1',
                    "alarmTypeItems": 'RACON Error',
                    "operation": '',
                    "columnValue": ''
                  },
                  { 
                    "id": 456,
                    "condition": '0',
                    "columnName": 'Condition 2',
                    "alarmTypeItems": 'Wind Speed (knots)',
                    "operation": '>=',
                    "columnValue": 12
                  },
                  { 
                    "id": 789,
                    "condition": '1',
                    "columnName": 'Filter',
                    "alarmTypeItems": '2',
                    "operation": '',
                    "columnValue": ''
                  }
              ],
              "isSendAISSafetyMessage": true,
              "aisMessage": "Mock Alarm 1 AIS Message",
              "isSendEmail": true,
              "emailAddress": "mdm1@greenfinder.com",
              "isDeleted": false,
              "deleterUserId": 0,
              "deletionTime": "",
              "lastModificationTime": "2018-10-14T08:41:01.158Z",
              "lastModifierUserId": 0,
              "creationTime": "2018-10-14T08:41:01.158Z",
              "creatorUserId": 0,
              "id": 1
            },
            {
                "tenantId": 1,
                "name": "Mock Alarm 2",
                "isEnabled": false,
                "priority": '0',
                "message": "Mock Alarm 2 Message",
                "color": "#d1256d",
                "repeatInterval": 121,
                "alarmItems": [{ 
                  "id": 123,
                  "condition": '',
                  "columnName": 'Condition 1',
                  "alarmTypeItems": 'RACON Error',
                  "operation": '',
                  "columnValue": ''
                },
                { 
                  "id": 456,
                  "condition": '0',
                  "columnName": 'Condition 2',
                  "alarmTypeItems": 'Wind Speed (knots)',
                  "operation": '>=',
                  "columnValue": 12
                }],
                "isSendAISSafetyMessage": true,
                "aisMessage": "Mock Alarm 2 AIS Message",
                "isSendEmail": false,
                "emailAddress": "mdm2@greenfinder.com",
                "isDeleted": false,
                "deleterUserId": 0,
                "deletionTime": "",
                "lastModificationTime": "2018-10-14T08:41:01.158Z",
                "lastModifierUserId": 0,
                "creationTime": "2018-10-14T08:41:01.158Z",
                "creatorUserId": 0,
                "id": 2
              },
              {
                "tenantId": 1,
                "name": "Mock Alarm 3",
                "isEnabled": true,
                "priority": '2',
                "message": "Mock Alarm 3 Message",
                "color": "#2abc0d",
                "repeatInterval": 122,
                "alarmItems": [{ 
                  "id": 123,
                  "condition": '',
                  "columnName": 'Condition 1',
                  "alarmTypeItems": 'RACON Error',
                  "operation": '',
                  "columnValue": ''
                },
                { 
                  "id": 456,
                  "condition": '0',
                  "columnName": 'Condition 2',
                  "alarmTypeItems": 'Wind Speed (knots)',
                  "operation": '>=',
                  "columnValue": 12
                }],
                "isSendAISSafetyMessage": true,
                "aisMessage": "Mock Alarm 3 AIS Message",
                "isSendEmail": false,
                "emailAddress": "mdm3@greenfinder.com",
                "isDeleted": false,
                "deleterUserId": 0,
                "deletionTime": "",
                "lastModificationTime": "2018-10-14T08:41:01.158Z",
                "lastModifierUserId": 0,
                "creationTime": "2018-10-14T08:41:01.158Z",
                "creatorUserId": 0,
                "id": 3
              }
          ]
    });

})()

export { alarmListMocks }