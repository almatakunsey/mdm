import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const alarmUpdateMocks = (()=>{
    
    mock.onPut(`${host}/api/services/app/Alarm/UpdateAsync`)
    .reply(200, 
    {
        result : "Success"
    });    

})()

export { alarmUpdateMocks }