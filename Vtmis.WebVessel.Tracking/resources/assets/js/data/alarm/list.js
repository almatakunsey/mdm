import utilHelper from '../../helpers/util'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const alarmListMocks = (()=>{
    
    mock.onGet(`${host}/api/services/app/Alarm/GetAllByUserIdAsync?page=1`)
    .reply(200, 
    {
        total: 20,
        per_page: 10,
        current_page: 1,
        last_page: 2,
        next_page_url: `${host}/api/services/app/Alarm/GetAllByUserIdAsync?page=2`,
        prev_page_url: null,
        from: 1,
        to: 10,
        data : [
          {
            dateTime:'2019-07-04 10:24:00',
            zone:'Anchor West',
            'mmsi':5501250,
            vesselName:'Angsana',
            message: 'Enter Langkawi Port',
            info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
          },
          {
            dateTime:'2019-07-04 10:24:00',
            zone:'Anchor West',
            'mmsi':5501250,
            vesselName:'Angsana',
            message: 'Enter Langkawi Port',
            info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
          },
          {
            dateTime:'2019-07-04 10:24:00',
            zone:'Anchor West',
            'mmsi':5501250,
            vesselName:'Angsana',
            message: 'Enter Langkawi Port',
            info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
          },
          {
            dateTime:'2019-07-04 10:24:00',
            zone:'Anchor West',
            'mmsi':5501250,
            vesselName:'Angsana',
            message: 'Enter Langkawi Port',
            info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
          },
          {
            dateTime:'2019-07-04 10:24:00',
            zone:'Anchor West',
            'mmsi':5501250,
            vesselName:'Angsana',
            message: 'Enter Langkawi Port',
            info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
          },
          {
            dateTime:'2019-07-04 10:24:00',
            zone:'Anchor West',
            'mmsi':5501250,
            vesselName:'Angsana',
            message: 'Enter Langkawi Port',
            info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
          },
          {
            dateTime:'2019-07-04 10:24:00',
            zone:'Anchor West',
            'mmsi':5501250,
            vesselName:'Angsana',
            message: 'Enter Langkawi Port',
            info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
          },
          {
            dateTime:'2019-07-04 10:24:00',
            zone:'Anchor West',
            'mmsi':5501250,
            vesselName:'Angsana',
            message: 'Enter Langkawi Port',
            info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
          },
          {
            dateTime:'2019-07-04 10:24:00',
            zone:'Anchor West',
            'mmsi':5501250,
            vesselName:'Angsana',
            message: 'Enter Langkawi Port',
            info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
          },
          {
            dateTime:'2019-07-04 10:24:00',
            zone:'Anchor West',
            'mmsi':5501250,
            vesselName:'Angsana',
            message: 'Enter Langkawi Port',
            info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
          }

        ]           
    });

    mock.onGet(`${host}/api/services/app/Alarm/GetAllByUserIdAsync?page=2`)
    .reply(200, 
    {
        total: 20,
        per_page: 10,
        current_page: 2,
        last_page: 2,
        next_page_url: null,
        prev_page_url: `${host}/api/services/app/Alarm/GetAllByUserIdAsync?page=1`,
        from: 11,
        to: 20,
        data : [          
            {
              dateTime:'2019-07-04 10:24:00',
              zone:'Anchor East',
              'mmsi':5501250,
              vesselName:'Angsana',
              message: 'Enter Langkawi Port',
              info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
            },
            {
              dateTime:'2019-07-04 10:24:00',
              zone:'Anchor East',
              'mmsi':5501250,
              vesselName:'Angsana',
              message: 'Enter Langkawi Port',
              info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
            },
            {
              dateTime:'2019-07-04 10:24:00',
              zone:'Anchor East',
              'mmsi':5501250,
              vesselName:'Angsana',
              message: 'Enter Langkawi Port',
              info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
            },
            {
              dateTime:'2019-07-04 10:24:00',
              zone:'Anchor East',
              'mmsi':5501250,
              vesselName:'Angsana',
              message: 'Enter Langkawi Port',
              info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
            },
            {
              dateTime:'2019-07-04 10:24:00',
              zone:'Anchor East',
              'mmsi':5501250,
              vesselName:'Angsana',
              message: 'Enter Langkawi Port',
              info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
            },
            {
              dateTime:'2019-07-04 10:24:00',
              zone:'Anchor East',
              'mmsi':5501250,
              vesselName:'Angsana',
              message: 'Enter Langkawi Port',
              info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
            },
            {
              dateTime:'2019-07-04 10:24:00',
              zone:'Anchor East',
              'mmsi':5501250,
              vesselName:'Angsana',
              message: 'Enter Langkawi Port',
              info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
            },
            {
              dateTime:'2019-07-04 10:24:00',
              zone:'Anchor East',
              'mmsi':5501250,
              vesselName:'Angsana',
              message: 'Enter Langkawi Port',
              info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
            },
            {
              dateTime:'2019-07-04 10:24:00',
              zone:'Anchor East',
              'mmsi':5501250,
              vesselName:'Angsana',
              message: 'Enter Langkawi Port',
              info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
            },
            {
              dateTime:'2019-07-04 10:24:00',
              zone:'Anchor East',
              'mmsi':5501250,
              vesselName:'Angsana',
              message: 'Enter Langkawi Port',
              info: 'SOG: 0.4knt, COG: 284, DEST:TP, ETA: 2019-07-13 21:00:00'
            }          
        ]           
    });

})()

export { alarmListMocks }