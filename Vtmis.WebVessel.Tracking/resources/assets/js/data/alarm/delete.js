import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const alarmDeleteMocks = (()=>{
    
    mock.onDelete(`${host}/api/services/app/Alarm/DeleteAsync?id=1`)
    .reply(200, 
    {
        result : "Success"
    });
    mock.onDelete(`${host}/api/services/app/Alarm/DeleteAsync?id=2`)
    .reply(200, 
    {
        result : "Success"
    });
    mock.onDelete(`${host}/api/services/app/Alarm/DeleteAsync?id=3`)
    .reply(200, 
    {
        result : "Success"
    }); 

})()

export { alarmDeleteMocks }