import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const filterListMocks = (()=>{
    
    mock.onGet(`${host}/api/services/app/FilterService/GetAllByUserIdAsync?userId=${localStorage.userId}`)
    .reply(200, 
    {
        result : [                        
            {id: 1, name: 'Blue Colored Vessel'},
            {id: 2, name: 'Green Colored Vessel'}            
        ]
    });

})()

export { filterListMocks }