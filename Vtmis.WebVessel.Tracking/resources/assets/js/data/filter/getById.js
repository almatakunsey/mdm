import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const filterGetByIdMocks = (()=>{
    
    mock.onGet(`${host}/api/services/app/FilterService/GetById?id=1`)
    .reply(200, 
    {
        result : {
            id: 1,
            name: 'Blue Colored Vessel',
            isEnable: true,
            isShowShips: true,
            filterItemDtos: [
                {                    
                    andOr: 'and',
                    field: 'color',
                    operator: '=',
                    value: '#0000ff'
                }
            ]
        }
    });

    mock.onGet(`${host}/api/services/app/FilterService/GetById?id=2`)
    .reply(200, 
    {
        result : {
            id: 2,
            name: 'Green Colored Vessel',
            isEnable: true,
            isShowShips: true,
            filterItemDtos: [
                {                    
                    andOr: 'and',
                    field: 'color',
                    operator: '=',
                    value: '#00ff00'
                }
            ]
        }
    });
    

})()

export { filterGetByIdMocks }