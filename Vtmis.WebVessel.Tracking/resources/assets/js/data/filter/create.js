import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const filterCreateMocks = (()=>{
    
    mock.onPost(`${host}/api/services/app/FilterService/Create`)
    .reply(200, 
    {
        result : "Success"
    });    

})()

export { filterCreateMocks }