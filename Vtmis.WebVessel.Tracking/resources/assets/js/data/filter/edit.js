import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const filterEditMocks = (()=>{
    
    mock.onPost(`${host}/api/services/app/FilterService/Update`)
    .reply(200, 
    {
        result : "Success"
    });    

})()

export { filterEditMocks }