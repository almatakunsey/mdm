import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const filterListVesselsMocks = (()=>{
    
    mock.onGet(`${host}/api/services/app/FilterService/GetVesselsByFilterId?filterId=1`)
    .reply(200, 
    {
        result : [                        
            111665702,
            636015131,
            419001217
        ]
    });

    mock.onGet(`${host}/api/services/app/FilterService/GetVesselsByFilterId?filterId=2`)
    .reply(200, 
    {
        result : [                        
            533190053,
            533191118,
            503465200
        ]
    });

})()

export { filterListVesselsMocks }