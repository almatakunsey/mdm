import utilHelper from '../../helpers/util'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const imageDeleteMock = (()=>{

	mock.onDelete(`${host}/api/Image/delete`)
	.reply(200,

		 // start data
		 {
		 	result:"OK"
		 }
		 // end data
	)

})()

export { imageDeleteMock }