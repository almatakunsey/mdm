import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const zoneCreateMocks = (()=>{
    
    mock.onPost(`${host}/api/services/app/ZoneService/Update`)
    .reply(200, 
    {
        result : "Success"
    });    

})()

export { zoneUpdateMocks }