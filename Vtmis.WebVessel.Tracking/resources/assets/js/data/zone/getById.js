import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const zoneGetByIdMocks = (()=>{
    
    mock.onGet(`${host}/api/services/app/ZoneService/GetById?id=1`)
    .reply(200, 
    {
        result : {
            "id": 1,
            "name": "No Entry Zone",
            "isEnable": true,
            "radius": 15000,
            "colour": "#60a608",
            "isFill": true,
            "zoneType": 0,
            "zoneItemDtos": [
                {
                    "latitude": 2.93,
                    "logitude": 100.77
                }
            ]
        }
    });

    mock.onGet(`${host}/api/services/app/ZoneService/GetById?id=2`)
    .reply(200, 
    {
        result :  {
            "id": 2,
            "name": "Type A Vessel Only",
            "isEnable": true,
            "radius": 0,
            "colour": "#b5107d",
            "isFill": true,
            "zoneType": 1,
            "zoneItemDtos": [
                {
                    "latitude": 2.61,
                    "logitude": 101.03
                },
                {
                    "latitude": 2.34,
                    "logitude": 100.99
                },
                {
                    "latitude": 2.35,
                    "logitude": 101.45
                }
            ]
        }  
    });

})()

export { zoneGetByIdMocks }