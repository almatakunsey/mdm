import utilHelper from '../../helpers/util'
const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const zoneListMocks = (()=>{
    
    mock.onGet(`${host}/api/services/app/ZoneService/GetAllByUserIdAsync?userId=${localStorage.userId}`)
    .reply(200, 
    {
        result : [            
            {
                "id": 1,
                "name": "No Entry Zone",
                "creationTime": "2018-10-05T07:24:43.404Z",
                "isEnable": true,
                "radius": 0,
                "colour": "string",
                "isFill": true,
                "zoneType": 0
            },
            {
                "id": 2,
                "name": "Type A Vessel Only",
                "creationTime": "2018-10-05T07:24:43.404Z",
                "isEnable": true,
                "radius": 0,
                "colour": "string",
                "isFill": true,
                "zoneType": 0
            }            
        ]
    });    

})()

export { zoneListMocks }