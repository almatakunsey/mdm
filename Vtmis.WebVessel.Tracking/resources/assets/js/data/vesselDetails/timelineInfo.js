import utilHelper from '../../helpers/util'
const mapTrackingHost = `${utilHelper.getProtocol()}://${window.location.hostname}:${utilHelper.getMapTrackingApiPort()}`

const timelineInfoMocks = (()=>{
    
    mock.onPost(`${mapTrackingHost}/api/targets/timeline/details`)
    .reply(200, 
    {
        result : [                        
            {id: 1, name: 'Vessel Group A', dateCreated: '2019-10-25T01:49:09'},
            {id: 2, name: 'Vessel Group B', dateCreated: '2019-10-25T02:49:09'},
            {id: 3, name: 'Vessel Group C', dateCreated: '2019-10-25T03:49:09'},
            {id: 4, name: 'Vessel Group D', dateCreated: '2019-10-25T04:49:09'},
            {id: 5, name: 'Vessel Group E', dateCreated: '2019-10-25T05:49:09'},
            {id: 6, name: 'Vessel Group F', dateCreated: '2019-10-25T06:49:09'},
            {id: 7, name: 'Vessel Group G', dateCreated: '2019-10-25T07:49:09'},
            {id: 8, name: 'Vessel Group H', dateCreated: '2019-10-25T08:49:09'},
            {id: 9, name: 'Vessel Group I', dateCreated: '2019-10-25T09:49:09'},
            {id: 10, name: 'Vessel Group J', dateCreated: '2019-10-25T10:49:09'},
            {id: 11, name: 'Vessel Group K', dateCreated: '2019-10-25T11:49:09'},
            {id: 12, name: 'Vessel Group L', dateCreated: '2019-10-25T12:49:09'}       
        ]
    });

})()

export { timelineInfoMocks }