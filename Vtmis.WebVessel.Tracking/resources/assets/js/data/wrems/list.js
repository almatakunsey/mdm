import utilHelper from '../../helpers/util'

const host = `http://${window.location.hostname}:${utilHelper.getWebAdminApiPort()}`

const wremsListMock = (()=>{

	mock.onGet(`${host}/api/services/wrems/details`)
	.reply(200,

		 // start data
		{
		  "wreckDetails": [
        {
      "id": 26258,
      "wreckNumber": 1000,
      "wreckName": "MV ARKTIS ISLAND",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nType - General Cargo\nLOA - 72.45 m\nGRT - 1510 Tons\nFlag - Denmark\nIncident - Grounding & Sinking\nCharted by symbol",
      "active": 1,
      "area": "0.66NM West Of ONE FATHOM BANK LIGHT HOUSE",
      "locking": 0,
      "objType": "COPRA COCONUT",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 2.885667,
          "longitude": 100.983333
        }
      ]
    },
    {
      "id": 26245,
      "wreckNumber": 1001,
      "wreckName": "MV Royal Pacific",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nType - Passenger Ship\nLOA - 135 m\nGRT - 13176 Tons\nBuild - 1969\nFlag - Bahamas \nIncident - Collision & Sinking\nCharted by wreck symbol",
      "active": 0,
      "area": "15.4 NM West OF TANJUNG TUAN",
      "locking": 0,
      "objType": "N/A",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 6,
      "repetitionClass": 6,
      "firstDaySurvey": "2016-01-23T00:00:00",
      "lastDaySurvey": "2016-01-23T00:00:00",
      "locations": [
        {
          "latitude": 2.456667 ,
          "longitude": 101.605000
        }
      ]
    },
    {
      "id": 26254,
      "wreckNumber": 1002,
      "wreckName": "MV ICL Vikraman",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nType - Bulk Carrier\nLOA - 198 m\nGRT - 31374 Tons\nBuild - 1979\nFlag - India\nIncident - Collision & Sinking\nunder water cutting till 23m CD and charted by wreck symbol",
      "active": 0,
      "area": "5.5 NM SOUTHERN OF TANJUNG TUAN",
      "locking": 0,
      "objType": "STEEL PRODUCT",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2016-01-22T00:00:00",
      "lastDaySurvey": "2016-01-22T00:00:00",
      "locations": [
        {
          "latitude": 2.308333 ,
          "longitude": 101.850000
        }
      ]
    },
    {
      "id": 26237,
      "wreckNumber": 1003,
      "wreckName": "MT Singapura Timur",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "2001-06-15T00:00:00",
      "description": "Ship Particulars\nType - Tanker\nLOA - 72 m\nGRT - 1369\nBuil - 1979\nFlag - Panama\nIncident - Collision & Sinking\nReport least depth 30m above the wreck and charted by wreck symbol",
      "active": 0,
      "area": "8 NM OF SUNGAI UDANG MELAKA/5 NM OF PULAU UNDAN",
      "locking": 0,
      "objType": "BITUMIN",
      "verticalAccuracy": 0.20000000298023224,
      "length": 72,
      "registeredTonnage": 1369,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 1.969500,
          "longitude": 102.235667
        }
      ]
    },
    {
      "id": 26247,
      "wreckNumber": 1004,
      "wreckName": "MV California",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nType - Bulk Carrier\nLOA - 242.02 m\nGRT 40182 Tons\nFlag - Panama\nIncident - Collision & Sinking\nCut to 31m CD (2012) and charted by wreck symbol",
      "active": 0,
      "area": "EAST BOUND TSS OFF PULAU UNDAN",
      "locking": 0,
      "objType": "IRON ORE",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2012-12-16T00:00:00",
      "lastDaySurvey": "2012-12-16T00:00:00",
      "locations": [
        {
          "latitude": 1.959833,
          "longitude": 102.175667
        }
      ]
    },
    {
      "id": 26271,
      "wreckNumber": 1005,
      "wreckName": "FERI BAHTERA MAS III",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nType - General Cargo\nBuild - 1984\nGRT - 14921 Tons",
      "active": 0,
      "area": "0.5 NM OFF KG PANTAI BARU LINGGI MELAKA",
      "locking": 0,
      "objType": "N/A",
      "verticalAccuracy": 0,
      "length": 156.6999969482422,
      "registeredTonnage": 0,
      "dateModified": "2019-06-11T00:00:00",
      "regiteredNumber": 2003,
      "year": 2019,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 2.355000,
          "longitude":102.018333
        }
      ]
    },
    {
      "id": 26275,
      "wreckNumber": 1006,
      "wreckName": "MV ASIATIC",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nGross Tonnage - 998\nLenght X Breadth - 64.8m X 10.5m\nFlag - unknown\nType of Ship - General Cargo\nYear of build -1945\nCharted by wreck symbol (Fire and sinking)",
      "active": 0,
      "area": "MALACCA STRAIT",
      "locking": 0,
      "objType": "GENERAL CARGO",
      "verticalAccuracy": 0,
      "length": 64.80000305175781,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 2.458000,
          "longitude": 100.983333
        }
      ]
    },
    {
      "id": 26328,
      "wreckNumber": 2008,
      "wreckName": "KD INDERAPURA",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nType of Ship - War Ship\nFlag - Malaysia\nFire & stranded removal operation in progress",
      "active": 0,
      "area": "Beting batu mandi Lumut Port Limit",
      "locking": 1,
      "objType": "War Ship",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude":4.272983,
          "longitude": 100.566933
        }
      ]
    },
    {
      "id": 26330,
      "wreckNumber": 2009,
      "wreckName": "MV UNION STAR 22",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nYear of build - 1989\nFlage - Indonesia\nRun aground & stranded",
      "active": 0,
      "area": "Pulau Tukun Lumut Port Limit",
      "locking": 0,
      "objType": "GENERAL CARGO",
      "verticalAccuracy": 0,
      "length": 72.5,
      "registeredTonnage": 1391,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 4.118000,
          "longitude": 100.560000
        }
      ]
    },
    {
      "id": 26332,
      "wreckNumber": 2010,
      "wreckName": "MV MALVEST 1",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nYear of Build - 1989\nFlage - Belize\nGross tonnage - 4108.04\nStranded",
      "active": 0,
      "area": "Teluk Kumbar Penang",
      "locking": 0,
      "objType": "Barge",
      "verticalAccuracy": 0,
      "length": 115.72000122070312,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 5.235000,
          "longitude":100.235000
        }
      ]
    },
    
    {
      "id": 26361,
      "wreckNumber": 3005,
      "wreckName": "MV BLISSFUL REEFER",
      "country": "Southern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship particulars\nFlag - Panama\nYear of build - N/A\n\nCharted by wreck symbol",
      "active": 0,
      "area": "East Johor",
      "locking": 0,
      "objType": "General Cargo",
      "verticalAccuracy": 0,
      "length": 151,
      "registeredTonnage": 0,
      "dateModified": "2019-03-21T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 1.455000,
          "longitude": 104.410000
        }
      ]
    },
    {
      "id": 26363,
      "wreckNumber": 3006,
      "wreckName": "MV EVERISE GLORY",
      "country": "Southern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship particulars\nFlag - Malaysia\n\nMark by an isolated danger bouy",
      "active": 0,
      "area": "East Johor",
      "locking": 0,
      "objType": "General Cargo",
      "verticalAccuracy": 0,
      "length": 151,
      "registeredTonnage": 0,
      "dateModified": "2019-03-21T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 1.422833,
          "longitude": 104.490000
        }
      ]
    },
    {
      "id": 26368,
      "wreckNumber": 3007,
      "wreckName": "MT POLAI",
      "country": "Southern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A",
      "active": 0,
      "area": "Singapore strait at eastern approaches",
      "locking": 1,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 1.294667,
          "longitude": 104.178000
        }
      ]
    },
    {
      "id": 104654,
      "wreckNumber": 4000,
      "wreckName": "MARITIME FIDELITY",
      "country": "Eastern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A\nWire sweep to 23m CD and Charted By wreck",
      "active": 0,
      "area": "Off Pulau Tioman",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 0,
      "year": 0,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "0001-01-01T00:00:00",
      "lastDaySurvey": "0001-01-01T00:00:00",
      "locations": [{
        "latitude": 1.716667,
        "longitude": 104.589667
      }]
    },
    {
      "id": 104655,
      "wreckNumber": 4001,
      "wreckName": "MV UNION STAR",
      "country": "Eastern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A\nCharted by wreck symbol (Run Aground)",
      "active": 0,
      "area": "Terengganu",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 0,
      "year": 0,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "0001-01-01T00:00:00",
      "lastDaySurvey": "0001-01-01T00:00:00",
      "locations": [
        {
          "latitude": 6.266667,
          "longitude": 102.250000
        }
      ]
    },
    
    {
      "id": 104657,
      "wreckNumber": 4003,
      "wreckName": "MEISUNG G7",
      "country": "Eastern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nType of ship - Barge",
      "active": 0,
      "area": "Kuantan port approaches",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 0,
      "year": 0,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "0001-01-01T00:00:00",
      "lastDaySurvey": "0001-01-01T00:00:00",
      "locations": [
        {
          "latitude": 3.957167,
          "longitude": 103.457167
        }
      ]
    },
    {
      "id": 104658,
      "wreckNumber": 4004,
      "wreckName": "AIR MAS",
      "country": "Eastern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nFlag - Singapore\n\nStanded & Abondon",
      "active": 0,
      "area": "Tanjung Gemok Rompin",
      "locking": 0,
      "objType": "Barge",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 2132,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 0,
      "year": 0,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "0001-01-01T00:00:00",
      "lastDaySurvey": "0001-01-01T00:00:00",
      "locations": [
        {
          "latitude":2.691300,
          "longitude": 103.610600
        }
      ]
    },
    {
      "id": 26294,
      "wreckNumber": 1010,
      "wreckName": "MV GOLDENROD",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A",
      "active": 0,
      "area": "Sepang Selangor",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 2.593333,
          "longitude": 101.475000
        }
      ]
    },
    {
      "id": 104676,
      "wreckNumber": 5001,
      "wreckName": "MV SENG SOON CHEW",
      "country": "Sarawak Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nFlag - Unknown\nType of ship - Unknown\nYear of build - Unknown\nCharted by wreck symbol",
      "active": 0,
      "area": "Sarawak",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 97.80000305175781,
      "registeredTonnage": 3260,
      "dateModified": "2019-02-20T00:00:00",
      "regiteredNumber": 0,
      "year": 0,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "0001-01-01T00:00:00",
      "lastDaySurvey": "0001-01-01T00:00:00",
      "locations": [
        {
          "latitude": 3.366667,
          "longitude": 111.833333
        }
      ]
    },
    
    {
      "id": 26281,
      "wreckNumber": 1007,
      "wreckName": "FAJAR SAMUDERA",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nCall Sign - 9MEO2\nGross tonnage - 4476\nDWT - 471\nLenght X Breadth - 89.4m X 15.6m\nType of Ship - Passenger/Ro-Ro Cargo\nYear of Build - 1987",
      "active": 0,
      "area": "DEEP WATER POINT NORTH PORT KLANG",
      "locking": 0,
      "objType": "Ro-Ro Cargo",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude":3.070833,
          "longitude": 101.339333
        }
      ]
    },
    {
      "id": 26286,
      "wreckNumber": 1008,
      "wreckName": "MV BANGA BIRAJ",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nFlag - Bangladesh\nType - General Cargo\nIMO - 8100686\nMMSI - 405000034\nCall Sign - S2BE\nGross Tonnage - 8350",
      "active": 0,
      "area": "BEACHED AT SUNGAI PULOH PORT KLANG",
      "locking": 0,
      "objType": "GENERAL CARGO",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude":3.063167,
          "longitude":101.350000
        }
      ]
    },
    {
      "id": 26290,
      "wreckNumber": 1009,
      "wreckName": "MV JENWIN",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nGross Tonnage - 1283\nLenght X Breadth - 74.7m\nFlag - N/A\nType Of Ship - General Cargo\nYear of build - N/A",
      "active": 0,
      "area": "MALACCA STRAIT",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 74.69999694824219,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 2.268000,
          "longitude": 101.796333
        }
      ]
    },
    {
      "id": 26306,
      "wreckNumber": 2000,
      "wreckName": "MV HOCK HAI LEE",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship particulars\nGross tonnage - 892\nLenght x breadth - 65.5m x 10.1m\nFlage - Unknown\nType of ship - General cargo\nYear of build - Unknown\nPartial Lifted Charted by wreck Symbol",
      "active": 0,
      "area": "OFF TANJUNG GELANG",
      "locking": 0,
      "objType": "GENERAL CARGO",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2019,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 3.255556,
          "longitude": 103.450000
        }
      ]
    },
    {
      "id": 26308,
      "wreckNumber": 2001,
      "wreckName": "MV RICO",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nGross tonnage - 6703\nLenght x breadth - 131.7m x 19.2m\nFlag - Unknown\nType of Ship - General Cargo\nCharted by wreck Symbol",
      "active": 0,
      "area": "OFF TANJUNG GELANG",
      "locking": 0,
      "objType": "GENERAL CARGO",
      "verticalAccuracy": 0,
      "length": 131.6999969482422,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2019,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 3.253333,
          "longitude": 103.450000
        }
      ]
    },
    {
      "id": 26310,
      "wreckNumber": 2002,
      "wreckName": "MV SS SUN VISTA",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nGross tonnage - 28083\nFlag - Bahamas\nYear of build - 1961\nType of ship - Ocean Liner\nCharted by wreck symbol (fire & sinking) (tendering process)",
      "active": 0,
      "area": "BRG 299 x 46.5NM OFF PULAU PANGKOR",
      "locking": 0,
      "objType": "OCEAN LINER",
      "verticalAccuracy": 0,
      "length": 213.64999389648438,
      "registeredTonnage": 28083,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2019,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 4.603333,
          "longitude": 99.870000
        }
      ]
    },
    {
      "id": 26314,
      "wreckNumber": 2003,
      "wreckName": "MV HAI SIN",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nType of ship - Tug Boat",
      "active": 0,
      "area": "Langkawi",
      "locking": 0,
      "objType": "Tug Boat",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2019,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude":5.752500,
          "longitude": 100.234500
        }
      ]
    },
    
    {
      "id": 26319,
      "wreckNumber": 2005,
      "wreckName": "MV SELAMA ABADI",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship particulars\nN/A\nCharted by symbol",
      "active": 0,
      "area": "South of langkawi",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2019,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 5.933333,
          "longitude": 99.811000
        }
      ]
    },
    {
      "id": 26322,
      "wreckNumber": 2006,
      "wreckName": "UNKNOWN",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nType of ship - Barge\nSinking 8m CD",
      "active": 0,
      "area": "Sg Dinding Lumut (Marina)",
      "locking": 0,
      "objType": "Barge",
      "verticalAccuracy": 0,
      "length": 22,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 6,
      "repetitionClass": 6,
      "firstDaySurvey": "2013-11-13T00:00:00",
      "lastDaySurvey": "2013-11-13T00:00:00",
      "locations": [
        {
          "latitude": 4.238500,
          "longitude": 100.637667
        }
      ]
    },
    
    {
      "id": 26252,
      "wreckNumber": 3001,
      "wreckName": "MV B Oceania",
      "country": "Southern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nFlag - Malta \nYear of build - 1989\nTotally remove (July 2013) collision and sinking",
      "active": 0,
      "area": "8NM SW Off Pulau Pisang",
      "locking": 0,
      "objType": "Bulk Carrier",
      "verticalAccuracy": 0,
      "length": 230,
      "registeredTonnage": 38337,
      "dateModified": "2019-06-11T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2013-07-16T00:00:00",
      "lastDaySurvey": "2013-07-16T00:00:00",
      "locations": [
        {
          "latitude":1.414833,
          "longitude": 103.128833
        }
      ]
    },
    {
      "id": 26347,
      "wreckNumber": 3002,
      "wreckName": "UNKNOWN",
      "country": "Southern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A",
      "active": 0,
      "area": "Near Tg Piai Beacon",
      "locking": 0,
      "objType": "Steel Ship",
      "verticalAccuracy": 0,
      "length": 54,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude":1.241667,
          "longitude": 103.491667
        }
      ]
    },
    {
      "id": 26350,
      "wreckNumber": 3003,
      "wreckName": "MV KHANH DUONG",
      "country": "Southern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A",
      "active": 0,
      "area": "Johor",
      "locking": 0,
      "objType": "Sand Dredger",
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude":1.421067,
          "longitude": 104.368200
        }
      ]
    },
   
    {
      "id": 104663,
      "wreckNumber": 4005,
      "wreckName": "MV EURASIA 21",
      "country": "Eastern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship particulaes\nN/A",
      "active": 0,
      "area": "Kuala Terengganu",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-20T00:00:00",
      "regiteredNumber": 0,
      "year": 0,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "0001-01-01T00:00:00",
      "lastDaySurvey": "0001-01-01T00:00:00",
      "locations": [
        {
          "latitude": 5.358333,
          "longitude": 103.151667
        }
      ]
    },
    {
      "id": 26372,
      "wreckNumber": 3008,
      "wreckName": "MV TIRTA MAS",
      "country": "Southern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship particulaes\nN/A",
      "active": 0,
      "area": "Tanjung Piai",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 1.246667,
          "longitude": 103.421667
        }
      ]
    },
    
    {
      "id": 26384,
      "wreckNumber": 3010,
      "wreckName": "MV ARA SINGA",
      "country": "Southern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Pariculars\nN/A",
      "active": 0,
      "area": "Teluk Punggai",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [ 
        {
        "latitude":1.430000,
        "longitude":  104.350000
      }
    ]
    },
    {
      "id": 26388,
      "wreckNumber": 3011,
      "wreckName": "MV SPHINX U",
      "country": "Southern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A",
      "active": 0,
      "area": "Tanjung Ayam",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude":1.324667,
          "longitude": 104.202000
        }
      ]
    },
    {
      "id": 26392,
      "wreckNumber": 3012,
      "wreckName": "PRESIDENT EISENHOWER",
      "country": "Southern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A",
      "active": 0,
      "area": "Tanjung Piai",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 1.262333,
          "longitude": 103.515500
        }
      ]
    },
    {
      "id": 26298,
      "wreckNumber": 1011,
      "wreckName": "TUG BOT LABU",
      "country": "Central Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A",
      "active": 0,
      "area": "0.5NM off Pantai Baru Linggi Melaka",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 2.355000,
          "longitude":102.018333
        }
      ]
    },
    {
      "id": 26338,
      "wreckNumber": 2012,
      "wreckName": "MV IMPIANA 1",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A",
      "active": 0,
      "area": "Kuala Kedah",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-06-12T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "2014-04-16T00:00:00",
      "lastDaySurvey": "2014-04-16T00:00:00",
      "locations": [
        {
          "latitude": 6.093333,
          "longitude":100.258333
        }
      ]
    },
    {
      "id": 104674,
      "wreckNumber": 5000,
      "wreckName": "MV SRI ARIKA",
      "country": "Sarawak Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nN/A",
      "active": 0,
      "area": "Tanjung Ensurai Sarawak",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 0,
      "registeredTonnage": 0,
      "dateModified": "2019-02-20T00:00:00",
      "regiteredNumber": 0,
      "year": 0,
      "nextResearch": 0,
      "repetitionClass": 0,
      "firstDaySurvey": "0001-01-01T00:00:00",
      "lastDaySurvey": "0001-01-01T00:00:00",
      "locations": [
        {
          "latitude": 2.259667,
          "longitude": 111.803000
        }
      ]
    },
   
    {
      "id": 26325,
      "wreckNumber": 2007,
      "wreckName": "UNKNOWN",
      "country": "Northern Region",
      "office": "Malaysia",
      "objAcronym": "WRECKS",
      "wreckType": "WRECKS",
      "dateWreck": "0001-01-01T00:00:00",
      "description": "Ship Particulars\nType of ship - Barge\nSinking 6.5m CD",
      "active": 0,
      "area": "Sungai Sitiawan",
      "locking": 0,
      "objType": null,
      "verticalAccuracy": 0,
      "length": 28,
      "registeredTonnage": 0,
      "dateModified": "2019-02-19T00:00:00",
      "regiteredNumber": 2003,
      "year": 2003,
      "nextResearch": 6,
      "repetitionClass": 6,
      "firstDaySurvey": "2013-11-13T00:00:00",
      "lastDaySurvey": "2013-11-13T00:00:00",
      "locations": [
        {
          "latitude": 4.269000,
          "longitude": 100.664667
        }
      ]
    }

    
    ],
		  "pageSize": 40,
		  "pageNumber": 1,
		  "total": 40
		}
		 // end data
	)

})()

export { wremsListMock }