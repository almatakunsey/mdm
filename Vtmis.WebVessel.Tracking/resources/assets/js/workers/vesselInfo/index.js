import Worker from './vesselInfo.worker'

import { Debounce } from 'lodash-decorators'

const w = w || new Worker();

w.onmessage = function(event) {  
  	const eventName = typeof event.data == 'object' ? event.data[0] : event.data
    const payload = typeof event.data == 'object' ? event.data[1] : null

    // switch (eventName) {
    //     case "Db.Routes.Update":
    //     {
    //     	Event.fire("Worker.Db.Routes.Update", payload)
    //     }
    //     break;

    //     case "Db.Routes.Update.Completed":
    //     {
    //     	Event.fire('Worker.Db.Routes.Update.Completed')
    //     }
    //     break;
    // }


};

const VesselInfoWorker = {
	
	initDb(){
		w.postMessage("init")
	},

    @Debounce(500)
	updateVesselInfo(vesselPositions){
		w.postMessage(['updateVesselInfo', {vesselPositions, host:window.location.hostname}])
	}
}

export {
	VesselInfoWorker
}

