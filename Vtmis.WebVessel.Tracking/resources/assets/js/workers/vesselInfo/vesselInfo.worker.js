import axios from 'axios'
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Content-Type'] = 'application/json' 
const _ = require('lodash')
import async from 'async'

import utilHelper from '../../helpers/util'

self.onmessage = function(event) {    

    const eventName = typeof event.data == 'object' ? event.data[0] : event.data
    const payload = typeof event.data == 'object' ? event.data[1] : null
    
    switch (eventName) {

        case 'updateVesselInfo':
        {            
        
            const {vesselPositions, host} = payload
            const mmsis = _.map(vesselPositions, p=> p.mmsi )            
        
          
            function handleSuccess(results){
                console.log(results)
            }         

            const vesselInfoGetterFunc = mmsis.map((vesselId)=>{
                return (callback)=>{
                    
                    axios.get(`http://${host}:${utilHelper.getMapTrackingApiPort()}/api/VesselFilters/GetTargetInfoByMmsi/?mmsi=${vesselId}`).then(
                        ({data})=>{
                           callback(null, data)
                        },
                        (error)=>{
                           callback(error)
                        }
                    )
                }
            })

            async.parallel(vesselInfoGetterFunc, (error, result) => {
                if(error){                    
                    console.error(error)
                }
                else{
                    // console.log(result)                                        
                    // handleSuccess(result.map( re => re.result  ))
                }
            })   

            // axios.post(`http://${host}/api/VesselRouteHistory:${port}`, mmsis)
            // .then(({data})=>{

            //     let req = indexedDB.open("mdm");
            //     let db

            //     req.onsuccess = function(e) {

            //         db = req.result                    

            //         let tx = db.transaction("routes", "readwrite");                
            //         let store = tx.objectStore("routes")                    
                
            //         _.each(data, ({vesselId, vesselRoutePosition})=>{
            //             isExistOrEqual(store, vesselId, vesselRoutePosition).then(({isExist, isEqual})=>{
            //                 if( !isExist || (isExist && !isEqual) ){
            //                     store.put({mmsi: vesselId, positions: vesselRoutePosition})
            //                     self.postMessage(["Db.Routes.Update", vesselId])
            //                 }
            //             })                        
            //         })
                    
            //         tx.oncomplete = function(){                        
            //             db.close()
            //             self.postMessage("Db.Routes.Update.Completed")
            //         }     
            //     }                                 
            // })
            // .catch((error)=>{
            //     console.error(error)
            // })

            
        }
        break;

    }
};