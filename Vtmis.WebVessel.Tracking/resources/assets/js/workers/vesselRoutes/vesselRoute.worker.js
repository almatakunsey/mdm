import axios from 'axios'
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['Content-Type'] = 'application/json' 
const _ = require('lodash')

import utilHelper from '../../helpers/util'


self.onmessage = function(event) {    

    const eventName = typeof event.data == 'object' ? event.data[0] : event.data
    const payload = typeof event.data == 'object' ? event.data[1] : null

    
    switch (eventName) {        

        case 'updateRoutes':
        {            
        
            const {vesselPositions, host} = payload
            const mmsis = _.map(vesselPositions, p=> p.mmsi )

           

            function isExistOrEqual(store, mmsi, vesselRoutePosition){
                return new Promise((resolve, reject)=>{
                    const storeRequest = store.get(mmsi)
                    storeRequest.onsuccess = function(){                    
                        const isExist = storeRequest.result ? true : false
                        const isEqual = isExist ? utilHelper.isArrayObjectEqual(vesselRoutePosition, storeRequest.result.positions) : false
                        resolve({isExist, isEqual})
                    }
                })            
            }

            const port = utilHelper.getMapPortalApiPort() 

            axios.post(`http://${host}${port ? ':'+port : ''}/api/VesselRouteHistory`, mmsis)
            .then(({data})=>{

                let req = indexedDB.open("mdm");
                let db

                req.onsuccess = function(e) {

                    db = req.result                    

                    let tx = db.transaction("routes", "readwrite");                
                    let store = tx.objectStore("routes")                    
                
                    _.each(data, ({vesselId, vesselRoutePosition})=>{
                        isExistOrEqual(store, vesselId, vesselRoutePosition).then(({isExist, isEqual})=>{
                            if( !isExist || (isExist && !isEqual) ){
                                store.put({mmsi: vesselId, positions: vesselRoutePosition})
                                self.postMessage(["Db.Routes.Update", vesselId])
                            }
                        })                        
                    })
                    
                    tx.oncomplete = function(){                        
                        db.close()
                        self.postMessage("Db.Routes.Update.Completed")
                    }     
                }                                 
            })
            .catch((error)=>{
                console.error(error)
            })

            
        }
        break;

    }
};