import Worker from './vesselRoute.worker'

const w = w || new Worker();

w.onmessage = function(event) {  
  	const eventName = typeof event.data == 'object' ? event.data[0] : event.data
    const payload = typeof event.data == 'object' ? event.data[1] : null

    switch (eventName) {
        case "Db.Routes.Update":
        {
        	Event.fire("Worker.Db.Routes.Update", payload)
        }
        break;

        case "Db.Routes.Update.Completed":
        {
        	Event.fire('Worker.Db.Routes.Update.Completed')
        }
        break;
    }


};

const VesselRouteWorker = {	

	updateRoutes(vesselPositions){
		w.postMessage(['updateRoutes', {vesselPositions, host:window.location.hostname}])
	}
}

export {
	VesselRouteWorker
}

