import Worker from './userlist.worker'

const w = w || new Worker();

w.onmessage = function(event) {  
  	const eventName = typeof event.data == 'object' ? event.data[0] : event.data
    const payload = typeof event.data == 'object' ? event.data[1] : null

    switch (eventName) {
        case "Users.GroupByVessel":
        {
        	Event.fire("Users.GroupByVessel", payload)
        }
        break;  

        case "Users.GroupByTenant":
        {
            Event.fire("Users.GroupByTenant", payload)   
        }
    }


};

const UserListWorker = {	

	groupByVessel(users){
		w.postMessage(['groupByVessel', {users}])
	},

    groupByTenant(users){
        w.postMessage(['groupByTenant', {users}])  
    }
}

export {
	UserListWorker
}

