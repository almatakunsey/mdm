// import axios from 'axios'
// axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
// axios.defaults.headers.common['Content-Type'] = 'application/json' 
const _ = require('lodash')

import utilHelper from '../../helpers/util'


self.onmessage = function(event) {    

    const eventName = typeof event.data == 'object' ? event.data[0] : event.data
    const payload = typeof event.data == 'object' ? event.data[1] : null

    
    switch (eventName) {        

        case 'groupByVessel':
        {            

            const {users} = payload

            let vessels = []
            
            // collect all vessels
            users.forEach((user)=>{
                if(user.vessels){
                    user.vessels.forEach((vessel)=>{
                        vessels.push(vessel)
                    })  
                }               
            })  

            // add a special vessel to store users with no vessels assigned
            vessels.push({mmsi: 'Unassigned', vesselName:'Unassigned'})

            // remove duplicate vessel
            vessels = _.uniqWith(vessels, (a, b)=> a.mmsi == b.mmsi && a.vesselName == b.vesselName )           

            // collect user which has the vessels assigned
            vessels.forEach((vessel)=>{
                // has vessel assingned
                vessel['users'] = users.filter( user => _.some(user.vessels, {mmsi:vessel.mmsi} ) )
            })

            // collect users with no vessels
            _.find(vessels, {vesselName:'Unassigned'})['users'] = users.filter( user => _.isArray(user.vessels) && !user.vessels.length)                         
        
           self.postMessage(["Users.GroupByVessel", vessels])
        }
        break;

        case 'groupByTenant':
        {

            const {users} = payload

            let tenants = []

            // collect all tenants
            users.forEach(({tenantId, tenantName})=>{
                tenants.push({tenantId, tenantName})            
            })          

            // remove duplicate teants
            tenants = _.uniqWith(tenants, (a, b)=> a.tenantId == b.tenantId && a.tenantName == b.tenantName )           

            tenants.forEach((tenant)=>{
                tenant['users'] = users.filter( user => user.tenantName == tenant.tenantName )
            })

            self.postMessage(["Users.GroupByTenant", tenants])                      
        }
        break;

    }
};