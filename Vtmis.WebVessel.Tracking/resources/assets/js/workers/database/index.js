import Worker from './database.worker'

const w = w || new Worker();

w.onmessage = function(event) {  
  	const eventName = typeof event.data == 'object' ? event.data[0] : event.data
    const payload = typeof event.data == 'object' ? event.data[1] : null
};

const DatabaseWorker = {
	
	initDb(){
		w.postMessage("init")
	}	
}

export {
	DatabaseWorker
}

