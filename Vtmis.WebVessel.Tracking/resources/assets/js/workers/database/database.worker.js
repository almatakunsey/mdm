self.onmessage = function(event) {    

    const eventName = typeof event.data == 'object' ? event.data[0] : event.data
    const payload = typeof event.data == 'object' ? event.data[1] : null
    
    switch (eventName) {
        case "init":
        {                            

            let req = indexedDB.open("mdm", 2);
            let db;
            let objectStore;

            req.onupgradeneeded = function(e) {
                
                console.log("A Db Upgrade Is Needed")                          
                
                db = req.result; 
                
                try{
                     db.deleteObjectStore("routes");                                      
                }
                catch(e){                    
                    // console.error(e.message)
                }

                // routes {mmsi, lat, lng} 
                db.createObjectStore("routes", { keyPath: "mmsi" });
                db.createObjectStore("vesselinfo", { keyPath: "mmsi" });
                                             
            };

            req.onsuccess = function(e) {                 
                console.log("Successfully connect db")
                db = req.result;                
            };

            req.onerror = function(e) {
                console.log("Error connect db")                                                
                console.log(e.message)
            };
        }
        break;       

    }
};