﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Vtmis.WebVessel.Tracking.Controllers
{
    public class SignalRStartupController : Controller
    {
        private readonly IConfiguration config;

        public SignalRStartupController(IConfiguration config)
        {
            this.config = config;
        }

        public IActionResult Index()
        {
            //ViewBag.ApiUrl = config["ApiUrl"];
            return View();
        }


        public IActionResult Event()
        {

            return View();
        }


        public IActionResult Replay()
        {
            return View();
        }
    }
}