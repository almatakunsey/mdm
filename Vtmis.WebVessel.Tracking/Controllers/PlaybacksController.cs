﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Flurl;
using Flurl.Http;

namespace Vtmis.WebVessel.Tracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlaybacksController : ControllerBase
    {
        public PlaybacksController()
        {

        }

        [HttpGet]
        public async Task<IActionResult> GetVesselTimeline(string url,int mmsi,DateTime fromDateTime,DateTime toDateTime)
        {
            string _url = "http://" + url;
          
            dynamic d = await _url
                .SetQueryParam("mmsi",mmsi)
                .SetQueryParam("fromDateTime",fromDateTime)
                .SetQueryParam("toDateTime",toDateTime)
                .GetJsonAsync();

            return Ok(d);
        }
    }
}