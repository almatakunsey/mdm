﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;

namespace Vtmis.WebVessel.Tracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VesselRouteHistoryController : Controller
    {            
        private readonly ICacheService _cacheService;

        public VesselRouteHistoryController(ICacheService cacheService)
        {           
            _cacheService = cacheService;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]List<int> vesselIds)
        {
            var vtStorage = await _cacheService.GetAllVesselTrack();
           
            if (vtStorage.Any())
            {
                return Ok(vtStorage.Where(c => vesselIds.Any(m => m.ToString() == c.VesselId)));
            }
            return Ok(vtStorage);
        }
    }


}