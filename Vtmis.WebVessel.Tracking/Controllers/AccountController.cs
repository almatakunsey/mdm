﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Vtmis.WebAdmin.Models.TokenAuth;
using Abp.MultiTenancy;
using Abp.Authorization.Users;
using Vtmis.Core.Model.ViewModels.Account;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.WebVessel.Tracking.Controllers
{

    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;
        private readonly IAccountService _accountService;
        private readonly IConfiguration _configuration;

        public AccountController(ILogger<AccountController> logger, IAccountService accountService, IConfiguration configuration)
        {
            _logger = logger;
            _accountService = accountService;
            _configuration = configuration;
        }
        public IActionResult Login()
        {
            return View();
        }

        public async Task<IActionResult> Logout()
        {

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            Response.Cookies.Delete("token");
            return RedirectToAction("Login");
        }

        /// <summary>
        /// To authenticate using JWT Token we need to install JWT package for asp.net core
        /// 1. Install-Package Microsoft.Owin.Security.Jwt
        /// 2. Configure in 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Login(string userName, string passWord,string tenantName, string returnUrl)
        {
            _logger.LogWarning($"Logging user with username: {userName} and {passWord}");
            if (string.IsNullOrEmpty(tenantName)
                && userName.ToLower() == AbpUserBase.AdminUserName.ToLower())
            {
                tenantName = AbpTenantBase.DefaultTenantName;
            }
            var authResult = _accountService.RequestToken(AppHelper.GetJwtIssuerUri(), userName, tenantName, passWord);


            if (authResult.Result != null)
            {
                var isTokenValid = _accountService.ValidateToken(authResult.Result.Result.AccessToken);
                
                if (isTokenValid)
                {
                    _logger.LogWarning($"Login token valid");
                    Response.Cookies.Append("token", authResult.Result.Result.AccessToken);

                    var principal = _accountService.GetLoginClaimsPrincipal(userName, authResult.Result.Result.UserId);

                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        TempData["userId"] = authResult.Result.Result.UserId;
                         
                        return RedirectToAction("Maps", "Dashboard");
                    }

                    return Redirect(returnUrl);
                }
                _logger.LogError($"Login token invalid");
            }

            return View();
        }

        [HttpPost]
        public async Task<AbpTokenRequestResponse> Authenticate([FromBody]AuthenticateModel model)
        {
            _logger.LogWarning($"Logging user with username: {model.UserNameOrEmailAddress} and {model.Password}");
            if (string.IsNullOrEmpty(model.TenantName) 
                && model.UserNameOrEmailAddress.ToLower() == AbpUserBase.AdminUserName.ToLower())
            {
                model.TenantName = AbpTenantBase.DefaultTenantName;
            }
            var authResult = await _accountService.RequestToken(AppHelper.GetJwtIssuerUri(), 
                model.UserNameOrEmailAddress, model.TenantName, model.Password);

            if (authResult.Result != null)
            {
                var isTokenValid = _accountService.ValidateToken(authResult.Result.AccessToken);

                if (isTokenValid)
                {
                    _logger.LogWarning($"Login token valid");
                    Response.Cookies.Append("token", authResult.Result.AccessToken);

                    var principal = _accountService.GetLoginClaimsPrincipal(model.UserNameOrEmailAddress,
                        authResult.Result.UserId);

                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                    return authResult;
                }
                _logger.LogError($"Login token invalid");
            }
            return null;
        }
    }
}
