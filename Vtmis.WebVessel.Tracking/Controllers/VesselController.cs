﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using System.Linq;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Model.ViewModels.Target;
using Vtmis.Core.Model.ViewModels.Target.Vessel;
using System.Threading.Tasks;
using System;
using System.Security.Claims;
using Vtmis.WebAdmin.EICS;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;

namespace Vtmis.WebVessel.Tracking.Controllers
{
    [Route("api/[controller]/[action]")]
    public class VesselController : Controller
    {
        private readonly IVesselService _vesselService;
        private MapTransactionSpecification _mapTransaction;
        private readonly IDapperAdmin _dapperAdmin;
        private readonly IMapper _mapper;
        public VesselController(IVesselService vesselService, MapTransactionSpecification mapTransaction, IDapperAdmin dapperAdmin)
        {
            _vesselService = vesselService;
            _mapTransaction = mapTransaction;
            _dapperAdmin = dapperAdmin;
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ShipPosition, ShipPositionViewModel>().ReverseMap();
                cfg.CreateMap<ShipStatic, ShipStaticViewModel>().ReverseMap();
            });
            _mapper = config.CreateMapper();
        }

        [HttpPost]
        public async Task<IActionResult> Search([FromBody]AdvancedSearchViewModel advancedSearch)
        {
            var userId = User?.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            //var history = _mapTransaction.HistoricalVesselPosition.ToList();
            if (string.IsNullOrWhiteSpace(advancedSearch.TargetCallSign) == false)
            {
                advancedSearch.TargetCallSign = advancedSearch.TargetCallSign.Trim();
            }

            if (string.IsNullOrWhiteSpace(advancedSearch.TargetName) == false)
            {
                advancedSearch.TargetName = advancedSearch.TargetName.Trim();
            }
            var targetOwner = await _dapperAdmin.GetTargetsByUserId(userId.ToInt());
            var result = await _vesselService.AdvancedSearch(advancedSearch, targetOwner.IsHost, targetOwner.TargetIdentifiers.Select(s => s.MMSI));
            return Ok(result);
        }

        public IActionResult GetRoute()
        {
            var result = _mapTransaction.VesselRoutes;
            return Ok(result);
        }

        public IActionResult GetVesselInfo(int id)
        {
            var result = new
            {
                historical = _mapTransaction.HistoricalVesselPosition.FirstOrDefault(x => x.VesselId == id.ToString()),
                routes = _mapTransaction.VesselRoutes.FirstOrDefault(x => x.VesselId == id.ToString()),
                targetImage = _vesselService.GetTargetImage(id, ImageSize.Medium, ImageSize.Medium)
            };
            return Ok(result);
        }

        public IActionResult GetVesselImage(int id)
        {
            return Ok(_vesselService.GetTargetImage(id, ImageSize.Medium, ImageSize.Medium));
        }

    }
}
