﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using Flurl.Http;

namespace Vtmis.WebVessel.Tracking.Controllers
{
    public class VesselOwnedsController : Controller
    {
        public const string _apiWebHost = "http://vtmis_webadmin_web_host";

        public VesselOwnedsController(IHostingEnvironment hostingEnvironment)
        {
            HostingEnvironment = hostingEnvironment;

            
        }

        public IHostingEnvironment HostingEnvironment { get; }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Upload()
        {
            IFormFile file = Request.Form.Files[0];
            string folderName = "Upload";
            string rootFolder = HostingEnvironment.WebRootPath;
            string newPath = Path.Combine(rootFolder, folderName);


            StringBuilder sb = new StringBuilder();
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }

            if (file.Length > 0)
            {
                FileInfo fileUpload = new FileInfo(Path.Combine(newPath, file.Name));

                using (var stream = new FileStream(fileUpload.DirectoryName+"\\"+file.Name, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                
                using (ExcelPackage package = new ExcelPackage(fileUpload))
                {
                    ExcelWorksheet workSheet = package.Workbook.Worksheets["Sheet1"];
                    int totalRows = workSheet.Dimension.Rows;

                    List<VesselOwned> customerList = new List<VesselOwned>();

                    for (int i = 2; i <= totalRows; i++)
                    {
                        customerList.Add(new VesselOwned
                        {
                            UserId = workSheet.Cells[i, 1].Value.ToString(),
                            MMSI = Int32.Parse(workSheet.Cells[i, 2].Value.ToString())
                           
                        });
                    }

                    var useridgroup = customerList.GroupBy(c => c.UserId);
                    List<int> ints = new List<int>();

                    string _webHost = Environment.GetEnvironmentVariable("API_WEBHOST");

                    foreach (var uid in useridgroup)
                    {
                        var mmsis = customerList.Where(c => c.UserId == uid.Key).ToList();

                        var url = $"{_webHost}/api/services/app/VesselOwnedService/CreateAsync";

                       
                        //url.WithHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjEiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiYWRtaW4iLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6ImU3MDgxMTc3LWY0MTUtNDUzMy04OTJhLTJhNmQyNTZjMTcwYiIsImh0dHA6Ly9zY2hlbWFzLm1pY3Jvc29mdC5jb20vd3MvMjAwOC8wNi9pZGVudGl0eS9jbGFpbXMvcm9sZSI6IkFkbWluIiwic3ViIjoiMSIsImp0aSI6IjU5MTY0MTk2LWVlMzMtNDZmNS04ZDRmLTUzNGE3MWQ3Y2NiNiIsImlhdCI6MTU0MTkyMTIwNCwibmJmIjoxNTQxOTIxMjA0LCJleHAiOjE1NDIwMDc2MDQsImlzcyI6IldlYkFkbWluIiwiYXVkIjoiV2ViQWRtaW4ifQ.PZLVzoOm8uaHncnnBjEfsGG4BcZjZRQKqDMmF4TmICE");

                        foreach (var n in mmsis)
                        {
                            ints.Add(n.MMSI);
                        }

                        await url.PostJsonAsync(new { UserId = uid.Key, MMSI = ints });
                    }

                    return Ok("Success");
                }
            }

            return BadRequest("Invalid file upload");

        }

        public class VesselOwned
        {
            public string UserId { get; set; }
            public int MMSI { get; set; }
         
        }
    }
}