using Microsoft.Extensions.Configuration;
using Castle.MicroKernel.Registration;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Vtmis.WebAdmin.Configuration;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Vtmis.WebAdmin.Migrator.DependencyInjection;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.WebAdmin.Migrator
{
    [DependsOn(typeof(WebAdminEntityFrameworkModule))]
    public class WebAdminMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public WebAdminMigratorModule(WebAdminEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(WebAdminMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            //Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
            //    WebAdminConsts.ConnectionStringName
            //);
            Configuration.DefaultNameOrConnectionString = AppHelper.GetMdmDbConnectionString();
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(typeof(IEventBus), () =>
            {
                IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                );
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WebAdminMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
