
#Start Publish========================================================================================
dotnet publish Vtmis.WebAdmin.Web.Host/Vtmis.WebAdmin.Web.Host.csproj -c Release -o bin/publish_staging

#dotnet publish Vtmis.WebAdmin.Web.Mvc/Vtmis.WebAdmin.Web.Mvc.csproj -c Release -o bin/publish_staging

#dotnet publish Vtmis.Cluster.Seed/Vtmis.Cluster.Seed.csproj -c Release -o bin/publish_staging

#dotnet publish Vtmis.Cluster.VesselQuery/Vtmis.Cluster.VesselQuery.csproj -c Release -o bin/publish_staging

#dotnet publish Vtmis.Cluster.VesselApi/Vtmis.Cluster.VesselApi.csproj -c Release -o bin/publish_staging

#dotnet publish Vtmis.Api.MapTracking/Vtmis.Api.MapTracking.csproj -c Release -o bin/publish_staging

#dotnet publish Vtmis.WebVessel.Tracking/Vtmis.WebVessel.Tracking.csproj -c Release -o bin/publish_staging

#dotnet publish Vtmis.Api.MMDIS/Vtmis.Api.MMDIS.csproj -c Release -o bin/publish_staging

#dotnet publish Vtmis.Cluster.EventListener/Vtmis.Cluster.EventListener.csproj -c Release -o bin/publish_staging


#Start Build Container Images========================================================================
docker-compose -f "docker-compose-swarmbuild-local.yml" build

#dotnet ef database update -c webadmindbcontext

#Start Tag===========================================================================================
docker tag vtmis.webadmin.web.host.local 192.168.0.103:5000/vtmis.webadmin.web.host.local

docker tag vtmis.webadmin.web.mvc.local 192.168.0.103:5000/vtmis.webadmin.web.mvc.local

docker tag vtmis.cluster.seed.local 192.168.0.103:5000/vtmis.cluster.seed.local

docker tag vtmis.api.mmdis.local 192.168.0.103:5000/vtmis.api.mmdis.local

docker tag vtmis.cluster.vesselquery.local 192.168.0.103:5000/vtmis.cluster.vesselquery.local

docker tag vtmis.api.maptracking.local 192.168.0.103:5000/vtmis.api.maptracking.local

docker tag vtmis.cluster.api.local 192.168.0.103:5000/vtmis.cluster.api.local

docker tag vtmis.cluster.eventlistener.local 192.168.0.103:5000/vtmis.cluster.eventlistener.local

docker tag vtmis.webvessel.tracking.local 192.168.0.103:5000/vtmis.webvessel.tracking.local


#Start push to registry==============================================================================
docker push 192.168.0.103:5000/vtmis.webadmin.web.host.local

docker push 192.168.0.103:5000/vtmis.webadmin.web.mvc.local

docker push 192.168.0.103:5000/vtmis.cluster.seed.local

docker push 192.168.0.103:5000/vtmis.api.mmdis.local

docker push 192.168.0.103:5000/vtmis.cluster.vesselquery.local

docker push 192.168.0.103:5000/vtmis.api.maptracking.local

docker push 192.168.0.103:5000/vtmis.cluster.api.local

docker push 192.168.0.103:5000/vtmis.cluster.eventlistener.local

docker push 192.168.0.103:5000/vtmis.webvessel.tracking.local

#Start deploy services==============================================================================
$e = convertfrom-stringdata (get-content .env -raw)

#docker stack deploy -c docker-compose-swarmstack-local.yml mdmdeploy

docker service create --name=myseed --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=4053,target=4053 --constraint 'node.hostname==DESKTOP-KF3CU4F' --env SEED=192.168.43.15 --env PORT=0 --env HOSTNAME=0.0.0.0 192.168.0.103:5000/vtmis.cluster.seed.swarm

docker service create --name=webhost --replicas 2 --endpoint-mode dnsrr --publish mode=host,published=50837,target=50837 192.168.0.103:5000/vtmis.webadmin.web.host.local --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env Mdm_Db_ConnectionString=$e.Loc_Mdm_Db_ConnectionString --env Jwt_Security_Key=$e.Loc_Jwt_Security_Key --env Jwt_Issuer=$e.Loc_Jwt_Issuer --env Jwt_Audience=$e.Loc_Jwt_Audience --env Jwt_IsEnabled=$e.Loc_Jwt_IsEnabled --env CORS_List=$e.Loc_CQRS_List

docker service create --name=webmvc --replicas 2 --endpoint-mode dnsrr --publish mode=host,published=8082,target=8082 192.168.0.103:5000/vtmis.webadmin.web.mvc.local --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env Mdm_Db_ConnectionString=$e.Loc_Mdm_Db_ConnectionString --env Jwt_Security_Key=$e.Loc_Jwt_Security_Key --env Jwt_Issuer=$e.Loc_Jwt_Issuer --env Jwt_Audience=$e.Loc_Jwt_Audience --env Jwt_IsEnabled=$e.Loc_Jwt_IsEnabled --env CORS_List=$e.Loc_CQRS_List


#MMDIS---------------------------------------------------------------------
docker service create --name=apimmdis1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=50838,target=50838 --publish mode=host,published=50839,target=50839 --constraint 'node.hostname==DESKTOP-KF3CU4F' --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=50839 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.103 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env Jwt_Security_Key=$e.Loc_Jwt_Security_Key --env Jwt_Issuer=$e.Loc_Jwt_Issuer --env Jwt_Audience=$e.Loc_Jwt_Audience --env Jwt_IsEnabled=$e.Loc_Jwt_IsEnabled --env CORS_List=$e.Loc_CQRS_List 192.168.0.103:5000/vtmis.api.mmdis.local

docker service create --name=apimmdis2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=50838,target=50838 --publish mode=host,published=50839,target=50839 --constraint 'node.hostname==irwan-officepc' --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=50839 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.103 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env Jwt_Security_Key=$e.Loc_Jwt_Security_Key --env Jwt_Issuer=$e.Loc_Jwt_Issuer --env Jwt_Audience=$e.Loc_Jwt_Audience --env Jwt_IsEnabled=$e.Loc_Jwt_IsEnabled --env CORS_List=$e.Loc_CQRS_List 	192.168.0.103:5000/vtmis.api.mmdis.local


#CLUSTER QUERY----------------------------------------------------------
docker service create --name=clusterquery1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=5011,target=5011 --constraint 'node.hostname==DESKTOP-KF3CU4F' --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5011 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.103 	192.168.0.103:5000/vtmis.cluster.vesselquery.local

docker service create --name=clusterquery2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=5011,target=5011 --constraint 'node.hostname==irwan-officepc' --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5011 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.104 	192.168.0.103:5000/vtmis.cluster.vesselquery.local


#API MAPTRACKING-------------------------------------------------------
docker service create --name=apimaptracking1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=8081,target=80 --publish mode=host,published=5021,target=5021 --constraint 'node.hostname==DESKTOP-KF3CU4F' --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5021 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.103 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env VTS_DB_PROD_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env CORS_List=$e.Loc_CQRS_List 192.168.0.103:5000/vtmis.api.maptracking.local

docker service create --name=apimaptracking2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=8081,target=80 --publish mode=host,published=5021,target=5021 --constraint 'node.hostname==irwan-officepc' --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5021 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.104 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env VTS_DB_PROD_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env CORS_List=$e.Loc_CQRS_List 	192.168.0.103:5000/vtmis.api.maptracking.local

#CLUSTER API---------------------------------------------------------
docker service create --name=clusterapi1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=5003,target=5003 --constraint 'node.hostname==DESKTOP-KF3CU4F'--env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5021 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.103 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env VTS_DB_PROD_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env CORS_List=$e.Loc_CQRS_List    192.168.0.103:5000/vtmis.cluster.api.local

docker service create --name=clusterapi2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=5003,target=5003 --constraint 'node.hostname==irwan-officepc'--env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5021 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.103 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env VTS_DB_PROD_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env CORS_List=$e.Loc_CQRS_List    192.168.0.103:5000/vtmis.cluster.api.local

#CLUSTER EVENTLISTENER---------------------------------------------
docker service create --name=clustereventlistener1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=6010,target=6010 --constraint 'node.hostname==DESKTOP-KF3CU4F' --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5021 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.103 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env VTS_DB_PROD_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env CORS_List=$e.Loc_CQRS_List    192.168.0.103:5000/vtmis.cluster.eventlistener.local

docker service create --name=clustereventlistener2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=6010,target=6010 --constraint 'node.hostname==irwan-officepc' --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5021 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.104 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env VTS_DB_PROD_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env CORS_List=$e.Loc_CQRS_List    192.168.0.103:5000/vtmis.cluster.eventlistener.local

#MAP TRACKING--------------------------------------------------
docker service create --name=tracking1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,target=80 --publish mode=host,published=5020,target=5020 --constraint 'node.hostname==DESKTOP-KF3CU4F' --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5020 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.103 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env VTS_DB_PROD_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env PUB_HOST="192.168.0.103:2001" --env PUB_USER="vtmis_user" --env PUB_PASSWORD="123qwe" --env USE_QUE="True" 	192.168.0.103:5000/vtmis.webvessel.tracking.local

docker service create --name=tracking1_2 --replicas 1 --endpoint-mode dnsrr --publish mode=host,target=80 --publish mode=host,published=5022,target=5022 --constraint 'node.hostname==DESKTOP-KF3CU4F'  --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5022 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.103 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env VTS_DB_PROD_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env PUB_HOST="192.168.0.103:2001" --env PUB_USER="vtmis_user" --env PUB_PASSWORD="123qwe" --env USE_QUE="True" 192.168.0.103:5000/vtmis.webvessel.tracking.local

docker service create --name=tracking2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,target=80 --publish mode=host,published=5020,target=5020 --constraint 'node.hostname==irwan-officepc'  --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5020 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.104 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env VTS_DB_PROD_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env PUB_HOST="192.168.0.103:2001" --env PUB_USER="vtmis_user" --env PUB_PASSWORD="123qwe" --env USE_QUE="True" 	192.168.0.103:5000/vtmis.webvessel.tracking.local

docker service create --name=tracking2_2 --replicas 1 --endpoint-mode dnsrr --publish mode=host,target=80 --publish mode=host,published=5022,target=5022 --constraint 'node.hostname==irwan-officepc'  --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env SEED=192.168.0.104 --env PORT=5022 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=192.168.0.104 --env USERNAME="vessel_sa" --env PASSWORD="P@ssw0rd123" --env DB_SERVER=192.168.0.103 --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env VTS_DB_PROD_CONNECTIONSTRING="Server=192.168.0.103,1433; Database=VTS;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;" --env PUB_HOST="192.168.0.103:2001" --env PUB_USER="vtmis_user" --env PUB_PASSWORD="123qwe" --env USE_QUE="True" 	192.168.0.103:5000/vtmis.webvessel.tracking.local

#nak test ci/cd