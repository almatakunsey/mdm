$versionDate = (Get-Date).ToString("dd-MM-yyyy")
#Start Publish========================================================================================


cd Vtmis.WebVessel.Tracking

npm i

npm run prod

cd..

dotnet publish Vtmis.WebAdmin.Web.Host/Vtmis.WebAdmin.Web.Host.csproj -c Release -o bin/publish_prod

dotnet publish Vtmis.WebAdmin.Web.Mvc/Vtmis.WebAdmin.Web.Mvc.csproj -c Release -o bin/publish_prod

dotnet publish Vtmis.Cluster.Seed/Vtmis.Cluster.Seed.csproj -c Release -o bin/publish_prod

dotnet publish Vtmis.Cluster.VesselQuery/Vtmis.Cluster.VesselQuery.csproj -c Release -o bin/publish_prod

dotnet publish Vtmis.Cluster.VesselApi/Vtmis.Cluster.VesselApi.csproj -c Release -o bin/publish_prod

dotnet publish Vtmis.Api.MapTracking/Vtmis.Api.MapTracking.csproj -c Release -o bin/publish_prod

dotnet publish Vtmis.WebVessel.Tracking/Vtmis.WebVessel.Tracking.csproj -c Release -o bin/publish_prod

dotnet publish Vtmis.Api.MMDIS/Vtmis.Api.MMDIS.csproj -c Release -o bin/publish_prod

dotnet publish Vtmis.Cluster.EventListener/Vtmis.Cluster.EventListener.csproj -c Release -o bin/publish_prod


#Start Build Container Images========================================================================
#docker-compose -f "docker-compose-swarmbuild-prod.yml" build

cd Vtmis.WebAdmin.Web.Host
docker build -t vtmis.webadmin.web.host.prod_$versionDate -f Dockerfile_production .
cd..

cd Vtmis.WebAdmin.Web.Mvc
docker build -t vtmis.webadmin.web.mvc.prod_$versionDate -f Dockerfile_production .
cd..

cd Vtmis.Cluster.Seed
docker build -t vtmis.cluster.seed.prod_$versionDate -f Dockerfile_production .
cd..

cd Vtmis.Api.MMDIS
docker build -t vtmis.api.mmdis.prod_$versionDate -f Dockerfile_production .
cd..

cd Vtmis.Cluster.VesselQuery
docker build -t vtmis.cluster.vesselquery.prod_$versionDate -f Dockerfile_production .
cd..

cd Vtmis.Api.MapTracking
docker build -t vtmis.api.maptracking.prod_$versionDate -f Dockerfile_production .
cd..

cd Vtmis.Cluster.VesselApi
docker build -t vtmis.cluster.api.prod_$versionDate -f Dockerfile_production .
cd..

cd Vtmis.Cluster.EventListener
docker build -t vtmis.cluster.eventlistener.prod_$versionDate -f Dockerfile_production .
cd..

cd Vtmis.WebVessel.Tracking
docker build -t vtmis.webvessel.tracking.prod_$versionDate -f Dockerfile_production .
cd..

cd Vtmis.WebAdmin.EntityFrameworkCore

dotnet ef database update

cd..

#Start Tag===========================================================================================
docker tag vtmis.webadmin.web.host.prod_$versionDate 10.1.2.13:5000/vtmis.webadmin.web.host.prod_$versionDate

docker tag vtmis.webadmin.web.mvc.prod_$versionDate 10.1.2.13:5000/vtmis.webadmin.web.mvc.prod_$versionDate

docker tag vtmis.cluster.seed.prod_$versionDate 10.1.2.13:5000/vtmis.cluster.seed.prod_$versionDate

docker tag vtmis.api.mmdis.prod_$versionDate 10.1.2.13:5000/vtmis.api.mmdis.prod_$versionDate

docker tag vtmis.cluster.vesselquery.prod_$versionDate 10.1.2.13:5000/vtmis.cluster.vesselquery.prod_$versionDate

docker tag vtmis.api.maptracking.prod_$versionDate 10.1.2.13:5000/vtmis.api.maptracking.prod_$versionDate

docker tag vtmis.cluster.api.prod_$versionDate 10.1.2.13:5000/vtmis.cluster.api.prod_$versionDate

docker tag vtmis.cluster.eventlistener.prod_$versionDate 10.1.2.13:5000/vtmis.cluster.eventlistener.prod_$versionDate

docker tag vtmis.webvessel.tracking.prod_$versionDate 10.1.2.13:5000/vtmis.webvessel.tracking.prod_$versionDate


#Start push to registry==============================================================================
docker push 10.1.2.13:5000/vtmis.webadmin.web.host.prod_$versionDate

docker push 10.1.2.13:5000/vtmis.webadmin.web.mvc.prod_$versionDate

docker push 10.1.2.13:5000/vtmis.cluster.seed.prod_$versionDate

docker push 10.1.2.13:5000/vtmis.api.mmdis.prod_$versionDate

docker push 10.1.2.13:5000/vtmis.cluster.vesselquery.prod_$versionDate

docker push 10.1.2.13:5000/vtmis.api.maptracking.prod_$versionDate

docker push 10.1.2.13:5000/vtmis.cluster.api.prod_$versionDate

docker push 10.1.2.13:5000/vtmis.cluster.eventlistener.prod_$versionDate

docker push 10.1.2.13:5000/vtmis.webvessel.tracking.prod_$versionDate

#Start deploy services==============================================================================
$e = convertfrom-stringdata (get-content .env -raw)

$Jwt_Audience=$e.Prod_Jwt_Audience
$ASPNETCORE_ENVIRONMENT=$e.Prod_Environment
$Mdm_Db_ConnectionString=$e.Prod_Mdm_Db_ConnectionString
$Jwt_Security_Key=$e.Prod_Jwt_Security_Key
$Jwt_Issuer=$e.Prod_Jwt_Issuer
$Jwt_Issuer_Uri="http://${e.Prod_Local_IP_Address}:50837/api/TokenAuth/Authenticate"
$CORS_List=$e.Prod_CORS_List
$SEED_PORT=$e.Prod_SEED_PORT
$Jwt_IsEnabled="true"
$Prod_Local_IP_Address=$e.Prod_Local_IP_Address

#docker stack deploy -c docker-compose-swarmstack-prod.yml mdmdeploy

docker service create --name=myseed --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=4053,target=4053 --constraint 'node.hostname==WIN-HGM7IPDSME1' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env SEED=10.1.2.13 --env PORT=4053 --env HOSTNAME=0.0.0.0 --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT 10.1.2.13:5000/vtmis.cluster.seed.prod_$versionDate

docker service create --name=webhost --replicas 2 --endpoint-mode dnsrr --publish mode=host,published=50837,target=50837 --publish mode=host,published=51837,target=51837 --constraint 'node.hostname!=WIN-HGM7IPDSME1' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.15 --env PORT=51837 --env DB_DATE="2017-08-30 08:00:00" --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env Mdm_Db_ConnectionString=$Mdm_Db_ConnectionString --env Jwt_Security_Key=$Jwt_Security_Key --env Jwt_Issuer=$Jwt_Issuer --env Jwt_Audience=$Jwt_Audience --env Jwt_IsEnabled=$Jwt_IsEnabled --env CORS_List=$CORS_List 10.1.2.13:5000/vtmis.webadmin.web.host.prod_$versionDate

docker service create --name=webmvc --replicas 2 --endpoint-mode dnsrr --publish mode=host,published=8082,target=8082 --constraint 'node.hostname!=WIN-HGM7IPDSME1' --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env Mdm_Db_ConnectionString=$Mdm_Db_ConnectionString --env Jwt_Security_Key=$Jwt_Security_Key --env Jwt_Issuer=$Jwt_Issuer --env Jwt_Audience=$Jwt_Audience --env Jwt_IsEnabled=$Jwt_IsEnabled 10.1.2.13:5000/vtmis.webadmin.web.mvc.prod_$versionDate

#docker service update -d --image 10.1.2.13:5000/vtmis.webadmin.web.host.prod_$versionDate webhost

#docker service update -d --image 10.1.2.13:5000/vtmis.webadmin.web.mvc.prod_$versionDate webmvc

#docker service update -d --image 10.1.2.13:5000/vtmis.cluster.seed.prod_$versionDate myseed

#MMDIS---------------------------------------------------------------------
docker service create --name=apimmdis1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=50838,target=5013 --publish mode=host,published=50839,target=50839 --constraint 'node.hostname==MDM-Server-15' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=50839 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.15 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env Mdm_Db_ConnectionString=$Mdm_Db_ConnectionString --env VTS_DB_PROD_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env Jwt_Security_Key=$Jwt_Security_Key --env Jwt_Issuer=$Jwt_Issuer --env Jwt_Audience=$Jwt_Audience --env Jwt_IsEnabled=$Jwt_IsEnabled --env CORS_List=$CORS_List 10.1.2.13:5000/vtmis.api.mmdis.prod_$versionDate

docker service create --name=apimmdis2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=50838,target=5013 --publish mode=host,published=50839,target=50839 --constraint 'node.hostname==MDM2_Server' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=50839 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.15.16 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env Mdm_Db_ConnectionString=$Mdm_Db_ConnectionString --env VTS_DB_PROD_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env Jwt_Security_Key=$Jwt_Security_Key --env Jwt_Issuer=$Jwt_Issuer --env Jwt_Audience=$Jwt_Audience --env Jwt_IsEnabled=$Jwt_IsEnabled --env CORS_List=$CORS_List 10.1.2.13:5000/vtmis.api.mmdis.prod_$versionDate

#docker service update -d --image 10.1.2.13:5000/vtmis.api.mmdis.prod_$versionDate apimmdis1_1

#docker service update -d --image 10.1.2.13:5000/vtmis.api.mmdis.prod_$versionDate apimmdis2_1

#CLUSTER QUERY----------------------------------------------------------
docker service create --name=clusterquery1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=5111,target=5111 --constraint 'node.hostname==MDM-Server-15' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=5011 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.15 10.1.2.13:5000/vtmis.cluster.vesselquery.prod_$versionDate

docker service create --name=clusterquery2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=5111,target=5111 --constraint 'node.hostname==MDM2_Server' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=5011 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.16 10.1.2.13:5000/vtmis.cluster.vesselquery.prod_$versionDate

#docker service update -d --image 10.1.2.13:5000/vtmis.cluster.vesselquery.prod_$versionDate clusterquery1_1

#docker service update -d --image 10.1.2.13:5000/vtmis.cluster.vesselquery.prod_$versionDate clusterquery2_1

#API MAPTRACKING-------------------------------------------------------
docker service create --name=apimaptracking1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=8081,target=5014 --publish mode=host,published=5030,target=5030 --constraint 'node.hostname==MDM-Server-15' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=5030 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.15 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env Jwt_Security_Key=$Jwt_Security_Key --env Jwt_Issuer=$Jwt_Issuer --env Jwt_Audience=$Jwt_Audience --env Jwt_IsEnabled=$Jwt_IsEnabled --env Mdm_Db_ConnectionString=$Mdm_Db_ConnectionString --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_PROD_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env CORS_List=$CORS_List 10.1.2.13:5000/vtmis.api.maptracking.prod_$versionDate

docker service create --name=apimaptracking2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=8081,target=5014 --publish mode=host,published=5030,target=5030 --constraint 'node.hostname==MDM2_Server' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=5030 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.16 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env Jwt_Security_Key=$Jwt_Security_Key --env Jwt_Issuer=$Jwt_Issuer --env Jwt_Audience=$Jwt_Audience --env Jwt_IsEnabled=$Jwt_IsEnabled --env Mdm_Db_ConnectionString=$Mdm_Db_ConnectionString --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_PROD_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env CORS_List=$CORS_List 10.1.2.13:5000/vtmis.api.maptracking.prod_$versionDate

#docker service update -d --image 10.1.2.13:5000/vtmis.api.maptracking.prod_$versionDate apimaptracking1_1

#docker service update -d --image 10.1.2.13:5000/vtmis.api.maptracking.prod_$versionDate apimaptracking2_1

#CLUSTER API---------------------------------------------------------
docker service create --name=clusterapi1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=5003,target=5003 --constraint 'node.hostname==MDM-Server-15' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env Prod_Local_IP_Address=$Prod_Local_IP_Address --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=5021 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.15 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env Mdm_Db_ConnectionString=$Mdm_Db_ConnectionString --env DB_SERVER=10.1.2.10 --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_PROD_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" 10.1.2.13:5000/vtmis.cluster.api.prod_$versionDate

docker service create --name=clusterapi2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=5003,target=5003 --constraint 'node.hostname==MDM2_Server' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env Prod_Local_IP_Address=$Prod_Local_IP_Address --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=5021 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.16 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env Mdm_Db_ConnectionString=$Mdm_Db_ConnectionString --env DB_SERVER=10.1.2.10 --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_PROD_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" 10.1.2.13:5000/vtmis.cluster.api.prod_$versionDate

#docker service update -d --image 10.1.2.13:5000/vtmis.cluster.api.prod_$versionDate clusterapi1_1

#docker service update -d --image 10.1.2.13:5000/vtmis.cluster.api.prod_$versionDate clusterapi2_1

#CLUSTER EVENTLISTENER---------------------------------------------
docker service create --name=clustereventlistener1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=6010,target=6010 --constraint 'node.hostname==MDM-Server-15' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=6010 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.15 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_PROD_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env CORS_List=$CORS_List 10.1.2.13:5000/vtmis.cluster.eventlistener.prod_$versionDate

docker service create --name=clustereventlistener2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=6010,target=6010 --constraint 'node.hostname==MDM2_Server' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=6010 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.16 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_PROD_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env CORS_List=$CORS_List 10.1.2.13:5000/vtmis.cluster.eventlistener.prod_$versionDate

#docker service update -d --image 10.1.2.13:5000/vtmis.cluster.eventlistener.prod_$versionDate clustereventlistener1_1

#docker service update -d --image 10.1.2.13:5000/vtmis.cluster.eventlistener.prod_$versionDate clustereventlistener2_1

#MAP TRACKING--------------------------------------------------

docker service create --name=tracking1_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=51949,target=80 --publish mode=host,published=5020,target=5020 --constraint 'node.hostname==MDM-Server-15' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env Prod_Local_IP_Address=$Prod_Local_IP_Address --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=5020 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.15 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env Jwt_Issuer_Uri=$Jwt_Issuer_Uri --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env Jwt_Security_Key=$Jwt_Security_Key --env Jwt_Issuer=$Jwt_Issuer --env Jwt_Audience=$Jwt_Audience --env Jwt_IsEnabled=$Jwt_IsEnabled --env DB_DATE="2017-08-30 08:00:00" --env PUB_HOST="10.1.2.13:5672" --env PUB_USER="vtmis_user" --env PUB_PASSWORD="123qwe" --env USE_QUEUE="True" 10.1.2.13:5000/vtmis.webvessel.tracking.prod_$versionDate

#docker service create --name=tracking1_2 --replicas 1 --endpoint-mode dnsrr --publish mode=host,target=80 --publish mode=host,published=5022,target=5022 --constraint 'node.hostname==MDM-Server-15' --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env Prod_Local_IP_Address=$Prod_Local_IP_Address --env SEED=10.1.2.13 --env PORT=5022 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.15 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_PROD_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env PUB_HOST="10.1.2.13:2001" --env PUB_USER="vtmis_user" --env PUB_PASSWORD="123qwe" --env USE_QUE="True" 	10.1.2.13:5000/vtmis.webvessel.tracking.prod

docker service create --name=tracking2_1 --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=51949,target=80 --publish mode=host,published=5020,target=5020 --constraint 'node.hostname==MDM2_Server' --mount type=bind,source=C:/MDM/Logs,destination=C:/logs/akka --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env Prod_Local_IP_Address=$Prod_Local_IP_Address --env SEED=10.1.2.13 --env SEED_PORT=$SEED_PORT --env PORT=5020 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.16 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env Jwt_Issuer_Uri=$Jwt_Issuer_Uri --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env Jwt_Security_Key=$Jwt_Security_Key --env Jwt_Issuer=$Jwt_Issuer --env Jwt_Audience=$Jwt_Audience --env Jwt_IsEnabled=$Jwt_IsEnabled --env DB_DATE="2017-08-30 08:00:00" --env PUB_HOST="10.1.2.13:5672" --env PUB_USER="vtmis_user" --env PUB_PASSWORD="123qwe" --env USE_QUEUE="True" 10.1.2.13:5000/vtmis.webvessel.tracking.prod_$versionDate

#docker service create --name=tracking2_2 --replicas 1 --endpoint-mode dnsrr --publish mode=host,target=80 --publish mode=host,published=5022,target=5022 --constraint 'node.hostname==MDM2_Server' --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env Prod_Local_IP_Address=$Prod_Local_IP_Address --env SEED=10.1.2.13 --env PORT=5022 --env HOSTNAME=0.0.0.0 --env PUBLIC_HOSTNAME=10.1.2.16 --env USERNAME="WebVtsUser" --env PASSWORD="WebVtsUser_P@ssw0rd" --env DB_SERVER=10.1.2.10 --env VTS_DB_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env DB_DATE="2017-08-30 08:00:00" --env VTS_DB_PROD_CONNECTIONSTRING="Server=10.1.2.10,1433; Database=VTS;User Id=WebVtsUser;Password=WebVtsUser_P@ssw0rd; Trusted_Connection=False;" --env PUB_HOST="192.168.0.103:2001" --env PUB_USER="vtmis_user" --env PUB_PASSWORD="123qwe" --env USE_QUE="True" 	10.1.2.13:5000/vtmis.webvessel.tracking.prod

#docker service update -d --image 10.1.2.13:5000/vtmis.webvessel.tracking.prod_$versionDate tracking1_1

#docker service update -d --image 10.1.2.13:5000/vtmis.webvessel.tracking.prod_$versionDate tracking2_1