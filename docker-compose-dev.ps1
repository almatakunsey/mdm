dotnet publish Vtmis.WebAdmin.Web.Host/Vtmis.WebAdmin.Web.Host.csproj -c Release -o bin/publish_staging

dotnet publish Vtmis.WebAdmin.Web.Mvc/Vtmis.WebAdmin.Web.Mvc.csproj -c Release -o bin/publish_staging

dotnet publish Vtmis.Cluster.Seed/Vtmis.Cluster.Seed.csproj -c Release -o bin/publish_staging

dotnet publish Vtmis.Cluster.VesselQuery/Vtmis.Cluster.VesselQuery.csproj -c Release -o bin/publish_staging

dotnet publish Vtmis.Cluster.VesselApi/Vtmis.Cluster.VesselApi.csproj -c Release -o bin/publish_staging

dotnet publish Vtmis.Api.MapTracking/Vtmis.Api.MapTracking.csproj -c Release -o bin/publish_staging

dotnet publish Vtmis.WebVessel.Tracking/Vtmis.WebVessel.Tracking.csproj -c Release -o bin/publish_staging

dotnet publish Vtmis.Api.MMDIS/Vtmis.Api.MMDIS.csproj -c Release -o bin/publish_staging

dotnet publish Vtmis.Cluster.EventListener/Vtmis.Cluster.EventListener.csproj -c Release -o bin/publish_staging

docker-compose down

docker-compose -f ".\docker-compose.staging.yml" up --build --detach

#nak test ci/cd