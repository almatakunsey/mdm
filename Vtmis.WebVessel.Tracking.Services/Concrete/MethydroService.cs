﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;
using Vtmis.Core.Model.DTO.Methydro;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Projection;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;

namespace Vtmis.WebVessel.Tracking.Services.Concrete
{
    public class MethydroService : IMethydroService
    {
        private readonly IDapperAisRepository _dapperAisRepository;
        private readonly ITargetQuery _targetQuery;

        public MethydroService(IDapperAisRepository dapperAisRepository, ITargetQuery targetQuery)
        {
            _dapperAisRepository = dapperAisRepository;
            _targetQuery = targetQuery;
        }

        public async Task<IEnumerable<ResponseWeatherHistorical>> GetWeatherHistoricalAsync(int mmsi, DateTime fromDate, DateTime toDate)
        {
            var qry = await _targetQuery.QueryMethydroHistorical(mmsi, fromDate, toDate);
            if (qry.Any())
            {
                //var dateCol = new HashSet<DateTime>();
                //var metCol = new List<ResponseWeatherHistorical>();
                ////if (qry.First() is ResponseDDMSHistorical)
                ////{
                //foreach (ResponseWeatherHistorical item in qry)
                //{
                //    dateCol.Add(item.LocalRecvTime);
                //}
                //DateTime? d = null;
                //foreach (var item in dateCol.OrderBy(o => o))
                //{
                //    if (d == null)
                //    {
                //        d = item;
                //    }
                //    if ((item - d).Value.TotalSeconds >= 120)
                //    {
                //        metCol.Add(qry.Where(x => x.LocalRecvTime == item).First());
                //        d = item;
                //    }
                //}
                //}
                return qry;
            }
            return null;
        }

        public async Task<PaginatedList<MethydroStation>> GetAllMethydroStationAsync(int pageIndex = 1, int pageSize = 100)
        {
            return await _targetQuery.QueryMethydroStation(pageIndex, pageSize).ConfigureAwait(false);
        }

        public async Task<List<MethydroDetailsWithDate>> GetInstrumentDataByDateRangeAsync(DateTime fromDate, DateTime toDate,
            MetHydroDataType? metHydroDataType = null)
        {
            if (fromDate.Date > toDate.Date)
            {
                throw new Exception($"From Date: {fromDate.Date} cannot be higher than To Date: {toDate.Date}");
            }
            try
            {
                var listDates = CommonHelper.EachDay(fromDate, toDate);
                var qryMetDetail = new List<MethydroDetails>().AsQueryable();
                var methydrosList = new List<MethydroDetailsWithDate>();
                var vtsDbContext = new VtsDatabaseContext();

                var vtsResult = await vtsDbContext.Query<WeatherInstruments>().FromSql(@"
                    SELECT ap.Name, met.MMSI, met.DAC, met.FI, met.RecvTime, met.AirPres,
                    met.AirTemp, met.RelHumidity, met.WndDir, met.WndGust, met.WndGustDir, met.WndSpeed,
                    met.SurfCurDir, met.SurfCurSpeed, met.WaterLevel, met.WaterTemp, met.WaveDir, met.WaveHeight,
                    met.WavePeriod, met.Visibility, met.CurDir2, met.CurDir3, met.CurSpeed2, met.CurSpeed3
                    --AtonMetData
                    FROM (
                        SELECT imet.MMSI, imet.DAC, imet.FI, imet.RecvTime, imet.AirPres,
                        imet.AirTemp, imet.RelHumidity, imet.WndDir, imet.WndGust, imet.WndGustDir, imet.WndSpeed,
                        imet.SurfCurDir, imet.SurfCurSpeed, imet.WaterLevel, imet.WaterTemp, imet.WaveDir, imet.WaveHeight,
                        imet.WavePeriod, imet.Visibility, imet.CurDir2, imet.CurDir3, imet.CurSpeed2, imet.CurSpeed3
                        --AtonMetDataGroup
                        FROM (
                            SELECT MMSI, MAX(RecvTime) RecvTime FROM AtonMetData GROUP BY MMSI
                        ) g
                        INNER JOIN AtonMetData imet on g.MMSI=imet.MMSI
                        WHERE imet.RecvTime=g.RecvTime
                    ) met
                     OUTER APPLY
                    (
                        SELECT TOP 1 Name
                        FROM AtonPosData M 
                        WHERE M.MMSI = met.MMSI 
                    ) ap                    
                ").OrderBy(o => o.RecvTime).AsQueryable().ToListAsync();
                //var a = await vtsResult.ToListAsync();
                //foreach (var requestDate in listDates)
                //{
                //var p0 = DateTime.SpecifyKind(requestDate, DateTimeKind.Local).ToUniversalTime();

                var methydros = new MethydroDetailsWithDate();
                var requestDate = string.Equals(AppHelper.GetEnvironmentName(),
                                       Core.Common.Constants.HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase) ?
                                       AppHelper.GetDevAisDate().ToDateTime() :
                                       DateTime.Now;
                //var requestDate = DateTime.Now;
                var dbContext = new VesselDatabaseContext(requestDate);

                var aisResult = await dbContext.Query<QueryMetStaticAtonPos>().FromSql(@"
                          SELECT 
                         ss.Name, 
                         ap.Name PosName, ap.MMSI, ap.StnID, ap.RecvTime, ap.Latitude,
                        ap.Longitude, met.DAC, met.FI,
                        met.AirPres, met.AirTemp, met.RelHumidity, met.WndDir, met.WndGust, met.WndGustDir, 
                        met.WndSpeed, met.SurfCurDir, met.SurfCurSpeed, met.WaterLevel, met.WaterTemp, met.WaveDir,
                        met.WaveHeight, met.WavePeriod, met.Visibility, met.CurDir2, met.CurDir3, met.CurSpeed2, met.CurSpeed3
                        --AtonPosData
                        FROM (
                                SELECT iap.Name, iap.MMSI, iap.StnID, iap.RecvTime, iap.Latitude, iap.Longitude                             
                                --AtonPosDataGroup
                                FROM (
                                    SELECT MMSI, MAX(RecvTime) RecvTime FROM AtonPosData
                                    GROUP BY MMSI
                                ) gap
                                INNER JOIN AtonPosData iap on gap.MMSI=iap.MMSI
                                WHERE iap.RecvTime=gap.RecvTime
                        ) ap
						--AtonMetData
                         OUTER APPLY
					    (
					        SELECT TOP 1 imet.MMSI, imet.DAC, imet.FI, imet.AirPres, imet.AirTemp, imet.RelHumidity,
                                imet.WndDir, imet.WndGust,
                                imet.WndGustDir, imet.WndSpeed, imet.SurfCurDir, imet.SurfCurSpeed, imet.WaterLevel,
                                imet.WaterTemp, imet.WaveDir, imet.WaveHeight, imet.WavePeriod, imet.Visibility,
                                imet.CurDir2, imet.CurDir3, imet.CurSpeed2, imet.CurSpeed3
					        FROM AtonMetData imet 
					        WHERE imet.MMSI = ap.MMSI
							ORDER BY imet.RecvTime DESC
					    ) met
						--ShipStatic
						OUTER APPLY
						(
					        SELECT TOP 1 iss.Name
					        FROM ShipStatic iss 
					        WHERE iss.MMSI = ap.MMSI
					    ) ss   
                    ").AsQueryable().ToListAsync();

                var qryAisMet = (from vts in vtsResult
                                 join ais in aisResult on vts.MMSI equals ais.MMSI
                                 select new QueryMetStaticAtonPos
                                 {
                                     AirPres = ais.AirPres.HasValue ? ais.AirPres : vts.AirPres,
                                     AirTemp = ais.AirTemp.HasValue ? ais.AirTemp : vts.AirTemp,
                                     DAC = vts.DAC,
                                     FI = vts.FI,
                                     Latitude = ais.Latitude,
                                     Longitude = ais.Longitude,
                                     MMSI = vts.MMSI,
                                     Name = ais != null ? ais.Name : vts.Name,
                                     PosName = ais.PosName,
                                     RecvTime = ais.RecvTime,
                                     RelHumidity = ais.RelHumidity.HasValue ? ais.RelHumidity : vts.RelHumidity,
                                     StnID = ais.StnID,
                                     SurfCurDir = ais.SurfCurDir.HasValue ? ais.SurfCurDir : vts.SurfCurDir,
                                     SurfCurSpeed = ais.SurfCurSpeed.HasValue ? ais.SurfCurSpeed : vts.SurfCurSpeed,
                                     WaterLevel = ais.WaterLevel.HasValue ? ais.WaterLevel : vts.WaterLevel,
                                     WaterTemp = ais.WaterTemp.HasValue ? ais.WaterTemp : vts.WaterTemp,
                                     WaveDir = ais.WaveDir.HasValue ? ais.WaveDir : vts.WaveDir,
                                     WaveHeight = ais.WaveHeight.HasValue ? ais.WaveHeight : vts.WaveHeight,
                                     WavePeriod = ais.WavePeriod.HasValue ? ais.WavePeriod : vts.WavePeriod,
                                     WndDir = ais.WndDir.HasValue ? ais.WndDir : vts.WndDir,
                                     WndGust = ais.WndGust.HasValue ? ais.WndGust : vts.WndGust,
                                     WndGustDir = ais.WndGustDir.HasValue ? ais.WndGustDir : vts.WndGustDir,
                                     WndSpeed = ais.WndSpeed.HasValue ? ais.WndSpeed : vts.WndSpeed,
                                     Visibility = ais.Visibility.HasValue ? ais.Visibility : vts.Visibility,
                                     CurDir2 = ais.CurDir2.HasValue ? ais.CurDir2 : vts.CurDir2,
                                     CurDir3 = ais.CurDir3.HasValue ? ais.CurDir3 : vts.CurDir3,
                                     CurSpeed2 = ais.CurSpeed2.HasValue ? ais.CurSpeed2 : vts.CurSpeed2,
                                     CurSpeed3 = ais.CurSpeed3.HasValue ? ais.CurSpeed3 : vts.CurSpeed3
                                 }).AsQueryable();

                if (metHydroDataType == MetHydroDataType.Water)
                {

                    if (qryAisMet.Any())
                    {
                        methydros.Date = requestDate;
                        qryMetDetail = qryAisMet.Select(x => new MethydroDetails
                        {
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,
                            DAC = x.DAC,
                            FI = x.FI,
                            Id = x.StnID,
                            UtcRecvTime = x.RecvTime,
                            LocalRecvTime = x.LocalRecvTime,
                            MMSI = x.MMSI,
                            StationName = GetStationName(x.PosName, x.Latitude, x.Longitude, x.Name, x.MMSI, x.DAC, x.FI, x.LocalRecvTime),
                            Instruments = new MethydroInstruments
                            {
                                Date = requestDate,
                                MetWaterData = new MetWaterData
                                {
                                    CurrentDirection = x.SurfCurDir,
                                    CurrentSpeed = x.SurfCurSpeed,
                                    WaterLevel = x.WaterLevel,
                                    WaterTemperature = x.WaterTemp,
                                    WaveDirection = x.WaveDir,
                                    WaveHeight = x.WaveHeight,
                                    WavePeriod = x.WavePeriod,
                                    Visibility = x.Visibility,
                                    CurrentDirection2 = x.CurDir2,
                                    CurrentDirection3 = x.CurDir3,
                                    CurrentSpeed2 = x.CurSpeed2,
                                    CurrentSpeed3 = x.CurSpeed3
                                }
                            }
                        });
                    }
                }
                else if (metHydroDataType == MetHydroDataType.Air)
                {
                    //qryAisMet = qryAisMet.Where(x => x.AirPres != null || x.AirTemp != null ||
                    //                x.RelHumidity != null || x.WndDir != null ||
                    //                x.WndGust != null || x.WndGustDir != null || x.WndSpeed != null);

                    if (qryAisMet.Any())
                    {
                        methydros.Date = requestDate;
                        qryMetDetail = qryAisMet.Select(x => new MethydroDetails
                        {
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,
                            DAC = x.DAC,
                            FI = x.FI,
                            Id = x.StnID,
                            UtcRecvTime = x.RecvTime,
                            LocalRecvTime = x.LocalRecvTime,
                            MMSI = x.MMSI,
                            StationName = GetStationName(x.PosName, x.Latitude, x.Longitude, x.Name, x.MMSI, x.DAC, x.FI, x.LocalRecvTime),
                            Instruments = new MethydroInstruments
                            {
                                Date = requestDate,
                                MetAirData = new MetAirData
                                {
                                    AirPressure = x.AirPres,
                                    AirTemperature = x.AirTemp,
                                    Humidity = x.RelHumidity,
                                    WindDirection = x.WndDir,
                                    WindGust = x.WndGust,
                                    WindGustDirection = x.WndGustDir,
                                    WindSpeed = x.WndSpeed
                                }
                            }
                        });
                    }
                }
                else
                {
                    if (qryAisMet.Any())
                    {

                        methydros.Date = requestDate;
                        qryMetDetail = qryAisMet.Select(x => new MethydroDetails
                        {
                            Latitude = x.Latitude,
                            Longitude = x.Longitude,
                            DAC = x.DAC,
                            FI = x.FI,
                            Id = x.StnID,
                            UtcRecvTime = x.RecvTime,
                            LocalRecvTime = x.LocalRecvTime,
                            MMSI = x.MMSI,
                            StationName = GetStationName(x.PosName, x.Latitude, x.Longitude, x.Name, x.MMSI, x.DAC, x.FI, x.LocalRecvTime),
                            Instruments = new MethydroInstruments
                            {
                                Date = requestDate,
                                MetAirData = new MetAirData
                                {
                                    AirPressure = x.AirPres,
                                    AirTemperature = x.AirTemp,
                                    Humidity = x.RelHumidity,
                                    WindDirection = x.WndDir,
                                    WindGust = x.WndGust,
                                    WindGustDirection = x.WndGustDir,
                                    WindSpeed = x.WndSpeed
                                },
                                MetWaterData = new MetWaterData
                                {
                                    CurrentDirection = x.SurfCurDir,
                                    CurrentSpeed = x.SurfCurSpeed,
                                    WaterLevel = x.WaterLevel,
                                    WaterTemperature = x.WaterTemp,
                                    WaveDirection = x.WaveDir,
                                    WaveHeight = x.WaveHeight,
                                    WavePeriod = x.WavePeriod,
                                    Visibility = x.Visibility,
                                    CurrentDirection2 = x.CurDir2,
                                    CurrentDirection3 = x.CurDir3,
                                    CurrentSpeed2 = x.CurSpeed2,
                                    CurrentSpeed3 = x.CurSpeed3
                                }
                            }
                        });
                    }
                }

                methydros.Methydros = qryMetDetail.ToList();
                methydrosList.Add(methydros);

                //}

                return methydrosList.OrderByDescending(o => o.Date).ToList();
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        public async Task<MethydroDetailInstruments> GetInstrumentDataByMmsiDateRangeAsync(int mmsi, DateTime fromDate, DateTime toDate)
        {
            return await _targetQuery.QueryInstrumentDataByMmsiDateRangeAsync(mmsi, fromDate, toDate).ConfigureAwait(false);
        }

        private string GetStationName(string posName, double? latitude, double? longitude, string name, int mmsi, short? dac, short? fi, DateTime localRecvTime)
        {
            string stationName;
            stationName = posName;
            if (string.IsNullOrEmpty(stationName))
            {
                stationName = name;

                if (string.IsNullOrEmpty(stationName))
                {
                    return $@"{mmsi}[{dac}_{fi}] - {CommonHelper.ConvertDecimalCoordToDegree(
                                                   latitude.Value, longitude.Value
                                               )} ({CommonHelper.TimeAgo(localRecvTime)})";
                }
            }
            return $@"{mmsi}[{dac}_{fi}] - {stationName} ({CommonHelper.TimeAgo(localRecvTime)})";
        }

        private List<MetHydroDataType> GetMethydroType(MetHydroDataTypeObj input)
        {
            var methydroDataTypes = new List<MetHydroDataType>();
            if (input.WaterData.CurrentDirection != null || input.WaterData.CurrentSpeed != null ||
                input.WaterData.WaterLevel != null || input.WaterData.WaterTemperature != null ||
                input.WaterData.WaveDirection != null || input.WaterData.WaveHeight != null ||
                input.WaterData.WavePeriod != null || input.WaterData.Visibility.HasValue ||
                input.WaterData.CurrentDirection2.HasValue || input.WaterData.CurrentDirection3.HasValue ||
                input.WaterData.CurrentSpeed2.HasValue || input.WaterData.CurrentSpeed3.HasValue)
            {
                methydroDataTypes.Add(MetHydroDataType.Water);
            }

            if (input.AirData.AirPressure != null || input.AirData.AirTemperature != null ||
                input.AirData.Humidity != null || input.AirData.WindDirection != null ||
                input.AirData.WindGust != null || input.AirData.WindGustDirection != null ||
                input.AirData.WindSpeed != null)
            {
                methydroDataTypes.Add(MetHydroDataType.Air);
            }
            return methydroDataTypes;
        }
    }

    public interface IMethydroService
    {
        /// <summary>
        /// Get Weather Historical
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        Task<IEnumerable<ResponseWeatherHistorical>> GetWeatherHistoricalAsync(int mmsi, DateTime fromDate, DateTime toDate);
        Task<PaginatedList<MethydroStation>> GetAllMethydroStationAsync(int pageIndex = 1, int pageSize = 100);

        Task<List<MethydroDetailsWithDate>> GetInstrumentDataByDateRangeAsync(DateTime fromDate, DateTime toDate,
            MetHydroDataType? metHydroDataType = null);

        Task<MethydroDetailInstruments> GetInstrumentDataByMmsiDateRangeAsync(int mmsi, DateTime fromDate, DateTime toDate);
    }
}
