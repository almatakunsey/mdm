﻿using Abp.UI;
using Akka.Actor;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Constants.Akka;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.AkkaModel;
using Vtmis.Core.Model.ViewModels.Target;
using Vtmis.Core.Model.ViewModels.Target.Vessel;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.WebVessel.Tracking.Services.Concrete
{
    public class VesselService : IVesselService
    {
        private readonly IHostingEnvironment _env;
        private readonly IDapperAisRepository _dapperAisRepository;
        private readonly ITargetQuery _targetQuery;

        public VesselService(IHostingEnvironment env, IDapperAisRepository dapperAisRepository, ITargetQuery targetQuery)
        {
            _env = env;
            _dapperAisRepository = dapperAisRepository;
            _targetQuery = targetQuery;
        }

        public string GetTargetImage(int targetId, ImageSize imageSize, ImageSize emptyImageSize)
        {
            var baseTargetImageFolder = "images/targets/vessels/";

            var targetImageFolder = _env.WebRootFileProvider.GetDirectoryContents(baseTargetImageFolder + targetId);

            if (targetImageFolder.Where(x => x.Exists == true).Any())
            {
                var targetFile = targetImageFolder.Where(x => x.Exists == true);

                if (imageSize == ImageSize.Small)
                {
                    return "/" + baseTargetImageFolder + targetId + "/" +
                        targetFile.Where(x => x.Name.Substring(x.Name.Length - 5)[0] == 's').FirstOrDefault()?.Name;
                }
                else if (imageSize == ImageSize.Standard)
                {
                    return "/" + baseTargetImageFolder + targetId + "/" +
                        targetFile.Where(x => x.Name.Substring(x.Name.Length - 5)[0] != 's' ||
                        x.Name.Substring(x.Name.Length - 5)[0] != 'm').FirstOrDefault()?.Name;
                }
                else if (imageSize == ImageSize.Medium)
                {
                    return "/" + baseTargetImageFolder + targetId + "/" +
                       targetFile.Where(x => x.Name.Substring(x.Name.Length - 5)[0] == 'm').FirstOrDefault()?.Name;
                }
            }

            string targetEmptyImageFile = "empty";

            if (emptyImageSize == ImageSize.Small)
            {
                targetEmptyImageFile = targetEmptyImageFile + "s";
            }
            else if (emptyImageSize == ImageSize.Medium)
            {
                targetEmptyImageFile = targetEmptyImageFile + "m";
            }

            var targetEmptyImageInfo = _env.WebRootFileProvider.GetFileInfo(baseTargetImageFolder + targetEmptyImageFile +
                ".jpg");
            return targetEmptyImageInfo.Exists == true ? "/" + baseTargetImageFolder + targetEmptyImageInfo.Name : null;
        }

        public async Task<IEnumerable<VesselPositionViewModel>> AdvancedSearch(AdvancedSearchViewModel advancedSearch, bool isHost, IEnumerable<int> targets)
        {
            // var result = new List<VesselPositionViewModel>();
            var currentDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local);
            var endDate = currentDate.ToUniversalTime();
            var startDate = endDate.AddHours(-24);
            if (advancedSearch.TimeSpan == TargetTimeSpan.Hours_24)
            {
                startDate = endDate.AddHours(-24);
                //if (AppHelper.GetEnvironmentName().IsEqual(HostingEnvironment.LOCAL) == true)
                //{
                //    currentDate = DateTime.SpecifyKind(await ActorRefsConst.Global.Ask<DateTime>(new RequestTargetMonitorDateTime()),
                //       DateTimeKind.Local);
                //    endDate = currentDate.ToUniversalTime();

                //}
            }
            else if (advancedSearch.TimeSpan == TargetTimeSpan.Hours_48)
            {
                startDate = endDate.AddHours(-48);
                //if (AppHelper.GetEnvironmentName().IsEqual(HostingEnvironment.LOCAL) == true)
                //{
                //    currentDate = DateTime.SpecifyKind(await ActorRefsConst.Global.Ask<DateTime>(new RequestTargetMonitorDateTime()),
                //       DateTimeKind.Local);
                //    endDate = currentDate.ToUniversalTime();
                //    startDate = endDate.AddHours(-48);
                //}
            }
            else if (advancedSearch.TimeSpan == TargetTimeSpan.Days_10)
            {
                startDate = endDate.AddDays(-10);
            }
            else if (advancedSearch.TimeSpan == TargetTimeSpan.Days_30)
            {
                startDate = endDate.AddDays(-30);
            }
            else if (advancedSearch.TimeSpan == TargetTimeSpan.Days_90)
            {
                startDate = endDate.AddDays(-90);
            }
            else if (advancedSearch.TimeSpan == TargetTimeSpan.Days_365)
            {
                startDate = endDate.AddDays(-365);
            }
            //else
            //{
            //    //TODO: add support for other time span
            //    throw new UserFriendlyException("Selected Time Span are supported at this time. Other Time Span will be added later");
            //}
            Console.WriteLine($"Advanced Search || Universal || Start Date: {startDate.ToString()} || End Date: {endDate.ToString()}");
            return await _targetQuery.QueryAdvanceSearchMongoDBAsync(startDate, endDate, advancedSearch, isHost, targets);

        }
    }
}
