﻿using Flurl.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Model.ViewModels.Account;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.WebVessel.Tracking.Services.Concrete
{
    public class AccountService : IAccountService
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<AccountService> _logger;

        public AccountService(IConfiguration configuration, ILogger<AccountService> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public ClaimsPrincipal GetLoginClaimsPrincipal(string username,int userId)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, userId.ToString()),
                    new Claim(JwtRegisteredClaimNames.UniqueName,username),
                    new Claim(ClaimTypes.NameIdentifier, userId.ToString())
                };

            var userIdentity = new ClaimsIdentity(claims, "login");
            return new ClaimsPrincipal(userIdentity);
        }

        public async Task<AbpTokenRequestResponse> RequestToken(string issuerUri, string username, string tenantName,string password)
        {
            _logger.LogInformation($"Issueing token from {issuerUri}");

            try
            {
                //TODO : Change header abp.tenantid
                return await issuerUri
                    .WithHeaders(new { Accept = "text/plain", User_Agent = "Flurl", ContentType = "application/json" })
                    .WithHeader("Abp.TenantId", "1")
                    .PostJsonAsync(new
                    {
                        userNameOrEmailAddress = username,
                        password = password,
                        tenantName = tenantName
                    }).ReceiveJson<AbpTokenRequestResponse>();
            }
            catch (Exception)
            {
                _logger.LogError($"Token request failed");
                return null;
            }

        }

        public bool ValidateToken(string accessToken)
        {
            _logger.LogInformation("Validating token..");
            var validationParameters = new TokenValidationParameters
            {
                ValidIssuer = _configuration["Authentication:JwtBearer:Issuer"],
                ValidAudience = _configuration["Authentication:JwtBearer:Audience"],
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Authentication:JwtBearer:SecurityKey"]))
            };

            try
            {
                new JwtSecurityTokenHandler().ValidateToken(accessToken, validationParameters, out SecurityToken securityToken);
                _logger.LogInformation("Token is valid");
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                return false;
            }
        }
    }
}
