﻿using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.DTO.Reporting;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.WebAdmin.Zones.Dto;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.WebVessel.Tracking.Services.Concrete
{
    public class ReportingService : IReportingService
    {
        private readonly VtsDatabaseContext _vtsDatabaseContext;
        public ReportingService(VtsDatabaseContext vtsDatabaseContext)
        {
            _vtsDatabaseContext = vtsDatabaseContext;
        }

        //public async Task<ArrivalDepartureReportDto> GetArrivalDepartureReportAsync(DateTime dateArrival, DateTime dateDeparture, ZoneDto zone)
        //{
        //    if (dateArrival > dateDeparture)
        //    {
        //        throw new UserFriendlyException("Arrival Date cannot be higher than Departure Date");
        //    }
        //    var coordinates = new List<PointF>();
        //    foreach (var item in zone?.ZoneItems)
        //    {
        //        coordinates.Add(new PointF
        //        {
        //            X = (float)item.Latitude,
        //            Y = (float)item.Logitude
        //        });
        //    }
        //    //TODO: add condition with zone id if VTS points to production server
        //    var result = new ArrivalDepartureReportDto();
        //    var eventsArrival = _vtsDatabaseContext.Events.Include(x => x.Alarm).Where(x => x.TimeStmp >= dateArrival && x.TimeStmp <= dateDeparture &&
        //                                        x.Alarm.AlarmName.IndexOf(AlarmNameEvent.Vessel_Arrival, StringComparison.OrdinalIgnoreCase) >= 0 ||
        //                                          x.Alarm.AlarmName.IndexOf(AlarmNameEvent.Vessel_Arrived, StringComparison.OrdinalIgnoreCase) >= 0 &&
        //                                          CommonHelper.IsPointInPolygon(coordinates.ToArray(), new PointF((float)x.Lat, (float)x.Lng))).Select(x=> new ArrivalDepartureReportItemDto {
        //                                              ArrivalTime = x.TimeStmp,
        //                                              CallSign = x.CallSign,
        //                                              MMSI = x.MMSI,
        //                                              Name = x.ShipName
        //                                          });

        //    var eventsDeparture = _vtsDatabaseContext.Events.Include(x => x.Alarm).Where(x => x.TimeStmp >= dateArrival && x.TimeStmp <= dateDeparture &&
        //                                        x.Alarm.AlarmName.IndexOf(AlarmNameEvent.Vessel_Departed, StringComparison.OrdinalIgnoreCase) >= 0 ||
        //                                          x.Alarm.AlarmName.IndexOf(AlarmNameEvent.Vessel_Departure, StringComparison.OrdinalIgnoreCase) >= 0 &&
        //                                          CommonHelper.IsPointInPolygon(coordinates.ToArray(), new PointF((float)x.Lat, (float)x.Lng))).Select(x => new ArrivalDepartureReportItemDto
        //                                          {
        //                                              ArrivalTime = x.TimeStmp,
        //                                              CallSign = x.CallSign,
        //                                              MMSI = x.MMSI,
        //                                              Name = x.ShipName
        //                                          });

        //    result.Arrival = await eventsArrival.ToListAsync();
        //    result.Departure = await eventsDeparture.ToListAsync();

        //    return result;
        //}
    }
}
