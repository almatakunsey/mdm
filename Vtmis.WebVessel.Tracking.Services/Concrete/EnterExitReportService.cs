﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.DTO;
using Vtmis.Core.Model.DTO.Report;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.WebVessel.Tracking.Services.Concrete
{
    public class EnterExitReportService : IEnterExitReportService
    {
        private readonly IReportREpository _report;

        public EnterExitReportService(IReportREpository report)
        {
            _report = report;
        }

        public async Task<IEnumerable<ResponseReportEnterExit>> GetResponseReportEnterExit(RequestReportEnterExit input)
        {
            var result = new List<ResponseReportEnterExit>();

            foreach (var responseDate in CommonHelper.EachDay(input.EnterTime, input.ExitTime))
            {
                _report.Dapper.ConnectionString = AppHelper.GetAisConnectionString(responseDate);
                result.AddRange(await _report.QueryReportEnterExit(input));
            }
            return result;

        }

        public async Task<IEnumerable<ResponseArrivalDeparture>> GetReportArrivalDeparture(RequestReportEnterExit input)
        {
            var result = new List<ResponseArrivalDeparture>();

            foreach (var responseDate in CommonHelper.EachDay(input.EnterTime, input.ExitTime))
            {
                _report.Dapper.ConnectionString = AppHelper.GetAisConnectionString(responseDate);
                result.AddRange(await _report.QueryReportArrivalDeparture(input));
            }
            return result;

        }
    }
}
