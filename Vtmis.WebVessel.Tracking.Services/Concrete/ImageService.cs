﻿using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Linq;
using Vtmis.Core.Common.Enums;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.WebVessel.Tracking.Services.Concrete
{
    public class ImageService : IImageService
    {
        private readonly IHostingEnvironment _env;
        public ImageService(IHostingEnvironment env)
        {
            _env = env;
        }

        public string GetTargetImage(int targetId, ImageSize imageSize, ImageSize emptyImageSize)
        {
            var baseTargetImageFolder = "images/targets/vessels/";

            var targetImageFolder = _env.WebRootFileProvider.GetDirectoryContents(baseTargetImageFolder + targetId);

            if (targetImageFolder.Where(x => x.Exists == true).Any())
            {
                var targetFile = targetImageFolder.Where(x => x.Exists == true);

                if (imageSize == ImageSize.Small)
                {
                    return "/" + baseTargetImageFolder + targetId + "/" +
                        targetFile.Where(x => x.Name.Substring(x.Name.Length - 5)[0] == 's').FirstOrDefault()?.Name;
                }
                else if (imageSize == ImageSize.Standard)
                {
                    return "/" + baseTargetImageFolder + targetId + "/" +
                        targetFile.Where(x => x.Name.Substring(x.Name.Length - 5)[0] != 's' ||
                        x.Name.Substring(x.Name.Length - 5)[0] != 'm').FirstOrDefault()?.Name;
                }
                else if (imageSize == ImageSize.Medium)
                {
                    return "/" + baseTargetImageFolder + targetId + "/" +
                       targetFile.Where(x => Path.GetFileNameWithoutExtension(x.Name).Substring(Path.GetFileNameWithoutExtension(x.Name).Length - 1) == "m").FirstOrDefault()?.Name;
                }
            }

            string targetEmptyImageFile = "empty";

            if (emptyImageSize == ImageSize.Small)
            {
                targetEmptyImageFile = targetEmptyImageFile + "s";
            }
            else if (emptyImageSize == ImageSize.Medium)
            {
                targetEmptyImageFile = targetEmptyImageFile + "m";
            }

            var targetEmptyImageInfo = _env.WebRootFileProvider.GetFileInfo(baseTargetImageFolder + targetEmptyImageFile +
                ".jpg");
            return targetEmptyImageInfo.Exists == true ? "/" + baseTargetImageFolder + targetEmptyImageInfo.Name : null;
        }
    }
}
