﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.AkkaModel.Messages.Route;
using Vtmis.Core.Model.ViewModels.Route.Dto;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.WebVessel.Tracking.Services.Concrete
{
    public class RouteService : IRouteService
    {
        private readonly IDapperAisRepository _aisRepo;
        private readonly ITargetQuery _targetQuery;

        public RouteService(IDapperAisRepository aisRepo, ITargetQuery targetQuery)
        {
            _aisRepo = aisRepo;
            _targetQuery = targetQuery;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<ResponseVesselEta> VesselEtaAsync(RequestVesselEta input)
        {
            //_aisRepo.Dapper.ConnectionString = AppHelper.GetAisConnectionString(input.RequestDate);
            var minSog = input.Route.Waypoints.Min(m => m.MinSpeed);
            var maxSog = input.Route.Waypoints.Max(m => m.MaxSpeed);

            var qry = await _targetQuery.QueryVesselEta(input.MMSI, input.RequestDate, 10, minSog, maxSog);
            if (qry == null)
            {
                return new ResponseVesselEta();
            }

            // Start Determine which waypoint does the vessels currently in
            var vesselsCurrentCheckpoint = new Dictionary<int, IEnumerable<int>>(); // key=> waypointId, value=> MMSI
            for (int i = 0; i < input.Route.Waypoints.Count; i++)
            {
                var pointA = new PointF
                {
                    X = (float)input.Route.Waypoints[i].Longitude,
                    Y = (float)input.Route.Waypoints[i].Latitude
                };
                var pointB = new PointF();
                if (i >= input.Route.Waypoints.Count - 1)
                {
                    pointB.X = (float)input.Route.Waypoints[i - 1].Longitude;
                    pointB.Y = (float)input.Route.Waypoints[i - 1].Latitude;
                }
                else
                {
                    pointB.X = (float)input.Route.Waypoints[i + 1].Longitude;
                    pointB.Y = (float)input.Route.Waypoints[i + 1].Latitude;
                }

                var vessels = new List<int>();
                if (CommonHelper.IsInRange(new PointF { X = float.Parse(qry.Longitude, CultureInfo.InvariantCulture.NumberFormat), Y = float.Parse(qry.Latitude, CultureInfo.InvariantCulture.NumberFormat) }, pointA
                        , pointB, input.Route.Waypoints[i].XteLeft, Unit.Meters) ||
                    CommonHelper.IsInRange(new PointF { X = float.Parse(qry.Longitude, CultureInfo.InvariantCulture.NumberFormat), Y = float.Parse(qry.Latitude, CultureInfo.InvariantCulture.NumberFormat) }, pointA
                        , pointB, input.Route.Waypoints[i].XteRight, Unit.Meters))
                {
                    vessels.Add(qry.MMSI);
                }

                vesselsCurrentCheckpoint.Add(input.Route.Waypoints[i].Id, vessels); // arrived in two waypoints because the range of waypoints are close
            }
            // End Determine which waypoint does the vessels currently in

            //Start get vessel info
            var waypointsNames = input.Route.Waypoints.Select(x => new ResponseWaypointRouteEtaDto
            {
                Id = x.Id,
                Name = x.Name,
                Order = x.Order
            }).OrderBy(o => o.Order).ToList();

            qry.Waypoints = input.Route.Waypoints.Select(w => new ResponseWaypointRouteEtaDto
            {
                Id = w.Id,
                Name = w.Name,
                IsArrived = vesselsCurrentCheckpoint.Any(a => a.Key == w.Id && a.Value.Any(ia =>
                    ia == qry.MMSI)),
                Distance = w.ActualWpDistance,
                IsShown = w.IsShown,
                Order = w.Order
            }).ToList();
            //End get vessel info

            var isWaypointAscending = true;
            var item = qry;
            var arrivedVessels = item.Waypoints.Where(x => x.IsArrived == true).ToList();
            var distList = new Dictionary<int, double>();
            for (int i = 0; i < arrivedVessels.Count(); i++)
            {
                if (arrivedVessels.Count() > 1)
                {
                    var wpPoint = new PointF
                    {
                        X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == arrivedVessels[i].Id).Longitude,
                        Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == arrivedVessels[i].Id).Latitude
                    };
                    var dist = CommonHelper.GetDistanceBetweenTwoCoordinates(new PointF
                    {
                        X = float.Parse(item.Longitude, CultureInfo.InvariantCulture.NumberFormat)
                        ,
                        Y = float.Parse(item.Latitude, CultureInfo.InvariantCulture.NumberFormat)
                    }, wpPoint);

                    distList.Add(arrivedVessels[i].Id, dist);
                    //if (i > 0)
                    //{                               
                    //    //item.Waypoints.FirstOrDefault(x => x.Id == arrivedVessels[i].Id).IsArrived = false;                                 
                    //}
                }
            }

            if (distList.Any())
            {
                Console.WriteLine("duplicate isarrived");
                var minDist = distList.Min(x => x.Value);
                var keepWp = distList.FirstOrDefault(x => x.Value == minDist);
                item.Waypoints = item.Waypoints.Select(x => { x.IsArrived = false; return x; }).ToList();
                item.Waypoints.FirstOrDefault(x => x.Id == keepWp.Key).IsArrived = true;
            }

            var arrivedWaypoint = item.Waypoints.FirstOrDefault(x => x.IsArrived);
            var wpData = input.Route.Waypoints.First(x => x.Id == arrivedWaypoint.Id);
            var prevWpData = input.Route.Waypoints.Where(w => w.Id != wpData.Id && w.Order < wpData.Order).OrderByDescending(o => o.Order).FirstOrDefault();
            var nextWpData = input.Route.Waypoints.Where(w => w.Id != wpData.Id && w.Order > wpData.Order).OrderBy(o => o.Order).FirstOrDefault();
            var waypointBearing = nextWpData != null ? CommonHelper.DegreeBearing(wpData.Latitude, wpData.Longitude, nextWpData.Latitude, nextWpData.Longitude)
                                    : CommonHelper.DegreeBearing(prevWpData.Latitude, prevWpData.Longitude, wpData.Latitude, wpData.Longitude);
            isWaypointAscending = CommonHelper.IsWaypointDirectionAsc(item.TrueHeading, waypointBearing);

            //End remove additional IsArrived


            //Start calculate eta
            //foreach (var vessel in finalResults)
            //{
            //Start Determine if the vessel is going from start to end or vice versa
            //var wpData = input.Route.Waypoints.First();
            //var nextWpData = input.Route.Waypoints.Where(w => w.Id != input.Route.Waypoints.FirstOrDefault().Id && w.Id > input.Route.Waypoints.FirstOrDefault().Id).OrderBy(o => o.Id).FirstOrDefault();
            //var waypointBearing = CommonHelper.DegreeBearing(wpData.Latitude, wpData.Longitude, nextWpData.Latitude, nextWpData.Longitude);
            //var isWaypointAscending = CommonHelper.IsWaypointDirectionAsc(vessel.COG, waypointBearing);

            if (isWaypointAscending == false)
            {
                item.Waypoints = item.Waypoints.OrderByDescending(o => o.Order).ToList();
            }
            else
            {
                item.Waypoints = item.Waypoints.OrderBy(o => o.Order).ToList();
            }
            //End Determine if the vessel is going from start to end or vice versa

            DateTime? eta = null;
            bool alreadyArrived = false; // 
            bool reached = false;
            var wpIndex = 0;
            double allDist = 0;
            foreach (var wpCheckpoint in item.Waypoints)
            {
                if (eta == null)
                {
                    eta = DateTime.Now;
                }

                if (wpCheckpoint.IsArrived || alreadyArrived)
                {
                    //Start calculate distance

                    var ptAforEta = new PointF();
                    var ptBforEta = new PointF
                    {
                        X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Longitude,
                        Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Latitude
                    };

                    if (wpIndex == 0)
                    {
                        ptAforEta.X = float.Parse(item.Longitude, CultureInfo.InvariantCulture.NumberFormat);
                        ptAforEta.Y = float.Parse(item.Latitude, CultureInfo.InvariantCulture.NumberFormat);
                    }
                    else
                    {
                        ptAforEta.X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == item.Waypoints[wpIndex - 1].Id).Longitude;
                        ptAforEta.Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == item.Waypoints[wpIndex - 1].Id).Latitude;
                    }

                    var ptA = new PointF
                    {
                        X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Longitude,
                        Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Latitude
                    };

                    var ptB = new PointF();

                    if (wpIndex >= (item.Waypoints.Count - 1)) // if final waypoint. Take current waypoint
                    {
                        ptB.X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Longitude;
                        ptB.Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Latitude;
                    }
                    else // else Take next waypoint
                    {
                        ptB.X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == item.Waypoints[wpIndex + 1].Id).Longitude;
                        ptB.Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == item.Waypoints[wpIndex + 1].Id).Latitude;
                    }
                    //start use vessel speed for first time only. else use waypoint max speed for eta calculation
                    var sog = item.SOG;
                    if (alreadyArrived)
                    {
                        sog = input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).MaxSpeed;
                    }
                    //end use vessel speed for first time only. else use waypoint max speed for eta calculation
                    if (reached)
                    {
                        //Start set vessel coordinate
                        ptA.X = float.Parse(item.Longitude, CultureInfo.InvariantCulture.NumberFormat);
                        ptA.Y = float.Parse(item.Latitude, CultureInfo.InvariantCulture.NumberFormat);
                        ptAforEta.X = float.Parse(item.Longitude, CultureInfo.InvariantCulture.NumberFormat);
                        ptAforEta.Y = float.Parse(item.Latitude, CultureInfo.InvariantCulture.NumberFormat);
                        //End set vessel coordinate
                        reached = false;
                    }

                    allDist = allDist + CommonHelper.GetDistanceBetweenTwoCoordinates(ptAforEta, ptBforEta, Unit.NauticalMiles);
                    wpCheckpoint.Distance = CommonHelper.GetDistanceBetweenTwoCoordinates(ptA, ptB, Unit.NauticalMiles);
                    //End calculate distance

                    //TODO: handle if speed = 0, if not..will get undefined result
                    if (alreadyArrived)
                    {
                        item.Waypoints[wpIndex].ETA = CommonHelper.GetETAByDistance(DateTime.Now, sog.Value, allDist);
                        item.Waypoints[wpIndex].Aging = CommonHelper.GetEstimationAging(item.Waypoints[wpIndex].ETA.Value);
                        eta = item.Waypoints[wpIndex].ETA;
                    }

                    if (alreadyArrived == false)
                    {
                        reached = true;
                        alreadyArrived = true;
                        //wpIndex += 1;
                        //continue;
                    }



                }

                wpIndex += 1;
            }

            if (item.Waypoints.Any(x => x.IsShown == false))
            {
                // Start filter visible route only
                alreadyArrived = false; // 
                var allRoutes = new List<ResponseWaypointRouteEtaDto>(item.Waypoints);
                wpIndex = 0;
                reached = false;
                double preserveDistance = 0;
                DateTime? preserveEta = null;
                int? preserveIndex = null;
                foreach (var wpCheckpoint in allRoutes)
                {
                    if (wpCheckpoint.IsArrived || alreadyArrived)
                    {
                        if (alreadyArrived == false)
                        {
                            alreadyArrived = true;
                            reached = true;
                        }

                        if (reached)
                        {
                            reached = false;
                            //wpIndex += 1;
                            //continue;
                        }

                        var isNextPointVisible = true;
                        if ((wpIndex >= (allRoutes.Count - 1)) == false)
                        {
                            isNextPointVisible = allRoutes[wpIndex + 1].IsShown;
                        }

                        preserveDistance = preserveDistance + wpCheckpoint.Distance;
                        if (preserveEta == null)
                        {
                            preserveEta = wpCheckpoint.ETA;
                        }
                        var diffDate = preserveEta.Value.Subtract(wpCheckpoint.ETA.Value);
                        preserveEta.Value.Add(diffDate);

                        if (wpCheckpoint.IsShown)
                        {
                            preserveIndex = wpIndex;
                        }

                        if (isNextPointVisible == true)
                        {
                            //var d = ObjectMapper.Map<WaypointDto>(waypoints[i]);
                            int? n = null;
                            if (preserveIndex.HasValue)
                            {
                                n = preserveIndex;

                            }
                            else
                            {
                                n = wpIndex;
                            }
                            //item.Waypoints[n.Value].Distance = preserveDistance;
                            item.Waypoints[n.Value].ETA = preserveEta;
                            item.Waypoints[n.Value].Aging = CommonHelper.GetEstimationAging(item.Waypoints[n.Value].ETA.Value);
                            preserveIndex = null;
                            //distanceList.Add(d);
                            preserveDistance = 0;
                            preserveEta = null;
                        }
                    }

                    wpIndex += 1;
                }
                // End filter visible route only
            }

            item.Waypoints = item.Waypoints.OrderBy(o => o.Order).ToList();
            //End calculate eta

            return qry;
        }

        /// <summary>
        /// Calculate Route ETA
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ResponseRouteEtaDto>> RouteEtaAsync(RequestRouteEta input)
        {
            var routeEta = new List<ResponseRouteEtaDto>();
            //_aisRepo.Dapper.ConnectionString = AppHelper.GetAisConnectionString(input.RequestDate);

            var minSog = input.Route.Waypoints.Min(m => m.MinSpeed);
            var maxSog = input.Route.Waypoints.Max(m => m.MaxSpeed);

            var qry = await _targetQuery.QueryRouteEta(input.RequestDate, 30, minSog, maxSog);

            //#####################################################################################################################
            // Start Determine which waypoint does the vessels currently in
            var vesselsCurrentCheckpoint = new Dictionary<int, IEnumerable<int>>(); // key=> waypointId, value=> MMSI
            for (int i = 0; i < input.Route.Waypoints.Count; i++)
            {
                var pointA = new PointF
                {
                    X = (float)input.Route.Waypoints[i].Longitude,
                    Y = (float)input.Route.Waypoints[i].Latitude
                };
                var pointB = new PointF();
                if (i >= input.Route.Waypoints.Count - 1)
                {
                    pointB.X = (float)input.Route.Waypoints[i - 1].Longitude;
                    pointB.Y = (float)input.Route.Waypoints[i - 1].Latitude;
                }
                else
                {
                    pointB.X = (float)input.Route.Waypoints[i + 1].Longitude;
                    pointB.Y = (float)input.Route.Waypoints[i + 1].Latitude;
                }

                var vessels = qry.Where(w =>
                    CommonHelper.IsInRange(new PointF { X = (float)w.Longitude, Y = (float)w.Latitude }, pointA
                        , pointB, input.Route.Waypoints[i].XteLeft, Unit.Meters) ||
                    CommonHelper.IsInRange(new PointF { X = (float)w.Longitude, Y = (float)w.Latitude }, pointA
                        , pointB, input.Route.Waypoints[i].XteRight, Unit.Meters)
                ).Select(x => x.MMSI).ToList();

                vesselsCurrentCheckpoint.Add(input.Route.Waypoints[i].Id, vessels); // arrived in two waypoints because the range of waypoints are close
            }

            // End Determine which waypoint does the vessels currently in

            //Start get vessel info            
            var waypointsNames = input.Route.Waypoints.Select(x => new ResponseWaypointRouteEtaDto
            {
                Id = x.Id,
                Name = x.Name,
                Order = x.Order
            }).OrderBy(o => o.Order).ToList();
            var finalResults = qry.Where(x => vesselsCurrentCheckpoint.Any(a => a.Value.Any(v => v == x.MMSI)))
                                .Select(x => new ResponseRouteEtaDto
                                {
                                    TrueHeading = x.TrueHeading,
                                    COG = x.COG,
                                    Latitude = x.Latitude.ToStringOrDefault(),
                                    Longitude = x.Longitude.ToStringOrDefault(),
                                    LocalRecvTime = x.LocalRecvTime,
                                    MMSI = x.MMSI,
                                    Name = x.Name,
                                    ShipTypeId = x.ShipType,
                                    SOG = x.SOG,
                                    Waypoints = input.Route.Waypoints.Select(w => new ResponseWaypointRouteEtaDto
                                    {
                                        Id = w.Id,
                                        Name = w.Name,
                                        IsArrived = vesselsCurrentCheckpoint.Any(a => a.Key == w.Id && a.Value.Any(ia =>
                                            ia == x.MMSI)),
                                        Distance = w.ActualWpDistance,
                                        IsShown = w.IsShown,
                                        Order = w.Order
                                    }).ToList()
                                }).OrderBy(o => o.LocalRecvTime).ToList();
            //End get vessel info

            if (finalResults.Any())
            {
                var isWaypointAscending = true;

                //Start remove additional IsArrived
                foreach (var item in finalResults)
                {
                    var arrivedVessels = item.Waypoints.Where(x => x.IsArrived == true).ToList();
                    var distList = new Dictionary<int, double>();
                    for (int i = 0; i < arrivedVessels.Count(); i++)
                    {
                        if (arrivedVessels.Count() > 1)
                        {
                            var wpPoint = new PointF
                            {
                                X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == arrivedVessels[i].Id).Longitude,
                                Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == arrivedVessels[i].Id).Latitude
                            };
                            var dist = CommonHelper.GetDistanceBetweenTwoCoordinates(new PointF
                            {
                                X = float.Parse(item.Longitude, CultureInfo.InvariantCulture.NumberFormat)
                                ,
                                Y = float.Parse(item.Latitude, CultureInfo.InvariantCulture.NumberFormat)
                            }, wpPoint);

                            distList.Add(arrivedVessels[i].Id, dist);
                            //if (i > 0)
                            //{                               
                            //    //item.Waypoints.FirstOrDefault(x => x.Id == arrivedVessels[i].Id).IsArrived = false;                                 
                            //}
                        }
                    }

                    if (distList.Any())
                    {
                        Console.WriteLine("duplicate isarrived");
                        var minDist = distList.Min(x => x.Value);
                        var keepWp = distList.FirstOrDefault(x => x.Value == minDist);
                        item.Waypoints = item.Waypoints.Select(x => { x.IsArrived = false; return x; }).ToList();
                        item.Waypoints.FirstOrDefault(x => x.Id == keepWp.Key).IsArrived = true;
                    }

                    var arrivedWaypoint = item.Waypoints.FirstOrDefault(x => x.IsArrived);
                    var wpData = input.Route.Waypoints.First(x => x.Id == arrivedWaypoint.Id);
                    var prevWpData = input.Route.Waypoints.Where(w => w.Id != wpData.Id && w.Order < wpData.Order).OrderByDescending(o => o.Order).FirstOrDefault();
                    var nextWpData = input.Route.Waypoints.Where(w => w.Id != wpData.Id && w.Order > wpData.Order).OrderBy(o => o.Order).FirstOrDefault();
                    var waypointBearing = nextWpData != null ? CommonHelper.DegreeBearing(wpData.Latitude, wpData.Longitude, nextWpData.Latitude, nextWpData.Longitude)
                                            : CommonHelper.DegreeBearing(prevWpData.Latitude, prevWpData.Longitude, wpData.Latitude, wpData.Longitude);
                    isWaypointAscending = CommonHelper.IsWaypointDirectionAsc(item.TrueHeading, waypointBearing);

                    //End remove additional IsArrived


                    //Start calculate eta
                    //foreach (var vessel in finalResults)
                    //{
                    //Start Determine if the vessel is going from start to end or vice versa
                    //var wpData = input.Route.Waypoints.First();
                    //var nextWpData = input.Route.Waypoints.Where(w => w.Id != input.Route.Waypoints.FirstOrDefault().Id && w.Id > input.Route.Waypoints.FirstOrDefault().Id).OrderBy(o => o.Id).FirstOrDefault();
                    //var waypointBearing = CommonHelper.DegreeBearing(wpData.Latitude, wpData.Longitude, nextWpData.Latitude, nextWpData.Longitude);
                    //var isWaypointAscending = CommonHelper.IsWaypointDirectionAsc(vessel.COG, waypointBearing);

                    if (isWaypointAscending == false)
                    {
                        item.Waypoints = item.Waypoints.OrderByDescending(o => o.Order).ToList();
                    }
                    else
                    {
                        item.Waypoints = item.Waypoints.OrderBy(o => o.Order).ToList();
                    }
                    //End Determine if the vessel is going from start to end or vice versa

                    DateTime? eta = null;
                    bool alreadyArrived = false; // 
                    bool reached = false;
                    var wpIndex = 0;
                    double allDist = 0;
                    foreach (var wpCheckpoint in item.Waypoints)
                    {
                        if (eta == null)
                        {
                            eta = DateTime.Now;
                        }

                        if (wpCheckpoint.IsArrived || alreadyArrived)
                        {
                            //Start calculate distance

                            var ptAforEta = new PointF();
                            var ptBforEta = new PointF
                            {
                                X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Longitude,
                                Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Latitude
                            };

                            if (wpIndex == 0)
                            {
                                ptAforEta.X = float.Parse(item.Longitude, CultureInfo.InvariantCulture.NumberFormat);
                                ptAforEta.Y = float.Parse(item.Latitude, CultureInfo.InvariantCulture.NumberFormat);
                            }
                            else
                            {
                                ptAforEta.X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == item.Waypoints[wpIndex - 1].Id).Longitude;
                                ptAforEta.Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == item.Waypoints[wpIndex - 1].Id).Latitude;
                            }

                            var ptA = new PointF
                            {
                                X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Longitude,
                                Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Latitude
                            };

                            var ptB = new PointF();

                            if (wpIndex >= (item.Waypoints.Count - 1)) // if final waypoint. Take current waypoint
                            {
                                ptB.X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Longitude;
                                ptB.Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).Latitude;
                            }
                            else // else Take next waypoint
                            {
                                ptB.X = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == item.Waypoints[wpIndex + 1].Id).Longitude;
                                ptB.Y = (float)input.Route.Waypoints.FirstOrDefault(x => x.Id == item.Waypoints[wpIndex + 1].Id).Latitude;
                            }
                            //start use vessel speed for first time only. else use waypoint max speed for eta calculation
                            var sog = item.SOG;
                            if (alreadyArrived)
                            {
                                sog = input.Route.Waypoints.FirstOrDefault(x => x.Id == wpCheckpoint.Id).MaxSpeed;
                            }
                            //end use vessel speed for first time only. else use waypoint max speed for eta calculation
                            if (reached)
                            {
                                //Start set vessel coordinate
                                ptA.X = float.Parse(item.Longitude, CultureInfo.InvariantCulture.NumberFormat);
                                ptA.Y = float.Parse(item.Latitude, CultureInfo.InvariantCulture.NumberFormat);
                                ptAforEta.X = float.Parse(item.Longitude, CultureInfo.InvariantCulture.NumberFormat);
                                ptAforEta.Y = float.Parse(item.Latitude, CultureInfo.InvariantCulture.NumberFormat);
                                //End set vessel coordinate
                                reached = false;
                            }

                            allDist = allDist + CommonHelper.GetDistanceBetweenTwoCoordinates(ptAforEta, ptBforEta, Unit.NauticalMiles);
                            wpCheckpoint.Distance = CommonHelper.GetDistanceBetweenTwoCoordinates(ptA, ptB, Unit.NauticalMiles);
                            //End calculate distance

                            //TODO: handle if speed = 0, if not..will get undefined result
                            if (alreadyArrived)
                            {
                                item.Waypoints[wpIndex].ETA = CommonHelper.GetETAByDistance(DateTime.Now, sog.Value, allDist);
                                item.Waypoints[wpIndex].Aging = CommonHelper.GetEstimationAging(item.Waypoints[wpIndex].ETA.Value);
                                eta = item.Waypoints[wpIndex].ETA;
                            }

                            if (alreadyArrived == false)
                            {
                                reached = true;
                                alreadyArrived = true;
                                //wpIndex += 1;
                                //continue;
                            }



                        }

                        wpIndex += 1;
                    }

                    if (item.Waypoints.Any(x => x.IsShown == false))
                    {
                        // Start filter visible route only
                        alreadyArrived = false; // 
                        var allRoutes = new List<ResponseWaypointRouteEtaDto>(item.Waypoints);
                        wpIndex = 0;
                        reached = false;
                        double preserveDistance = 0;
                        DateTime? preserveEta = null;
                        int? preserveIndex = null;
                        foreach (var wpCheckpoint in allRoutes)
                        {
                            if (wpCheckpoint.IsArrived || alreadyArrived)
                            {
                                if (alreadyArrived == false)
                                {
                                    alreadyArrived = true;
                                    reached = true;
                                }

                                if (reached)
                                {
                                    reached = false;
                                    //wpIndex += 1;
                                    //continue;
                                }

                                var isNextPointVisible = true;
                                if ((wpIndex >= (allRoutes.Count - 1)) == false)
                                {
                                    isNextPointVisible = allRoutes[wpIndex + 1].IsShown;
                                }

                                preserveDistance = preserveDistance + wpCheckpoint.Distance;
                                if (preserveEta == null)
                                {
                                    preserveEta = wpCheckpoint.ETA;
                                }
                                var diffDate = preserveEta.Value.Subtract(wpCheckpoint.ETA.Value);
                                preserveEta.Value.Add(diffDate);

                                if (wpCheckpoint.IsShown)
                                {
                                    preserveIndex = wpIndex;
                                }

                                if (isNextPointVisible == true)
                                {
                                    //var d = ObjectMapper.Map<WaypointDto>(waypoints[i]);
                                    int? n = null;
                                    if (preserveIndex.HasValue)
                                    {
                                        n = preserveIndex;

                                    }
                                    else
                                    {
                                        n = wpIndex;
                                    }
                                    //item.Waypoints[n.Value].Distance = preserveDistance;
                                    item.Waypoints[n.Value].ETA = preserveEta;
                                    item.Waypoints[n.Value].Aging = CommonHelper.GetEstimationAging(item.Waypoints[n.Value].ETA.Value);
                                    preserveIndex = null;
                                    //distanceList.Add(d);
                                    preserveDistance = 0;
                                    preserveEta = null;
                                }
                            }

                            wpIndex += 1;
                        }
                        // End filter visible route only
                    }

                    item.Waypoints = item.Waypoints.OrderBy(o => o.Order).ToList();
                }
                //End calculate eta

                return finalResults;
            }

            // return if no result
            return new List<ResponseRouteEtaDto>();
        }

      
    }
}
