﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Model;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto;
using Vtmis.WebAdmin.Vts;
using Vtmis.WebAdmin.Vts.Dto;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.WebVessel.Tracking.Services.Concrete
{
    public class VtsVesselService : IVtsVesselService
    {
        private readonly VtsDatabaseContext _vtsDatabaseContext;
        private readonly IExtendedShipService _extendedShipService;
        private readonly IMapper _mapper;

        public VtsVesselService(VtsDatabaseContext vtsDatabaseContext,
            IExtendedShipService extendedShipService, IMapper mapper)
        {
            _vtsDatabaseContext = vtsDatabaseContext;
            _extendedShipService = extendedShipService;
            _mapper = mapper;
        }
        public async Task<bool> IsTargetExist(int? mmsi = null, int? imo = null, string callSign = null)
        {
            var result = _vtsDatabaseContext.Ships.AsQueryable();
            if (mmsi != null && mmsi.Value > 0)
            {
                var data = result.Where(x => x.MMSI == mmsi.Value);
                if (await data.FirstOrDefaultAsync() != null)
                {
                    return true;
                }
            }
            if (imo != null && imo.Value > 0)
            {
                var data = result.Where(x => x.IMO == imo.Value);
                if (await data.FirstOrDefaultAsync() != null)
                {
                    return true;
                }
            }
            if (!string.IsNullOrEmpty(callSign))
            {
                var data = result.Where(x => x.CallSign.ToUpper() == callSign.ToUpper());
                if (await data.FirstOrDefaultAsync() != null)
                {
                    return true;
                }
            }
            return false;
        }
        public async Task<ResponseExtendedShipList> CreateVesselAsync(CreateExtendedShipList input)
        {
            try
            {
                var ship = _mapper.Map<ExtendedShipList>(input);
                ship.CreatedDate = DateTime.Now;

                var isShipExist = _vtsDatabaseContext.ExtendedShipLists
                                .Where(x => string.Equals(x.IMO, input.IMO,
                                    StringComparison.CurrentCultureIgnoreCase) && (x.MMSI == input.MMSI) &&
                                    string.Equals(x.CallSign, input.CallSign,
                                    StringComparison.CurrentCultureIgnoreCase)
                                    && x.IsDeleted == false).Any();

                if (isShipExist)
                {
                    throw new AlreadyExistException("Ship Already Exist");
                }

                await _vtsDatabaseContext.ExtendedShipLists.AddAsync(ship);
                await _vtsDatabaseContext.SaveChangesAsync();
                return _mapper.Map<ResponseExtendedShipList>(ship);
            }
            catch (AlreadyExistException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task<ResponseExtendedShipList> UpdateVesselAsync(UpdateExtendedShipList input)
        {
            try
            {
                var ship = input.Id != 0 ? await _vtsDatabaseContext.ExtendedShipLists
                                .FirstOrDefaultAsync(x => x.Id == input.Id && x.IsDeleted == false) :
                            await _vtsDatabaseContext.ExtendedShipLists
                                .FirstOrDefaultAsync(x => string.Equals(x.IMO, input.IMO,
                                    StringComparison.CurrentCultureIgnoreCase) && (x.MMSI == input.MMSI) &&
                                    string.Equals(x.CallSign, input.CallSign,
                                    StringComparison.CurrentCultureIgnoreCase)
                                    && x.IsDeleted == false)
                                ;

                if (ship == null)
                {
                    throw new NotFoundException("Vessel Does Not Exist");
                }

                //var isShipExist = _vtsDatabaseContext.ExtendedShipLists
                //    .Where(x =>
                //        ((x.MMSI != 0 && x.MMSI == input.MMSI) ||
                //        (!string.IsNullOrEmpty(input.IMO) && (!string.IsNullOrEmpty(x.IMO)) && x.IMO.ToLower() == input.IMO.ToLower()) ||
                //        (!string.IsNullOrEmpty(input.CallSign) && (!string.IsNullOrEmpty(x.CallSign)) && x.CallSign.ToLower() == input.CallSign.ToLower())) &&
                //        x.IsDeleted == false && x.Id != input.Id
                //    ).Any();

                //if (isShipExist)
                //{
                //    throw new AlreadyExistException("Vessel Already Exist");
                //}

                ship = _mapper.Map<UpdateExtendedShipList, ExtendedShipList>(input, ship);
                ship.LastModifiedDate = DateTime.Now;

                _vtsDatabaseContext.Update(ship);
                await _vtsDatabaseContext.SaveChangesAsync();
                return _mapper.Map<ResponseExtendedShipList>(ship);
            }
            catch (AlreadyExistException err)
            {
                throw err;
            }
            catch (NotFoundException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err);
            }
        }

        public PaginatedList<ResponseExtendedShipList> GetAllShips(int pageIndex = 1, int pageSize = 10, bool includeDeletedShips = false)
        {
            try
            {
                var qry = _vtsDatabaseContext.ExtendedShipLists.AsQueryable();

                if (includeDeletedShips == false)
                {
                    qry = qry.Where(x => x.IsDeleted == includeDeletedShips);
                }
                qry = qry.OrderByDescending(o => o.CreatedDate);

                var results = new PaginatedList<ExtendedShipList>().CreateTemp(
                    qry, pageIndex, pageSize);

                return _mapper.Map<PaginatedList<ResponseExtendedShipList>>(results);
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err);
            }
        }

        public async Task<ResponseExtendedShipList> GetShip(int vesselId, int mmsi, string imo, string callSign)
        {
            try
            {
                var ship = vesselId != 0 ? await _vtsDatabaseContext.ExtendedShipLists
                               .FirstOrDefaultAsync(x => x.Id == vesselId && x.IsDeleted == false) :
                           await _vtsDatabaseContext.ExtendedShipLists
                               .FirstOrDefaultAsync(x => string.Equals(x.IMO, imo, StringComparison.CurrentCultureIgnoreCase)
                               && (x.MMSI == mmsi) &&
                                   string.Equals(x.CallSign, callSign, StringComparison.CurrentCultureIgnoreCase)
                                   && x.IsDeleted == false)
                               ;

                if (ship == null)
                {
                    throw new NotFoundException("Vessel does not exist");
                }
                return _mapper.Map<ResponseExtendedShipList>(ship);
            }
            catch (NotFoundException err)
            {
                throw new NotFoundException(err.Message);
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err);
            }
        }

        public async Task<Ship> GetShipByShipIdAsync(int shipId)
        {
            var checkExtendedShipDataExist = await _extendedShipService.GetByShipId(shipId);
            if (checkExtendedShipDataExist == null)
            {
                return _vtsDatabaseContext.Ships.FirstOrDefault(x => x.ShipID == shipId);
            }
            else
            {
                if (checkExtendedShipDataExist.IsDeleted)
                {
                    return null;
                }
                return _vtsDatabaseContext.Ships.FirstOrDefault(x => x.ShipID == shipId);
            }
        }


        public async Task SoftDeleteVesselAsync(int vesselId, int mmsi, string imo, string callSign)
        {
            try
            {
                var ship = vesselId != 0 ? await _vtsDatabaseContext.ExtendedShipLists
                              .FirstOrDefaultAsync(x => x.Id == vesselId && x.IsDeleted == false) :
                          await _vtsDatabaseContext.ExtendedShipLists
                              .FirstOrDefaultAsync(x => string.Equals(x.IMO, imo, StringComparison.CurrentCultureIgnoreCase)
                              && (x.MMSI == mmsi) &&
                                  string.Equals(x.CallSign, callSign, StringComparison.CurrentCultureIgnoreCase)
                                  && x.IsDeleted == false)
                              ;

                if (ship == null)
                {
                    throw new NotFoundException("Vessel does not exist");
                }
                ship.IsDeleted = true;
                ship.DeletedDate = DateTime.Now;

                _vtsDatabaseContext.ExtendedShipLists.Update(ship);
                await _vtsDatabaseContext.SaveChangesAsync();
            }
            catch (NotFoundException err)
            {
                throw new NotFoundException(err.Message);
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err);
            }
        }

        public async Task BlackListVesselAsync(int vesselId, int mmsi, string imo, string callSign)
        {
            try
            {
                var ship = vesselId != 0 ? await _vtsDatabaseContext.ExtendedShipLists
                              .FirstOrDefaultAsync(x => x.Id == vesselId && x.IsDeleted == false) :
                          await _vtsDatabaseContext.ExtendedShipLists
                              .FirstOrDefaultAsync(x => string.Equals(x.IMO, imo, StringComparison.CurrentCultureIgnoreCase)
                              && (x.MMSI == mmsi) &&
                                  string.Equals(x.CallSign, callSign, StringComparison.CurrentCultureIgnoreCase)
                                  && x.IsDeleted == false)
                              ;

                if (ship == null)
                {
                    throw new NotFoundException("Vessel does not exist");
                }

                ship.IsBlackList = true;
                _vtsDatabaseContext.ExtendedShipLists.Update(ship);
                await _vtsDatabaseContext.SaveChangesAsync();
            }
            catch (NotFoundException err)
            {
                throw new NotFoundException(err.Message);
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err);
            }
        }

        public async Task<string> GetTargetPhotoFilenameByMmsiAsync(int? mmsi)
        {
            if (mmsi == null)
            {
                return null;
            }

            var result = await _vtsDatabaseContext.Photos.FirstOrDefaultAsync(x => x.MMSI == mmsi);
            if (result != null)
            {
                return result.FileName;
            }
            return null;
        }

        public string GetFlagNameByMmsi(int? mmsi)
        {
            if (mmsi == null)
            {
                return null;
            }
            var flagId = Convert.ToInt32(mmsi.ToString().Substring(0, 3));
            var result = _vtsDatabaseContext.Flags.FirstOrDefault(x => x.FlagID == flagId);
            if (result != null)
            {
                return result.FlagName;
            }
            return null;
        }

        public async Task<VesselClearance> GetVesselClearanceByTargetIdAsync(int targetId)
        {
            DateTime? ata = null;
            DateTime? atd = null;
            //DateTime? eta = null;

            var vesselWithDeparture = await _vtsDatabaseContext.Events.Include(i => i.Alarm).OrderByDescending(x => x.TimeStmp)
                .FirstOrDefaultAsync(x => x.MMSI == targetId &&
                (x.Alarm.AlarmName.IndexOf(AlarmNameEvent.Vessel_Departure, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                    x.Alarm.AlarmName.IndexOf(AlarmNameEvent.Vessel_Departed, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                    x.Alarm.AlarmName.IndexOf(AlarmNameEvent.Vessel_Exit, StringComparison.OrdinalIgnoreCase) >= 0));

            var vesselWithArrival = await _vtsDatabaseContext.Events.Include(i => i.Alarm).OrderByDescending(x => x.TimeStmp)
               .FirstOrDefaultAsync(x => x.MMSI == targetId &&
               (x.Alarm.AlarmName.IndexOf(AlarmNameEvent.Vessel_Arrived, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                   x.Alarm.AlarmName.IndexOf(AlarmNameEvent.Vessel_Arrival, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                   x.Alarm.AlarmName.IndexOf(AlarmNameEvent.Vessel_Entry, StringComparison.OrdinalIgnoreCase) >= 0));

            var result = new Event();

            if (vesselWithArrival != null && vesselWithDeparture != null)
            {
                if (vesselWithArrival.TimeStmp > vesselWithDeparture.TimeStmp)
                {
                    ata = vesselWithArrival.TimeStmp;
                    result = vesselWithArrival;
                }
                else
                {
                    atd = vesselWithDeparture.TimeStmp;
                    result = vesselWithDeparture;
                }
            }
            else if (vesselWithArrival != null && vesselWithDeparture == null)
            {
                ata = vesselWithArrival.TimeStmp;
                result = vesselWithArrival;
            }
            else if (vesselWithArrival == null && vesselWithDeparture != null)
            {
                atd = vesselWithDeparture.TimeStmp;
                result = vesselWithDeparture;
            }

            if (vesselWithArrival == null && vesselWithDeparture == null)
            {
                return null;
            }

            //var result = vesselWithDeparture ?? vesselWithArrival;

            return new VesselClearance
            {
                CallSign = result.CallSign,
                UTC = result.TimeStmp,
                LocalRecvTime = result.TimeStmp,
                MMSI = result.MMSI,
                ShipName = result.ShipName,
                Aging = result.TimeStmp,
                Info = result.Info,
                SOG = result.SOG,
                AlarmID = result.AlarmId,
                Status = result.Alarm?.AlarmMsg,
                ATA = ata,
                ATD = atd,
                TimeStamp = result.TimeStmp,
                Flag = GetFlagNameByMmsi(result.MMSI),
                Photo = await GetTargetPhotoFilenameByMmsiAsync(result.MMSI),
                Zone = (await _vtsDatabaseContext.Zones.FirstOrDefaultAsync(x => x.ZoneID == result.ZoneId))?
                        .ZoneName
            };
        }
        //TODO: No IMO in table Events (VTS)
        public async Task<VesselClearance> GetVesselClearanceByTargetMmsiImoCallSignAsync(int? mmsi = null, int? imo = null, string callSign = null)
        {

            //var vessel = _vtsDatabaseContext.Events.Include(i => i.Alarm).FirstOrDefault(x => x.MMSI == mmsi && x.CallSign == callSign);
            var vessel = _vtsDatabaseContext.Events.AsQueryable();
            if (mmsi != null && mmsi.Value > 0)
            {
                vessel = vessel.Where(x => x.MMSI == mmsi.Value);
            }
            if (imo != null && imo.Value > 0)
            {
                //result = result.Where(x => x.imo == mmsi);
            }
            if (!string.IsNullOrEmpty(callSign))
            {
                vessel = vessel.Where(x => x.CallSign.ToUpper() == callSign.ToUpper());
            }


            DateTime? ata = null;
            DateTime? atd = null;
            //DateTime? eta = null;

            var vesselWithDeparture = await vessel.Include(i => i.Alarm).OrderByDescending(x => x.TimeStmp)
                .FirstOrDefaultAsync(x => x.AlarmId == 5 || x.AlarmId == 8);

            var vesselWithArrival = await vessel.Include(i => i.Alarm).OrderByDescending(x => x.TimeStmp)
               .FirstOrDefaultAsync(x => x.AlarmId == 4 || x.AlarmId == 7);

            var result = new Event();

            if (vesselWithArrival != null && vesselWithDeparture != null)
            {
                if (vesselWithArrival.TimeStmp > vesselWithDeparture.TimeStmp)
                {
                    ata = vesselWithArrival.TimeStmp;
                    result = vesselWithArrival;
                }
                else
                {
                    ata = vesselWithArrival.TimeStmp;
                    atd = vesselWithDeparture.TimeStmp;
                    result = vesselWithDeparture;
                }
            }
            else if (vesselWithArrival != null && vesselWithDeparture == null)
            {
                ata = vesselWithArrival.TimeStmp;
                result = vesselWithArrival;
            }
            else if (vesselWithArrival == null && vesselWithDeparture != null)
            {
                atd = vesselWithDeparture.TimeStmp;
                result = vesselWithDeparture;
            }

            if (vesselWithArrival == null && vesselWithDeparture == null)
            {
                return null;
            }

            //result = vesselWithDeparture ?? vesselWithArrival;

            //if (vesselWithArrival != null)
            //{

            //    ata = vesselWithArrival.TimeStmp;
            //}

            //if (vesselWithDeparture != null)
            //{

            //    atd = vesselWithDeparture.TimeStmp;
            //    result = vesselWithDeparture;
            //}

            return new VesselClearance
            {
                CallSign = result.CallSign,
                UTC = result.TimeStmp,
                LocalRecvTime = result.TimeStmp,
                MMSI = result.MMSI,
                ShipName = result.ShipName,
                Aging = result.TimeStmp,
                Info = result.Info,
                SOG = result.SOG,
                AlarmID = result.AlarmId,
                Status = result.Alarm?.AlarmMsg,
                ATA = ata,
                ATD = atd,
                TimeStamp = result.TimeStmp,
                Flag = GetFlagNameByMmsi(result.MMSI),
                Photo = await GetTargetPhotoFilenameByMmsiAsync(result.MMSI),
                Zone = (await _vtsDatabaseContext.Zones.FirstOrDefaultAsync(x => x.ZoneID == result.ZoneId))?
                        .ZoneName
            };
        }

        public async Task<bool> UpdateVesselClearanceListAsync(List<VesselClearance> vesselClearanceList)
        {
            var events = new List<Event>();
            foreach (var item in vesselClearanceList)
            {
                var evnt = await _vtsDatabaseContext.Events.FirstOrDefaultAsync(x => x.EventID == item.EventID);
                if (evnt != null)
                {
                    evnt.AlarmId = item.AlarmID;
                    events.Add(evnt);
                }
            }
            if (events.Any())
            {
                _vtsDatabaseContext.Events.UpdateRange(events);
                await _vtsDatabaseContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> UpdateDredgingAlarmListAsync(List<AlarmEvent> alarmEvents)
        {
            var events = new List<Event>();
            foreach (var item in alarmEvents)
            {
                var evnt = await _vtsDatabaseContext.Events.FirstOrDefaultAsync(x => x.EventID == item.EventID);
                if (evnt != null)
                {
                    evnt.Lat = item.Latitude;
                    evnt.Lng = item.Longitude;
                    evnt.AlarmId = item.AlarmID;
                    events.Add(evnt);
                }
            }
            if (events.Any())
            {
                _vtsDatabaseContext.Events.UpdateRange(events);
                await _vtsDatabaseContext.SaveChangesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> UpdateStraightRepsListAsync(List<AlarmEvent> alarmEvents)
        {
            var events = new List<Event>();
            foreach (var item in alarmEvents)
            {
                var evnt = await _vtsDatabaseContext.Events.FirstOrDefaultAsync(x => x.EventID == item.EventID);
                if (evnt != null)
                {
                    evnt.Lat = item.Latitude;
                    evnt.Lng = item.Longitude;
                    evnt.AlarmId = item.AlarmID;
                    evnt.ZoneId = item.ZoneID;
                    evnt.Info = item.Info;
                    evnt.TimeStmp = item.Timestamp;
                    events.Add(evnt);
                }
            }
            if (events.Any())
            {
                _vtsDatabaseContext.Events.UpdateRange(events);
                await _vtsDatabaseContext.SaveChangesAsync();
                return true;
            }
            return false;
        }
    }
}
