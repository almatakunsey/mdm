﻿using Abp.UI;
using GeoJSON.Net.Geometry;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Common.Helpers.RMS;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.Model.AkkaModel.Messages.Playback;
using Vtmis.Core.Model.AkkaModel.Messages.Target;
using Vtmis.Core.Model.AkkaModel.Messages.Target.DDMS;
using Vtmis.Core.Model.AkkaModel.Messages.Target.RMS;
using Vtmis.Core.Model.DTO;
using Vtmis.Core.Model.DTO.DDMS;
using Vtmis.Core.Model.DTO.RMS;
using Vtmis.Core.Model.DTO.Target;
using Vtmis.Core.Model.ViewModels.Lloyds;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.WebVessel.Tracking.Services.Concrete
{
    public class AisMapTrackingService : IAisMapTrackingService
    {
        private readonly IDapperAisRepository _dapper;
        private readonly ITargetQuery _targetQuery;
        private readonly VtsDatabaseContext _vtsDatabaseContext;

        public AisMapTrackingService()
        {

        }
        public AisMapTrackingService(IDapperAisRepository dapper, ITargetQuery targetQuery, VtsDatabaseContext vtsDatabaseContext)
        {
            _dapper = dapper;
            _targetQuery = targetQuery;
            _vtsDatabaseContext = vtsDatabaseContext;
        }

        public async Task<GeoJsonResponse> GetPlaybackData(int mmsi, DateTime startDate, DateTime endDate)
        {
            var query = await _targetQuery.QueryPlayback(mmsi, startDate, endDate);

            var filterShipPositions = new List<ShipPosition>();
            if (query != null && query.Any())
            {
                //double prevLat = 0;
                //double prevLong = 0;
                //DateTime? prevDateTime = null;
                //var count = 0;
                //foreach (var item in query)
                //{
                //    if (count <= 0)
                //    {
                //        filterShipPositions.Add(item);
                //        prevLat = item.Latitude;
                //        prevLong = item.Longitude;
                //        prevDateTime = item.LocalRecvTime;
                //        count += 1;
                //        //Console.WriteLine($"Latitude: {item.Latitude} || Longitude: {item.Longitude} || Date : {item.LocalRecvTime}");
                //        continue;
                //    }
                //    if ((item.LocalRecvTime - prevDateTime).Value.TotalSeconds < 120)
                //    {
                //        count += 1;
                //        continue;
                //    }
                //    if (prevLat != item.Latitude && prevLong != item.Longitude &&
                //            prevDateTime != item.LocalRecvTime)
                //    {
                //        filterShipPositions.Add(item);
                //        prevLat = item.Latitude;
                //        prevLong = item.Longitude;
                //        prevDateTime = item.LocalRecvTime;
                //        //Console.WriteLine($"Latitude: {item.Latitude} || Longitude: {item.Longitude} || Date : {item.LocalRecvTime}");
                //    }

                //    count += 1;
                //}

                filterShipPositions = query.ToList();
                var timePositions = filterShipPositions.Select(s => MapTime(s));

                var multiPoint = new MultiPoint(timePositions.Select(c => c.Point));

                var timeArray = timePositions.Select(c => c.UnixTime).ToArray();
                var trueHeadings = timePositions.Select(c => c.TrueHeading).ToArray();
                return new GeoJsonResponse
                {
                    Status = true,
                    type = "Feature",
                    TargetData = query.OrderBy(o => o.RecvTime).Select(s => new TargetData
                    {
                        Destination = s.IP,
                        SOG = s.SOG,
                        NavStatus = s.NavigationalStatusToString
                    }).ToList(),
                    geometry = multiPoint,
                    properties = new Properties
                    {
                        Time = timeArray,
                        Orientations = trueHeadings
                    }
                };
            }
            return new GeoJsonResponse();
        }

        private TimePosition MapTime(ShipPosition s)
        {
            double? trueHeading = s.TrueHeading;
            if (s.TrueHeading == null || s.TrueHeading == 511)
            {
                trueHeading = (double)s.COG;
            }
            return new TimePosition
            {
                Point = new Point(new Position(s.Latitude, s.Longitude)),
                UnixTime = new DateTimeOffset(s.LocalRecvTime.Value).ToUnixTimeMilliseconds(),
                TrueHeading = trueHeading.HasValue ? trueHeading.Value : 0
                //TrueHeading = (double)s.COG
            };
        }

        public async Task<RMSLighthouse> GetLighthouseAsync(int mmsi)
        {
            return await _targetQuery.QueryLighthouse(mmsi).ConfigureAwait(false);
        }     

        public async Task<object> GetAtonList(RequestAton m)
        {
            if (m.Type == TargetClass.Aton) // AtonPosData
            {
                return await _targetQuery.QueryAtonListAsync(new RequestAtonList(m.PageIndex, m.PageSize)).ConfigureAwait(false);
            }
            else if (m.Type == TargetClass.MonitoringData) // AtonMonData
            {
                return await _targetQuery.QueryMonListAsync(new RequestMonList(m.PageIndex, m.PageSize)).ConfigureAwait(false);
            }
            else if (m.Type == TargetClass.Methydro) // AtonMetData
            {
                return await _targetQuery.QueryMetList(new RequestMethydroList(m.PageIndex, m.PageSize)).ConfigureAwait(false);
            }
            return null;
        }

        public async Task<object> GetRMSInfoAsync(int mmsi)
        {
            return await _targetQuery.QueryRMSInfoAsync(mmsi).ConfigureAwait(false);
        }

        public async Task<ResponseDDMSInfo> GetDDMSInfoAsync(int mmsi)
        {
            return await _targetQuery.QueryDDMSInfo(mmsi).ConfigureAwait(false);
        }

        public async Task<IEnumerable<ResponseDDMSHistorical>> GetDDMSHistorical(int mmsi, DateTime startdate, DateTime enddate)
        {
            var qry = await _targetQuery.QueryDDMSHistoricalAsync(mmsi, startdate, enddate);
            if (qry.Any())
            {
                //var dateCol = new HashSet<DateTime>();
                //var ddmsCol = new List<ResponseDDMSHistorical>();
                //if (qry.First() is ResponseDDMSHistorical)
                //{
                //    foreach (ResponseDDMSHistorical item in qry)
                //    {
                //        dateCol.Add(item.LocalRecvTime);
                //    }
                //    DateTime? d = null;
                //    foreach (var item in dateCol.OrderBy(o => o))
                //    {
                //        if (d == null)
                //        {
                //            d = item;
                //        }
                //        if ((item - d).Value.TotalSeconds >= 120)
                //        {
                //            ddmsCol.Add(qry.Where(x => x.LocalRecvTime == item).First());
                //            d = item;
                //        }
                //    }
                //}
                return qry;
            }
            return null;
        }
        public async Task<List<object>> RmsStatisticAsync(int mmsi, DateTime startDate, DateTime endDate)
        {
            var qry = await _targetQuery.QueryRMSHistoricalAsync(mmsi, startDate, endDate);
            if (qry.Any())
            {
                //var dateCol = new HashSet<DateTime>();
                //var resultCol = new List<object>();
                //if (qry.First() is ResponseIALAHistorical)
                //{
                //    foreach (ResponseIALAHistorical item in qry)
                //    {
                //        dateCol.Add(item.LocalDateTime);
                //    }
                //    DateTime? d = null;
                //    foreach (var item in dateCol.OrderBy(o => o))
                //    {
                //        if (d == null)
                //        {
                //            d = item;
                //        }
                //        if ((item - d).Value.TotalSeconds >= 120)
                //        {
                //            List<ResponseIALAHistorical> col = qry.Cast<ResponseIALAHistorical>().ToList();
                //            resultCol.Add(col.Where(x => x.LocalDateTime == item).First());
                //            d = item;
                //        }
                //    }
                //}
                //else if (qry.First() is ResponseLightBeaconHistorical)
                //{
                //    foreach (ResponseLightBeaconHistorical item in qry)
                //    {
                //        dateCol.Add(item.LocalDateTime);
                //    }

                //    DateTime? d = null;
                //    foreach (var item in dateCol.OrderBy(o => o))
                //    {
                //        if (d == null)
                //        {
                //            d = item;
                //        }
                //        if ((item - d).Value.TotalSeconds >= 120)
                //        {
                //            List<ResponseLightBeaconHistorical> col = qry.Cast<ResponseLightBeaconHistorical>().ToList();
                //            resultCol.Add(col.Where(x => x.LocalDateTime == item).First());
                //            d = item;
                //        }
                //    }
                //}
                //else if (qry.First() is ResponseLighthouseHistorical)
                //{
                //    foreach (ResponseLighthouseHistorical item in qry)
                //    {
                //        dateCol.Add(item.LocalDateTime);
                //    }
                //    DateTime? d = null;
                //    foreach (var item in dateCol.OrderBy(o => o))
                //    {
                //        if (d == null)
                //        {
                //            d = item;
                //        }
                //        if ((item - d).Value.TotalSeconds >= 120)
                //        {
                //            List<ResponseLighthouseHistorical> col = qry.Cast<ResponseLighthouseHistorical>().ToList();
                //            resultCol.Add(col.Where(x => x.LocalDateTime == item).First());
                //            d = item;
                //        }
                //    }
                //}
                //else if (qry.First() is ResponseSelfContainedLanternHistorical)
                //{
                //    foreach (ResponseSelfContainedLanternHistorical item in qry)
                //    {
                //        dateCol.Add(item.LocalDateTime);
                //    }
                //    DateTime? d = null;
                //    foreach (var item in dateCol.OrderBy(o => o))
                //    {
                //        if (d == null)
                //        {
                //            d = item;
                //        }
                //        if ((item - d).Value.TotalSeconds >= 120)
                //        {
                //            List<ResponseSelfContainedLanternHistorical> col = qry.Cast<ResponseSelfContainedLanternHistorical>().ToList();
                //            resultCol.Add(col.Where(x => x.LocalDateTime == item).First());
                //            d = item;
                //        }
                //    }
                //}
                //else if (qry.First() is ResponseZeniLiteHistorical)
                //{
                //    foreach (ResponseZeniLiteHistorical item in qry)
                //    {
                //        dateCol.Add(item.LocalDateTime);
                //    }
                //    DateTime? d = null;
                //    foreach (var item in dateCol.OrderBy(o => o))
                //    {
                //        if (d == null)
                //        {
                //            d = item;
                //        }
                //        if ((item - d).Value.TotalSeconds >= 120)
                //        {
                //            List<ResponseZeniLiteHistorical> col = qry.Cast<ResponseZeniLiteHistorical>().ToList();
                //            resultCol.Add(col.Where(x => x.LocalDateTime == item).First());
                //            d = item;
                //        }
                //    }
                //}

                return qry;
            }
            return null;
        }
        public bool IsTargetExistInAisDb(AisDbInfoDto aisDbInfo, int shipPositionId)
        {
            using (var dbContext = new VesselDatabaseContext(aisDbInfo.AisDbName, aisDbInfo.ServerName,
               aisDbInfo.Username, aisDbInfo.Password))
            {
                var result = dbContext.ShipPositions.Where(x => x.ShipPosID == shipPositionId);
                if (!result.Any())
                {
                    throw new UserFriendlyException("Could not found the target!");
                }
                return result.Any();
            }
        }

        public async Task<PaginatedList<ResponseVesselInquiry>> Get(int? mmsi, int? imo, string callSign, string targetName, int pageIndex, int pageSize)
        {
            var vtsConString = AppHelper.GetVtsProdDbConnectionString();
            //Console.WriteLine($"{vtsConString}");
            using (var vtsDbContext = new VtsDatabaseContext(vtsConString))
            {

                var qry = from sp in vtsDbContext.ShipLists.AsNoTracking()
                          select new ResponseVesselInquiry
                          {
                              CallSign = sp.CallSign,
                              Dimension_A = sp.Dimension_A,
                              Dimension_B = sp.Dimension_B,
                              Dimension_C = sp.Dimension_C,
                              Dimension_D = sp.Dimension_D,
                              ETA = sp.ETA,
                              IMO = sp.IMO,
                              LocalReceivedTime = sp.LocalRecvTime,
                              MaxDraught = sp.MaxDraught,
                              MMSI = sp.MMSI,
                              NavigationalStatus = sp.NavigationalStatus,
                              ReceivedTime = sp.RecvTime,
                              ShipType = sp.ShipType,
                              TargetName = sp.Name
                          };

                if (mmsi != null)
                {
                    qry = qry.Where(x => x.MMSI.ToString().StartsWith(mmsi.ToString(), StringComparison.CurrentCultureIgnoreCase));
                }
                if (imo != null)
                {
                    qry = qry.Where(x => x.IMO != null && x.IMO.ToString().StartsWith(imo.ToString(), StringComparison.CurrentCultureIgnoreCase));
                }
                if (callSign != null)
                {
                    qry = qry.Where(x => x.CallSign != null && x.CallSign.StartsWith(callSign.ToString(), StringComparison.CurrentCultureIgnoreCase));
                }
                if (!string.IsNullOrEmpty(targetName))
                {
                    qry = qry.Where(x => x.TargetName != null && x.TargetName.Contains(targetName, StringComparison.CurrentCultureIgnoreCase));
                }

                if (await qry.AnyAsync())
                {
                    return await new PaginatedList<ResponseVesselInquiry>().CreateAsync(qry, pageIndex, pageSize);
                }

            }

            return null;
        }

        public async Task<TargetDetail> GetTargetDetail(int? mmsi, int? imo, string callSign, string targetName)
        {
            var requestDate = DateTime.Now;
            using (var dbContext = new VesselDatabaseContext(requestDate))
            {
                var qry = from sp in dbContext.ShipPositions.AsNoTracking()
                          from ss in dbContext.ShipStatics.AsNoTracking().Where(x => x.MMSI == sp.MMSI).DefaultIfEmpty()
                          select new { sp, ss };

                if (mmsi != null)
                {
                    qry = qry.Where(x => x.sp.MMSI == mmsi);
                }

                if (imo != null)
                {
                    qry = qry.Where(x => x.ss != null && x.ss.IMO == imo);
                }

                if (!string.IsNullOrEmpty(callSign))
                {
                    qry = qry.Where(x => x.ss != null && string.Equals(x.ss.CallSign, callSign, StringComparison.CurrentCultureIgnoreCase));
                }

                if (!string.IsNullOrEmpty(targetName))
                {
                    qry = qry.Where(x => x.ss != null && x.ss.Name.Contains(targetName, StringComparison.CurrentCultureIgnoreCase));
                }

                var target = await qry.OrderByDescending(o => o.sp.LocalRecvTime).FirstOrDefaultAsync();
                if (target == null)
                {
                    return null;
                }

                var atonPosMonResult = new AtonPosMonDto();

                return new TargetDetail(target.sp.ShipPosID, target.sp.MMSI, target.ss?.Name, target.ss?.AISVersion, target.ss?.IMO, target.ss?.AtoNType, target.ss?.ETA,
                     target.ss?.Vendor, target.ss?.Destination, target.ss?.ShipType, target.ss?.CallSign, (float?)target.ss?.MaxDraught, target.ss?.VirtualAtoN, atonPosMonResult?.AnalogueExt1,
                     atonPosMonResult?.AnalogueExt2, atonPosMonResult?.AnalogueInt, target.ss?.Dimension_A, target.ss?.Dimension_B, target.ss?.Dimension_C, target.ss?.Dimension_D,
                     target.ss?.PositionFixingDevice, atonPosMonResult?.OffPosition21, atonPosMonResult?.Health21, atonPosMonResult?.Light21, atonPosMonResult?.RACON21, atonPosMonResult?.Health68,
                     atonPosMonResult?.Light68, atonPosMonResult?.RACON68, atonPosMonResult?.OffPosition68, atonPosMonResult?.DigitalInput, target.sp.LocalRecvTime, target.sp.RecvTime,
                     target.sp.Latitude, target.sp.Longitude, (float?)target.sp.ROT, (float?)target.sp.SOG, (float?)target.sp.COG, target.sp.TrueHeading, target.sp.MessageId, target.sp.RAIM, target.sp.NavigationalStatus,
                     target.sp.IP, target.sp.PositionAccuracy);
            }
        }
        public async Task<TargetInfo> GetTargetDetailsByShipPositionIdAsync(AisDbInfoDto aisDbInfo, int shipPositionId,
            int pageIndex = 1, int pageSize = 10)
        {
            //TODO: Change this with threshold
            for (int i = 0; i < 3; i++)
            {
                var aisDbDate = CommonHelper.ExtractDateFromAisDbName(aisDbInfo.AisDbName);

                if (i > 0)
                {
                    aisDbDate = aisDbDate.AddDays(-i);
                }
                var aisDbName = CommonHelper.GetAisDbNameFromDate(aisDbDate);
                //Console.WriteLine(aisDbName);
                using (var dbContext = new VesselDatabaseContext(aisDbName, aisDbInfo.ServerName,
                     aisDbInfo.Username, aisDbInfo.Password))
                {
                    var targetPositionsIq = dbContext.ShipPositions.Where(x => x.ShipPosID == shipPositionId);

                    if (!targetPositionsIq.Any())
                    {
                        if (i >= 2)
                        {
                            throw new UserFriendlyException("Could not found the target!");
                        }
                        continue;
                    }

                    if (string.Equals(aisDbInfo.EnvironmentName, Core.Common.Constants.HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase))
                    {

                    }
                    else
                    {
                        targetPositionsIq = targetPositionsIq.Where(x => x.LocalRecvTime <= aisDbInfo.ToDate);
                    }

                    var targetPositions = targetPositionsIq.OrderByDescending(o => o.LocalRecvTime)
                        .Select(x => new TargetPosition(x.ShipPosID, x.LocalRecvTime, x.RecvTime,
                                x.Latitude, x.Longitude, (float)x.ROT, (float)x.SOG, (float)x.COG, x.TrueHeading,
                                x.MMSI, x.MessageId, x.RAIM, x.NavigationalStatus, x.UTC, x.IP, x.PositionAccuracy));

                    var targetPositionsPaginated = await new PaginatedList<TargetPosition>()
                                                    .CreateAsync(targetPositions.AsNoTracking(), pageIndex, pageSize);

                    if (targetPositionsPaginated.Items.Count > 0)
                    {
                        var tpResult = targetPositionsPaginated.Items.FirstOrDefault();

                        var targetStatics = await dbContext.ShipStatics.Where(x => x.MMSI == tpResult.MMSI).FirstOrDefaultAsync();
                        var atonPosMonResult = new AtonPosMonDto();
                        if (tpResult.MessageId == 21)
                        {
                            var isAton = dbContext.AtonPosDatas.Where(x => x.MMSI == tpResult.MMSI).Any();
                            if (isAton)
                            {
                                atonPosMonResult = await (from ap in dbContext.AtonPosDatas
                                                          join am in dbContext.AtonMonDatas on ap.MMSI equals am.MMSI into ps
                                                          from am in ps.DefaultIfEmpty()
                                                          where ap.MMSI == tpResult.MMSI
                                                          orderby ap.RecvTime descending
                                                          select new AtonPosMonDto
                                                          {
                                                              MMSI = ap.MMSI,
                                                              OffPosition21 = ap.OffPosition21,
                                                              OffPosition68 = am.OffPosition68,
                                                              Error21 = ap.Error21,
                                                              Error68 = ap.Error68,
                                                              AnalogueExt1 = am.AnalogueExt1,
                                                              AnalogueExt2 = am.AnalogueExt2,
                                                              AnalogueInt = am.AnalogueInt,
                                                              DigitalInput = am.DigitalInput,
                                                              Health21 = ap.Health21,
                                                              Health68 = am.Health,
                                                              Light21 = ap.Light21,
                                                              Light68 = am.Light,
                                                              PositionAccuracy = ap.PositionAccuracy,
                                                              PositionFixingDevice = ap.PositionFixingDevice,
                                                              RACON21 = ap.RACON21,
                                                              RACON68 = am.RACON,
                                                              VirtualAtonStatus = ap.VirtualAtoN
                                                          })
                                                       .FirstOrDefaultAsync();
                            }
                        }

                        var result = new TargetInfo(shipPositionId, targetStatics?.Name,
                            targetStatics?.AISVersion, targetStatics?.IMO,
                            tpResult?.MMSI, targetStatics?.AtoNType, targetStatics?.ETA, targetStatics?.Vendor,
                            targetStatics?.Destination, targetStatics?.ShipType, targetStatics?.CallSign, (float?)targetStatics?.MaxDraught,
                            targetStatics?.Dimension_A, targetStatics?.Dimension_B, targetStatics?.Dimension_C, targetStatics?.Dimension_D,
                            targetStatics?.PositionFixingDevice, atonPosMonResult?.VirtualAtonStatus, atonPosMonResult?.OffPosition21,
                            atonPosMonResult?.Health21, atonPosMonResult?.Light21, atonPosMonResult?.RACON21,
                            atonPosMonResult?.OffPosition68, atonPosMonResult?.Health68, atonPosMonResult?.Light68,
                            atonPosMonResult?.RACON68, atonPosMonResult?.AnalogueExt1, atonPosMonResult?.AnalogueExt2,
                            atonPosMonResult?.AnalogueInt, targetPositionsPaginated);

                        return result;
                    }
                }
            }
            return null;
        }

        public List<VesselRoute> AddTargetPreviousRoute(string targetId, double targetLatitude, double targetLongitude,
            List<VesselRoute> vesselRoutes)
        {
            var maxRouteCollection = 10;
            if (string.IsNullOrEmpty(targetId) == false && targetId != "0")
            {
                if (vesselRoutes.Where(x => x.VesselId == targetId).Any())
                {

                    var vesselPosition = vesselRoutes
                        .Where(x => x.VesselId == targetId)
                        .FirstOrDefault();


                    if (vesselPosition.VesselRoutePosition.Any() && vesselPosition.VesselRoutePosition.Count < maxRouteCollection)
                    {
                        if (vesselPosition.VesselRoutePosition.Last.Value.Lat != targetLatitude
                            || vesselPosition.VesselRoutePosition.Last.Value.Lgt != targetLongitude)
                        {
                            vesselPosition.VesselRoutePosition.AddLast(new VesselRoutePosition(targetLatitude,
                                targetLongitude));
                        }

                    }
                    else if (vesselPosition.VesselRoutePosition.Count >= maxRouteCollection)
                    {
                        if (vesselPosition.VesselRoutePosition.Last.Value.Lat != targetLatitude
                            || vesselPosition.VesselRoutePosition.Last.Value.Lgt != targetLongitude)
                        {
                            vesselPosition.VesselRoutePosition.RemoveFirst();
                            vesselPosition.VesselRoutePosition.AddLast(new VesselRoutePosition(targetLatitude,
                                targetLongitude));
                        }

                    }

                }
                else
                {
                    var newStack = new LinkedList<VesselRoutePosition>();
                    newStack.AddFirst(new VesselRoutePosition(targetLatitude, targetLongitude));
                    vesselRoutes.Add(new VesselRoute { VesselId = targetId, VesselRoutePosition = newStack });
                }

            }
            return vesselRoutes;
        }


    }

    public class ResponseVesselInquiry
    {
        public ResponseLloyds Lloyds { get; set; } = new ResponseLloyds();
        public string TargetName { get; set; }
        //public int? AISVersion { get; set; }
        public int? IMO { get; set; }
        public string CallSign { get; set; }
        public int? MMSI { get; set; }
        //public float? ROT { get; private set; }
        //public string RotToString
        //{
        //    get
        //    {
        //        if (CommonHelper.GetTargetClassByMessageId(MessageId) == TargetClass.ClassA)
        //        {
        //            if (ROT > 720 || ROT < -720)
        //            {
        //                return null;
        //            }
        //            if (ROT < 0)
        //            {
        //                var result = Math.Round((double)ROT, 2);
        //                return $"Portside {result.ToString("0.0")}°/min";
        //            }

        //            if (ROT > 0)
        //            {
        //                var result = Math.Round((double)ROT, 2);
        //                return $"Starboard {result.ToString("0.0")}°/min";
        //            }
        //            if (ROT == 0)
        //            {
        //                return $"{ROT}°/min";
        //            }
        //            return null;
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //}
        //public float? SOG { get; private set; }
        //public float? COG { get; private set; }
        //public int? TrueHeading { get; private set; }
        public int MessageId { get; set; }
        //public int? RAIM { get; set; }
        public string NavigationalStatusToString
        {
            get
            {
                return TargetHelper.GetNavStatusDescriptionByNavStatusId(NavigationalStatus);
            }
        }
        public int? NavigationalStatus { get; set; }
        public DateTime? LocalReceivedTime { get; set; }
        public DateTime? ReceivedTime { get; set; }
        public DateTime? ETA { get; set; }
        //public string Vendor { get; set; }
        //public string Destination { get; set; }
        public int? ShipType { get; set; }
        public string ShipType_ToString
        {
            get
            {
                return TargetHelper.GetShipTypeByShipTypeId(ShipType);
            }
        }
        public double? MaxDraught { get; set; }
        public int? Dimension_A { get; set; }
        public int? Dimension_B { get; set; }
        public int? Dimension_C { get; set; }
        public int? Dimension_D { get; set; }
        public string LengthXbeam
        {
            get
            {
                return TargetHelper.GetLengthXBeam(true, Dimension_A,
                    Dimension_B, Dimension_C, Dimension_D, MessageId);
            }
        }
        //public int? PositionFixingDevice { get; set; }
        //public string PositionFixingDeviceToString
        //{
        //    get
        //    {
        //        return TargetHelper.GetPositionFixingDevice(PositionFixingDevice);
        //    }
        //}
        //public int? PositionAccuracy { get; set; }
        //public string PositionAccuracyToString
        //{
        //    get
        //    {
        //        return TargetHelper.GetPositionAccuracy(PositionAccuracy);
        //    }
        //}
        //public string LastSeen
        //{
        //    get
        //    {
        //        return CommonHelper.TimeAgo(LocalReceivedTime.Value);
        //    }
        //}
    }
}
