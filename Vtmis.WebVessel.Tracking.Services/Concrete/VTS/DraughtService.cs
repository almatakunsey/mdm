﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.Core.Model.ViewModels.VTS;
using Vtmis.WebVessel.Tracking.Repository.Interfaces.VTS;
using Vtmis.WebVessel.Tracking.Services.Interfaces.VTS;

namespace Vtmis.WebVessel.Tracking.Services.Concrete.VTS
{
    public class DraughtService : IDraughtService
    {
        private readonly IDraughtRepository _draughtRepository;
        private readonly IMapper _mapper;

        public DraughtService(IDraughtRepository draughtRepository, IMapper mapper)
        {
            _draughtRepository = draughtRepository;
            _mapper = mapper;
        }

        public async Task<DraughtViewModel> GetAsync(int mmsi)
        {
            return _mapper.Map<DraughtViewModel>(await _draughtRepository.GetAsync(mmsi));
        }

        public Task<IEnumerable<DraughtViewModel>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task<PaginatedList<DraughtViewModel>> GetAllAsync(int pageIndex, int pageSize)
        {
            throw new NotImplementedException();
        }
    }
}
