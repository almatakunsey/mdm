﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Model.DTO.ShipType;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Vtmis.WebVessel.Tracking.Services.Interfaces.VTS;

namespace Vtmis.WebVessel.Tracking.Services.Concrete.VTS
{
    public class ShipTypesService : IShipTypesService
    {
        private readonly IShipTypesRepository _shipTypesRepository;

        public ShipTypesService(IShipTypesRepository shipTypesRepository)
        {
            _shipTypesRepository = shipTypesRepository;
        }

        public IEnumerable<ResponseShipTypeDto> GetAll()
        {

            var result = new List<ResponseShipTypeDto>();
            var counter = 1;
            foreach (var item in ShipTypeConst.All().OrderBy(x => x))
            {
                result.Add(new ResponseShipTypeDto
                {
                    Id = counter,
                    ShipTypeName = item
                });
                counter += 1;
            }
            //var shiptypesDto = new List<ResponseShipTypeDto>();
            //var shiptypes = await _shipTypesRepository.GetAllAsync();

            //foreach (var item in shiptypes)
            //{
            //    var shiptype = new ResponseShipTypeDto
            //    {
            //        ShipTypeID = item.ShipTypeID,
            //        ShipTypeIDLast = item.ShipTypeIDLast,
            //        ShipTypeName = item.ShipTypeName,
            //        ShipTypeIdx = item.ShipTypeIdx
            //    };
            //    shiptypesDto.Add(shiptype);
            //}

            return result;

        }

    }
}
