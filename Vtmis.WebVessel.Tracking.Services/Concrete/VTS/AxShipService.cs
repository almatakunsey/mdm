﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Model.ViewModels.VTS;
using Vtmis.WebVessel.Tracking.Repository.Interfaces.VTS;
using Vtmis.WebVessel.Tracking.Services.Interfaces.VTS;

namespace Vtmis.WebVessel.Tracking.Services.Concrete.VTS
{
    public class AxShipService : IAxShipService
    {
        private readonly IAxShipRepository _repository;
        private readonly IMapper _mapper;

        public AxShipService(IAxShipRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// Get By MMSI
        /// </summary>
        /// <param name="mmsi"></param>
        /// <returns></returns>
        public async Task<AxShipViewModel> GetAsync(int mmsi)
        {
            return _mapper.Map<AxShipViewModel>(await _repository.GetAsync(mmsi));
        }

        /// <summary>
        /// Check if Middle value exist for specific vessel
        /// </summary>
        /// <param name="mmsi"></param>
        /// <returns></returns>
        public async Task<bool> IsMiddleValueExistAsync(int mmsi)
        {
            var result = await _repository.Query().Where(x => x.MMSI == mmsi && x.Middle > 0).AnyAsync();
            return result;
        }


        public async Task<float?> GetMiddleValue(int mmsi)
        {
            var result = (await _repository.Query().FirstOrDefaultAsync(x => x.MMSI == mmsi && x.Middle > 0))
                ?.Middle;
            return result;
        }
    }
}
