﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto;

namespace Vtmis.WebVessel.Tracking.Services.Interfaces
{
    public interface IVtsVesselService
    {
        Task<bool> IsTargetExist(int? mmsi = null, int? imo = null, string callSign = null);
        Task<ResponseExtendedShipList> CreateVesselAsync(CreateExtendedShipList input);
        Task<ResponseExtendedShipList> UpdateVesselAsync(UpdateExtendedShipList input);
        Task SoftDeleteVesselAsync(int vesselId, int mmsi, string imo, string callSign);
        Task BlackListVesselAsync(int vesselId, int mmsi, string imo, string callSign);
        PaginatedList<ResponseExtendedShipList> GetAllShips(int pageIndex = 1, int pageSize = 10, bool includeDeletedShips = false);
        Task<ResponseExtendedShipList> GetShip(int vesselId, int mmsi, string imo, string callSign);
        Task<Ship> GetShipByShipIdAsync(int shipId);
        Task<string> GetTargetPhotoFilenameByMmsiAsync(int? mmsi);
        string GetFlagNameByMmsi(int? mmsi);

        /// <summary>3
        /// Clearance
        /// </summary>
        /// <param name="targetId"></param>
        /// <returns></returns>
        Task<VesselClearance> GetVesselClearanceByTargetIdAsync(int targetId);
        Task<VesselClearance> GetVesselClearanceByTargetMmsiImoCallSignAsync(int? mmsi = null, int? imo = null, string callSign = null);
        Task<bool> UpdateVesselClearanceListAsync(List<VesselClearance> vesselClearanceList);
        Task<bool> UpdateDredgingAlarmListAsync(List<AlarmEvent> alarmEvents);
        Task<bool> UpdateStraightRepsListAsync(List<AlarmEvent> alarmEvents);
    }
}
