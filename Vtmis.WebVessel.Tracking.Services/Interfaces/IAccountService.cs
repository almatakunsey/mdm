﻿using System.Security.Claims;
using System.Threading.Tasks;
using Vtmis.Core.Model.ViewModels.Account;

namespace Vtmis.WebVessel.Tracking.Services.Interfaces
{
    public interface IAccountService
    {
        Task<AbpTokenRequestResponse> RequestToken(string issuerUri, string username, string tenantName, string password);
        bool ValidateToken(string accessToken);
        ClaimsPrincipal GetLoginClaimsPrincipal(string username,int userId);
    }
}
