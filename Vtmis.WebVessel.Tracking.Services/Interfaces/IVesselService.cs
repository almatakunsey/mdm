﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Model.ViewModels.Target;
using Vtmis.Core.Model.ViewModels.Target.Vessel;

namespace Vtmis.WebVessel.Tracking.Services.Interfaces
{
    public interface IVesselService
    {
        string GetTargetImage(int targetId, ImageSize imageSize, ImageSize emptyImageSize);
        Task<IEnumerable<VesselPositionViewModel>> AdvancedSearch(AdvancedSearchViewModel advancedSearch, bool isHost, IEnumerable<int> enumerable);
    }
}
