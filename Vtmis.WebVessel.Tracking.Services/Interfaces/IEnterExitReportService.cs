﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Model.DTO;
using Vtmis.Core.Model.DTO.Report;

namespace Vtmis.WebVessel.Tracking.Services.Interfaces
{
    public interface IEnterExitReportService
    {
        Task<IEnumerable<ResponseReportEnterExit>> GetResponseReportEnterExit(RequestReportEnterExit input);

        Task<IEnumerable<ResponseArrivalDeparture>> GetReportArrivalDeparture(RequestReportEnterExit input);
    }
}
