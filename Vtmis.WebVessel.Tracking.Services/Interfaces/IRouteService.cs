﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Model.AkkaModel.Messages.Route;
using Vtmis.Core.Model.ViewModels.Route.Dto;

namespace Vtmis.WebVessel.Tracking.Services.Interfaces
{
    public interface IRouteService
    {
        Task<ResponseVesselEta> VesselEtaAsync(RequestVesselEta input);
        Task<IEnumerable<ResponseRouteEtaDto>> RouteEtaAsync(RequestRouteEta input);
    }
}
