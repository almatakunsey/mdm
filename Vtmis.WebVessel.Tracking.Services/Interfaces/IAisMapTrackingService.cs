﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Common.Helpers.RMS;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.Model.AkkaModel.Messages.Playback;
using Vtmis.Core.Model.AkkaModel.Messages.Target;
using Vtmis.Core.Model.AkkaModel.Messages.Target.DDMS;
using Vtmis.Core.Model.AkkaModel.Messages.Target.RMS;
using Vtmis.Core.Model.DTO.DDMS;
using Vtmis.Core.Model.DTO.Target;
using Vtmis.WebVessel.Tracking.Services.Concrete;

namespace Vtmis.WebVessel.Tracking.Services.Interfaces
{
    public interface IAisMapTrackingService
    {
        /// <summary>
        /// getDDMSHistorical
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="startdate"></param>
        /// <param name="enddate"></param>
        /// <returns></returns>
        Task<IEnumerable<ResponseDDMSHistorical>> GetDDMSHistorical(int mmsi, DateTime startdate, DateTime enddate);
        Task<List<object>> RmsStatisticAsync(int mmsi, DateTime startDate, DateTime endDate);
        bool IsTargetExistInAisDb(AisDbInfoDto aisDbInfo, int shipPositionId);
        Task<PaginatedList<ResponseVesselInquiry>> Get(int? mmsi, int? imo, string callSign, string targetName, int pageIndex, int pageSize);
        Task<TargetInfo> GetTargetDetailsByShipPositionIdAsync(AisDbInfoDto aisDbInfo, int shipPositionId, 
            int pageIndex = 1, int pageSize = 10);
        Task<TargetDetail> GetTargetDetail(int? mmsi, int? imo, string callSign, string targetName);
        List<VesselRoute> AddTargetPreviousRoute(string targetId, double targetLatitude, double targetLongitude,
            List<VesselRoute> vesselRoutes);
        Task<ResponseDDMSInfo> GetDDMSInfoAsync(int mmsi);
        Task<object> GetRMSInfoAsync(int mmsi);
        Task<object> GetAtonList(RequestAton m);
        Task<RMSLighthouse> GetLighthouseAsync(int mmsi);
        /// <summary>
        /// Get playback data
        /// </summary>
        /// <param name="mmsi"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        Task<GeoJsonResponse> GetPlaybackData(int mmsi, DateTime startDate, DateTime endDate);
    }
}
