﻿using System.Threading.Tasks;
using Vtmis.Core.Model.ViewModels.VTS;

namespace Vtmis.WebVessel.Tracking.Services.Interfaces.VTS
{
    public interface IAxShipService
    {
        Task<AxShipViewModel> GetAsync(int mmsi);
        Task<bool> IsMiddleValueExistAsync(int mmsi);
        Task<float?> GetMiddleValue(int mmsi);
    }
}
