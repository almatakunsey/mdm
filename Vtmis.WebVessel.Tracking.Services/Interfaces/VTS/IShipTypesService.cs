﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Model.DTO.ShipType;

namespace Vtmis.WebVessel.Tracking.Services.Interfaces.VTS
{
    public interface IShipTypesService
    {
        IEnumerable<ResponseShipTypeDto> GetAll();
    }
}
