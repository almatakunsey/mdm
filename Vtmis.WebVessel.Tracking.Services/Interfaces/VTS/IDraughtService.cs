﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.Core.Model.ViewModels.VTS;

namespace Vtmis.WebVessel.Tracking.Services.Interfaces.VTS
{
    public interface IDraughtService
    {
        Task<IEnumerable<DraughtViewModel>> GetAllAsync();
        Task<PaginatedList<DraughtViewModel>> GetAllAsync(int pageIndex, int pageSize);
        Task<DraughtViewModel> GetAsync(int mmsi);
    }
}
