﻿using Vtmis.Core.Common.Enums;

namespace Vtmis.WebVessel.Tracking.Services.Interfaces
{
    public interface IImageService
    {
        string GetTargetImage(int targetId, ImageSize imageSize, ImageSize emptyImageSize);
    }
}
