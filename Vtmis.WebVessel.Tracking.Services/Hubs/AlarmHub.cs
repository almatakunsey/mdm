﻿using Abp.Authorization;
using Abp.Dependency;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.SignalR;

namespace Vtmis.WebVessel.Tracking.Services.Hubs
{
    [AbpAuthorize]
    public class AlarmHub : Hub, ITransientDependency
    {
        public IAbpSession AbpSession { get; set; }

        public AlarmHub()
        {
            AbpSession = NullAbpSession.Instance;
        }
        
    }
}
