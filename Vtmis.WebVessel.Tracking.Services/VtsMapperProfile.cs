﻿using AutoMapper;
using Vtmis.Core.Model.ViewModels.VTS;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto;

namespace Vtmis.WebVessel.Tracking.Services
{
    public class VtsMapperProfile : Profile
    {
        public VtsMapperProfile()
        {
            CreateMap<CreateExtendedShipList, ExtendedShipList>().ReverseMap();
            CreateMap<Draught, DraughtViewModel>().ReverseMap();
            CreateMap<AxShip, AxShipViewModel>().ReverseMap();
        }
    }
}
