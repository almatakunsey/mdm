﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using Vtmis.Core.Common.Constants;

namespace Vtmis.WebAdmin.Configuration
{
    public static class HostingEnvironmentExtensions
    {
        public static IConfigurationRoot GetAppConfiguration(this IHostingEnvironment env)
        {
            //Console.WriteLine("Status " + env.ContentRootPath + " " + env.EnvironmentName + " " + env.IsDevelopment());
            return AppConfigurations.Get(env.ContentRootPath, env.EnvironmentName, env.IsEnvironment(HostingEnvironment.LOCAL));
        }
    }
}
