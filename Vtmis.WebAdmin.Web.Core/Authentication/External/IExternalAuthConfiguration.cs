﻿using System.Collections.Generic;

namespace Vtmis.WebAdmin.Authentication.External
{
    public interface IExternalAuthConfiguration
    {
        List<ExternalLoginProviderInfo> Providers { get; }
    }
}
