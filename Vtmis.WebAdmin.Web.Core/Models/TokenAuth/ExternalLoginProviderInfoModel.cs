﻿using Abp.AutoMapper;
using Vtmis.WebAdmin.Authentication.External;

namespace Vtmis.WebAdmin.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
