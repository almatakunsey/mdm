﻿using Abp.AspNetCore;
using Abp.AspNetCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.Authentication.JwtBearer;
using Vtmis.WebAdmin.Configuration;
using Vtmis.WebAdmin.EntityFrameworkCore;

namespace Vtmis.WebAdmin
{
    [DependsOn(
        typeof(WebAdminApplicationModule),
        typeof(WebAdminEntityFrameworkModule),
        typeof(AbpAspNetCoreModule)
#if FEATURE_SIGNALR
        ,typeof(AbpWebSignalRModule)
#endif
     )]
    public class WebAdminDefaultModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public WebAdminDefaultModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            //Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
            //    WebAdminConsts.ConnectionStringName
            //);
            Configuration.DefaultNameOrConnectionString = AppHelper.GetMdmDbConnectionString();
            // Use database for language management
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();


            ConfigureTokenAuth();
        }

        private void ConfigureTokenAuth()
        {
            IocManager.Register<TokenAuthConfiguration>();
            var tokenAuthConfig = IocManager.Resolve<TokenAuthConfiguration>();
            tokenAuthConfig.Issuer = AppHelper.GetJwtIssuer();
            tokenAuthConfig.Audience = AppHelper.GetJwtAudience();
            tokenAuthConfig.SecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AppHelper.GetJwtSecurityKey()));

            tokenAuthConfig.SigningCredentials = new SigningCredentials(tokenAuthConfig.SecurityKey, SecurityAlgorithms.HmacSha256);
            tokenAuthConfig.Expiration = TimeSpan.FromDays(1);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WebAdminDefaultModule).GetAssembly());
        }
    }
}
