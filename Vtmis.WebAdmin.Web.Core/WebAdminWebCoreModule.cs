﻿using System;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Abp.AspNetCore;
using Abp.AspNetCore.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Zero.Configuration;
using Vtmis.WebAdmin.Authentication.JwtBearer;
using Vtmis.WebAdmin.Configuration;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Vtmis.Core.Common.Helpers;

#if FEATURE_SIGNALR
using Abp.Web.SignalR;
#endif

namespace Vtmis.WebAdmin
{
    [DependsOn(
         typeof(WebAdminApplicationModule),
         typeof(WebAdminEntityFrameworkModule),
         typeof(AbpAspNetCoreModule)
#if FEATURE_SIGNALR 
        ,typeof(AbpWebSignalRModule)
#endif
     )]
    public class WebAdminWebCoreModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public WebAdminWebCoreModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            //Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
            //    WebAdminConsts.ConnectionStringName
            //);
            Configuration.DefaultNameOrConnectionString = AppHelper.GetMdmDbConnectionString();
            // Use database for language management
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();

            Configuration.Modules.AbpAspNetCore()
                 .CreateControllersForAppServices(
                     typeof(WebAdminApplicationModule).GetAssembly()
                 );

            ConfigureTokenAuth();
        }

        private void ConfigureTokenAuth()
        {
            IocManager.Register<TokenAuthConfiguration>();
            var tokenAuthConfig = IocManager.Resolve<TokenAuthConfiguration>();
            tokenAuthConfig.Issuer = AppHelper.GetJwtIssuer();
            tokenAuthConfig.Audience = AppHelper.GetJwtAudience();
            tokenAuthConfig.SecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AppHelper.GetJwtSecurityKey()));
           
            tokenAuthConfig.SigningCredentials = new SigningCredentials(tokenAuthConfig.SecurityKey, SecurityAlgorithms.HmacSha256);
            tokenAuthConfig.Expiration = TimeSpan.FromDays(1);
            // tokenAuthConfig.Expiration = TimeSpan.FromSeconds(20);            
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WebAdminWebCoreModule).GetAssembly());
        }
    }
}
