﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using Abp.Runtime.Security;
using Abp.UI;
using Vtmis.WebAdmin.Authentication.External;
using Vtmis.WebAdmin.Authentication.JwtBearer;
using Vtmis.WebAdmin.Authorization;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Models.TokenAuth;
using Vtmis.WebAdmin.MultiTenancy;
using Newtonsoft.Json;
using Abp.Application.Services;
using Microsoft.IdentityModel.Tokens;
using Vtmis.WebAdmin.Identity;
using Vtmis.WebAdmin.Users;
using Vtmis.WebAdmin.Roles;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Personalizations;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Request.External;

namespace Vtmis.WebAdmin.Controllers
{
    [Route("api/[controller]/[action]")]
    public class TokenAuthController : WebAdminControllerBase
    {
        private readonly LogInManager _logInManager;
        private readonly MdmLogInManger _mdmLoginManager;
        private readonly ITenantCache _tenantCache;
        private readonly AbpLoginResultTypeHelper _abpLoginResultTypeHelper;
        private readonly TokenAuthConfiguration _configuration;
        private readonly IExternalAuthConfiguration _externalAuthConfiguration;
        private readonly IExternalAuthManager _externalAuthManager;
        private readonly UserRegistrationManager _userRegistrationManager;
        private readonly SignInManager _signInManager;
        private readonly TenantManager _tenantManager;
        private readonly IRoleAppService _roleAppService;
        private readonly IUserAppService _userAppService;
        private readonly RoleManager _roleManager;
        private readonly IPersonalizationService _personalizationService;

        public TokenAuthController(
            LogInManager logInManager,
            MdmLogInManger mdmLoginManager,
            ITenantCache tenantCache,
            AbpLoginResultTypeHelper abpLoginResultTypeHelper,
            TokenAuthConfiguration configuration,
            IExternalAuthConfiguration externalAuthConfiguration,
            IExternalAuthManager externalAuthManager,
            UserRegistrationManager userRegistrationManager,
            SignInManager signInManager,
            TenantManager tenantManager,
            IRoleAppService roleAppService,
            IUserAppService userAppService,
            RoleManager roleManager,
            IPersonalizationService personalizationService)
        {
            _logInManager = logInManager;
            _mdmLoginManager = mdmLoginManager;
            _tenantCache = tenantCache;
            _abpLoginResultTypeHelper = abpLoginResultTypeHelper;
            _configuration = configuration;
            _externalAuthConfiguration = externalAuthConfiguration;
            _externalAuthManager = externalAuthManager;
            _userRegistrationManager = userRegistrationManager;
            _signInManager = signInManager;
            _tenantManager = tenantManager;
            _roleAppService = roleAppService;
            _userAppService = userAppService;
            _roleManager = roleManager;
            _personalizationService = personalizationService;
        }



        [HttpGet]
        public bool ValidateToken(string token)
        {
            var result = new JwtSecurityTokenHandler().ReadJwtToken(token);
            return DateTime.Now > result.ValidTo ? false : true;
        }
        private async Task<AbpLoginResult<Tenant, User>> GetLoginResultAsync(string usernameOrEmailAddress, string password, string tenancyName)
        {
            //var tenant = await _tenantManager.FindByTenancyNameAsync(tenancyName);
            //int? tenantId = null;
            //if (tenant != null)
            //{
            //    tenantId = tenant.Id;
            //}

            //var user = await _userAppService.GetUserByUserNameAndTenantId(usernameOrEmailAddress,
            //    tenantId);
            try
            {
                var loginResult = await _mdmLoginManager.LoginAsync(usernameOrEmailAddress, password, tenancyName);

                switch (loginResult.Result)
                {
                    case AbpLoginResultType.Success:
                        return loginResult;
                    default:
                        throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(loginResult.Result, usernameOrEmailAddress, tenancyName);
                }
            }
            catch (Exception err)
            {
                var a = err;
                throw;
            }
        }

        [HttpPost]
        public async Task<AuthenticateResultModel> Authenticate([FromBody] AuthenticateModel model)
        {
            if (model.TenantName.IsEqual("admin"))
            {
                model.TenantName = null;
            }

            var loginResult = await GetLoginResultAsync(
                model.UserNameOrEmailAddress,
                model.Password,
                model.TenantName
            );

            var accessToken = CreateAccessToken(CreateJwtClaims(loginResult.Identity, loginResult.User.Id));
            int? tenantExist = null;

            if (loginResult.Tenant != null) { tenantExist = loginResult.Tenant.Id; }
            var permissions = new List<string>();

            ////var isTenantAdmin = false;
            var userLevel = UserLevelConst.Annonymous;
            if (loginResult.Tenant == null)
            {
                userLevel = UserLevelConst.HostAdmin;
            }
            foreach (var item in loginResult.User.Roles)
            {
                var role = await _roleAppService.Get(item.RoleId);

                if (userLevel.IsEqual(UserLevelConst.HostAdmin) == false)
                {
                    if (userLevel.IsEqual(UserLevelConst.Annonymous) == true || userLevel.IsEqual(UserLevelConst.TenantUser) == true)
                    {
                        if (role.Name.Contains("admin", StringComparison.CurrentCultureIgnoreCase))
                        {
                            userLevel = UserLevelConst.TenantAdmin;
                        }
                        else
                        {
                            userLevel = UserLevelConst.TenantUser;
                        }
                    }
                }

                foreach (var permission in role.Permissions)
                {
                    if (!permissions.Where(x => x == permission).Any())
                    {
                        permissions.Add(permission);
                    }
                }
            }
            //var personalizations = await _personalizationService.GetAsync(loginResult.User.Id, loginResult.Tenant?.Id);
            var filteredPermissions = new List<string>();
            foreach (var permission in permissions)
            {
                var permissionChunk = permission.Split(".").ToList();
                string combinedName = permission;
                if (permissionChunk.Count > 2)
                {
                    combinedName = $"{permissionChunk[0]}.{permissionChunk[1]}";
                }
                if (!filteredPermissions.Any(x => x == combinedName))
                {
                    filteredPermissions.Add(combinedName);
                }
            }
            var encToken = await new ENCRequest().GetToken();
            return new AuthenticateResultModel
            {
                AccessToken = accessToken,
                EncryptedAccessToken = GetEncrpyedAccessToken(accessToken),
                ExpireInSeconds = (int)_configuration.Expiration.TotalSeconds,
                UserId = loginResult.User.Id,
                TenantId = loginResult.Tenant?.Id,
                Username = loginResult.User?.UserName,
                TenantName = loginResult.Tenant?.Name,
                TenancyName = loginResult.Tenant?.TenancyName,
                UserLevel = userLevel,
                Permissions = filteredPermissions,
                ENCToken = encToken
            };

        }

        [HttpGet]
        public List<ExternalLoginProviderInfoModel> GetExternalAuthenticationProviders()
        {
            return ObjectMapper.Map<List<ExternalLoginProviderInfoModel>>(_externalAuthConfiguration.Providers);
        }

        [HttpPost]
        public async Task<ExternalAuthenticateResultModel> ExternalAuthenticate([FromBody] ExternalAuthenticateModel model)
        {
            var externalUser = await GetExternalUserInfo(model);

            var loginResult = await _logInManager.LoginAsync(new UserLoginInfo(model.AuthProvider, model.ProviderKey, model.AuthProvider), GetTenancyNameOrNull());

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    {
                        var accessToken = CreateAccessToken(CreateJwtClaims(loginResult.Identity, loginResult.User.Id));
                        return new ExternalAuthenticateResultModel
                        {
                            AccessToken = accessToken,
                            EncryptedAccessToken = GetEncrpyedAccessToken(accessToken),
                            ExpireInSeconds = (int)_configuration.Expiration.TotalSeconds
                        };
                    }
                case AbpLoginResultType.UnknownExternalLogin:
                    {
                        var newUser = await RegisterExternalUserAsync(externalUser);
                        if (!newUser.IsActive)
                        {
                            return new ExternalAuthenticateResultModel
                            {
                                WaitingForActivation = true
                            };
                        }

                        // Try to login again with newly registered user!
                        loginResult = await _logInManager.LoginAsync(new UserLoginInfo(model.AuthProvider, model.ProviderKey, model.AuthProvider), GetTenancyNameOrNull());
                        if (loginResult.Result != AbpLoginResultType.Success)
                        {
                            throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(
                                loginResult.Result,
                                model.ProviderKey,
                                GetTenancyNameOrNull()
                            );
                        }

                        return new ExternalAuthenticateResultModel
                        {
                            AccessToken = CreateAccessToken(CreateJwtClaims(loginResult.Identity, loginResult.User.Id)),
                            ExpireInSeconds = (int)_configuration.Expiration.TotalSeconds
                        };
                    }
                default:
                    {
                        throw _abpLoginResultTypeHelper.CreateExceptionForFailedLoginAttempt(
                            loginResult.Result,
                            model.ProviderKey,
                            GetTenancyNameOrNull()
                        );
                    }
            }
        }

        private async Task<User> RegisterExternalUserAsync(ExternalAuthUserInfo externalUser)
        {
            var user = await _userRegistrationManager.RegisterAsync(
                externalUser.Name,
                externalUser.Surname,
                externalUser.EmailAddress,
                externalUser.EmailAddress,
                Authorization.Users.User.CreateRandomPassword(),
                true
            );

            user.Logins = new List<UserLogin>
            {
                new UserLogin
                {
                    LoginProvider = externalUser.Provider,
                    ProviderKey = externalUser.ProviderKey,
                    TenantId = user.TenantId
                }
            };

            await CurrentUnitOfWork.SaveChangesAsync();

            return user;
        }

        private async Task<ExternalAuthUserInfo> GetExternalUserInfo(ExternalAuthenticateModel model)
        {
            var userInfo = await _externalAuthManager.GetUserInfo(model.AuthProvider, model.ProviderAccessCode);
            if (userInfo.ProviderKey != model.ProviderKey)
            {
                throw new UserFriendlyException(L("CouldNotValidateExternalUser"));
            }

            return userInfo;
        }

        private string GetTenancyNameOrNull()
        {
            if (!AbpSession.TenantId.HasValue)
            {
                return null;
            }

            return _tenantCache.GetOrNull(AbpSession.TenantId.Value)?.TenancyName;
        }





        private string CreateAccessToken(IEnumerable<Claim> claims, TimeSpan? expiration = null)
        {
            var now = DateTime.UtcNow;

            var jwtSecurityToken = new JwtSecurityToken(
                issuer: _configuration.Issuer,
                audience: _configuration.Audience,
                claims: claims,
                notBefore: now,
                expires: now.Add(expiration ?? _configuration.Expiration),
                signingCredentials: _configuration.SigningCredentials
            );

            return new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken);
        }

        private static List<Claim> CreateJwtClaims(ClaimsIdentity identity, long userId)
        {
            var claims = identity.Claims.ToList();
            var nameIdClaim = claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);
            if (nameIdClaim == null)
            {
                nameIdClaim = new Claim(ClaimTypes.NameIdentifier, userId.ToString());
                claims.Add(nameIdClaim);
            }
            var nameClaim = claims.FirstOrDefault(c => c.Type == ClaimTypes.Name);
            if (nameClaim == null)
            {
                nameClaim = new Claim(ClaimTypes.Name, userId.ToString());
                claims.Add(nameClaim);
            }
            //else
            //{
            //    claims.Remove(nameClaim);
            //    nameClaim = new Claim(ClaimTypes.Name, userId.ToString());
            //    claims.Add(nameClaim);
            //}
            // Specifically add the jti (random nonce), iat (issued timestamp), and sub (subject/user) claims.
            claims.AddRange(new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, nameIdClaim.Value),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTimeOffset.Now.ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64)
            });

            return claims;
        }

        private string GetEncrpyedAccessToken(string accessToken)
        {
            return SimpleStringCipher.Instance.Encrypt(accessToken, AppConsts.DefaultPassPhrase);
        }

        [HttpPost]
        public async Task<AuthenticateLoginResultModel> Login([FromBody]string body)
        {
            var result = JsonConvert.DeserializeObject<AuthenticationUserModel>(body);

            //Later change to Log debug
            Console.WriteLine("Username " + result.UserName);

            var loginResult = await _logInManager.LoginAsync(result.UserName, result.Password, "1");

            switch (loginResult.Result)
            {
                case AbpLoginResultType.Success:
                    return new AuthenticateLoginResultModel { UserName = "", DisplayName = "", Claims = loginResult.Identity.Claims.ToList() };
                default:
                    throw new Exception("Authentication execption");
            }
        }
    }

    public class AuthenticationUserModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class AuthenticateLoginResultModel
    {
        public AuthenticateLoginResultModel()
        {
            if (Claims == null)
                Claims = new List<Claim>();
        }
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public List<Claim> Claims { get; set; }
    }
}
