﻿namespace Vtmis.Core.Model.DTO.Image
{
    public class RequestCreateImage
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string FullPath { get; set; }
    }
}
