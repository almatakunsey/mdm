﻿using System.Collections.Generic;

namespace Vtmis.Core.Model.DTO.Image
{
    public class ResponseImageDto
    {       
        public string Name { get; set; }
        public string Path { get; set; }
        public string Original { get; set; }
        public string Thumbnail { get; set; }      
    }
}
