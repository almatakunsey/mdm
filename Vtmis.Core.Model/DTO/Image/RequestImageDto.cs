﻿using System.ComponentModel.DataAnnotations;
using Vtmis.Core.Common.Enums;

namespace Vtmis.Core.Model.DTO.Image
{
    public class RequestImageDto
    {
        [Required]
        public string ImageId { get; set; }
        [Required]
        public ImageType ImageType { get; set; }
    }
}
