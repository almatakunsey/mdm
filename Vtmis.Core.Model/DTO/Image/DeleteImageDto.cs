﻿using System.Collections.Generic;

namespace Vtmis.Core.Model.DTO.Image
{
    public class DeleteImageDto
    {
        public string Name { get; set; }
        public string Path { get; set; }
    }
}
