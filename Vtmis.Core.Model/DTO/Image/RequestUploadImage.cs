﻿using Microsoft.AspNetCore.Http;
using Vtmis.Core.Common.Enums;

namespace Vtmis.Core.Model.DTO.Image
{
    public class RequestUploadImage
    {
        public string ImageId { get; set; }
        public ImageType ImageType { get; set; }
        public IFormCollection Images { get; set; }
    }
}
