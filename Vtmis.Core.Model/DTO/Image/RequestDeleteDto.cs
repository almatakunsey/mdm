﻿using System.Collections.Generic;
using Vtmis.Core.Common.Enums;

namespace Vtmis.Core.Model.DTO.Image
{
    public class RequestDeleteDto
    {
        public string ImageId { get; set; }
        public ImageType ImageType { get; set; }
        public List<string> Name { get; set; }
    }
}
