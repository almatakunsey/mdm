﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtmis.Core.Model.DTO.Image
{
    public class UploadImageDto
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Path { get; set; }
        public string ImageData { get; set; }
    }
}
