﻿namespace Vtmis.Core.Model.DTO.Image
{
    public class CallbackUploadDto
    {
        public Vtmis.Core.Model.DTO.Image.RawDto Raw { get; set; }
        public ImageWatermarkedDto Watermarked { get; set; }
    }
}
