﻿using System;

namespace Vtmis.Core.Model.DTO
{
    public class AtonPosMonDto
    {
        public int MMSI { get; set; }
        public Int16? OffPosition21 { get; set; }
        public Int16? OffPosition68 { get; set; }
        public Int16? VirtualAtonStatus { get; set; }
        public int Error21 { get; set; }
        public int Error68 { get; set; }
        public Single? AnalogueInt { get; set; }
        public Single? AnalogueExt1 { get; set; }
        public Single? AnalogueExt2 { get; set; }
        public Int16? DigitalInput { get; set; }      
        public Int16? Health68 { get; set; }
        public Int16? Light68 { get; set; }
        public Int16? RACON68 { get; set; }
        public Int16? PositionAccuracy { get; set; }      
        public Int16? Health21 { get; set; }
        public Int16? Light21 { get; set; }
        public Int16? RACON21 { get; set; }
        public Int16? PositionFixingDevice { get; set; }
    }
}
