﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Model.DTO
{
    public class ResponseReportEnterExit
    {
        public DateTime EnterTime { get; set; }
        public DateTime ExitTime { get; set; }
        public Int16? TypeId { get; set; }
        public string Type
        {
            get
            {
                return CommonHelper.GetTargetTypeName(TypeId);
            }
        }
        public string Destination { get; set; }
        public string CallSign { get; set; }
        public Int32? IMO { get; set; }
        public Int32? MMSI { get; set; }
        public Int16? Length { get; set; }
        public string Name { get; set; }
        public DateTime ETA { get; set; }
    }
}
