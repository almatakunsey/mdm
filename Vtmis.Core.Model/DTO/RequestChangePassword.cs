﻿using System.ComponentModel.DataAnnotations;

namespace Vtmis.Core.Model.DTO
{
    public class RequestChangePassword
    {
        [Required]
        public string Username { get; set; }
        public string TenantName { get; set; }
        [Required]
        public string OldPassword { get; set; }
        [Required]
        [Compare("ConfirmNewPassword")]
        public string NewPassowrd { get; set; }
        [Required]
        public string ConfirmNewPassword { get; set; }
    }
}
