﻿using System;

namespace Vtmis.Core.Model.DTO.Reporting
{
    public class ReportRequest
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ReportId { get; set; }
    }
}
