﻿using System.Collections.Generic;

namespace Vtmis.Core.Model.DTO.Reporting
{
    public class ArrivalDepartureReportDto
    {
        public IList<ArrivalDepartureReportItemDto> Arrival { get; set; }
        public IList<ArrivalDepartureReportItemDto> Departure { get; set; }
    }
}
