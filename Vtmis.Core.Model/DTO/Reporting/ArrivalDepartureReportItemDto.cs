﻿using System;

namespace Vtmis.Core.Model.DTO.Reporting
{
    public class ArrivalDepartureReportItemDto
    {
        public DateTime? ArrivalTime { get; set; }
        public DateTime? DepartureTime { get; set; }
        public string Name { get; set; }
        public int? MMSI { get; set; }
        public int? IMO { get; set; }
        public string CallSign { get; set; }
        public string Type { get; set; }
        public decimal Length { get; set; }
        public string Destination { get; set; }
        public DateTime? ETA { get; set; }
    }
}
