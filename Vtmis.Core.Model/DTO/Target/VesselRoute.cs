﻿using System.Collections.Generic;
using Vtmis.Core.Model.AkkaModel.Messages;

namespace Vtmis.Core.Model.DTO.Target
{
    public class VesselRoute
    {
        public string VesselId { get; set; }
        public LinkedList<VesselRoutePosition> VesselRoutePosition { get; set; }
    }
}
