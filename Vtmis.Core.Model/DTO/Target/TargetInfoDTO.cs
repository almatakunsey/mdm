﻿using Vtmis.Core.Model.ViewModels.Target.Vessel;

namespace Vtmis.Core.Model.DTO.Target
{
    public class TargetInfoDTO
    {
        public ShipPositionViewModel ShipPosition { get; set; }
        public ShipStaticViewModel ShipStatic { get; set; }
    }
}
