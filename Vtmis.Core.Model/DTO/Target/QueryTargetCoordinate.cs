﻿using System;

namespace Vtmis.Core.Model.DTO.Target
{
    public class QueryTargetCoordinate
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int MMSI { get; set; }
        public DateTime RecvTime { get; set; }
        public double COG { get; set; }
    }
}
