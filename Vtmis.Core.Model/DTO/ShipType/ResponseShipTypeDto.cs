﻿namespace Vtmis.Core.Model.DTO.ShipType
{
    public class ResponseShipTypeDto
    {
        public int Id { get; set; }      
        public string ShipTypeName { get; set; }
    }
}
