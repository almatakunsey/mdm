﻿namespace Vtmis.Core.Model.DTO
{
    public class ImoCallSignDto
    {
        public int? IMO { get; set; }
        public string CallSign { get; set; }
    }
}
