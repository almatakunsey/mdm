﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;

namespace Vtmis.Core.Model.DTO.Methydro
{
    public class MethydroDetails : MethydroCommon
    {
        public MethydroInstruments Instruments { get; set; }
    }

    public class MethydroDetailsWithDate
    {
        public MethydroDetailsWithDate()
        {
            Methydros = new List<MethydroDetails>();
        }
        public DateTime Date { get; set; }
        public List<MethydroDetails> Methydros { get; set; }
    }

    public class MethydroDetailInstruments : MethydroCommon
    {
        public MethydroDetailInstruments()
        {
            Instruments = new List<MethydroInstruments>();
        }
        public List<MethydroInstruments> Instruments { get; set; }
    }


    public class MethydroInstruments
    {
        public DateTime Date { get; set; }
        public MetWaterData MetWaterData { get; set; }
        public MetAirData MetAirData { get; set; }
    }

    public class MethydroInstrument<T>
    {
        public DateTime Date { get; set; }
        public T MetData { get; set; }
    }

    public abstract class MethydroCommon
    {
        public int Id { get; set; }
        public int? MMSI { get; set; }
        public string StationName { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? DAC { get; set; }
        public int? FI { get; set; }
        public DateTime UtcRecvTime { get; set; }
        public DateTime LocalRecvTime { get; set; }
        public double Age
        {
            get
            {
                return (DateTime.Now - LocalRecvTime).TotalMinutes;
            }
        }
    }

    public class MetHydroDataTypeObj
    {
        public MetWaterData WaterData { get; set; }
        public MetAirData AirData { get; set; }
    }

    public class MetWaterData
    {
        public float? WaterLevel { get; set; }
        public float? WaterTemperature { get; set; }
        public float? CurrentSpeed { get; set; }
        public float? CurrentSpeed2 { get; set; }
        public float? CurrentSpeed3 { get; set; }
        public decimal? CurrentDirection { get; set; }
        public decimal? CurrentDirection2 { get; set; }
        public decimal? CurrentDirection3 { get; set; }
        public float? WaveHeight { get; set; }
        public decimal? WavePeriod { get; set; }
        public decimal? WaveDirection { get; set; }
        public Single? Visibility { get; set; }
    }

    public class MetAirData
    {
        public float? AirTemperature { get; set; }
        public decimal? AirPressure { get; set; }
        public decimal? Humidity { get; set; }
        public decimal? WindSpeed { get; set; }
        public decimal? WindGust { get; set; }
        public decimal? WindDirection { get; set; }
        public decimal? WindGustDirection { get; set; }
    }

    public class MethydroStation : MethydroCommon
    {
        public MethydroStation()
        {
            MethydroDataType = new List<MetHydroDataType>();
        }
        public List<MetHydroDataType> MethydroDataType { get; set; }
    }
}
