﻿using System;

namespace Vtmis.Core.Model.DTO.Methydro
{
    public class ResponseWeatherHistorical
    {
        public DateTime UtcRecvTime { get; set; }     
        public DateTime LocalRecvTime
        {
            get
            {
                DateTime date = DateTime.SpecifyKind(
                    UtcRecvTime, DateTimeKind.Utc);
                return date.ToLocalTime();
            }
        }
        public string Name { get; set; }
        public double? AirPressure { get; set; }
        public double? AirTemperature { get; set; }
        public double? RelHumidity { get; set; }
        public double? WindDirection { get; set; }
        public double? WindGust { get; set; }
        public double? WindGustDirection { get; set; }
        public double? WindSpeed { get; set; }
        public double? SurfCurrentDirection { get; set; }
        public double? SurfCurrentSpeed { get; set; }
        public double? WaterLevel { get; set; }
        public double? WaterTemp { get; set; }
        public double? WaveDirection { get; set; }
        public double? WaveHeight { get; set; }
        public double? WavePeriod { get; set; }
        public double? Visibility { get; set; }
        public double? CurrentDirection2 { get; set; }
        public double? CurrentDirection3 { get; set; }
        public double? CurrentSpeed2 { get; set; }
        public double? CurrentSpeed3 { get; set; }
    }
}
