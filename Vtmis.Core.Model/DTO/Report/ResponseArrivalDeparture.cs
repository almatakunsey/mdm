﻿using System;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Model.DTO.Report
{
    public class ResponseArrivalDeparture
    {
        public DateTime? ArrivalTime { get; set; }
        public DateTime? DepartureTime { get; set; }
        public Int16? TypeId { get; set; }
        public string Type
        {
            get
            {
                return CommonHelper.GetTargetTypeName(TypeId);
            }
        }
        public string Destination { get; set; }
        public string CallSign { get; set; }
        public Int32? IMO { get; set; }
        public Int32? MMSI { get; set; }
        public Int16? Length { get; set; }
        public string Name { get; set; }
        public DateTime ETA { get; set; }
    }
}
