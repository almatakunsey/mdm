﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtmis.Core.Model.DTO
{
    public class RequestReportEnterExit
    {
        public DateTime EnterTime { get; set; }
        public DateTime ExitTime { get; set; }
        public int ReportId { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
