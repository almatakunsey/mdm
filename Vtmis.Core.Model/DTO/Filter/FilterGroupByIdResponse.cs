﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using Vtmis.WebAdmin.Filters;

namespace Vtmis.Core.Model.DTO.Filter
{
    [AutoMapFrom(typeof(FilterGroup))]
    public class FilterGroupByIdResponse : EntityDto
    {
        public string Name { get; set; }

        public DateTime CreationTime { get; set; }

        public List<string> MMSIList { get; set; }
        public List<string> NameList { get; set; }
        public List<string> CallSignList { get; set; }
    }
}
