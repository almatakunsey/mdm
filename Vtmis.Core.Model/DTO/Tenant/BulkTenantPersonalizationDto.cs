﻿using System.Collections.Generic;

namespace Vtmis.Core.Model.DTO.Tenant
{
    public class BulkTenantPersonalizationDto
    {
        public BulkTenantPersonalizationDto()
        {
            TenantPersonalizationDto = new List<ResponseBulkTenantPersonalizationDto>();
        }
        public List<ResponseBulkTenantPersonalizationDto> TenantPersonalizationDto { get; set; }
        public string TenancyName { get; set; }
    }
}
