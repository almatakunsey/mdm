﻿namespace Vtmis.Core.Model.DTO.Tenant
{
    public class TenantPersonalizationDto
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string TenancyName { get; set; }
    }
}
