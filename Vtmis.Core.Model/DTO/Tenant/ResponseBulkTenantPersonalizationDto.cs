﻿using System.ComponentModel.DataAnnotations;

namespace Vtmis.Core.Model.DTO.Tenant
{
    public class ResponseBulkTenantPersonalizationDto
    {
        [Required]
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
