﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtmis.Core.Model.DTO
{
    public class ResponseLatitudeLogitude
    {
        public double Latitude { get; set; }

        public double Logitude { get; set; }

        public int ZoneType { get; set; }

        public decimal Radius { get; set; }
    }
}
