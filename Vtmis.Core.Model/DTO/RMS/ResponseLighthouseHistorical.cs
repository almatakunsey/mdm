﻿using System;
using Vtmis.Core.Common.Helpers.RMS;

namespace Vtmis.Core.Model.DTO.RMS
{
    public class ResponseLighthouseHistorical : RMSLighthouse
    {
        public DateTime UtcDateTime { get; set; }
        public DateTime LocalDateTime { get; set; }
        public int MMSI { get; set; }
        public string Name { get; set; }
        public int? LightSensorVal { get; set; }
        public int? BeatVal { get; set; }
        public int? MainLanternConditionVal { get; set; }
        public int? MainLanternStatusVal { get; set; }
        public int? StandbyLanternConditionVal { get; set; }
        public int? StandbyLanternStatusVal { get; set; }
        public int? EmergencyLanternConditionVal { get; set; }
        public int? EmergencyLanternStatusVal { get; set; }
        public int? OpticDriveAStatusVal { get; set; }
        public int? OpticDriveAConditionVal { get; set; }
        public int? OpticDriveBStatusVal { get; set; }
        public int? OpticDriveBConditionVal { get; set; }
        public int? HatchDoorVal { get; set; }
        public int? MainPowerVal { get; set; }
        public int? BMSConditionVal { get; set; } // Regulator or Battery Management System
        public int? OffPositionVal { get; set; }
        public RMSHealthStatusVal StatusSpecificVal { get; set; }
        public RMSHealthStatus StatusSpecific { get; set; }
    }
}
