﻿using System;
using Vtmis.Core.Common.Helpers.RMS;

namespace Vtmis.Core.Model.DTO.RMS
{
    public class ResponseLightBeaconHistorical : RMSLightBeaconApp
    {
        public DateTime UtcDateTime { get; set; }
        public DateTime LocalDateTime { get; set; }
        public int MMSI { get; set; }
        public string Name { get; set; }
        public int? BeatVal { get; set; }
        public int? AmbientVal { get; set; }
        public int? DoorVal { get; set; }
        public int? LanternVal { get; set; }
        public int? LanternBattVal { get; set; }
        public int? OffPositionVal { get; set; }
        public RMSHealthStatusVal StatusSpecificVal { get; set; }
        public RMSHealthStatus StatusSpecific { get; set; }
    }
}
