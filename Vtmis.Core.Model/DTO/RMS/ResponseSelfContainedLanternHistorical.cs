﻿using System;
using Vtmis.Core.Common.Helpers.RMS;

namespace Vtmis.Core.Model.DTO.RMS
{
    public class ResponseSelfContainedLanternHistorical : RMSSelfContainedLantern
    {
        public DateTime UtcDateTime { get; set; }
        public DateTime LocalDateTime { get; set; }
        public int MMSI { get; set; }
        public string Name { get; set; }
        public int? LightSensorVal { get; set; }
        public int? BeatVal { get; set; }
        public int? AlarmActiveVal { get; set; }
        public int? FlasherOffPowerThresVal { get; set; }
        public int? FlasherOffLowVinVal { get; set; }
        public int? FlasherOffPhotoCellVal { get; set; }
        public int? FlasherOffTemperatureVal { get; set; }
        public int? FlasherOffForceOffVal { get; set; }
        public int? IsNightVal { get; set; }
        public int? ErrorLedShortVal { get; set; }
        public int? ErrorLedOpenVal { get; set; }
        public int? ErrorLedVoltageLowVal { get; set; }
        public int? ErrorVinLowVal { get; set; }
        public int? ErrorPowerThresVal { get; set; }
        public int? FlasherAdjToLedVal { get; set; }
        public int? GSensorInterruptOccuredVal { get; set; }
        public int? SolarChargingOnVal { get; set; }
        public int? OffPositionVal { get; set; }
        public RMSHealthStatusVal StatusSpecificVal { get; set; }
        public RMSHealthStatus StatusSpecific { get; set; }
    }
}
