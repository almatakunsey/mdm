﻿using System;
using Vtmis.Core.Common.Helpers.RMS;

namespace Vtmis.Core.Model.DTO.RMS
{
    public class ResponseZeniLiteHistorical
    {
        public double? VoltageData { get; set; }
        public double? CurrentData { get; set; }
        public string PowerSupplyType { get; set; }
        public string LightStatus { get; set; }
        public string BatteryStatus { get; set; }
        public DateTime UtcDateTime { get; set; }
        public DateTime LocalDateTime { get; set; }
        public int MMSI { get; set; }
        public string Name { get; set; }
        public int? PowerSupplyTypeVal { get; set; }
        public double? LightStatusVal { get; set; }
        public int? BatteryStatusVal { get; set; }
        public string OffPosition { get; set; }
        public int? DAC { get; set; }
        public int? FI { get; set; }
        public int? OffPositionVal { get; set; }
    }
}
