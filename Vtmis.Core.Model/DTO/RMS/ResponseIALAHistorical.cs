﻿using System;
using Vtmis.Core.Common.Helpers.RMS;

namespace Vtmis.Core.Model.DTO.RMS
{
    public class ResponseIALAHistorical : RMSIALA
    {
        public DateTime UtcDateTime { get; set; }
        public DateTime LocalDateTime { get; set; }
        public int MMSI { get; set; }
        public string Name { get; set; }
        public int? OffPositionVal { get; set; }
        public RMSHealthStatusVal StatusSpecificVal { get; set; }
        public RMSHealthStatus StatusSpecific { get; set; }
    }
}
