﻿using System;

namespace Vtmis.Core.Model.DTO
{
    public class DateRangeDto
    {
        public DateTime? FromDateTime { get; set; }
        public DateTime? ToDateTime { get; set; }
    }
}
