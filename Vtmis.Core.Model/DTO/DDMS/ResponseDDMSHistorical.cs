﻿using System;

namespace Vtmis.Core.Model.DTO.DDMS
{
    public class ResponseDDMSHistorical
    {      
        public DateTime UtcRecvTime { get; set; }
        //change Utc Time to LocalTime
        public DateTime LocalRecvTime
        {
            get
            {
                return DateTime.SpecifyKind(new DateTime(UtcRecvTime.Year, UtcRecvTime.Month,
                                            UtcRecvTime.Day, UtcRecvTime.Hour, UtcRecvTime.Minute,
                                            UtcRecvTime.Second), DateTimeKind.Utc).ToLocalTime();
            }
        }
        public double? Speed { get; set; }
        public string Empty { get; set; }
        public string Full { get; set; }
        public string Actual { get; set; }
        public int? Loaded { get; set; }
        public string LoadedDesc { get; set; }
        public int? Supply { get; set; }
        public string SupplyDesc { get; set; }
        public int? Half { get; set; }
        public string Battery { get; set; }
        public string Name { get; set; }
        public double? MiddleValue { get; set; }
        public bool? Cover { get; set; }
        public string CoverDesc { get; set; }
        public int? Sonar { get; set; }
        public string SonarDesc { get; set; }
        public int? Hoppers { get; set; }
        public string HopperDesc { get; set; }
        public int? HopperFlag { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
    }
}
