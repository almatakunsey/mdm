﻿using System.Collections.Generic;

namespace Vtmis.Core.Model
{
    public class ModelResult<T>
    {
        public ModelResult()
        {
            Errors = new List<string>();
        }
        public T Result { get; set; }
        public bool IsSuccessful { get; set; } = false;
        public List<string> Errors { get; set; } = null;
    }
}
