﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Vtmis.Core.Model
{
    public class PaginatedList<T>
    {
        public int PageIndex { get;  set; }
        public int TotalPages { get; set; }
        public List<T> Items { get; set; }
        public int TotalItems { get; set; }
        public PaginatedList(List<T> items, int count, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalItems = count;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            Items = new List<T>();
            Items.AddRange(items);
        }

        public PaginatedList()
        {

        }

        public bool HasPreviousPage
        {
            get
            {
                return (PageIndex > 1);
            }
        }

        public bool HasNextPage
        {
            get
            {
                return (PageIndex < TotalPages);
            }
        }

        public PaginatedList<T> SetNull()
        {
            return new PaginatedList<T>(new List<T>(), 0, 0, 0);
        }

        public PaginatedList<T> Create(
           IEnumerable<T> source, int pageIndex, int pageSize)
        {
            var count = source.Count();
            var items = source.Skip(
                (pageIndex - 1) * pageSize)
                .Take(pageSize).ToList();
            return new PaginatedList<T>(items, count, pageIndex, pageSize);
        }

        public async Task<PaginatedList<T>> CreateAsync(
           IQueryable<T> source, int count, int pageIndex, int pageSize)
        {
            //var count = await source.CountAsync();
            var items = await source.Skip(
                (pageIndex - 1) * pageSize)
                .Take(pageSize).ToListAsync();
            return new PaginatedList<T>(items, count, pageIndex, pageSize);
        }

        public async Task<PaginatedList<T>> CreateAsync(
            IQueryable<T> source, int pageIndex, int pageSize)
        {
            var count = await source.CountAsync();
            var items = await source.Skip(
                (pageIndex - 1) * pageSize)
                .Take(pageSize).ToListAsync();
            return new PaginatedList<T>(items, count, pageIndex, pageSize);
        }

        //Solution to bug in EF Core 2.1 
        //TODO: Remove this if EF has been updated
        public async Task<PaginatedList<T>> CreateTempAsync(
            IQueryable<T> source, int pageIndex, int pageSize)
        {
            var srcList = await source.ToListAsync();
            var count = srcList.Count();
            var items = srcList.Skip(
                (pageIndex - 1) * pageSize)
                .Take(pageSize);
            return new PaginatedList<T>(items.ToList(), count, pageIndex, pageSize);
        }

        public PaginatedList<T> CreateTemp(
            IQueryable<T> source, int pageIndex, int pageSize)
        {
            //var srcList = await source.ToListAsync();
            var count = source.Count();
            var items = source.Skip(
                (pageIndex - 1) * pageSize)
                .Take(pageSize);
            return new PaginatedList<T>(items.ToList(), count, pageIndex, pageSize);
        }
    }
}
