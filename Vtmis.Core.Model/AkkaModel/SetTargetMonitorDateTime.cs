﻿using System;

namespace Vtmis.Core.Model.AkkaModel
{
    public class SetTargetMonitorDateTime
    {
        public SetTargetMonitorDateTime(DateTime currentDate)
        {
            CurrentDate = currentDate;
        }

        public DateTime CurrentDate { get; }
    }
}
