﻿using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.WebAdmin.Lloyds;

namespace Vtmis.Core.Model.AkkaModel
{
    public class ResponseTargetDetail
    {
        public ResponseTargetDetail(TargetDetail vessel, Lloyd lloyds)
        {
            Vessel = vessel;
            Lloyds = lloyds;
        }

        public TargetDetail Vessel { get;  }
        public Lloyd Lloyds { get;  }
    }
}
