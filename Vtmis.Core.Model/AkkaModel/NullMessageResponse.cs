﻿using System;

namespace Vtmis.Core.Model.AkkaModel
{
    public class NullMessageResponse
    {
        public NullMessageResponse(string error = null, Exception errorDetail = null)
        {
            Error = error;
            ErrorDetail = errorDetail;
        }
        public string Error { get; set; }
        public Exception ErrorDetail { get; set; }
    }
}
