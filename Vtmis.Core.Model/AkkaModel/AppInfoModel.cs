﻿using System;

namespace Vtmis.Core.Model.AkkaModel
{
    public class AppInfoModel
    {
        public DateTime DbDate { get; set; }
        public string ServerName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Environment { get; set; }
    }
}
