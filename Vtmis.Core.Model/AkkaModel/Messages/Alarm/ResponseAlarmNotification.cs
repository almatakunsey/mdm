﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Alarm
{
    public class ResponseAlarmNotification
    {
        public ResponseAlarmNotification(long alarmId, string alarmName, long eventId, long eventDetailId, int numOfTargetsAffected, 
            string aisMessage, string message, string priority, string color)
        {
            AlarmId = alarmId;
            AlarmName = alarmName;
            EventId = eventId;
            EventDetailId = eventDetailId;
            NumOfTargetsAffected = numOfTargetsAffected;
            AisMessage = aisMessage;
            Message = message;
            Priority = priority;
            Color = color;
        }

        public long AlarmId { get; }
        public string AlarmName { get; }
        public long EventId { get; }
        public long EventDetailId { get; }
        public int NumOfTargetsAffected { get; }
        public string AisMessage { get; }
        public string Message { get; }
        public string Priority { get;  }
        public string Color { get;  }
    }
}
