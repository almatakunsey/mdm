﻿using Microsoft.AspNetCore.SignalR.Client;

namespace Vtmis.Core.Model.AkkaModel.Messages.Alarm
{
    public class AlarmWorkerMsg
    {
        public AlarmWorkerMsg(long eventId)
        {
            EventId = eventId;
        }

        public long EventId { get; }
    }
}
