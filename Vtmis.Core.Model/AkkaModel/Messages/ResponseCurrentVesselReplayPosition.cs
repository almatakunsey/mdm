﻿using System.Collections.Generic;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class ResponseCurrentVesselReplayPosition
    {
        public ResponseCurrentVesselReplayPosition(IEnumerable<VesselPosition> vesselPositions)
        {
            VesselPositions = vesselPositions;
        }

        public IEnumerable<VesselPosition> VesselPositions { get; internal set; }
    }
}
