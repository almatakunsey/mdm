﻿using System;
using System.Collections.Generic;
using Vtmis.Core.VesselEntityFrameworkCore.Models;

namespace Vtmis.Core.Model.AkkaModel.Messages
{


    public class ShipPositionResponse
    {
        public bool Stream { get; set; }

        public bool Status { get; set; }

        public List<string> PrefixDbNames { get; set; }

        public List<ShipPosition> ShipPositions { get; set; }
    }

    public class VesselsByFromDateRequest
    {
        public DateTime FromDate { get; set; }
    }

    public class VesselsByFromDateResponse
    {
        public string Error { get; set; }
        public string Status { get; set; }
        public List<VesselDetail> VesselDetails { get; set; }
    }

    public class VesselDetail
    {
        public int MMSI { get; set; }

        public string VesselName { get; set; }
    }

    public class VesselTimelineRequest
    {
        public int MMSI { get; set; }

        public DateTime FromDateTime { get; set; }

        public DateTime ToDateTime { get; set; }
    }

    public class VesselTimelineResponse
    {
        public bool Status { get; set; }
        public string Error { get; set; }

        public List<ShipPosition> ShipPositions { get; set; }
    }

    

}
