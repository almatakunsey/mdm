﻿using System.Collections.Generic;
using Vtmis.Core.Model.ViewModels.Target;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class SearchTarget
    {
        public SearchTarget(AdvancedSearchViewModel advancedSearchViewModel, 
            IEnumerable<VesselPosition> vesselPositions)
        {
            AdvancedSearchViewModel = advancedSearchViewModel;
            VesselPositions = vesselPositions;
        }
        public AdvancedSearchViewModel AdvancedSearchViewModel { get; private set; }
        public IEnumerable<VesselPosition> VesselPositions { get; set; }
    }
}
