﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    /// <summary>
    /// 
    /// </summary>
    public class SpeedViolation
    {
        public SpeedViolation(int shipPositionID, string name, double lat,double lgt, int? imo, int? mmsi,
            double? minSpeed, double? maxSpeed, double? averageSpeed, int? duration, DateTime time)
        {
            Name = Name;
            MMSI = mmsi;
            IMO = imo;
            Time = Time;
            Latitude = lat;
            Longitude = lgt;
            MinSpeed = minSpeed;
            MaxSpeed = maxSpeed;
            AverageSpeed = averageSpeed;
            Duration = duration;
            ShipPositionID = shipPositionID;
        }
        public SpeedViolation()
        { }
        public int ShipPositionID { get; set; }
        public string Name { get; set; }
        public int? MMSI { get; set; }
        public int? IMO { get; set; }
        public DateTime Time { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public double? MinSpeed { get; set; }
        public double? MaxSpeed { get; set; }
        public double? AverageSpeed { get; set; }
        public int? Duration { get; set; }
        public int VoilationType { get; set; }
    }
}
