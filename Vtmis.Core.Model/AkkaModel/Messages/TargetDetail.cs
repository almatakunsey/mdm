﻿using System;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class TargetDetail : TargetPosition
    {
        public TargetDetail(int id, int? mmsi, string targetName, int? aISVersion, int? iMO,
            int? atoNType, DateTime? eTA, string vendor, string destination, int? shipType, string callSign, float? maxDraught, int? virtualAtonStatus,
            float? analogueExt1, float? analogueExt2, float? analogueInt, int? dimension_A, int? dimension_B, int? dimension_C, int? dimension_D,
            int? positionFixingDevice, int? offPosition21, int? health21, int? light21, int? rACON21, int? health68, int? light68, int? rACON68,
            int? offPosition68, int? digitalInput, DateTime? localReceivedTime, DateTime? utcReceivedTime, double latitude, double longitude,
                float? rot, float? sog, float? cog, int? trueHeading, int messageId, int? raim, int? navStatus, string ip, int? positionAccuracy)
                : base(id, localReceivedTime, utcReceivedTime, latitude, longitude,
                    rot, sog, cog, trueHeading, mmsi, messageId, raim, navStatus, utcReceivedTime, ip, positionAccuracy)
        {
            Id = id;
            TargetName = targetName;
            AISVersion = aISVersion;
            IMO = iMO;
            AtoNType = atoNType;
            ETA = eTA;
            Vendor = vendor;
            Destination = destination;
            ShipType = shipType;
            CallSign = callSign;
            MaxDraught = maxDraught;
            VirtualAtonStatus = virtualAtonStatus;
            AnalogueExt1 = analogueExt1;
            AnalogueExt2 = analogueExt2;
            AnalogueInt = analogueInt;
            Dimension_A = dimension_A;
            Dimension_B = dimension_B;
            Dimension_C = dimension_C;
            Dimension_D = dimension_D;
            PositionFixingDevice = positionFixingDevice;
            OffPosition21 = offPosition21;
            Health21 = health21;
            Light21 = light21;
            RACON21 = rACON21;
            Health68 = health68;
            Light68 = light68;
            RACON68 = rACON68;
            OffPosition68 = offPosition68;
            DigitalInput = digitalInput;
        }

        public int Id { get; private set; }
        public string TargetName { get; private set; }
        public int? AISVersion { get; private set; }
        public int? IMO { get; private set; }

        public int? AtoNType { get; private set; }
        public DateTime? ETA { get; private set; }
        public string Vendor { get; private set; }
        public string Destination { get; private set; }
        public int? ShipType { get; private set; }
        public string ShipType_ToString
        {
            get
            {
                return TargetHelper.GetShipTypeByShipTypeId(ShipType);
            }
        }
        public string CallSign { get; private set; }
        public float? MaxDraught { get; private set; }
        public string AtoNType_ToString
        {
            get
            {
                return TargetHelper.GetAtonTypeByAtonTypeId(AtoNType);
            }
        }
        public int? VirtualAtonStatus { get; private set; }
        public string VirtualAtoN_ToString
        {
            get
            {
                return TargetHelper.GetVirtualAtonStatus(VirtualAtonStatus);
            }
        }
        public float? AnalogueExt1 { get; set; }
        public float? AnalogueExt2 { get; set; }
        public float? AnalogueInt { get; set; }
        public int? Dimension_A { get; private set; }
        public int? Dimension_B { get; private set; }
        public int? Dimension_C { get; private set; }
        public int? Dimension_D { get; private set; }
        public string LengthXbeam
        {
            get
            {
                return TargetHelper.GetLengthXBeam(true, Dimension_A,
                    Dimension_B, Dimension_C, Dimension_D, MessageId);
            }
        }
        public int? PositionFixingDevice { get; private set; }
        public string PositionFixingDeviceToString
        {
            get
            {
                return TargetHelper.GetPositionFixingDevice(PositionFixingDevice);
            }
        }
        public int? OffPosition21 { get; set; }
        public int? Health21 { get; set; }
        public int? Light21 { get; set; }
        public int? RACON21 { get; set; }
        public int? Health68 { get; set; }
        public int? Light68 { get; set; }
        public int? RACON68 { get; set; }
        public string AtoNStatus21
        {
            get
            {
                return TargetHelper.GetAtonStatus(Health21, Light21, RACON21);
            }
        }
        public int? OffPosition68 { get; }
        public int? DigitalInput { get; }
        public string DigitalInput_ToString { get; }
        public string AtoNStatus68
        {
            get
            {
                return TargetHelper.GetAtonStatus(Health68, Light68, RACON68);
            }
        }
        public string AnalogueInt_1_2
        {
            get
            {
                return TargetHelper.GetAnalogueInt_1_2(AnalogueInt, AnalogueExt1, AnalogueExt2);
            }
        }

        public string OffPosition21_ToString
        {
            get
            {
                return TargetHelper.GetOffPosition(OffPosition21);
            }
        }
        public string OffPosition68_ToString
        {
            get
            {
                return TargetHelper.GetOffPosition(OffPosition68);
            }
        }

    }
}
