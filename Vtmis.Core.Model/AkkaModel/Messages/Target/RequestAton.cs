﻿using Vtmis.Core.Common.Enums;

namespace Vtmis.Core.Model.AkkaModel.Messages.Target
{
    public class RequestAton : PageRequest
    {
        public RequestAton(TargetClass type, int pageIndex, int pageSize) : base(pageIndex, pageSize)
        {
            Type = type;
        }

        public TargetClass Type { get; }
    }

}
