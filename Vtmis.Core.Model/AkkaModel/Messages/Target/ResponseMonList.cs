﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Target
{
    public class ResponseMonList : ResponseAtonList
    {
        //public string RACON { get; set; }
        //public string RACON21 { get; set; }
        //public string Light { get; set; }
        //public string Health { get; set; }
        public Single? AnalogueInt { get; set; }
        public Single? AnalogueExt1 { get; set; }
        public Single? AnalogueExt2 { get; set; }
        public Single? VoltageData { get; set; }
        public Single? CurrentData { get; set; }
        public string Battery { get; set; }
        public Int16? PhotoCell { get; set; }
    }
}
