﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Target
{
    public class RequestMethydroList : PageRequest
    {
        public RequestMethydroList(int pageIndex, int pageSize) : base(pageIndex, pageSize)
        {
        }
    }
}
