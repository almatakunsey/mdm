﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Target.DDMS
{
    public class RequestDDMS
    {
        public RequestDDMS(int mmsi)
        {
            MMSI = mmsi;
        }
        public int MMSI { get; }
    }
}
