﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Target.DDMS
{
    public class ResponseDDMSInfo
    {
        public string MaxAirDraught { get; set; }
        public string MinAirDraught { get; set; }
        public string ActualAirDraught { get; set; }
        public string Voltage { get; set; }
        public string CasingCover { get; set; }
        public string HalfFull { get; set; }
        public string ShipLoad { get; set; }
        public string Sonar { get; set; }
        public string Supply { get; set; }

        public int NumHoppers { get; set; }
        public string Hopper { get; set; }
    }
}
