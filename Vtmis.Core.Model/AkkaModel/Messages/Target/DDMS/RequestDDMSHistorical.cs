﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Target.DDMS
{
    public class RequestDDMSHistorical
    {
        public RequestDDMSHistorical(int mmsi, DateTime startDate, DateTime endDate)
        {
            MMSI = mmsi;
            StartDate = startDate;
            EndDate = endDate;
        }

        public int MMSI { get; }
        public DateTime StartDate { get; }
        public DateTime EndDate { get; }
    }
}
