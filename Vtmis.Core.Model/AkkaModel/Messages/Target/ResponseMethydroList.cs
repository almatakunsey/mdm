﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Target
{
    public class ResponseMethydroList : ResponseAtonList
    {
        public double? AirTemp { get; set; }
        public double? AirPres { get; set; }
        public double? WndSpeed { get; set; }
        public double? WndGust { get; set; }
        public double? WndDir { get; set; }
    }
}
