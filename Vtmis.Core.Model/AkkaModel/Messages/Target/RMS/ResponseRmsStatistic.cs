﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Target.RMS
{
    public class ResponseRmsStatistic
    {
        public int MMSI { get; set; }
        public DateTime UtcDateTime { get; set; }
        public DateTime LocalDateTime
        {
            get
            {
                var utcDate = DateTime.SpecifyKind(
                  UtcDateTime, DateTimeKind.Utc);
                return utcDate.ToLocalTime();
            }
        }
        public string VoltageData { get; set; }
        public string CurrentData { get; set; }
        public string Light { get; set; }
        public string AnalogueExt1 { get; set; }
        public string AnalogueExt2 { get; set; }
        public string Battery { get; set; }
        public string AnalogueInt { get; set; }
        public string Beat { get; set; }
        public string Door { get; set; }
        public string Ambient { get; set; }
        public string Lantern { get; set; }
        public string LanternBattery { get; set; }

    }
}
