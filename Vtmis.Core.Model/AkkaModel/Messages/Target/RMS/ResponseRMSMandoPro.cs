﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Target.RMS
{
    public class ResponseRMSMandoPro : ResponseRMSBase
    {
        public ResponseRMSMandoPro(string ambient, string beat, string alarmActive, string flshrOffByLedPwrThres, string flshrOffByLowVin, 
            string flshrOffByPhotocell, string flshrOffByTemp, string flshrOffByForceOff, string isNight, string errLedShort, string errLedOpen, 
            string errLedVoltageLow, string errVinLow, string errLedPowerThres, string flshrAdjToLedMaxAvgPwr, string gsensorInterruptOccur, 
            string solarChargingOn, int? dac, int? fi)
        {
            Ambient = ambient;
            Beat = beat;
            AlarmActive = alarmActive;
            FlshrOffByLedPwrThres = flshrOffByLedPwrThres;
            FlshrOffByLowVin = flshrOffByLowVin;
            FlshrOffByPhotocell = flshrOffByPhotocell;
            FlshrOffByTemp = flshrOffByTemp;
            FlshrOffByForceOff = flshrOffByForceOff;
            IsNight = isNight;
            ErrLedShort = errLedShort;
            ErrLedOpen = errLedOpen;
            ErrLedVoltageLow = errLedVoltageLow;
            ErrVinLow = errVinLow;
            ErrLedPowerThres = errLedPowerThres;
            FlshrAdjToLedMaxAvgPwr = flshrAdjToLedMaxAvgPwr;
            GsensorInterruptOccur = gsensorInterruptOccur;
            SolarChargingOn = solarChargingOn;
            DAC = dac;
            FI = fi;
        }

        public string Ambient { get;  }
        public string Beat { get;  }
        public string AlarmActive { get;  }
        public string FlshrOffByLedPwrThres { get;  }
        public string FlshrOffByLowVin { get;  }
        public string FlshrOffByPhotocell { get;  }
        public string FlshrOffByTemp { get;  }
        public string FlshrOffByForceOff { get;  }
        public string IsNight { get;  }
        public string ErrLedShort { get;  }
        public string ErrLedOpen { get;  }
        public string ErrLedVoltageLow { get;  }
        public string ErrVinLow { get;  }
        public string ErrLedPowerThres { get;  }
        public string FlshrAdjToLedMaxAvgPwr { get;  }
        public string GsensorInterruptOccur { get;  }
        public string SolarChargingOn { get;  }

        public int? DAC { get;  }
        public int? FI { get;  }
    }
}
