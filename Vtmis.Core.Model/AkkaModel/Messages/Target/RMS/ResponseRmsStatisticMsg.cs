﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtmis.Core.Model.AkkaModel.Messages.Target.RMS
{
    public class ResponseRmsStatisticMsg
    {
        public ResponseRmsStatisticMsg(List<ResponseRmsStatistic> results)
        {
            Results = results;
        }

        public List<ResponseRmsStatistic> Results { get; }
    }
}
