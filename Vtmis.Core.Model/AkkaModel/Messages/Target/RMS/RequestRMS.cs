﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Target.RMS
{
    public class RequestRMS
    {
        public RequestRMS(int mmsi)
        {
            MMSI = mmsi;
        }
        public int MMSI { get; }
    }
}
