﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Target.RMS
{
    public class RequestRmsHistorical
    {
        public RequestRmsHistorical(int mmsi, DateTime startDate, DateTime endDate)
        {
            MMSI = mmsi;
            StartDate = startDate;
            EndDate = endDate;
        }

        public int MMSI { get; set; }
        public DateTime StartDate { get;  }
        public DateTime EndDate { get;  }
    }
}
