﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Target.RMS
{
    public class ResponseRMS : ResponseRMSBase
    {
        public ResponseRMS(string beat, string lanternBatt, string lanternStat, string ambient, string door, int? dac, int? fi)
        {
            Beat = beat;
            LanternBatt = lanternBatt;
            LanternStat = lanternStat;
            Ambient = ambient;
            Door = door;
            DAC = dac;
            FI = fi;
        }

        public string Beat { get;  }
        public string LanternBatt { get;  }
        public string LanternStat { get;  }
        public string Ambient { get;  }
        public string Door { get;  }
        public int? DAC { get;  }
        public int? FI { get;  }
    }
}
