﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Target
{
    public class ResponseAtonList : ResponseAtonBase
    {
        public string StationId { get; set; }
        public int MMSI { get; set; }
        public string Name { get; set; }
        public string AisName { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Type { get; set; }
        public string VirtualAton { get; set; }
        public string OffPosition68 { get; set; } // 6/8
        public string OffPosition21 { get; set; } // 21
        public string LastReport6 { get; set; }
        public string LastReport8 { get; set; }
        public string LastReport21 { get; set; }
        public string Light68 { get; set; }
        public string Racon68 { get; set; }
        public string Health68 { get; set; }
        public string Light21 { get; set; }
        public string Racon21 { get; set; }
        public string Health21 { get; set; }


        public double? Light68Val { get; set; }
        public double? Racon68Val { get; set; }
        public double? Health68Val { get; set; }
        public double? Light21Val { get; set; }
        public double? Racon21Val { get; set; }
        public double? Health21Val { get; set; }
    }
}
