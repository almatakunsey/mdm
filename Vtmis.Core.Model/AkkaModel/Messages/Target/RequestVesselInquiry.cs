﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Target
{
    public class RequestVesselInquiry
    {
        public RequestVesselInquiry(int? mmsi, int? iMO, string callSign, string targetName, int pageIndex, int pageSize)
        {
            MMSI = mmsi;
            IMO = iMO;
            CallSign = callSign;
            TargetName = targetName;
            PageIndex = pageIndex;
            PageSize = pageSize;
        }

        public int? MMSI { get;  }
        public int? IMO { get;  }
        public string CallSign { get;  }
        public string TargetName { get;  }
        public int PageIndex { get;  }
        public int PageSize { get;  }
    }
}
