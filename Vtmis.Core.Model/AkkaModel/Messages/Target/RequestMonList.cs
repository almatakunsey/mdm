﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Target
{
    public class RequestMonList : PageRequest
    {
        public RequestMonList(int pageIndex, int pageSize) : base(pageIndex, pageSize)
        {
        }
    }
}
