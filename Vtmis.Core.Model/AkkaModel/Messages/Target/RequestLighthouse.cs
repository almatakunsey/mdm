﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Target
{
    public class RequestLighthouse
    {
        public RequestLighthouse(int mmsi)
        {
            MMSI = mmsi;
        }
        public int MMSI { get; }
    }
}
