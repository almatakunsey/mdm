﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Report
{
    public class VesselWithHighSpeed
    {
        public int MMSI { get; set; }
        public double DurationForMaxSpeed { get; set; }
        public double AvgSpeedForMaxSpeed { get; set; }
        public int AvgSpeedCounterForMaxSpeed { get; set; }
        public DateTime? FirstVesselViolationTimeForMaxSpeed { get; set; }
    }
}
