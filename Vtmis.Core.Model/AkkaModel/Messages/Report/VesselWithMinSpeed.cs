﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Report
{
    public class VesselWithMinSpeed
    {
        public int MMSI { get; set; }
        public double DurationForMinSpeed { get; set; }
        public double AvgSpeedForMinSpeed { get; set; }
        public int AvgSpeedCounterForMinSpeed { get; set; }
        public DateTime? FirstVesselViolationTimeForMinSpeed { get; set; }
    }
}
