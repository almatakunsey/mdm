﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Report
{
    public class ArrivalDetails
    {
        public ArrivalDetails(DateTime? arrivalTime, string name, int mmsi, string callSign, string callType, double length, string destination,
            DateTime? eta)
        {
            ArrivalTime = arrivalTime;
            Name = name;
            MMSI = mmsi;
            CallSign = callSign;
            CallType = callType;
            Length = length;
            Destination = destination;
            ETA = eta;
        }

        public DateTime? ArrivalTime { get; }
        public string Name { get; }
        public int MMSI { get; }
        public string CallSign { get; }
        public string CallType { get; }
        public double Length { get; }
        public string Destination { get; }
        public DateTime? ETA { get; }
    }
}
