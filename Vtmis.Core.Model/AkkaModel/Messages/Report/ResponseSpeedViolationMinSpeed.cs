﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Report
{
    public class ResponseSpeedViolationMinSpeed
    {
        public int MMSI { get; set; }
        public string Name { get; set; }
        public int IMO { get; set; }
        public DateTime LocalRecvTime { get; set; }
        public string Latitude { get; set; }
        public string Lat_Degree { get; set; }
        public string Longitude { get; set; }
        public string Long_Degree { get; set; }
        public double MinSpeed { get; set; }
        public string AverageSpeed { get; set; }
        public string Duration { get; set; }

        
    }
}
