﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Report
{
    public class DepartureDetails
    {
        public DepartureDetails(DateTime? departureTime, string name, int mmsi, string callSign, string callType, double length, string destination,
            DateTime? eta)
        {
            DepartureTime = departureTime;
            Name = name;
            MMSI = mmsi;
            CallSign = callSign;
            CallType = callType;
            Length = length;
            Destination = destination;
            ETA = eta;
        }

        public DateTime? DepartureTime { get; }
        public string Name { get; }
        public int MMSI { get; }
        public string CallSign { get; }
        public string CallType { get; }
        public double Length { get; }
        public string Destination { get; }
        public DateTime? ETA { get; }
    }
}
