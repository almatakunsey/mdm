﻿using System.Collections.Generic;

namespace Vtmis.Core.Model.AkkaModel.Messages.Report
{
    public class CollectSpeedViolationWorker
    {

        public CollectSpeedViolationWorker(List<ResponseSpeedViolationMinSpeed> minSpeedResultList, List<ResponseSpeedViolationMaxSpeed> maxSpeedResultList)
        {
            this.MinSpeedResultList = minSpeedResultList;
            this.MaxSpeedResultList = maxSpeedResultList;
        }
        public List<ResponseSpeedViolationMinSpeed> MinSpeedResultList { get; }
        public List<ResponseSpeedViolationMaxSpeed> MaxSpeedResultList { get; }
    }
}
