﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Report
{
    public class ArrivalDepartureResponse
    {
       
        public ArrivalDepartureResponse(DateTime? arrivalTime, DateTime? departureTime, string name, int mmsi, string callSign, string callType,
            double length, string destination, DateTime? eTA)
        {
            ArrivalTime = arrivalTime;
            DepartureTime = departureTime;
            Name = name;
            MMSI = mmsi;
            CallSign = callSign;
            CallType = callType;
            Length = length;
            Destination = destination;
            ETA = eTA;
        }

        public DateTime? ArrivalTime { get; set; }
        public DateTime? DepartureTime { get; set; }
        public string Name { get; set; }
        public int MMSI { get; set; }
        public string CallSign { get; set; }
        public string CallType { get; set; }
        public double Length { get; set; }
        public string Destination { get; set; }
        public DateTime? ETA { get;set; }
    }
}
