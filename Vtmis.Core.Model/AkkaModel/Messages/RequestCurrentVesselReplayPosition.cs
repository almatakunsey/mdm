﻿namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class RequestCurrentVesselReplayPosition
    {
        public RequestCurrentVesselReplayPosition(string postFixDbName,int count)
        {
            PostFixDbName = postFixDbName;
            Count = count;
        }

        public string PostFixDbName { get; internal set; }
        public int Count { get; }
    }
}
