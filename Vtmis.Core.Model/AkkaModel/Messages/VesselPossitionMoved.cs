﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class VesselPossitionMoved
    {
        public VesselPossitionMoved(string vesselId,double lat,double lgt, string vesselName, string callSign, DateTime? receivedTime,
            string destination, string vendor,int? imo, double? rot, double? sog, double? cog)
        {
            VesselId = vesselId;
            Lat = lat;
            Lgt = lgt;
            VesselName = vesselName;
            CallSign = callSign;
            Sog = sog;
            Rot = rot;
            Cog = cog;
            ReceivedTime = receivedTime;
            Destination = destination;
            Vendor = vendor;
            Imo = Imo;
        }

        public string VesselId { get; }
        public double Lat { get; }
        public double Lgt { get; }
        public string VesselName { get; }
        public string CallSign { get; }
        public double? Sog { get; }
        public double? Rot { get; }
        public double? Cog { get; }
        public DateTime? ReceivedTime { get; }
        public string Destination { get; }
        public string Vendor { get; }
        public int? Imo { get; }
    }
}
