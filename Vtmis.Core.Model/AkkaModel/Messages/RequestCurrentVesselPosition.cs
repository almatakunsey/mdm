﻿namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class RequestCurrentVesselPosition
    {
        public RequestCurrentVesselPosition(string userId)
        {           
            UserId = userId;
        }    
        public string UserId { get; }
    }
}
