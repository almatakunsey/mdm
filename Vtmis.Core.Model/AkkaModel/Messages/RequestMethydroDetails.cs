﻿using System;
using Vtmis.Core.Common.Enums;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class RequestMethydroDetails
    {
        public RequestMethydroDetails(DateTime fromDate, DateTime toDate,
            MetHydroDataType? metHydroDataType)
        {
            FromDate = fromDate;
            ToDate = toDate;
            MetHydroDataType = metHydroDataType;
        }

        public DateTime FromDate { get; }
        public DateTime ToDate { get; }
        public MetHydroDataType? MetHydroDataType { get; }
    }
}
