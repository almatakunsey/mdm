﻿using System;
using Vtmis.Core.Common.Enums;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class RequestMethydroInstruments
    {
        public RequestMethydroInstruments(int mmsi, DateTime fromDate, DateTime toDate,
            MetHydroDataType? metHydroDataType)
        {
            MMSI = mmsi;
            FromDate = fromDate;
            ToDate = toDate;
            MetHydroDataType = metHydroDataType;
        }

        public int MMSI { get;  }
        public DateTime FromDate { get; }
        public DateTime ToDate { get; }
        public MetHydroDataType? MetHydroDataType { get; }
    }
}
