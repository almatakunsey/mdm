﻿namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class StartMap
    {
        public string ApiUrl;

        public StartMap(string userId, string apiUrl)
        {
            UserId = userId;
            this.ApiUrl = apiUrl;
        }

        public string UserId { get; }

    }

    public class UpdateMap
    {
        public UpdateMap(string userId)
        {
            UserId = userId;           
        }

        public string UserId { get; }
      
    }
}
