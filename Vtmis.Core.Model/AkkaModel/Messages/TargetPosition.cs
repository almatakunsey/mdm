﻿using System;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class TargetPosition
    {
        public TargetPosition()
        {

        }
        //private readonly int? _navStatus;
        public TargetPosition(int shipPositionId, DateTime? localReceivedTime, DateTime? receivedTime,
            double latitude, double longitude,
            float? rot, float? sog, float? cog, int? trueHeading, int? mmsi, int messageId, int? raim,
            int? nav_Status, DateTime? utc, string ip, int? positionAccuracy)
        {
            ShipPositionId = shipPositionId;
            LocalReceivedTime = localReceivedTime;
            ReceivedTime = receivedTime;
            Latitude = latitude;
            Longitude = longitude;
            ROT = rot;
            SOG = sog;
            COG = cog;
            TrueHeading = trueHeading;
            MMSI = mmsi;
            MessageId = messageId;
            RAIM = raim;
            NavigationalStatus = nav_Status;
            UTC = utc;
            IP = ip;
            PositionAccuracy = positionAccuracy;
        }
        public int ShipPositionId { get; private set; }
        public DateTime? LocalReceivedTime { get; private set; }
        public DateTime? ReceivedTime { get; private set; }
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }
        public float? ROT { get; private set; }
        public string RotToString
        {
            get
            {
                if (CommonHelper.GetTargetClassByMessageId(MessageId) == TargetClass.ClassA)
                {
                    if (ROT > 720 || ROT < -720)
                    {
                        return null;
                    }
                    if (ROT < 0)
                    {
                        var result = Math.Round((double)ROT, 2);
                        return $"Portside {result.ToString("0.0")}°/min";
                    }

                    if (ROT > 0)
                    {
                        var result = Math.Round((double)ROT, 2);
                        return $"Starboard {result.ToString("0.0")}°/min";
                    }
                    if (ROT == 0)
                    {
                        return $"{ROT}°/min";
                    }
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }
        public float? SOG { get; private set; }
        public float? COG { get; private set; }
        public int? TrueHeading { get; private set; }
        public int? MMSI { get; private set; }
        public int MessageId { get; private set; }
        public int? RAIM { get; private set; }
        public string NavigationalStatusToString
        {
            get
            {
                return TargetHelper.GetNavStatusDescriptionByNavStatusId(NavigationalStatus);
            }
        }
        public int? NavigationalStatus { get; private set; }
        public DateTime? UTC { get; private set; }
        public string IP { get; private set; }
        public int? PositionAccuracy { get; private set; }
        public string PositionAccuracyToString
        {
            get
            {
                return TargetHelper.GetPositionAccuracy(PositionAccuracy);
            }
        }
        public string LastSeen
        {
            get
            {
                return CommonHelper.TimeAgo(LocalReceivedTime.Value);
            }
        }
    }
}
