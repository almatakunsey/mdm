﻿using System.Collections.Generic;
using Vtmis.Core.VesselEntityFrameworkCore.Models;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class GetAllShipPosition
    {
        public string PostFixDbName { get; internal set; }
        public string SqlConnectionString { get; internal set; }
    }

    public class ResponseGetAllShipPosition
    {
        public ResponseGetAllShipPosition(IEnumerable<ShipPosition> shipPositions,string sqlConnectionString)
        {
            ShipPositions = shipPositions;
            SqlConnectionString = sqlConnectionString;
        }

        public IEnumerable<ShipPosition> ShipPositions { get; }
        public string SqlConnectionString { get; }
    }
}
