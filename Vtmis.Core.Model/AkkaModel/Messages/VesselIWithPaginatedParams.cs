﻿namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class VesselIWithPaginatedParams
    {
        public VesselIWithPaginatedParams(int vesselId, int pageIndex = 1, int pageSize = 10)
        {
            VesselId = vesselId;
            PageIndex = pageIndex;
            PageSize = pageSize;
        }
        public int VesselId { get; private set; }
        public int PageIndex { get; private set; }
        public int PageSize { get; private set; }
    }
}
