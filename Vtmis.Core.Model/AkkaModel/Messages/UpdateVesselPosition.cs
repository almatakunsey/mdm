﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
   
    public class UpdateVesselPosition
    {
        public IEnumerable<VesselPosition> VesselPositions { get; }
       // public string DictVesselPosition { get; }
        public VesselPositionCategory VesselPositionCategory { get; }

        public UpdateVesselPosition(IEnumerable<VesselPosition> vesselPositions, VesselPositionCategory vesselPositionCategory)
        {
            this.VesselPositions = vesselPositions;
            VesselPositionCategory = vesselPositionCategory;
        }

     

    }

    public enum VesselPositionCategory
    {
        Current, Replay
    }
}
