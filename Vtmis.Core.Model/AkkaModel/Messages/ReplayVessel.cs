﻿namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class ReplayVessel
    {
        public ReplayVessel(int count)
        {
            Count = count;
        }

        public int Count { get; }
    }
}
