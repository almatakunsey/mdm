﻿using Vtmis.Core.VesselEntityFrameworkCore;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class CheckEvent
    {
        public CheckEvent(string vtsConnectionString)
        {
            this.VtsConnectionString = vtsConnectionString;
 
        }
        public VtsDatabaseContext VtsDatabaseContext { private set; get; }

        public string VtsConnectionString { private set; get; }
    }

    
}
