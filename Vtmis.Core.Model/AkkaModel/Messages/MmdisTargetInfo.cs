﻿using System;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class MmdisTargetInfo
    {
        public MmdisTargetInfo()
        {

        }

        public MmdisTargetInfo(string name, int? imo, int? mmsi, string dimension, string callSign,
            double latitude, double longitude, int? targetType, float? rot, float? sog, float? cog,
            int? trueHeading, DateTime receivedTime, DateTime? utc,DateTime? localRecvTime)
        {
            Name = name;
            IMO = imo;
            MMSI = mmsi;
            Dimension = dimension;
            CallSign = callSign;
            Latitude = latitude;
            Longitude = longitude;
            TargetType = CommonHelper.GetTargetTypeName(targetType);
            ROT = rot;
            SOG = sog;
            COG = cog;
            TrueHeading = trueHeading;
            Live = CommonHelper.GetLiveValue(receivedTime);
            OutOfRange = CommonHelper.GetOutOfRangeValue(receivedTime);
            FlagIcon = CommonHelper.GetFlagIcon(null);
            UTC = utc;
            LocalRecvTime = localRecvTime;
        }

        public string Name { get; private set; }
        public int? IMO { get; private set; }
        public int? MMSI { get; private set; }
        public string Dimension { get; private set; }
        public string CallSign { get; private set; }
        public double Latitude { get; private set; }
        public double Longitude { get; private set; }
        public string TargetType { get; private set; }
        public float? ROT { get; private set; }
        public float? SOG { get; private set; }
        public float? COG { get; private set; }
        public int? TrueHeading { get; private set; }
        public double Live { get; private set; }
        public double OutOfRange { get; private set; }
        public string Flag { get; set; }
        public string FlagIcon { get; private set; }
        public DateTime? UTC { get; private set; }
        public DateTime? LocalRecvTime { get; private set; }
    }
}
