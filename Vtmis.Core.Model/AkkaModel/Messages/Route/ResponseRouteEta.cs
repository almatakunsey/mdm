﻿using System.Collections.Generic;
using Vtmis.Core.Model.ViewModels.Route.Dto;

namespace Vtmis.Core.Model.AkkaModel.Messages.Route
{
    public class ResponseRouteEta
    {
        public IEnumerable<ResponseRouteEtaDto> FinalResults { get; }
        
        public ResponseRouteEta(IEnumerable<ResponseRouteEtaDto> finalResults)
        {
            this.FinalResults = finalResults;
        }
    }
}