﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Vtmis.Core.Model.ViewModels.Route.Dto;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;

namespace Vtmis.Core.Model.AkkaModel.Messages.Route
{
    public class QueryRouteEta
    {
        public QueryRouteEta(List<QueryShipPosStatic> result, WaypointDto waypointDto, int currentWaypointIndex, List<WaypointDto> waypoints, DateTime requestDate)
        {
            Result = result;
            Waypoint = waypointDto;
            CurrentWaypointIndex = currentWaypointIndex;
            Waypoints = waypoints;
            RequestDate = requestDate;
        }

        public WaypointDto Waypoint { get; }
        public DateTime RequestDate { get; }
        public int CurrentWaypointIndex { get;  }
        public List<QueryShipPosStatic> Result { get; }
        public List<WaypointDto> Waypoints { get; set; }
    }
}
