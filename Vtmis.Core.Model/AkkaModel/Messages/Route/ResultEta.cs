﻿using System;

namespace Vtmis.Core.Model.AkkaModel.Messages.Route
{
    public class ResultEta
    {
        public int MMSI { get; set; }
        public double SOG { get; set; }
        public double COG { get; set; }
        public DateTime LocalRecvTime { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime? ETA { get; set; }
    }
}
