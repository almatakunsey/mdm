﻿using System;
using Vtmis.Core.Model.ViewModels.Route.Dto;

namespace Vtmis.Core.Model.AkkaModel.Messages.Route
{
    public class RequestRouteEta
    {
        public RequestRouteEta(ResponseRouteDto route, DateTime requestDate, int pageIndex, int pageSize)
        {
            Route = route;
            RequestDate = requestDate;
            PageIndex = pageIndex;
            PageSize = pageSize;
        }
        public ResponseRouteDto Route { get;  }
        public DateTime RequestDate { get;  }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}