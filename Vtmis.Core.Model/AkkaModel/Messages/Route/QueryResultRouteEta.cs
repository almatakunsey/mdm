﻿using System.Collections.Generic;
using Vtmis.Core.Model.ViewModels.Route.Dto;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;

namespace Vtmis.Core.Model.AkkaModel.Messages.Route
{
    public class QueryResultRouteEta
    {
        public QueryResultRouteEta(List<ResultEta> results, WaypointDto waypoint)
        {
            Results = results;
            Waypoint = waypoint;
        }
        public List<ResultEta> Results { get; }
        public WaypointDto Waypoint { get;  }
    }
}
