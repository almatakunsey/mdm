﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Route
{
    public class ResponseVesselEtaMsg
    {       
        public ResponseVesselEtaMsg(ResponseVesselEta result)
        {
            Result = result;
        }
        public ResponseVesselEta Result { get; }
    }
}
