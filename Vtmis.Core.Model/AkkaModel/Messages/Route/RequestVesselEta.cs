﻿using System;
using Vtmis.Core.Model.ViewModels.Route.Dto;

namespace Vtmis.Core.Model.AkkaModel.Messages.Route
{
    public class RequestVesselEta
    {
        public RequestVesselEta(ResponseRouteDto route, int mmsi, DateTime requestDate)
        {
            Route = route;
            MMSI = mmsi;
            RequestDate = requestDate;
        }

        public ResponseRouteDto Route { get; }
        public int MMSI { get; }
        public DateTime RequestDate { get;  }
    }
}
