﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class TargetInfo
    {
        public TargetInfo(int id, string targetName, int? aisVersion,
            int? imo, int? mmsi, int? atonType, DateTime? eta, string vendor, string destination, int? shipType,
            string callSign, float? maxDraught, int? dimensionA, int? dimensionB, int? dimensionC, int? dimensionD,
            int? positionFixingDevice, int? virtualAtonStatus, int? offPosition21, int? health21, int? light21, int? racon21,
            int? offPosition68, int? health68, int? light68, int? racon68, float? analogueExt1, float? analogueExt2,
            float? analogueInt,
            PaginatedList<TargetPosition> targetPositions)
        {
            //CurrentPage = currentPage;
            //NumberOfRows = numberOfRows;
            Id = id;
            TargetName = targetName;
            //TargetType = targetType;
            AISVersion = aisVersion;
            IMO = imo;
            MMSI = mmsi;
            AtoNType = atonType;
            ETA = eta;
            Vendor = vendor;
            Destination = destination;
            ShipType = shipType;
            CallSign = callSign;
            MaxDraught = maxDraught;
            Dimension_A = dimensionA;
            Dimension_B = dimensionB;
            Dimension_C = dimensionC;
            Dimension_D = dimensionD;
            PositionFixingDevice = positionFixingDevice;
            OffPosition21 = offPosition21;
            Light21 = light21;
            RACON21 = racon21;
            Health21 = health21;
            Light68 = light68;
            RACON68 = racon68;
            Health68 = health68;
            OffPosition68 = offPosition68;
            AnalogueExt1 = analogueExt1;
            AnalogueExt2 = analogueExt2;
            AnalogueInt = analogueInt;
            TargetPositions = targetPositions;
            VirtualAtonStatus = virtualAtonStatus;
        }
        public int Id { get; private set; }
        public int? MMSI { get; private set; }
        public string TargetName { get; private set; }
        //public int? TargetType { get; private set; }
        public int? AISVersion { get; private set; }
        public int? IMO { get; private set; }
        
        public int? AtoNType { get; private set; }
        public DateTime? ETA { get; private set; }
        public string Vendor { get; private set; }
        public string Destination { get; private set; }
        //public int? CurrentPage { get; private set; }
        //public int? NumberOfRows { get; private set; }
        public int? ShipType { get; private set; }
        public string ShipType_ToString
        {
            get
            {
                return TargetHelper.GetShipTypeByShipTypeId(ShipType);
            }
        }
        public string CallSign { get; private set; }
        public float? MaxDraught { get; private set; }
        public string AtoNType_ToString
        {
            get
            {
                return TargetHelper.GetAtonTypeByAtonTypeId(AtoNType);
            }
        }
        public int? VirtualAtonStatus { get; private set; }
        public string VirtualAtoN_ToString
        {
            get
            {
                return TargetHelper.GetVirtualAtonStatus(VirtualAtonStatus);
            }
        }
        public float? AnalogueExt1 { get; set; }
        public float? AnalogueExt2 { get; set; }
        public float? AnalogueInt { get; set; }
        public int? Dimension_A { get; private set; }
        public int? Dimension_B { get; private set; }
        public int? Dimension_C { get; private set; }
        public int? Dimension_D { get; private set; }
        public string LengthXbeam
        {
            get
            {
                return TargetHelper.GetLengthXBeam(TargetPositions != null && TargetPositions.TotalItems > 0, Dimension_A,
                    Dimension_B, Dimension_C, Dimension_D, TargetPositions.Items.FirstOrDefault()?.MessageId);
            }
        }
        public int? PositionFixingDevice { get; private set; }
        public string PositionFixingDeviceToString
        {
            get
            {
                return TargetHelper.GetPositionFixingDevice(PositionFixingDevice);
            }
        }
        public int? OffPosition21 { get; set; }
        public int? Health21 { get; set; }
        public int? Light21 { get; set; }
        public int? RACON21 { get; set; }
        public int? Health68 { get; set; }
        public int? Light68 { get; set; }
        public int? RACON68 { get; set; }
        public string AtoNStatus21
        {
            get
            {
                return TargetHelper.GetAtonStatus(Health21, Light21, RACON21);
            }
        }
        public int? OffPosition68 { get; }
        public string AtoNStatus68
        {
            get
            {
                return TargetHelper.GetAtonStatus(Health68, Light68, RACON68);
            }
        }
        public string AnalogueInt_1_2
        {
            get
            {
                return TargetHelper.GetAnalogueInt_1_2(AnalogueInt, AnalogueExt1, AnalogueExt2);
            }
        }
        public string DigitalInput { get; }
        public string OffPosition21_ToString
        {
            get
            {
                return TargetHelper.GetOffPosition(OffPosition21);
            }
        }

        public string OffPosition68_ToString
        {
            get
            {
                return TargetHelper.GetOffPosition(OffPosition68);
            }
        }


        public string DigitalInput_ToString { get; }

        public PaginatedList<TargetPosition> TargetPositions { get; private set; }
    }
}
