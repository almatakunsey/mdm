﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Common.Helpers.RMS;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class ResponseCurrentVesselPosition
    {
        public ResponseCurrentVesselPosition(IEnumerable<VesselPosition> vesselPositions)
        {
            VesselPositions = vesselPositions;
        }

        public IEnumerable<VesselPosition> VesselPositions { get; internal set; }
    }


    public class RMSStats
    {
        public RMSStats(double? analogueExt1, double? analogueExt2, double? analogueInt, double? offPosition68,
            double? health68, double? light68, double? racon68, string atonStatus68, int? lantern, int? batteryStat)
        {
            AnalogueExt1 = analogueExt1.ToStringOrDefault(true);
            AnalogueExt2 = analogueExt2.ToStringOrDefault(true);
            AnalogueInt = analogueInt.ToStringOrDefault(true);
            OffPosition68 = offPosition68;
            Health68 = health68;
            Light68 = light68;
            RACON68 = racon68;
            AtoNStatus68 = atonStatus68;
            Lantern = lantern;
            BatteryStat = batteryStat;
        }
        public int? Lantern { get; set; }
        public int? BatteryStat { get; set; }
        public string AnalogueExt1 { get; set; }
        public string AnalogueExt2 { get; set; }
        public string AnalogueInt { get; set; }
        public double? OffPosition68 { get; }
        public double? Health68 { get; set; }
        public double? Light68 { get; set; }
        public double? RACON68 { get; set; }
        public string AtoNStatus68 { get; set; }
        public string AnalogueInt_1_2
        {
            get
            {
                double? anaInt = null;
                double? anaExt1 = null;
                double? anaExt2 = null;
                if (string.IsNullOrWhiteSpace(AnalogueInt) == false)
                {
                    anaInt = AnalogueInt.ToDoubleOrDefault();
                }
                if (string.IsNullOrWhiteSpace(AnalogueExt1) == false)
                {
                    anaExt1 = AnalogueExt1.ToDoubleOrDefault();
                }
                if (string.IsNullOrWhiteSpace(AnalogueExt2) == false)
                {
                    anaExt2 = AnalogueExt2.ToDoubleOrDefault();
                }
                return TargetHelper.GetAnalogueInt_1_2(anaInt, anaExt1, anaExt2);
            }
        }

        public string OffPosition68_ToString
        {
            get
            {
                return RMSHelper.GetOffPosition((int?)OffPosition68);
            }
        }
    }

    public class Msg21
    {
        public Msg21(double? offPosition21, double? health21, string atonStatus21, double? light21, double? racon21, bool isLighthouse,
            int? virtualAtonStatus, int? atonType)
        {
            VirtualAtonStatus = virtualAtonStatus;
            AtoNType = atonType;
            OffPosition21 = offPosition21;
            Health21 = health21;
            Light21 = light21;
            RACON21 = racon21;
            AtoNStatus21 = atonStatus21;
            IsLighthouse = isLighthouse;
        }
        public double? OffPosition21 { get; set; }
        public double? Health21 { get; set; }
        public double? Light21 { get; set; }
        public double? RACON21 { get; set; }
        public string AtoNStatus21 { get; set; }
        public bool IsLighthouse { get; set; }
        public int? VirtualAtonStatus { get; set; }
        public string VirtualAtoN_ToString
        {
            get
            {
                return TargetHelper.GetVirtualAtonStatus(VirtualAtonStatus);
            }
        }
        public string OffPosition21_ToString
        {
            get
            {
                return RMSHelper.GetOffPosition((int?)OffPosition21);
            }
        }
        public int? AtoNType { get; set; }
        public string AtoNType_ToString
        {
            get
            {
                return TargetHelper.GetAtonTypeByAtonTypeId(AtoNType);
            }
        }
    }

    public class VesselPosition
    {
        public VesselPosition(string vesselId, int? mmsi, double lat, double lgt, string vesselName, string callSign,
            DateTime? receivedTime, string destination, string vendor, int? imo, double? rot,
            double? sog, double? cog, Int16 trueHeading, Int16? shipType, int messageId, int? dimensionA,
            int? dimensionB, int? dimensionC, int? dimensionD, int? positionAccuracy, int? positionFixingDevice,
            bool isSart, int? raim, DateTime? utcReceivedTime, DateTime? eta, float? maxDraught,
            int? navigationalStatus,
            string ip, bool isMethydro, string digitalInput, EICSModel eics,
            string lloydsShipType, Msg21 msg21, RMSStats rmsStats)
        {
            Msg21 = msg21;
            RMSStats = rmsStats;
            VesselId = vesselId;
            MMSI = mmsi;
            CallSign = callSign;
            Imo = imo;
            VesselName = vesselName;
            Lat = lat;
            Lgt = lgt;
            Sog = sog;
            Rot = rot;
            Cog = cog;
            RAIM = raim;
            MessageId = messageId;
            ReceivedTime = receivedTime;
            UtcReceivedTime = utcReceivedTime;
            ETA = eta;
            IsSart = isSart;
            Destination = destination;
            Vendor = vendor;
            TrueHeading = trueHeading;
            ShipType = shipType;
            MaxDraught = maxDraught;
            PositionAccuracy = positionAccuracy;
            NavigationalStatus = navigationalStatus;
            Dimension_A = dimensionA;
            Dimension_B = dimensionB;
            Dimension_C = dimensionC;
            Dimension_D = dimensionD;
            PositionFixingDevice = positionFixingDevice;
            DigitalInput = digitalInput;
            IP = ip;
            IsMethydro = isMethydro;
            Eics = eics;
            LloydsShipType = lloydsShipType;
        }

        public bool IsLighthouse { get; set; }
        public string VesselId { get; set; }
        public RMSStats RMSStats { get; set; }
        public Msg21 Msg21 { get; set; }     
        public int? MMSI { get; set; }
        public string CallSign { get; set; }
        public int? Imo { get; set; }
        public string VesselName { get; set; }
        public double Lat { get; set; }
        public double Lgt { get; set; }
        public double? Sog { get; set; }
        public double? Rot { get; set; }
        public string RotToString
        {
            get
            {
                if (CommonHelper.GetTargetClassByMessageId(MessageId) == TargetClass.ClassA)
                {
                    if (Rot > 720 || Rot < -720)
                    {
                        return null;
                    }
                    if (Rot < 0)
                    {
                        var result = Math.Round((double)Rot, 2);
                        return $"Portside {result.ToString("0.0")}°/min";
                    }

                    if (Rot > 0)
                    {
                        var result = Math.Round((double)Rot, 2);
                        return $"Starboard {result.ToString("0.0")}°/min";
                    }
                    if (Rot == 0)
                    {
                        return $"{Rot}°/min";
                    }
                    return null;
                }
                else
                {
                    return null;
                }
            }
        }
        public double? Cog { get; set; }
        public int? RAIM { get; set; }
        public int MessageId { get; set; }
        public DateTime? ReceivedTime { get; }
        public DateTime? UtcReceivedTime
        {
            get;
        }
        public DateTime? ETA { get; set; }
        public string Destination { get; set; }


        public bool IsSart { get; set; }
        public bool IsMethydro { get; set; }

        public string Vendor { get; set; }
        public Int16 TrueHeading { get; set; }
        public Int16? ShipType { get; set; }
        public string ShipType_ToString
        {
            get
            {
                return TargetHelper.GetShipTypeByShipTypeId(ShipType);
            }

        }
        public float? MaxDraught { get; set; }
        public int? PositionAccuracy { get; set; }
        public string PositionAccuracyToString
        {
            get
            {
                return TargetHelper.GetPositionAccuracy(PositionAccuracy);
            }
        }
        public string NavigationalStatusToString
        {
            get
            {
                return TargetHelper.GetNavStatusDescriptionByNavStatusId(NavigationalStatus);
            }
        }
        public int? NavigationalStatus { get; set; }

        public int? Dimension_A { get; set; }
        public int? Dimension_B { get; set; }
        public int? Dimension_C { get; set; }
        public int? Dimension_D { get; set; }
        public string LengthXbeam
        {
            get
            {
                return TargetHelper.GetLengthXBeam(true, Dimension_A,
                    Dimension_B, Dimension_C, Dimension_D, MessageId);
            }

        }
        public int? PositionFixingDevice { get; set; }
        public string PositionFixingDeviceToString
        {
            get
            {
                return TargetHelper.GetPositionFixingDevice(PositionFixingDevice);
            }
        }


        public string DigitalInput { get; set; }
        //public string DigitalInput_ToString { get; }
        public string IP { get; set; }
        public string LastSeen
        {
            get
            {
                if (ReceivedTime == null)
                {
                    return null;
                }
                return CommonHelper.TimeAgo(ReceivedTime.Value);
            }
        }

        [JsonProperty(propertyName: "EICS")]
        public EICSModel Eics { get; set; }
        public string LloydsShipType { get; set; }
    }
}