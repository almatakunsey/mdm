﻿using GeoJSON.Net.Geometry;
using System.Collections.Generic;

namespace Vtmis.Core.Model.AkkaModel.Messages.Playback
{
    public class GeoJsonResponse
    {
        public GeoJsonResponse()
        {
            //geometry = new Geometry();
            properties = new Properties();
            TargetData = new List<TargetData>();
        }
        //public Geometry geometry { get; set; }
        //public int CountCoordinates { get; set; }
        //public int CountTimes { get; set; }
        public MultiPoint geometry { get; set; }
        public Properties properties { get; set; }
        public string type { get; set; }
        public bool Status { get; set; }
        public List<TargetData> TargetData { get; set; }
    }

    public class TargetData
    {
        public double? SOG { get; set; }
        public string NavStatus { get; set; }
        public string Destination { get; set; }
    }

}