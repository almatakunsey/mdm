﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Playback
{
    public class Properties
    {
        public long[] Time { get; set; }
        public double[] Orientations { get; set; }
    }
}