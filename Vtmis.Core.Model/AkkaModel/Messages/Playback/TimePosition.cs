﻿namespace Vtmis.Core.Model.AkkaModel.Messages.Playback
{
    //Message
    public class TimePosition
    {
        public long UnixTime { get; set; }
        public GeoJSON.Net.Geometry.Point Point { get; set; }
        public double TrueHeading { get; set; }
    }
}