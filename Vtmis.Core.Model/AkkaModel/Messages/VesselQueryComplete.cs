﻿using System.Collections.Generic;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class VesselQueryComplete
    {
        public VesselQueryComplete(string userId, System.Collections.Generic.List<VesselPosition> resultVesselPositions)
        {
            UserId = userId;
            ResultVesselPositions = resultVesselPositions;
        }
        public string UserId { get;  }
        public List<VesselPosition> ResultVesselPositions { get;  }
    }
}
