﻿using System.Collections.Generic;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class UpdateVesselPositionHistory
    {
        public UpdateVesselPositionHistory(List<VesselPosition> vesselPositions)
        {
            VesselPositions = vesselPositions;
        }

        public List<VesselPosition> VesselPositions { get; }
    }
}
