﻿namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class CommonTargetParams
    {
        public CommonTargetParams(int targetId, int mmsi, int imo, string callSign)
        {
            TargetId = targetId;
            MMSI = mmsi;
            IMO = imo;
            CallSign = callSign;
        }
        public int TargetId { get; private set; }
        public int MMSI { get; private set; }
        public int IMO { get; private set; }
        public string CallSign { get; private set; }
    }
}
