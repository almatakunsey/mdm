﻿namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class VesselRoutePosition
    {
        public VesselRoutePosition(double lat, double lgt)
        {
            Lat = lat;
            Lgt = lgt;
        }
        public double Lat { get; set; }
        public double Lgt { get; set; }
    }
}

