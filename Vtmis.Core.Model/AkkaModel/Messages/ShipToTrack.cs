﻿using System;
using System.Collections.Generic;
using Vtmis.Core.VesselEntityFrameworkCore.Models;

namespace Vtmis.Core.Model.AkkaModel.Messages
{
    public class ShipToTrack
    {
        public ShipToTrack(List<ShipStatic> mmsi,DateTime senderDemoDate)
        {
            Mmsi = mmsi;
            SenderDemoDate = senderDemoDate;
        }

        public List<ShipStatic> Mmsi { get; }
        public DateTime SenderDemoDate { get; }
    }
}
