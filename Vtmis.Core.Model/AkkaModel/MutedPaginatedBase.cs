﻿namespace Vtmis.Core.Model.AkkaModel
{
    public abstract class MutedPaginatedBase
    {
        public MutedPaginatedBase(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }
        public int PageIndex { get; }
        public int PageSize { get; }
    }
}
