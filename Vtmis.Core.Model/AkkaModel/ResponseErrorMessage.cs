﻿using System;

namespace Vtmis.Core.Model.AkkaModel
{
    public class ResponseErrorMessage
    {
        public ResponseErrorMessage(string errorMessage, Exception details)
        {
            ErrorMessage = errorMessage;
            Details = details;
        }

        public string ErrorMessage { get;  }
        public Exception Details { get;  }
    }
}
