﻿using System.Collections.Generic;

namespace Vtmis.Core.Model
{
    public class EICSModel
    {
        public EICSModel()
        {
            Equipments = new List<string>();
        }
        public List<string> Equipments { get; set; }
    }
}
