﻿using System;

namespace Vtmis.Core.Model.ViewModels.Target.Vessel
{
    public class ShipPositionViewModel
    {
        public int ShipPosID { get; set; }
        public int MMSI { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public Int16 TrueHeading { get; set; }
        public DateTime? LocalRecvTime { get; set; }
        public double? SOG { get; set; }
        public double? ROT { get; set; }
        public double? COG { get; set; }
    }
}
