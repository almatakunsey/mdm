﻿using System;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Model.ViewModels.Target.Vessel
{
    public class VesselPositionViewModel
    {
        public int VesselId { get; set; }
        public int? MMSI { get; set; }
        public double? Lat { get; set; }
        public double? Lgt { get; set; }
        public string VesselName { get; set; }
        public string CallSign { get; set; }
        public double? Sog { get; set; }
        public double? Rot { get; set; }
        public double? Cog { get; set; }
        public DateTime? ReceivedTime { get; set; }
        public DateTime? LocalReceivedTime { get; set; }
        public string Destination { get; set; }
        public int? Imo { get; set; }  
        public int? ShipType { get; set; }
        public string ShipType_ToString
        {
            get
            {
                return TargetHelper.GetShipTypeByShipTypeId(ShipType);
            }
        }
        public DateTime? ETA { get; set; }
        public string NavStatus
        {
            get
            {
                if (NavStatusId == null)
                {
                    return null;
                }
                return TargetHelper.GetNavStatusDescriptionByNavStatusId(NavStatusId);
            }
        }
        public int? NavStatusId { get; set; }
        public string LastSeen
        {
            get
            {
                if (LocalReceivedTime == null)
                {
                    return null;
                }
                return CommonHelper.TimeAgo(LocalReceivedTime.Value);
            }
        }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
