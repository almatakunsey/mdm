﻿namespace Vtmis.Core.Model.ViewModels.Target.Vessel
{
    public class ShipStaticViewModel
    {
        public int ShipStatID { get; set; }
        public string Name { get; set; }
        public int MMSI { get; set; }
        public string CallSign { get; set; }
        public string Vendor { get; set; }
        public string Destination { get; set; }
        public int? IMO { get; set; }

        public virtual ShipPositionViewModel ShipPosition { get; set; }
    }
}
