﻿namespace Vtmis.Core.Model.ViewModels.Target
{
    public class TargetInfoViewModel
    {
        public int Id { get; set; }
        public string TargetName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }      
        public int MMSI { get; set; }
        public string CallSign { get; set; }
        public int? IMO { get; set; }
    }
}
