﻿using Vtmis.Core.Common.Enums;

namespace Vtmis.Core.Model.ViewModels.Target
{
    public class AdvancedSearchViewModel
    {
        public string TargetName { get; set; }
        public string TargetMmsi { get; set; }
        public int TargetImo { get; set; }
        public string TargetCallSign { get; set; }
        public TargetTimeSpan TimeSpan { get; set; }
        public int PageSize { get; set; } = 50;
        public int PageIndex { get; set; } = 1;
        //public string Type { get; set; }
        //public string Status { get; set; }
        //public double Latitude { get; set; }
        //public double Longitude { get; set; }
        //public double? SOG { get; set; }
        //public double? COG { get; set; }
        //public string Destination { get; set; }
        //public string LastSeen { get; set; }
        //public DateTime ETA { get; set; }
    }
}
