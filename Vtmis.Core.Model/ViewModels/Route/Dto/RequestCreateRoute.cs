﻿using System.Collections.Generic;

namespace Vtmis.Core.Model.ViewModels.Route.Dto
{
    public class RequestCreateRoute
    {
        public RequestCreateRoute()
        {
            Waypoints = new List<RequestWaypointDto>();
        }
        public string Name { get; set; }
        public bool IsEnable { get; set; }
        public string Color { get; set; }
        public List<RequestWaypointDto> Waypoints { get; set; }
    }
}
