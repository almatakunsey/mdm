﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Vtmis.Core.Model.ViewModels.Route.Dto
{
    public class RequestUpdateRouteDto : EntityDto
    {
        public RequestUpdateRouteDto()
        {
            Waypoints = new List<RequestUpdateWaypointDto>();
        }
        public string Name { get; set; }
        public bool IsEnable { get; set; }
        public string Color { get; set; }
        public List<RequestUpdateWaypointDto> Waypoints { get; set; }
    }
}
