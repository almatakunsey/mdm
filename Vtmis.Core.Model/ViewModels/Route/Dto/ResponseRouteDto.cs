﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Vtmis.Core.Model.ViewModels.Route.Dto
{
    public class ResponseRouteDto : EntityDto
    {
        public ResponseRouteDto()
        {
            Waypoints = new List<WaypointDto>();
        }
        public string Name { get; set; }
        public bool IsEnable { get; set; }
        public string Color { get; set; }
        public List<WaypointDto> Waypoints { get; set; }
    }
}
