﻿using System;

namespace Vtmis.Core.Model.ViewModels.Route.Dto
{
    public class ResponseWaypointRouteEtaDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? ETA { get; set; }
        public double Distance { get; set; }
        public string Aging { get; set; }
        public bool IsArrived { get; set; }
        public bool IsShown { get; set; }
        public int Order { get; set; }
    }
}
