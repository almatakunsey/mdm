﻿using System;
using System.Collections.Generic;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Model.ViewModels.Route.Dto
{
    public class ResponseRouteEtaDto
    {
        public ResponseRouteEtaDto()
        {
            Waypoints = new List<ResponseWaypointRouteEtaDto>();
        }
        public string Name { get; set; }
        public int MMSI { get; set; }
        public int? ShipTypeId { get; set; }
        public string ShipType
        {
            get
            {
                return TargetHelper.GetShipTypeByShipTypeId(ShipTypeId);
            }
        }
        public double? SOG { get; set; }
        public double? COG { get; set; }
        public double TrueHeading { get; set; }
        public DateTime LocalRecvTime { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public List<ResponseWaypointRouteEtaDto> Waypoints { get; set; }
    }
}
