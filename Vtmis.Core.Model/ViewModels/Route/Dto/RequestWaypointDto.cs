﻿namespace Vtmis.Core.Model.ViewModels.Route.Dto
{
    public class RequestWaypointDto
    {
        //public int RouteId { get; set; }
        public bool IsShown { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double XteLeft { get; set; }
        public double XteRight { get; set; }
        public double MinSpeed { get; set; }
        public double MaxSpeed { get; set; }
        public double AverageSpeed { get; set; }
        public int Order { get; set; }
    }
}