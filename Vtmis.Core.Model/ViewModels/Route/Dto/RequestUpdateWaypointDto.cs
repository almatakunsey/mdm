﻿namespace Vtmis.Core.Model.ViewModels.Route.Dto
{
    public class RequestUpdateWaypointDto : RequestWaypointDto
    {
        public int Id { get; set; }
    }
}
