﻿namespace Vtmis.Core.Model.ViewModels.Alarms
{
    public class RequestCreateAlarmNotification
    {
        public long AlarmId { get; set; }
        public long EventId { get; }
        public long EventDetailsId { get; }
        public bool IsRead { get; set; }
        public long UserId { get; set; }
        public int? TenantId { get; set; }
    }
}
