﻿namespace Vtmis.Core.Model.ViewModels.Account
{
    public class AbpTokenRequestResult
    {
        public string AccessToken { get; set; }
        public int UserId { get; set; }
    }
}
