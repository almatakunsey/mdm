﻿namespace Vtmis.Core.Model.ViewModels.Account
{
    public class AbpTokenRequestResponse
    {
        public AbpTokenRequestResult Result { get; set; }
        public bool Success { get; set; }
    }
}
