﻿using System;

namespace Vtmis.Core.Model.ViewModels.VTS
{
    public class DraughtViewModel
    {
        public int MMSI { get; set; }
        public DateTime? RecvTime { get; set; }
        public Single? Empty { get; set; }
        public Single? Full { get; set; }
        public Single? Actual { get; set; }
        public bool? Supply { get; set; }
        public bool? Half { get; set; }
        public bool? Cover { get; set; }
        public Single? Battery { get; set; }
        public Int16? Sonar { get; set; }
        public Int16? Hoppers { get; set; }
        public Int16? HopperFlag { get; set; }
        public double? Lng { get; set; }
        public double? Lat { get; set; }
    }
}
