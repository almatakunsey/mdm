﻿using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Model.ViewModels.VTS
{
    public class DraughtDDMSTab
    {
        public DraughtDDMSTab(float? empty, float? full, float? actual, int? battery,
                bool? cover, bool? half, short? sonarVal, bool isMiddleValueExist, short? hoppers, short? hopperFlag, bool? supply)
        {
            var (maxAirDraught, minAirDraught, actualAirDraught, voltage, casingCover, halfFull, shipLoad, sonar, numHoppers, hopper, supplyResult) = TargetHelper.GetDDMS(empty, full, actual,
              battery, cover, half, sonarVal, isMiddleValueExist, hoppers, hopperFlag, supply);
            MaxAirDraught = maxAirDraught;
            MinAirDraught = minAirDraught;
            ActualAirDraught = maxAirDraught;
            Voltage = voltage;
            CasingCover = casingCover;
            HalfFull = halfFull;
            ShipLoad = shipLoad;
            NumHoppers = numHoppers;
            Hopper = hopper;
            Sonar = sonar;
            Supply = supplyResult;
        }

        public string MaxAirDraught { get; }
        public string MinAirDraught { get; }
        public string ActualAirDraught { get; }
        public string Voltage { get; }
        public string CasingCover { get; }
        public string HalfFull { get; set; }
        public string ShipLoad { get; set; }
        public string Sonar { get; set; }
        public string Supply { get; set; }

        public int NumHoppers { get; set; }
        public string Hopper { get; set; }
    }
}
