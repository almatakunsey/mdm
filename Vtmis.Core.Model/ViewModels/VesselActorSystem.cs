﻿using Akka.Actor;
using EasyNetQ;

namespace Vtmis.Core.Model.ViewModels
{
    public static class VesselActorSystem
    {
        public static ActorSystem ActorSystem;
        public static IActorRef Api = ActorRefs.Nobody;
        public static IActorRef Search = ActorRefs.Nobody;
        public static IActorRef MapActor = ActorRefs.Nobody;
        public static IActorRef EventActor = ActorRefs.Nobody;
        public static string ApiUrl { get; set; }
        public static string QueueId { get; set; }
        public static IBus _bus { get; set; }
        
    }
}
