﻿namespace Vtmis.Core.Model.ViewModels.Lloyds
{
    public class ResponseLloyds
    {
        public int? IMO { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Flag { get; set; }
        public string VesselType { get; set; }
        public int? Built { get; set; }
        public int? DWT { get; set; }
        public string HullType { get; set; }
        public string CallSign { get; set; }
        public int? GrossTonnes { get; set; }
        public string YardNumber { get; set; }
        public string BuiltBy { get; set; }
        public string BuiltAt { get; set; }
        public float? LengthOverall { get; set; }
        public float? LngthBtwnPpndicular { get; set; }
        public float? BreadthExtreme { get; set; }
        public float? BreadthMoulded { get; set; }
        public float? Depth { get; set; }
        public float? Draught { get; set; }
        public int? Freeboard { get; set; }
        public string EngineBuiltBy { get; set; }
        public string DesignedBy { get; set; }
        public string NarrativeText { get; set; }
        public string PIClub { get; set; }
        public string Class1 { get; set; }
        public string Class2 { get; set; }
        public string Class3 { get; set; }
        public string CompanyContact { get; set; }
        public string PortOfRegistry { get; set; }
        public int? NetWeight { get; set; }
    }
}
