﻿using Vtmis.Core.VesselEntityFrameworkCore.Models;

namespace Vtmis.Core.Model
{
    public class ShipPositionStatic
    {
        public ShipPosition sp { get; set; }
        public ShipStatic ss { get; set; }
    }
}
