﻿namespace Vtmis.Core.Model
{
    public abstract class PageRequest
    {
        protected PageRequest(int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
        }

        public int PageIndex { get;  }
        public int PageSize { get;  }
    }
}
