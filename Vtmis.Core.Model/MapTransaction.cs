﻿using System.Collections.Generic;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.Model.DTO.Target;

namespace Vtmis.Core.Model
{
    public class MapTransactionSpecification
    {
        public List<Spec> SpecificationTrans { get; set; }
        public List<VesselPosition> HistoricalVesselPosition { get; set; }
        //public Dictionary<int, LinkedList<VesselPosition>> VesselsRoutePosition { get; set; }
        public List<VesselRoute> VesselRoutes { get; set; }
        public MapTransactionSpecification()
        {
            SpecificationTrans = new List<Spec>();
            HistoricalVesselPosition = new List<VesselPosition>();
            VesselRoutes = new List<VesselRoute>();
        }


        public Spec AddSpecificationTransaction(string connectionId, TranSpec tranSpec)
        {
            var specTransIndex = SpecificationTrans.FindIndex(c => c.ConnectionId == connectionId && c.TranSpec == tranSpec);

            if (specTransIndex != -1)
            {

                return SpecificationTrans[specTransIndex];
            }
            else
            {
                var spec = new Spec { ConnectionId = connectionId, TranSpec = tranSpec };
                SpecificationTrans.Add(spec);

                return spec;
            }
        }
    }




    public enum TranSpec
    {
        Replay, Filter
    }

    public class Spec
    {
        public string ConnectionId { get; set; }
        public TranSpec TranSpec { get; set; }
        public bool IsReplay { get; set; }
        public string FilterId { get; internal set; }

        public void SetReplay(bool isReplay)
        {
            IsReplay = isReplay;
        }

        public void SetFilter(string filterId)
        {
            FilterId = filterId;
        }
    }


}
