#docker rm $(docker ps -a -q)
$e = convertfrom-stringdata (get-content .env -raw)
$e.Loc_Environment
$e.Loc_Mdm_Db_ConnectionString
$e.Loc_Jwt_Security_Key
$e.Loc_Jwt_Issuer
$e.Loc_Jwt_Audience
$e.Loc_CQRS_List

$Jwt_Audience=$e.Loc_Jwt_Audience
$ASPNETCORE_ENVIRONMENT=$e.Loc_Environment
$Mdm_Db_ConnectionString=$e.Loc_Mdm_Db_ConnectionString
$Jwt_Security_Key=$e.Loc_Jwt_Security_Key
$Jwt_Issuer=$e.Loc_Jwt_Issuer
$CORS_List=$e.Loc_CQRS_List
#docker stack deploy -c docker-compose-swarmstack-local.yml mdmdeploy

#docker service create --name=myseed --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=4053,target=4053 --constraint 'node.hostname==DESKTOP-KF3CU4F' --env SEED=192.168.43.15 --env PORT=0 --env HOSTNAME=0.0.0.0 192.168.0.103:5000/vtmis.cluster.seed.swarm

#docker service create --name=webhost --replicas 1 --endpoint-mode dnsrr --publish mode=host,published=50837,target=50837 192.168.0.103:5000/vtmis.webadmin.web.host.local --env ASPNETCORE_ENVIRONMENT=$e.Loc_Environment --env Mdm_Db_ConnectionString=$e.Loc_Mdm_Db_ConnectionString --env Jwt_Security_Key=$e.Loc_Jwt_Security_Key --env Jwt_Issuer=$e.Loc_Jwt_Issuer --env Jwt_Audience=$e.Loc_Jwt_Audience --env Jwt_IsEnabled=true --env CORS_List=$e.Loc_CQRS_List

docker run -p 50837:50837 --name "webhost" --env ASPNETCORE_ENVIRONMENT=$ASPNETCORE_ENVIRONMENT --env Mdm_Db_ConnectionString=$Mdm_Db_ConnectionString --env Jwt_Security_Key=$Jwt_Security_Key --env Jwt_Issuer=$Jwt_Issuer --env Jwt_Audience=$Jwt_Audience --env Jwt_IsEnabled=true --env CORS_List=$CQRS_List 192.168.0.103:5000/vtmis.webadmin.web.host.local