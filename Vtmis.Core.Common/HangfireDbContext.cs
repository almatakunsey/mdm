﻿using Microsoft.EntityFrameworkCore;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Common
{
    public class HangfireDbContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder
                    //.UseLoggerFactory(MyLoggerFactory)
                    .UseNpgsql(AppHelper.GetHangireDbConnectionString());
            }
        }
    }
}
