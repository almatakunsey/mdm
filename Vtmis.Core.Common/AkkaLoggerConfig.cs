﻿using Serilog;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Common
{
    public class AkkaLoggerConfig
    {
        public void SetupLog(string logFileName)
        {
            var path = "/akka/logs/";
            if (AppHelper.GetEnvironmentName().IsEqual(HostingEnvironment.LOCAL))
            {
                path = "C:/Dev/Logs/";
            }
            var logger = new LoggerConfiguration()
                               .MinimumLevel.Information()

                               .WriteTo.Async(a => a.File($"{path}Akka-{logFileName}-Log--Info--.txt", rollingInterval: RollingInterval.Day, retainedFileCountLimit: null,
                               restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information,
                               outputTemplate:
                               "[{Timestamp}][{LogSource}][{Level}] {Message}{NewLine}{Exception}",
                               fileSizeLimitBytes: null, shared: true), blockWhenFull: true)

                               .WriteTo.Async(e => e.File($"{path}Akka-{logFileName}-Log--Error--.txt", rollingInterval: RollingInterval.Day, retainedFileCountLimit: null,
                                restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Error,
                               outputTemplate:
                               "[{Timestamp}][{LogSource}][{Level}] {Message}{NewLine}{Exception}",
                               fileSizeLimitBytes: null, shared: true), blockWhenFull: true)

                               .CreateLogger();

            Serilog.Log.Logger = logger;
        }
    }
}
