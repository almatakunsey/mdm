﻿namespace Vtmis.Core.Common.Enums
{
    public enum ImageType
    {
        Target,
        User,
        TenantPersonalization
    }
}
