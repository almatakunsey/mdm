﻿namespace Vtmis.Core.Common.Enums
{
    public enum AtonMsgFormat
    {
        Iala,
        MsiaLightBeacon,
        MsiaLantern
    }
}
