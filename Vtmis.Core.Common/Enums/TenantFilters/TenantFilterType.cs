﻿namespace Vtmis.Core.Common.Enums.TenantFilters
{
    public enum TenantFilterType
    {
        Host,
        TenantAdmin,
        None
    }
}
