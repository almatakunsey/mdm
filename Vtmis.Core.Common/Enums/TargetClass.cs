﻿namespace Vtmis.Core.Common.Enums
{
    public enum TargetClass
    {
        ClassA,
        ClassB,
        Aton,
        VirtualAton,
        BaseStation,
        SAR,
        SART,
        Methydro,
        Other,
        MonitoringData
    }
}
