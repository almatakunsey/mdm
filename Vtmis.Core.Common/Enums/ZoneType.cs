﻿namespace Vtmis.Core.Common.Enums
{
    public enum ZoneType
    {
        Circle, Polygon, Line_L, Line_R
    }
}
