﻿namespace Vtmis.Core.Common.Enums
{
    public enum EnvironmentName
    {
        Local,
        Development,
        HafizDev,
        Production,
        Staging
    }
}
