﻿namespace Vtmis.Core.Common.Enums
{
    public enum RmsType
    {
        Beat,
        Door,
        Ambient,
        Lantern,
        LanternBattery,
        AlarmActive,
        FlshrOffByLedPwrThres,
        FlshrOffByLowVin,
        FlshrOffByPhotocell,
        FlshrOffByTemp,
        FlshrOffByForceOff,
        IsNight,
        ErrLedShort,
        ErrLedOpen,
        ErrLedVoltageLow,
        ErrVinLow,
        ErrLedPowerThres,
        FlshrAdjToLedMaxAvgPwr,
        GsensorInterruptOccur,
        SolarChargingOn
    }
}
