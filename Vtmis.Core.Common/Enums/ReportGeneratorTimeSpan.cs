﻿namespace Vtmis.Core.Common.Enums
{
    public enum ReportGeneratorTimeSpan
    {
        User_Defined,
        Last_24_Hours,
        Last_48_Hours,
        Last_10_Days,
        Last_30_Days,
        Last_90_Days,
        Last_365_Days,
        Today,
        Yesterday,
        This_Week,
        Last_Week,
        This_Month,
        Last_Month,
        This_Quarter,
        Last_Quater,
        This_Year,
        Last_Year,
        All
    }
}
