﻿namespace Vtmis.Core.Common.Enums
{
    public enum Unit
    {
        Meters,
        Kilometers,
        NauticalMiles
    }
}
