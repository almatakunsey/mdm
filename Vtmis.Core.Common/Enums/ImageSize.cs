﻿namespace Vtmis.Core.Common.Enums
{
    public enum ImageSize
    {
        Standard,
        Small,
        Medium
    }
}
