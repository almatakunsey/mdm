﻿namespace Vtmis.Core.Common.Enums
{
    public enum ImageVariationType
    {
        Original,
        Watermark,
        Blur,
        None
    }
}
