﻿namespace Vtmis.Core.Common.Enums
{
    public enum TargetTimeSpan
    {
        All,
        Hours_24,
        Hours_48,
        Days_10,
        Days_30,
        Days_90,
        Days_365
    }
}
