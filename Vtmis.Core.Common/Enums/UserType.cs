﻿namespace Vtmis.Core.Common.Enums
{
    public enum UserType
    {
        HostAdmin,
        HostUser,
        TenantAdmin,
        TenantUser,
        None
    }
}
