﻿using Microsoft.Extensions.Configuration;
using System;
using Vtmis.Core.Common.Constants;

namespace Vtmis.Core.Common.Helpers
{
    public class MEHObj
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string BaseURI { get; set; }
        public string TokenAuthURI { get; set; }
        public string IncidentReportURI { get; set; }
    }
    public class AppHelper
    {
        private readonly IConfiguration _configuration;

        public AppHelper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public static MEHObj GetMEHURI()
        {
            return new MEHObj
            {
                Username = Environment.GetEnvironmentVariable("MEHUsername"),
                Password = Environment.GetEnvironmentVariable("MEHPassword"),
                BaseURI = Environment.GetEnvironmentVariable("MEHBaseURI"),
                TokenAuthURI = Environment.GetEnvironmentVariable("MEHTokenAuthURI"),
                IncidentReportURI = Environment.GetEnvironmentVariable("MEHIncidentReportURI")
            };
        }

        public static string GetVesselusingMongoDB()
        {
            return Environment.GetEnvironmentVariable("MongoDB");
        }

        public static string GetPostgresConnectionString()
        {
            return Environment.GetEnvironmentVariable("Postgres_ConnectionString");
        }

        public static string GetENCTokenUrl()
        {
            return Environment.GetEnvironmentVariable("ENCTokenUrl");
        }

        public static string GetENCUsername()
        {
            return Environment.GetEnvironmentVariable("ENCUsername");
        }

        public static string GetENCPassword()
        {
            return Environment.GetEnvironmentVariable("ENCPassword");
        }

        public static string ConvertRequestUrl(string originUri)
        {
            if (originUri.StartsWith("10.", StringComparison.CurrentCultureIgnoreCase) || originUri.StartsWith("localhost", StringComparison.CurrentCultureIgnoreCase))
            {
                return GetImageProcessingBaseUri();
            }
            else
            {
                var port = GetImageProcessingBaseUri().Substring(GetImageProcessingBaseUri().Length - 4);
                return $"http://60.54.119.46:{port}";
            }
        }

        public static string GetHangireDbConnectionString()
        {
            return Environment.GetEnvironmentVariable("Hangfire_ConnectionString");
        }

        public static string GetMongoLocalConnectionString()
        {
            return Environment.GetEnvironmentVariable("MongoLocalConnectionString");
        }
        public static string GetMongoPassword()
        {
            return Environment.GetEnvironmentVariable("MongoPassword");
        }

        public static string GetMongoUsername()
        {
            return Environment.GetEnvironmentVariable("MongoUsername");
        }

        public static string GetMongoDbAuth()
        {
            return Environment.GetEnvironmentVariable("MongoDbAuth");
        }

        public static string GetReplicaSet()
        {
            return Environment.GetEnvironmentVariable("MongoReplicaSet");
        }

        public static string GetMongoServers()
        {
            return Environment.GetEnvironmentVariable("MongoServers");
        }

        public static string GetImageProcessingApiBaseUri()
        {
            return Environment.GetEnvironmentVariable("ImageProcessingApiBaseUri");
        }
        public static string GetImageProcessingApiUploadUri()
        {
            return Environment.GetEnvironmentVariable("ImageProcessingApiUploadUri");
        }
        public static string GetImageProcessingBaseUri()
        {
            return Environment.GetEnvironmentVariable("ImageProcessingBaseUri");
        }



        public static string GetSSLCertificationPath()
        {
            return "C:/cert/www_enav_my.pfx";
        }
        public static string GetSSLCertificationPassword()
        {
            return "VisitDK2010";
        }
        public static string GetProtocol()
        {
            return string.Equals(GetEnvironmentName(), HostingEnvironment.STAGING, StringComparison.CurrentCultureIgnoreCase) ? ProtocolConst.HTTPS : ProtocolConst.HTTP;
        }
        public static string GetSeedPort()
        {
            return Environment.GetEnvironmentVariable("SEED_PORT");
        }
        public static DateTime GetAisDateBasedOnCurrentEnv()
        {
            var env = GetEnvironmentName();
            return string.Equals(env, HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase) ? GetDevAisDate().ToDateTime() :
                DateTime.Now;
        }
        public static string GetDevAisDate()
        {
            return Environment.GetEnvironmentVariable("DB_DATE");
        }
        public static string GetAisDbServerIp()
        {
            return Environment.GetEnvironmentVariable("DB_SERVER");
        }
        public static string GetAisDbUserId()
        {
            return Environment.GetEnvironmentVariable("USERNAME");
        }
        public static string GetAisDbPassword()
        {
            return Environment.GetEnvironmentVariable("PASSWORD");
        }
        public static string GetLocalMachineIp()
        {
            return Environment.GetEnvironmentVariable("Loc_Local_IP_Address");
        }

        public static string GetAisConnectionString()
        {
            throw new NotImplementedException();
        }

        public static string GetUseQueue()
        {
            return Environment.GetEnvironmentVariable("USE_QUEUE");
        }
        public static string GetEnvironmentName()
        {
            return Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        }
        public static string GetVtsDbConnectionString()
        {
            return Environment.GetEnvironmentVariable("VTS_DB_CONNECTIONSTRING");
        }

        public static string GetVtsProdDbConnectionString()
        {
            return Environment.GetEnvironmentVariable("VTS_DB_PROD_CONNECTIONSTRING");
        }

        public static string GetMdmDbConnectionString()
        {
            return Environment.GetEnvironmentVariable("Mdm_Db_ConnectionString");
        }

        public static string IsJwtEnabled()
        {
            return Environment.GetEnvironmentVariable("Jwt_IsEnabled");
        }

        public static string GetCorsOriginList()
        {
            return Environment.GetEnvironmentVariable("CORS_List");
        }

        public static string GetJwtSecurityKey()
        {
            return Environment.GetEnvironmentVariable("Jwt_Security_Key");
        }

        public static string GetJwtIssuer()
        {
            return Environment.GetEnvironmentVariable("Jwt_Issuer");
        }

        public static string GetJwtAudience()
        {
            return Environment.GetEnvironmentVariable("Jwt_Audience");
        }

        public static string GetJwtIssuerUri()
        {
            return Environment.GetEnvironmentVariable("Jwt_Issuer_Uri");
        }

        public static string GetAisConnectionString(DateTime? date)
        {
            date = date == null ? DateTime.Now : date;
            var serverName = GetAisDbServerIp();
            var dbName = CommonHelper.GetAisDbNameFromDate(date.Value);
            var username = GetAisDbUserId();
            var password = GetAisDbPassword();
            return $"Server={serverName},1433;Database={dbName};User Id={username};Password={password};MultipleActiveResultSets=True;ConnectRetryCount=5";
        }

        public static string GetAisConnectionString(DateTime? date, string serverName, string username, string password)
        {
            date = date == null ? DateTime.Now : date;
            //var serverName = GetAisDbServerIp();
            var dbName = CommonHelper.GetAisDbNameFromDate(date.Value);
            //var username = GetAisDbUserId();
            //var password = GetAisDbPassword();
            return $"Server={serverName},1433;Database={dbName};User Id={username};Password={password};MultipleActiveResultSets=True;ConnectRetryCount=5";
        }

        public static string GetRedisConnectionString()
        {
            return Environment.GetEnvironmentVariable("RedisConnectionString");
        }
    }
}
