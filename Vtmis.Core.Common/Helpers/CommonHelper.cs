﻿using GeoCoordinatePortable;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using Vtmis.Core.Common.Enums;

namespace Vtmis.Core.Common.Helpers
{
    //TODO: Refactor this
    public class MandoProViewModel
    {
        public string Ambient { get; set; }
        public string Beat { get; set; }
        public string AlarmActive { get; set; }
        public string FlshrOffByLedPwrThres { get; set; }
        public string FlshrOffByLowVin { get; set; }
        public string FlshrOffByPhotocell { get; set; }
        public string FlshrOffByTemp { get; set; }
        public string FlshrOffByForceOff { get; set; }
        public string IsNight { get; set; }
        public string ErrLedShort { get; set; }
        public string ErrLedOpen { get; set; }
        public string ErrLedVoltageLow { get; set; }
        public string ErrVinLow { get; set; }
        public string ErrLedPowerThres { get; set; }
        public string FlshrAdjToLedMaxAvgPwr { get; set; }
        public string GsensorInterruptOccur { get; set; }
        public string SolarChargingOn { get; set; }
    }

    public static class CommonHelper
    {
        public static string TrimStringOrDefault(this string val)
        {
            if (string.IsNullOrWhiteSpace(val))
            {
                return null;
            }
            return val.Trim();
        }
        public static string ConvertImageToBase64(Stream imageStream)
        {
            using (var memoryStream = new MemoryStream())
            {
                imageStream.CopyTo(memoryStream);
                byte[] imageBytes = memoryStream.ToArray();
                return Convert.ToBase64String(imageBytes);
            }           
        }

        public static int? GetSpecificRms(int? digitalInput, RmsType rmsType, short? dac, short? fi)
        {
            if (digitalInput == null)
            {
                return null;
            }
            var binary = digitalInput.Value.ToBinary();
            if (binary.Substring(0, 1) == "-")
            {
                binary = binary.Remove(0, 1);
            }
            var format = TargetHelper.GetAtonMessageFormat(dac, fi);
            if (format == AtonMsgFormat.MsiaLantern) // Mando Pro
            {
                if (binary.Length < 16)
                {
                    var count = 16 - binary.Length;
                    for (int x = 0; x < count; x++)
                    {
                        binary = $"0{binary}";
                    }
                }

                if (rmsType == RmsType.Beat)
                {
                    return binary.Substring(0, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.AlarmActive)
                {
                    return binary.Substring(1, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.FlshrOffByLedPwrThres)
                {
                    return binary.Substring(2, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.FlshrOffByLowVin)
                {
                    return binary.Substring(3, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.FlshrOffByPhotocell)
                {
                    return binary.Substring(4, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.FlshrOffByTemp)
                {
                    return binary.Substring(5, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.FlshrOffByForceOff)
                {
                    return binary.Substring(6, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.IsNight)
                {
                    return binary.Substring(7, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.ErrLedShort)
                {
                    return binary.Substring(8, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.ErrLedOpen)
                {
                    return binary.Substring(9, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.ErrLedVoltageLow)
                {
                    return binary.Substring(10, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.ErrVinLow)
                {
                    return binary.Substring(11, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.ErrLedPowerThres)
                {
                    return binary.Substring(12, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.FlshrAdjToLedMaxAvgPwr)
                {
                    return binary.Substring(13, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.GsensorInterruptOccur)
                {
                    return binary.Substring(14, 1).FromBinaryToInt();
                }
                if (rmsType == RmsType.SolarChargingOn)
                {
                    return binary.Substring(15, 1).FromBinaryToInt();
                }
                return null;
            }
            else
            {
                if (rmsType == RmsType.Beat)
                {
                    return binary.Substring(0, 1).FromBinaryToInt();
                }

                if (rmsType == RmsType.Door)
                {
                    return binary.Substring(7, 1).FromBinaryToInt();
                }

                if (rmsType == RmsType.Ambient)
                {
                    return binary.Substring(5, 2).FromBinaryToInt();
                }

                if (rmsType == RmsType.LanternBattery)
                {
                    return binary.Substring(1, 2).FromBinaryToInt();
                }

                if (rmsType == RmsType.Lantern)
                {
                    return binary.Substring(3, 2).FromBinaryToInt();
                }
                return null;
            }
        }
        /// <summary>
        /// Get RMS Description by Digital Input (Mando Pro)
        /// </summary>
        /// <param name="digitalInput"></param>
        /// <param name="ambient"></param>
        /// <returns></returns>
        public static MandoProViewModel GetRms(int? digitalInput, int? ambient)
        {
            if (digitalInput.HasValue == false)
            {
                return new MandoProViewModel();
            }
            var result = new MandoProViewModel();
            if (ambient == 0)
            {
                result.Ambient = "No LDR";
            }
            else if (ambient == 1)
            {
                result.Ambient = "Dark";
            }
            else if (ambient == 2)
            {
                result.Ambient = "Dim";
            }
            else if (ambient == 3)
            {
                result.Ambient = "Bright";
            }

            var binary = digitalInput.Value.ToBinary();
            if (binary.Substring(0, 1) == "-")
            {
                binary = binary.Remove(0, 1);
            }
            if (binary.Length < 16)
            {
                var count = 16 - binary.Length;
                for (int x = 0; x < count; x++)
                {
                    binary = $"0{binary}";
                }
            }

            var a = binary.Substring(0, 1);
            result.Beat = a == "1" ? "Tock" : "Tick";
            binary = binary.Remove(0, 1);

            var b = binary.Substring(0, 1);
            result.AlarmActive = b == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var c = binary.Substring(0, 1);
            result.FlshrOffByLedPwrThres = c == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var d = binary.Substring(0, 1);
            result.FlshrOffByLowVin = d == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var e = binary.Substring(0, 1);
            result.FlshrOffByPhotocell = e == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var f = binary.Substring(0, 1);
            result.FlshrOffByTemp = f == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            //var g = binary.Substring(0, 1);
            //result.FlshrOffByTemp = g == "1" ? "Yes" : "No";
            //binary = binary.Remove(0, 1);

            var h = binary.Substring(0, 1);
            result.FlshrOffByForceOff = h == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var i = binary.Substring(0, 1);
            result.IsNight = i == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var j = binary.Substring(0, 1);
            result.ErrLedShort = j == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var k = binary.Substring(0, 1);
            result.ErrLedOpen = k == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var l = binary.Substring(0, 1);
            result.ErrLedVoltageLow = l == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var m = binary.Substring(0, 1);
            result.ErrVinLow = m == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var n = binary.Substring(0, 1);
            result.ErrLedPowerThres = n == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var o = binary.Substring(0, 1);
            result.FlshrAdjToLedMaxAvgPwr = o == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var p = binary.Substring(0, 1);
            result.GsensorInterruptOccur = p == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            var q = binary.Substring(0, 1);
            result.SolarChargingOn = q == "1" ? "Yes" : "No";
            binary = binary.Remove(0, 1);

            return result;
        }

        /// <summary>
        /// Get RMS Description by Digital Input (Not Mando Pro)
        /// </summary>
        /// <param name="digitalInput"></param>
        /// <returns></returns>
        public static (string beat, string batt, string stat, string ambient, string door) GetRms(int? digitalInput)
        {
            (string beatResult, string battResult, string statResult, string ambientResult, string doorResult) = (null, null, null, null, null);
            if (digitalInput.HasValue == false)
            {
                return (beatResult, battResult, statResult, ambientResult, doorResult);
            }
            var binary = digitalInput.Value.ToBinary();
            var beat = binary.Substring(0, 1).FromBinaryToInt();
            var batt = binary.Substring(1, 2).FromBinaryToInt();
            var stat = binary.Substring(3, 2).FromBinaryToInt();
            var ambient = binary.Substring(5, 2).FromBinaryToInt();
            var door = binary.Substring(7, 1).FromBinaryToInt();

            if (beat == 0)
            {
                beatResult = "Tick";
            }
            else if (beat == 1)
            {
                beatResult = "Tock";
            }
            if (batt == 0)
            {
                battResult = "Unknown";
            }
            else if (batt == 1)
            {
                battResult = "Bad";
            }
            else if (batt == 2)
            {
                battResult = "Low";
            }
            else if (batt == 3)
            {
                battResult = "Good";
            }
            if (stat == 0)
            {
                statResult = "No monitoring";
            }
            else if (stat == 1)
            {
                statResult = "Primary";
            }
            else if (stat == 2)
            {
                statResult = "Secondary";
            }
            else if (stat == 3)
            {
                statResult = "Emergency";
            }
            if (ambient == 0)
            {
                ambientResult = "No LDR";
            }
            else if (ambient == 1)
            {
                ambientResult = "Dark";
            }
            else if (ambient == 2)
            {
                ambientResult = "Dim";
            }
            else if (ambient == 3)
            {
                ambientResult = "Bright";
            }
            if (door == 0)
            {
                doorResult = "Close";
            }
            else if (door == 1)
            {
                doorResult = "Open";
            }
            return (beatResult, battResult, statResult, ambientResult, doorResult);
        }

        /// <summary>
        /// Convert coordinate (latitude,longitude) from radian to degree with direction
        /// </summary>
        /// <param name="lat">Latitude</param>
        /// <param name="lng">Longitude</param>
        /// <returns>return in Tuple result</returns>
        public static (string, string) RadianToDegree(double lat, double lng)
        {
            string latDir = (lat >= 0 ? "N" : "S");
            lat = Math.Abs(lat);
            double latMinPart = ((lat - Math.Truncate(lat) / 1) * 60);
            double latSecPart = ((latMinPart - Math.Truncate(latMinPart) / 1) * 60);

            string lonDir = (lng >= 0 ? "E" : "W");
            lng = Math.Abs(lng);
            double lonMinPart = ((lng - Math.Truncate(lng) / 1) * 60);
            double lonSecPart = ((lonMinPart - Math.Truncate(lonMinPart) / 1) * 60);

            return ($"{Math.Truncate(lat)}°{Math.Truncate(latMinPart)}°{Math.Truncate(latSecPart)}'{latDir}",
                     $"{Math.Truncate(lng)}°{Math.Truncate(lonMinPart)}°{Math.Truncate(lonSecPart)}'{lonDir}");
        }

        /// <summary>
        /// check if vessel direction is heading to Location A to B or vice versa
        /// </summary>
        /// <param name="cog"></param>
        /// <param name="pointBearing"></param>
        /// <returns></returns>
        public static bool IsWaypointDirectionAsc(double cog, double pointBearing)
        {
            var x = pointBearing - 90;
            var y = 180 + x;
            if (x < 0)
            {
                x = 360 + x;
            }
            if (y == 360)
            {
                y = 0;
            }
            if (y > 360)
            {
                y = y - 360;
            }

            var a = new Dictionary<double, double>();
            var b = new Dictionary<double, double>();
            if (pointBearing >= 90 && pointBearing <= 270)
            {

                a.Add(y, 360);
                if (y > x)
                {
                    a.Add(0, x);
                }
                b.Add(x, y);

                if ((cog >= a.First().Key && cog <= a.First().Value) || (cog >= a.Last().Key && cog <= a.Last().Value))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                b.Add(x, 360);
                if (x > y)
                {
                    b.Add(0, y);
                }
                a.Add(y, x);
                if ((cog > b.First().Key && cog < b.First().Value) || (cog > b.Last().Key && cog < b.Last().Value))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public static double DegreeBearing(
                    double lat1, double lon1,
                    double lat2, double lon2)
        {
            var dLon = ToRad(lon2 - lon1);
            var dPhi = Math.Log(
                Math.Tan(ToRad(lat2) / 2 + Math.PI / 4) / Math.Tan(ToRad(lat1) / 2 + Math.PI / 4));
            if (Math.Abs(dLon) > Math.PI)
                dLon = dLon > 0 ? -(2 * Math.PI - dLon) : (2 * Math.PI + dLon);
            return ToBearing(Math.Atan2(dLon, dPhi));
        }

        public static double ToRad(double degrees)
        {
            return degrees * (Math.PI / 180);
        }

        public static double ToDegrees(double radians)
        {
            return radians * 180 / Math.PI;
        }

        public static double ToBearing(double radians)
        {
            // convert radians to degrees (as bearing: 0...360)
            return (ToDegrees(radians) + 360) % 360;
        }

        public static double FindAngle(PointF pointA, PointF pointB)
        {
            double Rad2Deg = 180.0 / Math.PI;
            double dx = pointB.X - pointA.X;
            double dy = pointB.Y - pointA.Y;
            double Angle = Math.Atan2(dy, dx) * Rad2Deg;
            if (Angle < 0)
            {
                Angle = Angle + 360;
            }
            return Angle;
        }
        public static double GetDistanceBetweenTwoCoordinates(PointF pointA, PointF pointB, Unit unit = Unit.Meters)
        {
            GeoCoordinate c1 = new GeoCoordinate(pointA.Y, pointA.X);
            GeoCoordinate c2 = new GeoCoordinate(pointB.Y, pointB.X);
            var meters = c1.GetDistanceTo(c2);
            var km = meters / 1000;
            var nm = km * 0.539957;
            if (unit == Unit.Kilometers)
            {
                return km;
            }
            else if (unit == Unit.NauticalMiles)
            {
                return nm;
            }
            else
            {
                return meters;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentDate"></param>
        /// <param name="currentLoc"></param> vessel coordinate
        /// <param name="fromLoc"></param> from location
        /// <param name="targetLoc"></param> target location
        /// <param name="currentSpeed"></param> in Knots
        /// <returns></returns>
        public static DateTime GetETAByDistance(DateTime currentDate, PointF currentLoc, PointF targetLoc, PointF fromLoc, double currentSpeed)
        {
            var temp = GetClosetPointInLine(currentLoc, fromLoc, targetLoc);
            if (currentLoc == fromLoc)
            {
                temp = fromLoc;
            }
            GeoCoordinate c1 = new GeoCoordinate(temp.X, temp.Y);
            GeoCoordinate c2 = new GeoCoordinate(targetLoc.X, targetLoc.Y);
            double distanceInKm = c1.GetDistanceTo(c2) / 1000;
            var speedInKmh = currentSpeed * 1.852;//1.94384;// ;
            var resultInMinutes = (distanceInKm * 60) / speedInKmh;
            return currentDate.AddMinutes(resultInMinutes);
            //GeoCoordinate c1 = new GeoCoordinate(fromLoc.X, fromLoc.Y);
            //GeoCoordinate c2 = new GeoCoordinate(targetLoc.X, targetLoc.Y);
            //double distanceInKm = c1.GetDistanceTo(c2);
            //return currentDate.AddMinutes(distanceInKm);
        }


        public static DateTime GetETAByDistance(DateTime currentDate, double currentSpeed, double distance)
        {
            var distanceInKm = distance * 1.852;
            var speedInKmh = currentSpeed * 1.852;
            var resultInMinutes = (distanceInKm * 60) / speedInKmh;
            return currentDate.AddMinutes(resultInMinutes);
        }

        public static PointF GetClosetPointInLine(
              PointF pt, PointF p1, PointF p2)
        {
            var closest = new PointF();
            float dx = p2.X - p1.X;
            float dy = p2.Y - p1.Y;
            if ((dx == 0) && (dy == 0))
            {
                // It's a point not a line segment.
                return closest = p1;
            }

            // Calculate the t that minimizes the distance.
            float t = ((pt.X - p1.X) * dx + (pt.Y - p1.Y) * dy) /
                (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if (t < 0)
            {
                return closest = new PointF(p1.X, p1.Y);
            }
            else if (t > 1)
            {
                return closest = new PointF(p2.X, p2.Y);
            }
            else
            {
                return closest = new PointF(p1.X + t * dx, p1.Y + t * dy);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pt">Target</param>
        /// <param name="p1">Point A</param>
        /// <param name="p2">Point B</param>
        /// <param name="range">Range from the line (in Nautical Mile)</param>
        /// <returns></returns>
        public static bool IsInRange(PointF pt, PointF p1, PointF p2, double range, Unit unit = Unit.NauticalMiles)
        {
            var intersectPoint = GetClosetPointInLine(pt, p1, p2);

            GeoCoordinate c1 = new GeoCoordinate(intersectPoint.Y, intersectPoint.X);
            GeoCoordinate c2 = new GeoCoordinate(pt.Y, pt.X);
            var meters = c1.GetDistanceTo(c2);
            var km = meters / 1000;
            var nm = km * 0.539957;
            if (unit == Unit.NauticalMiles)
            {
                return nm <= range;
            }
            else if (unit == Unit.Meters)
            {
                return meters <= range;
            }
            else if (unit == Unit.Kilometers)
            {
                return km <= range;
            }
            else
            {
                return nm <= range;
            }
        }

        // Given three colinear points p, q, r, the function checks if 
        // point q lies on line segment 'pr' 
        private static bool OnSegment(PointF p, PointF q, PointF r)
        {
            if (q.X <= Math.Max(p.X, r.X) && q.X >= Math.Min(p.X, r.X) &&
                q.Y <= Math.Max(p.Y, r.Y) && q.Y >= Math.Min(p.Y, r.Y))
            {
                return true;
            }
            return false;
        }

        // To find orientation of ordered triplet (p, q, r). 
        // The function returns following values 
        // 0 --> p, q and r are colinear 
        // 1 --> Clockwise 
        // 2 --> Counterclockwise 
        private static int Orientation(PointF p, PointF q, PointF r)
        {
            // See https://www.geeksforgeeks.org/orientation-3-ordered-points/ 
            // for details of below formula. 
            double val = (q.Y - p.Y) * (r.X - q.X) -
                    (q.X - p.X) * (r.Y - q.Y);

            if (val == 0) return 0;  // colinear 

            return (val > 0) ? 1 : 2; // clock or counterclock wise 
        }

        //Refer https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/
        // Longitude INCREASES means to the EAST
        // Latitude INCREASES means to the NORTH
        // The main function that returns true if line segment 'p1q1' 
        // and 'p2q2' intersect. 
        // p1 and q1 is one line
        public static bool DoIntersect(PointF p1, PointF q1, PointF p2, PointF q2)
        {
            // Find the four orientations needed for general and 
            // special cases 
            int o1 = Orientation(p1, q1, p2);
            int o2 = Orientation(p1, q1, q2);
            int o3 = Orientation(p2, q2, p1);
            int o4 = Orientation(p2, q2, q1);

            // General case 
            if (o1 != o2 && o3 != o4)
                return true;

            // Special Cases 
            // p1, q1 and p2 are colinear and p2 lies on segment p1q1 
            if (o1 == 0 && OnSegment(p1, p2, q1)) return true;

            // p1, q1 and q2 are colinear and q2 lies on segment p1q1 
            if (o2 == 0 && OnSegment(p1, q2, q1)) return true;

            // p2, q2 and p1 are colinear and p1 lies on segment p2q2 
            if (o3 == 0 && OnSegment(p2, p1, q2)) return true;

            // p2, q2 and q1 are colinear and q1 lies on segment p2q2 
            if (o4 == 0 && OnSegment(p2, q1, q2)) return true;

            return false; // Doesn't fall in any of the above cases 
        }
        /// <summary>
        /// Determines if the given point is inside the polygon
        /// </summary>
        /// <param name="polygon">the vertices of polygon</param>
        /// <param name="testPoint">the given point</param>
        /// <returns>true if the point is inside the polygon; otherwise, false</returns>
        public static bool IsPointInPolygon(PointF[] polygon, PointF testPoint)
        {
            bool result = false;
            int j = polygon.Count() - 1;
            for (int i = 0; i < polygon.Count(); i++)
            {
                if (polygon[i].Y < testPoint.Y && polygon[j].Y >= testPoint.Y || polygon[j].Y < testPoint.Y && polygon[i].Y >= testPoint.Y)
                {
                    if (polygon[i].X + (testPoint.Y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) * (polygon[j].X - polygon[i].X) < testPoint.X)
                    {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }
        public static bool IsPointInCircle(double circle_x, double circle_y,
                              double rad, double x, double y)
        {
            GeoCoordinate c1 = new GeoCoordinate(circle_x, circle_y);
            GeoCoordinate c2 = new GeoCoordinate(x, y);

            //GetDistanceTo returns in meters
            double distanceInKm = c1.GetDistanceTo(c2) / 1000;
            //Console.WriteLine(distanceInKm);
            return distanceInKm < (rad * 1.852);
        }

        public static double GetLiveValue(DateTime receivedTime)
        {
            var diffTime = DateTime.Now - receivedTime;
            if (diffTime.TotalHours < 1)
            {
                return Math.Round(diffTime.TotalMinutes, 2);
            }
            return 0;
        }
        public static double GetOutOfRangeValue(DateTime receivedTime)
        {
            var diffTime = DateTime.Now - receivedTime;
            if (diffTime.TotalHours > 0)
            {
                return Math.Round(diffTime.TotalMinutes, 2);
            }
            return 0;
        }
        public static string GetFlagIcon(string flagName)
        {
            return null;
        }
        public static string GetFlagName(int? mmsi)
        {
            if (mmsi == null)
            {
                return null;
            }
            var flagId = Convert.ToInt32(mmsi.ToString().Substring(0, 3));
            if (flagId == 201)
            {
                return "Albania";
            }
            else if (flagId == 202)
            {
                return "Andorra";
            }
            else if (flagId == 203)
            {
                return "Austria";
            }
            else if (flagId == 204)
            {
                return "Azores";
            }
            else if (flagId == 205)
            {
                return "Belgium";
            }
            else if (flagId == 206)
            {
                return "Belarus";
            }
            else if (flagId == 207)
            {
                return "Bulgaria";
            }
            else if (flagId == 208)
            {
                return "Vatican City State";
            }
            else if (flagId == 209 || flagId == 210)
            {
                return "Cyprus";
            }
            else if (flagId == 211)
            {
                return "Germany";
            }
            else if (flagId == 212)
            {
                return "Cyprus";
            }
            else if (flagId == 213)
            {
                return "Georgia";
            }
            else if (flagId == 214)
            {
                return "Moldova";
            }
            else if (flagId == 215)
            {
                return "Malta";
            }
            else if (flagId == 216)
            {
                return "Armenia";
            }
            else
            {
                return null;
            }
        }
        public static IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from; day <= thru; day = day.AddDays(1))
            {
                if (day.Date == thru.Date)
                {
                    yield return thru;
                }
                else if (day.Date == from.Date)
                {
                    yield return from;
                }
                else
                {
                    yield return day.EndOfDay();
                }
            }
        }
        public static string ConvertDecimalCoordToDegree(double lat, double lgt)
        {
            var LatSec = (int)Math.Round(lat * 3600);
            int LatDeg = LatSec / 3600;
            LatSec = Math.Abs(LatSec % 3600);
            int LatMin = LatSec / 60;
            LatSec %= 60;
            var LatAngle = LatDeg < 0 ? "S" : "N";

            var LgtSec = (int)Math.Round(lgt * 3600);
            int LgtDeg = LgtSec / 3600;
            LgtSec = Math.Abs(LgtSec % 3600);
            int LgtMin = LgtSec / 60;
            LgtSec %= 60;
            var LgtAngle = LgtDeg < 0 ? "W" : "E";

            return $"{LatDeg}{LatMin}'{LatAngle}_{LgtDeg}.{LgtMin}'{LgtAngle}";
        }
        public static TimeSpan GetAgingViaLocalRecvTime(DateTime localRecvTime)
        {

            return DateTime.Now - localRecvTime;
        }
        public static string GetEstimationAging(DateTime estimationTime)
        {
            string result = string.Empty;
            var timeSpan = estimationTime.Subtract(DateTime.Now);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                if (timeSpan.Seconds < 0)
                {
                    result = $"0s";
                }
                else
                {
                    result = $"{timeSpan.Seconds}s";
                }

            }
            else if (timeSpan < TimeSpan.FromMinutes(60))
            {
                result = $"{timeSpan.Minutes}m {timeSpan.Seconds}s";
            }
            else if (timeSpan >= TimeSpan.FromHours(1) && timeSpan < TimeSpan.FromDays(1))
            {
                result = $"{timeSpan.Hours}h {timeSpan.Minutes}m {timeSpan.Seconds}s";
            }
            else if (timeSpan >= TimeSpan.FromDays(1))
            {
                result = $"{timeSpan.Days}d {timeSpan.Hours}h {timeSpan.Minutes}m {timeSpan.Seconds}s";
            }

            return result;
        }

        public static string TimeAgoUTC(this DateTime? dateTime)
        {
            if (dateTime == null)
            {
                return null;
            }
            string result = string.Empty;
            var timeSpan = DateTime.UtcNow.Subtract(dateTime.Value);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                var s = timeSpan.Seconds;
                if (s < 0)
                {
                    s = 0;
                }
                result = string.Format("{0} seconds ago", s);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("about {0} minutes ago", timeSpan.Minutes) :
                    "about a minute ago";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("about {0} hours ago", timeSpan.Hours) :
                    "about an hour ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                if (timeSpan.Days > 1)
                {
                    result = timeSpan.Days > 1 ?
                   String.Format("about {0} days ago", timeSpan.Days) :
                   "yesterday";
                }
                else
                {
                    result = timeSpan.Days > 0 ?
                   String.Format("about {0} day ago", timeSpan.Days) :
                   "yesterday";
                }

            }

            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("about {0} months ago", timeSpan.Days / 30) :
                    "about a month ago";
            }
            else
            {
                result = timeSpan.Days > 730 ?
                    String.Format("about {0} years ago", timeSpan.Days / 365) :
                    "about a year ago";
            }

            return result;
        }

        public static string TimeAgo(this DateTime? dateTime)
        {
            if (dateTime == null)
            {
                return null;
            }
            string result = string.Empty;
            var timeSpan = DateTime.Now.Subtract(dateTime.Value);

            if (timeSpan <= TimeSpan.FromSeconds(60))
            {
                var s = timeSpan.Seconds;
                if (s < 0)
                {
                    s = 0;
                }
                result = string.Format("{0} seconds ago", s);
            }
            else if (timeSpan <= TimeSpan.FromMinutes(60))
            {
                result = timeSpan.Minutes > 1 ?
                    String.Format("about {0} minutes ago", timeSpan.Minutes) :
                    "about a minute ago";
            }
            else if (timeSpan <= TimeSpan.FromHours(24))
            {
                result = timeSpan.Hours > 1 ?
                    String.Format("about {0} hours ago", timeSpan.Hours) :
                    "about an hour ago";
            }
            else if (timeSpan <= TimeSpan.FromDays(30))
            {
                if (timeSpan.Days > 1)
                {
                    result = timeSpan.Days > 1 ?
                   String.Format("about {0} days ago", timeSpan.Days) :
                   "yesterday";
                }
                else
                {
                    result = timeSpan.Days > 0 ?
                   String.Format("about {0} day ago", timeSpan.Days) :
                   "yesterday";
                }
               
            }
            
            else if (timeSpan <= TimeSpan.FromDays(365))
            {
                result = timeSpan.Days > 30 ?
                    String.Format("about {0} months ago", timeSpan.Days / 30) :
                    "about a month ago";
            }
            else
            {
                result = timeSpan.Days > 730 ?
                    String.Format("about {0} years ago", timeSpan.Days / 365) :
                    "about a year ago";
            }

            return result;
        }
        public static string GetTargetTypeName(int? targetType)
        {
            if ((targetType >= 10) && (targetType <= 19))
            {
                return "Reserved";
            }
            else if ((targetType >= 20) && (targetType <= 28))
            {
                return "Wing In Ground";
            }
            else if (targetType == 29)
            {
                return "SAR Aircraft";
            }
            else if (targetType == 30)
            {
                return "Fishing";
            }
            else if (targetType == 31)
            {
                return "Tug";
            }
            else if (targetType == 32)
            {
                return "Tug";
            }
            else if (targetType == 33)
            {
                return "Dredger";
            }
            else if (targetType == 34)
            {
                return "Dive Vessel";
            }
            else if (targetType == 35)
            {
                return "Military Ops";
            }
            else if (targetType == 36)
            {
                return "Sailing Vessel";
            }
            else if (targetType == 37)
            {
                return "Pleasure Craft";
            }
            else if (targetType == 38)
            {
                return "Reserved";
            }
            else if (targetType == 39)
            {
                return "Reserved";
            }
            else if ((targetType >= 40) && (targetType <= 49))
            {
                return "High-Speed Craft";
            }
            else if (targetType == 50)
            {
                return "Pilot Vessel";
            }
            else if (targetType == 51)
            {
                return "SAR";
            }
            else if (targetType == 52)
            {
                return "Tug";
            }
            else if (targetType == 53)
            {
                return "Port Tender";
            }
            else if (targetType == 54)
            {
                return "Anti-Pollution";
            }
            else if (targetType == 55)
            {
                return "Law Enforce";
            }
            else if (targetType == 56)
            {
                return "Local Vessel";
            }
            else if (targetType == 57)
            {
                return "Local Vessel";
            }
            else if (targetType == 58)
            {
                return "Medical Trans";
            }
            else if (targetType == 59)
            {
                return "Special Craft";
            }
            else if ((targetType >= 60) && (targetType <= 69))
            {
                return "Passenger";
            }
            else if (targetType == 70)
            {
                return "Cargo";
            }
            else if (targetType == 71)
            {
                return "Cargo - Hazard A (Major)";
            }
            else if (targetType == 72)
            {
                return "Cargo - Hazard B";
            }
            else if (targetType == 73)
            {
                return "Cargo - Hazard C (Minor)";
            }
            else if (targetType == 74)
            {
                return "Cargo - Hazard D (Recognizable)";
            }
            else if ((targetType >= 75) && (targetType <= 79))
            {
                return "Cargo";
            }
            else if (targetType == 80)
            {
                return "Tanker";
            }
            else if (targetType == 81)
            {
                return "Tanker - Hazard A (Major)";
            }
            else if (targetType == 82)
            {
                return "Tanker - Hazard B";
            }
            else if (targetType == 83)
            {
                return "Tanker - Hazard C (Minor)";
            }
            else if (targetType == 84)
            {
                return "Tanker - Hazard D (Recognizable)";
            }
            else if ((targetType >= 85) && (targetType <= 89))
            {
                return "Tanker";
            }
            else if ((targetType >= 90) && (targetType <= 99))
            {
                return "Other";
            }
            else if (targetType == 100)
            {
                return "Navigation Aid";
            }
            else if (targetType == 101)
            {
                return "Reference Point";
            }
            else if (targetType == 102)
            {
                return "RACON";
            }
            else if (targetType == 103)
            {
                return "OffShore Structure";
            }
            else if (targetType == 104)
            {
                return "Spare";
            }
            else if (targetType == 105)
            {
                return "Light, without Sectors";
            }
            else if (targetType == 106)
            {
                return "Light, with Sectors";
            }
            else if (targetType == 107)
            {
                return "Leading Light Front";
            }
            else if (targetType == 108)
            {
                return "Leading Light Rear";
            }
            else if (targetType == 109)
            {
                return "Beacon, Cardinal N";
            }
            else if (targetType == 110)
            {
                return "Beacon, Cardinal E";
            }
            else if (targetType == 111)
            {
                return "Beacon, Cardinal S";
            }
            else if (targetType == 112)
            {
                return "Beacon, Cardinal W";
            }
            else if (targetType == 113)
            {
                return "Beacon, Port Hand";
            }
            else if (targetType == 114)
            {
                return "Beacon, Starboard Hand";
            }
            else if (targetType == 115)
            {
                return "Beacon, Preferred Channel Port hand";
            }
            else if (targetType == 116)
            {
                return "Beacon, Preferred Channel Starboard hand";
            }
            else if (targetType == 117)
            {
                return "Beacon, Isolated danger";
            }
            else if (targetType == 118)
            {
                return "Beacon, Safe Water";
            }
            else if (targetType == 119)
            {
                return "Beacon, Special Mark";
            }
            else if (targetType == 120)
            {
                return "Cardinal Mark N";
            }
            else if (targetType == 121)
            {
                return "Cardinal Mark E";
            }
            else if (targetType == 122)
            {
                return "Cardinal Mark S";
            }
            else if (targetType == 123)
            {
                return "Cardinal Mark W";
            }
            else if (targetType == 124)
            {
                return "Port Hand Mark";
            }
            else if (targetType == 125)
            {
                return "Starboard Hand Mark";
            }
            else if (targetType == 126)
            {
                return "Preferred Channel Port Hand";
            }
            else if (targetType == 127)
            {
                return "Preferred Channel Starboard Hand";
            }
            else if (targetType == 128)
            {
                return "Isolated Danger";
            }
            else if (targetType == 129)
            {
                return "Safe Water";
            }
            else if (targetType == 130)
            {
                return "Manned VTS";
            }
            else if (targetType == 131)
            {
                return "Light Vessel";
            }
            else
            {
                return null;
            }
        }
        public static DateTime ExtractDateFromAisDbName(string aisDbName)
        {
            var result = aisDbName.Substring(0, 3);
            if (result.ToLower() == "ais")
            {
                var dateStr = aisDbName.Substring(3, aisDbName.Length - 3);
                var yearStr = $"20{dateStr.Substring(0, 2)}";
                var monthStr = dateStr.Substring(2, 2);
                var dayStr = dateStr.Substring(4, 2);
                var formatDateStr = $"{monthStr}/{dayStr}/{yearStr}";
                return Convert.ToDateTime(formatDateStr);
            }
            throw new Exception("Invalid AIS DB Date");
        }
        public static string GetAisDbNameFromDate(DateTime date)
        {
            return $"AIS{date.ToString("yyMMdd")}";
        }
        public static AisDbInfoDto GetAisDbInfo(string environmentName, DateTime dbDate, string serverName, string username, string password)
        {
            DateTime filterDate = DateTime.Now;

            if (string.Equals(environmentName, Constants.HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase))
            {
                //filterDate = dbDate.AddMinutes(minutes);
                filterDate = dbDate.AddMinutes((Convert.ToDateTime(dbDate.ToString("yyyy-MM-dd HH:mm:ss")) - dbDate).TotalMinutes);
            }

            var postFixDbName = $"AIS{filterDate.ToString("yyMMdd")}";

            if (filterDate >= Convert.ToDateTime($"{filterDate.ToString("yyyy-MM-dd")} 08:00:00"))
                postFixDbName = $"AIS{filterDate.ToString("yyMMdd")}";
            else
                postFixDbName = $"AIS{filterDate.AddDays(-1).ToString("yyMMdd")}";

            DateTime toDateTime = DateTime.Parse(filterDate.ToString("yyyy-MM-dd HH:mm:ss"));
            DateTime fromDateTime = toDateTime.AddMinutes(-1);

            var result = new AisDbInfoDto
            {
                AisDbName = postFixDbName,
                FromDate = fromDateTime,
                ToDate = toDateTime,
                ServerName = serverName,
                Password = password,
                Username = username,
                EnvironmentName = environmentName
            };
            return result;
        }
        public static List<int> GetMessageIdByTargetClass(TargetClass targetClass)
        {
            switch (targetClass)
            {
                case TargetClass.ClassA:
                    return new List<int> { 1, 2, 3 };
                case TargetClass.ClassB:
                    return new List<int> { 18 };
                case TargetClass.SAR:
                    return new List<int> { 9 };
                case TargetClass.SART:
                    return new List<int> { 21 };
                case TargetClass.Aton:
                    return new List<int> { 21 };
                case TargetClass.VirtualAton:
                    return new List<int> { 21 };
                case TargetClass.BaseStation:
                    return new List<int> { 4 };
                case TargetClass.Methydro:
                    return new List<int> { 8 };
                default:
                    return new List<int> { 0 };
            }
        }
        public static TargetClass GetTargetClassByMessageId(int messageId)
        {
            if (GetMessageIdByTargetClass(TargetClass.ClassA).Contains(messageId))
            {
                return TargetClass.ClassA;
            }
            else if (GetMessageIdByTargetClass(TargetClass.ClassB).Contains(messageId))
            {
                return TargetClass.ClassB;
            }
            else if (GetMessageIdByTargetClass(TargetClass.Aton).Contains(messageId))
            {
                return TargetClass.Aton;
            }
            else if (GetMessageIdByTargetClass(TargetClass.VirtualAton).Contains(messageId))
            {
                return TargetClass.VirtualAton;
            }
            else if (GetMessageIdByTargetClass(TargetClass.BaseStation).Contains(messageId))
            {
                return TargetClass.BaseStation;
            }
            else if (GetMessageIdByTargetClass(TargetClass.Methydro).Contains(messageId))
            {
                return TargetClass.Methydro;
            }
            else if (GetMessageIdByTargetClass(TargetClass.SAR).Contains(messageId))
            {
                return TargetClass.SAR;
            }
            else if (GetMessageIdByTargetClass(TargetClass.SART).Contains(messageId))
            {
                return TargetClass.SART;
            }
            else
            {
                return TargetClass.Other;
            }
        }
        /// <summary>
        /// Dictionary Key = Start Date
        /// Dictionary Value = End Date
        /// </summary>
        /// <param name="timeSpan"></param>
        /// <returns></returns>
        public static Dictionary<DateTime, DateTime> ResolveReportTimeSpanToStartEndDate(ReportGeneratorTimeSpan timeSpan)
        {
            var result = new Dictionary<DateTime, DateTime>();
            if (timeSpan == ReportGeneratorTimeSpan.All)
            {
                var lastTwoYear = DateTime.Now.AddYears(-2);
                result.Add(new DateTime(lastTwoYear.Year, lastTwoYear.Month, 1).StartOfDay(), DateTime.Now);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Last_24_Hours)
            {
                result.Add(DateTime.Now.AddHours(-24), DateTime.Now);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Last_48_Hours)
            {
                result.Add(DateTime.Now.AddHours(-48), DateTime.Now);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Last_10_Days)
            {
                result.Add(StartOfDay(DateTime.Now.AddDays(-10)), DateTime.Now);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Last_30_Days)
            {
                result.Add(DateTime.Now.AddDays(-30).StartOfDay(), DateTime.Now);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Last_90_Days)
            {
                result.Add(DateTime.Now.AddDays(-90).StartOfDay(), DateTime.Now);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Last_365_Days)
            {
                result.Add(DateTime.Now.AddDays(-365).StartOfDay(), DateTime.Now);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Today)
            {
                result.Add(StartOfDay(DateTime.Now), DateTime.Now);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Yesterday)
            {
                var yesterday = DateTime.Now.AddDays(-1);
                result.Add(StartOfDay(yesterday), EndOfDay(yesterday));
            }
            else if (timeSpan == ReportGeneratorTimeSpan.This_Week)
            {
                var nameOfDay = DateTime.Now.DayOfWeek;
                if (nameOfDay == DayOfWeek.Sunday)
                {
                    result.Add(StartOfDay(DateTime.Now), DateTime.Now);
                }
                else if (nameOfDay == DayOfWeek.Monday)
                {
                    result.Add(DateTime.Now.AddDays(-1).StartOfDay(), DateTime.Now);
                }
                else if (nameOfDay == DayOfWeek.Tuesday)
                {
                    result.Add(DateTime.Now.AddDays(-2).StartOfDay(), DateTime.Now);
                }
                else if (nameOfDay == DayOfWeek.Wednesday)
                {
                    result.Add(DateTime.Now.AddDays(-3).StartOfDay(), DateTime.Now);
                }
                else if (nameOfDay == DayOfWeek.Thursday)
                {
                    result.Add(DateTime.Now.AddDays(-4).StartOfDay(), DateTime.Now);
                }
                else if (nameOfDay == DayOfWeek.Friday)
                {
                    result.Add(DateTime.Now.AddDays(-5).StartOfDay(), DateTime.Now);
                }
                else if (nameOfDay == DayOfWeek.Saturday)
                {
                    result.Add(DateTime.Now.AddDays(-6).StartOfDay(), DateTime.Now);
                }
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Last_Week)
            {
                var nameOfDay = DateTime.Now.DayOfWeek;
                if (nameOfDay == DayOfWeek.Sunday)
                {
                    result.Add(DateTime.Now.AddDays(-6).StartOfDay(), DateTime.Now.AddDays(-1).EndOfDay());
                }
                else if (nameOfDay == DayOfWeek.Monday)
                {
                    result.Add(DateTime.Now.AddDays(-7).StartOfDay(), DateTime.Now.AddDays(-2).EndOfDay());
                }
                else if (nameOfDay == DayOfWeek.Tuesday)
                {
                    result.Add(DateTime.Now.AddDays(-8).StartOfDay(), DateTime.Now.AddDays(-3).EndOfDay());
                }
                else if (nameOfDay == DayOfWeek.Wednesday)
                {
                    result.Add(DateTime.Now.AddDays(-9).StartOfDay(), DateTime.Now.AddDays(-4).EndOfDay());
                }
                else if (nameOfDay == DayOfWeek.Thursday)
                {
                    result.Add(DateTime.Now.AddDays(-10).StartOfDay(), DateTime.Now.AddDays(-5).EndOfDay());
                }
                else if (nameOfDay == DayOfWeek.Friday)
                {
                    result.Add(DateTime.Now.AddDays(-11).StartOfDay(), DateTime.Now.AddDays(-6).EndOfDay());
                }
                else if (nameOfDay == DayOfWeek.Saturday)
                {
                    result.Add(DateTime.Now.AddDays(-12).StartOfDay(), DateTime.Now.AddDays(-7).EndOfDay());
                }

            }
            else if (timeSpan == ReportGeneratorTimeSpan.This_Month)
            {
                var eod = DateTime.Now;
                var sod = DateTime.Now.AddDays(-(DateTime.Now.Day - 1)).StartOfDay();
                result.Add(sod, eod);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Last_Month)
            {
                var lastMonth = DateTime.Now.AddMonths(-1);
                var numberOfDays = DateTime.DaysInMonth(lastMonth.Year, lastMonth.Month);
                var eod = new DateTime(lastMonth.Year, lastMonth.Month, numberOfDays).EndOfDay();
                var sod = new DateTime(lastMonth.Year, lastMonth.Month, 1).StartOfDay();
                result.Add(sod, eod);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.This_Quarter)
            {
                var today = DateTime.Now;
                var quarter = GetQuarterOfTheYear(today.Month);
                var eod = today;
                var sod = new DateTime(today.Year, quarter.First(), 1).StartOfDay();
                result.Add(sod, eod);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Last_Quater)
            {
                var today = DateTime.Now;
                var eod = new DateTime(today.Year, today.Month, 1).AddDays(-1).EndOfDay();
                var quarter = GetQuarterOfTheYear(eod.Month);
                var sod = new DateTime(today.Year, quarter.First(), 1).StartOfDay();
                result.Add(sod, eod);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.This_Year)
            {
                var eod = DateTime.Now;
                var sod = new DateTime(eod.Year, 1, 1).StartOfDay();
                result.Add(sod, eod);
            }
            else if (timeSpan == ReportGeneratorTimeSpan.Last_Year)
            {
                var today = DateTime.Now;
                var eod = new DateTime(today.Year - 1, 12, 31).EndOfDay();
                var sod = new DateTime(today.Year - 1, 1, 1).StartOfDay();
                result.Add(sod, eod);
            }

            return result;
        }

        public static List<int> GetQuarterOfTheYear(int dayOfTheMonth)
        {
            if (dayOfTheMonth == 1 || dayOfTheMonth == 2 || dayOfTheMonth == 3)
            {
                return new List<int> { 1, 2, 3 };
            }
            else if (dayOfTheMonth == 4 || dayOfTheMonth == 5 || dayOfTheMonth == 6)
            {
                return new List<int> { 4, 5, 6 };
            }
            else if (dayOfTheMonth == 7 || dayOfTheMonth == 8 || dayOfTheMonth == 9)
            {
                return new List<int> { 7, 8, 9 };
            }
            else if (dayOfTheMonth == 10 || dayOfTheMonth == 11 || dayOfTheMonth == 12)
            {
                return new List<int> { 10, 11, 12 };
            }
            return new List<int>();
        }

        public static DateTime StartOfDay(this DateTime theDate)
        {
            return new DateTime(theDate.Year, theDate.Month, theDate.Day, 0, 0, 0, 0);
        }

        public static DateTime EndOfDay(this DateTime theDate)
        {
            return theDate.Date.AddDays(1).AddTicks(-1);
        }
    }

    public class AisDbInfoDto
    {
        public string EnvironmentName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string AisDbName { get; set; }
        public string ServerName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsUsingIntegratedSecurity { get; set; } = false;
    }
}
