﻿using System;
using System.Collections.Generic;
using System.Linq;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Enums;

namespace Vtmis.Core.Common.Helpers
{
    public static class TargetHelper
    {
        public static string GetBattery(int? val)
        {
            if (val == 0)
            {
                return "Good";
            }
            if (val == 1)
            {
                return "Low";
            }
            return null;
        }
        public static string GetHealth(int? val)
        {
            if (val == 0)
            {
                return "Good";
            }
            if (val == 1)
            {
                return "Alarm";
            }
            return null;
        }
        public static string GetLight(int? val)
        {
            if (val == 0)
            {
                return "No Light";
            }
            if (val == 1)
            {
                return "On";
            }
            if (val == 2)
            {
                return "Off";
            }
            if (val == 3)
            {
                return "Fail";
            }
            return null;
        }
        public static string GetRacon(int? val)
        {
            if (val == 0)
            {
                return "Not Installed";
            }
            if (val == 1)
            {
                return "Not Monitored";
            }
            if (val == 2)
            {
                return "Operational";
            }
            if (val == 3)
            {
                return "Error";
            }
            return null;
        }
        public static AtonMsgFormat? GetAtonMessageFormat(int? dac, int? fi)
        {
            if (dac == 235 && fi == 10)
            {
                return AtonMsgFormat.Iala;
            }
            else if (dac == 533 && fi == 1)
            {
                return AtonMsgFormat.MsiaLightBeacon;
            }
            else if (dac == 533 && fi == 4)
            {
                return AtonMsgFormat.MsiaLantern;
            }
            else
            {
                return null;
            }
        }
        public static (string maxAirDraught, string minAirDraught, string actualAirDraught,
            string voltage, string casingCover, string halfFull, string shipLoad, string sonar, int numHoppers, string hopper, string supplyResult) GetDDMS(double? empty, double? full, double? actual,
            double? battery, bool? cover, bool? half, short? sonarVal, bool isMiddleValueExist, short? hoppers, short? hopperFlag, bool? supply)
        {
            (string maxAirDraught, string minAirDraught, string actualAirDraught,
            string voltage, string casingCover, string halfFull, string shipLoad, string sonar, int numHoppers, string hopper, string supplyResult) = (null, null, null, null, null, null, null, null, 0, null, null);

            if (empty.HasValue)
            {
                maxAirDraught = $"{empty}m";
            }

            if (full.HasValue)
            {
                minAirDraught = $"{full}m";
            }

            if (actual.HasValue)
            {
                actualAirDraught = $"{actual}m";
            }

            if (battery.HasValue)
            {
                voltage = battery.ToString();
            }

            if (cover.HasValue)
            {
                if (cover.Value)
                {
                    casingCover = $"Open";
                }
                else
                {
                    casingCover = $"Closed";
                }

            }

            if (isMiddleValueExist)
            {
                if (half.HasValue)
                {
                    if (half == true)
                    {
                        //halfFull = "Yes";
                        //shipLoad = "Loaded";
                        halfFull = "No";
                        shipLoad = "Empty";
                    }
                    else
                    {
                        //halfFull = "No";
                        //shipLoad = "Empty";
                        halfFull = "Yes";
                        shipLoad = "Loaded";
                    }
                }
                else
                {
                    halfFull = "No";
                    shipLoad = "Empty";
                }
            }
            else
            {
                halfFull = "N/A";
                shipLoad = "N/A";
            }

            if (sonarVal.HasValue)
            {
                if (sonarVal == 0)
                {
                    sonar = "Disabled";
                }
                else if (sonarVal == 1)
                {
                    sonar = "Enabled";
                }
                else if (sonarVal == 2)
                {
                    sonar = "Error";
                }
            }

            if (hoppers.HasValue)
            {
                numHoppers = (int)hoppers;
            }

            if (hopperFlag.HasValue)
            {
                if (hopperFlag == 0)
                {
                    hopper = "Open";
                }
                else if (hopperFlag == 1)
                {
                    hopper = "Close";
                }
            }

            if (supply.HasValue)
            {
                if (supply == true)
                {
                    supplyResult = "Powered";
                }
                else
                {
                    supplyResult = "Battery";
                }
            }

            return (maxAirDraught, minAirDraught, actualAirDraught,
             voltage, casingCover, halfFull, shipLoad, sonar, numHoppers, hopper, supplyResult);
        }
        public static int GetNavStatusIdByName(string name)
        {
            if (string.Equals(name, NavStatusConst.UTW_Using_Engine, StringComparison.CurrentCultureIgnoreCase))
            {
                return 0;
            }
            if (string.Equals(name, NavStatusConst.AtAnchor, StringComparison.CurrentCultureIgnoreCase))
            {
                return 1;
            }
            if (string.Equals(name, NavStatusConst.NotUnderCommand, StringComparison.CurrentCultureIgnoreCase))
            {
                return 2;
            }
            if (string.Equals(name, NavStatusConst.RestrictManeu, StringComparison.CurrentCultureIgnoreCase))
            {
                return 3;
            }
            if (string.Equals(name, NavStatusConst.ConstraintDraught, StringComparison.CurrentCultureIgnoreCase))
            {
                return 4;
            }
            if (string.Equals(name, NavStatusConst.Moored, StringComparison.CurrentCultureIgnoreCase))
            {
                return 5;
            }
            if (string.Equals(name, NavStatusConst.Aground, StringComparison.CurrentCultureIgnoreCase))
            {
                return 6;
            }
            if (string.Equals(name, NavStatusConst.EngagedFishing, StringComparison.CurrentCultureIgnoreCase))
            {
                return 7;
            }
            if (string.Equals(name, NavStatusConst.UTW_Sailing, StringComparison.CurrentCultureIgnoreCase))
            {
                return 8;
            }
            if (string.Equals(name, NavStatusConst.ReservedCarryCat_C, StringComparison.CurrentCultureIgnoreCase))
            {
                return 9;
            }
            if (string.Equals(name, NavStatusConst.ReservedCarryCat_A, StringComparison.CurrentCultureIgnoreCase))
            {
                return 10;
            }
            if (string.Equals(name, NavStatusConst.PowerDrivenVesselTowing, StringComparison.CurrentCultureIgnoreCase))
            {
                return 11;
            }
            if (string.Equals(name, NavStatusConst.PowerDrivenVesselPush, StringComparison.CurrentCultureIgnoreCase))
            {
                return 12;
            }
            if (string.Equals(name, NavStatusConst.ReservedFutureUse, StringComparison.CurrentCultureIgnoreCase))
            {
                return 13;
            }
            if (string.Equals(name, NavStatusConst.AisSartActiveETC, StringComparison.CurrentCultureIgnoreCase))
            {
                return 14;
            }
            if (string.Equals(name, NavStatusConst.NotDefined, StringComparison.CurrentCultureIgnoreCase))
            {
                return 15;
            }
            return -1;
        }
        public static string GetNavStatusByName(string navStatus)
        {
            return NavStatusConst.All()
                .FirstOrDefault(x =>
                string.Equals(x, navStatus, StringComparison.CurrentCultureIgnoreCase));
        }
        public static string GetNavStatusDescriptionByNavStatusId(int? navStatusId)
        {
            if (navStatusId.HasValue)
            {
                if (navStatusId.Value == 0)
                {
                    return NavStatusConst.UTW_Using_Engine;
                }
                else if (navStatusId.Value == 1)
                {
                    return NavStatusConst.AtAnchor;
                }
                else if (navStatusId.Value == 2)
                {
                    return NavStatusConst.NotUnderCommand;
                }
                else if (navStatusId.Value == 3)
                {
                    return NavStatusConst.RestrictManeu;
                }
                else if (navStatusId.Value == 4)
                {
                    return NavStatusConst.ConstraintDraught;
                }
                else if (navStatusId.Value == 5)
                {
                    return NavStatusConst.Moored;
                }
                else if (navStatusId.Value == 6)
                {
                    return NavStatusConst.Aground;
                }
                else if (navStatusId.Value == 7)
                {
                    return NavStatusConst.EngagedFishing;
                }
                else if (navStatusId.Value == 8)
                {
                    return NavStatusConst.UTW_Sailing;
                }
                else if (navStatusId.Value == 9)
                {
                    return NavStatusConst.ReservedCarryCat_C;
                }
                else if (navStatusId.Value == 10)
                {
                    return NavStatusConst.ReservedCarryCat_A;
                }
                else if (navStatusId.Value == 11)
                {
                    return NavStatusConst.PowerDrivenVesselTowing;
                }
                else if (navStatusId.Value == 12)
                {
                    return NavStatusConst.PowerDrivenVesselPush;
                }
                else if (navStatusId.Value == 13)
                {
                    return NavStatusConst.ReservedFutureUse;
                }
                else if (navStatusId.Value == 14)
                {
                    return NavStatusConst.AisSartActiveETC;
                }
                else if (navStatusId.Value == 15)
                {
                    return NavStatusConst.NotDefined;
                }
                else
                {
                    return null;
                }
            }
            return null;
        }
        public static int GetShipTypeIdByName(string name)
        {
            if (string.Equals(name, ShipTypeConst.Reserved, StringComparison.CurrentCultureIgnoreCase))
            {
                return 10;
            }
            if (string.Equals(name, ShipTypeConst.WingInGround, StringComparison.CurrentCultureIgnoreCase))
            {
                return 29;
            }
            //if (string.Equals(name, ShipTypeConst.SAR_Aircraft, StringComparison.CurrentCultureIgnoreCase))
            //{
            //    return 29;
            //}
            if (string.Equals(name, ShipTypeConst.Fishing, StringComparison.CurrentCultureIgnoreCase))
            {
                return 30;
            }
            if (string.Equals(name, ShipTypeConst.Tug, StringComparison.CurrentCultureIgnoreCase))
            {
                return 52;
            }
            if (string.Equals(name, ShipTypeConst.Dredger, StringComparison.CurrentCultureIgnoreCase))
            {
                return 33;
            }
            if (string.Equals(name, ShipTypeConst.DiveVessel, StringComparison.CurrentCultureIgnoreCase))
            {
                return 34;
            }
            if (string.Equals(name, ShipTypeConst.MilitaryOps, StringComparison.CurrentCultureIgnoreCase))
            {
                return 35;
            }
            if (string.Equals(name, ShipTypeConst.SailingVessel, StringComparison.CurrentCultureIgnoreCase))
            {
                return 36;
            }
            if (string.Equals(name, ShipTypeConst.PleasureCraft, StringComparison.CurrentCultureIgnoreCase))
            {
                return 37;
            }
            if (string.Equals(name, ShipTypeConst.HighSpeedCraft, StringComparison.CurrentCultureIgnoreCase))
            {
                return 40;
            }
            if (string.Equals(name, ShipTypeConst.PilotVessel, StringComparison.CurrentCultureIgnoreCase))
            {
                return 50;
            }
            if (string.Equals(name, ShipTypeConst.SAR, StringComparison.CurrentCultureIgnoreCase))
            {
                return 51;
            }
            if (string.Equals(name, ShipTypeConst.PortTender, StringComparison.CurrentCultureIgnoreCase))
            {
                return 53;
            }
            if (string.Equals(name, ShipTypeConst.AntiPollution, StringComparison.CurrentCultureIgnoreCase))
            {
                return 54;
            }
            if (string.Equals(name, ShipTypeConst.LawEnforce, StringComparison.CurrentCultureIgnoreCase))
            {
                return 55;
            }
            if (string.Equals(name, ShipTypeConst.LocalVessel, StringComparison.CurrentCultureIgnoreCase))
            {
                return 56;
            }
            if (string.Equals(name, ShipTypeConst.MedicalTrans, StringComparison.CurrentCultureIgnoreCase))
            {
                return 58;
            }
            if (string.Equals(name, ShipTypeConst.SpecialCraft, StringComparison.CurrentCultureIgnoreCase))
            {
                return 59;
            }
            if (string.Equals(name, ShipTypeConst.Passenger, StringComparison.CurrentCultureIgnoreCase))
            {
                return 69;
            }
            if (string.Equals(name, ShipTypeConst.Cargo, StringComparison.CurrentCultureIgnoreCase))
            {
                return 79;
            }
            if (string.Equals(name, ShipTypeConst.CargoHazardA_Major, StringComparison.CurrentCultureIgnoreCase))
            {
                return 71;
            }
            if (string.Equals(name, ShipTypeConst.CargoHazardB, StringComparison.CurrentCultureIgnoreCase))
            {
                return 72;
            }
            if (string.Equals(name, ShipTypeConst.CargoHazardC_Minor, StringComparison.CurrentCultureIgnoreCase))
            {
                return 73;
            }
            if (string.Equals(name, ShipTypeConst.CargoHazardD_Recognize, StringComparison.CurrentCultureIgnoreCase))
            {
                return 74;
            }
            if (string.Equals(name, ShipTypeConst.Tanker, StringComparison.CurrentCultureIgnoreCase))
            {
                return 89;
            }
            if (string.Equals(name, ShipTypeConst.Reserved, StringComparison.CurrentCultureIgnoreCase))
            {
                return 10;
            }
            return 0;
        }
        public static string GetShipTypeByName(string shipType)
        {
            return ShipTypeConst.All()
                .FirstOrDefault(x =>
                string.Equals(x, shipType, StringComparison.CurrentCultureIgnoreCase));
        }
        public static string GetShipTypeByShipTypeId(int? shipTypeId)
        {
            if (shipTypeId == null)
            {
                return null;
            }
            if ((shipTypeId >= 10) && (shipTypeId <= 19))
            {
                return ShipTypeConst.Reserved;
            }
            else if ((shipTypeId >= 20) && (shipTypeId <= 29))
            {
                return ShipTypeConst.WingInGround;
            }
            //else if (shipTypeId == 29)
            //{
            //    return ShipTypeConst.SAR_Aircraft;
            //}
            else if (shipTypeId == 30)
            {
                return ShipTypeConst.Fishing;
            }
            else if (shipTypeId == 31)
            {
                return ShipTypeConst.Tug;
            }
            else if (shipTypeId == 32)
            {
                return ShipTypeConst.Tug;
            }
            else if (shipTypeId == 33)
            {
                return ShipTypeConst.Dredger;
            }
            else if (shipTypeId == 34)
            {
                return ShipTypeConst.DiveVessel;
            }
            else if (shipTypeId == 35)
            {
                return ShipTypeConst.MilitaryOps;
            }
            else if (shipTypeId == 36)
            {
                return ShipTypeConst.SailingVessel;
            }
            else if (shipTypeId == 37)
            {
                return ShipTypeConst.PleasureCraft;
            }
            else if (shipTypeId == 38)
            {
                return ShipTypeConst.Reserved;
            }
            else if (shipTypeId == 39)
            {
                return ShipTypeConst.Reserved;
            }
            else if ((shipTypeId >= 40) && (shipTypeId <= 49))
            {
                return ShipTypeConst.HighSpeedCraft;
            }
            else if (shipTypeId == 50)
            {
                return ShipTypeConst.PilotVessel;
            }
            else if (shipTypeId == 51)
            {
                return ShipTypeConst.SAR;
            }
            else if (shipTypeId == 52)
            {
                return ShipTypeConst.Tug;
            }
            else if (shipTypeId == 53)
            {
                return ShipTypeConst.PortTender;
            }
            else if (shipTypeId == 54)
            {
                return ShipTypeConst.AntiPollution;
            }
            else if (shipTypeId == 55)
            {
                return ShipTypeConst.LawEnforce;
            }
            else if (shipTypeId == 56)
            {
                return ShipTypeConst.LocalVessel;
            }
            else if (shipTypeId == 57)
            {
                return ShipTypeConst.LocalVessel;
            }
            else if (shipTypeId == 58)
            {
                return ShipTypeConst.MedicalTrans;
            }
            else if (shipTypeId == 59)
            {
                return ShipTypeConst.SpecialCraft;
            }
            else if ((shipTypeId >= 60) && (shipTypeId <= 69))
            {
                return ShipTypeConst.Passenger;
            }
            else if (shipTypeId == 70)
            {
                return ShipTypeConst.Cargo;
            }
            else if (shipTypeId == 71)
            {
                return ShipTypeConst.CargoHazardA_Major;
            }
            else if (shipTypeId == 72)
            {
                return ShipTypeConst.CargoHazardB;
            }
            else if (shipTypeId == 73)
            {
                return ShipTypeConst.CargoHazardC_Minor;
            }
            else if (shipTypeId == 74)
            {
                return ShipTypeConst.CargoHazardD_Recognize;
            }
            else if ((shipTypeId >= 75) && (shipTypeId <= 79))
            {
                return ShipTypeConst.Cargo;
            }
            else if (shipTypeId == 80)
            {
                return ShipTypeConst.Tanker;
            }
            else if (shipTypeId == 81)
            {
                return ShipTypeConst.TankerHazardA_Major;
            }
            else if (shipTypeId == 82)
            {
                return ShipTypeConst.TankerHazardB;
            }
            else if (shipTypeId == 83)
            {
                return ShipTypeConst.TankerHazardC_Minor;
            }
            else if (shipTypeId == 84)
            {
                return ShipTypeConst.TankerHazardD_Recognize;
            }
            else if ((shipTypeId >= 85) && (shipTypeId <= 89))
            {
                return ShipTypeConst.Tanker;
            }
            else if ((shipTypeId >= 90) && (shipTypeId <= 99))
            {
                return ShipTypeConst.Other;
            }
            //else if (shipTypeId == 100)
            //{
            //    return ShipTypeConst.NavigationAid;
            //}
            //else if (shipTypeId == 101)
            //{
            //    return ShipTypeConst.ReferencePoint;
            //}
            //else if (shipTypeId == 102)
            //{
            //    return ShipTypeConst.Racon;
            //}
            //else if (shipTypeId == 103)
            //{
            //    return ShipTypeConst.OffshoreStructure;
            //}
            //else if (shipTypeId == 104)
            //{
            //    return ShipTypeConst.Spare;
            //}
            //else if (shipTypeId == 105)
            //{
            //    return ShipTypeConst.Light_NoSectors;
            //}
            //else if (shipTypeId == 106)
            //{
            //    return ShipTypeConst.Light_Sectors;
            //}
            //else if (shipTypeId == 107)
            //{
            //    return ShipTypeConst.LeadingLightFront;
            //}
            //else if (shipTypeId == 108)
            //{
            //    return ShipTypeConst.LeadingLightRear;
            //}
            //else if (shipTypeId == 109)
            //{
            //    return ShipTypeConst.BeaconCardinal_N;
            //}
            //else if (shipTypeId == 110)
            //{
            //    return ShipTypeConst.BeaconCardinal_E;
            //}
            //else if (shipTypeId == 111)
            //{
            //    return ShipTypeConst.BeaconCardinal_S;
            //}
            //else if (shipTypeId == 112)
            //{
            //    return ShipTypeConst.BeaconCardinal_W;
            //}
            //else if (shipTypeId == 113)
            //{
            //    return ShipTypeConst.BeaconPortHand;
            //}
            //else if (shipTypeId == 114)
            //{
            //    return ShipTypeConst.BeaconStarboardHand;
            //}
            //else if (shipTypeId == 115)
            //{
            //    return ShipTypeConst.BeaconPortHand_Channel;
            //}
            //else if (shipTypeId == 116)
            //{
            //    return ShipTypeConst.BeaconStarboardHand_Channel;
            //}
            //else if (shipTypeId == 117)
            //{
            //    return ShipTypeConst.BeaconIsolatedDanger;
            //}
            //else if (shipTypeId == 118)
            //{
            //    return ShipTypeConst.BeaconSafeWater;
            //}
            //else if (shipTypeId == 119)
            //{
            //    return ShipTypeConst.BeaconSpecialMark;
            //}
            //else if (shipTypeId == 120)
            //{
            //    return ShipTypeConst.CardinalMark_N;
            //}
            //else if (shipTypeId == 121)
            //{
            //    return ShipTypeConst.CardinalMark_E;
            //}
            //else if (shipTypeId == 122)
            //{
            //    return ShipTypeConst.CardinalMark_S;
            //}
            //else if (shipTypeId == 123)
            //{
            //    return ShipTypeConst.CardinalMark_W;
            //}
            //else if (shipTypeId == 124)
            //{
            //    return ShipTypeConst.PortHandMark;
            //}
            //else if (shipTypeId == 125)
            //{
            //    return ShipTypeConst.StarboardHandMark;
            //}
            //else if (shipTypeId == 126)
            //{
            //    return ShipTypeConst.PreferredChannelPortHand;
            //}
            //else if (shipTypeId == 127)
            //{
            //    return ShipTypeConst.PreferredChannelStarboardHand;
            //}
            //else if (shipTypeId == 128)
            //{
            //    return ShipTypeConst.IsolatedDanger;
            //}
            //else if (shipTypeId == 129)
            //{
            //    return ShipTypeConst.SafeWater;
            //}
            //else if (shipTypeId == 130)
            //{
            //    return ShipTypeConst.MannedVTS;
            //}
            //else if (shipTypeId == 131)
            //{
            //    return ShipTypeConst.LightVessel;
            //}
            else
            {
                return null;
            }
        }
        public static string GetAnalogueInt_1_2(double? analogueInt, double? analogueExt1, double? analogueExt2)
        {
            if (analogueInt != null && analogueExt1 != null && analogueExt2 != null)
            {
                return $"{analogueInt}/{analogueExt1}/{analogueExt2}V";
            }
            return null;
        }
        public static string GetAtonStatus(int? health, int? light, int? racon)
        {
            string atonStatus = "";
            if (health != null)
            {
                if (health == 0)
                {
                    atonStatus += "Good Health";
                }
                else if (health == 1)
                {
                    atonStatus += "Alarm";
                }
            }
            if (light != null)
            {
                if (health != null)
                {
                    atonStatus += ",";
                }
                if (light == 0)
                {
                    atonStatus += "No light or No monitoring";
                }
                else if (light == 1)
                {
                    atonStatus += "Light On";
                }
                else if (light == 2)
                {
                    atonStatus += "Light Off";
                }
                else if (light == 3)
                {
                    atonStatus += "Light fail or at reduced range";
                }
            }
            if (racon != null)
            {
                if (light != null)
                {
                    atonStatus += ",";
                }
                if (racon == 0)
                {
                    atonStatus += "No RACON installed";
                }
                else if (racon == 1)
                {
                    atonStatus += "RACON installed but not monitored";
                }
                else if (racon == 2)
                {
                    atonStatus += "RACON operational";
                }
                else if (racon == 3)
                {
                    atonStatus += "RACON Error";
                }
            }
            return atonStatus;
        }
        public static string GetAtonTypeByAtonTypeId(int? atonTypeId)
        {
            switch (atonTypeId)
            {
                case 0:
                    return "Default, Type of AtoN not specified";
                case 1:
                    return "Reference point";
                case 2:
                    return "RACON";
                case 3:
                    return @"Fixed structures off-shore";
                case 4:
                    return "Emergency Wreck Marking Buoy";
                case 5:
                    return "Light, without sectors";
                case 6:
                    return "Light, with sectors";
                case 7:
                    return "Leading Light Front";
                case 8:
                    return "Leading Light Rear";
                case 9:
                    return "Beacon, Cardinal N";
                case 10:
                    return "Beacon, Cardinal E";
                case 11:
                    return "Beacon, Cardinal S";
                case 12:
                    return "Beacon, Cardinal W";
                case 13:
                    return "Beacon, Port hand";
                case 14:
                    return "Beacon, Starboard hand";
                case 15:
                    return "Beacon, Preferred Channel port hand";
                case 16:
                    return "Beacon, Preferred Channel starboard hand";
                case 17:
                    return "Beacon, Isolated danger";
                case 18:
                    return "Beacon, Safe water";
                case 19:
                    return "Beacon, Special mark";
                case 20:
                    return "Cardinal Mark N";
                case 21:
                    return "Cardinal Mark E";
                case 22:
                    return "Cardinal Mark S";
                case 23:
                    return "Cardinal Mark W";
                case 24:
                    return "Port hand Mark";
                case 25:
                    return "Starboard hand Mark";
                case 26:
                    return "Preferred Channel Port hand";
                case 27:
                    return "Preferred Channel Starboard hand";
                case 28:
                    return "Isolated danger";
                case 29:
                    return "Safe Water";
                case 30:
                    return "Special Mark";
                case 31:
                    return "Light Vessel/LANBY/Rigs";
                default:
                    return null;
            }
        }
        public static string GetVirtualAtonStatus(int? virtualAtonStatus)
        {

            switch (virtualAtonStatus)
            {
                case -1:
                    return null;
                case 0:
                    return "No";
                case 1:
                    return "Yes";
                default:
                    return null;
            }
        }
        public static string GetLengthXBeam(bool isTargetPositionsExist, int? dimensionA,
            int? dimensionB, int? dimensionC, int? dimentsionD, int? messageId)
        {
            if (isTargetPositionsExist)
            {
                if (dimensionA.HasValue && dimensionB.HasValue && dimensionC.HasValue && dimentsionD.HasValue)
                {
                    return (dimensionA.Value + dimensionB.Value).ToString() + "m X " +
                        (dimensionC.Value + dimentsionD.Value).ToString() + "m";
                }
                return null;
            }
            return null;
        }
        public static int GetTargetLength(int? dimensionA, int? dimensionB)
        {
            return dimensionA.Value + dimensionB.Value;
        }
        public static string GetPositionFixingDevice(int? positionFixingDevice)
        {
            if (positionFixingDevice.HasValue)
            {
                if (positionFixingDevice.Value == 0)
                {
                    return "Undefined";
                }
                else if (positionFixingDevice.Value == 1)
                {
                    return "GPS";
                }
                else if (positionFixingDevice.Value == 2)
                {
                    return "GLONASS";
                }
                else if (positionFixingDevice.Value == 3)
                {
                    return "Combined GPS/GLONASS";
                }
                else if (positionFixingDevice.Value == 4)
                {
                    return "Loran-C";
                }
                else if (positionFixingDevice.Value == 5)
                {
                    return "Chayka";
                }
                else if (positionFixingDevice.Value == 6)
                {
                    return "Integrated Navigation System";
                }
                else if (positionFixingDevice.Value == 7)
                {
                    return "Surveyed";
                }
                else if (positionFixingDevice.Value == 8)
                {
                    return "Galileo";
                }
                else if (positionFixingDevice.Value >= 9 && positionFixingDevice.Value <= 14)
                {
                    return "Not Used";
                }
                else if (positionFixingDevice.Value == 15)
                {
                    return "Internal GNSS";
                }
            }
            return "Undefined";
        }
        public static string GetOffPosition(int? offPosition)
        {
            if (offPosition == 0)
            {
                return "On Position";
            }
            else if (offPosition == 1)
            {
                return "Off Position";
            }
            return null;
        }
        public static string GetPositionAccuracy(int? positionAccuracy)
        {
            if (positionAccuracy.HasValue)
            {
                if (positionAccuracy.Value == 0)
                {
                    return "Low (> 10 m)";
                }
                return "High (<= 10 m)";
            }
            return "Low (> 10 m)";
        }
        public static List<int> GetMessageIdByTargetClass(string targetClass)
        {
            if (string.Equals(TargetClassConst.ClassA, targetClass, StringComparison.CurrentCultureIgnoreCase))
            {
                return new List<int> { 1, 2, 3 };
            }
            else if (string.Equals(TargetClassConst.ClassB, targetClass, StringComparison.CurrentCultureIgnoreCase))
            {
                return new List<int> { 18 };
            }
            else if (string.Equals(TargetClassConst.AtoN, targetClass, StringComparison.CurrentCultureIgnoreCase))
            {
                return new List<int> { 21 };
            }
            else if (string.Equals(TargetClassConst.BaseStation, targetClass, StringComparison.CurrentCultureIgnoreCase))
            {
                return new List<int> { 4 };
            }
            else if (string.Equals(TargetClassConst.Methydro, targetClass, StringComparison.CurrentCultureIgnoreCase))
            {
                return new List<int> { 8 };
            }
            else if (string.Equals(TargetClassConst.SAR, targetClass, StringComparison.CurrentCultureIgnoreCase))
            {
                return new List<int> { 9 };
            }
            else if (string.Equals(TargetClassConst.SAR_Air, targetClass, StringComparison.CurrentCultureIgnoreCase))
            {
                return new List<int> { 9 };
            }
            else if (string.Equals(TargetClassConst.SART, targetClass, StringComparison.CurrentCultureIgnoreCase))
            {
                return new List<int> { 21 };
            }
            else if (string.Equals("all", targetClass, StringComparison.CurrentCultureIgnoreCase))
            {
                return new List<int> { 1, 2, 3, 4, 9, 18, 21 };
            }
            else
            {
                return new List<int> { 0 };
            }
        }
    }
}
