﻿using System.Collections.Generic;

namespace Vtmis.Core.Common.Helpers.DDMS
{
    public class DDMSDredgingVal : DDMSBaseResult
    {
        public DDMSDredgingVal()
        {
            Hoppers = new List<int>();
        }
        public double? MaxSonarReading { get; set; }
        public double? MinSonarReading { get; set; }
        public double? ActualSonarReading { get; set; }
        public int? Supply { get; set; }
        public int? Loaded { get; set; }
        public int? Half { get; set; }
        public bool? Door { get; set; }
        public double? BatteryVoltage { get; set; }
        public int? Sonar { get; set; }
        public int NumOfHopper { get; set; }
        public ICollection<int> Hoppers { get; set; }
    }
}
