﻿using System.Collections.Generic;
using System.Linq;

namespace Vtmis.Core.Common.Helpers.DDMS
{
    public static class DDMSHelper
    {
        /// <summary>
        /// RMSTypes , (DAC,FI)
        /// </summary>
        public static Dictionary<DDMSTypes, (int, int)> _DDMSCode = new Dictionary<DDMSTypes, (int, int)>
        {
            { DDMSTypes.Dredging, (133, 13) }
        };

        public static DDMSTypes GetDDMSType(int? designatedAreaCode, int? functionalId)
        {
            if (designatedAreaCode.HasValue == false || functionalId.HasValue == false)
            {
                return DDMSTypes.None;
            }
            if (_DDMSCode.Values.Any(x => x == (designatedAreaCode, functionalId)))
            {
                return _DDMSCode.Where(x => x.Value == (designatedAreaCode, functionalId)).First().Key;
            }
            return DDMSTypes.None;
        }

        public static DDMSBaseResult DecodeDDMS(string binaryData, int? designatedAreaCode, int? functionalId)
        {
            var rmsType = GetDDMSType(designatedAreaCode, functionalId);
            if (string.IsNullOrWhiteSpace(binaryData) || designatedAreaCode == null || functionalId == null)
            {
                return null;
            }
            if (rmsType == DDMSTypes.Dredging)
            {
                return DecodeDredging(binaryData);
            }
            return null;
        }

        public static DDMSBaseResult DecodeDDMSVal(string binaryData, int? designatedAreaCode, int? functionalId)
        {
            var rmsType = GetDDMSType(designatedAreaCode, functionalId);
            if (string.IsNullOrWhiteSpace(binaryData) || designatedAreaCode == null || functionalId == null)
            {
                return null;
            }
            if (rmsType == DDMSTypes.Dredging)
            {
                return DecodeDredgingVal(binaryData);
            }
            return null;
        }
        private static DDMSDredgingVal DecodeDredgingVal(string binaryData)
        {
            var result = new DDMSDredgingVal
            {
                MaxSonarReading = DecodeMaxSonarReading(binaryData.Substring(0, 9)),
                MinSonarReading = DecodeMinSonarReading(binaryData.Substring(9, 9)),
                ActualSonarReading = DecodeActualSonarReading(binaryData.Substring(18, 9)),
                Supply = DecodeSupplyVal(binaryData.Substring(27, 1)),
                Loaded = DecodeLoadedVal(binaryData.Substring(28, 1)),
                Door = DecodeDoorVal(binaryData.Substring(29, 1)),
                BatteryVoltage = DecodeBatteryVoltage(binaryData.Substring(30, 9)),
                Sonar = DecodeSonarVal(binaryData.Substring(39, 2))
            };

            result = DecodeHopperVal(binaryData.Substring(41), result);
            return result;
        }
        private static DDMSDredging DecodeDredging(string binaryData)
        {
            var result = new DDMSDredging
            {
                MaxSonarReading = DecodeMaxSonarReading(binaryData.Substring(0, 9)),
                MinSonarReading = DecodeMinSonarReading(binaryData.Substring(9, 9)),
                ActualSonarReading = DecodeActualSonarReading(binaryData.Substring(18, 9)),
                Supply = DecodeSupply(binaryData.Substring(27, 1)),
                Loaded = DecodeLoaded(binaryData.Substring(28, 1)),
                Door = DecodeDoor(binaryData.Substring(29, 1)),
                BatteryVoltage = DecodeBatteryVoltage(binaryData.Substring(30, 9)),
                Sonar = DecodeSonar(binaryData.Substring(39, 2))
            };

            result = DecodeHopper(binaryData.Substring(41), result);
            return result;
        }
        public static string DecodeHopper(int? val)
        {
            if (val == null)
            {
                return null;
            }

            if (val == 0)
            {
                return "Open";
            }
            else if (val == 1)
            {
                return "Close";
            }
            return null;
        }
        private static DDMSDredging DecodeHopper(string binary, DDMSDredging result)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return result;
            }
            var numOfHopperBinary = binary.Substring(0, 4);
            var numOfHopper = numOfHopperBinary.FromBinaryToInt();
            result.NumOfHopper = numOfHopper;
            if (numOfHopper > 0)
            {
                var startIndex = 4;
                for (int i = 0; i < numOfHopper; i++)
                {
                    var hopperValue = binary.Substring(startIndex, 1);
                    if (hopperValue == "0")
                    {
                        result.Hoppers.Add("Open");
                    }
                    else if (hopperValue == "1")
                    {
                        result.Hoppers.Add("Close");
                    }
                    startIndex += 1;
                }
            }
            return result;
        }

        private static DDMSDredgingVal DecodeHopperVal(string binary, DDMSDredgingVal result)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return result;
            }
            var numOfHopperBinary = binary.Substring(0, 4);
            var numOfHopper = numOfHopperBinary.FromBinaryToInt();
            result.NumOfHopper = numOfHopper;
            if (numOfHopper > 0)
            {
                var startIndex = 4;
                for (int i = 0; i < numOfHopper; i++)
                {
                    var hopperValue = binary.Substring(startIndex, 1);
                    if (hopperValue == "0")
                    {
                        result.Hoppers.Add(0);
                    }
                    else if (hopperValue == "1")
                    {
                        result.Hoppers.Add(1);
                    }
                    startIndex += 1;
                }
            }
            return result;
        }

        private static string DecodeSonar(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            var bin = binary.FromBinaryToInt();
            if (bin == 0)
            {
                return "Disabled";
            }
            else if (bin == 1)
            {
                return "Enabled";
            }
            return null;
        }

        public static string DecodeSonar(short? val)
        {
            if (val == null)
            {
                return null;
            }

            if (val == 0)
            {
                return "Disabled";
            }
            else if (val == 1)
            {
                return "Enabled";
            }
            return null;
        }

        private static int? DecodeSonarVal(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            return binary.FromBinaryToInt();
        }

        private static double? DecodeBatteryVoltage(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            double result = binary.FromBinaryToDouble();
            var r = result * 0.05;
            return r.ToDecimalPlace(2);
        }

        private static string DecodeDoor(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            if (binary == "0")
            {
                return "Close";
            }
            else if (binary == "1")
            {
                return "Open";
            }
            return null;
        }

        public static string DecodeDoor(bool? val)
        {
            if (val == null)
            {
                return null;
            }
            if (val == false)
            {
                return "Close";
            }
            else if (val == true)
            {
                return "Open";
            }
            return null;
        }

        private static bool? DecodeDoorVal(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            return binary.FromBinaryToInt() == 1;
        }

        private static string DecodeLoaded(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            if (binary == "0")
            {
                return "Loaded";
            }
            else if (binary == "1")
            {
                return "Empty";
            }
            return null;
        }

        public static string DecodeLoaded(bool? val)
        {
            if (val == null)
            {
                return null;
            }
            if (val == false)
            {
                return "Loaded";
            }
            else if (val == true)
            {
                return "Empty";
            }
            return null;
        }

        private static int? DecodeLoadedVal(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            return binary.FromBinaryToInt();
        }

        private static string DecodeSupply(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            if (binary == "0")
            {
                return "Battery";
            }
            else if (binary == "1")
            {
                return "Powered";
            }
            return null;
        }

        public static string DecodeSupply(bool? val)
        {
            if (val == null)
            {
                return null;
            }
            if (val == false)
            {
                return "Battery";
            }
            else if (val == true)
            {
                return "Powered";
            }
            return null;
        }

        private static int? DecodeSupplyVal(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            return binary.FromBinaryToInt();
        }

        private static double? DecodeActualSonarReading(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            var r = binary.FromBinaryToDouble() * 20;
            var result = r / 1000;
            return result.ToDecimalPlace(2);
        }

        private static double? DecodeMinSonarReading(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            var r = binary.FromBinaryToDouble() * 20;
            var result = r / 1000;
            return result.ToDecimalPlace(2);
        }


        private static double? DecodeMaxSonarReading(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            var r = binary.FromBinaryToDouble() * 20;
            var result = r / 1000;
            return result.ToDecimalPlace(2);
        }
    }
}
