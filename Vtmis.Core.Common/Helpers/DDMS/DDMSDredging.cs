﻿using System.Collections.Generic;

namespace Vtmis.Core.Common.Helpers.DDMS
{
    public class DDMSDredging : DDMSBaseResult
    {
        public DDMSDredging()
        {
            Hoppers = new List<string>();
        }
        public double? MaxSonarReading { get; set; }
        public double? MinSonarReading { get; set; }
        public double? ActualSonarReading { get; set; }
        public string Supply { get; set; }
        public string Loaded { get; set; }
        public string Door { get; set; }
        public double? BatteryVoltage { get; set; }
        public string Sonar { get; set; }
        public int NumOfHopper { get; set; }
        public ICollection<string> Hoppers { get; set; }
    }
}
