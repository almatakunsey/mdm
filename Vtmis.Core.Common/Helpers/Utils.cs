﻿using System;
using System.Collections.Generic;

namespace Vtmis.Core.Common.Helpers
{
    public static class Utils
    {
        public static DateTime ConvertToUTC(this DateTime val)
        {
            return DateTime.SpecifyKind(val, DateTimeKind.Local).ToUniversalTime();
        }
        public static bool IsEqual(this string val, string valToCompare)
        {
            return string.Equals(val, valToCompare, StringComparison.CurrentCultureIgnoreCase);
        }

        public static List<T> JsonArrayToList<T>(this string val)
        {
            return Newtonsoft.Json.JsonConvert
                           .DeserializeObject<List<T>>(val);
        }
        public static decimal NMToMeters(this decimal val)
        {
            return val * 1852;
        }

        // (1 = A, 2 = B...27 = AA...703 = AAA...)
        public static string ToChar(this int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        public static string ToStringOrDefault(this object val)
        {
            if (val == null)
            {
                return null;
            }
            return val.ToString();
        }

        public static string ToStringOrDefault(this object val, bool decimalPlace)
        {
            if (val == null)
            {
                return null;
            }
            if (decimalPlace)
            {
                return string.Format("{0:0.00}", val);
            }
            return val.ToString();
        }

        public static int ToInt(this string value)
        {
            return Convert.ToInt32(value);
        }

        public static double? ToDoubleOrDefault(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }
            return Convert.ToDouble(value);
        }

        public static DateTime ToDateTime(this string value)
        {
            return Convert.ToDateTime(value);
        }

        public static string RadianToDegree(this double angle)
        {
            //easy to customize by changing the number of directions you have 
            var directions = 4;
            var d = angle * (180.0 / Math.PI);
            var degree = 360 / directions;
            var resultDegree = d + degree / 2;

            if (resultDegree >= 0 * degree && resultDegree < 1 * degree)
            {
                return $"{d}'N";
            }

            if (resultDegree >= 1 * degree && resultDegree < 2 * degree)
                return $"{d}'E";
            if (resultDegree >= 2 * degree && resultDegree < 3 * degree)
                return $"{d}'S";
            if (resultDegree >= 3 * degree && resultDegree < 4 * degree)
                return $"{d}'W";

            return $"{d}'N";
        }

        /// <summary>
        /// Convert integer to binary
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string ToBinary(this int val)
        {
            if (val < 0)
            {
                val = val * (-1);
            }
            int remainder;
            string result = string.Empty;
            while (val > 0)
            {
                remainder = val % 2;
                val /= 2;
                result = remainder.ToString() + result;
            }
            if (result.Length < 8)
            {
                var count = 8 - result.Length;
                for (int i = 0; i < count; i++)
                {
                    result = $"0{result}";
                }
            }
            return result;
        }
        /// <summary>
        /// Convert integer to binary
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string ToBinary(this int val, int bitLength)
        {
            if (val < 0)
            {
                val = val * (-1);
            }
            int remainder;
            string result = string.Empty;
            while (val > 0)
            {
                remainder = val % 2;
                val /= 2;
                result = remainder.ToString() + result;
            }
            if (result.Length < bitLength)
            {
                var count = bitLength - result.Length;
                for (int i = 0; i < count; i++)
                {
                    result = $"0{result}";
                }
            }
            return result;
        }

        /// <summary>
        /// Convert integer to binary
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string ToBinary(this short? val)
        {
            if (val == null)
            {
                return null;
            }
            if (val < 0)
            {
                val = (short?)(val * (-1));
            }
            short? remainder;
            string result = string.Empty;
            while (val > 0)
            {
                remainder = (short?)(val % 2);
                val /= 2;
                result = remainder.ToString() + result;
            }
            if (result.Length < 8)
            {
                var count = 8 - result.Length;
                for (int i = 0; i < count; i++)
                {
                    result = $"0{result}";
                }
            }
            return result;
        }

        /// <summary>
        /// Convert integer to binary
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static string ToBinary(this short? val, int bitLength)
        {
            if (val == null)
            {
                return null;
            }
            if (val < 0)
            {
                val = (short?)(val * (-1));
            }
            short? remainder;
            string result = string.Empty;
            while (val > 0)
            {
                remainder = (short?)(val % 2);
                val /= 2;
                result = remainder.ToString() + result;
            }
            if (result.Length < bitLength)
            {
                var count = bitLength - result.Length;
                for (int i = 0; i < count; i++)
                {
                    result = $"0{result}";
                }
            }
            return result;
        }

        /// <summary>
        /// Convert binary to integer (For RMS use only)
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static int FromBinaryToInt(this string val)
        {
            var result = Convert.ToInt32(val, 2);
            return result;
        }

        /// <summary>
        /// Convert binary to short (For RMS use only)
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static short FromBinaryToShort(this string val)
        {
            var result = Convert.ToInt32(val, 2);
            return (short)result;
        }
        /// <summary>
        /// Convert binary to double
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static double FromBinaryToDouble(this string val)
        {
            var result = Convert.ToInt32(val, 2);
            return Convert.ToDouble(result);
        }

        public static double ToDecimalPlace(this double r, int d)
        {
            return Math.Round(r, d);
        }

        public static decimal ToDecimalPlace(this decimal r, int d)
        {
            return Math.Round(r, d);
        }

        public static double? ToDecimalPlaceOrDefault(this double? r, int d)
        {
            if (r == null)
            {
                return null;
            }
            return Math.Round(r.Value, d);
        }

        public static double? ToDecimalPlaceOrDefault(this float? r, int d)
        {
            if(r == null)
            {
                return null;
            }
            var result = Math.Round(r.Value, d, MidpointRounding.AwayFromZero);
            return result;
        }

        public static decimal? ToDecimalPlaceOrDefault(this decimal? r, int d)
        {
            if(r == null)
            {
                return null;
            }
            return Math.Round(r.Value, d);
        }
    }
}
