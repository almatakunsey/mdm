﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Vtmis.Core.Common.Helpers.Methydro
{
    /// <summary>
    /// Two's complement
    /// first digit represent positive negative ( 1= negative, 0 = positive)
    /// the remaining convert to decimal
    /// if all digit is equal to 1. change all of it to 0 and then add 1
    /// the first digit will represent its positive negative
    /// </summary>
    public static class MethydroHelper
    {
        /// <summary>
        /// RMSTypes , (DAC,FI)
        /// </summary>
        public static Dictionary<MethydroTypes, (int, int)> _MethydroCodes = new Dictionary<MethydroTypes, (int, int)>
        {
            { MethydroTypes.Met111, (1, 11) },
            { MethydroTypes.Met131, (1, 31) }
        };
        public static MethydroTypes GetMethydroType(int? designatedAreaCode, int? functionalId)
        {
            if (designatedAreaCode.HasValue == false || functionalId.HasValue == false)
            {
                return MethydroTypes.None;
            }
            if (_MethydroCodes.Values.Any(x => x == (designatedAreaCode, functionalId)))
            {
                return _MethydroCodes.Where(x => x.Value == (designatedAreaCode, functionalId)).First().Key;
            }
            return MethydroTypes.None;
        }

        public static MethydroResult DecodeMethydro(string binaryData, int? designatedAreaCode, int? functionalId)
        {
            var methydroType = GetMethydroType(designatedAreaCode, functionalId);
            if (string.IsNullOrWhiteSpace(binaryData) || designatedAreaCode == null || functionalId == null)
            {
                return null;
            }
            if (methydroType == MethydroTypes.Met111 || methydroType == MethydroTypes.Met131)
            {
                return new MethydroResult
                {
                    AverageWindSpeed = DecodeAverageWindSpeed(binaryData, designatedAreaCode, functionalId),
                    WindGust = DecodeWindGust(binaryData, designatedAreaCode, functionalId),
                    WindDirection = DecodeWindDirection(binaryData, designatedAreaCode, functionalId),
                    WindGustDirection = DecodeGustDirection(binaryData, designatedAreaCode, functionalId),
                    AirTemperature = DecodeAirTemperature(binaryData, designatedAreaCode, functionalId),
                    RelativeHumidity = DecodeRelativeHumidity(binaryData, designatedAreaCode, functionalId),
                    DewPoint = DecodeDewPoint(binaryData, designatedAreaCode, functionalId),
                    AirPressure = DecodeAirPressure(binaryData, designatedAreaCode, functionalId),
                    AirPressureTendency = DecodeAirPressureTendency(binaryData, designatedAreaCode, functionalId),
                    HorizontalVisibility = DecodeHorizontalVisibility(binaryData, designatedAreaCode, functionalId),
                    WaterLevel = DecodeWaterLevel(binaryData, designatedAreaCode, functionalId),
                    WaterLevelTrend = DecodeWaterLevelTrend(binaryData, designatedAreaCode, functionalId),
                    SurfaceCurrentSpeed = DecodeSurfaceCurrentSpeed(binaryData, designatedAreaCode, functionalId),
                    SurfaceCurrentDirection = DecodeSurfaceCurrentDirection(binaryData, designatedAreaCode, functionalId),
                    SurfaceCurrentSpeed2 = DecodeSurfaceCurrentSpeed2(binaryData, designatedAreaCode, functionalId),
                    SurfaceCurrentDirection2 = DecodeSurfaceCurrentDirection2(binaryData, designatedAreaCode, functionalId),
                    SurfaceMeasuringLevel2 = DecodeSurfaceMeasuringLevel2(binaryData, designatedAreaCode, functionalId),
                    SurfaceCurrentSpeed3 = DecodeSurfaceCurrentSpeed3(binaryData, designatedAreaCode, functionalId),
                    SurfaceCurrentDirection3 = DecodeSurfaceCurrentDirection3(binaryData, designatedAreaCode, functionalId),
                    SurfaceMeasuringLevel3 = DecodeSurfaceMeasuringLevel3(binaryData, designatedAreaCode, functionalId),
                    SignificantWaveHeight = DecodeSignificantWaveHeight(binaryData, designatedAreaCode, functionalId),
                    WavePeriod = DecodeWavePeriod(binaryData, designatedAreaCode, functionalId),
                    WaveDirection = DecodeWaveDirection(binaryData, designatedAreaCode, functionalId),
                    SwellHeight = DecodeSwellHeight(binaryData, designatedAreaCode, functionalId),
                    SwellPeriod = DecodeSwellPeriod(binaryData, designatedAreaCode, functionalId),
                    SwellDirection = DecodeSwellDirection(binaryData, designatedAreaCode, functionalId),
                    SeaState = DecodeSeaState(binaryData, designatedAreaCode, functionalId),
                    WaterTemperature = DecodeWaterTemperature(binaryData, designatedAreaCode, functionalId),
                    Precipitation = DecodePrecipitation(binaryData, designatedAreaCode, functionalId),
                    Salinity = DecodeSalinity(binaryData, designatedAreaCode, functionalId),
                    Ice = DecodeIce(binaryData, designatedAreaCode, functionalId)
                };
            }
            return null;
        }

        public static string DecodeIce(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(291, 2);
            }
            else
            {
                binary = binary.Substring(292, 2);
            }
            var c = binary.FromBinaryToInt();
            if (c == 0)
            {
                return "No";
            }
            else if (c == 1)
            {
                return "Yes";
            }
            else if (c == 2)
            {
                return "(reserved for future use)";
            }
            return null;
        }

        public static double? DecodeSalinity(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(282, 9);
            }
            else
            {
                binary = binary.Substring(283, 9);
            }
            var c = binary.FromBinaryToDouble();
            if (c >= 0 && c <= 501)
            {
                double result = (double)c / 10;
                return result.ToDecimalPlace(2);
            }
            return null;
        }

        public static string DecodePrecipitation(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(279, 3);
            }
            else
            {
                binary = binary.Substring(280, 3);
            }
            var c = binary.FromBinaryToInt();
            if (c == 0)
            {
                return "reserved";
            }
            else if (c == 1)
            {
                return "rain";
            }
            else if (c == 2)
            {
                return "thunderstorm";
            }
            else if (c == 3)
            {
                return "freezing rain";
            }
            else if (c == 4)
            {
                return "mixed/ice";
            }
            else if (c == 5)
            {
                return "snow";
            }
            else if (c == 6)
            {
                return "reserved";
            }
            return null;
        }

        public static double? DecodeWaterTemperature(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }

            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(269, 10);
            }
            else
            {
                binary = binary.Substring(270, 10);
            }

            var positiveNegative = binary.Substring(0, 1);
            var invertedBinary = string.Empty;          
            foreach (var b in binary)
            {
                if (b == '0')
                {
                    invertedBinary = $"{invertedBinary}1";
                }
                else
                {
                    invertedBinary = $"{invertedBinary}0";
                }
            }
            var invertedInt = (invertedBinary.FromBinaryToDouble() + 1) * 0.1;
            if (positiveNegative == "1")
            {
                if (invertedInt < 10)
                {
                    return null;
                }
                return (-invertedInt.ToDecimalPlace(2));
            }
            if (invertedInt > 50)
            {
                return null;
            }
            return invertedInt.ToDecimalPlace(2);
        }

        public static string DecodeSeaState(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(265, 4);
            }
            else
            {
                binary = binary.Substring(266, 4);
            }
            var c = binary.FromBinaryToInt();
            if (c == 0)
            {
                return "Flat";
            }
            else if (c == 1)
            {
                return "Ripples without crests";
            }
            else if (c == 2)
            {
                return "Small wavelets. Crests of glassy appearance, not breaking";
            }
            else if (c == 3)
            {
                return "Large wavelets. Crests begin to break; scattered whitecaps";
            }
            else if (c == 4)
            {
                return "Small waves";
            }
            else if (c == 5)
            {
                return "Moderate (1.2 m) longer waves. Some foam and spray";
            }
            else if (c == 6)
            {
                return "Large waves with foam crests and some spray";
            }
            else if (c == 7)
            {
                return "Sea heaps up and foam begins to streak.";
            }
            else if (c == 8)
            {
                return "Moderately high waves with breaking crests forming spindrift. Streaks of foam.";
            }
            else if (c == 9)
            {
                return "High waves (6-7 m) with dense foam. Wave crests start to roll over. Considerable spray.";
            }
            else if (c == 10)
            {
                return "Very high waves. The sea surface is white and there is considerable tumbling. Visibility is reduced.";
            }
            else if (c == 11)
            {
                return "Exceptionally high waves.";
            }
            else if (c == 12)
            {
                return "Huge waves. Air filled with foam and spray. Sea completely white with driving spray. Visibility greatly reduced";
            }
            else
            {
                return null;
            }
        }

        public static double? DecodeSwellDirection(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(256, 9);
            }
            else
            {
                binary = binary.Substring(257, 9);
            }
            var c = binary.FromBinaryToInt();
            if (c >= 0 && c <= 359)
            {
                return c;
            }
            return null;
        }
        public static double? DecodeSwellPeriod(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }



            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(250, 6);
            }
            else
            {
                binary = binary.Substring(251, 6);
            }
            var c = binary.FromBinaryToInt();
            if (c >= 0 && c <= 60)
            {
                return c;
            }
            return null;
        }
        public static double? DecodeSwellHeight(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(242, 8);
            }
            else
            {
                binary = binary.Substring(243, 8);
            }
            var c = binary.FromBinaryToDouble();
            if (c >= 0 && c <= 251)
            {
                var r = (double)c / 10;
                return r.ToDecimalPlace(2);
            }
            return null;
        }

        public static double? DecodeWaveDirection(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(233, 9);
            }
            else
            {
                binary = binary.Substring(234, 9);
            }
            var c = binary.FromBinaryToInt();
            if (c >= 0 && c <= 359)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeWavePeriod(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(227, 6);
            }
            else
            {
                binary = binary.Substring(228, 6);
            }
            var c = binary.FromBinaryToInt();
            if (c >= 0 && c <= 60)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeSignificantWaveHeight(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(219, 8);
            }
            else
            {
                binary = binary.Substring(220, 8);
            }
            var c = binary.FromBinaryToDouble();
            if (c >= 0 && c <= 251)
            {
                var r = (double)c / 10;
                return r.ToDecimalPlace(2);
            }
            return null;
        }
        public static double? DecodeSurfaceMeasuringLevel3(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(214, 5);
            }
            else
            {
                binary = binary.Substring(215, 5);
            }
            var c = binary.FromBinaryToInt();
            if (c >= 0 && c <= 30)
            {
                return c;
            }
            return null;
        }
        public static double? DecodeSurfaceCurrentDirection3(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(205, 9);
            }
            else
            {
                binary = binary.Substring(206, 9);
            }
            var c = binary.FromBinaryToInt();
            if (c >= 0 && c <= 359)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeSurfaceCurrentSpeed3(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(197, 8);
            }
            else
            {
                binary = binary.Substring(198, 8);
            }
            var c = binary.FromBinaryToDouble();

            if (c >= 0 && c <= 251)
            {
                var r = (double)c / 10;
                return r.ToDecimalPlace(2);
            }
            return null;
        }

        public static double? DecodeSurfaceMeasuringLevel2(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(192, 5);
            }
            else
            {
                binary = binary.Substring(193, 5);
            }
            var c = binary.FromBinaryToInt();
            if (c >= 0 && c <= 30)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeSurfaceCurrentDirection2(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(183, 9);
            }
            else
            {
                binary = binary.Substring(184, 9);
            }
            var c = binary.FromBinaryToInt();
            if (c >= 0 && c <= 359)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeSurfaceCurrentSpeed2(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(175, 8);
            }
            else
            {
                binary = binary.Substring(176, 8);
            }
            var c = binary.FromBinaryToDouble();

            if (c >= 0 && c <= 251)
            {
                var r = (double)c / 10;
                return r.ToDecimalPlace(2);
            }
            return null;
        }

        public static double? DecodeSurfaceCurrentDirection(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(166, 9);
            }
            else
            {
                binary = binary.Substring(167, 9);
            }
            var c = binary.FromBinaryToInt();
            if (c >= 0 && c <= 359)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeSurfaceCurrentSpeed(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(158, 8);
            }
            else
            {
                binary = binary.Substring(159, 8);
            }
            var c = binary.FromBinaryToDouble();

            if (c >= 0 && c <= 251)
            {
                var r = (double)c / 10;
                return r.ToDecimalPlace(2);
            }
            return null;
        }

        public static string DecodeWaterLevelTrend(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(156, 2);
            }
            else
            {
                binary = binary.Substring(157, 2);
            }
            var c = binary.FromBinaryToInt();
            if (c == 0)
            {
                return "Steady";
            }
            else if (c == 1)
            {
                return "Decreasing";
            }
            else if (c == 2)
            {
                return "Increasing";
            }
            else
            {
                return null;
            }
        }

        public static double? DecodeWaterLevel(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(144, 12);
            }
            else
            {
                binary = binary.Substring(145, 12);
            }
            double c = Convert.ToDouble(binary.FromBinaryToInt());

            if (c >= 0 && c <= 4000)
            {

                var r = ((double)c / 100) - 10;
                return r.ToDecimalPlace(1);
            }
            return null;
        }

        public static double? DecodeHorizontalVisibility(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(136, 8);
            }
            else
            {
                binary = binary.Substring(137, 8);
            }
            var c = binary.FromBinaryToDouble();

            if (c >= 0 && c <= 126)
            {
                var r = (double)c / 10;
                return r.ToDecimalPlace(2);
            }
            return null;
        }

        public static string DecodeAirPressureTendency(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(134, 2);
            }
            else
            {
                binary = binary.Substring(135, 2);
            }
            var c = binary.FromBinaryToInt();

            if (c == 0)
            {
                return "Steady";
            }
            else if (c == 1)
            {
                return "Decreasing";
            }
            else if (c == 2)
            {
                return "Increasing";
            }
            else
            {
                return null;
            }
        }

        public static double? DecodeAirPressure(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(125, 9);
            }
            else
            {
                binary = binary.Substring(126, 9);
            }
            var c = binary.FromBinaryToInt();

            if (c <= 401)
            {
                return c + (800 - 1);
            }
            return null;
        }

        public static double? DecodeDewPoint(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(115, 10);
            }
            else
            {
                binary = binary.Substring(116, 10);
            }
            var c = binary.FromBinaryToInt();

            if (c >= 0 && c <= 100)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeRelativeHumidity(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(108, 7);
            }
            else
            {
                binary = binary.Substring(109, 7);
            }
            var c = binary.FromBinaryToInt();

            if (c >= 0 && c <= 100)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeAirTemperature(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(97, 11);
            }
            else
            {
                binary = binary.Substring(98, 11);
            }
            var c = binary.FromBinaryToDouble();
            if (c >= 0 && c <= 600)
            {
                double result = (double)c / 10;
                return result.ToDecimalPlace(2);
            }
            return null;
        }

        public static double? DecodeGustDirection(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(88, 9);
            }
            else
            {
                binary = binary.Substring(89, 9);
            }
            var c = binary.FromBinaryToInt();

            if (c >= 0 && c <= 359)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeWindDirection(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(79, 9);
            }
            else
            {
                binary = binary.Substring(80, 9);
            }

            var c = binary.FromBinaryToInt();

            if (c >= 0 && c <= 359)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeWindGust(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }


            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(72, 7);
            }
            else
            {
                binary = binary.Substring(73, 7);
            }

            var c = binary.FromBinaryToInt();

            if (c >= 0 && c <= 126)
            {
                return c;
            }
            return null;
        }

        public static double? DecodeAverageWindSpeed(string binary, int? dac, int? fi)
        {
            var metType = GetMethydroType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || metType == MethydroTypes.None)
            {
                return null;
            }

            if (metType == MethydroTypes.Met111)
            {
                binary = binary.Substring(65, 7);
            }
            else
            {
                binary = binary.Substring(66, 7);
            }

            var c = binary.FromBinaryToInt();

            if (c >= 0 && c <= 126)
            {
                return c;
            }
            return null;
        }
    }
}
