﻿namespace Vtmis.Core.Common.Helpers.Methydro
{
    public enum MethydroTypes
    {
        Met131, // DAC (1) || FI (31)
        Met111, // DAC (1) || FI (11)
        None
    }
}
