﻿namespace Vtmis.Core.Common.Helpers.Methydro
{
    public class MethydroResult
    {
        public double? AverageWindSpeed { get; set; }
        public double? WindGust { get; set; }
        public double? WindDirection { get; set; }
        public double? WindGustDirection { get; set; }
        public double? AirTemperature { get; set; }
        public double? RelativeHumidity { get; set; }
        public double? DewPoint { get; set; }
        public double? AirPressure { get; set; }
        public string AirPressureTendency { get; set; }
        public double? HorizontalVisibility { get; set; }
        public double? WaterLevel { get; set; }
        public string WaterLevelTrend { get; set; }
        public double? SurfaceCurrentSpeed { get; set; }
        public double? SurfaceCurrentDirection { get; set; }
        public double? SurfaceCurrentSpeed2 { get; set; }
        public double? SurfaceCurrentDirection2 { get; set; }
        public double? SurfaceMeasuringLevel2 { get; set; }
        public double? SurfaceCurrentSpeed3 { get; set; }
        public double? SurfaceCurrentDirection3 { get; set; }
        public double? SurfaceMeasuringLevel3 { get; set; }
        public double? SignificantWaveHeight { get; set; }
        public double? WavePeriod { get; set; }
        public double? WaveDirection { get; set; }
        public double? SwellHeight { get; set; }
        public double? SwellPeriod { get; set; }
        public double? SwellDirection { get; set; }
        public string SeaState { get; set; }
        public double? WaterTemperature { get; set; }
        public string Precipitation { get; set; }
        public double? Salinity { get; set; }
        public string Ice { get; set; }
    }
}
