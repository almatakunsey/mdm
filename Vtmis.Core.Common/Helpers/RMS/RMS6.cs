﻿using System;

namespace Vtmis.Core.Common.Helpers.RMS
{
    public class RMS6
    {
        public Single? AnalogueInt { get; set; }
        public Single? AnalogueExt1 { get; set; }
        public Single? AnalogueExt2 { get; set; }
        public Int16? DigitalInput { get; set; }
        public Int16? OffPosition68 { get; set; }
        public Int16? Health { get; set; }
        public Int16? Light { get; set; }
        public Int16? RACON { get; set; }
        public DateTime RecvTime { get; set; }
        public Int16? DAC { get; set; }
        public Int16? FI { get; set; }
        public Single? VoltageData { get; set; }
        public Single? CurrentData { get; set; }
        public Int16? Battery { get; set; }
        public Int16? PowerSupplyType { get; set; }
    }
}
