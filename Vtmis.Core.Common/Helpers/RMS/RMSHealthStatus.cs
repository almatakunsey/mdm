﻿namespace Vtmis.Core.Common.Helpers.RMS
{
    public class RMSHealthStatus
    {
        public string Health { get; set; }
        public string Light { get; set; }
        public string Racon { get; set; }
    }
}
