﻿namespace Vtmis.Core.Common.Helpers.RMS
{
    public class RMSZeniLite : RMSBaseResult
    {
        public double? VoltageData { get; set; }
        public double? CurrentData { get; set; }
        public string PowerSupplyType { get; set; }
        public string LightStatus { get; set; }
        public string BatteryStatus { get; set; }
       
    }
}
