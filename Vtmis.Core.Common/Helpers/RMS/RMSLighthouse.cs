﻿namespace Vtmis.Core.Common.Helpers.RMS
{
    public class RMSLighthouse : RMSBaseResult
    {
        public string LightSensor { get; set; }
        public string Beat { get; set; }
        public string MainLanternCondition { get; set; }
        public string MainLanternStatus { get; set; }
        public string StandbyLanternCondition { get; set; }
        public string StandbyLanternStatus { get; set; }
        public string EmergencyLanternCondition { get; set; }
        public string EmergencyLanternStatus { get; set; }
        public string OpticDriveAStatus { get; set; }
        public string OpticDriveACondition { get; set; }
        public string OpticDriveBStatus { get; set; }
        public string OpticDriveBCondition { get; set; }
        public string HatchDoor { get; set; }
        public string MainPower { get; set; }
        public string BMSCondition { get; set; } // Regulator or Battery Management System
    }
}
