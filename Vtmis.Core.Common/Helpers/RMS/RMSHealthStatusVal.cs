﻿namespace Vtmis.Core.Common.Helpers.RMS
{
    public class RMSHealthStatusVal
    {
        public double? Health { get; set; }
        public double? Light { get; set; }
        public double? Racon { get; set; }
    }
}
