﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Vtmis.Core.Common.Helpers.RMS
{
    public static class RMSHelper
    {
        public static string GetBattery(int? val)
        {
            if (val == 0)
            {
                return "Good";
            }
            if (val == 1)
            {
                return "Low";
            }
            return null;
        }
        public static string GetHealth(int? val)
        {
            if (val == 0)
            {
                return "Good Health";
            }
            if (val == 1)
            {
                return "Alarm";
            }
            return null;
        }
        public static string GetLight(int? val)
        {
            if (val == 0)
            {
                return "No Light or No Monitoring";
            }
            if (val == 1)
            {
                return "Light On";
            }
            if (val == 2)
            {
                return "Light Off";
            }
            if (val == 3)
            {
                return "Light Error";
            }
            return null;
        }
        public static string GetRacon(int? val)
        {
            if (val == 0)
            {
                return "No RACON Installed";
            }
            if (val == 1)
            {
                return "RACON Not Monitored";
            }
            if (val == 2)
            {
                return "RACON Operational";
            }
            if (val == 3)
            {
                return "RACON Error";
            }
            return null;
        }
        public static string GetOffPosition(int? offPosition)
        {
            if (offPosition == 0)
            {
                return "On Position";
            }
            else if (offPosition == 1)
            {
                return "Off Position";
            }
            return null;
        }
        public static string GetAtonStatus(int? health, int? light, int? racon)
        {
            string atonStatus = "";
            if (health != null)
            {
                if (health == 0)
                {
                    atonStatus += "Good Health";
                }
                else if (health == 1)
                {
                    atonStatus += "Alarm";
                }
            }
            if (light != null)
            {
                if (health != null)
                {
                    atonStatus += ",";
                }
                if (light == 0)
                {
                    atonStatus += "No light or No monitoring";
                }
                else if (light == 1)
                {
                    atonStatus += "Light On";
                }
                else if (light == 2)
                {
                    atonStatus += "Light Off";
                }
                else if (light == 3)
                {
                    atonStatus += "Light fail or at reduced range";
                }
            }
            if (racon != null)
            {
                if (light != null)
                {
                    atonStatus += ",";
                }
                if (racon == 0)
                {
                    atonStatus += "No RACON installed";
                }
                else if (racon == 1)
                {
                    atonStatus += "RACON installed but not monitored";
                }
                else if (racon == 2)
                {
                    atonStatus += "RACON operational";
                }
                else if (racon == 3)
                {
                    atonStatus += "RACON Error";
                }
            }
            return atonStatus;
        }
        /// <summary>
        /// RMSTypes , (DAC,FI)
        /// </summary>
        public static Dictionary<RMSTypes, (int, int)> _RMSCode = new Dictionary<RMSTypes, (int, int)>
        {
            { RMSTypes.LightBeaconApplication, (533, 1) },
            { RMSTypes.IALA, (235, 10) },
            { RMSTypes.LighthouseRenewableEnergy, (533, 2) },
            { RMSTypes.SelfContainedLantern, (533, 4) },
             { RMSTypes.ZeniLiteBuoys, (0, 0) }
        };
        public static RMSTypes GetRMSType(int? designatedAreaCode, int? functionalId)
        {
            if (designatedAreaCode.HasValue == false || functionalId.HasValue == false)
            {
                return RMSTypes.None;
            }
            if (_RMSCode.Values.Any(x => x == (designatedAreaCode, functionalId)))
            {
                return _RMSCode.Where(x => x.Value == (designatedAreaCode, functionalId)).First().Key;
            }
            return RMSTypes.None;
        }

        public static RMSBaseResult DecodeRMS(string binaryData, int? designatedAreaCode, int? functionalId)
        {
            var rmsType = GetRMSType(designatedAreaCode, functionalId);
            if (string.IsNullOrWhiteSpace(binaryData) || designatedAreaCode == null || functionalId == null)
            {
                return null;
            }
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                return DecodeLightBeaconApp(binaryData);
            }
            else if (rmsType == RMSTypes.IALA)
            {
                return DecodeIALA(binaryData);
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                return DecodeLighthouse(binaryData);
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                return DecodeSelfContainedLantern(binaryData);
            }
            else if (rmsType == RMSTypes.ZeniLiteBuoys)
            {
                return DecodeZeniLite(binaryData);
            }
            else
            {
                return null;
            }
        }

        private static RMSBaseResult DecodeZeniLite(string binaryData)
        {
            return new RMSZeniLite
            {
                VoltageData = DecodeVoltageData(binaryData.Substring(16, 12)),
                CurrentData = DecodeCurrentData(binaryData.Substring(28, 10)),
                PowerSupplyType = DecodePowerSupplyType(binaryData.Substring(38, 1)),
                LightStatus = DecodeLightStatus(binaryData.Substring(39, 1)),
                BatteryStatus = DecodeBatteryStatus(binaryData.Substring(40, 1)),
                OffPosition = DecodeOffPosition(binaryData.Substring(41, 1))
            };
        }

        private static RMSBaseResult DecodeSelfContainedLantern(string binaryData)
        {
            return new RMSSelfContainedLantern
            {
                SupplyVoltageInt = DecodeVoltageInt(binaryData.Substring(0, 9)).ToStringOrDefault(true),
                SupplyVoltageExt1 = DecodeVoltageExternal1(binaryData.Substring(9, 9)).ToStringOrDefault(true),
                SupplyVoltageExt2 = DecodeVoltageExternal2(binaryData.Substring(18, 9)).ToStringOrDefault(true),
                OffPosition = DecodeOffPosition(binaryData.Substring(27, 1)),
                LightSensor = DecodeAmbient(binaryData.Substring(28, 2)),
                Status = DecodeStatus(binaryData.Substring(30, 5)),
                Beat = DecodeBeat(binaryData.Substring(35, 1)),
                AlarmActive = DecodeSelfLantern(binaryData.Substring(36, 1)),
                FlasherOffPowerThres = DecodeSelfLantern(binaryData.Substring(37, 1)),
                ErrorLedOpen = DecodeSelfLantern(binaryData.Substring(38, 1)),
                ErrorLedShort = DecodeSelfLantern(binaryData.Substring(39, 1)),
                ErrorLedVoltageLow = DecodeSelfLantern(binaryData.Substring(40, 1)),
                ErrorPowerThres = DecodeSelfLantern(binaryData.Substring(41, 1)),
                ErrorVinLow = DecodeSelfLantern(binaryData.Substring(42, 1)),
                FlasherAdjToLed = DecodeSelfLantern(binaryData.Substring(43, 1)),
                FlasherOffForceOff = DecodeSelfLantern(binaryData.Substring(44, 1)),
                FlasherOffLowVin = DecodeSelfLantern(binaryData.Substring(45, 1)),
                FlasherOffPhotoCell = DecodeSelfLantern(binaryData.Substring(46, 1)),
                FlasherOffTemperature = DecodeSelfLantern(binaryData.Substring(47, 1)),
                GSensorInterruptOccured = DecodeSelfLantern(binaryData.Substring(48, 1)),
                IsNight = DecodeSelfLantern(binaryData.Substring(49, 1)),
                SolarChargingOn = DecodeSelfLantern(binaryData.Substring(50, 1))
            };
        }

        private static RMSBaseResult DecodeLighthouse(string binaryData)
        {
            return new RMSLighthouse
            {
                SupplyVoltageInt = DecodeVoltageIntLight(binaryData.Substring(0, 9)).ToStringOrDefault(true),
                SupplyVoltageExt1 = DecodeVoltageExternal1(binaryData.Substring(9, 9)).ToStringOrDefault(true),
                SupplyVoltageExt2 = DecodeVoltageExternal2(binaryData.Substring(18, 9)).ToStringOrDefault(true),
                OffPosition = DecodeOffPosition(binaryData.Substring(27, 1)),
                LightSensor = DecodeAmbient(binaryData.Substring(28, 2)),
                Status = DecodeStatus(binaryData.Substring(30, 5)),
                Beat = DecodeBeat(binaryData.Substring(35, 1)),
                MainLanternCondition = DecodeComponentCondition(binaryData.Substring(36, 1)),
                MainLanternStatus = DecodeComponentStatus(binaryData.Substring(37, 1)),
                StandbyLanternCondition = DecodeComponentCondition(binaryData.Substring(38, 1)),
                StandbyLanternStatus = DecodeComponentStatus(binaryData.Substring(39, 1)),
                EmergencyLanternCondition = DecodeComponentCondition(binaryData.Substring(40, 1)),
                EmergencyLanternStatus = DecodeComponentStatus(binaryData.Substring(41, 1)),
                OpticDriveAStatus = DecodeComponentStatus(binaryData.Substring(42, 1)),
                OpticDriveACondition = DecodeComponentCondition(binaryData.Substring(43, 1)),
                OpticDriveBStatus = DecodeComponentStatus(binaryData.Substring(44, 1)),
                OpticDriveBCondition = DecodeComponentCondition(binaryData.Substring(45, 1)),
                HatchDoor = DecodeDoor(binaryData.Substring(46, 1)),
                MainPower = DecodeComponentStatus(binaryData.Substring(47, 1)),
                BMSCondition = DecodeComponentCondition(binaryData.Substring(48, 1))
            };
        }

        private static RMSBaseResult DecodeIALA(string binaryData)
        {
            return new RMSIALA
            {
                SupplyVoltageInt = DecodeVoltageInt(binaryData.Substring(0, 10)).ToStringOrDefault(true),
                SupplyVoltageExt1 = DecodeVoltageExternal1(binaryData.Substring(10, 10)).ToStringOrDefault(true),
                SupplyVoltageExt2 = DecodeVoltageExternal2(binaryData.Substring(20, 10)).ToStringOrDefault(true),
                Status = DecodeStatus(binaryData.Substring(30, 5)),
                ExternalDigitalInputs = DecodeExternalDigitalInputs(binaryData.Substring(35, 8)),
                OffPosition = DecodeOffPosition(binaryData.Substring(43, 1))
            };
        }

        private static RMSLightBeaconApp DecodeLightBeaconApp(string binaryData)
        {
            return new RMSLightBeaconApp
            {
                SupplyVoltageInt = DecodeVoltageInt(binaryData.Substring(0, 10)).ToStringOrDefault(true),
                SupplyVoltageExt1 = DecodeVoltageExternal1(binaryData.Substring(10, 10)).ToStringOrDefault(true),
                SupplyVoltageExt2 = DecodeVoltageExternal2(binaryData.Substring(20, 10)).ToStringOrDefault(true),
                Status = DecodeStatus(binaryData.Substring(30, 5)),
                Beat = DecodeBeat(binaryData.Substring(35, 1)),
                LanternBatt = DecodeLanternBatt(binaryData.Substring(36, 2)),
                Lantern = DecodeLantern(binaryData.Substring(38, 2)),
                Ambient = DecodeAmbient(binaryData.Substring(40, 2)),
                Door = DecodeDoor(binaryData.Substring(42, 1)),
                OffPosition = DecodeOffPosition(binaryData.Substring(43, 1))
            };
        }

        public static string GetHealthBinary(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null)
            {
                return null;
            }
            string e = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                e = binary.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.IALA)
            {
                e = binary.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                e = binary.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                e = binary.Substring(30, 5);
            }
            else
            {
                return null;
            }
            return e.Substring(4, 1);
        }

        public static string GetLightBinary(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null)
            {
                return null;
            }
            string e = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                e = binary.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.IALA)
            {
                e = binary.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                e = binary.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                e = binary.Substring(30, 5);
            }
            else
            {
                return null;
            }
            return e.Substring(2, 2);
        }

        public static string GetRaconBinary(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null)
            {
                return null;
            }
            string e = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                e = binary.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.IALA)
            {
                e = binary.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                e = binary.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                e = binary.Substring(30, 5);
            }
            else
            {
                return null;
            }
            return e.Substring(0, 2);
        }

        public static string GetOffpositionBinary(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null)
            {
                return null;
            }
            string e = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                e = binary.Substring(43, 1);
            }
            else if (rmsType == RMSTypes.IALA)
            {
                e = binary.Substring(43, 1);
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                e = binary.Substring(26, 1);
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                e = binary.Substring(26, 1);
            }
            else
            {
                return null;
            }
            return e;
        }

        #region SelfContainedLantern
        public static string DecodeSelfLantern(string binary)
        {
            if (binary == "0")
            {
                return "No";
            }
            else if (binary == "1")
            {
                return "Yes";
            }
            return null;
        }
        #endregion

        #region Lighthouse

        public static string DecodeComponentCondition(string binary)
        {
            if (binary == "0")
            {
                return "Normal";
            }
            else if (binary == "1")
            {
                return "Fail";
            }
            return null;
        }
        public static string DecodeComponentStatus(string binary)
        {
            if (binary == "0")
            {
                return "Off";
            }
            else if (binary == "1")
            {
                return "On";
            }
            return null;
        }

        public static int? DecodeMainLanternCondition(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 36;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeMainLanternStatusVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 37;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeStandbyLanternConditionVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 38;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeStandbyLanternStatusVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 39;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeEmergencyLanternConditionVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 40;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeEmergencyLanternStatusVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 41;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeOpticDriveAStatusVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 42;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeOpticDriveAConditionVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 43;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeOpticDriveBStatusVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 44;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeOpticDriveBConditionVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 45;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeHatchDoorVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 46;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeMainPowerVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 47;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for Lighthouse only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeBMSConditionVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 48;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        #endregion

        #region IALA
        private static string DecodeExternalDigitalInputs(string binary)
        {
            return binary;
        }
        #endregion

        #region LightBeaconApp      

        public static string DecodeLanternBatt(string binary)
        {
            var dec = binary.FromBinaryToInt();
            if (dec == 0)
            {
                return "Unknown";
            }
            else if (dec == 1)
            {
                return "Bad";
            }
            else if (dec == 2)
            {
                return "Low";
            }
            else if (dec == 3)
            {
                return "Good";
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// Specifically for LightBeaconApplication only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeLanternBatt(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                startIndex = 36;
                length = 2;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static string DecodeLantern(string binary)
        {
            var dec = binary.FromBinaryToInt();

            if (dec == 0)
            {
                return "No light or no monitoring";
            }
            else if (dec == 1)
            {
                return "Primary";
            }
            else if (dec == 2)
            {
                return "Secondary";
            }
            else if (dec == 3)
            {
                return "Emergency";
            }
            else
            {
                return null;
            }
        }
        public static int? DecodeAlarmActiveVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 36;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeFlshPowerThresVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 37;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeErrorLedOpenVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 38;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeErrorLedShortVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 39;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeErrorLedVoltageLowVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 40;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeErrorPowerThresVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 41;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeErrorVinLowVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 42;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeFlasherAdjToLedVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 43;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeFlasherOffForceOffVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 44;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeFlasherOffLowVinVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 45;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeFlasherOffPhotoCellVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 46;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeFlasherOffTemperatureVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 47;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeGSensorInterruptOccuredVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 48;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeIsNightVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 49;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static int? DecodeSolarChargingOnVal(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 50;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// Specifically for LightBeaconApplication only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeLantern(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                startIndex = 38;
                length = 2;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        #endregion

        #region common
        public static string DecodeDoor(string binary)
        {
            if (binary == "0")
            {
                return "Close";
            }
            else if (binary == "1")
            {
                return "Open";
            }
            else
            {
                return null;
            }
        }
        public static int? DecodeDoor(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                startIndex = 42;
                length = 1;
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 46;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static string DecodeBeat(string binary)
        {
            if (binary == "0")
            {
                return "Tick";
            }
            else
            {
                return "Tock";
            }
        }
        public static int? DecodeBeat(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                startIndex = 35;
                length = 1;
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 35;
                length = 1;
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 35;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static string DecodeAmbient(string binary)
        {
            var dec = binary.FromBinaryToInt();

            if (dec == 0)
            {
                return "No LDR";
            }
            else if (dec == 1)
            {
                return "Dark";
            }
            else if (dec == 2)
            {
                return "Dim";
            }
            else if (dec == 3)
            {
                return "Bright";
            }
            else
            {
                return null;
            }
        }

        public static int? DecodeAmbient(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                startIndex = 40;
                length = 2;
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 28;
                length = 2;
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 28;
                length = 2;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }

        /// <summary>
        /// for lighthouse rms
        /// </summary>
        /// <param name="binary"></param>
        /// <returns></returns>
        private static double DecodeVoltageIntLight(string binary)
        {
            var r = ((double)binary.FromBinaryToDouble() * 10) / 100;
            return r.ToDecimalPlace(2);
        }
        private static double DecodeVoltageInt(string binary)
        {
            var r = (double)binary.FromBinaryToDouble() / 20;
            return r.ToDecimalPlace(2);
        }
        public static double? DecodeVoltageInt(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                startIndex = 0;
                length = 10;
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 0;
                length = 9;
                return ((double)binary.Substring(startIndex, length).FromBinaryToDouble() * 10).ToDecimalPlace(2);
            }
            else if (rmsType == RMSTypes.IALA)
            {
                startIndex = 0;
                length = 10;
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 0;
                length = 9;
            }
            else
            {
                return null;
            }

            var r = (double)binary.Substring(startIndex, length).FromBinaryToDouble() / 20;
            return r.ToDecimalPlace(2);
        }

        private static double DecodeVoltageExternal1(string binary)
        {
            var r = (double)binary.FromBinaryToDouble() / 20;
            return r.ToDecimalPlace(2);
        }
        public static double? DecodeVoltageExternal1(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 9;
                length = 9;
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 9;
                length = 9;
            }
            else if (rmsType == RMSTypes.IALA)
            {
                startIndex = 10;
                length = 10;
            }
            else if (rmsType == RMSTypes.LightBeaconApplication)
            {
                startIndex = 10;
                length = 10;
            }
            else
            {
                return null;
            }

            var r = (double)binary.Substring(startIndex, length).FromBinaryToDouble() / 20;
            return r.ToDecimalPlace(2);
        }
        private static double DecodeVoltageExternal2(string binary)
        {
            var r = (double)binary.FromBinaryToDouble() / 20;
            return r.ToDecimalPlace(2);
        }
        public static double? DecodeVoltageExternal2(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 18;
                length = 9;
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 18;
                length = 9;
            }
            else if (rmsType == RMSTypes.IALA)
            {
                startIndex = 20;
                length = 10;
            }
            else if (rmsType == RMSTypes.LightBeaconApplication)
            {
                startIndex = 20;
                length = 10;
            }
            else
            {
                return null;
            }

            var r = (double)binary.Substring(startIndex, length).FromBinaryToDouble() / 20;
            return r.ToDecimalPlace(2);
        }
        private static string DecodeStatus(string binary)
        {
            string result = string.Empty;
            if (binary.Substring(0, 2) == "00")
            {
                result += "No RACON installed, ";
            }
            else if (binary.Substring(0, 2) == "01")
            {
                result += "RACON not monitored, ";
            }
            else if (binary.Substring(0, 2) == "10")
            {
                result += "RACON operational, ";
            }
            else if (binary.Substring(0, 2) == "11")
            {
                result += "RACON error, ";
            }

            if (binary.Substring(2, 2) == "00")
            {
                result += "No light or no monitoring, ";
            }
            else if (binary.Substring(2, 2) == "01")
            {
                result += "Light ON, ";
            }
            else if (binary.Substring(2, 2) == "10")
            {
                result += "Light OFF, ";
            }
            else if (binary.Substring(2, 2) == "11")
            {
                result += "Light ERROR, ";
            }

            if (binary.Substring(4, 1) == "0")
            {
                result += "Good Health";
            }
            else
            {
                result += "Alarm";
            }

            return result;
        }
        public static string DecodeOffPosition(string binary)
        {
            if (binary == "0")
            {
                return "On Position";
            }
            else if (binary == "1")
            {
                return "Off Position";
            }
            else
            {
                return null;
            }
        }
        public static int? DecodeOffPosition(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                startIndex = 43;
                length = 1;
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                startIndex = 27;
                length = 1;
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                startIndex = 27;
                length = 1;
            }
            else if (rmsType == RMSTypes.ZeniLiteBuoys)
            {
                startIndex = 41;
                length = 1;
            }
            else if (rmsType == RMSTypes.IALA)
            {
                startIndex = 43;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        public static RMSHealthStatusVal DecodeHealthStatusVal(string binaryData, int? designatedAreaCode, int? functionalId)
        {
            var rmsType = GetRMSType(designatedAreaCode, functionalId);
            if (string.IsNullOrWhiteSpace(binaryData) || designatedAreaCode == null || functionalId == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            string bin = null;
            double? health = null;
            double? light = null;
            double? racon = null;

            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                bin = binaryData.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.IALA)
            {
                bin = binaryData.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                bin = binaryData.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                bin = binaryData.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.ZeniLiteBuoys)
            {
                bin = binaryData.Substring(39, 1);
            }

            if (rmsType != RMSTypes.ZeniLiteBuoys)
            {
                racon = bin.Substring(0, 2).FromBinaryToShort();
                light = bin.Substring(2, 2).FromBinaryToShort();
                health = bin.Substring(4, 1).FromBinaryToShort();
            }
            else
            {
                light = bin.FromBinaryToShort();
            }

            return new RMSHealthStatusVal
            {
                Health = health,
                Light = light,
                Racon = racon
            };
        }


        public static RMSHealthStatus DecodeHealthStatus(string binaryData, int? designatedAreaCode, int? functionalId)
        {
            var rmsType = GetRMSType(designatedAreaCode, functionalId);
            if (string.IsNullOrWhiteSpace(binaryData) || designatedAreaCode == null || functionalId == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            string bin = null;
            string health = null;
            string light = null;
            string racon = null;

            if (rmsType == RMSTypes.LightBeaconApplication)
            {
                bin = binaryData.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.IALA)
            {
                bin = binaryData.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.LighthouseRenewableEnergy)
            {
                bin = binaryData.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.SelfContainedLantern)
            {
                bin = binaryData.Substring(30, 5);
            }
            else if (rmsType == RMSTypes.ZeniLiteBuoys)
            {
                bin = binaryData.Substring(39, 1);
            }

            if (rmsType != RMSTypes.ZeniLiteBuoys)
            {
                if (bin.Substring(0, 2) == "00")
                {
                    racon = "No RACON installed";
                }
                else if (bin.Substring(0, 2) == "01")
                {
                    racon = "RACON not monitored";
                }
                else if (bin.Substring(0, 2) == "10")
                {
                    racon = "RACON operational";
                }
                else if (bin.Substring(0, 2) == "11")
                {
                    racon = "RACON error";
                }

                if (bin.Substring(2, 2) == "00")
                {
                    light = "No light or no monitoring";
                }
                else if (bin.Substring(2, 2) == "01")
                {
                    light = "Light ON";
                }
                else if (bin.Substring(2, 2) == "10")
                {
                    light = "Light OFF";
                }
                else if (bin.Substring(2, 2) == "11")
                {
                    light = "Light ERROR";
                }

                if (bin.Substring(4, 1) == "0")
                {
                    health = "Good Health";
                }
                else
                {
                    health = "Alarm";
                }
            }
            else
            {
                light = DecodeLightStatus(bin);
            }


            return new RMSHealthStatus
            {
                Health = health,
                Light = light,
                Racon = racon
            };
        }
        #endregion

        #region ZeniLite
        private static string DecodeBatteryStatus(string binary)
        {
            if (binary == "0")
            {
                return "Good";
            }
            else if (binary == "1")
            {
                return "Low voltage";
            }
            return null;
        }
        /// <summary>
        /// Specifically for Zeni lite only
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodeBatteryStatus(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            string bin = string.Empty;
            if (rmsType == RMSTypes.ZeniLiteBuoys)
            {
                startIndex = 40;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }
        /// <summary>
        /// for zeni lite only
        /// </summary>
        /// <param name="binary"></param>
        /// <returns></returns>
        private static string DecodeLightStatus(string binary)
        {
            if (binary == "0")
            {
                return "Light Off";
            }
            else if (binary == "1")
            {
                return "Light On";
            }
            return null;
        }

        public static string DecodePowerSupplyType(string binary)
        {
            if (string.IsNullOrEmpty(binary))
            {
                return null;
            }
            if (binary == "0")
            {
                return "AC";
            }
            else if (binary == "1")
            {
                return "DC";
            }
            return null;
        }

        /// <summary>
        /// Zeni lite
        /// </summary>
        /// <param name="binary"></param>
        /// <param name="dac"></param>
        /// <param name="fi"></param>
        /// <returns></returns>
        public static int? DecodePowerSupplyType(string binary, int? dac, int? fi)
        {
            var rmsType = GetRMSType(dac, fi);
            if (string.IsNullOrWhiteSpace(binary) || dac == null || fi == null || rmsType == RMSTypes.None)
            {
                return null;
            }
            var startIndex = 0;
            var length = 0;
            if (rmsType == RMSTypes.ZeniLiteBuoys)
            {
                startIndex = 38;
                length = 1;
            }
            else
            {
                return null;
            }

            return binary.Substring(startIndex, length).FromBinaryToInt();
        }

        private static double? DecodeCurrentData(string binary)
        {
            if (string.IsNullOrWhiteSpace(binary))
            {
                return null;
            }

            var r = (double)binary.FromBinaryToDouble() / 10;
            return r.ToDecimalPlace(2);
        }

        private static double? DecodeVoltageData(string binary)
        {
            if (string.IsNullOrWhiteSpace(binary))
            {
                return null;
            }

            var r = (double)binary.FromBinaryToDouble() / 10;
            return r.ToDecimalPlace(2);
        }
        #endregion
    }
}
