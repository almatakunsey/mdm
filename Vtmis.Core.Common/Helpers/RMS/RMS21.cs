﻿using System;

namespace Vtmis.Core.Common.Helpers.RMS
{
    public class RMS21
    {
        public Int16? Health21 { get; set; }
        public Int16? Light21 { get; set; }
        public Int16? RACON21 { get; set; }
        public Int16? OffPosition21 { get; set; }
        public int Error21 { get; set; }
    }
}
