﻿namespace Vtmis.Core.Common.Helpers.RMS
{
    public class RMSLightBeaconApp : RMSBaseResult
    {
        public string Beat { get; set; }
        public string LanternBatt { get; set; }
        public string Lantern { get; set; }
        public string Ambient { get; set; }
        public string Door { get; set; }
    }
}
