﻿namespace Vtmis.Core.Common.Helpers.RMS
{
    public class RMSSelfContainedLantern : RMSBaseResult
    {
        public string LightSensor { get; set; }
        public string Beat { get; set; }
        public string AlarmActive { get; set; }
        public string FlasherOffPowerThres { get; set; }
        public string FlasherOffLowVin { get; set; }
        public string FlasherOffPhotoCell { get; set; }
        public string FlasherOffTemperature { get; set; }
        public string FlasherOffForceOff { get; set; }
        public string IsNight { get; set; }
        public string ErrorLedShort { get; set; }
        public string ErrorLedOpen { get; set; }
        public string ErrorLedVoltageLow { get; set; }
        public string ErrorVinLow { get; set; }
        public string ErrorPowerThres { get; set; }
        public string FlasherAdjToLed { get; set; }
        public string GSensorInterruptOccured { get; set; }
        public string SolarChargingOn { get; set; }
    }
}
