﻿namespace Vtmis.Core.Common.Helpers.RMS
{
    public enum RMSTypes
    {
        LightBeaconApplication, // DAC (533) || FI (1)
        IALA, // DAC (235) || FI (10)
        LighthouseRenewableEnergy, // DAC (533) || FI (2)
        SelfContainedLantern, // DAC (533) || FI (4)
        ZeniLiteBuoys, // DAC (0) || FI (0)
        None
    }
}
