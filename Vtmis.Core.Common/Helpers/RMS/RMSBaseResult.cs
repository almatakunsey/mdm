﻿namespace Vtmis.Core.Common.Helpers.RMS
{
    public abstract class RMSBaseResult
    {
        public string SupplyVoltageInt { get; set; }
        public string SupplyVoltageExt1 { get; set; }
        public string SupplyVoltageExt2 { get; set; }
        public string OffPosition { get; set; }
        public string Status { get; set; }
        public int? DAC { get; set; }
        public int? FI { get; set; }
    }
}
