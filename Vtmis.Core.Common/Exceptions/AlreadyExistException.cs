﻿using System;

namespace Vtmis.Core.Common.Exceptions
{
    public class AlreadyExistException : Exception
    {
        public AlreadyExistException()
        {

        }
        public AlreadyExistException(string message) : base(message)
        {

        }
    }
}
