﻿using System;

namespace Vtmis.Core.Common.Exceptions
{
    public class ServerFaultException : Exception
    {
        public ServerFaultException()
        {

        }
        public ServerFaultException(string message) : base(message)
        {

        }
        public ServerFaultException(Exception err) : base("", err)
        {

        }
    }
}
