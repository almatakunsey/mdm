﻿using System;

namespace Vtmis.Core.Common.Exceptions
{
    public class RequestFaultException : Exception
    {
        public RequestFaultException()
        {

        }
        public RequestFaultException(string message) : base(message)
        {

        }
    }
}
