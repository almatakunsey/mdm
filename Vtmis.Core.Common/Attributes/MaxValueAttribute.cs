﻿using System.ComponentModel.DataAnnotations;

namespace Vtmis.Core.Common.Attributes
{
    public class MaxValueAttribute : ValidationAttribute
    {
        private readonly int _maxValue;

        public MaxValueAttribute(int maxValue)
        {
            _maxValue = maxValue;


        }

        public override bool IsValid(object value)
        {
            return (int)value <= _maxValue;
        }
    }
}
