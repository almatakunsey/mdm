﻿using System.Collections.Generic;

namespace Vtmis.Core.Common.Constants
{
    public static class MessageIdsConst
    {
        public static IEnumerable<int> StaticMessage()
        {
            return new List<int>
            {
                5, 24
            };
        }

        public static IEnumerable<int> AllowedTarget()
        {
            return new List<int>
            {
                1,2,3,4,9,18,19,21
            };
        }

        public static IEnumerable<int> TargetWithNoStaticData()
        {
            return new List<int>
            {
                21, 4, 9
            };
        }
    }
}
