﻿namespace Vtmis.Core.Common.Constants
{
    public class MmdisApis
    {
        public const string Dredging_Dev = "http://202.129.173.69:8080/MMDIS_AIS/Services/Dredging/Location/Insert";
        public const string Vessel_Clearance_Dev = "http://202.129.173.69:8080/MMDIS_AIS/Services/ShipClearance/Update";
        public const string Light_Error_Dev = "http://202.129.173.69:8080/MMDIS_AIS/Services/LightError/Insert";
        public const string Strait_Reps_Dev = "";
    }
}
