﻿using System.Collections.Generic;

namespace Vtmis.Core.Common.Constants
{
    public static class TargetClassConst
    {
        public static List<string> All()
        {
            return new List<string>
            {
                ClassA,
                ClassB,
                AtoN,
                SAR,
                SART,
                SAR_Air,
                BaseStation
            };
        }
        public const string ClassA = "Class A";
        public const string ClassB = "Class B";
        public const string AtoN = "AtoN";
        public const string SAR = "SAR";
        public const string SART = "SART";
        public const string SAR_Air = "SAR Air";
        public const string BaseStation = "Base Station";
        public const string Methydro = "Methydro";
    }
}
