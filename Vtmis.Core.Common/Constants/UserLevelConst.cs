﻿namespace Vtmis.Core.Common.Constants
{
    public class UserLevelConst
    {
        public static string HostAdmin = "HostAdmin";
        public static string TenantAdmin = "TenantAdmin";
        public static string TenantUser = "TenantUser";
        public static string Annonymous = null;
    }
}
