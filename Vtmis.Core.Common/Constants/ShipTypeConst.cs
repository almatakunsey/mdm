﻿using System.Collections.Generic;

namespace Vtmis.Core.Common.Constants
{
    public static class ShipTypeConst
    {
        public static List<string> All()
        {
            return new List<string>
            {
                Reserved,
                WingInGround,
                //SAR_Aircraft,
                Fishing,
                Tug,
                Dredger,
                DiveVessel,
                MilitaryOps,
                SailingVessel,
                PleasureCraft,
                HighSpeedCraft,
                PilotVessel,
                SAR,
                PortTender,
                AntiPollution,
                LawEnforce,
                LocalVessel,
                MedicalTrans,
                SpecialCraft,
                Passenger,
                Cargo,
                CargoHazardA_Major,
                CargoHazardB,
                CargoHazardC_Minor,
                CargoHazardD_Recognize,
                Tanker,
                TankerHazardA_Major,
                TankerHazardB,
                TankerHazardC_Minor,
                TankerHazardD_Recognize,
                Other
                //NavigationAid,
                //ReferencePoint,
                //Racon,
                //OffshoreStructure,
                //Spare,
                //Light_NoSectors,
                //Light_Sectors,
                //LeadingLightRear,
                //BeaconCardinal_N,
                //BeaconCardinal_E,
                //BeaconCardinal_S,
                //BeaconCardinal_W,
                //BeaconPortHand,
                //BeaconStarboardHand,
                //BeaconPortHand_Channel,
                //BeaconStarboardHand_Channel,
                //BeaconIsolatedDanger,
                //BeaconSafeWater,
                //BeaconSpecialMark,
                //CardinalMark_N,
                //CardinalMark_E,
                //CardinalMark_S,
                //CardinalMark_W,
                //PortHandMark,
                //StarboardHandMark,
                //PreferredChannelPortHand,
                //PreferredChannelStarboardHand,
                //IsolatedDanger,
                //SafeWater,
                //MannedVTS,
                //LightVessel
            };
        }

        public const string Reserved = "Reserved";
        public const string WingInGround = "Wing In Ground";
        //public const string SAR_Aircraft = "SAR Aircraft";
        public const string Fishing = "Fishing";
        public const string Tug = "Tug";
        public const string Dredger = "Dredger";
        public const string DiveVessel = "Dive Vessel";
        public const string MilitaryOps = "Military Ops";
        public const string SailingVessel = "Sailing Vessel";
        public const string PleasureCraft = "Pleasure Craft";
        public const string HighSpeedCraft = "High-Speed Craft";
        public const string PilotVessel = "Pilot Vessel";
        public const string SAR = "SAR";
        public const string PortTender = "Port Tender";
        public const string AntiPollution = "Anti-Pollution";
        public const string LawEnforce = "Law Enforce";
        public const string LocalVessel = "Local Vessel";
        public const string MedicalTrans = "Medical Trans";
        public const string SpecialCraft = "Special Craft";
        public const string Passenger = "Passenger";
        public const string Cargo = "Cargo";
        public const string CargoHazardA_Major = "Cargo - Hazard A (Major)";
        public const string CargoHazardB = "Cargo - Hazard B";
        public const string CargoHazardC_Minor = "Cargo - Hazard C (Minor)";
        public const string CargoHazardD_Recognize = "Cargo - Hazard D (Recognizable)";
        public const string Tanker = "Tanker";
        public const string TankerHazardA_Major = "Tanker - Hazard A (Major)";
        public const string TankerHazardB = "Tanker - Hazard B";
        public const string TankerHazardC_Minor = "Tanker - Hazard C (Minor)";
        public const string TankerHazardD_Recognize = "Tanker - Hazard D (Recognizable)";
        public const string Other = "Other";

        //TODO: Remark for Aton only
        //public const string NavigationAid = "Navigation Aid";
        //public const string ReferencePoint = "Reference Point";
        //public const string Racon = "RACON";
        //public const string OffshoreStructure = "Offshore Structure";
        //public const string Spare = "Spare";
        //public const string Light_NoSectors = "Light, without Sectors";
        //public const string Light_Sectors = "Light, with Sectors";
        //public const string LeadingLightRear = "Leading Light Rear";
        //public const string LeadingLightFront = "Leading Light Front";
        //public const string BeaconCardinal_N = "Beacon, Cardinal N";
        //public const string BeaconCardinal_E = "Beacon, Cardinal E";
        //public const string BeaconCardinal_S = "Beacon, Cardinal S";
        //public const string BeaconCardinal_W = "Beacon, Cardinal W";
        //public const string BeaconPortHand = "Beacon, Port Hand";
        //public const string BeaconStarboardHand = "Beacon, Starboard Hand";
        //public const string BeaconPortHand_Channel = "Beacon, Preferred Channel Port hand";
        //public const string BeaconStarboardHand_Channel = "Beacon, Preferred Channel Starboard hand";
        //public const string BeaconIsolatedDanger = "Beacon, Isolated danger";
        //public const string BeaconSafeWater = "Beacon, Safe Water";
        //public const string BeaconSpecialMark = "Beacon, Special Mark";
        //public const string CardinalMark_N = "Cardinal Mark N";
        //public const string CardinalMark_E = "Cardinal Mark E";
        //public const string CardinalMark_S = "Cardinal Mark S";
        //public const string CardinalMark_W = "Cardinal Mark W";
        //public const string PortHandMark = "Port Hand Mark";
        //public const string StarboardHandMark = "Starboard Hand Mark";
        //public const string PreferredChannelPortHand = "Preferred Channel Port Hand";
        //public const string PreferredChannelStarboardHand = "Preferred Channel Starboard Hand";
        //public const string IsolatedDanger = "Isolated Danger";
        //public const string SafeWater = "Safe Water";
        //public const string MannedVTS = "Manned VTS";
        //public const string LightVessel = "Light Vessel";
    }
}
