﻿namespace Vtmis.Core.Common.Constants
{
    public class AlarmNameEvent
    {
        public const string DDMS = "DDMS";
        public const string DDMS_Dumping = "Dumping";
        public const string NO_DDMS = "No DDMS";
        public const string Vessel_Arrival = "Arrival";
        public const string Vessel_Departure = "Departure";
        public const string Vessel_Arrived = "Arrived";
        public const string Vessel_Departed = "Departed";
        public const string Vessel_Entry = "Entry";
        public const string Vessel_Exit = "Exit";
        public const string Light_Failure = "Light Failure";
        public const string Light_Error = "Light Error";
        public const string Strait_Reps = "Light Error";
    }
}
