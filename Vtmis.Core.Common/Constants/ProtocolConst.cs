﻿namespace Vtmis.Core.Common.Constants
{
    public class ProtocolConst
    {
        public const string HTTP = "http";
        public const string HTTPS = "https";
    }
}
