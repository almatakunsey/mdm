﻿using Akka.Actor;

namespace Vtmis.Core.Common.Constants.Akka
{
    public static class ActorRefsConst
    {
        public static ActorSystem ActorSystem;
        public static IActorRef Search = ActorRefs.Nobody;
        public static IActorRef Playback = ActorRefs.Nobody;
        public static IActorRef Filter = ActorRefs.Nobody;
        public static IActorRef Report = ActorRefs.Nobody;
        public static IActorRef Route = ActorRefs.Nobody;
        public static IActorRef Global = ActorRefs.Nobody;
    }
}
