﻿namespace Vtmis.Core.Common.Constants
{
    public static class ReportTypeConst
    {
        public static int ArrivalDeparture = 3;
        public static int SpeedViolation = 7;
    }
}
