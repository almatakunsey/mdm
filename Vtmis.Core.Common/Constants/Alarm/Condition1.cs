﻿namespace Vtmis.Core.Common.Constants.Alarm
{
    public static class Condition1
    {
        public const string Self = "Condition 1";
        public const string OnEnter = "On Enter";
        public const string InZone = "In Zone";
        public const string OnExit = "On Exit";
        public const string OnEnterExit = "On Enter/Exit";
        public const string LightOff = "Light Off";
        public const string LightError = "Light Error";
        public const string RaconError = "RACON Error";
        public const string HealthAlarm = "Health Alarm";
        public const string OffPosition = "Off Position";
        public const string LowBattery = "Low Battery";
        public const string PhotoCellError = "Photocell Error";
        public const string NotInZone = "Not In Zone";
        public const string Bit0 = "Bit0 - TILT";
        public const string Bit1 = "Bit1 - SHOCK";
        public const string Bit2 = "Bit2 - GPS";
        public const string Bit3 = "Bit3 - GSM";
        public const string Bit4 = "Bit4 - AIS";
        public const string Bit5 = "Bit5 - GUARD";
        public const string Bit6 = "Bit6 - CTRL";
        public const string Bit7 = "Bit7 - POWER";
        public const string CoverOpen = "[DDMS] Cover Open";
        public const string CoverClose = "[DDMS] Cover Closed";
        public const string SonarError = "[DDMS] Sonar Error";
    }
}
