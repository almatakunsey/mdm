﻿namespace Vtmis.Core.Common.Constants.Alarm
{
    public static class Condition2
    {
        public const string Self = "Condition 2";
        public const string Speed = "Speed (knots)";
        public const string WindSpeed = "Wind Speed (knots)";
        public const string WindGust = "Wind Gust (knots)";
        public const string AirPressure = "Air Pres. (hPa)";
        public const string AirTemperature = "Air Temp. (°C)";
        public const string AnalogueInt = "Analogue Int. (V)";
        public const string AnalogueExternal1 = "Analogue Ext.1 (V)";
        public const string AnalogueExternal2 = "Analogue Ext.2 (V)";
        public const string Voltage = "Voltage (V)";
        public const string Current = "Current (A)";
        public const string TimeToEnter = "Time To Enter (min)";
        public const string TimeToExit = "Time To Exit (min)";
        public const string CourseChange = "Course Change (min|deg)";
        public const string Time = "Time (HHMM)";
        public const string UtcTime = "UTC Time (HHMM)";
        public const string BatteryVoltage = "[DDMS] Battery Voltage (V)";
        public const string NoHopper = "[DDMS] Hopper (No)";
    }
}
