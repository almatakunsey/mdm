﻿namespace Vtmis.Core.Common.Constants
{
    public static class FilterFieldsConst
    {
        public static string MMSI = "MMSI";
        public static string IMO = "IMO";
        public static string CallSign = "CallSign";
        public static string ShipName = "ShipName";
        public static string ShipType = "ShipType";
        public static string GroupType = "GroupTypeId";
        public static string NavStatus = "NavStatus";
        public static string AisType = "AisType";
        public static string DataSource = "DataSource";
        public static string MinSpeed = "MinSpeed";
        public static string MaxSpeed = "MaxSpeed";
        public static string MinCourse = "MinCourse";
        public static string MaxCourse = "MaxCourse";
        public static string MinLength = "MinLength";
        public static string MaxLength = "MaxLength";
        public static string MinAge = "MinAge";
        public static string MaxAge = "MaxAge";
    }
}
