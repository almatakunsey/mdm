﻿namespace Vtmis.Core.Common.Constants
{
    public class RedisConfig
    {
        public string RedisConnectionString
        {
            get
            {
                return "redis";
            }
        }
    }
}
