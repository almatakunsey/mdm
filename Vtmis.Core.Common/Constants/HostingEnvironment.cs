﻿namespace Vtmis.Core.Common.Constants
{
    public class HostingEnvironment
    {
        public const string PRODUCTION = "Production";
        public const string HAFIZ_DEV = "HafizDev";
        public const string STAGING = "Staging";
        public const string DEVELOPMENT = "Development";
        public const string POC = "POC";
        public const string LOCAL = "Local";

        public const string DEV_URL = "mdm.greenfinder.asia:5011";
        public const string STAGING_URL = "mdm.greenfinder.asia:5021";
        public const string PROD_URL = "mdm.greenfinder.asia";
    }
}
