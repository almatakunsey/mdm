﻿namespace Vtmis.Core.Common.Constants
{
    public class AppConst
    {
        //TODO: https
        public const string Protocol = "http://";
        public const int HostPort = 50837;
        public const string HostDomain = "vtmis_webadmin_web_host";
        public const string HostDomainStaging = "vtmis_webadmin_web_host_staging";
        public const int MapPort = 80;
        public const string MapDomain = "vtmis_webvessel_tracking";
        public const int MmdisPort = 50838;
        public const string MmdisDomain = "vtmis_api_mmdis";
        public const int WebAdminPort = 8082;
        public const string WebAdminDomain = "vtmis_webadmin_web_mvc";
        public const int ApiTrackingPort = 8081;
        public const string ApiTrackingDomain = "vtmis_api_maptracking";
    }
}
