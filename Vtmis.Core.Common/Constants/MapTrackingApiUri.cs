﻿namespace Vtmis.Core.Common.Constants
{
    public static class MapTrackingApiUri
    {
        public static string GetTargetInfoByVesselIdUri = "http://vtmis_api_maptracking/api/VesselFilters/GetTargetInfoByVesselId";
    }
}
