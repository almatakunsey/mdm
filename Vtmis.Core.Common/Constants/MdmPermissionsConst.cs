﻿using System.Collections.Generic;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Common.Constants
{
    /// <summary>
    /// "[Feature].[Action].[Target]"  e.g => Location.Update.Tenant
    /// Description:
    /// User under this permission can update any Location under his/her Tenant
    /// </summary>
    public static class MdmPermissionsConst
    {
        public static List<string> All()
        {
            var result = new List<string>();
            result.AddRange(Location());
            result.AddRange(Zone());
            result.AddRange(Filter());
            //if (string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL, System.StringComparison.CurrentCultureIgnoreCase) ||
            //        string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.DEVELOPMENT, System.StringComparison.CurrentCultureIgnoreCase) ||
            //        AppHelper.GetEnvironmentName().IsEqual("POC"))
            //{
            result.AddRange(Report());
            result.AddRange(Alarm());
            result.AddRange(Cable());
            //}

            result.AddRange(Playback());
            result.AddRange(User());
            result.AddRange(Tenant());
            result.AddRange(Role());
            result.AddRange(Route());

            result.AddRange(Methydro());
            result.AddRange(RMS());
            result.AddRange(DDMS());
            result.AddRange(AtonManager());
            result.AddRange(UserPersonalization());
            result.AddRange(TenantPersonalization());
            result.AddRange(MyWREMS());
            result.AddRange(EICS());
            result.AddRange(Images());
            result.AddRange(VesselGroup());
            result.AddRange(TenantFilter());

            return result;
        }
        public static List<string> DefaultAdmin()
        {
            var result = new List<string>();
            result.AddRange(All());

            result.Remove(UpdateLocationWithinMdm);
            result.Remove(DeleteLocationWithinMdm);
            result.Remove(ViewLocationWithinMdm);
            result.Remove(ShareLocationWithinMdm);

            result.Remove(UpdateZoneWithinMdm);
            result.Remove(DeleteZoneWithinMdm);
            result.Remove(ViewZoneWithinMdm);
            result.Remove(ShareZoneWithinMdm);

            result.Remove(UpdateFilterWithinMdm);
            result.Remove(DeleteFilterWithinMdm);
            result.Remove(ViewFilterWithinMdm);
            result.Remove(ShareFilterWithinMdm);

            //if (string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL, System.StringComparison.CurrentCultureIgnoreCase) ||
            //        string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.DEVELOPMENT, System.StringComparison.CurrentCultureIgnoreCase) ||
            //        AppHelper.GetEnvironmentName().IsEqual("POC"))
            //{
            result.Remove(UpdateReportWithinMdm);
            result.Remove(DeleteReportWithinMdm);
            result.Remove(ViewReportWithinMdm);
            result.Remove(ShareReportWithinMdm);

            result.Remove(UpdateAlarmWithinMdm);
            result.Remove(DeleteAlarmWithinMdm);
            result.Remove(ViewAlarmWithinMdm);
            result.Remove(ShareAlarmWithinMdm);
            //}

            result.Remove(User_Create_Mdm);
            result.Remove(User_Delete_Mdm);
            result.Remove(User_Update_Mdm);
            result.Remove(User_View_Mdm);
            result.Remove(User_Deactivate_Mdm);

            result.Remove(Tenant_Create_Mdm);
            result.Remove(Tenant_Delete_Mdm);
            result.Remove(Tenant_Update_Mdm);
            result.Remove(Tenant_View_Mdm);

            result.Remove(Role_Assign_Mdm);
            result.Remove(Role_Create_Mdm);
            result.Remove(Role_Delete_Mdm);
            result.Remove(Role_Update_Mdm);
            result.Remove(Role_View_Mdm);

            result.Remove(Route_Update_Mdm);
            result.Remove(Route_Delete_Mdm);
            result.Remove(Route_View_Mdm);
            result.Remove(Route_Share_Mdm);

            //if (string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL, System.StringComparison.CurrentCultureIgnoreCase) ||
            //        string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.DEVELOPMENT, System.StringComparison.CurrentCultureIgnoreCase) ||
            //        AppHelper.GetEnvironmentName().IsEqual("POC"))
            //{
            result.Remove(Cable_Update_Mdm);
            result.Remove(Cable_Delete_Mdm);
            result.Remove(Cable_View_Mdm);
            result.Remove(Cable_Share_Mdm);
            //}

            result.Remove(VesselGroup_Update_MDM);
            result.Remove(VesselGroup_Delete_MDM);
            result.Remove(VesselGroup_View_MDM);

            result.Remove(TenantFilter_View_MDM);
            return result;
        }
        public static List<string> DefaultUser()
        {
            var result = new List<string>();
            result.AddRange(DefaultAdmin());

            result.Remove(UpdateLocationWithinTenant);
            result.Remove(DeleteLocationWithinTenant);
            result.Remove(ViewLocationWithinTenant);
            result.Remove(ShareLocationWithinTenant);

            result.Remove(UpdateLocationWithinMdm);
            result.Remove(DeleteLocationWithinMdm);
            result.Remove(ViewLocationWithinMdm);
            result.Remove(ShareLocationWithinMdm);

            result.Remove(UpdateZoneWithinTenant);
            result.Remove(DeleteZoneWithinTenant);
            result.Remove(ViewZoneWithinTenant);
            result.Remove(ShareZoneWithinTenant);

            result.Remove(UpdateZoneWithinMdm);
            result.Remove(DeleteZoneWithinMdm);
            result.Remove(ViewZoneWithinMdm);
            result.Remove(ShareZoneWithinMdm);

            result.Remove(UpdateFilterWithinTenant);
            result.Remove(DeleteFilterWithinTenant);
            result.Remove(ViewFilterWithinTenant);
            result.Remove(ShareFilterWithinTenant);

            result.Remove(UpdateFilterWithinMdm);
            result.Remove(DeleteFilterWithinMdm);
            result.Remove(ViewFilterWithinMdm);
            result.Remove(ShareFilterWithinMdm);

            //if (string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL, System.StringComparison.CurrentCultureIgnoreCase) ||
            //       string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.DEVELOPMENT, System.StringComparison.CurrentCultureIgnoreCase) ||
            //       AppHelper.GetEnvironmentName().IsEqual("POC"))
            //{
            result.Remove(UpdateReportWithinTenant);
            result.Remove(DeleteReportWithinTenant);
            result.Remove(ViewReportWithinTenant);
            result.Remove(ShareReportWithinTenant);

            result.Remove(UpdateReportWithinMdm);
            result.Remove(DeleteReportWithinMdm);
            result.Remove(ViewReportWithinMdm);
            result.Remove(ShareReportWithinMdm);

            result.Remove(UpdateAlarmWithinTenant);
            result.Remove(DeleteAlarmWithinTenant);
            result.Remove(ViewAlarmWithinTenant);
            result.Remove(ShareAlarmWithinTenant);

            result.Remove(UpdateAlarmWithinMdm);
            result.Remove(DeleteAlarmWithinMdm);
            result.Remove(ViewAlarmWithinMdm);
            result.Remove(ShareAlarmWithinMdm);
            //}
            result.Remove(User_Create_Mdm);
            result.Remove(User_Delete_Mdm);
            result.Remove(User_Update_Mdm);
            result.Remove(User_View_Mdm);
            result.Remove(User_Deactivate_Mdm);

            result.Remove(User_Create_Tenant);
            result.Remove(User_Delete_Tenant);
            result.Remove(User_Update_Tenant);
            result.Remove(User_View_Tenant);
            result.Remove(User_Deactivate_Tenant);

            result.Remove(Tenant_Create_Mdm);
            result.Remove(Tenant_Delete_Mdm);
            result.Remove(Tenant_Update_Mdm);
            result.Remove(Tenant_View_Mdm);

            //result.Remove(Tenant_Create_Tenant);
            //result.Remove(Tenant_Delete_Tenant);
            //result.Remove(Tenant_Update_Tenant);
            //result.Remove(Tenant_View_Tenant);

            result.Remove(Role_Assign_Mdm);
            result.Remove(Role_Create_Mdm);
            result.Remove(Role_Delete_Mdm);
            result.Remove(Role_Update_Mdm);
            result.Remove(Role_View_Mdm);

            result.Remove(Role_Assign_Tenant);
            result.Remove(Role_Create_Tenant);
            result.Remove(Role_Delete_Tenant);
            result.Remove(Role_Update_Tenant);
            result.Remove(Role_View_Tenant);

            result.Remove(Route_Update_Mdm);
            result.Remove(Route_Delete_Mdm);
            result.Remove(Route_View_Mdm);
            result.Remove(Route_Share_Mdm);

            result.Remove(Route_Update_Tenant);
            result.Remove(Route_Delete_Tenant);
            result.Remove(Route_View_Tenant);
            result.Remove(Route_Share_Tenant);

            //if (string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL, System.StringComparison.CurrentCultureIgnoreCase) ||
            //        string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.DEVELOPMENT, System.StringComparison.CurrentCultureIgnoreCase) ||
            //        AppHelper.GetEnvironmentName().IsEqual("POC"))
            //{
            result.Remove(Cable_Update_Tenant);
            result.Remove(Cable_Delete_Tenant);
            result.Remove(Cable_View_Tenant);
            result.Remove(Cable_Share_Tenant);

            result.Remove(Cable_Update_Mdm);
            result.Remove(Cable_Delete_Mdm);
            result.Remove(Cable_View_Mdm);
            result.Remove(Cable_Share_Mdm);
            //}

            result.Remove(VesselGroup_Update_MDM);
            result.Remove(VesselGroup_Delete_MDM);
            result.Remove(VesselGroup_View_MDM);

            result.Remove(VesselGroup_Update_Tenant);
            result.Remove(VesselGroup_Delete_Tenant);
            result.Remove(VesselGroup_View_Tenant);

            result.Remove(TenantFilter_View_MDM);
            result.Remove(TenantFilter_View_Tenant);
            result.Remove(TenantFilter_Create);

            return result;
        }

        //Location
        public static List<string> Location()
        {
            return new List<string>
            {
                CreateLocation,
                UpdateOwnLocation,
                UpdateLocationWithinTenant,
                UpdateLocationWithinMdm,
                DeleteOwnLocation,
                DeleteLocationWithinTenant,
                DeleteLocationWithinMdm,
                ViewOwnLocation,
                ViewLocationWithinTenant,
                ViewLocationWithinMdm,
                ShareLocationWithinTenant,
                ShareLocationWithinMdm
            };
        }
        public const string CreateLocation = "Location.Create";
        public const string UpdateOwnLocation = "Location.Update.Own";
        public const string UpdateLocationWithinTenant = "Location.Update.Tenant";
        public const string UpdateLocationWithinMdm = "Location.Update.Mdm";
        public const string DeleteOwnLocation = "Location.Delete.Own";
        public const string DeleteLocationWithinTenant = "Location.Delete.Tenant";
        public const string DeleteLocationWithinMdm = "Location.Delete.Mdm";
        public const string ViewOwnLocation = "Location.View.Own";
        public const string ViewLocationWithinTenant = "Location.View.Tenant";
        public const string ViewLocationWithinMdm = "Location.View.Mdm";
        public const string ShareLocationWithinTenant = "Location.Share.Tenant";
        public const string ShareLocationWithinMdm = "Location.Share.Mdm";

        //Zone
        public static List<string> Zone()
        {
            return new List<string>
            {
                CreateZone,
                UpdateOwnZone,
                UpdateZoneWithinTenant,
                UpdateZoneWithinMdm,
                DeleteOwnZone,
                DeleteZoneWithinTenant,
                DeleteZoneWithinMdm,
                ViewOwnZone,
                ViewZoneWithinTenant,
                ViewZoneWithinMdm,
                ShareZoneWithinTenant,
                ShareZoneWithinMdm
           };
        }
        public const string CreateZone = "Zone.Create";
        public const string UpdateOwnZone = "Zone.Update.Own";
        public const string UpdateZoneWithinTenant = "Zone.Update.Tenant";
        public const string UpdateZoneWithinMdm = "Zone.Update.Mdm";
        public const string DeleteOwnZone = "Zone.Delete.Own";
        public const string DeleteZoneWithinTenant = "Zone.Delete.Tenant";
        public const string DeleteZoneWithinMdm = "Zone.Delete.Mdm";
        public const string ViewOwnZone = "Zone.View.Own";
        public const string ViewZoneWithinTenant = "Zone.View.Tenant";
        public const string ViewZoneWithinMdm = "Zone.View.Mdm";
        public const string ShareZoneWithinTenant = "Zone.Share.Tenant";
        public const string ShareZoneWithinMdm = "Zone.Share.Mdm";

        //Filter
        public static List<string> Filter()
        {
            return new List<string>
            {
                CreateFilter,
                UpdateOwnFilter,
                UpdateFilterWithinTenant,
                UpdateFilterWithinMdm,
                DeleteOwnFilter,
                DeleteFilterWithinTenant,
                DeleteFilterWithinMdm,
                ViewOwnFilter,
                ViewFilterWithinTenant,
                ViewFilterWithinMdm,
                ShareFilterWithinTenant,
                ShareFilterWithinMdm
            };
        }
        public const string CreateFilter = "Filter.Create";
        public const string UpdateOwnFilter = "Filter.Update.Own";
        public const string UpdateFilterWithinTenant = "Filter.Update.Tenant";
        public const string UpdateFilterWithinMdm = "Filter.Update.Mdm";
        public const string DeleteOwnFilter = "Filter.Delete.Own";
        public const string DeleteFilterWithinTenant = "Filter.Delete.Tenant";
        public const string DeleteFilterWithinMdm = "Filter.Delete.Mdm";
        public const string ViewOwnFilter = "Filter.View.Own";
        public const string ViewFilterWithinTenant = "Filter.View.Tenant";
        public const string ViewFilterWithinMdm = "Filter.View.Mdm";
        public const string ShareFilterWithinTenant = "Filter.Share.Tenant";
        public const string ShareFilterWithinMdm = "Filter.Share.Mdm";

        //Report
        public static List<string> Report()
        {
            return new List<string>
            {
                CreateReport,
                UpdateOwnReport,
                UpdateReportWithinTenant,
                UpdateReportWithinMdm,
                DeleteOwnReport,
                DeleteReportWithinTenant,
                DeleteReportWithinMdm,
                ViewOwnReport,
                ViewReportWithinTenant,
                ViewReportWithinMdm,
                ShareReportWithinTenant,
                ShareReportWithinMdm
            };
        }
        public const string CreateReport = "Report.Create";
        public const string UpdateOwnReport = "Report.Update.Own";
        public const string UpdateReportWithinTenant = "Report.Update.Tenant";
        public const string UpdateReportWithinMdm = "Report.Update.Mdm";
        public const string DeleteOwnReport = "Report.Delete.Own";
        public const string DeleteReportWithinTenant = "Report.Delete.Tenant";
        public const string DeleteReportWithinMdm = "Report.Delete.Mdm";
        public const string ViewOwnReport = "Report.View.Own";
        public const string ViewReportWithinTenant = "Report.View.Tenant";
        public const string ViewReportWithinMdm = "Report.View.Mdm";
        public const string ShareReportWithinTenant = "Report.Share.Tenant";
        public const string ShareReportWithinMdm = "Report.Share.Mdm";


        //Alarm

        public static List<string> Alarm()
        {
            return new List<string>
            {
                CreateAlarm,
                UpdateOwnAlarm,
                UpdateAlarmWithinTenant,
                UpdateAlarmWithinMdm,
                DeleteOwnAlarm,
                DeleteAlarmWithinTenant,
                DeleteAlarmWithinMdm,
                ViewOwnAlarm,
                ViewAlarmWithinTenant,
                ViewAlarmWithinMdm,
                ShareAlarmWithinTenant,
                ShareAlarmWithinMdm
            };
        }
        public const string CreateAlarm = "Alarm.Create";
        public const string UpdateOwnAlarm = "Alarm.Update.Own";
        public const string UpdateAlarmWithinTenant = "Alarm.Update.Tenant";
        public const string UpdateAlarmWithinMdm = "Alarm.Update.Mdm";
        public const string DeleteOwnAlarm = "Alarm.Delete.Own";
        public const string DeleteAlarmWithinTenant = "Alarm.Delete.Tenant";
        public const string DeleteAlarmWithinMdm = "Alarm.Delete.Mdm";
        public const string ViewOwnAlarm = "Alarm.View.Own";
        public const string ViewAlarmWithinTenant = "Alarm.View.Tenant";
        public const string ViewAlarmWithinMdm = "Alarm.View.Mdm";
        public const string ShareAlarmWithinTenant = "Alarm.Share.Tenant";
        public const string ShareAlarmWithinMdm = "Alarm.Share.Mdm";

        //Playback
        public static List<string> Playback()
        {
            return new List<string>
            {
                RunPlayback
            };
        }
        public const string RunPlayback = "Playback.Run";

        //Route
        public static List<string> Route()
        {
            return new List<string>
            {
                Route_Create,
                Route_Update_Own,
                Route_Update_Mdm,
                Route_Update_Tenant,
                Route_Delete_Own,
                Route_Delete_Mdm,
                Route_Delete_Tenant,
                Route_View_Own,
                Route_View_Mdm,
                Route_View_Tenant,
                Route_Share_Mdm,
                Route_Share_Tenant,
                VesselETA_View_Mdm,
                RouteETA_View_Mdm
            };
        }
        public const string Route_Create = "Route.Create";
        public const string Route_Update_Own = "Route.Update.Own";
        public const string Route_Update_Mdm = "Route.Update.Mdm";
        public const string Route_Update_Tenant = "Route.Update.Tenant";
        public const string Route_Delete_Own = "Route.Delete.Own";
        public const string Route_Delete_Mdm = "Route.Delete.Mdm";
        public const string Route_Delete_Tenant = "Route.Delete.Tenant";
        public const string Route_View_Own = "Route.View.Own";
        public const string Route_View_Mdm = "Route.View.Mdm";
        public const string Route_View_Tenant = "Route.View.Tenant";
        public const string Route_Share_Mdm = "Route.Share.Mdm";
        public const string Route_Share_Tenant = "Route.Share.Tenant";
        public const string VesselETA_View_Mdm = "VesselETA.View.Mdm";
        public const string RouteETA_View_Mdm = "RouteETA.View.Mdm";

        //User
        public static List<string> User()
        {
            return new List<string>
            {
                User_Create_Mdm,
                User_Create_Tenant,
                User_Update_Mdm,
                User_Update_Tenant,
                User_View_Mdm,
                User_View_Tenant,
                User_Delete_Mdm,
                User_Delete_Tenant,
                User_Deactivate_Mdm,
                User_Deactivate_Tenant
            };
        }
        public const string User_Create_Mdm = "User.Create.Mdm";
        public const string User_Create_Tenant = "User.Create.Tenant";
        public const string User_Update_Mdm = "User.Update.Mdm";
        public const string User_Update_Tenant = "User.Update.Tenant";
        public const string User_View_Mdm = "User.View.Mdm";
        public const string User_View_Tenant = "User.View.Tenant";
        public const string User_Delete_Mdm = "User.Delete.Mdm";
        public const string User_Delete_Tenant = "User.Delete.Tenant";
        public const string User_Deactivate_Mdm = "User.Deactivate.Mdm";
        public const string User_Deactivate_Tenant = "User.Deactivate.Tenant";
        //Tenant
        public static List<string> Tenant()
        {
            return new List<string>
            {
                Tenant_Create_Mdm,
                //Tenant_Create_Tenant,
                Tenant_Update_Mdm,
                //Tenant_Update_Tenant,
                Tenant_Delete_Mdm,
                //Tenant_Delete_Tenant,
                Tenant_View_Mdm,
                //Tenant_View_Tenant
            };
        }
        public const string Tenant_Create_Mdm = "Tenant.Create.Mdm";
        //public const string Tenant_Create_Tenant = "Tenant.Create.Tenant";
        public const string Tenant_Update_Mdm = "Tenant.Update.Mdm";
        //public const string Tenant_Update_Tenant = "Tenant.Update.Tenant";
        public const string Tenant_View_Mdm = "Tenant.View.Mdm";
        //public const string Tenant_View_Tenant = "Tenant.View.Tenant";
        public const string Tenant_Delete_Mdm = "Tenant.Delete.Mdm";
        //public const string Tenant_Delete_Tenant = "Tenant.Delete.Tenant";

        //Role - Permission
        public static List<string> Role()
        {
            return new List<string>
            {
                Role_Create_Mdm,
                Role_Create_Tenant,
                Role_Update_Mdm,
                Role_Update_Tenant,
                Role_View_Mdm,
                Role_View_Tenant,
                Role_Delete_Mdm,
                Role_Delete_Tenant,
                Role_Assign_Mdm,
                Role_Assign_Tenant
            };
        }
        public const string Role_Create_Mdm = "Role.Create.Mdm";
        public const string Role_Create_Tenant = "Role.Create.Tenant";
        public const string Role_Update_Mdm = "Role.Update.Mdm";
        public const string Role_Update_Tenant = "Role.Update.Tenant";
        public const string Role_View_Mdm = "Role.View.Mdm";
        public const string Role_View_Tenant = "Role.View.Tenant";
        public const string Role_Delete_Mdm = "Role.Delete.Mdm";
        public const string Role_Delete_Tenant = "Role.Delete.Tenant";
        public const string Role_Assign_Mdm = "Role.Assign.Mdm";
        public const string Role_Assign_Tenant = "Role.Assign.Tenant";

        //Cable

        public static List<string> Cable()
        {
            return new List<string>
            {
                Cable_Create,
                Cable_Update_Own,
                Cable_Update_Mdm,
                Cable_Update_Tenant,
                Cable_Delete_Own,
                Cable_Delete_Mdm,
                Cable_Delete_Tenant,
                Cable_View_Own,
                Cable_View_Mdm,
                Cable_View_Tenant,
                Cable_Share_Mdm,
                Cable_Share_Tenant
            };
        }
        public const string Cable_Create = "Cable.Create";
        public const string Cable_Update_Own = "Cable.Update.Own";
        public const string Cable_Update_Mdm = "Cable.Update.Mdm";
        public const string Cable_Update_Tenant = "Cable.Update.Tenant";
        public const string Cable_Delete_Own = "Cable.Delete.Own";
        public const string Cable_Delete_Mdm = "Cable.Delete.Mdm";
        public const string Cable_Delete_Tenant = "Cable.Delete.Tenant";
        public const string Cable_View_Own = "Cable.View.Own";
        public const string Cable_View_Mdm = "Cable.View.Mdm";
        public const string Cable_View_Tenant = "Cable.View.Tenant";
        public const string Cable_Share_Mdm = "Cable.Share.Mdm";
        public const string Cable_Share_Tenant = "Cable.Share.Tenant";


        //Methydro
        public static List<string> Methydro()
        {
            return new List<string>
            {
               Methydro_View_Mdm
            };
        }
        public const string Methydro_View_Mdm = "Methydro.View.Mdm";

        //Remote Monitoring System (RMS)
        public static List<string> RMS()
        {
            return new List<string>
            {
               RMS_View_Mdm
            };
        }
        public const string RMS_View_Mdm = "RMS.View.Mdm";

        //DDMS
        public static List<string> DDMS()
        {
            return new List<string>
            {
               DDMS_View_Mdm
            };
        }
        public const string DDMS_View_Mdm = "DDMS.View.Mdm";

        //Aton Manager
        public static List<string> AtonManager()
        {
            return new List<string>
            {
               Aton_View_Mdm
            };
        }
        public const string Aton_View_Mdm = "Aton.View.Mdm";

        //User Personalization
        public static List<string> UserPersonalization()
        {
            return new List<string>
            {
               UserPersonalization_Create
            };
        }
        public const string UserPersonalization_Create = "UserPersonalization.Create";

        //Tenant Personalization
        public static List<string> TenantPersonalization()
        {
            return new List<string>
            {
               TenantPersonalization_Create
            };
        }
        public const string TenantPersonalization_Create = "TenantPersonalization.Create";

        //MyWREMS
        public static List<string> MyWREMS()
        {
            return new List<string>
            {
                MyWREMS_View_Mdm
            };
        }
        public const string MyWREMS_View_Mdm = "MyWREMS.View.Mdm";


        public static List<string> EICS()
        {
            return new List<string>
            {
                EICS_View
            };
        }
        public const string EICS_View = "EICS_View";

        public static List<string> Images()
        {
            return new List<string>
            {
                Image_Upload,
                Image_Delete
            };
        }
        public const string Image_Upload = "Image.Upload";
        public const string Image_Delete = "Image.Delete";


        public static List<string> VesselGroup()
        {
            return new List<string>
            {
                VesselGroup_Create,
                VesselGroup_View_MDM,
                VesselGroup_View_Tenant,
                VesselGroup_View_Own,
                VesselGroup_Update_MDM,
                VesselGroup_Update_Tenant,
                VesselGroup_Update_Own,
                VesselGroup_Delete_MDM,
                VesselGroup_Delete_Tenant,
                VesselGroup_Delete_Own
            };
        }
        public const string VesselGroup_Create = "VesselGroup.Create";
        public const string VesselGroup_View_MDM = "VesselGroup.View.Mdm";
        public const string VesselGroup_View_Tenant = "VesselGroup.View.Tenant";
        public const string VesselGroup_View_Own = "VesselGroup.View.Own";
        public const string VesselGroup_Update_MDM = "VesselGroup.Update.Mdm";
        public const string VesselGroup_Update_Tenant = "VesselGroup.Update.Tenant";
        public const string VesselGroup_Update_Own = "VesselGroup.Update.Own";
        public const string VesselGroup_Delete_MDM = "VesselGroup.Delete.Mdm";
        public const string VesselGroup_Delete_Tenant = "VesselGroup.Delete.Tenant";
        public const string VesselGroup_Delete_Own = "VesselGroup.Delete.Own";

        public static List<string> TenantFilter()
        {
            return new List<string>
            {
               TenantFilter_Create,
               TenantFilter_View_MDM,
               TenantFilter_View_Tenant,
               TenantFilter_Assign
            };
        }

        public const string TenantFilter_Create = "TenantFilter.Create";
        public const string TenantFilter_View_MDM = "TenantFilter.View.Mdm";
        public const string TenantFilter_View_Tenant = "TenantFilter.View.Tenant";
        public const string TenantFilter_Assign = "TenantFilter.Assign";
    }
}
