﻿using System.Collections.Generic;

namespace Vtmis.Core.Common.Constants
{
    public static class NavStatusConst
    {
        public static List<string> All()
        {
            return new List<string>
            {
                UTW_Using_Engine,
                AtAnchor,
                NotUnderCommand,
                RestrictManeu,
                ConstraintDraught,
                Moored,
                Aground,
                EngagedFishing,
                UTW_Sailing,
                ReservedCarryCat_C,
                ReservedCarryCat_A,
                PowerDrivenVesselTowing,
                PowerDrivenVesselPush,
                ReservedFutureUse,
                AisSartActiveETC,
                NotDefined
            };
        }

        public const string UTW_Using_Engine = "Under way using engine";
        public const string AtAnchor = "At anchor";
        public const string NotUnderCommand = "Not under command";
        public const string RestrictManeu = "Restricted maneuverability";
        public const string ConstraintDraught = "Constrained by her draught";
        public const string Moored = "Moored";
        public const string Aground = "Aground";
        public const string EngagedFishing = "Engaged in fishing";
        public const string UTW_Sailing = "Under way sailing";
        public const string ReservedCarryCat_C = "DG,HS,MP(C),HSC";
        public const string ReservedCarryCat_A = "DG,HS,MP(A),WIG";
        public const string PowerDrivenVesselTowing = "Towing astern";
        public const string PowerDrivenVesselPush = "Pushing or towing alongside";
        public const string ReservedFutureUse = "Reserved (13)";
        public const string AisSartActiveETC = "AIS-SART";
        public const string NotDefined = "Not Defined";
    }
}
