﻿namespace Vtmis.Core.Common.Constants.Mongo
{
    public static class MongoConst
    {
        public static string AIS_DB = "AIS";
        public static string TargetInfoCollectionName = "TargetInfos";
        public static string TargetPositionCollectionName = "TargetPositions";
        public static string TargetMessageCollectionName = "TargetMessages";
        public const string FullMessages = "FullMessages";
        public const string RMSMessages = "RMSMessages";
        public const string MethydroMessages = "MethydroMessages";
        public const string DDMSMessages = "DDMSMessages";
    }
}
