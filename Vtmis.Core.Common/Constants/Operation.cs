﻿namespace Vtmis.Core.Common.Constants
{
    public class Operation
    {
        public const string EQUALS_TO = "=";
        public const string MORE_THAN = ">";
        public const string LESS_THAN = "<";
        public const string MORE_THAN_OR_EQUALS_TO = ">=";
        public const string LESS_THAN_OR_EQUALS_TO = "<=";
        public const string NOT_EQUALS_TO = "!=";
        public const string START_WITH = "StartWith";
        public const string CONTAINS = "Contains";

        public static string SQLOp(string val, string operation)
        {
            if (operation == START_WITH)
            {
                return $"LIKE {val}%";
            }
            if (operation == CONTAINS)
            {
                return $"LIKE %{val}%";
            }
            if (operation == EQUALS_TO)
            {
                return $"{EQUALS_TO} {val}";
            }
            if (operation == NOT_EQUALS_TO)
            {
                return $"{NOT_EQUALS_TO} {val}";
            }
            return null;
        }
    }
}
