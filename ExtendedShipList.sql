USE [VTS]
GO
/****** Object:  Table [dbo].[ExtendedShipList]    Script Date: 14/2/2019 5:06:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExtendedShipList](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsBlackList] [bit] NOT NULL,
	[MMSI] [int] NOT NULL,
	[CallSign] [nvarchar](50) NULL,
	[IMO] [nvarchar](max) NULL,
	[OfficialNo] [nvarchar](max) NULL,
	[VesselName] [nvarchar](max) NULL,
	[VesselCategory] [int] NOT NULL,
	[VesselType] [nvarchar](max) NULL,
	[Value] [int] NOT NULL,
	[ValueCurrency] [nvarchar](10) NULL,
	[PortOfRegistry] [nvarchar](max) NULL,
	[TradingArea] [nvarchar](max) NULL,
	[PlyingLimit] [int] NOT NULL,
	[NoOfDeck] [int] NOT NULL,
	[NoOfHull] [int] NOT NULL,
	[HullMaterial] [nvarchar](max) NULL,
	[Length] [float] NOT NULL,
	[Breadth] [float] NOT NULL,
	[Draught] [float] NOT NULL,
	[AnnualTonnageDue] [float] NOT NULL,
	[GrossTonnage] [float] NOT NULL,
	[NetTonnage] [float] NOT NULL,
	[DeadWeight] [float] NOT NULL,
	[EngineDescription] [nvarchar](max) NULL,
	[PropulsionType] [nvarchar](max) NULL,
	[NumberOfEngine] [int] NOT NULL,
	[YardNumber] [int] NOT NULL,
	[DateOfBuildingContract] [datetime] NULL,
	[DateOfDelivery] [datetime] NULL,
	[DateOfKeelLaid] [datetime] NULL,
	[PlaceOfBuilt] [nvarchar](max) NULL,
	[BuilderName] [nvarchar](max) NULL,
	[BuilderAddress1] [nvarchar](max) NULL,
	[BuilderAddress2] [nvarchar](max) NULL,
	[BuilderAddress3] [nvarchar](max) NULL,
	[BuilderCountryCode] [nvarchar](10) NULL,
	[TotalEnginePower] [float] NOT NULL,
	[Speed] [float] NOT NULL,
	[NumberOfCabin] [int] NOT NULL,
	[NoOfPassenger] [int] NOT NULL,
	[TotalNumberOfCrew] [int] NOT NULL,	
	[Status] [int] NOT NULL,
	[BuilderPostCode] [nvarchar](max) NULL,
	[TypeOfRegistry] [nvarchar](max) NULL,
	[CountryFlag] [nvarchar](50) NULL,
	[Class] [nvarchar](max) NULL,
	[IsCharter] [bit] NOT NULL,
	[LocalityStatus] [nvarchar](max) NULL,
	[Beam] [float] NOT NULL,
	[Propel] [float] NOT NULL,
	[CreatedDate] [datetime] NULL,
	[LastModifiedDate] [datetime] NULL,
	[DeletedDate] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_ExtendedShipList] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
