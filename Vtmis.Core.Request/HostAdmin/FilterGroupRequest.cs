﻿using Vtmis.Core.Common.Constants;
using Flurl.Http;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.DTO.Filter;

namespace Vtmis.Core.Request.HostAdmin
{
    public class FilterGroupRequest
    {
        private readonly string _hostDomain;
        private readonly string _base;
        public FilterGroupRequest()
        {
            _hostDomain = string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.STAGING,
                            System.StringComparison.CurrentCultureIgnoreCase) ? AppConst.HostDomainStaging :
                            AppConst.HostDomain;

            _base = $"{AppConst.Protocol}{_hostDomain}:{AppConst.HostPort}";
        }
      
        public async Task<FilterGroupByIdResponse> GetFilterGroupById(int id)
        {
            var uri = $"{_base}/api/services/app/FilterGroupService/GetById?id={id}";
            return await uri.GetJsonAsync<FilterGroupByIdResponse>();
        }       
    }
}
