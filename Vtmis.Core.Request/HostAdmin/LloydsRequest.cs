﻿using Flurl.Http;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.ViewModels.Lloyds;

namespace Vtmis.Core.Request.HostAdmin
{
    public class LloydsRequest
    {
        private readonly string _hostDomain;
        private readonly string _base;
        public LloydsRequest()
        {
            _hostDomain = string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.STAGING,
                            System.StringComparison.CurrentCultureIgnoreCase) ? AppConst.HostDomainStaging :
                            AppConst.HostDomain;

            _base = $"{AppConst.Protocol}{_hostDomain}:{AppConst.HostPort}";
        }

        //private readonly string _base = $"{AppConst.Protocol}{AppConst.HostDomain}:{AppConst.HostPort}";
        public async Task<ResponseLloyds> GetAsync(int? imo, string callSign, string targetName)
        {
            var uri = $"{_base}/api/services/app/LloydsService/GetAsync?imo={imo}&&callSign={callSign}&&targetName={targetName}";
            return await uri.GetJsonAsync<ResponseLloyds>();
        }
    }
}
