﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtmis.Core.Request.Model.MEH
{
    public class MEHPaginate<T>
    {
        public MEHPaginate()
        {
            Items = new List<T>();
        }
        public int PageNo { get; set; }
        public int TotalItems { get; set; }
        public int PerPage { get; set; }
        public int TotalPages { get; set; }
        public List<T> Items { get; set; }
    }
}
