﻿using System;
using System.Collections.Generic;

namespace Vtmis.Core.Request.Model.MEH
{
    public class MEHIncidentLocationSource
    {
        public float Lat { get; set; }
        public float Lng { get; set; }
    }    
    public class MEHIncidentSource
    {
        public MEHIncidentSource()
        {
            Location = new List<List<MEHIncidentLocationSource>>();
        }
        public int EntryId { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime? EntryModified { get; set; }
        public string MetaStatus { get; set; }
        public string MetaType { get; set; }
        public DateTime? Date { get; set; }
        public string TypeOfCasualty { get; set; }
        public string CoordType { get; set; }
        public List<List<MEHIncidentLocationSource>> Location { get; set; }
        public int? LossOfLife { get; set; }

        public string MarinePollution { get; set; }

        public string Visibility { get; set; }

        public string SeaState { get; set; }

        public string ForceWind { get; set; }

        public string VesselNameA { get; set; }

        public string CallSignA { get; set; }

        public int? MmsiA { get; set; }

        public string TypeA { get; set; }

        public string FlagA { get; set; }

        public string DestinationA { get; set; }

        public string CargoA { get; set; }

        public string VesselNameB { get; set; }

        public string CallSignB { get; set; }

        public int? MmsiB { get; set; }

        public string TypeB { get; set; }

        public string FlagB { get; set; }

        public string DestinationB { get; set; }

        public string CargoB { get; set; }
        public string AccidentDescription { get; set; }

        public string OperatorInCharge { get; set; }
    }
}
