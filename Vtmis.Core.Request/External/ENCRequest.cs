﻿using Flurl.Http;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Core.Request.External
{
    public class ENCRequest
    {
        private string _base = AppHelper.GetENCTokenUrl();
        public async Task<string> GetToken()
        {
            try
            {
                string referer = HostingEnvironment.PROD_URL;
                if (AppHelper.GetEnvironmentName().IsEqual(HostingEnvironment.POC) || AppHelper.GetEnvironmentName().IsEqual(HostingEnvironment.DEVELOPMENT))
                {
                    referer = HostingEnvironment.DEV_URL;
                }
                else if (AppHelper.GetEnvironmentName().IsEqual(HostingEnvironment.STAGING))
                {
                    referer = HostingEnvironment.STAGING_URL;
                }
                //_base = $"{AppConst.Protocol}{referer}:{AppConst.HostPort}";

                var uri = $"{_base}";
                //var result = await uri.PostJsonAsync(new { username = "Greenfinder", password = "P@ssw0rd!", client = "referer", referer = "localhost", expiration = 20160, f = "pjson" });
                FlurlClient flurlClient = new FlurlClient(uri);
                flurlClient.Configure(settings => settings.Timeout = TimeSpan.FromSeconds(10));
                //var result = await flurlClient.Request("").GetAsync();
                var result = await flurlClient.Request("").PostMultipartAsync(p =>
                       p
                //.AddStringParts(new { username = "Greenfinder", password = "P@ssw0rd!", client = "referer", referer = "localhost", expiration = 20160, f = "pjson" })
                .AddString("username", AppHelper.GetENCUsername())
                 .AddString("password", AppHelper.GetENCPassword())
                 .AddString("client", "referer")
                 .AddString("referer", referer)
                 .AddString("expiration", "20160")
                 .AddString("f", "pjson")
                );
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var json = await result.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ENCResponse>(json).Token;
                }
            }
            catch (System.Exception err)
            {
                //throw err;
            }
         
            return null;
        }
    }

    public class ENCResponse
    {
        public string Token { get; set; }
        public long Expires { get; set; }
        public bool Ssl { get; set; }
    }
}
