﻿using Flurl.Http;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Request.Model.MEH;

namespace Vtmis.Core.Request.External
{
    public class MEHAuth
    {
        public string Token { get; set; }
    }
    public class MEHRequest
    {
        private readonly MEHObj _mehURI;

        public MEHRequest()
        {
            _mehURI = AppHelper.GetMEHURI();
        }
        public async Task<MEHAuth> GetTokenAsync(string username, string password)
        {
            try
            {
                var uri = $"{_mehURI.BaseURI}{_mehURI.TokenAuthURI}";
                FlurlClient flurlClient = new FlurlClient(uri);
                flurlClient.Configure(settings => settings.Timeout = TimeSpan.FromSeconds(5));
                var result = await flurlClient.Request("").PostJsonAsync(new
                {
                    username,
                    password
                });
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var json = await result.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<MEHAuth>(json); ;
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public async Task<MEHPaginate<MEHIncidentSource>> GetIncidentReportAsync(DateTime startDate, bool isDataAvailable)
        {
            try
            {
                if (isDataAvailable == false)
                {
                    startDate = new DateTime(1990, 01, 01);
                }
                var token = await GetTokenAsync(_mehURI.Username, _mehURI.Password);
                if (token == null)
                {
                    return null;
                }
                if (token.Token == null)
                {
                    return null;
                }
                var uri = $"{_mehURI.BaseURI}{_mehURI.IncidentReportURI}/?fromDate={startDate.StartOfDay().ToString("yyyy-MM-dd")}&perPage=1000";
                FlurlClient flurlClient = new FlurlClient(uri);
                flurlClient.Configure(settings => settings.Timeout = TimeSpan.FromSeconds(5));
                var result = await flurlClient.WithHeader("Authorization", $"Bearer {token.Token}")
                    .Request("").GetAsync();
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var json = await result.Content.ReadAsStringAsync();
                    var resultObj = JsonConvert.DeserializeObject<MEHPaginate<MEHIncidentSource>>(json);
                    return resultObj;
                }
                return null;
            }
            catch (Exception err)
            {
                return null;
            }
        }
    }
}
