﻿using Flurl.Http;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.DTO.Image;

namespace Vtmis.Core.Request.Image
{
    public class ImageRequest
    {
        public async Task<string> UploadImageAsync(UploadImageDto input)
        {
            var imageApi = $"{AppHelper.GetImageProcessingApiBaseUri()}/{AppHelper.GetImageProcessingApiUploadUri()}";
            var result = await imageApi.PostJsonAsync(new
            {
                name = input.Name,
                type = input.Type,
                path = input.Path,
                imageData = input.ImageData,
                decorate = new
                {
                    watermark = new
                    {
                        replace = true,
                        text = "eNav Malaysia",
                        color = "white",
                        fontSize = "20" 
                    }
                }
            });
            if (result.IsSuccessStatusCode)
            {
                return await result.Content.ReadAsStringAsync();
            }
            return null;
        }

        public async Task<string> DeleteImageAsync(DeleteImageDto input)
        {
            var imageApi = $"{AppHelper.GetImageProcessingApiBaseUri()}/images/delete";
            var result = await imageApi.PostJsonAsync(new { name = input.Name, path = input.Path });
            if (result.IsSuccessStatusCode)
            {
                return await result.Content.ReadAsStringAsync();
            }
            return null;
        }
    }
}
