﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vtmis.WebAdmin.VesselOwneds.Dto
{
    public class VesselToTenantAssignedDto
    {
        public long UserId { get; set; }
        public int MMSI { get; set; }
    }
}
