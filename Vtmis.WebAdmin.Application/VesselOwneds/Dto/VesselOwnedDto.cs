﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vtmis.WebAdmin.VesselOwneds.Dto
{
    public class VesselOwnedDto
    {
        public int Id { get; set; }

        public int TenantId { get; set; }
        public DateTime CreationTime { get; set; }

        public virtual long UserId { get; set; }

        public string Username { get; set; }

        public string MMSI { get; set; }
    }
}
