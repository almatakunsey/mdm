﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vtmis.WebAdmin.VesselOwneds.Dto
{
    public class AssignVesselToTenantDto
    {
        public long UserId { get; set; }
        public int[] MMSI { get; set; }
    }

    public class VesselArrayIdDto
    {
        public int MMSI { get; set; }
    }
}
