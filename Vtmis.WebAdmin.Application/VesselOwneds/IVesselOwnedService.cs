﻿using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using Vtmis.WebAdmin.VesselOwneds.Dto;

namespace Vtmis.WebAdmin.VesselOwneds
{
    public interface IVesselOwnedService
    {
        Task<VesselOwnedDto> CreateAsync(AssignVesselToTenantDto input);
        Task<VesselOwnedDto> RemoveAsync(VesselToTenantAssignedDto input);
        ListResultDto<VesselArrayIdDto> GetVesselToTenantAssignedListByTenantId(int? tenantId);
        Task<ListResultDto<VesselArrayIdDto>> GetVesselToTenantAssignedListByUserIdAsync();
        //Task<ListResultDto<VesselArrayIdDto>> GetVesselToTenantAssignedListByUserIdAsync(long userId);
        Task<ListResultDto<VesselArrayIdDto>> GetVesselToTenantAssignedListByUsernameAsync(string username);
        //Task<VesselOwnedDto> CreateAsync(AssignMultipleVesselToTenantDto input)
    }
}
