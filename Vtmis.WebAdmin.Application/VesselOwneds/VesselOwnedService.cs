﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Text;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.VesselOwneds.Dto;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Vtmis.WebAdmin.Roles;
using Vtmis.WebAdmin.Users;
using Microsoft.EntityFrameworkCore;

namespace Vtmis.WebAdmin.VesselOwneds
{
    //[AbpAuthorize]
    public class VesselOwnedService : ApplicationService, IVesselOwnedService
    {
        private readonly IRepository<VesselOwned> _vesselOwnedRepository;
        private readonly UserManager _userManager;
        private readonly IRoleAppService _roleAppService;
        private readonly IUserAppService _userAppService;

        public VesselOwnedService(IRepository<VesselOwned> vesselOwnedRepository,
            UserManager userManager,
            IRoleAppService roleAppService,
            IUserAppService userAppService
            )
        {
            _vesselOwnedRepository = vesselOwnedRepository;
            _userManager = userManager;
            _roleAppService = roleAppService;
            _userAppService = userAppService;
        }

        //[UnitOfWork]
        public async Task<VesselOwnedDto> CreateAsync(AssignVesselToTenantDto input)
        {
            //Contract Validation
            if (!input.MMSI.Any())
            {
                throw new Exception("Invalid mmsi input.");
            }

            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var user = await _userManager.Users.IgnoreQueryFilters()
                            .FirstOrDefaultAsync(x => x.Id == input.UserId);

                if (user != null)
                {
                    var _vesselOwned = _vesselOwnedRepository.FirstOrDefault(c => c.UserId == user.Id);

                    if (_vesselOwned != null)
                    {
                        var vesselArrayId = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VesselArrayIdDto>>(_vesselOwned.MMSI);

                        foreach (var _mmsi in input.MMSI)
                        {
                            if (!vesselArrayId.Where(x => x.MMSI == _mmsi).Any())
                            {
                                vesselArrayId.Add(new VesselArrayIdDto { MMSI = _mmsi });
                            }
                        }

                        var jsonVesselId = Newtonsoft.Json.JsonConvert.SerializeObject(vesselArrayId);

                        _vesselOwned.MMSI = jsonVesselId;

                        var newVesselVo = await _vesselOwnedRepository.InsertOrUpdateAsync(_vesselOwned);
                        await CurrentUnitOfWork.SaveChangesAsync();
                        return new VesselOwnedDto { Id = newVesselVo.Id, CreationTime = newVesselVo.CreationTime, TenantId = newVesselVo.TenantId.Value, MMSI = newVesselVo.MMSI, UserId = newVesselVo.UserId, Username = newVesselVo.Username };
                    }
                    else
                    {
                        var vesselArrayId = new List<VesselArrayIdDto>();

                        foreach (var _mmsi in input.MMSI.Distinct())
                        {
                            vesselArrayId.Add(new VesselArrayIdDto { MMSI = _mmsi });
                        }

                        var jsonVesselId = Newtonsoft.Json.JsonConvert.SerializeObject(vesselArrayId);

                        VesselOwned vo = new VesselOwned { CreationTime = DateTime.Now, TenantId = user.TenantId.Value, User = user, UserId = user.Id, Username = user.UserName, MMSI = jsonVesselId };

                        var newVesselVo = await _vesselOwnedRepository.InsertOrUpdateAsync(vo);
                        await CurrentUnitOfWork.SaveChangesAsync();
                        return new VesselOwnedDto { Id = newVesselVo.Id, CreationTime = newVesselVo.CreationTime, TenantId = newVesselVo.TenantId.Value, MMSI = newVesselVo.MMSI, UserId = newVesselVo.UserId, Username = newVesselVo.Username };
                    }
                }
                else
                {
                    throw new Exception("User not found!");
                }
            }
        }

        public async Task<VesselOwnedDto> RemoveAsync(VesselToTenantAssignedDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var user = _userManager.Users.Where(c => c.Id == input.UserId).FirstOrDefault();

                if (user != null)
                {
                    var _vesselOwned = _vesselOwnedRepository.Single(c => c.UserId == user.Id);

                    if (_vesselOwned != null)
                    {
                        var vesselArrayId = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VesselArrayIdDto>>(_vesselOwned.MMSI);

                        var mmsiToRemove = vesselArrayId.SingleOrDefault(c => c.MMSI == input.MMSI);

                        vesselArrayId.Remove(mmsiToRemove);

                        var jsonVesselId = Newtonsoft.Json.JsonConvert.SerializeObject(vesselArrayId);

                        _vesselOwned.MMSI = jsonVesselId;

                        var newVesselVo = await _vesselOwnedRepository.InsertOrUpdateAsync(_vesselOwned);

                        return new VesselOwnedDto { Id = newVesselVo.Id, CreationTime = newVesselVo.CreationTime, TenantId = newVesselVo.TenantId.Value, MMSI = newVesselVo.MMSI, UserId = newVesselVo.UserId, Username = newVesselVo.Username };

                    }
                    else
                    {
                        throw new Exception("Record vessel owned not found!");
                    }
                }
                else
                {
                    throw new Exception("User not found!");
                }
            }
        }

        //Get VesselOwned based on Tenant
        public ListResultDto<VesselArrayIdDto> GetVesselToTenantAssignedListByTenantId(int? tenantId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {

                var _vesselOwned = _vesselOwnedRepository.GetAll()
                                    .AsNoTracking().IgnoreQueryFilters().Where(s => s.TenantId == tenantId);  //await _vesselOwnedRepository.FirstOrDefaultAsync(c => c.UserId == user.Id);

                if (_vesselOwned != null)
                {
                    var vessels = _vesselOwned.Select(x => x.MMSI);
                    var col = new Dictionary<int, List<VesselArrayIdDto>>();
                    var count = 1;
                    foreach (var item in vessels)
                    {
                        col.Add(count,
                           Newtonsoft.Json.JsonConvert
                               .DeserializeObject<List<VesselArrayIdDto>>(item));
                        count += 1;
                    }
                    var results = new List<VesselArrayIdDto>();
                    foreach (var item in col.Values)
                    {
                        foreach (var item2 in item)
                        {
                            if (!results.Where(x => x.MMSI == item2.MMSI).Any())
                            {
                                results.Add(item2);
                            }
                        }
                    }
                    return new ListResultDto<VesselArrayIdDto>(results);
                }
                else
                {
                    return new ListResultDto<VesselArrayIdDto>();
                }

            }
        }

        //public async Task<ListResultDto<VesselArrayIdDto>> GetVesselToTenantAssignedListByUserIdAsync(long userId)
        //{
        //    try
        //    {


        //        var user = await _userAppService.GetUserByIdAsync(userId);

        //        //var _vesselOwned = await _vesselOwnedRepository
        //        //    .FirstOrDefaultAsync(s => s.UserId == AbpSession.UserId);

        //        var isTenantAdmin = false;
        //        //var isHostAdmin = false;

        //        foreach (var item in user.RoleNames.ToList())
        //        {
        //            //var role = await _roleAppService.Get(item.RoleId);

        //            if (item.ToLower().Contains("admin"))
        //            {
        //                isTenantAdmin = true;
        //                break;
        //            }
        //        }

        //        if (AbpSession.TenantId == null && isTenantAdmin == true)
        //        {
        //            //isHostAdmin = true;
        //            isTenantAdmin = false;
        //        }

        //        var _vesselOwned = _vesselOwnedRepository.GetAll().AsQueryable();

        //        if (isTenantAdmin)
        //        {
        //            _vesselOwned = _vesselOwned.Where(s => s.TenantId == AbpSession.TenantId);
        //        }
        //        else
        //        {
        //            _vesselOwned = _vesselOwned.Where(s => s.UserId == AbpSession.UserId);
        //        }


        //        if (_vesselOwned != null)
        //        {
        //            var vessels = _vesselOwned.Select(x => x.MMSI);
        //            var col = new Dictionary<int, List<VesselArrayIdDto>>();
        //            var count = 1;
        //            foreach (var item in vessels)
        //            {
        //                col.Add(count,
        //                   Newtonsoft.Json.JsonConvert
        //                       .DeserializeObject<List<VesselArrayIdDto>>(item));
        //                count += 1;
        //            }
        //            var results = new List<VesselArrayIdDto>();
        //            foreach (var item in col.Values)
        //            {
        //                foreach (var item2 in item)
        //                {
        //                    if (!results.Where(x => x.MMSI == item2.MMSI).Any())
        //                    {
        //                        results.Add(item2);
        //                    }
        //                }
        //            }
        //            return new ListResultDto<VesselArrayIdDto>(results);
        //        }
        //        else
        //        {
        //            return new ListResultDto<VesselArrayIdDto>();
        //        }
        //    }
        //    catch (Exception err)
        //    {

        //        throw err;
        //    }
        //}

        public async Task<ListResultDto<VesselArrayIdDto>> GetVesselToTenantAssignedListByUserIdAsync()
        {
            var user = await _userAppService.GetUserByIdAsync(AbpSession.UserId.Value);

            //var _vesselOwned = await _vesselOwnedRepository
            //    .FirstOrDefaultAsync(s => s.UserId == AbpSession.UserId);

            var isTenantAdmin = false;
            //var isHostAdmin = false;

            foreach (var item in user.RoleNames.ToList())
            {
                //var role = await _roleAppService.Get(item.RoleId);

                if (item.ToLower().Contains("admin"))
                {
                    isTenantAdmin = true;
                    break;
                }
            }

            if (AbpSession.TenantId == null && isTenantAdmin == true)
            {
                //isHostAdmin = true;
                isTenantAdmin = false;
            }

            var _vesselOwned = _vesselOwnedRepository.GetAll().AsQueryable();

            if (isTenantAdmin)
            {
                _vesselOwned = _vesselOwned.Where(s => s.TenantId == AbpSession.TenantId);
            }
            else
            {
                _vesselOwned = _vesselOwned.Where(s => s.UserId == AbpSession.UserId);
            }


            if (_vesselOwned != null)
            {
                var vessels = _vesselOwned.Select(x => x.MMSI);
                var col = new Dictionary<int, List<VesselArrayIdDto>>();
                var count = 1;
                foreach (var item in vessels)
                {
                    col.Add(count,
                       Newtonsoft.Json.JsonConvert
                           .DeserializeObject<List<VesselArrayIdDto>>(item));
                    count += 1;
                }
                var results = new List<VesselArrayIdDto>();
                foreach (var item in col.Values)
                {
                    foreach (var item2 in item)
                    {
                        if (!results.Where(x => x.MMSI == item2.MMSI).Any())
                        {
                            results.Add(item2);
                        }
                    }
                }
                return new ListResultDto<VesselArrayIdDto>(results);
            }
            else
            {
                return new ListResultDto<VesselArrayIdDto>();
            }

        }

        public async Task<ListResultDto<VesselArrayIdDto>> GetVesselToTenantAssignedListByUsernameAsync(string username)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var user = _userManager.Users.Where(c => c.UserName == username).FirstOrDefault();

                if (user != null)
                {
                    var _vesselOwned = await _vesselOwnedRepository.FirstOrDefaultAsync(c => c.UserId == user.Id);

                    if (_vesselOwned != null)
                    {
                        var vesselArrayId = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VesselArrayIdDto>>(_vesselOwned.MMSI);

                        return new ListResultDto<VesselArrayIdDto>(vesselArrayId);
                    }
                    else
                    {
                        return new ListResultDto<VesselArrayIdDto>();
                    }
                }
                else
                {
                    throw new Exception("User not found!");
                }
            }
        }


    }
}
