﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Roles.Dto;

namespace Vtmis.WebAdmin.Roles
{
    
    public interface IRoleAppService : IAsyncCrudAppService<RoleDto, int, PagedResultRequestDto, CreateRoleDto, RoleDto>
    {
        Task<Role> CreateStatic(int tenantId);
        Task<List<RoleDto>> GetAllAsync();
        [AbpAllowAnonymous]
        Task<RoleDto> Get(int roleId);
       
        Task<List<PermissionDto>> GetAllPermissions();
        IEnumerable<PermissionDto> GetAllCustomPermissions();
        Task<RoleDto> UpdateRolePermissionsAsync(int roleId, List<string> permissions);
        Task<RoleDto> RemovePermission(int roleId, string permissionName);
        //Task TempSeedCustomPermissions();
    }
}
