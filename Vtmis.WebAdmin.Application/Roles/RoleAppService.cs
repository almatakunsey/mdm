﻿
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.UI;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Authorization;
using Vtmis.WebAdmin.Roles.Dto;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Common.Constants;
using Abp.MultiTenancy;
using Abp;
using Vtmis.WebAdmin.MultiTenancy;
using System;
using Abp.Domain.Uow;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories;

namespace Vtmis.WebAdmin.Roles
{
    [AbpAuthorize]
    public class RoleAppService : AsyncCrudAppService<Role, RoleDto, int, PagedResultRequestDto, CreateRoleDto, RoleDto>, IRoleAppService
    {
        private readonly IRepository<Role> _repository;
        private readonly RoleManager _roleManager;
        private readonly UserManager _userManager;
        private readonly TenantManager _tenantManager;
        private readonly IAbpPermissionRepository _abpPermissionRepository;
        private readonly IMdmUserRoleRepository _abpUserRoleRepository;

        public RoleAppService(IRepository<Role> repository,
            RoleManager roleManager, UserManager userManager,
            TenantManager tenantManager, IAbpPermissionRepository abpPermissionRepository, IMdmUserRoleRepository abpUserRoleRepository)
            : base(repository)
        {
            _repository = repository;
            _roleManager = roleManager;
            _userManager = userManager;
            _tenantManager = tenantManager;
            _abpPermissionRepository = abpPermissionRepository;
            _abpUserRoleRepository = abpUserRoleRepository;
        }

        [AbpAllowAnonymous]
        public async Task<RoleDto> Get(int roleId)
        {
            var role = await _repository.GetAll().IgnoreQueryFilters().Include(x => x.Permissions)
                .FirstOrDefaultAsync(x => x.Id == roleId && x.Permissions.Where(p => p.IsGranted == true)
                .Any());
           
            var result = ObjectMapper.Map<RoleDto>(role);

            result.Permissions = (await _abpPermissionRepository.GetByRoleIdAsync(role.Id)).Where(w => w.IsGranted == true).Select(s => s.Name).ToList();
            return result;
        }

        public override Task<RoleDto> Get(EntityDto<int> input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant,
                AbpDataFilters.MustHaveTenant))
            {
                return base.Get(input);
            }

        }

        public async Task<List<RoleDto>> GetAllAsync()
        {
            var roles = _repository.GetAll().IgnoreQueryFilters()
                .Where(x => x.IsDeleted == false).AsQueryable();
            var isHighPermission = false;
            if (await PermissionChecker.IsGrantedAsync(MdmPermissionsConst.Role_View_Mdm))
            {
                isHighPermission = true;
            }
            if (await PermissionChecker.IsGrantedAsync(MdmPermissionsConst.Role_View_Tenant))
            {
                if (isHighPermission == false)
                {
                    isHighPermission = true;
                    roles = roles.Where(x => x.TenantId == AbpSession.TenantId);
                }
            }
            if (isHighPermission == false)
            {
                return new List<RoleDto>();
            }
            return ObjectMapper.Map<List<RoleDto>>(roles);
        }



        ////TODO: Remove this after all db server has been seed
        //public async Task TempSeedCustomPermissions()
        //{
        //    var roleAdmin = _repository.GetAll().IgnoreQueryFilters().Include(x => x.Permissions)
        //                        .Where(x => x.Name == "Admin").ToList();

        //    foreach (var role in roleAdmin)
        //    {
        //        var permissions = new List<string>();
        //        if (role.TenantId == null)
        //        {
        //            permissions = MdmPermissionsConst.All();
        //        }
        //        else if (role.TenantId != null && role.TenantId > 0)
        //        {
        //            permissions = MdmPermissionsConst.DefaultAdmin();
        //        }
        //        foreach (var permissionName in permissions)
        //        {
        //            role.Permissions.Add(
        //              new Abp.Authorization.Roles.RolePermissionSetting
        //              {
        //                  CreatorUserId = AbpSession.UserId,
        //                  TenantId = role.TenantId,
        //                  Name = permissionName,
        //                  IsGranted = true,
        //                  RoleId = role.Id
        //              });
        //        }
        //        await _repository.UpdateAsync(role);
        //    }
        //}

        public async Task<Role> CreateStatic(int tenantId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var tenant = _tenantManager.GetById(tenantId);
                var role = new Role(tenant.Id, tenant.Name, tenant.Name)
                {
                    CreationTime = DateTime.Now,
                    CreatorUserId = AbpSession.UserId,
                    DisplayName = $"Admin {tenant.Name}",
                    Name = StaticRoleNames.Tenants.Admin,
                    IsDefault = false,
                    IsStatic = false,
                    TenantId = tenantId
                };
                role.SetNormalizedName();

                var isHighPermission = false;

                if (await PermissionChecker.IsGrantedAsync(MdmPermissionsConst.Role_Create_Mdm))
                {
                    isHighPermission = true;
                }
                if (await PermissionChecker.IsGrantedAsync(MdmPermissionsConst.Role_Create_Tenant))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        if (role.TenantId != AbpSession.TenantId)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }
                }
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }

                return await _repository.InsertAsync(role);
            }
        }

        public override async Task<RoleDto> Create(CreateRoleDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                CheckCreatePermission();

                var role = ObjectMapper.Map<Role>(input);
                role.IsDefault = input.IsDefault;
                role.SetNormalizedName();
                if (input.TenantId == null)
                {
                    role.TenantId = AbpSession.TenantId;
                }


                var isHighPermission = false;
                if (await IsGrantedAsync(MdmPermissionsConst.Role_Create_Mdm))
                {
                    isHighPermission = true;
                }
                if (await IsGrantedAsync(MdmPermissionsConst.Role_Create_Tenant))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        //if (role.TenantId != AbpSession.TenantId)
                        //{
                        //    throw new ForbiddenException("Permission Denied");
                        //}
                    }
                }
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }

                var isRoleExist = _repository.GetAll().IgnoreQueryFilters().Where(x => x.TenantId == role.TenantId
                                     && x.Name.ToLower() == input.Name.ToLower()).Any();

                if (isRoleExist == false)
                {
                    await _repository.InsertAsync(role);
                    await CurrentUnitOfWork.SaveChangesAsync();
                }

                var host = role.IsDefault ? Abp.MultiTenancy.MultiTenancySides.Host : Abp.MultiTenancy.MultiTenancySides.Tenant;

                var customPermissions = new List<Permission>();

                if (input.Permissions != null && input.Permissions.Any())
                {
                    foreach (var permissionName in input.Permissions)
                    {
                        customPermissions.Add(new Abp.Authorization.Permission(permissionName, null, null, host, null));

                    }

                    await _roleManager.SetGrantedPermissionsAsync(role, customPermissions);
                }

                return MapToEntityDto(role);
            }

        }

        public override async Task<RoleDto> Update(RoleDto input)
        {
            CheckUpdatePermission();
            //try
            //{
            //    await _roleManager.GetRoleByIdAsync(input.Id);
            //}
            //catch (Exception error)
            //{

            //    throw;
            //}


            //var roletenant = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(z => z.TenantId == AbpSession.TenantId && z.Name.Contains("admin"));

            var role = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == input.Id);

            //if (role.Name.Contains("admin") == false)
            //{
            //    //host admin
            //    throw new ForbiddenException("Permission Denied");
            //}
            //if (roletenant.TenantId != null && roletenant.Id == role.Id && roletenant.Id == role.CreatorUserId)
            //{
            //    //tenant user
            //    throw new ForbiddenException("Permission Denied");
            //}
            //if (roletenant.TenantId != null && roletenant.Id == role.Id)
            //{
            //    //tenant admin
            //    throw new ForbiddenException("Permission Denied");
            //}

            ObjectMapper.Map(input, role);
            var isHighPermission = false;
            if (await IsGrantedAsync(MdmPermissionsConst.Role_Update_Mdm))
            {
                isHighPermission = true;
            }
            if (await IsGrantedAsync(MdmPermissionsConst.Role_Update_Tenant))
            {
                if (isHighPermission == false)
                {
                    isHighPermission = true;
                    if (role.TenantId != AbpSession.TenantId)
                    {
                        throw new UserFriendlyException("Permission Denied");
                    }
                }
            }
            if (isHighPermission == false)
            {
                throw new UserFriendlyException("Permission Denied");
            }

            var currentUserRoles = await _abpUserRoleRepository.GetAll()
                                        .IgnoreQueryFilters().Include(x => x.Role).Where(x => x.TenantId == AbpSession.TenantId).ToListAsync();

            var isAdmin = false;
            if (currentUserRoles.Any())
            {
                foreach (var item in currentUserRoles)
                {
                    if (item.Role.Name.Contains("admin", StringComparison.CurrentCultureIgnoreCase))
                    {
                        isAdmin = true;
                        break;
                    }
                }
            }

            if (isAdmin == false)
            {
                throw new UserFriendlyException("Permission Denied");
            }

            role.SetNormalizedName();
            await _repository.UpdateAsync(role);
            //CheckErrors(await _roleManager.UpdateAsync(role));

            var grantedPermissions = PermissionManager
                .GetAllPermissions()
                .Where(p => input.Permissions.Contains(p.Name))
                .ToList();
            try
            {
                await _abpPermissionRepository.UpdatePermissionByRoleId(input.Permissions, role.Id, role.TenantId);
                //await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);
            }
            catch (Exception err)
            {

                throw;
            }


            await CurrentUnitOfWork.SaveChangesAsync();

            return MapToEntityDto(role);
        }

        public override async Task Delete(EntityDto<int> input)
        {
            CheckDeletePermission();

            var role = await _roleManager.FindByIdAsync(input.Id.ToString());
            var users = await _userManager.GetUsersInRoleAsync(role.NormalizedName);

            var isHighPermission = false;
            if (await IsGrantedAsync(MdmPermissionsConst.Role_Delete_Mdm))
            {
                isHighPermission = true;
            }
            if (await IsGrantedAsync(MdmPermissionsConst.Role_Delete_Tenant))
            {
                if (isHighPermission == false)
                {
                    isHighPermission = true;
                    if (role.TenantId != AbpSession.TenantId)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }
            }
            if (isHighPermission == false)
            {
                throw new ForbiddenException("Permission Denied");
            }

            foreach (var user in users)
            {
                CheckErrors(await _userManager.RemoveFromRoleAsync(user, role.NormalizedName));
            }

            CheckErrors(await _roleManager.DeleteAsync(role));
        }

        public Task<List<PermissionDto>> GetAllPermissions()
        {
            var permissions = PermissionManager.GetAllPermissions();

            return Task.FromResult(new List<PermissionDto>(
                ObjectMapper.Map<List<PermissionDto>>(permissions)
            ));
        }


        public IEnumerable<PermissionDto> GetAllCustomPermissions()
        {
            var permissions = MdmPermissionsConst.All();
            return permissions.Select((x, y) => new PermissionDto
            {
                Id = y,
                Name = x,
                DisplayName = x
            });
        }

        public async Task<RoleDto> RemovePermission(int roleId, string permissionName)
        {
            try
            {
                var role = await _repository.GetAll().Include(x => x.Permissions).
                                FirstOrDefaultAsync(x => x.Id == roleId);

                if (permissionName == null)
                {
                    throw new RequestFaultException("Invalid permissions");
                }
                var permission = role.Permissions.FirstOrDefault(x => x.Name == permissionName);
                if (permission != null)
                {
                    role.Permissions.Remove(permission);
                }
                await _repository.UpdateAsync(role);
                return MapToEntityDto(role);
            }
            catch (RequestFaultException err)
            {
                throw err;
            }
            catch (System.Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task<RoleDto> UpdateRolePermissionsAsync(int roleId, List<string> permissions)
        {
            var role = await _roleManager.GetRoleByIdAsync(roleId);
            if (permissions == null || !permissions.Any())
            {
                throw new RequestFaultException("Invalid permissions");
            }
            //if (PermissionManager.GetAllPermissions().Where(x=> permissions.Contains(x.Name)).Any())
            //{
            //    throw new AlreadyExistException("Some permission");
            //}
            //var grantedPermissions = PermissionManager
            //    .GetAllPermissions()
            //    .ToList();

            var host = role.IsDefault ? Abp.MultiTenancy.MultiTenancySides.Host : Abp.MultiTenancy.MultiTenancySides.Tenant;

            var customPermissions = new List<Permission>();
            foreach (var permissionName in permissions)
            {
                customPermissions.Add(new Permission(permissionName, null, null, host, null));
            }
            await _roleManager.SetGrantedPermissionsAsync(role, customPermissions);
            return MapToEntityDto(role);
        }

        protected override IQueryable<Role> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Permissions);
        }

        protected override async Task<Role> GetEntityByIdAsync(int id)
        {
            return await Repository.GetAllIncluding(x => x.Permissions).FirstOrDefaultAsync(x => x.Id == id);
        }

        protected override IQueryable<Role> ApplySorting(IQueryable<Role> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.DisplayName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
