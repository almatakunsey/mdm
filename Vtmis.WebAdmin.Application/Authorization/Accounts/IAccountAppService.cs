﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Vtmis.WebAdmin.Authorization.Accounts.Dto;

namespace Vtmis.WebAdmin.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
