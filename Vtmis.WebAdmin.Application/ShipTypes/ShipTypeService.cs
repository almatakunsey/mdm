﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.ShipTypes.Dto;
//using Vtmis.WebAdmin.VtmisDomains;
using Abp.UI;
using Vtmis.WebAdmin.VtmisType;
using Vtmis.Core.Common.Constants;
using System.Linq;
using Abp.Authorization;

namespace Vtmis.WebAdmin.ShipTypes
{
    [AbpAuthorize]
    public class ShipTypeService : ApplicationService
    {
        public IEnumerable<ShipTypeDto> Get()
        {
            return ShipTypeConst.All().Select(x => new ShipTypeDto
            {
                ShipTypeName = x
            }).OrderBy(x => x.ShipTypeName);
        }
    }
}
