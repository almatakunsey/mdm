﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Vtmis.WebAdmin.VtmisType;
//using Vtmis.WebAdmin.VtmisDomains;

namespace Vtmis.WebAdmin.ShipTypes.Dto
{
    //[AutoMapFrom(typeof(ShipType))]
    public class ShipTypeDto
    {
        //public ShipTypeDto()
        //{

        //}

        //public DateTime CreationTime { get; set; }

        //public int ShipTypeNumber { get; set; }

        public string ShipTypeName { get; set; }

        //public string DetailType { get; set; }

        //public string ShipTypeSummary { get; set; }
    }
}
