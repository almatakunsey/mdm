﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.EntityFrameworkCore;
using Abp.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;
using Vtmis.Core.Model.DTO;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Vtmis.WebAdmin.Lloyds.Dto;

namespace Vtmis.WebAdmin.Lloyds
{
    [AbpAuthorize]
    public class LloydsService : ApplicationService, ILloydsService
    {
        private readonly IRepository<Lloyd> _llyodRepo;
        private readonly IDbContextProvider<WebAdminDbContext> _context;
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        private List<LloydsShipTypeDto> _lloydShipType;
        public LloydsService(IRepository<Lloyd> llyodRepo, IDbContextProvider<WebAdminDbContext> context, IConnectionMultiplexer connectionMultiplexer)
        {
            _llyodRepo = llyodRepo;
            _context = context;
            _connectionMultiplexer = connectionMultiplexer;
            _lloydShipType = new List<LloydsShipTypeDto>()
            {
                new LloydsShipTypeDto() { Id = 1, Name = "Anchor"}
            };

        }
        [Route("/api/lloyds/ShipTypes")]
        [HttpGet]
        public IEnumerable<LloydsShipTypeDto> GetAllShipTypes()
        {
            return _lloydShipType;
        }

        [Route("/api/lloyds")]
        [HttpGet]
        public async Task<PaginatedList<Lloyd>> GetAllAsync(int pageIndex = 1, int pageSize = 100)
        {
            return await new PaginatedList<Lloyd>()
                                                   .CreateAsync(_llyodRepo.GetAll(), pageIndex, pageSize);
        }

        [Route("/api/lloyds/seed")]
        [HttpGet]
        public async Task<string> Seed()
        {
            try
            {
                var sqlFile = $@"{AppDomain.CurrentDomain.BaseDirectory}/LlyodsData.sql";
                if (File.Exists(sqlFile))
                {
                    Console.WriteLine("Seeding Lloyds data");
                    _context.GetDbContext().Database.SetCommandTimeout(1200);
                    _context.GetDbContext().Database.ExecuteSqlCommand(File.ReadAllText(sqlFile));
                    Console.WriteLine("Seed lloyds to redis");

                    var redis = _connectionMultiplexer.GetDatabase();
                    var lloydsHash = await redis.HashGetAllAsync($"Lloyds_{AppHelper.GetEnvironmentName()}");
                    if (lloydsHash.Length > 0)
                    {
                        Console.WriteLine("Removing existing lloyds data");
                        foreach (var item in lloydsHash)
                        {
                            await redis.HashDeleteAsync($"Lloyds_{AppHelper.GetEnvironmentName()}", item.Name, CommandFlags.FireAndForget);
                        }
                    }

                    var lloydsRows = await _llyodRepo.GetAll().IgnoreQueryFilters().AsNoTracking().CountAsync();
                    if (lloydsRows > 0)
                    {
                        double numOfPage = Math.Round((double)((double)lloydsRows / 100), 2);
                        int actualNumOfPage = 0;
                        if ((numOfPage % 1) == 0)
                        {
                            actualNumOfPage = (int)numOfPage;
                        }
                        else
                        {
                            actualNumOfPage = (int)(numOfPage + 1);
                        }

                        List<Lloyds.Lloyd> lloyds = new List<Lloyds.Lloyd>();
                        for (int i = 0; i < actualNumOfPage; i++)
                        {
                            var data = await _llyodRepo.GetAll().IgnoreQueryFilters().AsNoTracking().Skip(i).Take(100).ToListAsync();
                            lloyds.AddRange(data);
                        }
                        Console.WriteLine("Seeding lloyds data");
                        for (int i = 0; i < lloyds.Count; i++)
                        {
                            var data = JsonConvert.SerializeObject(lloyds[0]);
                            await redis.HashSetAsync($"Lloyds_{AppHelper.GetEnvironmentName()}", i, data, When.Always, CommandFlags.FireAndForget);
                        }
                    }


                    return "Seeding success";
                }
                else
                {
                    return "Lloyds script not found";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
                // TODO: Handle failure
            }
        }

        [Route("/api/lloyds/{id}")]
        [HttpGet]
        public async Task<Lloyd> GetByIdAsync(int id)
        {
            var result = await _llyodRepo.GetAll().Where(x => x.Id ==
                            id).FirstOrDefaultAsync();
            if (result == null)
            {
                throw new UserFriendlyException($"No vessel found with Id: {id}");
            }
            return result;
        }
       

        [Route("/api/lloyds/details")]
        [HttpPost]
        public async Task<Lloyd> GetByImoAndCallSignAsync([FromBody]ImoCallSignDto input)
        {
            /// TODO: Refactor this
            if (input.CallSign == null && input.IMO == null)
            {
                throw new UserFriendlyException($"Invalid parameter");
            }
           

            if (string.IsNullOrEmpty(input.CallSign) == false)
            {
                var query = _llyodRepo.GetAll().IgnoreQueryFilters().AsNoTracking();
                query = query.Where(x => string.IsNullOrEmpty(x.CallSign) == false
                        && x.CallSign.IsEqual(input.CallSign));

                var result = await query.FirstOrDefaultAsync();
                if (result != null)
                {
                    return result;
                }
            }
           
            if (input.IMO != null)
            {
                var query = _llyodRepo.GetAll().IgnoreQueryFilters().AsNoTracking();
                query = query.Where(x => x.IMO == input.IMO);
                var result = await query.FirstOrDefaultAsync();
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        //[Route("/api/lloyds/details")]
        [AbpAllowAnonymous]
        public async Task<Lloyd> GetAsync(int? imo, string callSign, string targetName)
        {
            if (callSign == null && imo == null && string.IsNullOrEmpty(targetName))
            {
                return null;
            }
            var query = _llyodRepo.GetAll();

            if (!string.IsNullOrEmpty(callSign))
            {
                query = query.Where(x => !string.IsNullOrEmpty(x.CallSign)
                        && string.Equals(x.CallSign, callSign, StringComparison.CurrentCultureIgnoreCase));
            }
            if (!string.IsNullOrEmpty(targetName))
            {
                query = query.Where(x => !string.IsNullOrEmpty(x.Name) && x.Name.Contains(targetName, StringComparison.CurrentCultureIgnoreCase));
            }
            if (imo != null)
            {
                query = query.Where(x => x.IMO == imo);
            }

            if (!query.Any())
            {
                return null;
                //throw new UserFriendlyException($"No vessel found with IMO: {imo} and Call Sign: {callSign}");
            }
            return await query.FirstOrDefaultAsync();
        }


    }
}
