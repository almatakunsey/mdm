﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.Core.Model.DTO;
using Vtmis.WebAdmin.Lloyds.Dto;

namespace Vtmis.WebAdmin.Lloyds
{
    public interface ILloydsService
    {
        Task<PaginatedList<Lloyd>> GetAllAsync(int pageIndex = 1, int pageSize = 100);
        Task<Lloyd> GetByIdAsync(int id);
        Task<Lloyd> GetByImoAndCallSignAsync([FromBody]ImoCallSignDto input);
        Task<Lloyd> GetAsync(int? imo, string callSign, string targetName);
        IEnumerable<LloydsShipTypeDto> GetAllShipTypes();
        Task<string> Seed();
        //Task<PaginatedList<Lloyd>> GetByImoAsync(int? imo = null, int pageIndex = 1, int pageSize = 100);
        //Task<PaginatedList<Lloyd>> GetByCallSignAsync(string callSign = null, int pageIndex = 1, int pageSize = 100);
    }
}
