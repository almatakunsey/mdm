﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Alarms.Dto;

namespace Vtmis.WebAdmin.Alarms
{
    public interface IAlarmAppService
    {
        Task<IEnumerable<AlarmDto>> GetAll();
        Task<IEnumerable<AlarmDto>> GetAllByTenantIdAsync(int tenantId);
        Task<IEnumerable<AlarmDto>> GetAllByUserIdAsync(int userId);
        Task<List<AlarmDto>> GetAllByCurrentUserSessionAsync();
        Task<AlarmDto> GetByIdAsync(int id);
        Task CreateAsync(CreateAlarmDto alarm);
        Task UpdateAsync(AlarmDto updatedAlarm);
        Task DeleteAsync(int id);
    }
}
