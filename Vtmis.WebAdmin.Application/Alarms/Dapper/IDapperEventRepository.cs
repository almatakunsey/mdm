﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.Alarms.Dapper
{
    public interface IDapperEventRepository
    {
        Task UpdateKillByIds(IEnumerable<long> ids);
    }
}
