﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.WebAdmin.Alarms.Dapper
{
    public class DapperEventRepository : IDapperEventRepository
    {
        public async Task UpdateKillByIds(IEnumerable<long> ids)
        {
            using (var db = new SqlConnection(AppHelper.GetMdmDbConnectionString()))
            {
                await db.ExecuteAsync($@"UPDATE Events
                            SET [Kill] = 1
                            WHERE Id IN @ids;", new { ids });               
            }
        }
    }
}
