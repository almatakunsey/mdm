﻿using System.Collections.Generic;
using Vtmis.WebAdmin.ReadOnlyDB;

namespace Vtmis.WebAdmin.Alarms.Dapper
{
    public class DapperAlarmService : IDapperAlarmService
    {
        private readonly IDapperAlarmRepository _repository;

        public DapperAlarmService(IDapperAlarmRepository repository)
        {
            _repository = repository;
        }
        public AlarmR Get(int id)
        {
            return _repository.Get(id);
        }

        public IEnumerable<AlarmR> Get()
        {
            return _repository.Get();
        }
    }
}
