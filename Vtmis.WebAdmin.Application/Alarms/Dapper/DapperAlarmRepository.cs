﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.ReadOnlyDB;

namespace Vtmis.WebAdmin.Alarms.Dapper
{
    public class DapperAlarmRepository : IDapperAlarmRepository
    {
        public async Task UpdateAlarmChanges(bool isChanged, long alarmId)
        {
            using (var db = new SqlConnection(AppHelper.GetMdmDbConnectionString()))
            {
                await db.ExecuteAsync($@"UPDATE Alarms
                            SET Changes = @isChanged
                            WHERE Id=@alarmId;", new { isChanged, alarmId });               
            }
        }

        public async Task<IEnumerable<Alarm>> UpdateAlarmChanges(bool isChanged)
        {
            using (var db = new SqlConnection(AppHelper.GetMdmDbConnectionString()))
            {
                await db.ExecuteAsync($@"UPDATE Alarms
                            SET Changes = @isChanged
                            WHERE Changes=1;", new { isChanged });
                return await db.QueryAsync<Alarm>(@"SELECT Id FROM Alarms WHERE IsDeleted=0 AND IsEnabled=1");
            }
        }

        public AlarmR Get(int id)
        {
            using (var db = new SqlConnection(AppHelper.GetMdmDbConnectionString()))
            {
                return db.QueryFirstOrDefault<AlarmR>(@"
                    SELECT * FROM Alarms WHERE Id=@Id
                ", new { Id = id });
            }
        }

        public IEnumerable<AlarmR> Get()
        {
            using (var db = new SqlConnection(AppHelper.GetMdmDbConnectionString()))
            {
                return db.Query<AlarmR>(@"
                    SELECT * FROM Alarms"
                );
            }
        }
    }
}
