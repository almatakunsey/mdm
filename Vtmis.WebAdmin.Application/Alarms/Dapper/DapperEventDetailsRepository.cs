﻿using Dapper;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.ReadOnlyDB;

namespace Vtmis.WebAdmin.Alarms.Dapper
{
    public class DapperEventDetailsRepository : IDapperEventDetailsRepository
    {
        public async Task<EventDetailsR> GetAsync(long id)
        {
            using (var db = new SqlConnection(AppHelper.GetMdmDbConnectionString()))
            {
                return await db.QueryFirstOrDefaultAsync<EventDetailsR>(@"
                    SELECT * FROM EventDetails WHERE Id=@Id
                ", new { Id = id });
            }
        }

        public async Task<IEnumerable<EventDetailsR>> GetAsync()
        {
            using (var db = new SqlConnection(AppHelper.GetMdmDbConnectionString()))
            {
                return await db.QueryAsync<EventDetailsR>(@"
                    SELECT * FROM EventDetails
                ");
            }
        }

        public async Task<IEnumerable<EventDetailsR>> GetByEventIdAsync(long eventId, bool isLatest = true)
        {
            using (var db = new SqlConnection(AppHelper.GetMdmDbConnectionString()))
            {
                return await db.QueryAsync<EventDetailsR>(@"
                    SELECT * FROM EventDetails WHERE EventId=@eventId AND IsLatest=@IsLatest
                ", new { eventId, IsLatest = isLatest ? 1 : 0 });
            }
        }

        public async Task UpdateIslatest(bool isLatest, long eventId)
        {
            using (var db = new SqlConnection(AppHelper.GetMdmDbConnectionString()))
            {
                await db.ExecuteAsync("UPDATE EventDetails SET IsLatest=@IsLatest WHERE EventId=@eventId", new { IsLatest = isLatest ? 1 : 0, eventId });
            }
        }
    }
}
