﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.ReadOnlyDB;

namespace Vtmis.WebAdmin.Alarms.Dapper
{
    public interface IDapperAlarmRepository
    {
        AlarmR Get(int id);
        IEnumerable<AlarmR> Get();
        Task<IEnumerable<Alarm>> UpdateAlarmChanges(bool isChanged);
        Task UpdateAlarmChanges(bool isChanged, long alarmId);
    }
}
