﻿using System.Collections.Generic;
using Vtmis.WebAdmin.ReadOnlyDB;

namespace Vtmis.WebAdmin.Alarms.Dapper
{
    public interface IDapperAlarmService
    {
        AlarmR Get(int id);
        IEnumerable<AlarmR> Get();
    }
}
