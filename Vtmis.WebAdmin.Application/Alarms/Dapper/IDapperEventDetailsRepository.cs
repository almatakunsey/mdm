﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.ReadOnlyDB;

namespace Vtmis.WebAdmin.Alarms.Dapper
{
    public interface IDapperEventDetailsRepository
    {
        Task<EventDetailsR> GetAsync(long id);
        Task<IEnumerable<EventDetailsR>> GetAsync();
        Task<IEnumerable<EventDetailsR>> GetByEventIdAsync(long eventId, bool isLatest = true);
        Task UpdateIslatest(bool isLatest, long eventId);
    }
}
