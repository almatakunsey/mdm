﻿using Abp.Application.Services;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Alarms.Dto;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Permissions;

namespace Vtmis.WebAdmin.Alarms
{
    [AbpAuthorize]
    public class AlarmAppService : ApplicationService, IAlarmAppService
    {
        private readonly IRepository<Alarm> _repo;
        private readonly IRepository<AlarmItem> _alarmItemRepo;
        private readonly UserManager _userManager;
        private readonly IMdmPermissionService _mdmPermission;

        public AlarmAppService(IRepository<Alarm> repo, IRepository<AlarmItem> alarmItemRepo,
            UserManager userManager, IMdmPermissionService mdmPermission)
        {
            _repo = repo;
            _alarmItemRepo = alarmItemRepo;
            _userManager = userManager;
            _mdmPermission = mdmPermission;
        }
        private bool IsValidEmail(string email)
        {
            try
            {

                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        [HttpPost]
        public async Task CreateAsync(CreateAlarmDto alarm)
        {

            if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.CreateAlarm, true))
            {
                //var input = ObjectMapper.Map<Alarm>(alarm);
                //input.TenantId = AbpSession.TenantId;
                //await _repo.InsertAsync(input);

                var isAlarmNameExist = _repo.GetAll().IgnoreQueryFilters().Where(x => string.Equals(x.Name, alarm.Name, StringComparison.CurrentCultureIgnoreCase) && x.UserId == AbpSession.UserId).Any();

                if (isAlarmNameExist)
                {
                    throw new AlreadyExistException($"Alarm {alarm.Name} already exist.");
                }

                var input = ObjectMapper.Map<Alarm>(alarm);
                if (!string.IsNullOrEmpty(input.EmailAddress))
                {
                    input.EmailAddress = input.EmailAddress.Trim();
                    if (!IsValidEmail(input.EmailAddress))
                    {
                        throw new RequestFaultException("Invalid email address");
                    }
                }
                input.SetCurrentUser(AbpSession.TenantId, AbpSession.UserId);
                input.SetDefaultChanges();
                await _repo.InsertAsync(input);
            }
            else
            {
                throw new ForbiddenException("Permission Denied");
            }

        }

        public async Task DeleteAsync(int id)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant, AbpDataFilters.SoftDelete))
            {
                var isExist = await _repo.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
                if (isExist == null)
                {
                    throw new NotFoundException("Alarm Does Not Exist");
                }

                var isHighPermission = false;

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteAlarmWithinMdm, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteAlarmWithinTenant, true))
                {
                    isHighPermission = true;
                    if (isExist.TenantId != AbpSession.TenantId && isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteOwnAlarm, true))
                {
                    isHighPermission = true;
                    if (isExist.UserId != AbpSession.UserId && isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
                isExist.DeleterUserId = AbpSession.UserId;
                isExist.DeletionTime = DateTime.Now;
                isExist.IsDeleted = true;
                isExist.Changes = true;
                await _repo.UpdateAsync(isExist);
                await CurrentUnitOfWork.SaveChangesAsync();
            }

        }

        public async Task<IEnumerable<AlarmDto>> GetAll()
        {
            //var alarms = _repo.GetAllIncluding(x => x.AlarmItems).ToList();
            //return ObjectMapper.Map<IEnumerable<AlarmDto>>(alarms);

            var qry = _repo.GetAll().IgnoreQueryFilters().AsQueryable();
            var isHighPermission = false;

            if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewAlarmWithinMdm, true))
            {
                isHighPermission = true;
            }
            if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewAlarmWithinTenant, true))
            {
                if (isHighPermission == false)
                {
                    isHighPermission = true;
                    qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                }
            }
            if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewOwnAlarm, true))
            {
                if (isHighPermission == false)
                {
                    isHighPermission = true;
                    qry = qry.Where(x => x.UserId == AbpSession.UserId);
                }
            }
            if (isHighPermission == false)
            {
                throw new ForbiddenException("Permission Denied");
            }
            //TODO: add pagination

            qry = qry.Include(x => x.AlarmItems).Where(x => x.IsDeleted == false).OrderByDescending(o => o.CreationTime);

            return ObjectMapper.Map<IEnumerable<AlarmDto>>(qry);
        }

        public async Task<IEnumerable<AlarmDto>> GetAllByTenantIdAsync(int tenantId)
        {
            var alarms = await _repo.GetAll().Include(x => x.AlarmItems).Where(x => x.TenantId == tenantId).ToListAsync();
            return ObjectMapper.Map<IEnumerable<AlarmDto>>(alarms);
        }

        public async Task<IEnumerable<AlarmDto>> GetAllByUserIdAsync(int userId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                CancellationTokenSource source = new CancellationTokenSource();
                CancellationToken cancellationToken = source.Token;

                var user = _userManager.Users.Where(c => c.Id == userId).FirstOrDefault();

                if (user != null)
                {
                    var alarms = await _repo.GetAll().Include(x => x.AlarmItems).Where(c => c.TenantId == user.TenantId).ToListAsync();

                    var alarmsDto = ObjectMapper.Map<List<AlarmDto>>(alarms);

                    return alarmsDto;
                }
                else
                {
                    throw new UserFriendlyException("Invalid user!");
                }
            }


        }

        public async Task<List<AlarmDto>> GetAllByCurrentUserSessionAsync()
        {
            List<Alarm> alarms;
            List<AlarmDto> alarmDto;
            if (AbpSession.TenantId == null)
            {
                alarms = await _repo.GetAll().Include(x => x.AlarmItems).ToListAsync();
                alarmDto = ObjectMapper.Map<List<AlarmDto>>(alarms);
            }
            else
            {
                alarms = await _repo.GetAll().Include(x => x.AlarmItems).Where(c => c.TenantId == AbpSession.TenantId).ToListAsync();
                alarmDto = ObjectMapper.Map<List<AlarmDto>>(alarms);
            }

            return alarmDto;
        }

        public async Task<AlarmDto> GetByIdAsync(int id)
        {
            //var alarm = await _repo.GetAll().Include(x => x.AlarmItems).Where(x => x.Id == id).FirstOrDefaultAsync();

            //if (alarm == null)
            //{
            //    throw new UserFriendlyException("Could not found the alarm, maybe it's deleted.");
            //}

            //return ObjectMapper.Map<AlarmDto>(alarm);

            var isHighPermission = false;
            var qry = _repo.GetAll().IgnoreQueryFilters().Include(x => x.AlarmItems).AsQueryable();

            //Check permissions
            if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewAlarmWithinMdm, true))
            {
                isHighPermission = true;
            }
            if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewAlarmWithinTenant, true))
            {
                if (isHighPermission == false)
                {
                    isHighPermission = true;
                    qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                }
            }
            if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewOwnAlarm, true))
            {
                if (isHighPermission == false)
                {
                    isHighPermission = true;
                    qry = qry.Where(x => x.UserId == AbpSession.UserId);
                }
            }
            if (isHighPermission == false)
            {
                throw new ForbiddenException("Permission Denied");
            }
            qry = qry.Where(x => x.Id == id && x.IsDeleted == false);

            var result = await qry.FirstOrDefaultAsync();
            if (result == null)
            {
                throw new NotFoundException("Alarm does not exist");
            }
            return ObjectMapper.Map<AlarmDto>(result);
        }

        public async Task UpdateAsync(AlarmDto updatedAlarm)
        {
            var result = await _repo.GetAll().IgnoreQueryFilters()
                                .Where(c => c.Id == updatedAlarm.Id).Include(x => x.AlarmItems).FirstOrDefaultAsync();
           
            if (result == null)
            {
                throw new UserFriendlyException("Could not found the alarm, maybe it's deleted.");
            }
            var existingTenantId = result.TenantId;
            await _repo.EnsureCollectionLoadedAsync(result, x => x.AlarmItems);
            //TODO: Find a way to refactor this and make it shareable
            var allAlarmItems = new List<AlarmItem>();

            foreach (var existingAlarmItem in result.AlarmItems.ToList())
            {
                allAlarmItems.Add(existingAlarmItem);
            }

            foreach (var item in updatedAlarm.AlarmItems.ToList())
            {
                var dirtyCheckAlarmItem = allAlarmItems.FirstOrDefault(x => x.Id == item.Id && x.Id > 0);
                if (dirtyCheckAlarmItem != null)
                {
                    dirtyCheckAlarmItem.ColumnType = item.ColumnType;
                    dirtyCheckAlarmItem.ColumnItem = item.ColumnItem;
                    dirtyCheckAlarmItem.ColumnValue = item.ColumnValue;
                    dirtyCheckAlarmItem.Condition = item.Condition;
                    dirtyCheckAlarmItem.Operation = item.Operation;
                    dirtyCheckAlarmItem.RowIndex = item.RowIndex;
                }
                else
                {
                    allAlarmItems.Add(ObjectMapper.Map<AlarmItem>(item));
                }

            }

            ObjectMapper.Map(updatedAlarm, result);
            result.AlarmItems = allAlarmItems;

            var isHighPermission = false;

            if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateAlarmWithinMdm, true))
            {
                isHighPermission = true;
            }
            if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateAlarmWithinTenant, true))
            {
                isHighPermission = true;
                if (result.TenantId != AbpSession.TenantId && isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
            }

            if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateOwnAlarm, true))
            {
                isHighPermission = true;
                if (result.UserId != AbpSession.UserId && isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
            }

            if (isHighPermission == false)
            {
                throw new ForbiddenException("Permission Denied");
            }

            if (!string.IsNullOrEmpty(result.EmailAddress))
            {
                result.EmailAddress = result.EmailAddress.Trim();
                if (!IsValidEmail(result.EmailAddress))
                {
                    throw new Exception("Invalid email address");
                }
            }
            result.SetDefaultChanges();
            result.TenantId = existingTenantId;
            await _repo.UpdateAsync(result);
        }
    }
}
