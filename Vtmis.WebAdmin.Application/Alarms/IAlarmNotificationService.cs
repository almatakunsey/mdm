﻿using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Alarms.Dto;

namespace Vtmis.WebAdmin.Alarms
{
    public interface IAlarmNotificationService
    {
        /// <summary>
        /// Get All AlarmNotification based on current user and user role
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        Task<PaginatedList<ResponseAlarmNotificationDto>> GetAsync(int pageIndex, int pageSize);
        /// <summary>
        /// Get specific AlarmNotification
        /// </summary>
        /// <param name="evenDetailId"></param>
        /// <returns></returns>
        Task<ResponseAlarmNotificationDto> GetAsync(long evenDetailId);
    }
}
