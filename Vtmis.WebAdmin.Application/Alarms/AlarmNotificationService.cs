﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Alarms.Dto;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.ReadOnlyDB;

namespace Vtmis.WebAdmin.Alarms
{
    [RemoteService(IsMetadataEnabled = false)]
    public class AlarmNotificationService : ApplicationService, IAlarmNotificationService
    {
        private readonly IRepository<AlarmNotification, long> _repository;
        private readonly MdmAdminDbContext _mdmAdminDb;

        public AlarmNotificationService(IRepository<AlarmNotification, long> repository, MdmAdminDbContext mdmAdminDb)
        {
            _repository = repository;
            _mdmAdminDb = mdmAdminDb;
        }

        public async Task<PaginatedList<ResponseAlarmNotificationDto>> GetAsync(int pageIndex, int pageSize)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var notifications = _repository.GetAll().IgnoreQueryFilters().AsNoTracking().Include(x => x.EventDetails)
                                            .ThenInclude(x => x.EventHistories).AsQueryable();

                if (AbpSession.TenantId != null)
                {
                    var currentUser = _mdmAdminDb.AbpUserRoles.AsNoTracking().Include(i => i.User).Include(i => i.Role).Where(x => x.UserId == AbpSession.UserId.Value);
                    var isAdmin = await currentUser.Where(x => string.Equals(StaticRoleNames.Tenants.Admin, x.Role.Name,
                                        StringComparison.CurrentCultureIgnoreCase) && (x.UserId == AbpSession.UserId)).AnyAsync();

                    notifications = isAdmin ? notifications.Where(x => x.TenantId == AbpSession.TenantId) :
                            notifications.Where(x => x.UserId == AbpSession.UserId);
                }

                var paginatedQuery = await new PaginatedList<AlarmNotification>().CreateAsync(notifications, pageIndex, pageSize);
                var results = ObjectMapper.Map<PaginatedList<ResponseAlarmNotificationDto>>(paginatedQuery);

                return results;
            }
        }

        public async Task<ResponseAlarmNotificationDto> GetAsync(long evenDetailId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var notification = await _repository.GetAll().IgnoreQueryFilters().AsNoTracking().Include(x => x.EventDetails)
                                            .ThenInclude(x => x.EventHistories)
                                        .FirstOrDefaultAsync(x => x.EventDetailsId == evenDetailId);
                if (notification == null)
                {
                    throw new NotFoundException($"Event Details {evenDetailId} not found");
                }
                var results = ObjectMapper.Map<ResponseAlarmNotificationDto>(notification);
                return results;
            }
        }
    }
}
