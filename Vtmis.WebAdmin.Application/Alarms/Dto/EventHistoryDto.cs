﻿using System;

namespace Vtmis.WebAdmin.Alarms.Dto
{
    public class EventHistoryDto
    {
        public int? MMSI { get; set; }
        public DateTime? TargetRecvTime { get; set; } // RecvTime from AIS ShipPosition   
        public string ShipName { get; set; }
        public string ZoneName { get; set; }
        public string Info { get; set; }
        public string Message { get; set; }
        public DateTime? LocalRecvTime { get; set; }
    }
}
