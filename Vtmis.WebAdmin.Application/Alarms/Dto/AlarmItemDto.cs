﻿using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Alarms.Dto
{
    public class AlarmItemDto
    {
        public int Id { get; set; }
        //public virtual AlarmDto Alarm { get; set; }
        public virtual int AlarmId { get; set; }

        public Condition Condition { get; set; }
        public string ColumnType { get; set; }
        public string ColumnItem { get; set; }
        public string Operation { get; set; }
        public string ColumnValue { get; set; }
        public int RowIndex { get; set; }
    }
}
