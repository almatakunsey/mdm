﻿using Abp.Application.Services.Dto;

namespace Vtmis.WebAdmin.Alarms.Dto
{
    public class ResponseAlarmNotificationDto : EntityDto<long>
    {
        public long EventDetailsId { get; set; }
        public bool IsRead { get; set; }
        public long UserId { get; set; }
        public int? TenantId { get; set; }

        public virtual EventDetailsDto EventDetails { get; set; }
    }
}
