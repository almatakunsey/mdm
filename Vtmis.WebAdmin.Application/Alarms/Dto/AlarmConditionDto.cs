﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Filters.Dto;
using Vtmis.WebAdmin.Zones.Dto;

namespace Vtmis.WebAdmin.Alarms.Dto
{
    [AutoMapFrom(typeof(AlarmCondition))]
    public class AlarmConditionDto : EntityDto
    {
        public AlarmConditionDto()
        {
            CreationTime = DateTime.Now;
        }
        public ZoneDto Zone { get; set; }

        public FilterDto Filter { get; set; }

        // Condition

        public int Min { get; set; }

        public int Max { get; set; }

        public int? TenantId { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
