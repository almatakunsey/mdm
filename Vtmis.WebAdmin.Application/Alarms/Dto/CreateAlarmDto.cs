﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Alarms.Dto
{
    public class CreateAlarmDto
    {
        //public int TenantId { get; set; }

        public string Name { get; set; }

        public bool IsEnabled { get; set; }

        /// <summary>
        /// If priority Medium or High, user must be notified
        /// </summary>
        public AlarmPriority Priority { get; set; }

        /// <summary>
        /// Message to be displayed to notify user
        /// </summary>
        public string Message { get; set; }

        public string Color { get; set; }

        /// <summary>
        /// Specify the minimum time interval between notifications - In minutes
        /// </summary>
        public int RepeatInterval { get; set; }

        public virtual IList<CreateAlarmItemDto> AlarmItems { get; set; }

        //public virtual IList<AlarmCondition> Conditions { get; set; }

        /// <summary>
        /// Send AIS Safety Message if true
        /// </summary>
        public bool IsSendAISSafetyMessage { get; set; }

        public string AISMessage { get; set; }

        /// <summary>
        /// Send email alert if true
        /// </summary>
        public bool IsSendEmail { get; set; }

        //[EmailAddress(ErrorMessage = "Invalid email address format")]
        public string EmailAddress { get; set; }
    }
}
