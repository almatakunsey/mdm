﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.Alarms.Dto
{
    public class EventDetailsDto : EntityDto<long>
    {
        public EventDetailsDto()
        {
            EventHistories = new List<EventHistoryDto>();
        }
        public virtual List<EventHistoryDto> EventHistories { get; set; }
    }
}
