﻿using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Alarms.Dto
{
    public class CreateAlarmItemDto
    {
        public Condition Condition { get; set; }
        public string ColumnType { get; set; }
        public string ColumnItem { get; set; }
        public string Operation { get; set; }
        public string ColumnValue { get; set; }
    }
}
