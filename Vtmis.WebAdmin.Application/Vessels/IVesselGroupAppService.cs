﻿using Abp.Application.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Vessels.Dto;

namespace Vtmis.WebAdmin.Vessels
{
    public interface IVesselGroupAppService : IApplicationService
    {
        Task<CreateVesselGroupDto> CreateAsync(RequestCreateVesselGroup input);
        Task<IEnumerable<CreateVesselGroupDto>> GetAllAsync();
        Task<CreateVesselGroupDto> GetAsync(int id);
        Task<CreateVesselGroupDto> UpdateAsync(int id, RequestCreateVesselGroup input);
        Task DeleteAsync(int id);



    }
}
