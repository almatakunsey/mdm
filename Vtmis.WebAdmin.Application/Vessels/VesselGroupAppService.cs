﻿using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Vtmis.WebAdmin.Vessels.Dto;
using Abp.Application.Services;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Collections.Generic;
using Vtmis.Core.Common.Exceptions;
using Abp.Domain.Uow;
using Vtmis.WebAdmin.Permissions;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.WebAdmin.Vessels
{
    [RemoteService(IsMetadataEnabled = false)]
    public class VesselGroupAppService : ApplicationService, IVesselGroupAppService
    {
        private readonly IRepository<VesselGroup> _repository;
        private readonly IMdmPermissionService _mdmPermission;

        public VesselGroupAppService(IRepository<VesselGroup> repository, IMdmPermissionService mdmPermission)
        {
            _repository = repository;
            _mdmPermission = mdmPermission;
        }

        public async Task<CreateVesselGroupDto> CreateAsync(RequestCreateVesselGroup input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_Create, true) == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }

                var isNameExist = await _repository.GetAll().IgnoreQueryFilters().Where(x =>
                        x.Name.IsEqual(input.Name) && x.UserId == AbpSession.UserId).AnyAsync();

                if (isNameExist)
                {
                    throw new AlreadyExistException($"VesselGroup {input.Name} already exist.");
                }

                var vessel = ObjectMapper.Map<VesselGroup>(input);
                vessel.SetCurrentUser(AbpSession.TenantId, AbpSession.UserId);
                var result = await _repository.InsertAsync(vessel);
              
                await CurrentUnitOfWork.SaveChangesAsync();

                return ObjectMapper.Map<CreateVesselGroupDto>(result);
            }
        }

        public async Task<IEnumerable<CreateVesselGroupDto>> GetAllAsync()
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var qry = _repository.GetAll().IgnoreQueryFilters().AsQueryable();
                var isHighPermission = false;

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_View_MDM, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_View_Tenant, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                    }
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_View_Own, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        qry = qry.Where(x => x.UserId == AbpSession.UserId);
                    }
                }
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
                //TODO: add pagination

                qry = qry.Include(x => x.VesselGroupDetails);

                return ObjectMapper.Map<List<CreateVesselGroupDto>>(await qry.ToListAsync());
            }
        }

        public async Task<CreateVesselGroupDto> GetAsync(int id)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var isHighPermission = false;
                var qry = _repository.GetAll().IgnoreQueryFilters().Include(x => x.VesselGroupDetails).AsQueryable();

                //Check permissions
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_View_MDM, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_View_Tenant, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                    }
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_View_Own, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        qry = qry.Where(x => x.UserId == AbpSession.UserId);
                    }
                }
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
                qry = qry.Where(x => x.Id == id);

                var result = await qry.FirstOrDefaultAsync();
                if (result == null)
                {
                    throw new NotFoundException("Vessel Group does not exist");
                }
                return ObjectMapper.Map<CreateVesselGroupDto>(result);
            }
        }

        public async Task<CreateVesselGroupDto> UpdateAsync(int id, RequestCreateVesselGroup input)
        {

            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var existingVesselGroup = await _repository.GetAll().IgnoreQueryFilters().Include(i => i.VesselGroupDetails)
                                                   .Where(x => x.Id == id).FirstOrDefaultAsync();

                if (existingVesselGroup == null)
                {
                    throw new NotFoundException($"Vessel Group does not exist");
                }
                else
                {
                    var othersVesselGroup = await _repository.GetAll().IgnoreQueryFilters().Where(x => x.Name.ToLower() == input.Name.ToLower() &&
                                                x.Id != id && x.UserId == AbpSession.UserId).AnyAsync();
                    if (othersVesselGroup)
                    {
                        throw new AlreadyExistException($"Vessel Group {input.Name} already exist.");
                    }
                }

                var vesselGroup = ObjectMapper.Map(input, existingVesselGroup);
                var isHighPermission = false;

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_Update_MDM, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_Update_Tenant, true))
                {
                    isHighPermission = true;
                    if (vesselGroup.TenantId != AbpSession.TenantId && isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_Update_Own, true))
                {
                    isHighPermission = true;
                    if (vesselGroup.UserId != AbpSession.UserId && isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }

                var result = await _repository.UpdateAsync(vesselGroup);
                await CurrentUnitOfWork.SaveChangesAsync();
                return ObjectMapper.Map<CreateVesselGroupDto>(result);
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var isExist = await _repository.GetAll().Include(x=>x.VesselGroupDetails).IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == id);
                if (isExist == null)
                {
                    throw new NotFoundException("Vessel Group Does Not Exist");
                }

                var isHighPermission = false;

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_Delete_MDM, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_Delete_Tenant, true))
                {
                    isHighPermission = true;
                    if (isExist.TenantId != AbpSession.TenantId && isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.VesselGroup_Delete_Own, true))
                {
                    isHighPermission = true;
                    if (isExist.UserId != AbpSession.UserId && isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }

                await _repository.DeleteAsync(isExist);
                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }
    }
}
