﻿using System.Collections.Generic;

namespace Vtmis.WebAdmin.Vessels.Dto
{
    public class RequestCreateVesselGroup
    {
        public RequestCreateVesselGroup()
        {
            VesselGroupDetails = new List<RequestVesselDetail>();
        }
        public string Name { get; set; }
        public List<RequestVesselDetail> VesselGroupDetails { get; set; }
        
    }
}
