﻿using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Vessels.Dto
{
    public class RequestVesselDetail
    {
        public string ColumnName { get; set; }
        public string Operation { get; set; }
        public string ColumnValue { get; set; }
        public Condition Condition { get; set; }
    }
}
