﻿using Abp.Domain.Entities;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.Vessels.Dto
{
    public class CreateVesselGroupDto : Entity
    {
        public CreateVesselGroupDto()
        {
            VesselGroupDetails = new List<CreateVesselGroupDetailDto>();
        }
        public string Name { get; set; }
        public List<CreateVesselGroupDetailDto> VesselGroupDetails { get; set; }
        
    }
}
