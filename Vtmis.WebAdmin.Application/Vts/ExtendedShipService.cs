﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Vts.Dto;

namespace Vtmis.WebAdmin.Vts
{
    public class ExtendedShipService : ApplicationService, IExtendedShipService
    {
        private readonly IRepository<ExtendedShip> _extendedShipRepository;
        public ExtendedShipService(IRepository<ExtendedShip> extendedShipRepository)
        {
            _extendedShipRepository = extendedShipRepository;
        }

        [HttpGet]
        public IQueryable<ExtendedShip> QueryEntendedShip()
        {
            return _extendedShipRepository.GetAll();
        }

        [HttpGet]
        public async Task SoftDelete(int shipId)
        {
            var result = await _extendedShipRepository.FirstOrDefaultAsync(x => x.ShipId == shipId);
            result.IsDeleted = true;
            await _extendedShipRepository.UpdateAsync(result);
        }

        [HttpGet]
        [Route("SoftDeleteByMmsiCallSignImo")]
        public async Task SoftDelete(int? mmsi = null, string callSign = null, int? imo = null)
        {
            var result = _extendedShipRepository.GetAll();
            if (mmsi != null && mmsi.Value > 0)
            {
                result = result.Where(x => x.MMSI == mmsi);
            }
            if (imo != null && imo.Value > 0)
            {
                result = result.Where(x => x.IMO == imo);
            }
            if (!string.IsNullOrEmpty(callSign))
            {
                result = result.Where(x => x.CallSign.ToUpper() == callSign.ToUpper());
            }
            var ship = await result.FirstOrDefaultAsync();
            if (ship != null)
            {
                ship.IsDeleted = true;
            }            
            await _extendedShipRepository.UpdateAsync(ship);
        }

        public async Task<ExtendedShipDto> Create(ExtendedShipDto input)
        {
            return ObjectMapper.Map<ExtendedShipDto>(await _extendedShipRepository
                .InsertAsync(ObjectMapper.Map<ExtendedShip>(input)));
        }

        public async Task<ExtendedShipDto> Update(ExtendedShipDto input)
        {
            var result = await _extendedShipRepository.GetAsync(input.Id);
            result.IsBlacklisted = input.IsBlacklisted;
            result.ShipId = input.ShipId;

            return ObjectMapper.Map<ExtendedShipDto>(await _extendedShipRepository
                .UpdateAsync(ObjectMapper.Map<ExtendedShip>(result)));
        }

        public async Task<List<ExtendedShipDto>> Get()
        {
            var result = await _extendedShipRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ExtendedShipDto>>(result);
        }

        public async Task<ExtendedShipDto> GetById(int id)
        {
            return ObjectMapper.Map<ExtendedShipDto>(await _extendedShipRepository.GetAsync(id));
        }

        public async Task<ExtendedShipDto> GetByShipId(int id)
        {
            return ObjectMapper.Map<ExtendedShipDto>(await _extendedShipRepository.FirstOrDefaultAsync(x =>
                x.ShipId == id));
        }

        public async Task<ExtendedShipDto> GetByMmsiCallSignImo(int? mmsi,string callSign,int? imo)
        {
            var result = _extendedShipRepository.GetAll();
            if (mmsi != null && mmsi.Value > 0)
            {
                result = result.Where(x => x.MMSI == mmsi);
                //return ObjectMapper.Map<ExtendedShipDto>(await _extendedShipRepository.FirstOrDefaultAsync(x =>
                //x.MMSI == mmsi && x.IMO == imo && x.CallSign == callSign));
            }
            if (!string.IsNullOrEmpty(callSign))
            {
                result = result.Where(x => x.CallSign.ToUpper() == callSign.ToUpper());
            }
            if (imo != null && imo.Value > 0)
            {
                result = result.Where(x => x.IMO == imo);
            }
            return ObjectMapper.Map<ExtendedShipDto>(await result.FirstOrDefaultAsync());
        }

        public async Task Delete(int id)
        {
            var result = await _extendedShipRepository.GetAsync(id);

            await _extendedShipRepository.DeleteAsync(result);

        }
    }
}
