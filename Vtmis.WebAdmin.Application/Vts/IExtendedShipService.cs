﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Vts.Dto;

namespace Vtmis.WebAdmin.Vts
{
    public interface IExtendedShipService
    {
        IQueryable<ExtendedShip> QueryEntendedShip();
        Task SoftDelete(int shipId);
        Task SoftDelete(int? mmsi, string callSign, int? imo);
        Task<ExtendedShipDto> Create(ExtendedShipDto input);
        Task<ExtendedShipDto> Update(ExtendedShipDto input);
        Task<List<ExtendedShipDto>> Get();
        Task<ExtendedShipDto> GetById(int id);
        Task<ExtendedShipDto> GetByShipId(int id);
        Task<ExtendedShipDto> GetByMmsiCallSignImo(int? mmsi, string callSign, int? imo);
        Task Delete(int id);
    }
}
