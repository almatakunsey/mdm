﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.Vts.Dto
{
    [AutoMapFrom(typeof(ExtendedShip))]
    public class ExtendedShipDto : EntityDto
    {
        public int ShipId { get; set; }
        public bool IsBlacklisted { get; set; }
        public bool IsDeleted { get; set; }
        public int? MMSI { get; set; }
        public int? IMO { get; set; }
        public string CallSign { get; set; }
    }
}
