﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vtmis.WebAdmin.VtmisType;
using System.Linq;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.Colors
{
    public class AISColourService : ApplicationService
    {
        private readonly IRepository<AISColourSchema> _aisColourSchemaRepository;
        private readonly IRepository<ShipType> _shipTypeRepository;

        private readonly UserManager _userManager;

        public AISColourService(IRepository<AISColourSchema> aisColourSchemaRepository,IRepository<ShipType> shipTypeRepository, UserManager userManager)
        {
            _aisColourSchemaRepository = aisColourSchemaRepository;
            _shipTypeRepository = shipTypeRepository;
            _userManager = userManager;
        }

        public async Task<CreateAISColourSchemaDto> Create(CreateAISColourSchemaDto m)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var user = _userManager.Users.Where(c => c.Id == m.UserId).FirstOrDefault();

                if (user != null)
                {

                    var AISColorSchema = await this._aisColourSchemaRepository.FirstOrDefaultAsync(c => c.Name == m.Name && c.UserId == user.Id);

                    if (AISColorSchema == null)
                    {
                        AISColorSchema = ObjectMapper.Map<AISColourSchema>(m);
                        AISColorSchema.SetPersonalizeUser(user);
                        AISColorSchema.AISColourSchemaItems = ObjectMapper.Map<List<AISColourSchemaItem>>(m.AISColorSchemaItemDtos);
                        await AISColorSchema.UpdateAISColourSchemaItemRelation(_shipTypeRepository);

                        int id = await this._aisColourSchemaRepository.InsertAndGetIdAsync(AISColorSchema);

                        m.Id = id;
                        return m;
                    }
                    else
                    {
                        throw new UserFriendlyException("AISColorSchema with same name exist!");
                    }
                }
                else
                {
                    throw new UserFriendlyException("Invalid user!");
                }
            }
        }

        public async Task<UpdateAISColourSchema> Update(UpdateAISColourSchema m)
        {
            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken cancellationToken = source.Token;

            var AISColourSchema = await _aisColourSchemaRepository.GetAsync(m.Id);
            await _aisColourSchemaRepository.EnsureCollectionLoadedAsync(AISColourSchema, x => x.AISColourSchemaItems, cancellationToken);

            if (AISColourSchema != null)
            {
                AISColourSchema.Name = m.Name;
                AISColourSchema.ClearItemBeforeUpdate();
                AISColourSchema.AISColourSchemaItems = ObjectMapper.Map<List<AISColourSchemaItem>>(m.AISColorSchemaItemDtos);
                await AISColourSchema.UpdateAISColourSchemaItemRelation(_shipTypeRepository);

                await _aisColourSchemaRepository.UpdateAsync(AISColourSchema);
            }
            else
            {
                throw new UserFriendlyException("Unable to update AISColorSchema!");
            }

            return m;
        }

        public async Task Delete(int id)
        {
            var AISColorSchema = await _aisColourSchemaRepository.GetAsync(id);

            await _aisColourSchemaRepository.DeleteAsync(AISColorSchema);

        }

        public async Task<AISColourSchemaListDto> GetById(int id)
        {
            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken cancellationToken = source.Token;

            var AISColourSchema = await _aisColourSchemaRepository.GetAsync(id);
            await _aisColourSchemaRepository.EnsureCollectionLoadedAsync(AISColourSchema, m => m.AISColourSchemaItems, cancellationToken);


            var AISColorSchemaDto = ObjectMapper.Map<AISColourSchemaListDto>(AISColourSchema);
            AISColorSchemaDto.AISColourchemaItemDtos = ObjectMapper.Map<List<AISColourSchemaItemDto>>(AISColourSchema.AISColourSchemaItems);

            return AISColorSchemaDto;
        }

        public List<ListAISColourSchemaDto> GetAll(int userId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                CancellationTokenSource source = new CancellationTokenSource();
                CancellationToken cancellationToken = source.Token;

                var user = _userManager.Users.Where(c => c.Id == userId).FirstOrDefault();

                if (user != null)
                {
                    var AISColorSchemas = _aisColourSchemaRepository.GetAll().Where(c => c.TenantId == user.TenantId);

                    var listAISColorSchemaDto = ObjectMapper.Map<List<ListAISColourSchemaDto>>(AISColorSchemas);

                    return listAISColorSchemaDto;
                }
                else
                {
                    throw new UserFriendlyException("Invalid user!");
                }
            }
        }
    }


    [AutoMapFrom(typeof(AISColourSchema))]
    public class CreateAISColourSchemaDto:EntityDto
    {

        public long UserId { get; set; }
        public string Name { get; set; }
        public List<AISColourSchemaItemDto> AISColorSchemaItemDtos { get; set; }

    }

    [AutoMapFrom(typeof(AISColourSchema))]
    public class UpdateAISColourSchema : CreateAISColourSchemaDto
    {

    }

    [AutoMapFrom(typeof(AISColourSchema))]
    public class ListAISColourSchemaDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }
    }

    [AutoMapFrom(typeof(AISColourSchema))]
    public class AISColourSchemaListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }
        public List<AISColourSchemaItemDto> AISColourchemaItemDtos { get; set; }
    }


    [AutoMapFrom(typeof(AISColourSchemaItem))]
    public class AISColourSchemaItemDto
    {
        public string ColorName { get; set; }
        public string ShipTypeName { get; set; }
        public string ShipTypeNumber { get; set; }
        public int ShipTypeId { get; set; }
        public AISType AISType { get; set; }
        public DateTime CreationTime { get; set; }
    }
}