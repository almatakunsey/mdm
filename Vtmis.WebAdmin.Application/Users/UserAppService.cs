using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.IdentityFramework;
using Abp.Localization;
using Abp.Runtime.Session;
using Vtmis.WebAdmin.Authorization;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Users.Dto;
using Vtmis.WebAdmin.Roles.Dto;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Model;
using Abp.Domain.Uow;
using Vtmis.WebAdmin.MultiTenancy;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories;
using Abp.AutoMapper;
using System;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.VesselOwneds;
using Vtmis.WebAdmin.VesselOwneds.Dto;
using Abp.UI;
using System.ComponentModel.DataAnnotations;
using Vtmis.Core.Model.DTO;
using Vtmis.WebAdmin.Permissions;
using Abp.Authorization.Users;

namespace Vtmis.WebAdmin.Users
{
    //[AbpAuthorize(PermissionNames.Pages_Users)]
    [AbpAuthorize]
    public class UserAppService : AsyncCrudAppService<User, UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>, IUserAppService
    {
        private readonly IRepository<User, long> _userRepo;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IRepository<Role> _roleRepository;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly TenantManager _tenantManager;
        private readonly IMdmUserRoleRepository _userRoleRepo;
        private readonly IRepository<VesselOwned> _vesselOwnRepo;
        private readonly IMdmPermissionService _mdmPermissionService;

        public UserAppService(
            IRepository<User, long> repository,
            UserManager userManager,
            RoleManager roleManager,
            IRepository<Role> roleRepository,
            IPasswordHasher<User> passwordHasher,
            TenantManager tenantManager, IMdmUserRoleRepository userRoleRepo,
            IRepository<VesselOwned> vesselOwnRepo,
            IMdmPermissionService mdmPermissionService)
            : base(repository)
        {
            _userRepo = repository;
            _userManager = userManager;
            _roleManager = roleManager;
            _roleRepository = roleRepository;
            _passwordHasher = passwordHasher;
            _tenantManager = tenantManager;
            _userRoleRepo = userRoleRepo;
            _vesselOwnRepo = vesselOwnRepo;
            _mdmPermissionService = mdmPermissionService;
        }

        public override async Task<UserDto> Get(EntityDto<long> input)
        {
            var user = await _userRepo.GetAll().IgnoreQueryFilters().Include(x => x.Roles).FirstOrDefaultAsync(x => x.Id == input.Id);

            return new UserDto
            {
                Id = user.Id,
                CreationTime = user.CreationTime,
                EmailAddress = user.EmailAddress,
                FullName = user.FullName,
                IsActive = user.IsActive,
                Roles = user.Roles.Select(s => MapRole(s)).ToList(),
                LastLoginTime = user.LastModificationTime,
                Name = user.Name,
                Surname = user.Surname,
                TenantId = user.TenantId,
                UserName = user.UserName,
                RoleNames = user.Roles.Select(s => MapRoleName(s)).ToArray()
            };
        }

        private string MapRoleName(UserRole s)
        {
            var role = _roleRepository.GetAll().IgnoreQueryFilters().FirstOrDefault(x => x.Id == s.RoleId);
            return role.Name;
        }

        private MetaRole MapRole(UserRole s)
        {
            var role = _roleRepository.GetAll().IgnoreQueryFilters().FirstOrDefault(x => x.Id == s.RoleId);
            return new MetaRole
            {
                Id = role.Id,
                DisplayName = role.DisplayName,
                TenancyName = role.Name
            };
        }

        [AbpAllowAnonymous]
        public async Task<User> GetUserByUserNameAndTenantId(string username, int? tenantId)
        {
            return await _userRepo.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenantId ==
                        tenantId && x.UserName.ToLower() == username.ToLower() && x.IsDeleted == false);
        }

        public async Task<UserDto> DeactivateUser(long id)
        {

            try
            {
                var user = await _userRepo.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == id);
                var isHighPermission = false;
                if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_Deactivate_Mdm, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_Deactivate_Tenant, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                    }
                }
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
                if (user == null)
                {
                    throw new UserFriendlyException("Invalid user!");
                }
                else
                {
                    if (user.Id == AbpSession.UserId)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }

                    if (user.IsActive == true)
                    {
                        user.IsActive = false;
                    }
                    else
                    {
                        user.IsActive = true;
                    }

                    await _userRepo.UpdateAsync(user);
                    await CurrentUnitOfWork.SaveChangesAsync();


                }
                return ObjectMapper.Map<UserDto>(user);
            }
            catch (Exception error)
            {

                throw error;
            }


        }

        [AbpAllowAnonymous]
        public async Task ChangePassword(RequestChangePassword input)
        {
            var tenant = await _tenantManager.FindByTenancyNameAsync(input.TenantName);
            var tenantId = tenant?.Id;

            var user = await _userRepo.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.NormalizedUserName == input.Username.ToUpper() &&
                                      x.TenantId == tenantId);
            var isValid = await _userManager.CheckPasswordAsync(user, input.OldPassword);
            if (isValid)
            {
                var result = await _userManager.ChangePasswordAsync(user, input.NewPassowrd);
                if (!result.Succeeded)
                {
                    throw new UserFriendlyException("Ooppps! There is a problem!", "Failed to change password");
                }
            }
            else
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "Invalid users");
            }

        }

        /// <summary>
        /// Get All Users.
        /// </summary>
        /// <param name="input"></param>
        public override async Task<PagedResultDto<UserDto>> GetAll(PagedResultRequestDto input)
        {
            try
            {

                var users = _userRepo.GetAll().IgnoreQueryFilters()
                   .Include(x => x.Roles).Where(x => x.IsDeleted == false).AsQueryable();
                var isHighPermission = false;

                if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_View_Mdm, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_View_Tenant, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        users = users.Where(x => x.TenantId == AbpSession.TenantId);
                    }
                }
                if (isHighPermission == false)
                {
                    return new PagedResultDto<UserDto>();
                }

                var result = ObjectMapper.Map<List<UserDto>>(users);
                var userCounter = result.Count;

                var finalMap = result.Select(s => new UserDto
                {
                    CreationTime = s.CreationTime,
                    EmailAddress = s.EmailAddress,
                    FullName = s.FullName,
                    Id = s.Id,
                    IsActive = s.IsActive,
                    LastLoginTime = s.LastLoginTime,
                    Name = s.Name,
                    RoleNames = GetListOfRoles(_userRepo.GetAll().IgnoreQueryFilters()
                                    .FirstOrDefault(x => x.Id == s.Id).Roles.Select(r => r.RoleId)),
                    Surname = s.Surname,
                    UserName = s.UserName,
                    IsAdmin = s.TenantId == null || GetListOfRoles(_userRepo.GetAll().IgnoreQueryFilters()
                                    .FirstOrDefault(x => x.Id == s.Id).Roles.Select(r => r.RoleId)).Contains("ADMIN")
                                    ? true : false,
                    TenantId = s.TenantId,
                    TenantName = s.TenantId != null ? _tenantManager.FindByIdAsync(s.TenantId.Value).Result.Name : null
                    //Vessels = GetVessels(s.TenantId == null || GetListOfRoles(_userRepo.GetAll().IgnoreQueryFilters()
                    //                .FirstOrDefault(x => x.Id == s.Id).Roles.Select(r => r.RoleId)).Contains("ADMIN")
                    //                ? true : false, s.TenantId, s.Id)
                });
                //Skip(
                //    (input.SkipCount - 1) * input.MaxResultCount)
                //    .Take(input.MaxResultCount)
                return new PagedResultDto<UserDto>
                {
                    TotalCount = userCounter,
                    Items = finalMap.MapTo<List<UserDto>>()
                };
            }
            catch (Exception er)
            {

                throw er;
            }
        }

        private List<Vessel> GetVessels(bool isAdmin, int? tenantId, long userId)
        {
            var requestDate = string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL,
                                    StringComparison.CurrentCultureIgnoreCase) ? AppHelper.GetDevAisDate().ToDateTime() :
                                    DateTime.Now;
            using (var aisDbContext = new VesselDatabaseContext(requestDate))
            {
                var qry = from ss in
                            (from iss in aisDbContext.ShipStatics
                             group iss by iss.MMSI into g
                             select new { MMSI = g.Max(e => e.MMSI), Name = g.Max(e => e.Name) })
                          where ss.MMSI >= 0
                          select new Vessel { MMSI = ss.MMSI, VesselName = ss.Name };
                //var qry = aisDbContext.ShipStatics.Where(x=> x.MMSI > 0).Select(x => new Vessel { MMSI = x.MMSI, VesselName = x.Name }).AsQueryable();
                if (tenantId == null)
                {
                    return qry.ToList();
                }

                if (isAdmin)
                {
                    var owners = GetVesselListByTenantId(tenantId);
                    return qry.Where(x => owners.Items.Any(i => i.MMSI == x.MMSI)).ToList();
                }

                var owner = GetVesselListByUserId(userId);
                return qry.Where(x => owner.Items.Any(i => i.MMSI == x.MMSI)).ToList();
            }
        }

        private ListResultDto<VesselArrayIdDto> GetVesselListByTenantId(int? tenantId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {

                var _vesselOwned = _vesselOwnRepo.GetAll()
                                    .AsNoTracking().IgnoreQueryFilters().Where(s => s.TenantId == tenantId);  //await _vesselOwnedRepository.FirstOrDefaultAsync(c => c.UserId == user.Id);

                if (_vesselOwned != null)
                {
                    var vessels = _vesselOwned.Select(x => x.MMSI);
                    var col = new Dictionary<int, List<VesselArrayIdDto>>();
                    var count = 1;
                    foreach (var item in vessels)
                    {
                        col.Add(count,
                           Newtonsoft.Json.JsonConvert
                               .DeserializeObject<List<VesselArrayIdDto>>(item));
                        count += 1;
                    }
                    var results = new List<VesselArrayIdDto>();
                    foreach (var item in col.Values)
                    {
                        foreach (var item2 in item)
                        {
                            if (!results.Where(x => x.MMSI == item2.MMSI).Any())
                            {
                                results.Add(item2);
                            }
                        }
                    }
                    return new ListResultDto<VesselArrayIdDto>(results);
                }
                else
                {
                    return new ListResultDto<VesselArrayIdDto>();
                }

            }
        }

        private ListResultDto<VesselArrayIdDto> GetVesselListByUserId(long userId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {

                var _vesselOwned = _vesselOwnRepo.GetAll()
                                    .AsNoTracking().IgnoreQueryFilters().Where(s => s.UserId == userId);

                if (_vesselOwned != null)
                {
                    var vessels = _vesselOwned.Select(x => x.MMSI);
                    var col = new Dictionary<int, List<VesselArrayIdDto>>();
                    var count = 1;
                    foreach (var item in vessels)
                    {
                        col.Add(count,
                           Newtonsoft.Json.JsonConvert
                               .DeserializeObject<List<VesselArrayIdDto>>(item));
                        count += 1;
                    }
                    var results = new List<VesselArrayIdDto>();
                    foreach (var item in col.Values)
                    {
                        foreach (var item2 in item)
                        {
                            if (!results.Where(x => x.MMSI == item2.MMSI).Any())
                            {
                                results.Add(item2);
                            }
                        }
                    }
                    return new ListResultDto<VesselArrayIdDto>(results);
                }
                else
                {
                    return new ListResultDto<VesselArrayIdDto>();
                }

            }
        }

        /// <summary>
        /// Get All Users. TODO: Remove includeVessel
        /// </summary>
        /// <param name="input"></param>
        /// <param name="includeVessel"></param>
        /// <returns></returns>
        public async Task<PagedResultDto<UserDto>> GetAllAsync(PagedResultRequestDto input, bool includeVessel = false)
        {
            try
            {


                var users = _userRepo.GetAll().IgnoreQueryFilters()
                   .Include(x => x.Roles).Where(x => x.IsDeleted == false).AsQueryable();
                var isHighPermission = false;

                if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_View_Mdm, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_View_Tenant, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        users = users.Where(x => x.TenantId == AbpSession.TenantId);
                    }
                }
                if (isHighPermission == false)
                {
                    return new PagedResultDto<UserDto>();
                }

                var result = ObjectMapper.Map<List<UserDto>>(users);
                var userCounter = result.Count;

                var finalMap = result.Select(s => new UserDto
                {
                    CreationTime = s.CreationTime,
                    EmailAddress = s.EmailAddress,
                    FullName = s.FullName,
                    Id = s.Id,
                    IsActive = s.IsActive,
                    LastLoginTime = s.LastLoginTime,
                    Name = s.Name,
                    RoleNames = GetListOfRoles(_userRepo.GetAll().IgnoreQueryFilters()
                                    .FirstOrDefault(x => x.Id == s.Id).Roles.Select(r => r.RoleId)),
                    Surname = s.Surname,
                    UserName = s.UserName,
                    IsAdmin = s.TenantId == null || GetListOfRoles(_userRepo.GetAll().IgnoreQueryFilters()
                                    .FirstOrDefault(x => x.Id == s.Id).Roles.Select(r => r.RoleId)).Contains("ADMIN")
                                    ? true : false,
                    TenantId = s.TenantId,
                    TenantName = s.TenantId != null ? _tenantManager.FindByIdAsync(s.TenantId.Value).Result.Name : null
                    //Vessels = includeVessel == true ? GetVessels(s.TenantId == null || GetListOfRoles(_userRepo.GetAll().IgnoreQueryFilters()
                    //                .FirstOrDefault(x => x.Id == s.Id).Roles.Select(r => r.RoleId)).Contains("ADMIN")
                    //                ? true : false, s.TenantId, s.Id) : null
                });

                return new PagedResultDto<UserDto>
                {
                    TotalCount = userCounter,
                    Items = finalMap.MapTo<List<UserDto>>()
                };
            }
            catch (Exception er)
            {

                throw er;
            }
        }

        private string[] GetListOfRoles(IEnumerable<int> rolesId)
        {
            var roles = new List<string>();
            foreach (var item in rolesId)
            {
                roles.Add(_roleRepository.GetAll().IgnoreQueryFilters()
                                        .FirstOrDefault(x => x.Id == item).NormalizedName);
            }
            return roles.ToArray();
        }

        public async Task InternalCreate(User input)
        {
            var isHighPermission = false;
            if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_Create_Mdm, true))
            {
                isHighPermission = true;
            }

            if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_Create_Tenant, true))
            {
                if (isHighPermission == false)
                {
                    isHighPermission = true;
                    if (input.TenantId != AbpSession.TenantId)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }
            }

            if (isHighPermission == false)
            {
                throw new ForbiddenException("Permission Denied");
            }
            var isUserExist = _userRepo.GetAll().IgnoreQueryFilters().Where(x => x.TenantId == input.TenantId
                                  && x.Name.ToLower() == input.Name.ToLower()).Any();

            if (isUserExist == false)
            {
                await _userRepo.InsertAsync(input);
            }

            CurrentUnitOfWork.SaveChanges();
        }

        public override async Task<UserDto> Create(CreateUserDto input)
        {
            try
            {
                CheckCreatePermission();
                var isHighPermission = false;
                if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_Create_Mdm, true))
                {
                    isHighPermission = true;
                }

                var user = ObjectMapper.Map<User>(input);

                user.TenantId = input.TenantId == null ? AbpSession.TenantId : input.TenantId;
                user.Password = _passwordHasher.HashPassword(user, input.Password);
                user.IsEmailConfirmed = true;
                user.SetNormalizedNames();

                if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_Create_Tenant, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        if (user.TenantId != AbpSession.TenantId)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }
                }

                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }

                if (await _userRepo.GetAll().IgnoreQueryFilters().Where(x => x.EmailAddress.IsEqual(input.EmailAddress)).AnyAsync())
                {
                    throw new UserFriendlyException($"Email {input.EmailAddress} already exist!.");
                }

                var isUserExist = _userRepo.GetAll().IgnoreQueryFilters().Where(x => x.TenantId == user.TenantId
                                      && x.UserName.ToLower() == input.UserName.ToLower()).Any();

                if (isUserExist == false)
                {
                    await _userRepo.InsertAsync(user);
                    CurrentUnitOfWork.SaveChanges();

                    if (input.RoleIds != null)
                    {
                        if (input.RoleIds.Any())
                        {
                            var roles = new List<string>();
                            foreach (var item in input.RoleIds)
                            {
                                await _userManager.SetRoleToUser(user.Id, item, user.TenantId);

                                //var roleName = (await _roleRepository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == item))?.Name;
                                //roles.Add(roleName);
                            }
                            CurrentUnitOfWork.SaveChanges();
                            //input.RoleNames = roles.ToArray();
                        }
                    }


                    //if (input.RoleNames != null)
                    //{
                    //    await _userManager.SetRoles(user, input.RoleNames);
                    //    CurrentUnitOfWork.SaveChanges();
                    //}
                }
                else
                {
                    throw new UserFriendlyException($"User {input.Name} already exist!.");
                }
                return ObjectMapper.Map<UserDto>(user);
            }
            catch (Exception err)
            {

                throw err;
            }


        }

        public override async Task<UserDto> Update(UserDto input)
        {
            try
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant, AbpDataFilters.SoftDelete))
                {
                    CheckUpdatePermission();
                    var isHighPermission = false;
                    if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_Update_Mdm, true))
                    {
                        isHighPermission = true;
                    }

                    var user = await _userRepo.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == input.Id);
                    if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_Update_Tenant, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            if (user.TenantId != AbpSession.TenantId)
                            {
                                throw new ForbiddenException("Permission Denied");
                            }
                        }
                    }
                    input.TenantId = user.TenantId;
                    MapToEntity(input, user);
                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }

                    CheckErrors(await _userManager.UpdateAsync(user));

                    if (input.RoleIds.Any())
                    {
                        await _userRoleRepo.RemoveAllRoleFromUser(user.Id);
                        await _userRoleRepo.SetRoles(user.Id, user.TenantId, input.RoleIds);
                    }
                    else
                    {
                        if (input.RoleNames != null)
                        {
                            await _userRoleRepo.RemoveAllRoleFromUser(user.Id);
                            CheckErrors(await _userManager.SetRoles(user, input.RoleNames));
                        }
                    }

                    return await Get(input);
                }
            }
            catch (Exception err)
            {

                throw err;
            }

        }

        public override async Task Delete(EntityDto<long> input)
        {
            var user = await _userManager.GetUserByIdAsync(input.Id);
            var isHighPermission = false;
            if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_Delete_Mdm, true))
            {
                isHighPermission = true;
            }
            if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.User_Delete_Tenant, true))
            {
                if (isHighPermission == false)
                {
                    isHighPermission = true;
                    if (user.TenantId != AbpSession.TenantId)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }
            }
            await _userManager.DeleteAsync(user);
        }

        public async Task<ListResultDto<RoleDto>> GetRoles()
        {
            var qry = _roleRepository.GetAll().IgnoreQueryFilters().AsQueryable();
            var isHighPermission = false;
            if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.Role_View_Mdm, true))
            {
                isHighPermission = true;
            }
            if (await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.Role_View_Tenant, true))
            {
                if (isHighPermission == false)
                {
                    isHighPermission = true;
                    qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                }
            }
            if (isHighPermission == false)
            {
                return new ListResultDto<RoleDto>();
            }
            return new ListResultDto<RoleDto>(ObjectMapper.Map<List<RoleDto>>(qry));
        }

        public async Task ChangeLanguage(ChangeUserLanguageDto input)
        {
            await SettingManager.ChangeSettingForUserAsync(
                AbpSession.ToUserIdentifier(),
                LocalizationSettingNames.DefaultLanguage,
                input.LanguageName
            );
        }

        public async Task<UserDto> GetUserByIdAsync(long userId)
        {
            var user = await GetEntityByIdAsync(userId);
            return MapToEntityDto(user);
        }

        protected override User MapToEntity(CreateUserDto createInput)
        {
            var user = ObjectMapper.Map<User>(createInput);
            user.SetNormalizedNames();
            return user;
        }

        protected override void MapToEntity(UserDto input, User user)
        {
            ObjectMapper.Map(input, user);
            user.SetNormalizedNames();
        }

        protected override UserDto MapToEntityDto(User user)
        {
            var roles = _roleManager.Roles.Where(r => user.Roles.Any(ur => ur.RoleId == r.Id)).Select(r => r.NormalizedName);
            var userDto = base.MapToEntityDto(user);
            userDto.RoleNames = roles.ToArray();
            return userDto;
        }

        protected override IQueryable<User> CreateFilteredQuery(PagedResultRequestDto input)
        {
            return Repository.GetAllIncluding(x => x.Roles);
        }

        protected override async Task<User> GetEntityByIdAsync(long id)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var result = await Repository.GetAllIncluding(x => x.Roles).FirstOrDefaultAsync(x => x.Id == id);
                return result;
            }
        }

        protected override IQueryable<User> ApplySorting(IQueryable<User> query, PagedResultRequestDto input)
        {
            return query.OrderBy(r => r.UserName);
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }

        public void CreateUserWithCustomRolePermission()
        {
            throw new System.NotImplementedException();
        }
    }
}
