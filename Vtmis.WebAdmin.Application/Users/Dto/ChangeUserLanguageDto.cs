using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}