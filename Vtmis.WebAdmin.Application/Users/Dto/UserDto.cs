using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.Users.Dto
{
    [AutoMapFrom(typeof(User))]
    public class UserDto : EntityDto<long>
    {
        public UserDto()
        {
            Vessels = new List<Vessel>();
            Roles = new List<MetaRole>();
        }
        [Required]
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [StringLength(AbpUserBase.MaxSurnameLength)]
        public string Surname { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(AbpUserBase.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }

        public bool IsActive { get; set; }

        public string FullName { get; set; }

        public DateTime? LastLoginTime { get; set; }

        public DateTime CreationTime { get; set; }

        public string[] RoleNames { get; set; }
        public int[] RoleIds { get; set; }
        public List<MetaRole> Roles { get; set; }
        public int? TenantId { get; set; }
        public string TenantName { get; set; }
        public bool IsAdmin { get; set; }
        public List<Vessel> Vessels { get; set; }
    }

    public class MetaRole
    {
        public long Id { get; set; }
        public string TenancyName { get; set; }
        public string DisplayName { get; set; }
    }

    public class Vessel
    {
        public int MMSI { get; set; }
        public string VesselName { get; set; }
    }
}
