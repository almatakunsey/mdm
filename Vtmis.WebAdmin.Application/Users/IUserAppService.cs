using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Vtmis.Core.Model.DTO;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Roles.Dto;
using Vtmis.WebAdmin.Users.Dto;

namespace Vtmis.WebAdmin.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>
    {
        Task ChangePassword(RequestChangePassword input);
        Task<User> GetUserByUserNameAndTenantId(string username, int? tenantId);
        Task InternalCreate(User input);
        //Task<List<UserDto>> GetAllAsync();
        Task<PagedResultDto<UserDto>> GetAllAsync(PagedResultRequestDto input, bool includeVessel = false);
        //Task<PagedResultDto<UserDto>> GetAll(PagedResultRequestDto input, bool includeVessel);
        Task<ListResultDto<RoleDto>> GetRoles();

        Task ChangeLanguage(ChangeUserLanguageDto input);

        void CreateUserWithCustomRolePermission();

        Task<UserDto> GetUserByIdAsync(long userId);

        Task<UserDto> DeactivateUser(long id);
     
    }
}
