﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Reports;
using Vtmis.WebAdmin.ReportTypes.Dto;


namespace Vtmis.WebAdmin.ReportTypes
{
    public class ReportTypeService : ApplicationService, IReportTypeService
    {
        private readonly IRepository<ReportType> _reportTypeRepository;

        public ReportTypeService(IRepository<ReportType> reportTypeRepository)
        {
            _reportTypeRepository = reportTypeRepository;
        }

        public async Task<ReportTypeDto> Create(ReportTypeDto input)
        {
            var reportType = await _reportTypeRepository.FirstOrDefaultAsync(c => c.Name.ToLower()
                == input.Name.ToLower());

            if (reportType == null)
            {
                reportType = ObjectMapper.Map<ReportType>(input);
             
                int id = await _reportTypeRepository.InsertAndGetIdAsync(reportType);

                input.Id = id;
            }

            return input;
        }

        public async Task<List<ReportTypeDto>> Get()
        {
            var shipTypes = await _reportTypeRepository.GetAllListAsync();

            return ObjectMapper.Map<List<ReportTypeDto>>(shipTypes);
        }

        public async Task<ReportTypeDto> GetById(int id)
        {
            var reportType = await _reportTypeRepository.FirstOrDefaultAsync(c => c.Id == id);

            if (reportType == null)
            {
                throw new UserFriendlyException("Report Does Not Exist");
            }
            else
            {
                return ObjectMapper.Map<ReportTypeDto>(reportType);
            }
        }

        public async Task<ReportTypeDto> Update(ReportTypeDto input)
        {

            var reportType = await _reportTypeRepository.GetAsync(input.Id);

            if (reportType != null)
            {
                reportType.Name = input.Name;

                await _reportTypeRepository.UpdateAsync(reportType);
            }

            return input;
        }

        public async Task Delete(int id)
        {
            var reportType = await _reportTypeRepository.GetAsync(id);

            await _reportTypeRepository.DeleteAsync(reportType);

        }
    }
}
