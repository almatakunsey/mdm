﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using Vtmis.WebAdmin.Reports;
using Vtmis.WebAdmin.VtmisType;

namespace Vtmis.WebAdmin.ReportTypes.Dto
{
    [AutoMapFrom(typeof(ReportType))]
    public class ReportTypeDto : EntityDto
    {
        public ReportTypeDto()
        {
            CreationTime = DateTime.Now;
        }
        public DateTime CreationTime { get; set; }
        public string Name { get; set; }

        //01 - Zone Summary
        //02 - Extended Zone Summary
        //03 - Arrival/Departure
        //04 - Arrival Time Forecast
        //05 - Berth Time Summary
        //06 - Speed Summary
        //07 - Speed Violation
        //08 - Enter Exit
        //09 - Weather
        //10 - Out-of-tolerance
        //11 - Daily Fuel Consumption
        //12 - Mothly Fuel Consumption
        //13 - Extimated Fule Consumption
        //14 - Zone Details
        //15 - Extended Zone Details
        //16 - Anchorage Time
        //17 - Extended Anchorage Time
        //18 - Zone to Zone Trips
        //19 - Zone to Zone Trips with Draught
        //20 - Route ETA
        public string ReportTypeNumber { get; set; }
        public Status Status { get; set; }

    }
}
