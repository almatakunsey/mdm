﻿using Abp.Application.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.ReportTypes.Dto;

namespace Vtmis.WebAdmin.ReportTypes
{
    public interface IReportTypeService : IApplicationService
    {
        Task<ReportTypeDto> Create(ReportTypeDto input);
        Task<List<ReportTypeDto>> Get();
        Task<ReportTypeDto> GetById(int id);
        Task<ReportTypeDto> Update(ReportTypeDto input);
        Task Delete(int id);
    }
}
