﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages;

namespace Vtmis.WebAdmin.EICS
{
    public interface IDapperAdmin
    {
        Task<Expression<Func<VesselPosition, bool>>> GetTenantFilter(string userId);
        EICSModel QueryEICS(int mmsi);
        string QueryLloydsShipType(int? imo, string callSign);
        Task<TargetOwner> GetTargetsByUserId(int userId);
    }
}