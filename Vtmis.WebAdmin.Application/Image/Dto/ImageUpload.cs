﻿namespace Vtmis.WebAdmin.Image.Dto
{
    public class ImageUpload
    {
        public string Name { get; set; }

        public string Path { get; set; }
        public string  FullPath { get; set; }
        public string Original { get; set; }
        public string MMSI { get; set; }

    }
}
