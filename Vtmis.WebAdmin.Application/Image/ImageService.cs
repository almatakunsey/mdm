﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.DTO.Image;
using Vtmis.WebAdmin.Images;
using Vtmis.Core.Request.Image;
using System.IO;
using Newtonsoft.Json;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Constants.Images;
using Abp.UI;
using Vtmis.WebAdmin.Image.Dto;

namespace Vtmis.WebAdmin.Image
{
    [RemoteService(IsMetadataEnabled = false)]
    public class ImageService : ApplicationService, IImageService
    {
        private readonly IRepository<ImageInfo, long> _repository;

        public ImageService(IRepository<ImageInfo, long> repository)
        {
            _repository = repository;
        }

        public async Task<List<ImageUpload>> Seed()
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var jsonFile = $@"{AppDomain.CurrentDomain.BaseDirectory}/watermark.json";


                    var model = JsonConvert.DeserializeObject<List<ImageUpload>>(File.ReadAllText(jsonFile));


                    var result = model.Select(x => new ImageInfo
                    {

                        ImageId = x.MMSI,
                        ImageType = ImageType.Target,
                        ImageDetails = model.Where(w => w.MMSI == x.MMSI).Select(z => new ImageDetail
                        {
                            Name = z.Name,
                            FullPath = z.FullPath,
                            Path = z.Path,
                            Original = z.Original
                        }).ToList()
                    });

                    foreach (var item in result)
                    {

                        var mmsi = await _repository.GetAll().IgnoreQueryFilters().AsNoTracking().AnyAsync(x => x.ImageId == item.ImageId);

                        if (mmsi == false)
                        {
                            await _repository.InsertAsync(item);
                        }
                        await CurrentUnitOfWork.SaveChangesAsync();
                    }


                    return model;

                }
                catch (Exception error)
                {

                    throw error;
                }
            }

        }

        public async Task<bool> DeleteImages(RequestDeleteDto input)
        {
            try
            {
                var image = await _repository.GetAll().IgnoreQueryFilters().Include(x => x.ImageDetails).Where(x => x.ImageId.IsEqual(input.ImageId) && x.ImageType == input.ImageType).FirstOrDefaultAsync();

                if (image == null)
                {
                    throw new UserFriendlyException("Image Not Found");
                }

                var name = image.ImageDetails.Where(x => input.Name.Any(s => x.Name.IsEqual(s))).ToList();

                var imageRequest = new ImageRequest();

                foreach (var item in name)
                {
                    var deleteResult = await imageRequest.DeleteImageAsync(new DeleteImageDto
                    {
                        Path = item.Path,
                        Name = item.Name
                    });

                    //delete from child table in DB
                    image.ImageDetails.Remove(item);
                    await _repository.UpdateAsync(image);

                }

                await CurrentUnitOfWork.SaveChangesAsync();

                return true;

            }
            catch (Exception error)
            {
                return false;
            }

        }



        public async Task<IEnumerable<ResponseImageDto>> GetImages(RequestImageDto input, string baseUri)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var image = await _repository.GetAll().IgnoreQueryFilters().Include(x => x.ImageDetails)
                                        .FirstOrDefaultAsync(x => x.ImageId.IsEqual(input.ImageId)
                                    && x.ImageType == input.ImageType);
                if (image == null)
                {
                    throw new UserFriendlyException("Image Not Found");
                }

                return image.ImageDetails.Select(x => new ResponseImageDto
                {
                    Name = x.Name,
                    Original = $"{AppHelper.ConvertRequestUrl(baseUri)}/{x.FullPath}",
                    Path = x.Path,
                    Thumbnail = $"{AppHelper.ConvertRequestUrl(baseUri)}/{x.FullPath}{ImageParameters.Thumbnail}"                   
                });
            }
        }

        public async Task<IEnumerable<ResponseImageDto>> CreateOrUpdateAsync(string imageId, ImageType imageType, IEnumerable<ResponseImageDto> input, string baseUri)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var image = await _repository.GetAll().IgnoreQueryFilters().Include(x => x.ImageDetails)
                                            .FirstOrDefaultAsync(x => x.ImageId.IsEqual(imageId)
                                            && x.ImageType == imageType);
                    var result = new ImageInfo();
                    if (image != null)
                    {
                        var imageDetails = image.ImageDetails.Select(x => new ImageDetail
                        {
                            FullPath = x.FullPath,
                            Name = x.Name,
                            Path = x.Path                         
                        }).ToList();

                        imageDetails.AddRange(input.Select(x => new ImageDetail
                        {
                            FullPath = x.Original,
                            Name = x.Name,
                            Path = x.Path                            
                        }));
                        image.ImageDetails = imageDetails;
                        result = await _repository.UpdateAsync(image);
                    }
                    else
                    {
                        result = await _repository.InsertOrUpdateAsync(new ImageInfo
                        {
                            ImageId = imageId,
                            ImageType = imageType,
                            ImageDetails = input.Select(x => new ImageDetail
                            {
                                FullPath = x.Original,
                                Name = x.Name,
                                Path = x.Path                               
                            }).ToList()
                        });
                    }

                    await CurrentUnitOfWork.SaveChangesAsync();

                    return result.ImageDetails.Select(x => new ResponseImageDto
                    {
                        Name = x.Name,
                        Original = $"{AppHelper.ConvertRequestUrl(baseUri)}/{x.FullPath}",
                        Path = x.Path,
                        Thumbnail = $"{AppHelper.ConvertRequestUrl(baseUri)}/{x.FullPath}{ImageParameters.Thumbnail}"                      
                    });
                }
                catch (Exception err)
                {
                    throw new Exception("Error", err);
                }
            }
        }

        public async Task<IEnumerable<ResponseImageDto>> UploadImageAsync(RequestUploadImage input, string baseUri)
        {
            var dto = new List<CallbackUploadDto>();
            var imageRequest = new ImageRequest();
            var path = "MDM";
            if (input.ImageType == Core.Common.Enums.ImageType.Target)
            {
                path = $"{path}/{(int)input.ImageType}/{input.ImageId}";
            }
            else
            {
                var tenantId = AbpSession.TenantId.HasValue ? AbpSession.TenantId.ToStringOrDefault() : "null";
                path = $"{path}/{tenantId}/{(int)input.ImageType}/{input.ImageId}";
            }
            foreach (var item in input.Images.Files)
            {
                var extension = Path.GetExtension(item.FileName).Remove(0, 1);
                var stream = item.OpenReadStream();
                var uploadResult = await imageRequest.UploadImageAsync(new UploadImageDto
                {
                    ImageData = $"data:image/{extension};base64,{CommonHelper.ConvertImageToBase64(stream)}",
                    Name = Path.GetFileNameWithoutExtension(item.FileName),
                    Path = $"MDM/{(int)input.ImageType}/{input.ImageId}",
                    Type = "png"
                });
                var uploadResultObj = JsonConvert.DeserializeObject<CallbackUploadDto>(uploadResult);
                dto.Add(uploadResultObj);
            }

            var result = dto.Select(x => new ResponseImageDto
            {
                Name = x.Raw.Name,
                Original = $"{x.Raw.FullPath}",
                Path = x.Raw.Path,
                Thumbnail = $"{x.Raw.FullPath}{ImageParameters.Thumbnail}"              
            });

            //TODO: Save image here
            await CreateOrUpdateAsync(input.ImageId, input.ImageType, result, baseUri);
            //End save image

            return result;
        }

        public async Task<IEnumerable<ResponseImageDto>> UploadImageAsync(RequestUploadImageBase64 input, string baseUri)
        {
            var dto = new List<CallbackUploadDto>();
            var imageRequest = new ImageRequest();
            var path = "MDM";
            if (input.ImageType == Core.Common.Enums.ImageType.Target)
            {
                path = $"{path}/{(int)input.ImageType}/{input.ImageId}";
            }
            else
            {
                var tenantId = AbpSession.TenantId.HasValue ? AbpSession.TenantId.ToStringOrDefault() : "null";
                path = $"{path}/{tenantId}/{(int)input.ImageType}/{input.ImageId}";
            }
            foreach (var item in input.Images)
            {
                //var extension = Path.GetExtension(item.FileName).Remove(0, 1);
                //var stream = item.OpenReadStream();
                var uploadResult = await imageRequest.UploadImageAsync(new UploadImageDto
                {
                    ImageData = item,
                    Name = input.ImageId,
                    Path = $"MDM/{(int)input.ImageType}/{input.ImageId}",
                    Type = "png"
                });
                var uploadResultObj = JsonConvert.DeserializeObject<CallbackUploadDto>(uploadResult);
                dto.Add(uploadResultObj);
            }

            var result = dto.Select(x => new ResponseImageDto
            {
                Name = x.Raw.Name,
                Original = $"{x.Raw.FullPath}",
                Path = x.Raw.Path,
                Thumbnail = $"{x.Raw.FullPath}{ImageParameters.Thumbnail}"                
            });

            //TODO: Save image here
            await CreateOrUpdateAsync(input.ImageId, input.ImageType, result, baseUri);
            //End save image

            return result;
        }


    }
}