﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Model.DTO.Image;
using Vtmis.WebAdmin.Image.Dto;

namespace Vtmis.WebAdmin.Image
{
    public interface IImageService
    {
        Task<bool> DeleteImages(RequestDeleteDto input);
        Task<IEnumerable<ResponseImageDto>> GetImages(RequestImageDto input, string baseUri);
        Task<IEnumerable<ResponseImageDto>> CreateOrUpdateAsync(string imageId, ImageType imageType, IEnumerable<ResponseImageDto> input, string baseUri);
        Task<IEnumerable<ResponseImageDto>> UploadImageAsync(Vtmis.Core.Model.DTO.Image.RequestUploadImage input, string baseUri);
        Task<IEnumerable<ResponseImageDto>> UploadImageAsync(RequestUploadImageBase64 input, string baseUri);

        Task<List<ImageUpload>> Seed();
    }
}
