﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.EICS_Equipment.DTO;

namespace Vtmis.WebAdmin.EICS_Equipment
{
    public interface IEICS_EquipmentService
    {
        Task<EICS_EquipmentDto> Create(string name);

        Task<IEnumerable<EICS_EquipmentDto>> GetAllAsync();
        Task<EICS_EquipmentDto> Update(EICS_EquipmentDto input);
        Task DeleteAsync(int id);

        Task<EICS_EquipmentDto> Get(int id);
    }
}
