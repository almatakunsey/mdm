﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.EICS_Equipment;
using Vtmis.WebAdmin.EICS_Equipment.DTO;

namespace Vtmis.WebAdmin.EICS_Equipments
{
    [RemoteService(IsMetadataEnabled = false)]
    public class EICS_EquipmentService : ApplicationService,IEICS_EquipmentService
    {
        private readonly IRepository<EICS_Equipment> _repository;

        public EICS_EquipmentService(IRepository<EICS_Equipment> repository)
        {
            _repository = repository;
        }

        public async Task<EICS_EquipmentDto> Create(string name)
        {
            var equipment = new EICS_Equipment { Name = name };
            var result =  await _repository.InsertAsync(equipment);
            await CurrentUnitOfWork.SaveChangesAsync();

            return ObjectMapper.Map<EICS_EquipmentDto>(result);
        }

        public async Task<IEnumerable<EICS_EquipmentDto>> GetAllAsync()
        {
            var equipment = await _repository.GetAll().IgnoreQueryFilters().ToListAsync();

            return ObjectMapper.Map<IEnumerable<EICS_EquipmentDto>>(equipment);
        }

        public async Task<EICS_EquipmentDto> Get(int id)
        {
            var query = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id.Equals(id));

            return ObjectMapper.Map<EICS_EquipmentDto>(query);
        }

        public async Task<EICS_EquipmentDto> Update(EICS_EquipmentDto input)
        {
            var query = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id.Equals(input.Id));

            if (query == null)
            {
                throw new UserFriendlyException("Equipment Not Found");
            }
            else
            {
                //notes : Object Mapper if want map 2 param 
                var equipment = ObjectMapper.Map(input, query);
                var result = await _repository.UpdateAsync(equipment);
                await CurrentUnitOfWork.SaveChangesAsync();

                return ObjectMapper.Map<EICS_EquipmentDto>(result);
            }
        }

        public async Task DeleteAsync(int id)
        {
            var query = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id.Equals(id));

            await _repository.DeleteAsync(query);
            await CurrentUnitOfWork.SaveChangesAsync();
            return;

        }
    }
}
