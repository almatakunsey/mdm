﻿namespace Vtmis.WebAdmin.EICS_Equipment.DTO
{
    public class EICS_EquipmentDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
