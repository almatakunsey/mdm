﻿namespace Vtmis.WebAdmin.MultiTenancy.Dto
{
    public class ResponseCheckTenantByEmail
    {
        public int? TenantId { get; set; }
        public string TenantName { get; set; }
        public string TenancyName { get; set; }
    }
}
