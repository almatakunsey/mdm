﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Model.DTO.Tenant;
using Vtmis.WebAdmin.MultiTenancy.Dto;

namespace Vtmis.WebAdmin.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
        [HttpPost]
        Task<BulkTenantPersonalizationDto> Personalization([FromBody]BulkTenantPersonalizationDto input);
        Task<BulkTenantPersonalizationDto> Personalization(string tenancyName);
        [HttpDelete]
        Task DeletePersonalization([FromBody]BulkTenantPersonalizationDto input);
        Task<Tenant> FindByTenancyNameAsync(string tenantName);
    }
}
