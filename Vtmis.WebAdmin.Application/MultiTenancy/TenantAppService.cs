using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IdentityFramework;
using Abp.MultiTenancy;
using Abp.Runtime.Security;
using Vtmis.WebAdmin.Authorization;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Editions;
using Vtmis.WebAdmin.MultiTenancy.Dto;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Roles;
using System.Collections.Generic;
using Vtmis.WebAdmin.Users;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Vtmis.Core.Model.DTO.Tenant;
using Vtmis.Core.Common.Helpers;
using Abp.UI;
using Abp.Runtime.Validation;
using Abp.Domain.Entities;

namespace Vtmis.WebAdmin.MultiTenancy
{

    //[AbpAuthorize(PermissionNames.Pages_Tenants)]
    [AbpAuthorize]
    public class TenantAppService : AsyncCrudAppService<Tenant, TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>, ITenantAppService
    {
        private readonly TenantManager _tenantManager;
        private readonly EditionManager _editionManager;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;
        private readonly IAbpZeroDbMigrator _abpZeroDbMigrator;
        private readonly IPasswordHasher<User> _passwordHasher;
        private readonly IRoleAppService _roleAppService;
        private readonly IUserAppService _userAppService;
        private readonly IRepository<TenantPersonalization, long> _tenantPersonalizationRepo;
        private readonly IRepository<Tenant, int> _repository;
        private readonly IRepository<User, long> _userRepo;

        public TenantAppService(
            IRepository<Tenant, int> repository,
            IRepository<User, long> userRepo,
            TenantManager tenantManager,
            EditionManager editionManager,
            UserManager userManager,
            RoleManager roleManager,
            IAbpZeroDbMigrator abpZeroDbMigrator,
            IPasswordHasher<User> passwordHasher,
            IRoleAppService roleAppService,
            IUserAppService userAppService,
            IRepository<TenantPersonalization, long> tenantPersonalizationRepo)
            : base(repository)
        {
            _tenantManager = tenantManager;
            _editionManager = editionManager;
            _userManager = userManager;
            _roleManager = roleManager;
            _abpZeroDbMigrator = abpZeroDbMigrator;
            _passwordHasher = passwordHasher;
            _roleAppService = roleAppService;
            _userAppService = userAppService;
            _tenantPersonalizationRepo = tenantPersonalizationRepo;
            _repository = repository;
            _userRepo = userRepo;
        }

        [AbpAllowAnonymous]
        [HttpGet]
        [Route("/api/services/app/Tenant/{email}")]
        public async Task<ResponseCheckTenantByEmail> FindTenantByEmail(string email)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var user = await _userRepo.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.EmailAddress.IsEqual(email) && x.IsDeleted == false);
                if (user == null)
                {
                    throw new EntityNotFoundException($"Email {email} does not exist");
                }

                if (user.TenantId == null)
                {
                    return new ResponseCheckTenantByEmail
                    {
                        TenancyName = "Admin",
                        TenantName = "Admin"
                    };
                }
                var tenant = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == user.TenantId && x.IsDeleted == false);
                if (tenant == null)
                {
                    throw new EntityNotFoundException($"Tenant does not exist");
                }
                return new ResponseCheckTenantByEmail
                {
                    TenantId = tenant.Id,
                    TenancyName = tenant.TenancyName,
                    TenantName = tenant.Name
                };
            }
        }

        [AbpAllowAnonymous]
        [HttpGet]
        public async Task<Tenant> FindByTenancyNameAsync(string tenantName)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var result = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenancyName.IsEqual(tenantName));
                return result;
            }
        }

        [AbpAllowAnonymous]
        [HttpGet]
        public async Task<BulkTenantPersonalizationDto> Personalization(string tenancyName)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    if (tenancyName.IsEqual("admin"))
                    {
                        tenancyName = null;
                    }
                    var tenant = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenancyName.IsEqual(tenancyName));
                    //if (tenant == null)
                    //{
                    //    //check if host admin
                    //    var isHostAdmin = await _tenantPersonalizationRepo.GetAll().IgnoreQueryFilters().AnyAsync(x => x.TenantId == null);
                    //    if (isHostAdmin == false)
                    //    {
                    //        throw new UserFriendlyException("Tenant Does Not Exist.");
                    //    }

                    //    if (!string.IsNullOrWhiteSpace(tenancyName))
                    //    {
                    //        throw new UserFriendlyException("Tenant Does Not Exist.");
                    //    }
                    //}

                    var tenantId = tenant?.Id;
                    var tenantName = tenant?.TenancyName;
                    var items = new List<ResponseBulkTenantPersonalizationDto>();
                    if (tenant == null && !string.IsNullOrEmpty(tenancyName))
                    {

                    }
                    else
                    {
                        items = await _tenantPersonalizationRepo.GetAll().IgnoreQueryFilters().Where(x => x.TenantId == tenantId).Select(x => new ResponseBulkTenantPersonalizationDto
                        {
                            Key = x.Key,
                            Value = x.Value

                        }).ToListAsync();
                    }

                    return new BulkTenantPersonalizationDto
                    {
                        TenancyName = tenantName == null && string.IsNullOrEmpty(tenancyName) ? "admin" : tenantName,
                        TenantPersonalizationDto = items
                    };
                }
                catch (System.Exception err)
                {

                    throw;
                }

            }
        }

        //[HttpPost]
        //public async Task<TenantPersonalizationDto> Personalization([FromBody]TenantPersonalizationDto items)
        //{
        //    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
        //    {
        //        var tenant = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenancyName.IsEqual(items.TenancyName));
        //        if (tenant == null)
        //        {
        //            throw new NotFoundException("Tenant Does Not Exist.");
        //        }

        //        var tenantPersona = await _tenantPersonalizationRepo.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenantId == tenant.Id);

        //        if (tenantPersona == null)
        //        {
        //            tenantPersona = new TenantPersonalization();
        //        }

        //        tenantPersona.Key = items.Key;
        //        tenantPersona.Value = items.Value;
        //        tenantPersona.TenantId = tenant.Id;
        //        var result = await _tenantPersonalizationRepo.InsertOrUpdateAsync(tenantPersona);
        //        await CurrentUnitOfWork.SaveChangesAsync();
        //        return new TenantPersonalizationDto {
        //            Key = result.Key,
        //            Value = result.Value,
        //            TenancyName = tenant.TenancyName
        //        };
        //    }
        //}

        [HttpPost]
        public async Task<BulkTenantPersonalizationDto> Personalization([FromBody]BulkTenantPersonalizationDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    if (input.TenancyName.IsEqual("admin"))
                    {
                        input.TenancyName = null;
                    }
                    if (input.TenantPersonalizationDto.Select(x => x.Key).Count() != input.TenantPersonalizationDto.Select(x => x.Key).Distinct().Count())
                    {
                        throw new UserFriendlyException($"Duplicate keys in the request");
                    }
                    var tenant = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenancyName.IsEqual(input.TenancyName));

                    if (tenant == null)
                    {
                        //check if host admin
                        //var isHostAdmin = await _tenantPersonalizationRepo.GetAll().IgnoreQueryFilters().AnyAsync(x => x.TenantId == null);
                        //if (isHostAdmin == false)
                        //{
                        //    throw new UserFriendlyException("Tenant Does Not Exist.");
                        //}

                        if (!string.IsNullOrWhiteSpace(input.TenancyName))
                        {
                            throw new UserFriendlyException("Tenant Does Not Exist.");
                        }
                    }
                    //if (tenant == null)
                    //{
                    //    //check if host admin
                    //    var isHostAdmin = await _tenantPersonalizationRepo.GetAll().IgnoreQueryFilters().AnyAsync(x => x.TenantId == null);
                    //    if (isHostAdmin == false)
                    //    {
                    //        throw new NotFoundException("Tenant Does Not Exist.");
                    //    }
                    //}

                    var results = new BulkTenantPersonalizationDto();
                    results.TenancyName = tenant?.TenancyName;
                    results.TenancyName = results.TenancyName == null && string.IsNullOrEmpty(results.TenancyName) ? "admin" : results.TenancyName;
                    var tenantId = tenant?.Id;
                    foreach (var item in input.TenantPersonalizationDto)
                    {
                        var personalization = await _tenantPersonalizationRepo.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenantId == tenantId &&
                                    x.Key.IsEqual(item.Key));

                        personalization = ObjectMapper.Map(item, personalization);
                        personalization.TenantId = tenant?.Id;
                        await _tenantPersonalizationRepo.InsertOrUpdateAsync(personalization);
                        await CurrentUnitOfWork.SaveChangesAsync();

                        results.TenantPersonalizationDto.Add(new ResponseBulkTenantPersonalizationDto
                        {
                            Key = personalization.Key,
                            Value = personalization.Value
                        });
                    }

                    return results;
                }
                catch (System.Exception err)
                {

                    throw;
                }

            }
        }

        [HttpDelete]
        public async Task DeletePersonalization([FromBody]BulkTenantPersonalizationDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                if (input.TenancyName.IsEqual("admin"))
                {
                    input.TenancyName = null;
                }
                if (input.TenantPersonalizationDto.Select(x => x.Key).Count() != input.TenantPersonalizationDto.Select(x => x.Key).Distinct().Count())
                {
                    throw new UserFriendlyException($"Duplicate keys in the request");
                }
                var tenant = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenancyName.IsEqual(input.TenancyName));
                if (tenant == null)
                {
                    //check if host admin
                    var isHostAdmin = await _tenantPersonalizationRepo.GetAll().IgnoreQueryFilters().AnyAsync(x => x.TenantId == null);
                    if (isHostAdmin == false)
                    {
                        throw new UserFriendlyException("Tenant Does Not Exist.");
                    }

                    if (!string.IsNullOrWhiteSpace(input.TenancyName))
                    {
                        throw new UserFriendlyException("Tenant Does Not Exist.");
                    }
                }
                var tenantId = tenant?.Id;
                await _tenantPersonalizationRepo.DeleteAsync(x => input.TenantPersonalizationDto.Any(a => a.Key.IsEqual(x.Key)) && x.TenantId == tenantId);
                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }

        public override Task<TenantDto> Update(TenantDto input)
        {
            if (IsGranted(MdmPermissionsConst.Tenant_Update_Mdm) == false)
            {
                throw new ForbiddenException("Permission Denied");
            }
            return base.Update(input);
        }


        public override async Task<PagedResultDto<TenantDto>> GetAll(PagedResultRequestDto input)
        {
            if (IsGranted(MdmPermissionsConst.Tenant_View_Mdm) == false)
            {
                //throw new ForbiddenException("Permission Denied");
                return new PagedResultDto<TenantDto>();
            }
            return await base.GetAll(input);
        }

        public override async Task<TenantDto> Create(CreateTenantDto input)
        {
            CheckCreatePermission();

            // Create tenant
            var tenant = ObjectMapper.Map<Tenant>(input);
            tenant.ConnectionString = input.ConnectionString.IsNullOrEmpty()
                ? null
                : SimpleStringCipher.Instance.Encrypt(input.ConnectionString);

            var defaultEdition = await _editionManager.FindByNameAsync(EditionManager.DefaultEditionName);
            if (defaultEdition != null)
            {
                tenant.EditionId = defaultEdition.Id;
            }
            //var isHighPermission = false;
            if ((await IsGrantedAsync(MdmPermissionsConst.Tenant_Create_Mdm)) == false)
            {
                throw new ForbiddenException("Permission Denied");
            }
            await _tenantManager.CreateAsync(tenant);
            await CurrentUnitOfWork.SaveChangesAsync(); // To get new tenant's id.

            // Create tenant database
            _abpZeroDbMigrator.CreateOrMigrateForTenant(tenant);

            // We are working entities of new tenant, so changing tenant filter
            using (CurrentUnitOfWork.SetTenantId(tenant.Id))
            {
                // Create static roles for new tenant
                //CheckErrors(await _roleManager.CreateStaticRoles(tenant.Id));
                var role = new Role();
                try
                {
                    role = await _roleAppService.CreateStatic(tenant.Id);
                }
                catch (System.Exception err)
                {
                    var a = err;
                }

                await CurrentUnitOfWork.SaveChangesAsync(); // To get static role ids

                // Grant all permissions to admin role
                var adminRole = _roleManager.Roles.Single(r => r.Name == StaticRoleNames.Tenants.Admin && r.TenantId == tenant.Id);
                //await _roleManager.GrantAllPermissionsAsync(adminRole);
                var host = adminRole.IsDefault ? Abp.MultiTenancy.MultiTenancySides.Host : Abp.MultiTenancy.MultiTenancySides.Tenant;

                var grantedPermissions = PermissionManager
                    .GetAllPermissions(multiTenancySides: MultiTenancySides.Tenant)
                    .Select(x => x.Name);

                var permissions = PermissionFinder
                   .GetAllPermissions(new TenantAdminAuthorizationProvider())
                   .ToList();
                //var customPermissions = new List<Permission>();

                //foreach (var permissionName in MdmPermissionsConst.DefaultAdmin())
                //{
                //    customPermissions.Add(new Abp.Authorization.Permission(permissionName, null, null, host, null));
                //}
                //customPermissions.Add(new Abp.Authorization.Permission(PermissionNames.Pages_Roles, null, null, host, null));
                //customPermissions.Add(new Abp.Authorization.Permission(PermissionNames.Pages_Users, null, null, host, null));

                await _roleManager.SetGrantedPermissionsAsync(adminRole, permissions);
                // Create admin user for the tenant
                var adminUser = User.CreateTenantAdminUser(tenant.Id, input.AdminEmailAddress);
                adminUser.Password = _passwordHasher.HashPassword(adminUser, User.DefaultPassword);

                await _userAppService.InternalCreate(adminUser);
                //CheckErrors(await _userManager.CreateAsync(adminUser));
                await CurrentUnitOfWork.SaveChangesAsync(); // To get admin user's id

                // Assign admin user to role!
                var roles = new List<string>
                {
                    adminRole.Name
                };
                await _userManager.SetRoleToUser(adminUser.Id, adminRole.Id, tenant.Id);
                //CheckErrors(await _userManager.AddToRoleAsync(adminUser, adminRole.Name));
                await CurrentUnitOfWork.SaveChangesAsync();
            }

            return MapToEntityDto(tenant);
        }

        protected override void MapToEntity(TenantDto updateInput, Tenant entity)
        {
            // Manually mapped since TenantDto contains non-editable properties too.
            entity.Name = updateInput.Name;
            entity.TenancyName = updateInput.TenancyName;
            entity.IsActive = updateInput.IsActive;
        }

        public override async Task Delete(EntityDto<int> input)
        {
            CheckDeletePermission();
            if ((await IsGrantedAsync(MdmPermissionsConst.Tenant_Delete_Mdm)) == false)
            {
                throw new ForbiddenException("Permission Denied");
            }
            var tenant = await _tenantManager.GetByIdAsync(input.Id);
            await _tenantManager.DeleteAsync(tenant);
        }

        private void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
