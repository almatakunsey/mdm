﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Request.External;
using Vtmis.WebAdmin.MEH.DTO;

namespace Vtmis.WebAdmin.MEH
{
    [RemoteService(IsMetadataEnabled = false)]
    public class MEHIncidentService : ApplicationService, IMEHIncidentService
    {
        private readonly IRepository<MEHIncidentReport> _repository;
        private readonly IRepository<MEHIncidentLocation> _locationRepo;
        private readonly IRepository<MEHIncidentCoordinate> _coordRepo;
        private readonly MEHRequest _request;

        public MEHIncidentService(IRepository<MEHIncidentReport> repository, IRepository<MEHIncidentLocation> locationRepo,
            IRepository<MEHIncidentCoordinate> coordRepo)
        {
            _repository = repository;
            _locationRepo = locationRepo;
            _coordRepo = coordRepo;
            _request = new MEHRequest();
        }

        public async Task PullIncidentDataFromMEH(DateTime startDate)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                // check if meh incident table have data
                var isDataAvailable = await _repository.GetAll().IgnoreQueryFilters().AnyAsync();
                var source = await _request.GetIncidentReportAsync(startDate, isDataAvailable);
                if (source == null)
                {
                    return;
                }
                if (source.Items.Count <= 0)
                {
                    return;
                }

                foreach (var item in source.Items)
                {
                    var qry = await _repository.GetAll().IgnoreQueryFilters().Include(x => x.Locations)
                        .ThenInclude(y => y.Coordinates).FirstOrDefaultAsync(x => x.IncidentId == item.EntryId);
                    if (qry == null)
                    {
                        // Insert
                        var entity = ObjectMapper.Map<MEHIncidentReport>(item);
                        entity.DateInsertedByMDM = DateTime.Now;
                        await _repository.InsertAsync(entity);
                    }
                    else
                    {
                        // Update 
                        if (qry.LastModifiedDateByMDM == null || (qry.LastModifiedDateFromMEH >= startDate.StartOfDay()))
                        {
                            if (qry.Locations.Any())
                            {
                                foreach (var location in qry.Locations)
                                {
                                    if (location.Coordinates.Any())
                                    {
                                        foreach (var coordinate in location.Coordinates)
                                        {
                                            await _coordRepo.DeleteAsync(coordinate.Id);
                                        }
                                    }
                                    await _locationRepo.DeleteAsync(location.Id);
                                }
                                await CurrentUnitOfWork.SaveChangesAsync();
                                qry = await _repository.GetAll().IgnoreQueryFilters().Include(x => x.Locations)
                                    .ThenInclude(y => y.Coordinates).FirstOrDefaultAsync(x => x.IncidentId == item.EntryId);
                            }

                            var entity = ObjectMapper.Map(item, qry);
                            entity.LastModifiedDateByMDM = DateTime.Now;
                            await _repository.UpdateAsync(entity);
                        }
                    }

                }

                await CurrentUnitOfWork.SaveChangesAsync();
            }
        }

        public async Task<bool> CreateAsync(RequestCreateMEHIncident input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var entity = ObjectMapper.Map<MEHIncidentReport>(input);
                    entity.DateInsertedByMDM = DateTime.Now;
                    await _repository.InsertAsync(entity);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    return true;
                }
                catch (Exception err)
                {
                    throw new Exception(err.Message);
                }
            }
        }

        public async Task<List<ResponseMEHIncidentDto>> GetAllAsync()
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var entity = await _repository.GetAll().IgnoreQueryFilters().Include(x => x.Locations).ToListAsync();
                    return ObjectMapper.Map<List<ResponseMEHIncidentDto>>(entity);
                }
                catch (Exception err)
                {
                    throw new Exception(err.Message);
                }
            }
        }
    }
}
