﻿namespace Vtmis.WebAdmin.MEH.DTO
{
    public class MEHIncidentLocationDto
    {
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
}