﻿using System;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.MEH.DTO
{
    public class RequestCreateMEHIncident
    {
        public RequestCreateMEHIncident()
        {
            Locations = new List<MEHIncidentLocationDto>();
        }

        public string MetaStatus { get; set; }
        public string MetaType { get; set; }
        public DateTime? Date { get; set; }
        public string TypeOfCasualty { get; set; }
        public List<MEHIncidentLocationDto> Locations { get; set; }
        public string LossOfLife { get; set; }
        public string MarinePollution { get; set; }
        public string Visibility { get; set; }
        public string SeaState { get; set; }
        public string ForceWind { get; set; }
        public string VesselNameA { get; set; }
        public string CallSignA { get; set; }
        public int? MmsiA { get; set; }
        public string TypeA { get; set; }
        public string FlagA { get; set; }
        public string DestinationA { get; set; }
        public string CargoA { get; set; }
        public string VesselNameB { get; set; }
        public string CallSignB { get; set; }
        public int? MmsiB { get; set; }
        public string TypeB { get; set; }
        public string FlagB { get; set; }
        public string DestinationB { get; set; }
        public string CargoB { get; set; }
        public string AccidentDescription { get; set; }
        public string OperatorInCharge { get; set; }
    }
}