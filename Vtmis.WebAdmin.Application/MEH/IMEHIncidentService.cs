﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.MEH.DTO;

namespace Vtmis.WebAdmin.MEH
{
    public interface IMEHIncidentService
    {
        Task PullIncidentDataFromMEH(DateTime startDate);
        Task<bool> CreateAsync(RequestCreateMEHIncident input);
        Task<List<ResponseMEHIncidentDto>> GetAllAsync();
    }
}