﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.MicroKernel.Registration;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.DTO.Tenant;
using Vtmis.Core.Model.ViewModels.Lloyds;
using Vtmis.Core.Model.ViewModels.Route.Dto;
using Vtmis.Core.Request.Model.MEH;
using Vtmis.WebAdmin.Alarms;
using Vtmis.WebAdmin.Alarms.Dto;
using Vtmis.WebAdmin.Authorization;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Cable.Dto;
using Vtmis.WebAdmin.Colors;
using Vtmis.WebAdmin.EICS;
using Vtmis.WebAdmin.EICS.DTO;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Filters.Dto;
using Vtmis.WebAdmin.Filters.v2.Dto;
using Vtmis.WebAdmin.Lloyds;
using Vtmis.WebAdmin.Lloyds.Dto;
using Vtmis.WebAdmin.Locations;
using Vtmis.WebAdmin.Locations.Dto;
using Vtmis.WebAdmin.MEH;
using Vtmis.WebAdmin.MEH.DTO;
using Vtmis.WebAdmin.MultiTenancy;
using Vtmis.WebAdmin.Personalizations;
using Vtmis.WebAdmin.Personalizations.Dto;
using Vtmis.WebAdmin.ReadOnlyDB;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories;
using Vtmis.WebAdmin.Reports;
using Vtmis.WebAdmin.Reports.Dto;
using Vtmis.WebAdmin.ReportTypes.Dto;
using Vtmis.WebAdmin.Routes;
using Vtmis.WebAdmin.TenantFilters;
using Vtmis.WebAdmin.TenantFilters.Dto;
using Vtmis.WebAdmin.Users.Dto;
using Vtmis.WebAdmin.Vessels;
using Vtmis.WebAdmin.Vessels.Dto;
using Vtmis.WebAdmin.Zones;
using Vtmis.WebAdmin.Zones.Dto;

namespace Vtmis.WebAdmin
{
    [DependsOn(
        typeof(WebAdminCoreModule),
        typeof(AbpAutoMapperModule))]
    public class WebAdminApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {

            Configuration.Authorization.Providers.Add<WebAdminAuthorizationProvider>();
            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                cfg.CreateMap<MEHIncidentSource, MEHIncidentReport>()                  
                    .ForMember(dest => dest.Locations, opt => opt.MapFrom(s => MapLocation(s.Location)))
                    .ForMember(dest => dest.DateInsertedFromMEH, opt => opt.MapFrom(s => s.EntryDate))
                    .ForMember(dest => dest.LastModifiedDateFromMEH, opt => opt.MapFrom(s => s.EntryModified))
                    .ForMember(dest => dest.IncidentId, opt => opt.MapFrom(s => s.EntryId))
                    ;
                //cfg.CreateMap<MEHIncidentLocationSource, MEHIncidentLocation>()
                //     .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.Lat))
                //     .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.Lng))
                //;

                cfg.CreateMap<RequestCreateTenantFilter, TenantFilter>();
                cfg.CreateMap<RequestTenantFilterItem, TenantFilterItem>();
                cfg.CreateMap<TenantFilter, ResponseTenantFilter>();
                cfg.CreateMap<TenantFilterItem, ResponseTenantFilterItem>();
                cfg.CreateMap<RequestCreateMEHIncident, MEHIncidentReport>();
                cfg.CreateMap<MEHIncidentLocationDto, MEHIncidentLocation>().ReverseMap();
                cfg.CreateMap<MEHIncidentReport, ResponseMEHIncidentDto>();

                cfg.CreateMap<User, UserDto>().ForMember(f => f.Roles, ig => ig.Ignore()).ForMember(f => f.RoleIds, ig => ig.Ignore());
                cfg.CreateMap<ResponseBulkTenantPersonalizationDto, TenantPersonalization>().ForMember(f => f.Id, ig => ig.Ignore());
                cfg.CreateMap<RequestCreatePersonalizationDto, Personalization>().ForMember(f => f.Id, ig => ig.Ignore());
                cfg.CreateMap<RequestBulkItemPersonalization, Personalization>().ForMember(f => f.Id, ig => ig.Ignore());
                cfg.CreateMap<Personalization, ResponsePersonalizationDto>();

                cfg.CreateMap<AlarmNotification, ResponseAlarmNotificationDto>();
                cfg.CreateMap<RequestUpdateCableDto, Cable.Cable>()
                        .ForMember(f =>
                            f.CreationTime, ig => ig.Ignore())
                        .ForMember(f =>
                            f.CreatorUserId, ig => ig.Ignore())
                        .ForMember(f =>
                        f.DeleterUserId, ig => ig.Ignore())
                         .ForMember(f =>
                        f.DeletionTime, ig => ig.Ignore())
                          .ForMember(f =>
                        f.IsDeleted, ig => ig.Ignore())
                          .ForMember(f =>
                        f.LastModificationTime, ig => ig.Ignore())
                          .ForMember(f =>
                        f.LastModifierUserId, ig => ig.Ignore())
                          .ForMember(f =>
                        f.TenantId, ig => ig.Ignore())
                          .ForMember(f =>
                        f.UserId, ig => ig.Ignore());

                //    f.IsDeleted,
                //    f.LastModificationTime,
                //    f.LastModifierUserId,
                //    f.TenantId,
                //    f.UserId
                //}, ig => ig.Ignore());

                cfg.CreateMap<RequestUpdateCableDetailDto, Cable.CableDetail>()
                    .ForMember(f => f.Id, ig => ig.Ignore());

                cfg.CreateMap<RequestCreateCableDto, Cable.Cable>();
                cfg.CreateMap<RequestCreateCableDetailDto, Cable.CableDetail>();

                cfg.CreateMap<Cable.Cable,
                  ResponseCableDto>().ReverseMap();

                cfg.CreateMap<Cable.CableDetail,
                 ResponseCableDetailDto>().ReverseMap();

                cfg.CreateMap<RequestCreateVesselGroup, VesselGroup>();
                cfg.CreateMap<RequestVesselDetail, VesselGroupDetail>();
                cfg.CreateMap<VesselGroup, CreateVesselGroupDto>().ReverseMap();
                cfg.CreateMap<VesselGroupDetail, CreateVesselGroupDetailDto>().ReverseMap();

                cfg.CreateMap<RequestCreateEICSInfoDto, EICS_info>();
                cfg.CreateMap<RequestCreateEICSDetailDto, EICS_details>();
                cfg.CreateMap<RequestUpdateInfoEICS, EICS_info>();
                cfg.CreateMap<RequestUpdateDetailsEICS, EICS_details>();
                cfg.CreateMap<EICS_info, ResponseEICSInfoDto>().ReverseMap();
                cfg.CreateMap<EICS_details, ResponseEICSDetailDto>().ReverseMap();

                cfg.CreateMap<Lloyd,
                   ResponseLloyds>().ReverseMap();

                cfg.CreateMap<Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto.CreateExtendedShipList,
                    Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.ExtendedShipList>().ReverseMap();

                cfg.CreateMap<Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto.UpdateExtendedShipList,
                   Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.ExtendedShipList>()
                     .ForMember(x => x.Id, op => op.Ignore());

                cfg.CreateMap<Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.ExtendedShipList,
                   Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto.ResponseExtendedShipList>();

                cfg.CreateMap<RequestCreateRoute,
                   Route>().ReverseMap();

                cfg.CreateMap<ResponseRouteDto,
                  RequestUpdateRouteDto>();

                cfg.CreateMap<RequestUpdateRouteDto,
                   Route>()
                     .ForMember(x => x.Id, op => op.Ignore());

                cfg.CreateMap<Route,
                   ResponseRouteDto>()
                    .ForMember(x => x.Waypoints, op => op.AddTransform(t => CalculateWaypointDistance(t)));

                cfg.CreateMap<RequestWaypointDto,
                  Waypoint>().ForMember(x => x.Id, op => op.Ignore()).ReverseMap();

                cfg.CreateMap<RequestUpdateWaypointDto,
                 Waypoint>().ForMember(x => x.Id, op => op.Ignore()).ReverseMap();

                cfg.CreateMap<Waypoint,
                  WaypointDto>();
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg.CreateMap<FilterDto, Filter>()
                    .ForMember(u => u.CreationTime, op => op.Ignore());

                cfg.CreateMap<CreateFilterDto, Filter>();
                cfg.CreateMap<CreateFilterItemDto, FilterItem>();
                cfg.CreateMap<CreateFilterDetailDto, FilterDetail>();

                cfg.CreateMap<Filter, UpdateFilterDto>();
                cfg.CreateMap<FilterItem, UpdateFilterItemDto>();
                cfg.CreateMap<FilterDetail, UpdateFilterDetailDto>();

                cfg.CreateMap<UpdateFilterDto, Filter>()
                    .ForMember(u => u.Id, op => op.Ignore())
                    .ForMember(u => u.CreationTime, op => op.Ignore());
                cfg.CreateMap<UpdateFilterItemDto, FilterItem>()
                    .ForMember(u => u.Id, op => op.Ignore());
                cfg.CreateMap<UpdateFilterDetailDto, FilterDetail>()
                    .ForMember(u => u.Id, op => op.Ignore());

                cfg.CreateMap<Filter, FilterResponseDto>();
                cfg.CreateMap<FilterItem, FilterItemResponseDto>();
                cfg.CreateMap<FilterDetail, FilterDetailResponseDto>();

                cfg.CreateMap<FilterGroupRequestNew, FilterGroup>()
                   .ForMember(u => u.CreationTime, op => op.Ignore());

                cfg.CreateMap<CreateAISColourSchemaDto, AISColourSchema>()
                   .ForMember(u => u.CreationTime, op => op.Ignore());

                cfg.CreateMap<AISColourSchemaItemDto, AISColourSchemaItem>()
                   .ForMember(u => u.AISColourSchema, op => op.Ignore())
                   .ForMember(u => u.AISColourSchemaId, op => op.Ignore());

                cfg.CreateMap<Zone, ZoneListDto>();
                cfg.CreateMap<Zone, ListZoneDto>();
                cfg.CreateMap<Zone, ZoneDto>();
                cfg.CreateMap<ZoneDto, Zone>()
                    .ForMember(u => u.Id, op => op.Ignore())
                    .ForMember(u => u.CreationTime, op => op.Ignore());

                cfg.CreateMap<ZoneItemDto, ZoneItem>()
                   .ForMember(u => u.CreationTime, op => op.Ignore())
                   .ForMember(u => u.Id, op => op.Ignore()).ReverseMap();

                cfg.CreateMap<ResponseZoneItemDto, ZoneItem>()
                  .ForMember(u => u.CreationTime, op => op.Ignore())
                  .ForMember(u => u.Id, op => op.Ignore()).ReverseMap();
                //.ForMember(u => u.Zone, op => op.Ignore());

                cfg.CreateMap<UpdateZoneDto, Zone>()
                   .ForMember(u => u.CreationTime, op => op.Ignore())
                   .ForMember(u => u.Id, op => op.Ignore())
                   .ForMember(u => u.TenantId, op => op.Ignore())
                   .ForMember(u => u.User, op => op.Ignore())
                   .ForMember(u => u.Id, op => op.Ignore());

                cfg.CreateMap<ReportDto, Report>()
                    //.ForMember(u => u.ReportType, op => op.Ignore())
                    //.ForMember(u => u.ReportTypeId, op => op.Ignore())
                    .ForMember(u => u.ReportItems, op => op.Ignore())
                    .ForMember(u => u.CreationTime, op => op.Ignore());

                cfg.CreateMap<Report, ReportDto>()
                    //.ForMember(u => u.ReportType, op => op.Ignore())
                    //.ForMember(u => u.ReportTypeId, op => op.Ignore())
                    .ForMember(u => u.ReportItemDtos, op => op.MapFrom(x => x.ReportItems))
                    .ForMember(u => u.CreationTime, op => op.Ignore());

                cfg.CreateMap<ReportItemDto, ReportItem>()
                   //.ForMember(u => u.ReportType, op => op.Ignore())
                   //.ForMember(u => u.ReportTypeId, op => op.Ignore())
                   .ForMember(u => u.Zone, op => op.Ignore())
                   .ForMember(u => u.Filter, op => op.Ignore())
                   .ForMember(u => u.Report, op => op.Ignore());

                cfg.CreateMap<ReportItem, ReportItemDto>();
                cfg.CreateMap<Report, CreateReportDto>().ReverseMap();
                cfg.CreateMap<ReportItem, CreateReportItemDto>().ReverseMap();
                cfg.CreateMap<Reports.SpeedViolation, CreateReportItemDto>().ReverseMap();
                cfg.CreateMap<Reports.Dto.SpeedViolationResponseDto, ReportItemResponseDto>().ReverseMap();

                cfg.CreateMap<Report, UpdateReportDto>().ReverseMap()
                        .ForMember(x => x.Id, op => op.Ignore()); ;
                cfg.CreateMap<ReportItem, UpdateReportItemDto>().ReverseMap()
                        .ForMember(x => x.Id, op => op.Ignore()); ;

                cfg.CreateMap<Report, ReportResponseDto>();
                cfg.CreateMap<ReportItem, ReportItemResponseDto>();

                cfg.CreateMap<Report, ReportReponseDetailDto>();
                cfg.CreateMap<ReportItem, ReportItemResponseDetailDto>();

                cfg.CreateMap<ReportType, ReportTypeResponseDto>();

                cfg.CreateMap<ReportTypeDto, ReportType>()
                   .ForMember(u => u.CreationTime, op => op.Ignore());

                cfg.CreateMap<UpdateLocationDto, Location>()
                   .ForMember(u => u.Id, op => op.Ignore());

                cfg.CreateMap<Location, LocationDto>().ReverseMap();
                cfg.CreateMap<CreateLocationDto, Location>();

                cfg.CreateMap<Alarm, AlarmDto>().ReverseMap();
                cfg.CreateMap<Alarm, CreateAlarmDto>().ReverseMap();
                cfg.CreateMap<AlarmItem, CreateAlarmItemDto>().ReverseMap();
                cfg.CreateMap<AlarmItem, AlarmItemDto>().ReverseMap();
                //.ForSourceMember(u => u.Id, op => op.());
                //.ForMember(x => x.Alarm, xp => xp.Ignore());
                //.ForMember(x => x.id, xp => xp.Ignore());
                cfg.CreateMap<AlarmCondition, AlarmConditionDto>();


                cfg.CreateMap<VesselGroup, CreateVesselGroupDto>();
                cfg.CreateMap<Lloyd, LloydsDetailDto>().ReverseMap();
            });
        }

        private List<MEHIncidentLocation> MapLocation(List<List<MEHIncidentLocationSource>> locations)
        {
            var result = new List<MEHIncidentLocation>();
            if (locations.Any())
            {               
                foreach (var coordinates in locations)
                {
                    var locationResult = new MEHIncidentLocation();
                    if (coordinates.Any())
                    {
                        var coordinatesResult = new List<MEHIncidentCoordinate>();
                        foreach (var coordinate in coordinates)
                        {
                            coordinatesResult.Add(new MEHIncidentCoordinate { 
                                Latitude = coordinate.Lat,
                                Longitude = coordinate.Lng
                            });
                        }

                        if (coordinatesResult.Any())
                        {
                            locationResult.Coordinates = coordinatesResult.Select(x => new MEHIncidentCoordinate { 
                                Latitude = x.Latitude,
                                Longitude = x.Longitude
                            }).ToList();
                        }
                    }

                    if (locationResult.Coordinates.Any())
                    {
                        result.Add(locationResult);
                    }
                }                
            }
            return result;
        }

        private List<WaypointDto> CalculateWaypointDistance(List<WaypointDto> waypoints)
        {
            if (waypoints.Any())
            {
                waypoints = waypoints.OrderBy(x => x.Order).ToList();
                //var distanceList = new List<WaypointDto>();
                double preserveDistance = 0;
                int? preserveIndex = null;
                for (int i = 0; i < waypoints.Count; i++)
                {
                    var nextPoint = new PointF();
                    var isNextPointVisible = true;
                    if (i >= (waypoints.Count - 1))
                    {
                        nextPoint.X = (float)waypoints[i].Longitude;
                        nextPoint.Y = (float)waypoints[i].Latitude;
                    }
                    else
                    {
                        nextPoint.X = (float)waypoints[i + 1].Longitude;
                        nextPoint.Y = (float)waypoints[i + 1].Latitude;
                        isNextPointVisible = waypoints[i + 1].IsShown;
                    }
                    var distance = CommonHelper.GetDistanceBetweenTwoCoordinates(
                        new PointF
                        {
                            X = (float)waypoints[i].Longitude,
                            Y = (float)waypoints[i].Latitude
                        },
                       nextPoint,
                        Unit.NauticalMiles
                    );

                    waypoints[i].ActualWpDistance = distance;
                    preserveDistance = preserveDistance + distance;

                    if (waypoints[i].IsShown)
                    {
                        preserveIndex = i;
                    }

                    if (isNextPointVisible == true)
                    {
                        //var d = ObjectMapper.Map<WaypointDto>(waypoints[i]);
                        int? n = null;
                        if (preserveIndex.HasValue)
                        {
                            n = preserveIndex;

                        }
                        else
                        {
                            n = i;
                        }
                        waypoints[n.Value].WpDistance = preserveDistance;
                        preserveIndex = null;
                        //distanceList.Add(d);
                        preserveDistance = 0;
                    }
                }
                return waypoints;
            }
            return new List<WaypointDto>();
        }


        public override void Initialize()
        {
            var thisAssembly = typeof(WebAdminApplicationModule).GetAssembly();

            IocManager.Register<MdmAdminDbContext>(
                   Abp.Dependency.DependencyLifeStyle.Singleton);
            IocManager.Register<IMdmUserRoleRepository, MdmUserRoleRepository>(
                    Abp.Dependency.DependencyLifeStyle.Transient);
            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(cfg =>
            {
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                //cfg.CreateMap<PersonalizeFilterDto, PersonalizeFilter>()
                //    .ForMember(u => u.User, op => op.Ignore());

                cfg.AddProfiles(thisAssembly);
            });
        }
    }
}
