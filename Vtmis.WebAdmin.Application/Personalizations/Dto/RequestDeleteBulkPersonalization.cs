﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vtmis.WebAdmin.Personalizations.Dto
{
    public class RequestDeleteBulkPersonalization
    {
        public List<string> Keys { get; set; }
        public int? TenantId { get; set; }
        public string UserLevel { get; set; }
    }
}
