﻿using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Personalizations.Dto
{
    public class RequestCreatePersonalizationDto
    {
        [StringLength(30)]
        public string Key { get; set; }
        public string Value { get; set; }
        public int? TenantId { get; set; }
        public string UserLevel { get; set; }
    }
}
