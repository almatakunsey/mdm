﻿using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Personalizations.Dto
{
    public class RequestSpecificPersonaKey
    {      
        public string UserLevel { get; set; }       
        public string Key { get; set; }
        public long? UserId { get; set; }
        public int? TenantId { get; set; }
        public string TenantName { get; set; }
    }
}
