﻿using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Personalizations.Dto
{
    public class RequestBulkItemPersonalization
    {
        [StringLength(30)]
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
