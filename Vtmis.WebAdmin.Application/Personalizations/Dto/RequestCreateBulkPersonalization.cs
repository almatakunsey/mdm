﻿using System.Collections.Generic;

namespace Vtmis.WebAdmin.Personalizations.Dto
{
    public class RequestCreateBulkPersonalization
    {
        public List<RequestBulkItemPersonalization> Items { get; set; }
        public int? TenantId { get; set; }
        public string UserLevel { get; set; }
    }
}
