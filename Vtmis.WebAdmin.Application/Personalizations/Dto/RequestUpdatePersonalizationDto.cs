﻿namespace Vtmis.WebAdmin.Personalizations.Dto
{
    public class RequestUpdatePersonalizationDto
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string UserLevel { get; set; }
    }
}
