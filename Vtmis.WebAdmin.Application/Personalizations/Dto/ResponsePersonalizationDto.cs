﻿using Abp.Application.Services.Dto;

namespace Vtmis.WebAdmin.Personalizations.Dto
{
    public class ResponsePersonalizationDto : EntityDto
    {
        public int? TenantId { get; set; }
        public string UserLevel { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
