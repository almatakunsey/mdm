﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Personalizations.Dto;

namespace Vtmis.WebAdmin.Personalizations
{
    public interface IPersonalizationService
    {
        /// <summary>
        /// Bulk Insert/Update personalization
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<IEnumerable<ResponsePersonalizationDto>> CreateOrUpdateAsync(RequestCreateBulkPersonalization input);
        /// <summary>
        /// Insert/Update personalization
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponsePersonalizationDto> CreateOrUpdateAsync(RequestCreatePersonalizationDto input);
        /// <summary>
        /// Get All Personalization by userid and tenantid
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tenantId"></param>
        /// <returns></returns>
        Task<IEnumerable<ResponsePersonalizationDto>> GetAsync(long userId, int? tenantId, string userLevel);
        /// <summary>
        /// Get Specific Key
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<ResponsePersonalizationDto> GetSpecificKeyAsync(RequestSpecificPersonaKey input);

        /// <summary>
        /// Get All Personalization by current user
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<ResponsePersonalizationDto>> GetAsync();
        /// <summary>
        /// Get specific personalization by key (For current user)
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task<ResponsePersonalizationDto> GetAsync(string key);
        /// <summary>
        /// Get specific personalization by key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="userId"></param>
        /// <param name="tenantId"></param>
        /// <param name="userLevel"></param>
        /// <returns></returns>
        Task<ResponsePersonalizationDto> GetAsync(string key, long userId, int? tenantId, string userLevel);
        /// <summary>
        /// Update personalization
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //Task<ResponsePersonalizationDto> UpdateAsync(string key, RequestUpdatePersonalizationDto input);
        /// <summary>
        /// Bulk update personalization
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //Task<List<ResponsePersonalizationDto>> UpdateAsync(List<RequestUpdatePersonalizationDto> input);
        /// <summary>
        /// Delete Personalization
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        Task DeleteAsync(string key);
        /// <summary>
        /// Bulk delete - personalization
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        Task DeleteAsync(List<string> keys);
        /// <summary>
        /// Delete Personalization
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="userId"></param>
        /// <param name="tenantId"></param>
        /// <param name="userLevel"></param>
        /// <returns></returns>
        Task DeleteAsync(List<string> keys, long userId, int? tenantId, string userLevel);
        /// <summary>
        /// Bulk delete - personalization
        /// </summary>
        /// <param name="key"></param>
        /// <param name="userId"></param>
        /// <param name="tenantId"></param>
        /// <param name="userLevel"></param>
        /// <returns></returns>
        Task DeleteAsync(string key, long userId, int? tenantId, string userLevel);
    }
}
