﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Permissions;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.Personalizations.Dto;
using Vtmis.WebAdmin.MultiTenancy;
using Abp.Authorization;
using Abp.UI;

namespace Vtmis.WebAdmin.Personalizations
{
    [RemoteService(IsMetadataEnabled = false)]
    public class PersonalizationService : ApplicationService, IPersonalizationService
    {
        private readonly IRepository<Personalization> _repository;
        private readonly IMdmPermissionService _mdmPermissionService;
        private readonly IRepository<Tenant, int> _tenantRepository;

        public PersonalizationService(IRepository<Personalization> repository, IMdmPermissionService mdmPermissionService, IRepository<Tenant, int> tenantRepository)
        {
            _repository = repository;
            _mdmPermissionService = mdmPermissionService;
            _tenantRepository = tenantRepository;
        }

        public async Task<ResponsePersonalizationDto> CreateOrUpdateAsync(RequestCreatePersonalizationDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                if ((await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.UserPersonalization_Create, true)) == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }

                Personalization personalization;
                var ownerId = AbpSession.UserId.Value;
                if (input.Key.IsEqual("theme"))
                {
                    personalization = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenantId == input.TenantId && x.UserLevel.IsEqual(input.UserLevel) &&
                              string.Equals(x.Key, input.Key, System.StringComparison.CurrentCultureIgnoreCase));
                }
                else
                {
                    personalization = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.UserId == AbpSession.UserId &&
                              x.TenantId == input.TenantId && x.UserLevel.IsEqual(input.UserLevel) &&
                              string.Equals(x.Key, input.Key, System.StringComparison.CurrentCultureIgnoreCase));
                }
                if (personalization != null)
                {
                    ownerId = personalization.UserId;
                }

                personalization = ObjectMapper.Map(input, personalization);
                personalization.UserId = ownerId;
                personalization.TenantId = input.TenantId;
                await _repository.InsertOrUpdateAsync(personalization);
                await CurrentUnitOfWork.SaveChangesAsync();

                return ObjectMapper.Map<ResponsePersonalizationDto>(personalization);
            }
        }

        public async Task<IEnumerable<ResponsePersonalizationDto>> CreateOrUpdateAsync(RequestCreateBulkPersonalization input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {

                if ((await _mdmPermissionService.IsGrantedAsync(MdmPermissionsConst.UserPersonalization_Create, true)) == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }

                if (input.Items.Select(x => x.Key).Count() != input.Items.Select(x => x.Key).Distinct().Count())
                {
                    throw new AlreadyExistException($"Duplicate keys in the request");
                }

                var results = new List<ResponsePersonalizationDto>();
                foreach (var item in input.Items)
                {
                    Personalization personalization;
                    var ownerId = AbpSession.UserId.Value;
                    if (item.Key.IsEqual("theme"))
                    {
                        personalization = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenantId == input.TenantId && x.UserLevel.IsEqual(input.UserLevel) &&
                                  string.Equals(x.Key, item.Key, System.StringComparison.CurrentCultureIgnoreCase));
                    }
                    else
                    {
                        personalization = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.UserId == AbpSession.UserId &&
                                  x.TenantId == input.TenantId && x.UserLevel.IsEqual(input.UserLevel) &&
                                  string.Equals(x.Key, item.Key, System.StringComparison.CurrentCultureIgnoreCase));
                    }
                    if (personalization != null)
                    {
                        ownerId = personalization.UserId;
                    }

                    personalization = ObjectMapper.Map(item, personalization);
                    personalization.UserId = ownerId;
                    personalization.TenantId = input.TenantId;
                    personalization.UserLevel = input.UserLevel;
                    await _repository.InsertOrUpdateAsync(personalization);
                    await CurrentUnitOfWork.SaveChangesAsync();

                    results.Add(ObjectMapper.Map<ResponsePersonalizationDto>(personalization));
                }

                return results;
            }
        }

        public async Task<IEnumerable<ResponsePersonalizationDto>> GetAsync()
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var personalizations = await _repository.GetAll().IgnoreQueryFilters().Where(x => x.UserId == AbpSession.UserId && x.TenantId == AbpSession.TenantId).ToListAsync();

                return ObjectMapper.Map<IEnumerable<ResponsePersonalizationDto>>(personalizations);
            }
        }

        public async Task<IEnumerable<ResponsePersonalizationDto>> GetAsync(long userId, int? tenantId, string userLevel)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var personalizations = _repository.GetAll().IgnoreQueryFilters().Where(x => x.UserLevel.IsEqual(userLevel)).AsQueryable();

                if (userLevel.IsEqual(UserLevelConst.TenantAdmin))
                {
                    personalizations = personalizations.Where(x => x.TenantId == tenantId);
                }
                else if (userLevel.IsEqual(UserLevelConst.TenantUser))
                {
                    personalizations = personalizations.Where(x => x.TenantId == tenantId && x.UserId == userId);
                }
                var result = await personalizations.ToListAsync();
                return ObjectMapper.Map<IEnumerable<ResponsePersonalizationDto>>(result);
            }
        }

        public async Task<ResponsePersonalizationDto> GetAsync(string key)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var personalization = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.UserId == AbpSession.UserId && x.TenantId == AbpSession.TenantId
                         && string.Equals(x.Key, key, System.StringComparison.CurrentCultureIgnoreCase));
                if (personalization == null)
                {
                    throw new NotFoundException($"Key - {key} not found");
                }
                return ObjectMapper.Map<ResponsePersonalizationDto>(personalization);

            }
        }

        public async Task<ResponsePersonalizationDto> GetAsync(string key, long userId, int? tenantId, string userLevel)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var personalization = _repository.GetAll().IgnoreQueryFilters().Where(x => x.Key.IsEqual(key) && x.UserLevel.IsEqual(userLevel)).AsQueryable();

                if (userLevel.IsEqual(UserLevelConst.TenantAdmin))
                {
                    personalization = personalization.Where(x => x.TenantId == tenantId);
                }
                else if (userLevel.IsEqual(UserLevelConst.TenantUser))
                {
                    personalization = personalization.Where(x => x.TenantId == tenantId && x.UserId == userId);
                }

                var result = await personalization.FirstOrDefaultAsync();
                if (result == null)
                {
                    throw new NotFoundException($"Key - {key} not found");
                }
                return ObjectMapper.Map<ResponsePersonalizationDto>(result);

            }
        }
        public async Task DeleteAsync(string key)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var query = _repository.GetAll().IgnoreQueryFilters().Where(x => x.UserId == AbpSession.UserId
                        && string.Equals(x.Key, key, System.StringComparison.CurrentCultureIgnoreCase));
                if (await query.AnyAsync())
                {
                    var result = await query.FirstOrDefaultAsync();
                    await _repository.DeleteAsync(result);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    return;
                }

                throw new NotFoundException($"Key - {key} not found");

            }
        }

        public async Task DeleteAsync(string key, long userId, int? tenantId, string userLevel)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var query = _repository.GetAll().IgnoreQueryFilters().Where(x => x.UserId == userId
                        && x.Key.IsEqual(key) && x.TenantId == tenantId && x.UserLevel.IsEqual(userLevel));
                if (await query.AnyAsync())
                {
                    var result = await query.FirstOrDefaultAsync();
                    await _repository.DeleteAsync(result);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    return;
                }

                throw new NotFoundException($"Key - {key} not found");

            }
        }

        public async Task DeleteAsync(List<string> keys)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                if (keys.Count() != keys.Distinct().Count())
                {
                    throw new AlreadyExistException($"Duplicate keys in the request");
                }

                var queryExistKeys = _repository.GetAll().IgnoreQueryFilters().Where(x => x.UserId == AbpSession.UserId
                         && keys.Any(i => string.Equals(x.Key, i, System.StringComparison.CurrentCultureIgnoreCase)));

                if ((await queryExistKeys.CountAsync() != keys.Count) || !(await queryExistKeys.AnyAsync()))
                {
                    throw new NotFoundException($"Request contain keys that do not exist");
                }

                if (await queryExistKeys.AnyAsync())
                {
                    foreach (var item in await queryExistKeys.ToListAsync())
                    {
                        await _repository.DeleteAsync(item);
                        await CurrentUnitOfWork.SaveChangesAsync();
                    }
                }

            }
        }

        public async Task DeleteAsync(List<string> keys, long userId, int? tenantId, string userLevel)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                if (keys.Count() != keys.Distinct().Count())
                {
                    throw new AlreadyExistException($"Duplicate keys in the request");
                }

                var queryExistKeys = _repository.GetAll().IgnoreQueryFilters().Where(x => x.UserId == userId
                         && keys.Any(i => x.Key.IsEqual(i)) && x.TenantId == tenantId && x.UserLevel.IsEqual(userLevel));

                if ((await queryExistKeys.CountAsync() != keys.Count) || !(await queryExistKeys.AnyAsync()))
                {
                    throw new NotFoundException($"Request contain keys that do not exist");
                }

                if (await queryExistKeys.AnyAsync())
                {
                    foreach (var item in await queryExistKeys.ToListAsync())
                    {
                        await _repository.DeleteAsync(item);
                        await CurrentUnitOfWork.SaveChangesAsync();
                    }
                }
            }
        }

        [AbpAllowAnonymous]
        public async Task<ResponsePersonalizationDto> GetSpecificKeyAsync(RequestSpecificPersonaKey input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var query = _repository.GetAll().IgnoreQueryFilters().Where(x => x.Key.IsEqual(input.Key)
                              && x.UserLevel.IsEqual(input.UserLevel)).AsQueryable();

                if (input.TenantId != null)
                {
                    query = query.Where(x => x.TenantId == input.TenantId);
                }
                else if (string.IsNullOrWhiteSpace(input.TenantName) == false)
                {
                    if (input.TenantName.IsEqual("admin"))
                    {
                        query = query.Where(x => x.TenantId == null);
                    }
                    else
                    {
                        var tenant = await _tenantRepository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.TenancyName.IsEqual(input.TenantName) && x.IsDeleted == false);
                        query = query.Where(x => x.TenantId == tenant.Id);
                    }

                }
                else if (input.UserId != null)
                {
                    query = query.Where(x => x.UserId == input.UserId);
                }

                var result = await query.FirstOrDefaultAsync();
                if (result != null)
                {
                    return ObjectMapper.Map<ResponsePersonalizationDto>(result);
                }

                throw new UserFriendlyException($"Key - {input.Key} not found");
            }

        }
    }
}
