﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Personalize.Dto;

namespace Vtmis.WebAdmin.Personalize
{
    public class PersonalizeService : ApplicationService
    {
        //private readonly IRepository<VtmisDomains.Personalize> _personalizeRepository;

        //public PersonalizeService(IRepository<VtmisDomains.Personalize> personalizeRepository)
        //{
        //    _personalizeRepository = personalizeRepository;
        //}

        //public async Task<PersonalizeDto> Create(PersonalizeDto input)
        //{
        //    var personalize = await _personalizeRepository.FirstOrDefaultAsync(c => c.Name.ToLower()
        //        == input.Name.ToLower());

        //    if (personalize == null)
        //    {
        //        personalize = ObjectMapper.Map<VtmisDomains.Personalize>(input);

        //        int id = await _personalizeRepository.InsertAndGetIdAsync(personalize);

        //        input.Id = id;
        //    }
        //    else
        //    {

        //        if (input.Reports.Any())
        //        {
        //            personalize.Reports = ObjectMapper.Map<List<VtmisDomains.Report>>(input.Reports);
        //        }

        //        int id = await _personalizeRepository.InsertOrUpdateAndGetIdAsync(personalize);

        //        input.Id = id;
        //    }

        //    return input;
        //}

        //public List<PersonalizeDto> Get()
        //{
        //    var personalize = _personalizeRepository.GetAllIncluding().Include(x => x.Reports)
        //        .ToList();

        //    return ObjectMapper.Map<List<PersonalizeDto>>(personalize);
        //}

        //public async Task<PersonalizeDto> GetById(int id)
        //{
        //    var personalize = await _personalizeRepository.FirstOrDefaultAsync(c => c.Id == id);

        //    if (personalize == null)
        //    {
        //        throw new UserFriendlyException("Personalize Does Not Exist");
        //    }
        //    else
        //    {
        //        return ObjectMapper.Map<PersonalizeDto>(personalize);
        //    }
        //}

        //public async Task<PersonalizeDto> Update(PersonalizeDto input)
        //{

        //    var personalize = await _personalizeRepository.GetAsync(input.Id);

        //    if (personalize != null)
        //    {
        //        personalize.Name = input.Name;
        //        personalize.PersonalizeType = input.PersonalizeType;
        //        personalize.Reports = ObjectMapper.Map<List<VtmisDomains.Report>>(input.Reports);
        //        await _personalizeRepository.UpdateAsync(personalize);
        //    }

        //    return input;
        //}

        //public async Task Delete(int id)
        //{
        //    var personalize = await _personalizeRepository.GetAsync(id);

        //    await _personalizeRepository.DeleteAsync(personalize);

        //}
    }
}
