﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Reports.Dto;

namespace Vtmis.WebAdmin.Reports
{
    public interface IReportService
    {
        Task<ReportResponseDto> CreateWithCurrentUser(CreateReportDto input);
        Task<PaginatedList<ReportResponseDto>> GetAllAsync(int pageIndex = 1, int pageSize = 10);
        Task<ReportReponseDetailDto> GetByIdAsync(int id, bool isDetail = false);
        Task<ReportResponseDto> UpdateAsync(UpdateReportDto input);
        Task DeleteAsync(int id);
        IEnumerable<TimeSpanTypeObj> GetTimeSpanType();
    }
}
