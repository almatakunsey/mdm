﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Reports.Dto;
using Vtmis.WebAdmin.Zones;

namespace Vtmis.WebAdmin.Reports
{
    //[AbpAuthorize]
    [RemoteService(IsMetadataEnabled = false)]
    public class ReportService : ApplicationService, IReportService
    {
        private readonly IRepository<Report> _reportRepository;
        private readonly IRepository<ReportType> _reportTypeRepository;
        private readonly UserManager _userManager;
        private readonly IZoneService _zoneService;
        private readonly IFilterAppService _filterAppService;

        public ReportService(IRepository<Report> reportRepository, IRepository<ReportType> reportTypeRepository,
            UserManager userManager, IZoneService zoneService, IFilterAppService filterAppService)
        {
            _reportRepository = reportRepository;
            _reportTypeRepository = reportTypeRepository;
            _userManager = userManager;
            _zoneService = zoneService;
            _filterAppService = filterAppService;
        }

        public async Task<ReportResponseDto> CreateWithCurrentUser(CreateReportDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                   
                    if (input.ReportItems != null && input.ReportItems.Any())
                    {
                        foreach (var item in input.ReportItems)
                        {
                            var zone = await _zoneService.GetById(item.ZoneId);
                            if (zone == null)
                            {
                                throw new NotFoundException($"Zone does not exist");
                            }
                            var filter = await _filterAppService.GetByIdAsync(item.FilterId);
                            if (filter == null)
                            {
                                throw new NotFoundException($"Filter does not exist");
                            }
                        }
                    }
                    if ((await _reportTypeRepository.FirstOrDefaultAsync(x => x.Id == input.ReportTypeId)) == null)
                    {
                        throw new NotFoundException("Report type does not exist");
                    }
                    if (await _reportRepository.GetAll().Where(x => x.Name.ToUpper() == input.Name.ToUpper()
                                && x.UserId == AbpSession.UserId && x.IsDeleted == false).AnyAsync())
                    {
                        throw new AlreadyExistException("Report with same name already exist!");
                    }
                    else
                    {
                        if (await IsGrantedAsync(MdmPermissionsConst.CreateReport))
                        {
                            var reportType = await _reportTypeRepository.FirstOrDefaultAsync(x => x.Id == input.ReportTypeId);
                            var report = ObjectMapper.Map<Report>(input);
                            if (reportType.ReportTypeNumber.ToInt() == ReportTypeConst.SpeedViolation)
                            {
                                foreach (var item in input.ReportItems)
                                {
                                    if (item.MinSpeed == item.MaxSpeed)
                                    {
                                        throw new RequestFaultException("Min Speed value cannot be the same as Max Speed");
                                    }
                                    if (item.MinSpeed > item.MaxSpeed)
                                    {
                                        throw new RequestFaultException("Min Speed value cannot be higher than Max Speed");
                                    }
                                }

                                report.ReportItems.Clear();
                                foreach (var item in input.ReportItems)
                                {
                                    report.ReportItems.Add(new SpeedViolation
                                    {
                                        ZoneId = item.ZoneId,
                                        FilterId = item.FilterId,
                                        MaxSpeed = item.MaxSpeed,
                                        MinSpeed = item.MinSpeed,
                                    });
                                }
                            }
                            report.SetCurrentUser(AbpSession.TenantId, AbpSession.UserId);
                            var result = await _reportRepository.InsertAsync(report);

                            await _reportRepository.EnsureCollectionLoadedAsync(result, x => x.ReportItems);
                            await CurrentUnitOfWork.SaveChangesAsync();
                            return ObjectMapper.Map<ReportResponseDto>(result);
                        }
                        throw new ForbiddenException("Permision Denied");
                    }
                }
                catch (RequestFaultException err)
                {
                    throw err;
                }
                catch (ForbiddenException err)
                {
                    throw err;
                }
                catch (AlreadyExistException err)
                {
                    throw err;
                }
                catch (NotFoundException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }
            }

        }

        public async Task<PaginatedList<ReportResponseDto>> GetAllAsync(int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                {
                    var qry = _reportRepository.GetAll().IgnoreQueryFilters()
                                .Include(x => x.ReportItems)
                                .Where(x => x.IsDeleted == false)
                                .OrderByDescending(x => x.CreationTime).AsQueryable();

                    var isHighPermission = false;
                    if (await IsGrantedAsync(MdmPermissionsConst.ViewReportWithinMdm))
                    {
                        isHighPermission = true;
                    }
                    if (await IsGrantedAsync(MdmPermissionsConst.ViewReportWithinTenant))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                        }
                    }
                    if (await IsGrantedAsync(MdmPermissionsConst.ViewOwnReport))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.UserId == AbpSession.UserId);
                        }
                    }
                    if (isHighPermission == false)
                    {
                        var result = new PaginatedList<Report>().SetNull();
                        return ObjectMapper.Map<PaginatedList<ReportResponseDto>>(result);
                    }
                    var results = await new PaginatedList<Report>().CreateAsync(qry, pageIndex, pageSize);
                    return ObjectMapper.Map<PaginatedList<ReportResponseDto>>(results);
                }
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err);
            }
        }

        public async Task<ReportReponseDetailDto> GetByIdAsync(int id, bool isDetail = false)
        {
            try
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                {
                    var qry = _reportRepository.GetAll().IgnoreQueryFilters().AsQueryable();

                    if (isDetail)
                    {
                        qry = qry.Include("ReportItems")
                                    .Include("ReportType")
                                    .Include("ReportItems.Zone")
                                    .Include("ReportItems.Zone.ZoneItems")
                                    .Include("ReportItems.Filter")
                                    .Include("ReportItems.Filter.FilterItems")
                                    .Include("ReportItems.Filter.FilterItems.FilterDetails")
                                    .Include("ReportItems.FilterGroup");
                    }

                    var report = await qry.FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
                    if (report == null)
                    {
                        throw new NotFoundException("Report does not exist");
                    }
                    var isHighPermission = false;
                    if (await IsGrantedAsync(MdmPermissionsConst.ViewReportWithinMdm))
                    {
                        isHighPermission = true;
                    }
                    if (await IsGrantedAsync(MdmPermissionsConst.ViewReportWithinTenant))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            if (report.TenantId != AbpSession.TenantId)
                            {
                                throw new ForbiddenException("Permission Denied");
                            }
                        }
                    }
                    if (await IsGrantedAsync(MdmPermissionsConst.ViewOwnReport))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            if (report.UserId != AbpSession.UserId)
                            {
                                throw new ForbiddenException("Permission Denied");
                            }
                        }
                    }
                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                    var vm =  ObjectMapper.Map<ReportReponseDetailDto>(report);
                    if (report.ReportType.ReportTypeNumber.ToInt() == ReportTypeConst.SpeedViolation)
                    {
                        foreach (var item in report.ReportItems.OfType<SpeedViolation>())
                        {
                            vm.ReportItems.Where(x => x.Id == item.Id).FirstOrDefault().MinSpeed = item.MinSpeed;
                            vm.ReportItems.Where(x => x.Id == item.Id).FirstOrDefault().MaxSpeed = item.MaxSpeed;
                        }
                    }                    
                    return vm;
                }
            }
            catch (ForbiddenException err)
            {
                throw new ForbiddenException(err.Message);
            }
            catch (NotFoundException err)
            {
                throw new NotFoundException(err.Message);
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err);
            }
        }

        public async Task<ReportResponseDto> UpdateAsync(UpdateReportDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {

                try
                {
                    var existingReport = await _reportRepository.GetAll().IgnoreQueryFilters().Include(x => x.ReportItems)
                                                .Where(x => x.Id == input.Id && x.IsDeleted == false).FirstOrDefaultAsync();
                    
                    if (input.ReportItems != null && input.ReportItems.Any())
                    {
                        foreach (var item in input.ReportItems)
                        {
                            var zone = await _zoneService.GetById(item.ZoneId);
                            if (zone == null)
                            {
                                throw new NotFoundException($"Zone does not exist");
                            }
                            var filter = await _filterAppService.GetByIdAsync(item.FilterId);
                            if (filter == null)
                            {
                                throw new NotFoundException($"Filter does not exist");
                            }
                        }
                    }

                    if (existingReport == null)
                    {
                        throw new NotFoundException($"Report does not exist");
                    }
                    else
                    {
                        var othersFilters = _reportRepository.GetAll().IgnoreQueryFilters()
                            .Where(x => string.Equals(x.Name, input.Name, StringComparison.CurrentCultureIgnoreCase) &&
                                                    x.Id != input.Id && x.IsDeleted == false && x.UserId == AbpSession.UserId).Any();
                        if (othersFilters)
                        {
                            throw new AlreadyExistException($"Report {input.Name} already exist.");
                        }
                    }



                    var report = ObjectMapper.Map(input, existingReport);
                    var isHighPermission = false;

                    if (await IsGrantedAsync(MdmPermissionsConst.UpdateReportWithinMdm))
                    {
                        isHighPermission = true;
                    }
                    if (await IsGrantedAsync(MdmPermissionsConst.UpdateReportWithinTenant))
                    {
                        isHighPermission = true;
                        if (report.TenantId != AbpSession.TenantId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (await IsGrantedAsync(MdmPermissionsConst.UpdateOwnReport))
                    {
                        isHighPermission = true;
                        if (report.UserId != AbpSession.UserId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                    var reportType = await _reportTypeRepository.FirstOrDefaultAsync(x => x.Id == input.ReportTypeId);
                    if (reportType.ReportTypeNumber.ToInt() == ReportTypeConst.SpeedViolation)
                    {
                        report.ReportItems.Clear();
                        //await _reportRepository.UpdateAsync(report);
                        //await CurrentUnitOfWork.SaveChangesAsync();
                        foreach (var item in input.ReportItems)
                        {
                            report.ReportItems.Add(new SpeedViolation
                            {
                                FilterId = item.FilterId,
                                MinSpeed = item.MinSpeed,
                                MaxSpeed = item.MaxSpeed,
                                ZoneId = item.ZoneId,
                            });
                        }
                    }

                    var result = await _reportRepository.UpdateAsync(report);
                    await CurrentUnitOfWork.SaveChangesAsync();

                    var vm = new ReportResponseDto();
                    if (reportType.ReportTypeNumber.ToInt() == ReportTypeConst.SpeedViolation)
                    {
                        vm.Id = result.Id;
                        vm.IsEnabled = result.IsEnabled;
                        vm.Name = result.Name;
                        //vm.ReportType = result.ReportType;
                        vm.ReportTypeId = result.ReportTypeId;
                        vm.TimeSpan = result.TimeSpan;
                        vm.TimeSpanType = result.TimeSpanType;
                        foreach (var item in result.ReportItems.OfType<SpeedViolation>())
                        {
                            vm.ReportItems.Add(new SpeedViolationResponseDto {
                                FilterGroupId = item.FilterGroupId,
                                FilterId = item.FilterId,
                                Id = item.Id,
                                MaxSpeed = item.MaxSpeed,
                                MinSpeed = item.MinSpeed,
                                ZoneId = item.ZoneId
                            });
                        }
                    }
                    else
                    {
                        vm = ObjectMapper.Map<ReportResponseDto>(result);
                    }
                    return vm;
                }
                catch (AlreadyExistException err)
                {
                    throw err;
                }
                catch (NotFoundException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err);
                }
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var isExist = await _reportRepository.GetAll().IgnoreQueryFilters()
                            .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
                    if (isExist == null)
                    {
                        throw new NotFoundException("Report Does Not Exist");
                    }

                    var isHighPermission = false;

                    if (await IsGrantedAsync(MdmPermissionsConst.DeleteReportWithinMdm))
                    {
                        isHighPermission = true;
                    }
                    if (await IsGrantedAsync(MdmPermissionsConst.DeleteReportWithinTenant))
                    {
                        isHighPermission = true;
                        if (isExist.TenantId != AbpSession.TenantId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (await IsGrantedAsync(MdmPermissionsConst.DeleteOwnReport))
                    {
                        isHighPermission = true;
                        if (isExist.UserId != AbpSession.UserId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }
                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                    isExist.DeleterUserId = AbpSession.UserId;
                    isExist.DeletionTime = DateTime.Now;
                    isExist.IsDeleted = true;
                    await _reportRepository.UpdateAsync(isExist);

                    await CurrentUnitOfWork.SaveChangesAsync();
                }
                catch (NotFoundException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }
            }
        }

        public IEnumerable<TimeSpanTypeObj> GetTimeSpanType()
        {
            return Enum.GetValues(typeof(TimeSpanType)).Cast<TimeSpanType>().Select(x => new TimeSpanTypeObj
            {
                Value = (int)x,
                Name = x.ToString()
            });
        }

        private void CreateReportItemByReportType(string reportTypeNumber, Report report, List<ReportItemDto> reportItemDtos)
        {
            if (reportTypeNumber == "03")
            {
                foreach (var ri in reportItemDtos)
                {
                    var zt = new ArrivalDeparture
                    {
                        Report = report
                    };

                    report.ReportItems.Add(zt);
                }
            }
            else if (reportTypeNumber == "05")
            {
                foreach (var ri in reportItemDtos)
                {
                    var zt = new BerthTimeSummary
                    {
                        Report = report,
                        Fee = ri.Fee
                    };

                    report.ReportItems.Add(zt);
                }
            }
            else if (reportTypeNumber == "18")
            {
                foreach (var ri in reportItemDtos)
                {
                    var zt = new ZoneTrip
                    {
                        Report = report,
                        Fee = ri.Fee
                    };

                    report.ReportItems.Add(zt);
                }
            }
            else if (reportTypeNumber == "07")
            {
                foreach (var ri in reportItemDtos)
                {
                    var zt = new SpeedViolation
                    {
                        Report = report,
                        MaxSpeed = ri.MaxSpeed,
                        MinSpeed = ri.MinSpeed,
                        FilterGroupId = ri.FilterGroupId,
                        FilterId = ri.FilterId
                    };
                    report.ReportItems.Add(zt);
                }
            }
            else if (reportTypeNumber == "18")
            {
                foreach (var ri in reportItemDtos)
                {
                    var zt = new AnchorageTime
                    {
                        Report = report,
                        MaxSpeed = ri.MaxSpeed,
                        Fee = ri.Fee
                    };

                    report.ReportItems.Add(zt);
                }
            }
        }


    }

    public class TimeSpanTypeObj
    {
        public int Value { get; set; }
        public string Name { get; set; }
    }
}
