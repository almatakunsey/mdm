﻿namespace Vtmis.WebAdmin.Reports.Dto
{
    public class CreateReportItemDto
    {
        public decimal Fee { get; set; }       
        public int ZoneId { get; set; }
        public int FilterId { get; set; }       
        public decimal MinSpeed { get; set; }
        public decimal MaxSpeed { get; set; }
    }
}
