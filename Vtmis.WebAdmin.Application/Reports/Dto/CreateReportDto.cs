﻿using System.Collections.Generic;

namespace Vtmis.WebAdmin.Reports.Dto
{
    public class CreateReportDto
    {
        public CreateReportDto()
        {
            ReportItems = new List<CreateReportItemDto>();
        }
        public string Name { get; set; }
        public double TimeSpan { get; set; }
        public TimeSpanType TimeSpanType { get; set; }
        public bool IsEnabled { get; set; }
        public int ReportTypeId { get; set; }
      
        public List<CreateReportItemDto> ReportItems { get; set; }
    }
}
