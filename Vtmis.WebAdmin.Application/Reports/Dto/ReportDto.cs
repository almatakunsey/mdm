﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using Vtmis.WebAdmin.ReportTypes.Dto;
using Vtmis.WebAdmin.Users.Dto;

namespace Vtmis.WebAdmin.Reports.Dto
{
    [AutoMapFrom(typeof(Report))]
    public class ReportDto : EntityDto
    {
        public ReportDto()
        {
            CreationTime = DateTime.Now;
        }
        public int UserId { get; set; }
        public DateTime CreationTime { get; set; }
        public string Name { get; set; }
        public double TimeSpan { get; set; }
        public TimeSpanType TimeSpanType { get; set; }
        public bool IsEnabled { get; set; }
        public int ReportTypeId { get; set; }
        public virtual ReportTypeDto ReportType { get; set; }

        public virtual List<ReportItemDto> ReportItemDtos { get; set; }

    }

    public class ReportItemDto : EntityDto
    {
       

        public decimal Fee { get; set; }

        public int ReportId { get; set; }

        public int ZoneId { get; set; }

        public int FilterId { get; set; }
        public int? FilterGroupId { get; set; }

        public decimal MinSpeed { get; set; }
        public decimal MaxSpeed { get; set; }
    }

}
