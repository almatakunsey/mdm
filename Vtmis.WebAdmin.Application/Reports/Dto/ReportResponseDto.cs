﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using Vtmis.WebAdmin.ReportTypes.Dto;

namespace Vtmis.WebAdmin.Reports.Dto
{
    public class ReportResponseDto : EntityDto
    {
        public ReportResponseDto()
        {
            ReportItems = new List<ReportItemResponseDto>();
        }
        public string Name { get; set; }
        public double TimeSpan { get; set; }
        public TimeSpanType TimeSpanType { get; set; }
        public bool IsEnabled { get; set; }

        public virtual ReportTypeResponseDto ReportType { get; set; }
        public virtual int ReportTypeId { get; set; }
       
        public virtual List<ReportItemResponseDto> ReportItems { get; set; }
    }
}
