﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Vtmis.Core.Model.DTO.Filter;
using Vtmis.WebAdmin.Filters.Dto;
using Vtmis.WebAdmin.Filters.v2.Dto;
using Vtmis.WebAdmin.Zones.Dto;

namespace Vtmis.WebAdmin.Reports.Dto
{
    [AutoMapFrom(typeof(ReportItem))]
    public class ReportItemResponseDetailDto : EntityDto
    {
        public int ZoneId { get; set; }
        public ZoneDto Zone { get; set; }
        public int? FilterGroupId { get; set; }
        public FilterGroupByIdResponse FilterGroup { get; set; }
        public int FilterId { get; set; }
        public FilterResponseDto Filter { get; set; }
        public decimal Fee { get; set; }
        public decimal MinSpeed { get; set; }
        public decimal MaxSpeed { get; set; }
    }   
}
