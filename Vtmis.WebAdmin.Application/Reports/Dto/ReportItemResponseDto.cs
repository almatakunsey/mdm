﻿using Abp.Application.Services.Dto;
using Vtmis.WebAdmin.Filters.v2.Dto;
using Vtmis.WebAdmin.Zones.Dto;

namespace Vtmis.WebAdmin.Reports.Dto
{
    public class ReportItemResponseDto : EntityDto
    {
        public virtual ZoneDto Zone { get; set; }
        public virtual int ZoneId { get; set; }
        public virtual int? FilterGroupId { get; set; }
        public virtual FilterResponseDto Filter { get; set; }
        public virtual int FilterId { get; set; }
    }

    public class DefaultReportItem : ReportItemResponseDto
    {

    }

    //03
    public class ArrivalDeparture : ReportItemResponseDto
    {

    }

    //18
    public class ZoneTrip : ReportItemResponseDto
    {
        public decimal Fee { get; set; }
    }

    //05
    public class BerthTimeSummary : ReportItemResponseDto
    {
        public decimal Fee { get; set; }
    }

    //07
    public class SpeedViolationResponseDto : ReportItemResponseDto
    {
        public decimal MinSpeed { get; set; }
        public decimal MaxSpeed { get; set; }
    }

    //16
    public class AnchorageTime : ReportItemResponseDto
    {
        public decimal MaxSpeed { get; set; }
        public decimal Fee { get; set; }
    }
}
