﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.Reports.Dto
{
    public class UpdateReportDto : EntityDto
    {
        public UpdateReportDto()
        {
            ReportItems = new List<UpdateReportItemDto>();
        }
        public string Name { get; set; }
        public double TimeSpan { get; set; }
        public TimeSpanType TimeSpanType { get; set; }
        public bool IsEnabled { get; set; }
        public int ReportTypeId { get; set; }

        public List<UpdateReportItemDto> ReportItems { get; set; }
    }
}
