﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System.Collections.Generic;
using Vtmis.WebAdmin.ReportTypes.Dto;

namespace Vtmis.WebAdmin.Reports.Dto
{
    [AutoMapFrom(typeof(Report))]
    public class ReportReponseDetailDto : EntityDto
    {
        public ReportReponseDetailDto()
        {
            ReportItems = new List<ReportItemResponseDetailDto>();
        }
        public string Name { get; set; }
        public double TimeSpan { get; set; }
        public TimeSpanType TimeSpanType { get; set; }
        public bool IsEnabled { get; set; }

        public virtual ReportTypeResponseDto ReportType { get; set; }
        public virtual int ReportTypeId { get; set; }

        public List<ReportItemResponseDetailDto> ReportItems { get; set; }
    }
}
