﻿using Abp.Application.Services.Dto;

namespace Vtmis.WebAdmin.Reports.Dto
{
    public class UpdateReportItemDto : EntityDto
    {
        public decimal Fee { get; set; }
        public int ZoneId { get; set; }
        public int FilterId { get; set; }
        public decimal MinSpeed { get; set; }
        public decimal MaxSpeed { get; set; }
    }
}
