﻿using Abp.AutoMapper;

namespace Vtmis.WebAdmin.Zones.Dto
{
    [AutoMapFrom(typeof(ZoneItem))]
    public class ZoneItemDto
    {
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Logitude { get; set; }
        public int Order { get; set; }
        //public int ZoneId { get; set; }
    }
}
