﻿namespace Vtmis.WebAdmin.Zones.Dto
{
    public class UpdateZoneDto
    {
        public long UserId { get; set; }
        public ZoneDto ZoneDto { get; set; }
    }
}
