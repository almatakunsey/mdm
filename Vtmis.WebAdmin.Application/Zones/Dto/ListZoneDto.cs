﻿using Abp.AutoMapper;
using System;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Zones.Dto
{
    [AutoMapFrom(typeof(Zone))]
    public class ListZoneDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsEnable { get; set; }
        public decimal Radius { get; set; }
        public string Colour { get; set; }
        public bool IsFill { get; set; }
        public ZoneType ZoneType { get; set; }
    }
}
