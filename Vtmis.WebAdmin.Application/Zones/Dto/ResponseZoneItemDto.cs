﻿using Abp.AutoMapper;
using System;

namespace Vtmis.WebAdmin.Zones.Dto
{
    [AutoMapFrom(typeof(ZoneItem))]
    public class ResponseZoneItemDto
    {
        private double _latitude;
        private double _longitude;
        public int Id { get; set; }
        public int Order { get; set; }
        public string Latitude
        {
            get { return this._latitude.ToString("N8"); }
            set { this._latitude = Convert.ToDouble(value); }
        }
        public string Logitude
        {
            get { return this._longitude.ToString("N8"); }
            set { this._longitude = Convert.ToDouble(value); }
        }
    }
}
