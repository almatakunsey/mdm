﻿namespace Vtmis.WebAdmin.Zones.Dto
{
    public class CreateZoneDto
    {
        public long UserId { get; set; }
        public ZoneDto ZoneDto { get; set; }
        //public List<FilterItemDto> FilterItemDtos { get; set; }
    }
}
