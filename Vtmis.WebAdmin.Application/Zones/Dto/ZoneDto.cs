﻿using Abp.AutoMapper;
using System.Collections.Generic;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Zones.Dto
{
    [AutoMapFrom(typeof(Zone))]
    public class ZoneDto
    {
        public ZoneDto()
        {
            ZoneItems = new List<ResponseZoneItemDto>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsEnable { get; set; }

        public decimal Radius { get; set; }
        public string Colour { get; set; }
        public bool IsFill { get; set; }
        public ZoneType ZoneType { get; set; }
        public ICollection<ResponseZoneItemDto> ZoneItems { get; set; }

    }
}
