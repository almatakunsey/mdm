﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System.Collections.Generic;
using Vtmis.WebAdmin.Authorization.Users;
using System.Linq;
using Abp.UI;
using System.Threading;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Zones.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using System;
using Vtmis.WebAdmin.Permissions;

namespace Vtmis.WebAdmin.Zones
{
    [AbpAuthorize]
    public class ZoneService : ApplicationService, IZoneService
    {
        private readonly IRepository<Zone> _zoneRepository;
        private readonly IRepository<ZoneItem> _zoneItemRepo;
        private readonly UserManager _userManager;
        private readonly IMdmPermissionService _mdmPermission;

        public ZoneService(IRepository<Zone> zoneRepository, IRepository<ZoneItem> zoneItemRepo, UserManager userManager, IMdmPermissionService mdmPermission)
        {
            _zoneRepository = zoneRepository;
            _zoneItemRepo = zoneItemRepo;
            _userManager = userManager;
            _mdmPermission = mdmPermission;
        }

        public async Task<CreateZoneDto> Create(CreateZoneDto m)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {

                if ((await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.CreateZone, true)) == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
                try
                {
                    var isZoneNameExist = _zoneRepository.GetAll().IgnoreQueryFilters().Where(x => x.Name.ToLower() ==
                                    m.ZoneDto.Name.ToLower() && x.UserId == AbpSession.UserId).Any();

                    if (isZoneNameExist)
                    {
                        throw new AlreadyExistException($"Zone {m.ZoneDto.Name} already exist.");
                    }

                    var zone = ObjectMapper.Map<Zone>(m.ZoneDto);
                    zone.SetCurrentUser(AbpSession.TenantId, AbpSession.UserId);
                    //var coordinateOrder = 0;
                    //foreach (var item in zone.ZoneItems)
                    //{
                    //    coordinateOrder += 1;
                    //    item.Order = coordinateOrder;
                    //}
                    var result = ObjectMapper.Map<ZoneDto>(await
                        _zoneRepository.InsertAsync(zone));
                    return new CreateZoneDto
                    {
                        UserId = AbpSession.UserId.Value,
                        ZoneDto = result
                    };
                }
                catch (ForbiddenException err)
                {
                    throw err;
                }
                catch (AlreadyExistException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }
            }
        }

        public async Task<UpdateZoneDto> Update(UpdateZoneDto m)
        {

            try
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                {
                    var existingZone = await _zoneRepository.GetAll().IgnoreQueryFilters()
                                            .Include(x => x.ZoneItems)
                                            .Where(x => x.Id == m.ZoneDto.Id).FirstOrDefaultAsync();



                    if (existingZone == null)
                    {
                        throw new NotFoundException($"Zone does not exist");
                    }
                    else
                    {
                        var othersZone = _zoneRepository.GetAll().IgnoreQueryFilters().Where(x => x.Name.ToLower() == m.ZoneDto.Name.ToLower() &&
                                                    x.Id != m.ZoneDto.Id && x.UserId == AbpSession.UserId).Any();
                        if (othersZone)
                        {
                            throw new AlreadyExistException($"Zone {m.ZoneDto.Name} already exist.");
                        }
                    }

                    //var zoneItems = _zoneItemRepo.GetAll().IgnoreQueryFilters().Where(x => x.ZoneId == existingZone.Id);
                    //foreach (var zoneItem in zoneItems)
                    //{
                    //    await _zoneItemRepo.DeleteAsync(zoneItem);
                    //}                 

                    existingZone.ZoneItems.Clear();
                    _zoneRepository.Update(existingZone);
                    await CurrentUnitOfWork.SaveChangesAsync();

                    var zone = ObjectMapper.Map(m.ZoneDto, existingZone);
                    var isHighPermission = false;

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateZoneWithinMdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateZoneWithinTenant, true))
                    {
                        isHighPermission = true;
                        if (zone.TenantId != AbpSession.TenantId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateOwnZone, true))
                    {
                        isHighPermission = true;
                        if (zone.UserId != AbpSession.UserId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }
                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                    var zoneItems = zone.ZoneItems.ToList();
                    zone.ZoneItems.Clear();
                    var result = _zoneRepository.Update(zone);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    //var coordinateOrder = 0;
                    var newZoneItems = new List<ZoneItem>();
                    foreach (var zoneItem in zoneItems)
                    {
                        //coordinateOrder += 1;
                        var z = await _zoneItemRepo.InsertAsync(new ZoneItem
                        {
                            Latitude = zoneItem.Latitude,
                            Logitude = zoneItem.Logitude,
                            ZoneId = result.Id,
                            Order = zoneItem.Order
                        });
                        newZoneItems.Add(z);
                    }
                    await CurrentUnitOfWork.SaveChangesAsync();
                    result.ZoneItems = newZoneItems;
                    return new UpdateZoneDto
                    {
                        UserId = AbpSession.UserId.Value,
                        ZoneDto = ObjectMapper.Map<ZoneDto>(result)
                    };
                }
            }
            catch (AlreadyExistException err)
            {
                throw err;
            }
            catch (NotFoundException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task Delete(int id)
        {
            //var zone = await _zoneRepository.GetAsync(id);

            //await _zoneRepository.DeleteAsync(zone);

            try
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                {


                    var isExist = await _zoneRepository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == id);
                    if (isExist == null)
                    {
                        throw new NotFoundException("Zone Does Not Exist");
                    }

                    var isHighPermission = false;

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteZoneWithinMdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteZoneWithinTenant, true))
                    {
                        isHighPermission = true;
                        if (isExist.TenantId != AbpSession.TenantId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteOwnZone, true))
                    {
                        isHighPermission = true;
                        if (isExist.UserId != AbpSession.UserId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }
                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                    await _zoneRepository.DeleteAsync(isExist);
                }
            }
            catch (NotFoundException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task<ZoneListDto> GetById(int id)
        {
            //CancellationTokenSource source = new CancellationTokenSource();
            //CancellationToken cancellationToken = source.Token;

            //var zone = await _zoneRepository.GetAsync(id);
            //await _zoneRepository.EnsureCollectionLoadedAsync(zone, m => m.ZoneItems, cancellationToken);


            //var zoneDto = ObjectMapper.Map<ZoneListDto>(zone);
            //zoneDto.ZoneItemDtos = ObjectMapper.Map<List<ZoneItemDto>>(zone.ZoneItems);

            //return zoneDto;

            try
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                {
                    var isHighPermission = false;
                    var qry = _zoneRepository.GetAll().IgnoreQueryFilters().AsQueryable();

                    //Check permissions
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewZoneWithinMdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewZoneWithinTenant, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                        }
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewOwnZone, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.UserId == AbpSession.UserId);
                        }
                    }

                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }

                    qry = qry.Include(x => x.ZoneItems).Where(x => x.Id == id);

                    var result = await qry.FirstOrDefaultAsync();
                    if (result == null)
                    {
                        throw new NotFoundException("Zone does not exist");
                    }
                    result.ZoneItems = result.ZoneItems.OrderBy(x => x.Id).ToList();
                    var vm = ObjectMapper.Map<ZoneListDto>(result);
                    vm.ZoneItemDtos = ObjectMapper.Map<List<ResponseZoneItemDto>>(result.ZoneItems);
                    return vm;
                }
            }
            catch (NotFoundException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task<List<ListZoneDto>> GetAllAsync()
        {
            //var zone = await _zoneRepository.GetAllListAsync();

            //var listZoneDto = ObjectMapper.Map<List<ListZoneDto>>(zone);

            //return listZoneDto;

            try
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
                {
                    var qry = _zoneRepository.GetAll().IgnoreQueryFilters().AsQueryable();
                    var isHighPermission = false;

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewZoneWithinMdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewZoneWithinTenant, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                        }
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewOwnZone, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.UserId == AbpSession.UserId);
                        }
                    }
                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }

                    qry = qry.Include(x => x.ZoneItems).OrderByDescending(o => o.CreationTime);

                    return ObjectMapper.Map<List<ListZoneDto>>(qry);
                }
            }
            catch (ForbiddenException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task<List<ListZoneDto>> GetAllByUserIdAsync(int userId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                CancellationTokenSource source = new CancellationTokenSource();
                CancellationToken cancellationToken = source.Token;

                var user = _userManager.Users.Where(c => c.Id == userId).FirstOrDefault();

                if (user != null)
                {
                    var zone = await _zoneRepository.GetAll().IgnoreQueryFilters().Where(c => c.UserId == user.Id).ToListAsync();

                    var listZoneDto = ObjectMapper.Map<List<ListZoneDto>>(zone);

                    return listZoneDto;
                }
                else
                {
                    throw new UserFriendlyException("Invalid user!");
                }
            }
        }

        public async Task<List<ListZoneDto>> GetAllByTenantIdAsync(int tenantId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var zone = await _zoneRepository.GetAll().IgnoreQueryFilters().Where(c => c.TenantId == tenantId).ToListAsync();

                var listZoneDto = ObjectMapper.Map<List<ListZoneDto>>(zone);

                return listZoneDto;
            }
        }

        public async Task<List<ListZoneDto>> GetAllByCurrentUserSessionAsync()
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var zone = await _zoneRepository.GetAll().IgnoreQueryFilters().Where(c => c.TenantId == AbpSession.TenantId).ToListAsync();

                var listZoneDto = ObjectMapper.Map<List<ListZoneDto>>(zone);

                return listZoneDto;
            }
        }
    }
}