﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Zones.Dto;

namespace Vtmis.WebAdmin.Zones
{
    public interface IZoneService
    {
        Task<CreateZoneDto> Create(CreateZoneDto m);
        Task<UpdateZoneDto> Update(UpdateZoneDto m);
        Task Delete(int id);
        Task<ZoneListDto> GetById(int id);
        Task<List<ListZoneDto>> GetAllByUserIdAsync(int userId);
        Task<List<ListZoneDto>> GetAllByTenantIdAsync(int tenantId);
        Task<List<ListZoneDto>> GetAllByCurrentUserSessionAsync();
        Task<List<ListZoneDto>> GetAllAsync();
    }
}
