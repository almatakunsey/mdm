﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Vtmis.WebAdmin.Locations.Dto;
using System.Collections.Generic;
using Abp.Application.Services;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Abp.UI;
using Abp.Authorization;
using Abp.Domain.Uow;
using System.Threading;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Common.Constants;
using Vtmis.WebAdmin.Permissions;

namespace Vtmis.WebAdmin.Locations
{
    [AbpAuthorize]
    public class LocationAppService : ApplicationService, ILocationAppService
    {
        private readonly ILocationManager _locationManager;
        private readonly IRepository<Location, Guid> _repository;
        private readonly UserManager _userManager;
        private readonly IMdmPermissionService _mdmPermission;

        public LocationAppService(
                ILocationManager locationManager,
                IRepository<Location, Guid> repository, UserManager userManager, IMdmPermissionService mdmPermission)
        {
            _repository = repository;
            _locationManager = locationManager;
            _userManager = userManager;
            _mdmPermission = mdmPermission;
        }

        public async Task CreateAsync(CreateLocationDto input)
        {
            //Location location;
            ////if (AbpSession.TenantId == null)
            ////{
            ////    location = Location.Create(-1, input.Name, input.Latitude, input.Longitude);
            ////}
            ////else
            ////{
            ////    location = Location.Create(AbpSession.TenantId.Value, input.Name, input.Latitude, input.Longitude);
            ////}
            //location = Location.Create(AbpSession.TenantId, input.Name, input.Latitude, input.Longitude, input.ZoomLevel);
            ////var location = Location.Create(1, input.Name, input.Latitude, input.Longitude); // Temporary for Demo Sprint 14 only
            //await _locationManager.CreateAsync(location);

            if ((await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.CreateLocation, true)) == false)
            {
                throw new ForbiddenException("Permission Denied");
            }
            try
            {
                var isLocationNameExist = _repository.GetAll().IgnoreQueryFilters().Where(x => string.Equals(x.Name, input.Name, StringComparison.CurrentCultureIgnoreCase)
                                               && x.IsDeleted == false && x.UserId == AbpSession.UserId).Any();

                if (isLocationNameExist)
                {
                    throw new AlreadyExistException($"Location {input.Name} already exist.");
                }

                var location = ObjectMapper.Map<Location>(input);
                location.SetCurrentUser(AbpSession.TenantId, AbpSession.UserId);
                await _repository.InsertAsync(location);
            }
            catch (ForbiddenException err)
            {
                throw err;
            }
            catch (AlreadyExistException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task DeleteAsync(Guid id)
        {
            //var result = await _repository.FirstOrDefaultAsync(c => c.Id == id);
            //if (result == null)
            //{
            //    throw new UserFriendlyException("Could not found the location, maybe it's deleted.");
            //}
            //await _repository.DeleteAsync(id);

            try
            {
                var isExist = await _repository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
                if (isExist == null)
                {
                    throw new NotFoundException("Location Does Not Exist");
                }

                var isHighPermission = false;

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteLocationWithinMdm, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteLocationWithinTenant, true))
                {
                    isHighPermission = true;
                    if (isExist.TenantId != AbpSession.TenantId && isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteOwnLocation, true))
                {
                    isHighPermission = true;
                    if (isExist.UserId != AbpSession.UserId && isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }

                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }

                isExist.DeleterUserId = AbpSession.UserId;
                isExist.DeletionTime = DateTime.Now;
                isExist.IsDeleted = true;
                _repository.Update(isExist);
            }
            catch (NotFoundException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task UpdateAsync(UpdateLocationDto input)
        {
            //var result = await _repository.FirstOrDefaultAsync(c => c.Id == input.Id);
            //if (result == null)
            //{
            //    throw new UserFriendlyException("Could not found the location, maybe it's deleted.");
            //}
            //result = ObjectMapper.Map(input, result);
            //await _repository.UpdateAsync(result);

            try
            {
                var existingLocation = await _repository.GetAll().IgnoreQueryFilters()
                                            .Where(x => x.Id == input.Id && x.IsDeleted == false).FirstOrDefaultAsync();



                if (existingLocation == null)
                {
                    throw new NotFoundException($"Location does not exist");
                }
                else
                {
                    var othersFilters = _repository.GetAll().IgnoreQueryFilters().Where(x => x.Name.ToLower() == input.Name.ToLower() &&
                                                x.Id != input.Id && x.IsDeleted == false && x.UserId == AbpSession.UserId).Any();
                    if (othersFilters)
                    {
                        throw new AlreadyExistException($"Location {input.Name} already exist.");
                    }
                }

                var filter = ObjectMapper.Map(input, existingLocation);
                var isHighPermission = false;

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateLocationWithinMdm, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateLocationWithinTenant, true))
                {
                    isHighPermission = true;
                    if (filter.TenantId != AbpSession.TenantId && isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateOwnLocation, true))
                {
                    isHighPermission = true;
                    if (filter.UserId != AbpSession.UserId && isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                }
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
                await _repository.UpdateAsync(filter);
            }
            catch (AlreadyExistException err)
            {
                throw err;
            }
            catch (NotFoundException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task<LocationDto> GetDetailAsync(EntityDto<Guid> input)
        {
            //var location = await _repository
            //    .GetAll()
            //    .Where(r => r.Id == input.Id)
            //    .FirstOrDefaultAsync();

            //if (location == null)
            //{
            //    throw new UserFriendlyException("Could not found the location, maybe it's deleted.");
            //}
            //return ObjectMapper.Map<LocationDto>(location);

            try
            {
                var isHighPermission = false;
                var qry = _repository.GetAll().IgnoreQueryFilters().AsQueryable();

                //Check permissions
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewLocationWithinMdm, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewLocationWithinTenant, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                    }
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewOwnLocation, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        qry = qry.Where(x => x.UserId == AbpSession.UserId);
                    }
                }
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
                qry = qry.Where(x => x.Id == input.Id && x.IsDeleted == false);

                var result = await qry.FirstOrDefaultAsync();
                if (result == null)
                {
                    throw new NotFoundException("Location does not exist");
                }
                return ObjectMapper.Map<LocationDto>(result);
            }
            catch (NotFoundException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task<ListResultDto<LocationDto>> GetListAsync()
        {
            //var locations = await _repository
            //    .GetAll()
            //    .ToListAsync();

            //return new ListResultDto<LocationDto>(ObjectMapper.Map<List<LocationDto>>(locations));

            try
            {
                var qry = _repository.GetAll().IgnoreQueryFilters().AsQueryable();
                var isHighPermission = false;

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewLocationWithinMdm, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewLocationWithinTenant, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                    }
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewOwnLocation, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        qry = qry.Where(x => x.UserId == AbpSession.UserId);
                    }
                }
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
                qry = qry.Where(x => x.IsDeleted == false).OrderByDescending(o => o.CreationTime);

                return new ListResultDto<LocationDto>(ObjectMapper.Map<List<LocationDto>>(qry));
            }
            catch (ForbiddenException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        public async Task<ListResultDto<LocationDto>> GetListByTenantIdAsync(int tenantId)
        {
            var locations = await _repository
                .GetAll().IgnoreQueryFilters().Where(x => x.TenantId == tenantId && x.IsDeleted == false)
                .ToListAsync();

            return new ListResultDto<LocationDto>(ObjectMapper.Map<List<LocationDto>>(locations));
        }

        public async Task<ListResultDto<LocationDto>> GetListByUserIdAsync(int userId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                CancellationTokenSource source = new CancellationTokenSource();
                CancellationToken cancellationToken = source.Token;

                var user = _userManager.Users.Where(c => c.Id == userId).FirstOrDefault();

                if (user != null)
                {
                    var locations = await _repository.GetAll().IgnoreQueryFilters()
                        .Where(x => x.UserId == userId && x.IsDeleted == false).ToListAsync();

                    return new ListResultDto<LocationDto>(ObjectMapper.Map<List<LocationDto>>(locations));
                }
                else
                {
                    throw new UserFriendlyException("Invalid user!");
                }
            }
        }

        public async Task<ListResultDto<LocationDto>> GetListByCurrentUserSessionAsync()
        {
            List<Location> locations;
            if (AbpSession.TenantId == null)
            {
                // HOST ADMIN RETURN ALL LOCATION
                locations = await _repository
               .GetAll().IgnoreQueryFilters().Where(x => x.IsDeleted == false).ToListAsync();
            }
            else
            {
                // RETURN TENANT LOCATION AND HOST LOCATION
                locations = await _repository
                .GetAll().IgnoreQueryFilters().Where(x => x.TenantId == AbpSession.TenantId && x.IsDeleted == false)
                .ToListAsync();
            }

            return new ListResultDto<LocationDto>(ObjectMapper.Map<List<LocationDto>>(locations));
        }
    }
}
