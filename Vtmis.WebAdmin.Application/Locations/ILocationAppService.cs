﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Locations.Dto;

namespace Vtmis.WebAdmin.Locations
{
    public interface ILocationAppService : IApplicationService
    {
        Task<ListResultDto<LocationDto>> GetListAsync();
        Task<ListResultDto<LocationDto>> GetListByTenantIdAsync(int tenantId);
        Task<ListResultDto<LocationDto>> GetListByUserIdAsync(int userId);
        Task<ListResultDto<LocationDto>> GetListByCurrentUserSessionAsync();
        Task<LocationDto> GetDetailAsync(EntityDto<Guid> input);

        Task CreateAsync(CreateLocationDto input);
        Task DeleteAsync(Guid id);
        Task UpdateAsync(UpdateLocationDto input);
    }
}
