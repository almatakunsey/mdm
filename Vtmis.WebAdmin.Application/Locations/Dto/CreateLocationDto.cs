﻿using System.ComponentModel.DataAnnotations;
using Abp.AutoMapper;

namespace Vtmis.WebAdmin.Locations.Dto
{
    [AutoMapFrom(typeof(Location))]
    public class CreateLocationDto
    {
        [Required]
        public string Name { get; set; }

        public string Longitude { get; set; }

        public string Latitude { get; set; }
        public int ZoomLevel { get; set; }
    }
}
