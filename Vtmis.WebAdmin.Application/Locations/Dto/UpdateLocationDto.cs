﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Locations.Dto
{
    [AutoMapFrom(typeof(Location))]
    public class UpdateLocationDto
    {
      
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }

        public string Longitude { get; set; }

        public string Latitude { get; set; }
        public int ZoomLevel { get; set; }
    }
}
