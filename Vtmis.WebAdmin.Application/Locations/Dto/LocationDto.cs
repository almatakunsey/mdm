﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;

namespace Vtmis.WebAdmin.Locations.Dto
{
    [AutoMapFrom(typeof(Location))]
    public class LocationDto : FullAuditedEntityDto<Guid>
    {
        public int? TenantId { get; set; }

        [Required]
        public string Name { get; set; }

        public string Longitude { get; set; }

        public string Latitude { get; set; }
        public int ZoomLevel { get; set; }
    }
}
