﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Model;
using Vtmis.Core.Model.ViewModels.Route.Dto;
using Vtmis.WebAdmin.Permissions;

namespace Vtmis.WebAdmin.Routes
{
    [RemoteService(IsMetadataEnabled = false)]
    public class RouteService : ApplicationService, IRouteService
    {
        private readonly IRepository<Route> _routeRepo;
        private readonly IRepository<Waypoint> _waypointRepo;
        private readonly IMdmPermissionService _mdmPermission;

        //private readonly IHostingEnvironment _env;

        public RouteService(IRepository<Route> routeRepo, IRepository<Waypoint> waypointRepo, IMdmPermissionService mdmPermission)
        {
            _routeRepo = routeRepo;
            _waypointRepo = waypointRepo;
            _mdmPermission = mdmPermission;
            //_env = env;
        }

        public async Task<ResponseRouteDto> CreateAsync(RequestCreateRoute input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    if ((await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_Create, true)) == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }

                    var isRouteNameExist = await _routeRepo.GetAll().IgnoreQueryFilters().AnyAsync(x =>
                        x.IsDeleted == false && x.UserId == AbpSession.UserId &&
                        string.Equals(x.Name, input.Name, StringComparison.CurrentCultureIgnoreCase));

                    if (isRouteNameExist)
                    {
                        throw new AlreadyExistException("Route Name Already Exist");
                    }

                    var route = ObjectMapper.Map<Route>(input);
                    route.UserId = AbpSession.UserId.Value;
                    route.TenantId = AbpSession.TenantId;
                    route.CreatorUserId = AbpSession.UserId;

                    var result = await _routeRepo.InsertAsync(route);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    //var waypointResults = CalculateWaypointDistance(result.Waypoints);
                    var r = ObjectMapper.Map<ResponseRouteDto>(result);
                    //r.Waypoints = waypointResults;
                    return r;
                }
                catch (AlreadyExistException err)
                {
                    throw new AlreadyExistException(err.Message);
                }
                catch (ForbiddenException err)
                {
                    throw new ForbiddenException(err.Message);
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err);
                }
            }
        }

        //private List<WaypointDto> CalculateWaypointDistance(List<Waypoint> waypoints)
        //{
        //    if (waypoints.Any())
        //    {
        //        var distanceList = new List<WaypointDto>();
        //        double preserveDistance = 0;
        //        for (int i = 0; i < waypoints.Count; i++)
        //        {
        //            var nextPoint = new PointF();
        //            var isNextPointVisible = true;
        //            if (i >= (waypoints.Count - 1))
        //            {
        //                nextPoint.X = (float)waypoints[i - 1].Latitude;
        //                nextPoint.Y = (float)waypoints[i - 1].Longitude;
        //            }
        //            else
        //            {
        //                nextPoint.X = (float)waypoints[i + 1].Latitude;
        //                nextPoint.Y = (float)waypoints[i + 1].Longitude;
        //                isNextPointVisible = waypoints[i + 1].IsShown;
        //            }
        //            var distance = CommonHelper.GetDistanceBetweenTwoCoordinates(
        //                new PointF
        //                {
        //                    X = (float)waypoints[i].Latitude,
        //                    Y = (float)waypoints[i].Longitude
        //                },
        //               nextPoint,
        //                Unit.NauticalMiles
        //            );
        //            preserveDistance = preserveDistance + distance;
        //            if (isNextPointVisible == true)
        //            {
        //                var d = ObjectMapper.Map<WaypointDto>(waypoints[i]);
        //                d.WpDistance = preserveDistance;
        //                distanceList.Add(d);
        //                preserveDistance = 0;
        //            }
        //        }
        //    }
        //    return new List<WaypointDto>();
        //}

        public async Task<PaginatedList<ResponseRouteDto>> GetAllAsync(int pageIndex = 1, int pageSize = 10)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var qry = _routeRepo.GetAll().IgnoreQueryFilters().Include(x => x.Waypoints).AsQueryable();
                    var isHighPermission = false;

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_View_Mdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_View_Tenant, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                        }
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_View_Own, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.UserId == AbpSession.UserId);
                        }
                    }

                    qry = qry.Where(x => x.IsDeleted == false);
                    var results = await new PaginatedList<Route>().CreateAsync(qry, pageIndex, pageSize);
                    return ObjectMapper.Map<PaginatedList<ResponseRouteDto>>(results);
                }
                catch (ForbiddenException err)
                {
                    throw new ForbiddenException(err.Message);
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err);
                }
            }
        }

        public async Task<ResponseRouteDto> GetAsync(int id)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var isHighPermission = false;
                    var qry = _routeRepo.GetAll().IgnoreQueryFilters().Include(x => x.Waypoints).AsQueryable();

                    //Check permissions
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_View_Mdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_View_Tenant, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                        }
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_View_Own, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.UserId == AbpSession.UserId);
                        }
                    }

                    var result = await qry.FirstOrDefaultAsync(x => x.IsDeleted == false && x.Id == id);
                    if (result != null)
                    {
                        return ObjectMapper.Map<ResponseRouteDto>(result);
                    }
                    throw new NotFoundException("Route does not exist");
                }
                catch (NotFoundException err)
                {
                    throw new NotFoundException(err.Message);
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err);
                }
            }
        }

        public async Task<ResponseRouteDto> UpdateAsync(RequestUpdateRouteDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var existingRoute = await _routeRepo.GetAll().IgnoreQueryFilters().Include(i => i.Waypoints)
                                                .FirstOrDefaultAsync(x => x.Id == input.Id && x.IsDeleted == false);

                    if (existingRoute == null)
                    {
                        throw new NotFoundException($"Route does not exist");
                    }
                    else
                    {
                        var otherRoute = await _routeRepo.GetAll().IgnoreQueryFilters().Where(x => x.Name.ToLower() == input.Name.ToLower() &&
                                                    x.Id != input.Id && x.IsDeleted == false && x.UserId == AbpSession.UserId).AnyAsync();
                        if (otherRoute)
                        {
                            throw new AlreadyExistException($"Route {input.Name} already exist.");
                        }
                    }
                    existingRoute.Waypoints.Clear();
                    await _routeRepo.UpdateAsync(existingRoute);
                    await CurrentUnitOfWork.SaveChangesAsync();

                    var route = ObjectMapper.Map(input, existingRoute);
                    var isHighPermission = false;

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_Update_Mdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_Update_Tenant, true))
                    {
                        isHighPermission = true;
                        if (route.TenantId != AbpSession.TenantId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_Update_Own, true))
                    {
                        isHighPermission = true;
                        if (route.UserId != AbpSession.UserId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }
                    var waypoints = route.Waypoints.ToList();
                    route.Waypoints.Clear();
                    var result = _routeRepo.Update(route);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    foreach (var waypoint in waypoints)
                    {
                        await _waypointRepo.InsertAsync(new Waypoint
                        {
                            Latitude = waypoint.Latitude,
                            Longitude = waypoint.Longitude,
                            AverageSpeed = waypoint.AverageSpeed,
                            IsShown = waypoint.IsShown,
                            MaxSpeed = waypoint.MaxSpeed,
                            MinSpeed = waypoint.MinSpeed,
                            Name = waypoint.Name,
                            RouteId = result.Id,
                            XteLeft = waypoint.XteLeft,
                            XteRight = waypoint.XteRight
                        });
                    }
                    await CurrentUnitOfWork.SaveChangesAsync();
                    result.Waypoints = waypoints;
                    return ObjectMapper.Map<ResponseRouteDto>(result);
                }
                catch (AlreadyExistException err)
                {
                    throw err;
                }
                catch (NotFoundException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }
            }
        }

        public async Task DeleteAsync(int routeId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var isExist = await _routeRepo.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == routeId && x.IsDeleted == false);
                    if (isExist == null)
                    {
                        throw new NotFoundException("Route Does Not Exist");
                    }

                    var isHighPermission = false;

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_Delete_Mdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_Delete_Tenant, true))
                    {
                        isHighPermission = true;
                        if (isExist.TenantId != AbpSession.TenantId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Route_Delete_Own, true))
                    {
                        isHighPermission = true;
                        if (isExist.UserId != AbpSession.UserId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    isExist.DeleterUserId = AbpSession.UserId;
                    isExist.DeletionTime = DateTime.Now;
                    isExist.IsDeleted = true;
                    _routeRepo.Update(isExist);

                    await CurrentUnitOfWork.SaveChangesAsync();
                }
                catch (NotFoundException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }
            }
        }
    }
}

