﻿using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.Core.Model.ViewModels.Route.Dto;

namespace Vtmis.WebAdmin.Routes
{
    public interface IRouteService
    {
        Task<ResponseRouteDto> CreateAsync(RequestCreateRoute input);
        Task<PaginatedList<ResponseRouteDto>> GetAllAsync(int pageIndex = 1, int pageSize = 10);
        Task<ResponseRouteDto> GetAsync(int id);
        Task<ResponseRouteDto> UpdateAsync(RequestUpdateRouteDto input);
        Task DeleteAsync(int routeId);
    }
}