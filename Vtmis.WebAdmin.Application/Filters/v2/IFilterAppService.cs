﻿using Abp.Application.Services;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Filters.v2.Dto;

namespace Vtmis.WebAdmin.Filters
{
    public interface IFilterAppService : IApplicationService
    {
        Task<PaginatedList<FilterResponseDto>> GetAllAsync(bool includeDeletedFilters = false, bool includeFilterItems = false, bool includeFilterDetails = false,
                    int pageIndex = 1, int pageSize = 10);
        Task<FilterResponseDto> GetByIdAsync(int filterId, bool includeFilterItems = false, bool includeFilterDetails = false, bool includeDeletedFilter = false);
        Task<FilterResponseDto> CreateAsync(CreateFilterDto input);
        Task<FilterResponseDto> UpdateAsync(UpdateFilterDto input);
        Task DeleteAsync(int filterId, bool isHardDelete = false);
    }


}
