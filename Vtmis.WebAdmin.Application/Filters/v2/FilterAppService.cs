﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Filters.v2.Dto;
using Vtmis.WebAdmin.Permissions;

namespace Vtmis.WebAdmin.Filters
{
    //[Route("/api/filters")]
    [RemoteService(IsMetadataEnabled = false)]
    public class FilterAppService : ApplicationService, IFilterAppService
    {
        private readonly IRepository<Filter> _filterRepository;
        private readonly IMdmPermissionService _mdmPermission;

        public FilterAppService(IRepository<Filter> filterRepository, IMdmPermissionService mdmPermission)
        {
            _filterRepository = filterRepository;
            _mdmPermission = mdmPermission;
        }


        public async Task<PaginatedList<FilterResponseDto>> GetAllAsync(bool includeDeletedFilters = false, bool includeFilterItems = false, bool includeFilterDetails = false,
                    int pageIndex = 1, int pageSize = 10)
        {

            try
            {
                IQueryable<Filter> qry = _filterRepository.GetAll().IgnoreQueryFilters().AsQueryable();
                var isHighPermission = false;

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewFilterWithinMdm, true))
                {
                    isHighPermission = true;
                }
                if (isHighPermission == false)
                {
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewFilterWithinTenant, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;

                            qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                        }
                    }
                }
                if (isHighPermission == false)
                {
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewOwnFilter, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.UserId == AbpSession.UserId);
                        }
                    }
                }



                if (includeFilterItems)
                {
                    qry = qry.Include("FilterItems");
                }
                if (includeFilterDetails)
                {
                    qry = qry.Include("FilterItems.FilterDetails");
                }
                if (includeDeletedFilters == false)
                {
                    qry = qry.Where(x => x.IsDeleted == false);
                }

                qry = qry.OrderByDescending(o => o.CreationTime);

                var result = new PaginatedList<Filter>().CreateTemp(qry,
                           pageIndex, pageSize);

                return ObjectMapper.Map<PaginatedList<FilterResponseDto>>(result);
            }
            catch (ForbiddenException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }


        }


        public async Task<FilterResponseDto> GetByIdAsync(int filterId, bool includeFilterItems = false, bool includeFilterDetails = false,
            bool includeDeletedFilter = false)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {

                try
                {
                    var isHighPermission = false;
                    var qry = _filterRepository.GetAll().IgnoreQueryFilters().AsQueryable();

                    //Check permissions
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewFilterWithinMdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewFilterWithinTenant, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                        }
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.ViewOwnFilter, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.UserId == AbpSession.UserId);
                        }
                    }

                    if (includeFilterItems)
                    {
                        qry = qry.Include("FilterItems");
                    }
                    if (includeFilterDetails)
                    {
                        qry = qry.Include("FilterItems.FilterDetails");
                    }
                    if (includeDeletedFilter == false)
                    {
                        qry = qry.Where(x => x.IsDeleted == includeDeletedFilter);
                    }

                    qry = qry.Where(x => x.Id == filterId);

                    var result = await qry.FirstOrDefaultAsync();
                    if (result == null)
                    {
                        throw new NotFoundException("Filter does not exist");
                    }
                    return ObjectMapper.Map<FilterResponseDto>(result);
                }
                catch (NotFoundException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }

            }
        }


        public async Task<FilterResponseDto> CreateAsync(CreateFilterDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant,
                AbpDataFilters.MustHaveTenant))
            {

                if ((await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.CreateFilter, true)) == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }
                try
                {
                    var isFilterNameExist = _filterRepository.GetAll().IgnoreQueryFilters().Where(x => x.Name.ToLower() ==
                                    input.Name.ToLower() && x.UserId == AbpSession.UserId && x.IsDeleted == false).Any();

                    if (isFilterNameExist)
                    {
                        throw new AlreadyExistException($"Filter {input.Name} already exist.");
                    }

                    var filter = ObjectMapper.Map<Filter>(input);
                    filter.SetCurrentUser(AbpSession.TenantId, AbpSession.UserId);
                    var result = ObjectMapper.Map<FilterResponseDto>(await
                        _filterRepository.InsertAsync(filter));
                    await CurrentUnitOfWork.SaveChangesAsync();
                    return result;
                }
                catch (ForbiddenException err)
                {
                    throw err;
                }
                catch (AlreadyExistException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }

            }
        }

        public async Task<FilterResponseDto> UpdateAsync(UpdateFilterDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {

                try
                {
                    var existingFilter = await _filterRepository.GetAll().IgnoreQueryFilters().Include("FilterItems")
                                                .Include("FilterItems.FilterDetails")
                                                .Where(x => x.Id == input.Id && x.IsDeleted == false).FirstOrDefaultAsync();



                    if (existingFilter == null)
                    {
                        throw new NotFoundException($"Filter does not exist");
                    }
                    else
                    {
                        var othersFilters = _filterRepository.GetAll().IgnoreQueryFilters().Where(x => x.Name.ToLower() == input.Name.ToLower() &&
                                                    x.Id != input.Id && x.IsDeleted == false && x.UserId == AbpSession.UserId).Any();
                        if (othersFilters)
                        {
                            throw new AlreadyExistException($"Filter {input.Name} already exist.");
                        }
                    }

                    var filter = ObjectMapper.Map(input, existingFilter);
                    var isHighPermission = false;

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateFilterWithinMdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateFilterWithinTenant, true))
                    {
                        isHighPermission = true;
                        if (filter.TenantId != AbpSession.TenantId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.UpdateOwnFilter, true))
                    {
                        isHighPermission = true;
                        if (filter.UserId != AbpSession.UserId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    var result = _filterRepository.Update(filter);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    return ObjectMapper.Map<FilterResponseDto>(result);
                }
                catch (AlreadyExistException err)
                {
                    throw err;
                }
                catch (NotFoundException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }
            }
        }

        //Cannot hard delete
        public async Task DeleteAsync(int filterId, bool isHardDelete = false)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var isExist = await _filterRepository.GetAll().IgnoreQueryFilters().FirstOrDefaultAsync(x => x.Id == filterId && x.IsDeleted == false);
                    if (isExist == null)
                    {
                        throw new NotFoundException("Filter Does Not Exist");
                    }

                    var isHighPermission = false;

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteFilterWithinMdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteFilterWithinTenant, true))
                    {
                        isHighPermission = true;
                        if (isExist.TenantId != AbpSession.TenantId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.DeleteOwnFilter, true))
                    {
                        isHighPermission = true;
                        if (isExist.UserId != AbpSession.UserId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (isHardDelete)
                    {
                        _filterRepository.Delete(isExist);
                    }
                    else
                    {
                        isExist.DeleterUserId = AbpSession.UserId;
                        isExist.DeletionTime = DateTime.Now;
                        isExist.IsDeleted = true;
                        _filterRepository.Update(isExist);
                    }
                    await CurrentUnitOfWork.SaveChangesAsync();
                    //await _dbContext.SaveChangesAsync();
                }
                catch (NotFoundException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }
            }
        }

        //public async Task<FilterResponse> Create(FilterRequest m)
        //{
        //    //Parameter Contract

        //    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
        //    {
        //        var user = _userManager.Users.Where(c => c.Id == m.UserId).FirstOrDefault();

        //        if (user != null)
        //        {
        //            var filter = await this._filterRepository.FirstOrDefaultAsync(c => c.Name == m.Name && c.UserId == user.Id);

        //            if (filter == null)
        //            {
        //                var filterItems = new List<FilterItem>();

        //                foreach(var _filterItem in m.FilterItemRequests)
        //                {
        //                    var filterName = FilterItem.CreateFilterItem(_filterItem.AndOrOperator, _filterItem.Name, _filterItem.Operator, _filterItem.Value,_filterItem.Color,_filterItem.BorderColor);
        //                    filterItems.Add(filterName);
        //                }


        //                filter = Filter.Create(m.Name,m.IsEable,m.IsShowShips, user,filterItems);

        //                var _returnFilterId =await _filterRepository.InsertOrUpdateAndGetIdAsync(filter);

        //                return new FilterResponse { Id = _returnFilterId, Name = filter.Name};
        //            }
        //            throw new UserFriendlyException("Save filter error", "Filter with same name already exist");
        //        }

        //        throw new UserFriendlyException("Get user error", "User does not exist");

        //    }
        //}

        //public async Task<List<FilterByNameResponse>> GetAllAsync(int tenantId)
        //{
        //    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
        //    {
        //        var filters = await _filterRepository.GetAllListAsync(c => c.TenantId == tenantId);

        //        if (filters != null)
        //        {
        //            return filters.Select(c => new FilterByNameResponse
        //            {
        //                Id = c.Id,
        //                Name = c.Name,
        //                IsEnable = c.IsEnable,
        //                IsShowShips = c.IsShowShips,
        //                UserId = c.UserId,
        //                Username = c.User?.UserName

        //            }).ToList();
        //        }
        //        throw new Exception($"Your query filter with TenantId {tenantId} does not exist. Please contact Administrator");
        //    }
        //}

        //public async Task<List<FilterByNameResponse>> GetFilterByName(FilterByNameRequest m)
        //{
        //    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
        //    {
        //        var user = _userManager.Users.Where(c => c.Id == m.UserId).FirstOrDefault();
        //        if (user != null)
        //        {
        //            var filters = await _filterRepository.GetAllListAsync(c => c.Name == m.FilterName && c.TenantId == user.TenantId);

        //            if(filters!=null)
        //            {
        //                return filters.Select(c => new FilterByNameResponse
        //                {
        //                    Id = c.Id,
        //                    Name = c.Name,
        //                    IsEnable = c.IsEnable,
        //                    IsShowShips = c.IsShowShips,
        //                    UserId = c.UserId,
        //                    Username = c.User?.UserName
        //                    //FilterItemResponses = c.FilterItems.Select(f => new FilterItemResponse { AndOrOperator = f.ColumnAndOr, Name = f.ColumnName, Operator = f.ColumnOpr, Value = f.ColumnValue }).ToList()
        //                }).ToList();
        //            }

        //            throw new Exception("Error when query filter");
        //        }

        //        throw new Exception("Get user error - User doesnt exist");
        //    }
        //}

        //public async Task<List<FilterByUserIdResponse>> GetFilterByUserId(FilterByUserIdRequest m)
        //{
        //    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
        //    {
        //        var user = _userManager.Users.Where(c => c.Id == m.UserId).FirstOrDefault();
        //        if (user != null)
        //        {
        //            var filters = await _filterRepository.GetAllListAsync(c => c.UserId ==user.Id && c.TenantId == user.TenantId);

        //            if (filters != null)
        //            {
        //                return filters.Select(c => new FilterByUserIdResponse
        //                {
        //                    Id = c.Id,
        //                    Name = c.Name,
        //                    IsEnable = c.IsEnable,
        //                    IsShowShips = c.IsShowShips,
        //                    UserId = c.UserId,
        //                    Username = c.User?.UserName

        //                }).ToList();
        //            }

        //            throw new Exception("Error when query filter");
        //        }

        //        throw new Exception("Get user error - User doesnt exist");
        //    }
        //}

        //public async Task<FilterDetailByUserIdResponse> GetFilterDetailByUserId(FilterDetailByUserIdRequest m)
        //{
        //    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
        //    {
        //        var user = _userManager.Users.Where(c => c.Id == m.UserId).FirstOrDefault();
        //        if (user != null)
        //        {
        //            var filters = await _filterRepository.GetAllListAsync(c => c.Name == m.FilterName && c.TenantId == user.TenantId);


        //            if (filters != null)
        //            {
        //                return filters.Select(c => {
        //                    var f = new FilterDetailByUserIdResponse(c, _filterRepository)
        //                    {
        //                        Id = c.Id,
        //                        Name = c.Name,
        //                        IsEnable = c.IsEnable,
        //                        IsShowShips = c.IsShowShips,
        //                        UserId = c.UserId,
        //                        Username = c.User?.UserName//,
        //                       // FilterItemResponses = Where(c.FilterItems)
        //                    };
        //                    f.FilterItemResponses =  Where(c.FilterItems, f.ReturnVesselFilters);
        //                    return f;
        //                }).FirstOrDefault();
        //            }

        //            throw new Exception("Error when query filter");
        //        }

        //        throw new Exception("Get user error - User doesnt exist");
        //    }
        //}

        //private List<FilterItemResponse> Where(ICollection<FilterItem> filterItems,List<VesselColor> vesselColors)
        //{
        //    List<FilterItemResponse> filterItemResponses = new List<FilterItemResponse>();

        //    foreach (var fi in filterItems)
        //    {
        //        FilterItemResponse fir = new FilterItemResponse
        //        {
        //            AndOrOperator = fi.ColumnAndOr,
        //            Name = fi.ColumnName,
        //            Operator = fi.ColumnOpr,
        //            Value = fi.ColumnValue,
        //            Color = fi.ColumnColor,
        //            BorderColor = fi.ColumnBorderColor
        //        };

        //        if (fi.ColumnName.Contains("Group"))
        //        {

        //            fir.GroupResponses = _filterGroupRepository.GetAllList(c => c.Name == fi.ColumnValue)
        //                .Select(c => new GroupResponse { MMSIs = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(c.MMSIList) }).ToList();
        //        }

        //        //filterItemResponses.Query(fir);
        //        filterItemResponses.Add(fir);
        //    }

        //    //var url = $"{_apiMapTracking}/api/VesselFilters/GetFilterSpecification";
        //    //try
        //    //{
        //    //    var vesselColorResults = await url.PostJsonAsync(filterItemResponses);//.ReceiveJson<List<VesselColor>>();
        //    //}
        //    //catch(Exception err)
        //    //{
        //    //    Console.WriteLine(err.ToString());
        //    //}
        //    //vesselColors.AddRange(vesselColorResults);

        //    return filterItemResponses;
        //}

        //public async Task<FilterResponse> Update(UpdateFilter m)
        //{
        //    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
        //    {
        //        var filter = await _filterRepository.GetAsync(m.FilterId);
        //        filter.Name = m.Name;
        //        filter.IsEnable = m.IsEable;
        //        filter.IsShowShips = m.IsShowShips;
        //        filter.FilterItems.Clear();

        //        var filterItems = new List<FilterItem>();

        //        foreach (var _filterItem in m.FilterItemRequests)
        //        {
        //            var filterName = FilterItem.CreateFilterItem(_filterItem.AndOrOperator, _filterItem.Name, _filterItem.Operator, _filterItem.Value, _filterItem.Color, _filterItem.BorderColor);
        //            filterItems.Add(filterName);
        //        }

        //        var _returnFilterId = await _filterRepository.InsertOrUpdateAndGetIdAsync(filter);

        //        return new FilterResponse { Id = _returnFilterId, Name = filter.Name };
        //    }

        //}

        //public async Task Delete(int filterId)
        //{
        //    using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
        //    {
        //        var filter = await _filterRepository.GetAsync(filterId);

        //        await _filterRepository.DeleteAsync(filter);
        //    }

        //}


    }
}
