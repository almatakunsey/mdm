﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.Filters.v2.Dto
{
    public class FilterItemResponseDto : EntityDto
    {
        public FilterItemResponseDto()
        {
            FilterDetails = new List<FilterDetailResponseDto>();
        }
        public string Colour { get; set; }
        public string BorderColour { get; set; }
        public List<FilterDetailResponseDto> FilterDetails { get; set; }
    }
}
