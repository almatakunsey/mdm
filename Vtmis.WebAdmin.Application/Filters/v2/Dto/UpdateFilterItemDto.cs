﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Filters.v2.Dto
{
    public class UpdateFilterItemDto : EntityDto
    {
        public UpdateFilterItemDto()
        {
            FilterDetails = new List<UpdateFilterDetailDto>();
        }
        public string Colour { get; set; }
        public string BorderColour { get; set; }
        [Required]
        public List<UpdateFilterDetailDto> FilterDetails { get; set; }
    }
}
