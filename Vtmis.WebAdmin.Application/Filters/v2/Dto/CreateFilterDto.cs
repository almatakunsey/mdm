﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Filters.v2.Dto
{
    public class CreateFilterDto
    {
        public CreateFilterDto()
        {
            FilterItems = new List<CreateFilterItemDto>();
        }
        [Required]
        public string Name { get; set; }
        public bool IsEnable { get; set; }
        public bool IsShowShips { get; set; }
        [Required]
        public virtual ICollection<CreateFilterItemDto> FilterItems { get; set; }
    }
}
