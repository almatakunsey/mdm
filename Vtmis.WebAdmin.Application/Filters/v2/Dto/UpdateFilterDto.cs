﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Filters.v2.Dto
{
    public class UpdateFilterDto : FullAuditedEntityDto<int>
    {
        public UpdateFilterDto()
        {
            FilterItems = new List<UpdateFilterItemDto>();
        }
        [Required]
        public virtual ICollection<UpdateFilterItemDto> FilterItems { get; set; }
        [Required]
        public string Name { get; set; }

        public bool IsEnable { get; set; }
        public bool IsShowShips { get; set; }
    }
}
