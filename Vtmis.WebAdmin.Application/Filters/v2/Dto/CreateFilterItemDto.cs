﻿using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Filters.v2.Dto
{
    public class CreateFilterItemDto
    {
        public CreateFilterItemDto()
        {
            FilterDetails = new List<CreateFilterDetailDto>();
        }
        public string Colour { get; set; }
        public string BorderColour { get; set; }
        [Required]
        public List<CreateFilterDetailDto> FilterDetails { get; set; }
    }
}
