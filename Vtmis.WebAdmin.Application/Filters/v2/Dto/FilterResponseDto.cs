﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.Filters.v2.Dto
{
    public class FilterResponseDto : FullAuditedEntityDto<int>
    {
        public FilterResponseDto()
        {
            FilterItems = new List<FilterItemResponseDto>();
        }
        public virtual ICollection<FilterItemResponseDto> FilterItems { get; set; }

        public string Name { get; set; }

        public bool IsEnable { get; set; }
        public bool IsShowShips { get; set; }
    }
}
