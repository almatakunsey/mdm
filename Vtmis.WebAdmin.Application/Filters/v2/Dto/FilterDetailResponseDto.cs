﻿using Abp.Application.Services.Dto;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Filters.v2.Dto
{
    public class FilterDetailResponseDto : EntityDto
    {
        public Condition Condition { get; set; }
        public string ColumnName { get; set; }
        public string Operation { get; set; }
        public string ColumnValue { get; set; }

        public int RowIndex { get; set; }
    }
}
