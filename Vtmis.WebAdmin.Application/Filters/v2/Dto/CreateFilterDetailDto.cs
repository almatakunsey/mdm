﻿using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Filters.v2.Dto
{
    public class CreateFilterDetailDto
    {
        public Condition Condition { get; set; }
        public string ColumnName { get; set; }
        public string Operation { get; set; }
        public string ColumnValue { get; set; }
        //public string ColumnColor { get; set; }
        //public string ColumnBorderColor { get; set; }

        public int RowIndex { get; set; }
    }
}
