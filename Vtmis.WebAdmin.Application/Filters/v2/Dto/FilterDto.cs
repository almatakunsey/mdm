﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Vtmis.WebAdmin.Filters.v2.Dto
{
    public class FilterRequest
    {
        public FilterRequest()
        {
            if (FilterItemRequests == null)
                FilterItemRequests = new List<FilterItemRequest>();
        }
        public long UserId { set; get; }

        public string Name { set; get; }

        public bool IsEable { get; set; }

        public bool IsShowShips { get; set; }

        public List<FilterItemRequest> FilterItemRequests { get; set; }
    }

    public class UpdateFilter
    {
        public UpdateFilter()
        {
            if (FilterItemRequests == null)
                FilterItemRequests = new List<FilterItemRequest>();
        }
        public int FilterId { get; set; }

        public string Name { set; get; }

        public bool IsEable { get; set; }

        public bool IsShowShips { get; set; }

        public List<FilterItemRequest> FilterItemRequests { get; set; }
    }


    public class FilterResponse
    {
        public int Id { get; set; }

        public string Name { set; get; }
    }

    public class FilterItemRequest
    {
        //public string GroupName { get; set; }
        public string Color { get; set; }

        public string BorderColor { get; set; }

        public string AndOrOperator { get; set; }

        public string Name { get; set; }

        public string Operator { get; set; }

        public string Value { get; set; }
    }

    [AutoMapFrom(typeof(FilterItem))]
    public class FilterItemResponse
    {
        public string Color { get; set; }

        public string BorderColor { get; set; }

        public string AndOrOperator { get; set; }

        public string Name { get; set; }

        public string Operator { get; set; }

        public string Value { get; set; }

        public List<VesselColor> MMSIs { get; set; }

        public List<GroupResponse> GroupResponses { get; set; }
    }

    public class VesselColor
    {
        public string Border { get; set; }
        public string BorderColor { get; set; }
        public string MMSI { get; set; }
    }

    public class GroupResponse
    {
        public List<string> MMSIs { get; set; }
        public List<string> Names { get; set; }
        public List<string> CallSigns { get; set; }
    }

    public class FilterByNameRequest
    {
        public long UserId { get; set; }
        public string FilterName { get; set; }
    }

    [AutoMapFrom(typeof(Filter))]
    public class FilterByNameResponse
    {
        public int Id { get; set; }
        public long UserId { get; set; }

        public string Username { get; set; }

        public string Name { set; get; }

        public bool IsEnable { get; set; }

        public bool IsShowShips { get; set; }

        public List<FilterItemResponse> FilterItemResponses { get; set; }
        
        public List<VesselColor> ReturnVesselFilters { get; set; }
    }

    public class FilterByUserIdRequest
    {
        public long UserId { get; set; }
    }

    public class FilterByUserIdResponse
    {
        public int Id { get; set; }
        public long UserId { get; set; }

        public string Username { get; set; }

        public string Name { set; get; }

        public bool IsEnable { get; set; }

        public bool IsShowShips { get; set; }
    }

    public class FilterDetailByUserIdRequest
    {
        public long UserId { get; set; }
        public string FilterName { get; set; }
    }

    public class FilterDetailByUserIdResponse : FilterByNameResponse
    {
        public FilterDetailByUserIdResponse(Filter filter,IRepository<Filter> _filterRepo)
        {
            _filterRepo.EnsureCollectionLoaded(filter, c => c.FilterItems);
        }
    }

}
