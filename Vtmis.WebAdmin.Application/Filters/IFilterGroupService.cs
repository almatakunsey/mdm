﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Filters.Dto;

namespace Vtmis.WebAdmin.Filters
{
    public interface IFilterGroupService
    {
        Task<FilterGroupResponseNew> Create(FilterGroupRequestNew input);
    }
}
