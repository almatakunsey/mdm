﻿using Abp.Application.Services;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Authorization.Users;
using System.Linq;
using Abp.Domain.Uow;
using System.Threading;
using Vtmis.WebAdmin.Filters.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;

namespace Vtmis.WebAdmin.Filters
{
    [AbpAuthorize]
    public class FilterService : ApplicationService, IFilterService
    {
        private readonly IRepository<Filter> _filterRepository;
        private readonly UserManager _userManager;

        public FilterService(IRepository<Filter> filterRepository, UserManager userManager)
        {
            _filterRepository = filterRepository;
            _userManager = userManager;
        }

        public async Task<CreateFilter> Create(CreateFilter m)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var user = _userManager.Users.Where(c => c.Id == m.UserId).FirstOrDefault();

                if (user != null)
                {

                    var filter = await this._filterRepository.FirstOrDefaultAsync(c => c.Name == m.FilterDto.Name && c.UserId == user.Id);

                    if (filter == null)
                    {
                        filter = ObjectMapper.Map<Filter>(m.FilterDto);
                        filter.SetPersonalizeUser(user);
                        filter.FilterItems = ObjectMapper.Map<List<FilterItem>>(m.FilterDto.FilterItemDtos);
                        filter.UpdateFilterItemWithId();

                        int id = await this._filterRepository.InsertAndGetIdAsync(filter);

                        m.FilterDto.Id = id;
                        return m;
                    }
                    else
                    {
                        throw new UserFriendlyException("Filter with same name exist!");
                    }
                }
                else
                {
                    throw new UserFriendlyException("Invalid user!");
                }
            }
        }

        public async Task<UpdateFilter> Update(UpdateFilter m)
        {
            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken cancellationToken = source.Token;

            var filter = await _filterRepository.GetAsync(m.FilterDto.Id);
            await _filterRepository.EnsureCollectionLoadedAsync(filter, x => x.FilterItems, cancellationToken);

            if (filter != null)
            {
                filter.Name = m.FilterDto.Name;
                if (m.FilterDto.FilterItemDtos != null)
                {
                    filter.ClearItemBeforeUpdate();
                    filter.FilterItems = ObjectMapper.Map<List<FilterItem>>(m.FilterDto.FilterItemDtos);
                    filter.UpdateFilterItemWithId();
                }
                await _filterRepository.UpdateAsync(filter);
            }
            else
            {
                throw new UserFriendlyException("Unable to update filter!");
            }

            return m;
        }

        public async Task Delete(int id)
        {
            var filter = await _filterRepository.GetAsync(id);

            await _filterRepository.DeleteAsync(filter);

        }

        public async Task<FilterListDto> GetById(int id)
        {
            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken cancellationToken = source.Token;

            var filter = await _filterRepository.GetAsync(id);
            await _filterRepository.EnsureCollectionLoadedAsync(filter, m => m.FilterItems, cancellationToken);

            var filterDto = ObjectMapper.Map<FilterListDto>(filter);
            filterDto.FilterItemDtos = ObjectMapper.Map<List<FilterItemDto>>(filter.FilterItems);

            return filterDto;
        }

        public async Task<List<ListFilterDto>> GetAllAsync()
        {
            var filters = await _filterRepository.GetAllListAsync();
            return ObjectMapper.Map<List<ListFilterDto>>(filters);
        }
        public async Task<List<ListFilterDto>> GetAllByTenantIdAsync(int tenantId)
        {
            var filters = await _filterRepository.GetAll().Where(x => x.TenantId == tenantId).ToListAsync();
            return ObjectMapper.Map<List<ListFilterDto>>(filters);
        }
        public async Task<List<ListFilterDto>> GetAllByCurrentUserSessionAsync()
        {
            var filters = await _filterRepository.GetAll().Where(c => c.TenantId == AbpSession.TenantId).ToListAsync();
            return ObjectMapper.Map<List<ListFilterDto>>(filters);
        }
        public async Task<List<ListFilterDto>> GetAllByUserIdAsync(int userId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                CancellationTokenSource source = new CancellationTokenSource();
                CancellationToken cancellationToken = source.Token;

                var user = await _userManager.Users.Where(c => c.Id == userId).FirstOrDefaultAsync();

                if (user != null)
                {
                    var filters = _filterRepository.GetAll().Where(c => c.TenantId == user.TenantId);

                    var listFilterDto = ObjectMapper.Map<List<ListFilterDto>>(filters);

                    return listFilterDto;
                }
                else
                {
                    throw new UserFriendlyException("Invalid user!");
                }
            }
        }


    }

}
