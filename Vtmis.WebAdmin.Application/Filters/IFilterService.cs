﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Filters.Dto;

namespace Vtmis.WebAdmin.Filters
{
    public interface IFilterService
    {
        Task<CreateFilter> Create(CreateFilter m);
        Task<UpdateFilter> Update(UpdateFilter m);
        Task Delete(int id);
        Task<FilterListDto> GetById(int id);
        Task<List<ListFilterDto>> GetAllByUserIdAsync(int userId);
        Task<List<ListFilterDto>> GetAllByTenantIdAsync(int tenantId);
        Task<List<ListFilterDto>> GetAllByCurrentUserSessionAsync();
        Task<List<ListFilterDto>> GetAllAsync();
    }
}
