﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;


namespace Vtmis.WebAdmin.Filters.Dto
{
    [AutoMapFrom(typeof(FilterGroup))]
    public class FilterGroupRequestNew:EntityDto
    {

        public string Name { get; set; }
        public int? TenantId { get; set; }
        public long UserId { get; set; }
        public List<string> MMSIList { get; set; }
        public List<string> NameList { get; set; }
        public List<string> CallSignList { get; set; }
    }

    public class FilterGroupResponseNew
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    [AutoMapFrom(typeof(FilterGroup))]
    public class FilterGroupRequestChange:EntityDto
    {
        public string Name { get; set; }
    }

    

    [AutoMapFrom(typeof(FilterGroup))]
    public class FilterGroupListResponse : EntityDto
    {
        public string Name { get; set; }

        public DateTime CreationTime { get; set; }
    }


}
