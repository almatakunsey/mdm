﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vtmis.WebAdmin.Filters.Dto
{
    public class FilterItemColumnRequest
    {
        public string Name { get; set; }
        public long UserId { get; set; }
    }

    public class FilterItemColumnResponse
    {
        public string Name { get; set; }
    }

    //

   
}
