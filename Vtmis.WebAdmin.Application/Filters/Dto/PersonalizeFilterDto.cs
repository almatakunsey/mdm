﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using Vtmis.WebAdmin.Report.Dto;
using Vtmis.WebAdmin.VtmisDomains;

namespace Vtmis.WebAdmin.Filters.Dto
{
    [AutoMapFrom(typeof(PersonalizeFilter))]
    public class PersonalizeFilterDto :EntityDto
    {
        public PersonalizeFilterDto()
        {
            PersonalizeFilterItems = new List<PersonalizeFilterItem>();
        }

        public DateTime CreationTime { get; set; }
        public string FilterName { get; set; }
        //public long UserId { get; set; }
        public List<PersonalizeFilterItem> PersonalizeFilterItems { get; set; }

    }

    [AutoMapFrom(typeof(PersonalizeFilterItem))]
    public class PersonalizeFilterItemDto : EntityDto
    {
        public PersonalizeFilterItemDto()
        {

        }

        public int PersonalizeFilterId { get; set; }
        
        public bool IsEnable { get; set; }
        public bool IsShowShips { get; set; }
        public int FilterGroupId { get; set; }
        public int MMSI { get; set; }
        public string ShipName { get; set; }
        public string CallSign { get; set; }
        public string ShipType { get; set; }
        public string AISType { get; set; }
        public decimal? MinSpeed { get; set; }
        public decimal? MaxSpeed { get; set; }
        public decimal? MinCourse { get; set; }
        public decimal? MinLength { get; set; }
        public decimal? MaxLength { get; set; }
        public string DataSource { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public string Color { get; set; }
        public string BorderColor { get; set; }
    }


    public class UpdatePersonalizeFilterDto : EntityDto
    {
        public string FilterName { get; set; }
    }

    public class UpdatePersonalizeFilterItemDto : EntityDto
    {
        public UpdatePersonalizeFilterItemDto()
        {

        }

        public int PersonalizeFilterId { get; set; }

        public bool IsEnable { get; set; }
        public bool IsShowShips { get; set; }
        public int FilterGroupId { get; set; }
        public int MMSI { get; set; }
        public string ShipName { get; set; }
        public string CallSign { get; set; }
        public string ShipType { get; set; }
        public string AISType { get; set; }
        public decimal? MinSpeed { get; set; }
        public decimal? MaxSpeed { get; set; }
        public decimal? MinCourse { get; set; }
        public decimal? MinLength { get; set; }
        public decimal? MaxLength { get; set; }
        public string DataSource { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public string Color { get; set; }
        public string BorderColor { get; set; }
    }
}
