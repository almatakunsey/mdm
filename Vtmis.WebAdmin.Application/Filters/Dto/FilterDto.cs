﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Vtmis.WebAdmin.Filters.Dto
{

    public class CreateFilter
    {

        public long UserId { get; set; }

        public FilterDto FilterDto { get; set; }

    }

    public class UpdateFilter : CreateFilter
    {

    }

    [AutoMapFrom(typeof(Filter))]
    public class ListFilterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }
    }

    [AutoMapFrom(typeof(Filter))]
    public class FilterListDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }
        public List<FilterItemDto> FilterItemDtos { get; set; }
    }

    [AutoMapFrom(typeof(Filter))]
    public class FilterDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<FilterItemDto> FilterItemDtos { get; set; }

    }

    [AutoMapFrom(typeof(FilterItem))]
    public class FilterItemDto
    {
        public virtual int FilterId { get; set; }

        public bool IsEnable { get; set; }
        public bool IsShowShips { get; set; }
        public int MMSI { get; set; }
        public string ShipName { get; set; }
        public string CallSign { get; set; }

        //Filter Group
        public int GroupTypeId { get; set; }
        public FilterGroupListResponse GroupType { get; set; }

        //VtmisType
        public string ShipType { get; set; }
        public string AISType { get; set; }

        public decimal? MinSpeed { get; set; }
        public decimal? MaxSpeed { get; set; }
        public decimal? MinCourse { get; set; }
        public decimal? MinLength { get; set; }
        public decimal? MaxLength { get; set; }
        public string DataSource { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public string Color { get; set; }
        public string BorderColor { get; set; }
    }
}
