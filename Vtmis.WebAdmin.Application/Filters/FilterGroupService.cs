﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Filters.Dto;
using System.Linq;
using Vtmis.WebAdmin.Users;
using Vtmis.WebAdmin.Authorization.Roles;
using Abp.Domain.Uow;
using Vtmis.Core.Model.DTO.Filter;

namespace Vtmis.WebAdmin.Filters
{
    //Global 
    public class FilterGroupService : ApplicationService, IFilterGroupService
    {
        private readonly IRepository<FilterGroup> _filterGroupRepository;
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;

        public FilterGroupService(IRepository<FilterGroup> _filterGroupRepository, UserManager userManager, RoleManager roleManager)
        {
            this._filterGroupRepository = _filterGroupRepository;
            this._userManager = userManager;
            this._roleManager = roleManager;
        }

        public async Task<FilterGroupResponseNew> Create(FilterGroupRequestNew input)
        {
            try
            {
                using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant,AbpDataFilters.MustHaveTenant))
                {
                    var user = _userManager.Users.Where(c => c.Id == input.UserId).FirstOrDefault();

                    if (user != null)
                    {
                        var filterGroup = await this._filterGroupRepository.FirstOrDefaultAsync(c => c.Name == input.Name && c.TenantId == user.TenantId);

                        if (filterGroup == null)
                        {
                            filterGroup = ObjectMapper.Map<FilterGroup>(input);

                            if (input.MMSIList != null)
                            {
                                var mmsiListJson = Newtonsoft.Json.JsonConvert.SerializeObject(input.MMSIList);
                                filterGroup.MMSIList = mmsiListJson;
                            }
                            if (input.NameList != null)
                            {
                                var nameListJson = Newtonsoft.Json.JsonConvert.SerializeObject(input.NameList);
                                filterGroup.NameList = nameListJson;
                            }
                            if (input.MMSIList != null)
                            {
                                var callSignListJson = Newtonsoft.Json.JsonConvert.SerializeObject(input.CallSignList);
                                filterGroup.CallSignList = callSignListJson;
                            }

                            filterGroup.TenantId = user.TenantId;
                            filterGroup.UserId = user.Id;
                            filterGroup.User = user;


                            var _filterGroup = await this._filterGroupRepository.InsertOrUpdateAsync(filterGroup);
                            await CurrentUnitOfWork.SaveChangesAsync();
                            return new FilterGroupResponseNew { Id = _filterGroup.Id, Name = _filterGroup.Name };
                        }

                        throw new Abp.UI.UserFriendlyException("Save filter group error", "Filter group with same name exist");
                    }
                    throw new Abp.UI.UserFriendlyException("Get user error", "User doesnt exist");
                }
            }
            catch (Exception err)
            {
                throw new Abp.UI.UserFriendlyException("Save filter group error", err.Message);
            }

        }

        public async Task Update(FilterGroupRequestChange input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var filterGroup = await _filterGroupRepository.GetAsync(input.Id);

                if (filterGroup != null)
                {
                    filterGroup = ObjectMapper.Map<FilterGroup>(input);

                    await _filterGroupRepository.UpdateAsync(filterGroup);
                }
                else
                {
                    throw new UserFriendlyException("Unable to update filter group!");
                }
            }
        }

        public async Task<FilterGroupByIdResponse> GetById(int id)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var filterGroup = await _filterGroupRepository.GetAsync(id);

                //var filterGroupByIdDto = ObjectMapper.Map<FilterGroupByIdResponse>(filterGroup);

                //Get is TenantAdmin
                //var user = _userManager.Users.Where(c => c.Id == filterGroup.UserId).FirstOrDefault();

                //var roles = _roleManager.Roles.Where(r => user.Roles.Any(ur => ur.RoleId == r.Id)).Select(r => r.NormalizedName);

                //if (roles.Where(c => c == "ADMIN").Count() == 1 && filterGroup.TenantId != null)
                //    filterGroupByIdDto.IsTenantAdmin = true;

                var result = new FilterGroupByIdResponse {
                    CallSignList = Newtonsoft.Json.JsonConvert
                           .DeserializeObject<List<string>>(filterGroup.CallSignList),
                    MMSIList = Newtonsoft.Json.JsonConvert
                           .DeserializeObject<List<string>>(filterGroup.MMSIList),
                    Name = filterGroup.Name,
                    Id = filterGroup.Id,
                    CreationTime = filterGroup.CreationTime,
                    NameList = Newtonsoft.Json.JsonConvert
                           .DeserializeObject<List<string>>(filterGroup.NameList)
                };
                return result;
            }
        }

        public async Task<List<FilterGroupListResponse>> GetAll()
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var filterGroupList = await _filterGroupRepository.GetAllListAsync();

                var filterGroupListDto = ObjectMapper.Map<List<FilterGroupListResponse>>(filterGroupList);

                return filterGroupListDto;
            }
        }
    }
}
