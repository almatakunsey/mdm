﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Filters.Dto;

namespace Vtmis.WebAdmin.Filters
{
    public interface IFilterItemColumnAppService
    {
        Task<FilterItemColumnResponse> Create(FilterItemColumnRequest m);
        Task<List<FilterItemColumnResponse>> GetAll();
    }
}
