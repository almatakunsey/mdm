﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Filters.Dto;
using System.Linq;

namespace Vtmis.WebAdmin.Filters
{
    public class FilterItemColumnAppService : ApplicationService, IFilterItemColumnAppService
    {
        private readonly IRepository<FilterItemColumn> _filterItemColumnRepository;
        private readonly UserManager _userManager;

        public FilterItemColumnAppService(IRepository<FilterItemColumn> filterItemColumnRepository, UserManager userManager)
        {
            _filterItemColumnRepository = filterItemColumnRepository;
            _userManager = userManager;
        }

        public async Task<FilterItemColumnResponse> Create(FilterItemColumnRequest m)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                //Get User 
                var user = _userManager.Users.Where(c => c.Id == m.UserId).FirstOrDefault();

                if(user!=null)
                {
                    var filterItemColumn = await _filterItemColumnRepository.InsertOrUpdateAsync(FilterItemColumn.Create(m.Name,user.Id));

                    return new FilterItemColumnResponse { Name = m.Name };
                }
                else
                {
                    throw new Abp.UI.UserFriendlyException("Invalid user", $"User with Id {m.UserId} not exist");
                }
            }
        }

        public async Task<List<FilterItemColumnResponse>> GetAll()
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant))
            {
                var filterItemColumns =await _filterItemColumnRepository.GetAllListAsync();
                return ObjectMapper.Map<List<FilterItemColumnResponse>>(filterItemColumns); 
            }
        }
    }
}
