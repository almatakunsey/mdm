﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Filters.Dto;
using Vtmis.WebAdmin.VtmisDomains;

namespace Vtmis.WebAdmin.Filters
{
    public class PersonalizeFilterService : ApplicationService
    {

        private readonly IRepository<PersonalizeFilter> _personalizeFilterRepository;
        private readonly IRepository<PersonalizeFilterItem> _personalizeFilterItemRepository;

        public PersonalizeFilterService(IRepository<PersonalizeFilter> personalizeFilterRepository,IRepository<PersonalizeFilterItem> personalizeFilterItemRepository)
        {
            this._personalizeFilterRepository = personalizeFilterRepository;
            this._personalizeFilterItemRepository = personalizeFilterItemRepository;
        }


        public async Task<PersonalizeFilterDto> Create(PersonalizeFilterDto input)
        {

            var filter = await this._personalizeFilterRepository.FirstOrDefaultAsync(c => c.FilterName == input.FilterName);

            if (filter == null)
            {
                
                filter = ObjectMapper.Map<PersonalizeFilter>(input);

                int id = await this._personalizeFilterRepository.InsertAndGetIdAsync(filter);
                input.Id = id;
            }

            return input;
        }

        public async Task<PersonalizeFilterDto> GetPersonalizeFilterById(int id)
        {
            CancellationTokenSource source = new CancellationTokenSource();
            CancellationToken cancellationToken = source.Token;

            var personalizeFilter = await _personalizeFilterRepository.GetAsync(id);
            await _personalizeFilterRepository.EnsureCollectionLoadedAsync(personalizeFilter, m => m.PersonalizeFilterItems, cancellationToken);


            return ObjectMapper.Map<PersonalizeFilterDto>(personalizeFilter);
        }

        public async Task<List<PersonalizeFilterDto>> Get()
        {
            var personalizeFilter = await _personalizeFilterRepository.GetAllListAsync();

            return ObjectMapper.Map<List<PersonalizeFilterDto>>(personalizeFilter);
        }

        public async Task<UpdatePersonalizeFilterDto> UpdatePersonalizeFilter(UpdatePersonalizeFilterDto input)
        {
            
            var personalizeFilter = await _personalizeFilterRepository.GetAsync(input.Id);
            //await _personalizeFilterRepository.EnsureCollectionLoadedAsync(personalizeFilter, m => m.PersonalizeFilterItems, cancellationToken);

            if (personalizeFilter != null)
            {
                personalizeFilter.FilterName = input.FilterName;

                await _personalizeFilterRepository.UpdateAsync(personalizeFilter);
            }

            return input;
        }

        public async Task<UpdatePersonalizeFilterItemDto> UpdatePersonalizeFilterItem(UpdatePersonalizeFilterItemDto input)
        {

            var personalizeFilterItem = await _personalizeFilterItemRepository.GetAsync(input.Id);
            //await _personalizeFilterRepository.EnsureCollectionLoadedAsync(personalizeFilter, m => m.PersonalizeFilterItems, cancellationToken);

            if (personalizeFilterItem != null)
            {
                personalizeFilterItem.BorderColor = input.BorderColor;

                await _personalizeFilterItemRepository.UpdateAsync(personalizeFilterItem);
            }

            return input;
        }

        public async Task Delete(int id)
        {
            var filter = await _personalizeFilterRepository.GetAsync(id);

            await _personalizeFilterRepository.DeleteAsync(filter);
           
        }
    }
}
