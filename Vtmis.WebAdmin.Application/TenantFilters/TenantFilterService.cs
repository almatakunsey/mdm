﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Enums.TenantFilters;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.MultiTenancy;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Permissions;
using Vtmis.WebAdmin.TenantFilters.Dto;

namespace Vtmis.WebAdmin.TenantFilters
{
    [RemoteService(IsMetadataEnabled = false)]
    public class TenantFilterService : ApplicationService, ITenantFilterService
    {
        private readonly IRepository<TenantFilter> _repository;
        private readonly IRepository<Tenant> _tenantRepo;
        private readonly IRepository<User, long> _userRepo;
        private readonly IMdmPermissionService _mdmPermission;

        public TenantFilterService(IRepository<TenantFilter> repository,
            IRepository<Tenant> tenantRepo,
            IRepository<User, long> userRepo,
            IMdmPermissionService mdmPermission)
        {
            _repository = repository;
            _tenantRepo = tenantRepo;
            _userRepo = userRepo;
            _mdmPermission = mdmPermission;
        }
        
        public async Task<ResponseTenantFilter> CreateAsync(RequestCreateTenantFilter input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    if ((await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.TenantFilter_Create, true)) == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                    input.FilterName = input.FilterName.TrimStringOrDefault();
                    if (input.FilterItems.Any())
                    {
                        foreach (var item in input.FilterItems)
                        {
                            item.ColumnName = item.ColumnName.TrimStringOrDefault();
                            item.ColumnValue = item.ColumnValue.TrimStringOrDefault();
                            item.Operation = item.Operation.TrimStringOrDefault();
                        }
                    }
                    var existingName = await _repository.GetAll().IgnoreQueryFilters().AnyAsync(x =>
                        x.IsDeleted == false && x.TenantId == AbpSession.TenantId &&
                        x.FilterName.IsEqual(input.FilterName));

                    if (existingName)
                    {
                        throw new AlreadyExistException("Tenant Filter Name Already Exist");
                    }

                    var filter = ObjectMapper.Map<TenantFilter>(input);
                    filter.TenantId = AbpSession.TenantId;
                    filter.CreatorUserId = AbpSession.UserId;
                    if (AbpSession.TenantId == null)
                    {
                        filter.Type = TenantFilterType.Host;
                    }
                    else if (AbpSession.TenantId != null)
                    {
                        filter.Type = TenantFilterType.TenantAdmin;
                    }
                    else
                    {
                        filter.Type = TenantFilterType.Host;
                    }
                    var result = await _repository.InsertAsync(filter);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    return ObjectMapper.Map<ResponseTenantFilter>(result);
                }
                catch (ForbiddenException ex)
                {
                    throw ex;
                }
                catch (AlreadyExistException err)
                {
                    throw new AlreadyExistException(err.Message);
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err);
                }
            }
        }

        public async Task<PaginatedList<ResponseTenantFilter>> GetAllAsync(int pageIndex, int pageSize)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {               
                var qry = _repository.GetAll().IgnoreQueryFilters().Include(x => x.FilterItems).Where(x => x.IsDeleted == false);
                var isHighPermission = false;

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.TenantFilter_View_MDM, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.TenantFilter_View_Tenant, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        qry = qry.Where(x => x.Type == TenantFilterType.TenantAdmin && x.TenantId == AbpSession.TenantId);
                    }
                }
              
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }

                var entities = await new PaginatedList<TenantFilter>().CreateAsync(qry, pageIndex, pageSize);

                return ObjectMapper.Map<PaginatedList<ResponseTenantFilter>>(entities);
            }
        }

        public async Task<ResponseTenantFilter> GetByIdAsync(int tenantFilterId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                var qry = _repository.GetAll().IgnoreQueryFilters().Include(x => x.FilterItems).Where(x => x.IsDeleted == false);
                var isHighPermission = false;

                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.TenantFilter_View_MDM, true))
                {
                    isHighPermission = true;
                }
                if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.TenantFilter_View_Tenant, true))
                {
                    if (isHighPermission == false)
                    {
                        isHighPermission = true;
                        qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                    }
                }
               
                if (isHighPermission == false)
                {
                    throw new ForbiddenException("Permission Denied");
                }

                var resultQry = await qry.Where(x => x.Id == tenantFilterId).FirstOrDefaultAsync();
                if (resultQry == null)
                {
                    throw new NotFoundException("Tenant Filter Does not exist");
                }
                return ObjectMapper.Map<ResponseTenantFilter>(resultQry);
            }
        }

        public async Task<bool> AssignFilterAsync(RequestAssignTenantFilter input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    if ((await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.TenantFilter_Assign, true)) == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }

                    var filter = await _repository.GetAll().IgnoreQueryFilters()
                        .FirstOrDefaultAsync(x =>
                        x.IsDeleted == false && x.Id == input.TenantFilterId && 
                        x.TenantId == AbpSession.TenantId);

                    if (filter == null)
                    {
                        throw new NotFoundException("Tenant Filter Does Not Exist");
                    }

                    if (AbpSession.TenantId != null)
                    {
                        if (filter.TenantId != AbpSession.TenantId)
                        {
                            throw new NotFoundException("Tenant Filter Does Not Exist");
                        }
                    }

                    if (filter.Type == TenantFilterType.Host)
                    {
                        if (AbpSession.TenantId != null)
                        {
                            throw new ForbiddenException("Action Denied");
                        }
                        if (filter.AssignedTenants == null)
                        {
                            filter.AssignedTenants = new List<int>();
                        }
                        foreach (var item in input.Assignees)
                        {
                            // check if existing filter have matching ids with input ids
                            if (filter.AssignedTenants.Any(x => x == item.Key) == false)
                            {
                                //check if input assign is true
                                if (item.Value)
                                {
                                    //assign
                                    filter.AssignedTenants.Add((int)item.Key);
                                }
                            }
                            else
                            {
                                //check if input assign is false
                                if (item.Value == false)
                                {
                                    //remove assign
                                    filter.AssignedTenants.Remove((int)item.Key);
                                }
                            }

                            // check if input ids exist in db
                            if (await _tenantRepo.GetAll().IgnoreQueryFilters()
                                .AnyAsync(x => x.Id == item.Key && x.IsDeleted == false) == false)
                            {
                                //throw error if input id does not exist
                                throw new Exception("One of the Tenant Id's does not exist");
                            }
                        }
                    }
                    else if (filter.Type == TenantFilterType.TenantAdmin)
                    {
                        if (filter.AssignedUsers == null)
                        {
                            filter.AssignedUsers = new List<long>();
                        }
                        foreach (var item in input.Assignees)
                        {
                            // check if existing filter have matching ids with input ids
                            if (filter.AssignedUsers.Any(x => x == item.Key) == false)
                            {
                                //check if input assign is true
                                if (item.Value)
                                {
                                    //assign
                                    filter.AssignedUsers.Add(item.Key);
                                }
                            }
                            else
                            {
                                //check if input assign is false
                                if (item.Value == false)
                                {
                                    //remove assign
                                    filter.AssignedUsers.Remove(item.Key);
                                }
                            }
                            // check if input ids exist in db
                            if (await _userRepo.GetAll().IgnoreQueryFilters()
                              .AnyAsync(x => x.Id == item.Key && x.IsDeleted == false) == false)
                            {
                                //throw error if input id does not exist
                                throw new Exception("One of the User Id's does not exist");
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }

                    await _repository.UpdateAsync(filter);
                    await CurrentUnitOfWork.SaveChangesAsync();

                    return true;
                }
                catch (ForbiddenException ex)
                {
                    throw ex;
                }
                catch (NotFoundException err)
                {
                    throw new NotFoundException(err.Message);
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err);
                }
            }
        }

    }
}
