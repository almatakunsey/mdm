﻿using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.TenantFilters.Dto;

namespace Vtmis.WebAdmin.TenantFilters
{
    public interface ITenantFilterService
    {
        Task<ResponseTenantFilter> GetByIdAsync(int tenantFilterId);
        Task<ResponseTenantFilter> CreateAsync(RequestCreateTenantFilter input);
        Task<bool> AssignFilterAsync(RequestAssignTenantFilter input);
        Task<PaginatedList<ResponseTenantFilter>> GetAllAsync(int pageIndex, int pageSize);
    }
}