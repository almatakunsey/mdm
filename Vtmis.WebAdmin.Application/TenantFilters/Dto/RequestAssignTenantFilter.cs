﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.TenantFilters.Dto
{
    public class RequestAssignTenantFilter
    {
        public RequestAssignTenantFilter()
        {
            Assignees = new Dictionary<long, bool>(); // Key: Id , Value: Assign
        }
        [Required]
        public int TenantFilterId { get; set; }
        [Required]
        public Dictionary<long, bool> Assignees { get; set; }
    }
}
