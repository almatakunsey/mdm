﻿using System.Collections.Generic;
using Vtmis.Core.Common.Enums.TenantFilters;

namespace Vtmis.WebAdmin.TenantFilters.Dto
{
    public class ResponseTenantFilter
    {      
        public ResponseTenantFilter()
        {
            FilterItems = new List<ResponseTenantFilterItem>();
            AssignedTenants = new List<int?>();
            AssignedUsers = new List<long>();
        }
        public int Id { get; set; }
        public string FilterName { get; set; }
        public TenantFilterType Type { get; set; }
        public List<ResponseTenantFilterItem> FilterItems { get; set; }
        public int? TenantId { get; set; }
        public List<int?> AssignedTenants { get; set; }
        public List<long> AssignedUsers { get; set; }
    }
}
