﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Vtmis.Core.Common.Enums.TenantFilters;

namespace Vtmis.WebAdmin.TenantFilters.Dto
{
    public class RequestCreateTenantFilter
    {
        public RequestCreateTenantFilter()
        {
            FilterItems = new List<RequestTenantFilterItem>();
        }      
        [Required]
        public string FilterName { get; set; }
        //[Required]
        //public TenantFilterType Type { get; set; }
        [Required]
        public List<RequestTenantFilterItem> FilterItems { get; set; }       
    }
}
