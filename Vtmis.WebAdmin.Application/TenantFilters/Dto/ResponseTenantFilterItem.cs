﻿using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.TenantFilters.Dto
{
    public class ResponseTenantFilterItem
    {
        public int Id { get; set; }
        public Condition Condition { get; set; }     
        public string ColumnName { get; set; }      
        public string Operation { get; set; }      
        public string ColumnValue { get; set; }

        public int RowIndex { get; set; }
    }
}