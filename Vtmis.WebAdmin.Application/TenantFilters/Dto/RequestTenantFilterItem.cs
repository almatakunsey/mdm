﻿using System.ComponentModel.DataAnnotations;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.TenantFilters.Dto
{
    public class RequestTenantFilterItem
    {
        [Required]
        public Condition Condition { get; set; }
        [Required]
        public string ColumnName { get; set; }
        [Required]
        public string Operation { get; set; }
        [Required]
        public string ColumnValue { get; set; }
        [Required]
        public int RowIndex { get; set; }
    }
}
