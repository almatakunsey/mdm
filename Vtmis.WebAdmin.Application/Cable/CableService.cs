﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Cable.Dto;
using Vtmis.WebAdmin.Permissions;

namespace Vtmis.WebAdmin.Cable
{
    [RemoteService(IsMetadataEnabled = false)]
    public class CableService : ApplicationService, ICableService
    {
        private readonly IRepository<Cable> _repository;
        private readonly IMdmPermissionService _mdmPermission;

        public CableService(IRepository<Cable> repository, IMdmPermissionService mdmPermission)
        {
            _repository = repository;
            _mdmPermission = mdmPermission;
        }

        public async Task<ResponseCableDto> CreateAsync(RequestCreateCableDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    if ((await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_Create, true)) == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }

                    var isCableNameExist = await _repository.GetAll().IgnoreQueryFilters().AnyAsync(x =>
                        x.IsDeleted == false && x.UserId == AbpSession.UserId &&
                        string.Equals(x.Name, input.Name, StringComparison.CurrentCultureIgnoreCase));

                    if (isCableNameExist)
                    {
                        throw new AlreadyExistException("Cable Name Already Exist");
                    }

                    var route = ObjectMapper.Map<Cable>(input);
                    route.UserId = AbpSession.UserId.Value;
                    route.TenantId = AbpSession.TenantId;
                    route.CreatorUserId = AbpSession.UserId;

                    var result = await _repository.InsertAsync(route);
                    await CurrentUnitOfWork.SaveChangesAsync();
                    return ObjectMapper.Map<ResponseCableDto>(result);
                }
                catch (AlreadyExistException err)
                {
                    throw new AlreadyExistException(err.Message);
                }
                catch (ForbiddenException err)
                {
                    throw new ForbiddenException(err.Message);
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err);
                }
            }

        }

        public async Task<PaginatedList<ResponseCableDto>> GetAllAsync(int pageIndex, int pageSize)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var isHighPermission = false;
                    var qry = _repository.GetAll()
                                .AsNoTracking().IgnoreQueryFilters().Include(x => x.CableDetails).Where(w => w.IsDeleted == false).AsQueryable();
                    var a = AbpSession.TenantId;
                    var b = AbpSession.UserId;
                    //Check permissions
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_View_Mdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_View_Tenant, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                        }
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_View_Own, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.UserId == AbpSession.UserId);
                        }
                    }
                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                    var domainResult = await new PaginatedList<Cable>().CreateAsync(qry, pageIndex, pageSize);

                    return ObjectMapper.Map<PaginatedList<ResponseCableDto>>(domainResult);
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err);
                }
            }
        }

        public async Task<ResponseCableDto> GetAsync(int id)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var isHighPermission = false;
                    var qry = _repository.GetAll().IgnoreQueryFilters()
                        .Include(x => x.CableDetails).AsQueryable();

                    //Check permissions
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_View_Mdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_View_Tenant, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.TenantId == AbpSession.TenantId);
                        }
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_View_Own, true))
                    {
                        if (isHighPermission == false)
                        {
                            isHighPermission = true;
                            qry = qry.Where(x => x.UserId == AbpSession.UserId);
                        }
                    }
                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                    var result = await qry.FirstOrDefaultAsync(x => x.IsDeleted == false
                            && x.Id == id);
                    if (result != null)
                    {
                        return ObjectMapper.Map<ResponseCableDto>(result);
                    }
                    throw new NotFoundException("Cable does not exist");
                }
                catch (NotFoundException err)
                {
                    throw new NotFoundException(err.Message);
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err);
                }
            }
        }

        public async Task<ResponseCableDto> UpdateAsync(RequestUpdateCableDto input)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var existingCable = await _repository.GetAll().IgnoreQueryFilters()
                                         .Include(i => i.CableDetails)
                            .FirstOrDefaultAsync(x => x.Id == input.Id && x.IsDeleted == false);

                    if (existingCable == null)
                    {
                        throw new NotFoundException($"Cable does not exist");
                    }
                    else
                    {
                        var otherRoute = await _repository.GetAll().IgnoreQueryFilters()
                                    .Where(x => string.Equals(x.Name, input.Name,
                                    StringComparison.CurrentCultureIgnoreCase) &&
                                    x.Id != input.Id && x.IsDeleted == false
                                    && x.UserId == AbpSession.UserId).AnyAsync();
                        if (otherRoute)
                        {
                            throw new AlreadyExistException($"Cable {input.Name} already exist.");
                        }
                    }
                    //existingCable.CableDetails.Clear();
                    //await _repository.UpdateAsync(existingCable);
                    //await CurrentUnitOfWork.SaveChangesAsync();

                    var cable = ObjectMapper.Map(input, existingCable);
                    var isHighPermission = false;

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_Update_Mdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_Update_Tenant, true))
                    {
                        isHighPermission = true;
                        if (cable.TenantId != AbpSession.TenantId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_Update_Own, true))
                    {
                        isHighPermission = true;
                        if (cable.UserId != AbpSession.UserId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }
                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                    var result = await _repository.UpdateAsync(cable);
                    await CurrentUnitOfWork.SaveChangesAsync();

                    //removeCoordinates.CableDetails = ObjectMapper.Map(input.CableDetails, 
                    //    removeCoordinates.CableDetails);
                    //var result = await _repository.UpdateAsync(removeCoordinates);
                    //await CurrentUnitOfWork.SaveChangesAsync();
                    return ObjectMapper.Map<ResponseCableDto>(result);
                }
                catch (AlreadyExistException err)
                {
                    throw err;
                }
                catch (NotFoundException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }
            }
        }

        public async Task DeleteAsync(int id)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant, AbpDataFilters.MustHaveTenant))
            {
                try
                {
                    var isExist = await _repository.GetAll().IgnoreQueryFilters()
                            .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
                    if (isExist == null)
                    {
                        throw new NotFoundException("Cable Does Not Exist");
                    }

                    var isHighPermission = false;

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_Delete_Mdm, true))
                    {
                        isHighPermission = true;
                    }
                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_Delete_Tenant, true))
                    {
                        isHighPermission = true;
                        if (isExist.TenantId != AbpSession.TenantId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }

                    if (await _mdmPermission.IsGrantedAsync(MdmPermissionsConst.Cable_Delete_Own, true))
                    {
                        isHighPermission = true;
                        if (isExist.UserId != AbpSession.UserId && isHighPermission == false)
                        {
                            throw new ForbiddenException("Permission Denied");
                        }
                    }
                    if (isHighPermission == false)
                    {
                        throw new ForbiddenException("Permission Denied");
                    }
                    isExist.DeleterUserId = AbpSession.UserId;
                    isExist.DeletionTime = DateTime.Now;
                    isExist.IsDeleted = true;
                    await _repository.UpdateAsync(isExist);

                    await CurrentUnitOfWork.SaveChangesAsync();
                }
                catch (NotFoundException err)
                {
                    throw err;
                }
                catch (Exception err)
                {
                    throw new ServerFaultException(err.Message);
                }
            }

        }
    }
}
