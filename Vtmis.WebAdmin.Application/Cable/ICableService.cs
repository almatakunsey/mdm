﻿using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Cable.Dto;

namespace Vtmis.WebAdmin.Cable
{
    public interface ICableService
    {
        Task<PaginatedList<ResponseCableDto>> GetAllAsync(int pageIndex, int pageSize);
        Task<ResponseCableDto> GetAsync(int id);
        Task<ResponseCableDto> CreateAsync(RequestCreateCableDto input);
        Task<ResponseCableDto> UpdateAsync(RequestUpdateCableDto input);
        Task DeleteAsync(int id);
    }
}
