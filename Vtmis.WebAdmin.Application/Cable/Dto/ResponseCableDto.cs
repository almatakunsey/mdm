﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.Cable.Dto
{
    public class ResponseCableDto : EntityDto<int>
    {
        public ResponseCableDto()
        {
            CableDetails = new List<ResponseCableDetailDto>();
        }     
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public bool IsEnable { get; set; }
        public string Color { get; set; }
        public DateTime Date { get; set; }
        public List<ResponseCableDetailDto> CableDetails { get; set; }
    }
}
