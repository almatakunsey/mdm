﻿using Abp.Application.Services.Dto;

namespace Vtmis.WebAdmin.Cable.Dto
{
    public class ResponseCableDetailDto : EntityDto<int>
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Order { get; set; }
    }
}
