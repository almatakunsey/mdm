﻿using System;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.Cable.Dto
{
    public class RequestCreateCableDto
    {
        public RequestCreateCableDto()
        {
            CableDetails = new List<RequestCreateCableDetailDto>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public bool IsEnable { get; set; }
        public string Color { get; set; }
        public DateTime Date { get; set; }
        public List<RequestCreateCableDetailDto> CableDetails { get; set; }
    }
}
