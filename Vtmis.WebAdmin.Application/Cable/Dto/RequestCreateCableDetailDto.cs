﻿namespace Vtmis.WebAdmin.Cable.Dto
{
    public class RequestCreateCableDetailDto
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Order { get; set; }
    }
}
