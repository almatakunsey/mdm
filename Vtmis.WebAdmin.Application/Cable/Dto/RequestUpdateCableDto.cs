﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.Cable.Dto
{
    public class RequestUpdateCableDto : EntityDto<int>
    {
        public RequestUpdateCableDto()
        {
            CableDetails = new List<RequestUpdateCableDetailDto>();
        }      
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public bool IsEnable { get; set; }
        public string Color { get; set; }
        public DateTime Date { get; set; }
        public List<RequestUpdateCableDetailDto> CableDetails { get; set; }
    }
}
