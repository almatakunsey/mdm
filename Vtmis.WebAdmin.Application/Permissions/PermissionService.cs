﻿using Abp.Application.Services;
using Abp.Authorization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Authorization;

namespace Vtmis.WebAdmin.Permissions
{
    [RemoteService(IsMetadataEnabled = false)]
    public class PermissionService : ApplicationService, IPermissionService
    {
        public IEnumerable<string> Get(bool isGranted = true, bool fullname = false)
        {
            try
            {
                var allPermissions = PermissionFinder.GetAllPermissions(new WebAdminAuthorizationProvider()).Select(x => x.Name);
                if (isGranted)
                {
                    allPermissions = allPermissions.Where(x => IsGranted(x));
                }

                if (fullname)
                {
                    return allPermissions;
                }
                if (allPermissions.Any())
                {
                    var filteredPermissions = new List<string>();
                    foreach (var permission in allPermissions)
                    {
                        var permissionChunk = permission.Split(".").ToList();
                        string combinedName = permission;
                        if (permissionChunk.Count > 2)
                        {
                            combinedName = $"{permissionChunk[0]}.{permissionChunk[1]}";
                        }
                        if (!filteredPermissions.Any(x => x == combinedName))
                        {
                            filteredPermissions.Add(combinedName);
                        }
                    }
                    return filteredPermissions;
                }
                return null;
            }
            catch (System.Exception err)
            {
                throw err;
            }

        }

        public async Task<bool> IsPermissionGranted(string permissionName, bool fullname = false)
        {
            
            try
            {
                if (fullname)
                {
                    return await IsGrantedAsync(permissionName);
                }

                var targets = new List<string> { "Mdm", "Tenant", "Own" };
                var result = false;
                foreach (var item in targets)
                {
                    var isExist = MdmPermissionsConst.All().Where(x => x.Contains(permissionName, System.StringComparison.CurrentCultureIgnoreCase)).Any();
                    if (isExist == false)
                    {
                        return false;
                    }
                    result = await IsGrantedAsync($"{permissionName}.{item}");
                    if (result)
                    {
                        break;
                    }
                }
                return result;
            }
            catch (System.Exception err)
            {
                throw err;
            }

        }
    }


}
