﻿using Abp.Runtime.Session;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Permissions.Dto;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories;

namespace Vtmis.WebAdmin.Permissions
{
    public class MdmPermissionService : IMdmPermissionService
    {
        private readonly IAbpPermissionRepository _repository;
        private readonly IAbpSession _abpSession;
        private readonly IMdmUserRoleRepository _userRoleRepository;

        public MdmPermissionService(IAbpPermissionRepository repository, IAbpSession abpSession, IMdmUserRoleRepository userRoleRepository)
        {
            _repository = repository;
            _abpSession = abpSession;
            _userRoleRepository = userRoleRepository;
        }

        public async Task<IEnumerable<string>> GetAllAsync(bool isGranted, bool fullname)
        {
            var userId = _abpSession.UserId;
            var tenantId = _abpSession.TenantId;
            var roles = await _userRoleRepository.GetAll().Where(x => x.UserId == userId && x.TenantId == tenantId)
                                       .Select(s => s.RoleId).ToListAsync();
            //var qry = _repository.GetAll().Where(x => x.IsGranted == isGranted)
            //        .Select(x => x.Name);
            if (roles.Count <= 0)
            {
                return null;
            }
         
            var permissions = new List<string>();
            foreach (var roleId in roles)
            {
                var p = _repository.GetAll().Where(x => x.TenantId == tenantId && x.RoleId == roleId && x.IsGranted == isGranted);
                foreach (var item in p)
                {
                    permissions.Add(item.Name);
                }

            }
            permissions = permissions.Distinct().ToList();
            if (fullname == false)
            {
                var filteredPermissions = new List<string>();
                foreach (var permission in permissions)
                {
                    var permissionChunk = permission.Split(".").ToList();
                    string combinedName = permission;
                    if (permissionChunk.Count > 2)
                    {
                        combinedName = $"{permissionChunk[0]}.{permissionChunk[1]}";
                    }
                    if (!filteredPermissions.Any(x => x == combinedName))
                    {
                        filteredPermissions.Add(combinedName);
                    }
                }
                return filteredPermissions;
            }
            return permissions;
        }

        public async Task<bool> IsGrantedAsync(string permissionName, bool fullname = false)
        {
            try
            {
                var userId = _abpSession.UserId;
                var tenantId = _abpSession.TenantId;
                var roles = await _userRoleRepository.GetAll().Where(x => x.UserId == userId && x.TenantId == tenantId)
                                        .Select(s => s.RoleId).ToListAsync();
                if (roles.Count <= 0)
                {
                    return false;
                }
                if (fullname)
                {

                    var fullNameResult = false;
                    foreach (var roleId in roles)
                    {
                        fullNameResult = await _repository.GetAll().Where(x => string.Equals(x.Name, permissionName,
                            System.StringComparison.CurrentCultureIgnoreCase)
                             && x.TenantId == tenantId && x.RoleId == roleId && x.IsGranted == true).AnyAsync();
                        if (fullNameResult == true)
                        {
                            return fullNameResult;
                        }
                    }

                }

                var targets = new List<string> { "Mdm", "Tenant", "Own" };
                var result = false;
                foreach (var item in targets)
                {
                    foreach (var roleId in roles)
                    {
                        result = await _repository.GetAll().Where(x => string.Equals(x.Name, $"{permissionName}.{item}",
                            System.StringComparison.CurrentCultureIgnoreCase)
                             && x.TenantId == tenantId && x.RoleId == roleId && x.IsGranted == true).AnyAsync();

                        if (result == true)
                        {
                            return result;
                        }
                    }
                }
                return result;
            }
            catch (System.Exception err)
            {
                throw err;
            }

        }
    }
}
