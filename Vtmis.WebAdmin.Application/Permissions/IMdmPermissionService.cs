﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Permissions.Dto;

namespace Vtmis.WebAdmin.Permissions
{
    public interface IMdmPermissionService
    {
        Task<IEnumerable<string>> GetAllAsync(bool isGranted, bool fullname);
        Task<bool> IsGrantedAsync(string permissionName, bool fullname = false);
    }
}
