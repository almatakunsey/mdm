﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.Permissions
{
    public interface IPermissionService
    {
        IEnumerable<string> Get(bool isGranted = true, bool fullname = false);
        Task<bool> IsPermissionGranted(string permissionName, bool fullname = false);
    }
}