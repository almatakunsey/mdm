﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.Permissions.Dto
{
    public class ResponseMdmPermission
    {
        public string Name { get; set; }
    }
}
