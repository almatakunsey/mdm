﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StackExchange.Redis;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.EICS.DTO;
using Vtmis.WebAdmin.EntityFrameworkCore.Seed;

namespace Vtmis.WebAdmin.EICS
{
    [RemoteService(IsMetadataEnabled = false)]
    public class EICSService : ApplicationService, IEICSService
    {
        private readonly IRepository<EICS_info> _repository;
        private readonly IRepository<EICS_Equipments.EICS_Equipment> _eicsEquipmentrepository;
        private readonly IConnectionMultiplexer _connectionMultiplexer;

        public EICSService(IRepository<EICS_info> repository, IRepository<EICS_Equipments.EICS_Equipment> EicsEquipmentrepository, IConnectionMultiplexer connectionMultiplexer)
        {
            _repository = repository;
            _eicsEquipmentrepository = EicsEquipmentrepository;
            _connectionMultiplexer = connectionMultiplexer;
        }

        public async Task<ResponseEICSInfoDto> CreateAsync(RequestCreateEICSInfoDto input)
        {
            var eics = ObjectMapper.Map<EICS_info>(input);
            var result = await _repository.InsertAsync(eics);
            await CurrentUnitOfWork.SaveChangesAsync();

            var qry = await _repository.GetAll().IgnoreQueryFilters().Include("EICS_Details").Include("EICS_Details.Equipment").AsNoTracking().FirstOrDefaultAsync(s => s.MMSI == input.MMSI);
            var data = new EICSSeedData();
            if (qry != null)
            {
                var redis = _connectionMultiplexer.GetDatabase();
                data.MMSI = qry.MMSI;
                data.EICS = qry.EICS_Details.Select(x => x.Equipment.Name).ToList();
                var dataString = JsonConvert.SerializeObject(data);
                await redis.HashSetAsync($"eics_{AppHelper.GetEnvironmentName()}", input.MMSI.ToString(), dataString, When.Always, CommandFlags.FireAndForget);
            }

            return ObjectMapper.Map<ResponseEICSInfoDto>(result);
        }

        public async Task<PaginatedList<ResponseEICSInfoDto>> GetAllAsync(int pageIndex, int pageSize)
        {
            var query = _repository.GetAll().IgnoreQueryFilters().Include("EICS_Details").Include("EICS_Details.Equipment").AsQueryable();


            var result = await new PaginatedList<EICS_info>().CreateAsync(query, pageIndex, pageSize);

            return ObjectMapper.Map<PaginatedList<ResponseEICSInfoDto>>(result);
        }

        public async Task<ResponseEICSInfoDto> GetAsync(int mmsi)
        {
            var query = await _repository.GetAll().IgnoreQueryFilters().Include("EICS_Details").Include("EICS_Details.Equipment").Where(x => x.MMSI == mmsi).FirstOrDefaultAsync();

            return ObjectMapper.Map<ResponseEICSInfoDto>(query);
        }

        public async Task<ResponseEICSInfoDto> UpdateAsync(int mmsi, RequestUpdateInfoEICS input)
        {

            var query = await _repository.GetAll().IgnoreQueryFilters().Include(x => x.EICS_Details).Where(x => x.MMSI == mmsi).FirstOrDefaultAsync();

            if (query == null)
            {
                throw new UserFriendlyException("Equipment Not Found");
            }
            else
            {
                var eics = ObjectMapper.Map(input, query);
                var result = await _repository.UpdateAsync(eics);
                await CurrentUnitOfWork.SaveChangesAsync();

                var qry = await _repository.GetAll().IgnoreQueryFilters().Include("EICS_Details").Include("EICS_Details.Equipment").AsNoTracking().FirstOrDefaultAsync(s => s.MMSI == mmsi);
                var data = new EICSSeedData();
                if (qry != null)
                {
                    var redis = _connectionMultiplexer.GetDatabase();
                    data.MMSI = qry.MMSI;
                    data.EICS = qry.EICS_Details.Select(x => x.Equipment.Name).ToList();
                    var dataString = JsonConvert.SerializeObject(data);
                    await redis.HashSetAsync($"eics_{AppHelper.GetEnvironmentName()}", mmsi.ToString(), dataString, When.Always, CommandFlags.FireAndForget);
                }

                return ObjectMapper.Map<ResponseEICSInfoDto>(result);
            }
        }

        public async Task<bool> DeleteAsync(int mmsi)
        {
            var query = await _repository.GetAll().IgnoreQueryFilters().Include("EICS_Details").Include("EICS_Details.Equipment").Where(x => x.MMSI == mmsi).FirstOrDefaultAsync();

            await _repository.DeleteAsync(query);
            await CurrentUnitOfWork.SaveChangesAsync();

            var redis = _connectionMultiplexer.GetDatabase();
            await redis.HashDeleteAsync($"eics_{AppHelper.GetEnvironmentName()}", mmsi.ToString(), CommandFlags.FireAndForget);

            return true;
        }
    }
}
