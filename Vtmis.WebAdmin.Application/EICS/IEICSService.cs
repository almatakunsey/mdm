﻿using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.EICS.DTO;

namespace Vtmis.WebAdmin.EICS
{
    public interface IEICSService
    {
        Task<ResponseEICSInfoDto> CreateAsync(RequestCreateEICSInfoDto input);
        Task<PaginatedList<ResponseEICSInfoDto>> GetAllAsync(int pageIndex, int pageSize);
        Task<ResponseEICSInfoDto> GetAsync(int mmsi);
        Task<ResponseEICSInfoDto> UpdateAsync(int mmsi, RequestUpdateInfoEICS input);
        Task<bool> DeleteAsync(int mmsi);
    }
}
