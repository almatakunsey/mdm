﻿using System.Collections.Generic;

namespace Vtmis.WebAdmin.EICS.DTO
{
    public class RequestUpdateInfoEICS
    {
        public RequestUpdateInfoEICS()
        {
            EICS_Details = new List<RequestUpdateDetailsEICS>();
        }
        public List<RequestUpdateDetailsEICS> EICS_Details { get; set; }
    }
}
