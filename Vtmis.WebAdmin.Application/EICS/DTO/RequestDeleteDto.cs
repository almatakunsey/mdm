﻿using System.Collections.Generic;

namespace Vtmis.WebAdmin.EICS.DTO
{
    public class RequestDeleteDto
    {
        public List<int> EquipmentId { get; set; }
    }
}
