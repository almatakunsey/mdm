﻿using System.Collections.Generic;

namespace Vtmis.WebAdmin.EICS.DTO
{
    public class ResponseEICSInfoDto
    {
        public ResponseEICSInfoDto()
        {
            EICS_Details = new List<ResponseEICSDetailDto>();
        }
        public int MMSI { get; set; }

        public IList<ResponseEICSDetailDto> EICS_Details { get; set; }
    }
}
