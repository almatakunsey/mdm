﻿using Abp.Application.Services.Dto;

namespace Vtmis.WebAdmin.EICS.DTO
{
    public class ResponseEICSDetailDto
    {
        public int EquipmentId { get; set; }

        public virtual EICS_Equipments.EICS_Equipment Equipment { get; set; }
    }
}
