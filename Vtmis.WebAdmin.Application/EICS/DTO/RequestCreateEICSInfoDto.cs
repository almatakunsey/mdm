﻿using System.Collections.Generic;

namespace Vtmis.WebAdmin.EICS.DTO
{
    public class RequestCreateEICSInfoDto
    {
        public RequestCreateEICSInfoDto()
        {
            EICS_Details = new List<RequestCreateEICSDetailDto>();
        }
        public int MMSI { get; set; }
        public IList<RequestCreateEICSDetailDto> EICS_Details { get; set; }
    }
}
