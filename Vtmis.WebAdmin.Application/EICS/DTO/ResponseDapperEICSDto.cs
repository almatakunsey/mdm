﻿using System.Collections.Generic;

namespace Vtmis.WebAdmin.EICS.DTO
{
    public class ResponseDapperEICSDto
    {
        public List<string> Equipment { get; set; }
    }
}
