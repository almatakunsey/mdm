﻿using Dapper;
using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.WebAdmin.TenantFilters;

namespace Vtmis.WebAdmin.EICS
{
    public class TargetOwner
    {
        public TargetOwner()
        {
            TargetIdentifiers = new List<TargetIdentifier>();
        }
        public bool IsHost { get; set; }
        public List<TargetIdentifier> TargetIdentifiers { get; set; }
    }
    public class TargetIdentifier
    {
        public int MMSI { get; set; }
    }
    public class DapperAdmin : IDapperAdmin
    {
        private readonly NpgsqlConnection _dapper;

        public DapperAdmin(NpgsqlConnection dapper)
        {
            _dapper = dapper;
        }

        public async Task<Expression<Func<VesselPosition, bool>>> GetTenantFilter(string userId)
        {
            Expression<Func<VesselPosition, bool>> vp = x => true;
            long id = Convert.ToInt64(userId);
            _dapper.ConnectionString = AppHelper.GetMdmDbConnectionString();
            var userType = await GetUserTypeAsync(id);

            if (userType == UserType.HostAdmin || userType == UserType.HostUser)
            {
                return vp;
            }

            var tenantId = await _dapper.QueryFirstOrDefaultAsync<int?>(@"
                        SELECT ""TenantId"" from public.""AbpUsers""                          
                        WHERE ""Id"" = @userId", new { userId = id });

            if (tenantId == null)
            {
                return vp;
            }

            //List<int> tenantFilterId = new List<int>();
            if (userType == UserType.TenantAdmin || userType == UserType.TenantUser)
            {
                var tenantFilterId = await _dapper.QueryAsync<int>(@"
                        SELECT ""Id"" from public.""TenantFilters""                          
                        WHERE @tenantId = ANY (""AssignedTenants"")", new { tenantId });

                if (tenantFilterId.Any())
                {
                    var count = 0;
                    foreach (var item in tenantFilterId)
                    {
                        var tenantFilterItem = await _dapper.QueryAsync<TenantFilterItem>(@"
                         SELECT ""Condition"", ""ColumnName"",""Operation"", ""ColumnValue"", ""RowIndex"" from public.""TenantFilterItem""                          
                         WHERE ""TenantFilterId"" = @tenantFilterId
                         ORDER BY ""RowIndex""", new { tenantFilterId = item }
                         );

                        if (tenantFilterItem.Any() == false)
                        {
                            return vp;
                        }

                        var f = CombineFilter(tenantFilterItem);
                        if (count == 0)
                        {
                            vp = f;
                        }
                        else
                        {
                            vp = vp.Or(f);
                        }

                        count += 1;
                    }
                }

                if (userType == UserType.TenantAdmin)
                {
                    return vp;
                }

                var tenantFilterForUser = await _dapper.QueryAsync<int>(@"
                        SELECT ""Id"" from public.""TenantFilters""                          
                        WHERE @id = ANY (""AssignedUsers"")", new { id });

                if (tenantFilterForUser.Any() == false)
                {
                    return vp;
                }

                Expression<Func<VesselPosition, bool>> vpForUser = x => true;
                var countForUser = 0;
                foreach (var item in tenantFilterForUser)
                {
                    var tenantFilterItem = await _dapper.QueryAsync<TenantFilterItem>(@"
                         SELECT ""Condition"", ""ColumnName"",""Operation"", ""ColumnValue"", ""RowIndex"" from public.""TenantFilterItem""                          
                         WHERE ""TenantFilterId"" = @tenantFilterId
                         ORDER BY ""RowIndex""", new { tenantFilterId = item }
                     );

                    if (tenantFilterItem.Any() == false)
                    {
                        return vp;
                    }

                    var f = CombineFilter(tenantFilterItem);
                    if (countForUser == 0)
                    {
                        vpForUser = f;
                    }
                    else
                    {
                        vpForUser = vpForUser.Or(f);
                    }

                    countForUser += 1;
                }

                if (tenantFilterId.Any() == false)
                {
                    return vpForUser;
                }
                else
                {
                    return vp.And(vpForUser);
                }
            }
            return vp;
        }

        public Expression<Func<VesselPosition, bool>> CombineFilter(IEnumerable<TenantFilterItem> tenantFilterItems)
        {
            Expression<Func<VesselPosition, bool>> vp = x => true;
            var counter = 0;
            var itemCount = tenantFilterItems.Count();
            var items = tenantFilterItems.ToList();
            foreach (var item in items)
            {
                Expression<Func<VesselPosition, bool>> innerVp = x => true;
                // handle mmsi filter
                if (item.ColumnName.IsEqual("mmsi"))
                {
                    var mmsisString = item.ColumnValue.Split(",");
                    List<int> mmsis = new List<int>();
                    foreach (var mmsi in mmsisString)
                    {
                        mmsis.Add(mmsi.ToInt());
                    }

                    if (item.Operation.IsEqual("="))
                    {
                        innerVp = x => mmsis.Any(a => a == x.MMSI);
                    }
                    else if (item.Operation.IsEqual("!="))
                    {
                        innerVp = x => mmsis.Any(a => a != x.MMSI);
                    }
                }
                // handle ais type filter
                else if (item.ColumnName.IsEqual("aistype"))
                {
                    if (item.Operation.IsEqual("="))
                    {
                        if (item.ColumnValue.IsEqual("classA"))
                        {
                            innerVp = x => (x.MessageId == 1 || x.MessageId == 2 || x.MessageId == 3);
                        }
                        else if (item.ColumnValue.IsEqual("classB"))
                        {
                            innerVp = x => (x.MessageId == 18 || x.MessageId == 19);
                        }
                        else if (item.ColumnValue.IsEqual("aton"))
                        {
                            innerVp = x => (x.MessageId == 21 || x.MessageId == 8);
                        }
                        else if (item.ColumnValue.IsEqual("sar"))
                        {
                            innerVp = x => (x.MessageId == 9);
                        }
                        else if (item.ColumnValue.IsEqual("basestation"))
                        {
                            innerVp = x => (x.MessageId == 4);
                        }
                    }
                    else if (item.Operation.IsEqual("!="))
                    {
                        if (item.ColumnValue.IsEqual("classA"))
                        {
                            innerVp = x => (x.MessageId != 1 || x.MessageId != 2 || x.MessageId != 3);
                        }
                        else if (item.ColumnValue.IsEqual("classB"))
                        {
                            innerVp = x => (x.MessageId != 18 || x.MessageId != 19);
                        }
                        else if (item.ColumnValue.IsEqual("aton"))
                        {
                            innerVp = x => (x.MessageId != 21 || x.MessageId != 8);
                        }
                        else if (item.ColumnValue.IsEqual("sar"))
                        {
                            innerVp = x => (x.MessageId != 9);
                        }
                        else if (item.ColumnValue.IsEqual("basestation"))
                        {
                            innerVp = x => (x.MessageId != 4);
                        }
                    }
                }
                //handle ship type filter
                else if (item.ColumnName.IsEqual("shiptype"))
                {
                    if (item.Operation.IsEqual("="))
                    {
                        if (ShipTypeConst.All().Any(a => a.IsEqual(item.ColumnValue)))
                        {
                            innerVp = x => (x.ShipType_ToString.IsEqual(item.ColumnValue));
                        }
                    }
                    else if (item.Operation.IsEqual("!="))
                    {
                        innerVp = x => (x.ShipType_ToString.IsEqual(item.ColumnValue) == false);
                    }
                }

                if (counter == 0)
                {
                    vp = innerVp;
                }
                else
                {
                    if (item.Condition == Condition.AND)
                    {
                        vp = vp.And(innerVp);
                    }
                    else
                    {
                        vp = vp.Or(innerVp);
                    }
                }
                counter += 1;
            }
            return vp;
        }

        public async Task<UserType> GetUserTypeAsync(long userId)
        {
            _dapper.ConnectionString = AppHelper.GetMdmDbConnectionString();
            UserType userType = UserType.TenantUser;
            var isTenant = await _dapper.QueryFirstOrDefaultAsync<(int? tenantId, string username)>(@"
                        SELECT ""TenantId"", ""UserName"" from public.""AbpUsers""                          
                        where ""Id"" = @userId", new { userId });

            if (isTenant.tenantId == null)
            {
                userType = UserType.HostAdmin;
            }

            if (isTenant.username.IsEqual("admin"))
            {
                userType = UserType.TenantAdmin;
            }
            return userType;
        }

        public async Task<TargetOwner> GetTargetsByUserId(int userId)
        {
            _dapper.ConnectionString = AppHelper.GetMdmDbConnectionString();
            bool isTenantAdmin = false;
            var isTenant = await _dapper.QueryFirstOrDefaultAsync<(int?, string)>(@"
                        SELECT ""TenantId"", ""UserName"" from public.""AbpUsers""                          
                        where ""Id"" = @userId", new { userId });

            string where = string.Empty;
            object param = new { tenantId = isTenant.Item1 };
            if (isTenant.Item2.IsEqual("admin") == false)
            {
                isTenantAdmin = true;
                where = $@"AND ""UserId""=@userId";
                param = new { tenantId = isTenant.Item1, userId };
            }

            var query = await _dapper.QueryAsync<string>($@"
                        SELECT ""MMSI"" from public.""VesselOwned""                          
                        where ""TenantId"" = @tenantId {where}", param);



            if (query.Any() == false && isTenant.Item2 == null)
            {
                return new TargetOwner();
            }
            if (string.IsNullOrEmpty(isTenant.Item2) == false && isTenant.Item1 == null)
            {
                return new TargetOwner { IsHost = true };
            }
            if (query.Any() == false)
            {
                return new TargetOwner();
            }

            HashSet<int> mmsis = new HashSet<int>();

            foreach (var item in query)
            {
                var data = JsonConvert.DeserializeObject<List<TargetIdentifier>>(item);
                if (data.Any())
                {
                    foreach (var i in data)
                    {
                        mmsis.Add(i.MMSI);
                    }
                }
            }
            return new TargetOwner
            {
                IsHost = false,
                TargetIdentifiers = mmsis.Select(s => new TargetIdentifier { MMSI = s }).ToList()
            };
        }

        public EICSModel QueryEICS(int mmsi)
        {
            _dapper.ConnectionString = AppHelper.GetMdmDbConnectionString();


            var query = _dapper.Query<string>(@"
                        select b.""Name"" Equipments from public.""EICS_Details"" a
                        inner join public.""EICS_Equipment"" b on b.""Id"" = a.""EquipmentId""
                        left join public.""EICS_Infos"" c on c.""Id"" = a.""EICS_infoid""
                        where c.""MMSI"" = @mmsi", new { mmsi });

            var result = new EICSModel
            {
                Equipments = query.ToList()
            };

            return result;
        }

        public string QueryLloydsShipType(int? imo, string callSign)
        {
            _dapper.ConnectionString = AppHelper.GetMdmDbConnectionString();
            if (imo == null && string.IsNullOrWhiteSpace(callSign))
            {
                return null;
            }

            if (imo != null)
            {
                var query = _dapper.QueryFirstOrDefault<string>($@"
                        select ""VesselType"" from public.""Lloyds"" a
                        WHERE ""IMO""=@imo LIMIT 1", new { imo });
                return query;
            }

            if (string.IsNullOrWhiteSpace(callSign))
            {
                var query = _dapper.QueryFirstOrDefault<string>($@"
                        select ""VesselType""  from public.""Lloyds"" a
                        WHERE ""CallSign""=@callSign", new { callSign });
                return query;
            }
            return null;
        }
    }
}
