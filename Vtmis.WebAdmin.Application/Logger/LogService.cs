﻿using Abp.Application.Services;
using Abp.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.Logger
{
    public class LogService : ApplicationService, ILogService
    {
        private readonly IRepository<Logger> _repo;
        public LogService(IRepository<Logger> repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<Logger>> GetAllLogAsync()
        {
            return await _repo.GetAll().OrderByDescending(x => x.CreationTime).ToListAsync();
        }

        public async Task<IEnumerable<Logger>> GetAllLogByDateTimeAsync(DateTime input)
        {
            return await _repo.GetAll().Where(x => x.CreationTime.Date == input.Date).OrderByDescending(x => x.CreationTime).ToListAsync();
        }

        public async Task CreateLogAsync(Logger logger)
        {
            await _repo.InsertAndGetIdAsync(logger);

        }
    }
}
