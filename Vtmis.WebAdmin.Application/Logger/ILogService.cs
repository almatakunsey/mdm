﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.Logger
{
    public interface ILogService
    {
        Task<IEnumerable<Logger>> GetAllLogAsync();
        Task<IEnumerable<Logger>> GetAllLogByDateTimeAsync(DateTime input);
        Task CreateLogAsync(Logger logger);
    }
}
