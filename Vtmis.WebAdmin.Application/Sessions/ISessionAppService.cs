﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Vtmis.WebAdmin.Sessions.Dto;

namespace Vtmis.WebAdmin.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
