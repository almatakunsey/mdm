﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Auditing;
using Microsoft.AspNetCore.Mvc;
using Vtmis.WebAdmin.Sessions.Dto;
using Vtmis.WebAdmin.SignalR;

namespace Vtmis.WebAdmin.Sessions
{
    //[RemoteService(IsMetadataEnabled = false)]
    public class SessionAppService : WebAdminAppServiceBase, ISessionAppService
    {
        [DisableAuditing]
        public async Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations()
        {
            var output = new GetCurrentLoginInformationsOutput
            {
                Application = new ApplicationInfoDto
                {
                    Version = AppVersionHelper.Version,
                    ReleaseDate = AppVersionHelper.ReleaseDate,
                    Features = new Dictionary<string, bool>
                    {
                        { "SignalR", SignalRFeature.IsAvailable }
                    }
                }
            };

            if (AbpSession.TenantId.HasValue)
            {
                output.Tenant = ObjectMapper.Map<TenantLoginInfoDto>(await GetCurrentTenantAsync());
            }

            if (AbpSession.UserId.HasValue)
            {
                output.User = ObjectMapper.Map<UserLoginInfoDto>(await GetCurrentUserAsync());
            }

            return output;
        }
        [HttpGet]
        public bool CheckSession()
        {
            return AbpSession.UserId != null ? true : false;
        }
    }
}
