﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Authorization;
using Abp.Runtime.Session;
using Vtmis.WebAdmin.Configuration.Dto;

namespace Vtmis.WebAdmin.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : WebAdminAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
