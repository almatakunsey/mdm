﻿using System.Threading.Tasks;
using Vtmis.WebAdmin.Configuration.Dto;

namespace Vtmis.WebAdmin.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
