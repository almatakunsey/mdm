﻿using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.Configuration.Dto
{
    public class ChangeUiThemeInput
    {
        [Required]
        [StringLength(32)]
        public string Theme { get; set; }
    }
}
