﻿using Microsoft.AspNetCore.Mvc.Formatters;
using Newtonsoft.Json;
using System.Buffers;
using System.Text;

namespace Vtmis.Api.MMDIS.Helper
{
    public class ForcedUtf8JsonFormatter : JsonOutputFormatter
    {
        public ForcedUtf8JsonFormatter(JsonSerializerSettings serializerSettings, ArrayPool<char> charPool)
            : base(serializerSettings, charPool)
        {
        }

        public override Encoding SelectCharacterEncoding(OutputFormatterWriteContext context)
        {
            return Encoding.UTF8;
        }
    }
}
