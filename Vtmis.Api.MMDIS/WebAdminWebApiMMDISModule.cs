﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Vtmis.WebAdmin.Configuration;
using Vtmis.WebAdmin;
using Vtmis.Api.MMDIS.Controllers;

namespace Vtmis.Api.MMDIS
{
    //[Abp.Modules.DependsOn(typeof(WebAdminWebCoreModule))]
    [DependsOn(
        typeof(WebAdminWebCoreModule))]
    public class WebAdminWebApiMMDISModule:AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public WebAdminWebApiMMDISModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WebAdminWebApiMMDISModule).GetAssembly());
            //IocManager.Register<ValuesController>(Abp.Dependency.DependencyLifeStyle.Transient);
            //IocManager.Register<ZonesController>(Abp.Dependency.DependencyLifeStyle.Transient);
        }
    }
}
