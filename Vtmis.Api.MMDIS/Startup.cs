﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Abp.AspNetCore;
using Castle.Facilities.Logging;
using Abp.Castle.Logging.Log4Net;
using Vtmis.WebAdmin.Identity;
using Vtmis.Core.VesselEntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Swagger;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using Vtmis.WebVessel.Tracking.Services.Concrete;
using Akka.Configuration;
using Akka.Actor;
using Akka.Routing;
using Vtmis.Api.MMDIS.Models;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using System.Linq;
using Vtmis.Api.MMDIS.Helper;
using Newtonsoft.Json;
using System.Buffers;
using Vtmis.Core.Common.Helpers;
using AutoMapper;
using Vtmis.Core.Common;
using Microsoft.AspNetCore.Http;
using Vtmis.Core.Common.Constants;
using System.Net.Sockets;

namespace Vtmis.Api.MMDIS
{
    public class Startup
    {
        private const string _defaultCorsPolicyName = "localhost";
        private readonly IHostingEnvironment _env;

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
         
            var connectionString = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("VTS_DB_CONNECTIONSTRING")) ?
               AppHelper.GetVtsDbConnectionString() : Environment.GetEnvironmentVariable("VTS_DB_CONNECTIONSTRING");
            
            services.AddMvc(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory(_defaultCorsPolicyName));
                options.OutputFormatters.Add(new ForcedUtf8JsonFormatter(new JsonSerializerSettings(), ArrayPool<char>.Shared));

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            // TODO: SSLConfig
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    services.AddHsts(options =>
            //    {
            //        options.Preload = true;
            //        options.IncludeSubDomains = true;
            //        options.MaxAge = TimeSpan.FromHours(23);
            //    });
            //    services.AddHttpsRedirection(options =>
            //    {
            //        options.RedirectStatusCode = StatusCodes.Status308PermanentRedirect;
            //        options.HttpsPort = 50838;
            //    });
            //}

            services.AddCors(options =>
            {
                options.AddPolicy(_defaultCorsPolicyName, builder =>
                {
                    // App:CorsOrigins in appsettings.json can contain more than one address separated by comma.
                    builder
                        .WithOrigins(AppHelper.GetCorsOriginList().Split(",", StringSplitOptions.RemoveEmptyEntries)
                                                                         .ToArray())
                         .SetIsOriginAllowed(isOriginAllowed: _ => true)
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            IdentityRegistrar.Register(services);

            services.AddDbContext<VtsDatabaseContext>(o => o.UseSqlServer(connectionString), ServiceLifetime.Scoped);
            services.AddTransient<IVtsVesselService, VtsVesselService>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "MMDIS API"
                });
                c.DocInclusionPredicate((docName, description) => true);
            });
            //services.AddAutoMapper();
            return services.AddAbp<WebAdminWebApiMMDISModule>(options =>
            {
                // Configure Log4Net logging
                options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig("log4net.config")
                );
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseAbp(options => { options.UseAbpRequestLocalization = false; });

            if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.LOCAL))
            {
                app.UseDeveloperExceptionPage();
            }
            // TODO: SSLConfig
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    app.UseHsts();
            //    app.UseHttpsRedirection();
            //}
            app.UseAbpRequestLocalization();
            app.UseStaticFiles();
            app.UseCors(_defaultCorsPolicyName);
            app.UseMvc();
           
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API MMDIS");
            });
        }
    }
}
