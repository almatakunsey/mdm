﻿using Abp.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Mmdis.Dto;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto;
using Vtmis.WebVessel.Tracking.Services.Interfaces;


namespace Vtmis.Api.MMDIS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VesselsController : AbpController
    {
        private readonly IVtsVesselService _vtsVesselService;
        public VesselsController(IVtsVesselService vtsVesselService)
        {
            _vtsVesselService = vtsVesselService;
        }

        /// <summary>
        /// TODO: Change this after Alarm Conditional has been identified
        /// </summary>
        /// <param name="alarmEvents"></param>
        /// <returns></returns>
        //[HttpPost("light/update")]
        //public async Task<IActionResult> UpdateOnLightErrors([FromBody]List<AlarmEvent> alarmEvents)
        //{
        //    var result = await _vtsVesselService.UpdateDredgingAlarmListAsync(alarmEvents);
        //    if (result)
        //    {
        //        return Ok();
        //    }
        //    return BadRequest();
        //}

        [HttpPost("create")]
        public async Task<IActionResult> Create(CreateExtendedShipList input)
        {
            try
            {
                if (input.MMSI == 0 && string.IsNullOrEmpty(input.IMO) && string.IsNullOrEmpty(input.CallSign))
                {
                    return BadRequest("Please fill in one of the items: MMSI, Call Sign, IMO");
                }
                var result = await _vtsVesselService.CreateVesselAsync(input);
                return Created("/api/vessels/single", new { result });
            }
            catch (AlreadyExistException err)
            {
                return BadRequest(err.Message);
            }
            catch (ServerFaultException err)
            {
                throw err;
            }
        }

        [HttpPut("update")]
        public async Task<IActionResult> Update([FromBody]UpdateExtendedShipList input)
        {
            try
            {
                if (input.MMSI == 0 && string.IsNullOrEmpty(input.IMO) && string.IsNullOrEmpty(input.CallSign))
                {
                    return BadRequest("Please fill in one of the items: MMSI, Call Sign, IMO");
                }
                return Ok(await _vtsVesselService.UpdateVesselAsync(input));
            }
            catch (NotFoundException err)
            {
                return BadRequest(err.Message);
            }
            catch (AlreadyExistException err)
            {
                return BadRequest(err.Message);
            }
            catch (ServerFaultException err)
            {
                throw err;
            }
        }

        [HttpGet]
        public IActionResult Get(int pageIndex = 1, int pageSize = 10, bool includeDeletedShips = false)
        {
            try
            {
                return Ok(_vtsVesselService.GetAllShips(pageIndex, pageSize, includeDeletedShips));
            }
            catch (ServerFaultException err)
            {
                throw err;
            }
        }

        [HttpPost]
        [Route("single")]
        public async Task<IActionResult> GetShip([FromBody]IdMmsiImoCallSign input)
        {
            try
            {
                if (input.TargetId == 0 && input.Mmsi == 0 && string.IsNullOrEmpty(input.Imo)
                 && string.IsNullOrEmpty(input.CallSign))
                {
                    return BadRequest();
                }
                return Ok(await _vtsVesselService.GetShip(input.TargetId,
                    input.Mmsi, input.Imo, input.CallSign));
            }
            catch (NotFoundException err)
            {
                return BadRequest(err);
            }
            catch (ServerFaultException err)
            {
                throw err;
            }
        }

        [HttpPost]
        [Route("blacklist")]
        public async Task<IActionResult> Blacklist([FromBody]IdMmsiImoCallSign input)
        {
            try
            {
                if (input.TargetId == 0 && input.Mmsi == 0 && string.IsNullOrEmpty(input.Imo)
                   && string.IsNullOrEmpty(input.CallSign))
                {
                    return BadRequest();
                }
                await _vtsVesselService.BlackListVesselAsync(input.TargetId, input.Mmsi, input.Imo, input.CallSign);
                return Ok();
            }
            catch (NotFoundException err)
            {
                return BadRequest(err);
            }
            catch (ServerFaultException err)
            {
                throw err;
            }
        }

        [HttpDelete]
        [Route("remove")]
        public async Task<IActionResult> Delete([FromBody]IdMmsiImoCallSign input)
        {
            try
            {
                if (input.TargetId == 0 && input.Mmsi == 0 && string.IsNullOrEmpty(input.Imo)
                   && string.IsNullOrEmpty(input.CallSign))
                {
                    return BadRequest();
                }
                await _vtsVesselService.SoftDeleteVesselAsync(input.TargetId, input.Mmsi, input.Imo, input.CallSign);
                return Ok();
            }
            catch (NotFoundException err)
            {
                return BadRequest(err);
            }
            catch (ServerFaultException err)
            {
                throw err;
            }
        }
    }
}
