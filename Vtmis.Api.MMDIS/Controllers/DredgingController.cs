﻿using Abp.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.Api.MMDIS.Controllers
{
    public class DredgingController : AbpController
    {
        private readonly IVtsVesselService _vtsVesselService;
        public DredgingController(IVtsVesselService vtsVesselService)
        {
            _vtsVesselService = vtsVesselService;
        }

        //[Route("update")]
        //[HttpPost]
        //public async Task<IActionResult> UpdateDredgingAlarm([FromBody]List<AlarmEvent> alarmEvents)
        //{
        //    var result = await _vtsVesselService.UpdateDredgingAlarmListAsync(alarmEvents);
        //    if (result)
        //    {
        //        return Ok();
        //    }
        //    return BadRequest();
        //}
    }
}
