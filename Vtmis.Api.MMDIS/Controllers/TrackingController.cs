﻿using Abp.AspNetCore.Mvc.Controllers;
using Akka.Actor;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Mmdis.Dto;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.Api.MMDIS.Controllers
{
    [Route("api/[controller]/[action]")]
    public class TrackingController : AbpController
    {
        private readonly IVtsVesselService _vtsVesselService;
        public TrackingController(IVtsVesselService vtsVesselService)
        {
            _vtsVesselService = vtsVesselService;
        }

        [HttpPost]
        public MmdisTargetInfo GetLastPosition([FromBody]MmsiImoCallSignParams input)
        {

            var result = QueryLastTargetPosition(new CommonTargetParams(input.TargetId, input.Mmsi, input.Imo, input.CallSign));
            if (result != null)
            {
                result.Flag = _vtsVesselService.GetFlagNameByMmsi(result.MMSI);
            }
            return result;
        }

        private MmdisTargetInfo QueryLastTargetPosition(CommonTargetParams targetParams)
        {
            try
            {
                //   _aisDbInfo = CommonHelper.GetAisDbInfo(_paramObject.Environment, _paramObject.DbDate, _paramObject.ServerName,
                //_paramObject.Username, _paramObject.Password);
                using (var dbContext = new VesselDatabaseContext())
                {
                    //Console.WriteLine($"DB Name:{_postFixDbName} || Server Name:{ServerName} || Username:{Username} || Password:{Password}");
                    var shipsResult = dbContext.ShipStatics.Join(dbContext.ShipPositions, s => s.MMSI, p => p.MMSI, (s, p) =>
                                    new
                                    { s, p });

                    if (targetParams.MMSI > 0)
                    {
                        shipsResult = shipsResult.Where(x => x.s.MMSI == targetParams.MMSI);
                    }
                    if (targetParams.IMO > 0)
                    {
                        shipsResult = shipsResult.Where(x => x.s.IMO == targetParams.IMO);
                    }
                    if (!string.IsNullOrEmpty(targetParams.CallSign))
                    {
                        shipsResult = shipsResult.Where(x => x.s.CallSign == targetParams.CallSign);
                    }

                    var result = targetParams.TargetId > 0 ? dbContext.ShipStatics.Join(dbContext.ShipPositions, s => s.MMSI, p => p.MMSI, (s, p) =>
                                    new
                                    { s, p }).Where(x => x.s.MMSI == targetParams.TargetId).OrderByDescending(o => o.p.RecvTime)
                                    .Select(s => new
                                    {
                                        s.s.Name,
                                        s.s.IMO,
                                        s.s.MMSI,
                                        s.s.CallSign,
                                        s.p.Latitude,
                                        s.p.Longitude,
                                        s.s.ShipType,
                                        s.p.ROT,
                                        s.p.SOG,
                                        s.p.COG,
                                        s.p.TrueHeading,
                                        s.p.RecvTime,
                                        s.p.UTC,
                                        s.p.LocalRecvTime
                                    }).FirstOrDefault() :
                                    shipsResult.OrderByDescending(o => o.p.RecvTime)
                                    .Select(s => new
                                    {
                                        s.s.Name,
                                        s.s.IMO,
                                        s.s.MMSI,
                                        s.s.CallSign,
                                        s.p.Latitude,
                                        s.p.Longitude,
                                        s.s.ShipType,
                                        s.p.ROT,
                                        s.p.SOG,
                                        s.p.COG,
                                        s.p.TrueHeading,
                                        s.p.RecvTime,
                                        s.p.UTC,
                                        s.p.LocalRecvTime
                                    }).FirstOrDefault();


                    if (result != null)
                    {
                        return new MmdisTargetInfo(result.Name, result.IMO, result.MMSI, null, result.CallSign,
                             result.Latitude, result.Longitude, result.ShipType, (float)result.ROT, (float)result.SOG,
                             (float)result.COG, result.TrueHeading, result.RecvTime, result.UTC, result.LocalRecvTime);
                    }
                    else
                    {
                        return new MmdisTargetInfo();
                    }

                }

            }
            catch (Exception err)
            {

                throw err;
            }

        }
    }
}
