﻿using Abp.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Mmdis.Dto;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vts.Dto;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.Api.MMDIS.Controllers
{
    [Route("api/[controller]")]
    public class ClearanceController : AbpController
    {
        private readonly IVtsVesselService _vtsVesselService;
        public ClearanceController(IVtsVesselService vtsVesselService)
        {
            _vtsVesselService = vtsVesselService;
        }

        [Route("{targetId}")]
        [HttpGet]
        public async Task<IActionResult> Get(int targetId)
        {
            return Ok(await _vtsVesselService.GetVesselClearanceByTargetIdAsync(targetId));
        }


        [HttpPost]
        [Route("")]
        public async Task<VesselClearance> Get([FromBody]MmsiImoCallSignWithNullParams input)
        {
            return await _vtsVesselService.GetVesselClearanceByTargetMmsiImoCallSignAsync(input.Mmsi, input.Imo, input.CallSign);
        }

        //[Route("update")]
        //[HttpPost]
        //public async Task<IActionResult> UpdateTargetClearance([FromBody]List<VesselClearance> vesselClearanceList)
        //{
        //    var result = await _vtsVesselService.UpdateVesselClearanceListAsync(vesselClearanceList);
        //    if (result)
        //    {
        //        return Ok();
        //    }
        //    return BadRequest();
        //}
    }
}
