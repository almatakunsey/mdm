﻿using Abp.Authorization;
using Abp.Dependency;
using Abp.Runtime.Session;
using Akka.Actor;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading;
using System.Threading.Tasks;
using Vtmis.Core.ActorModel.Actors;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.WebAdmin.EICS;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Hangfire;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;

namespace Vtmis.Core.ActorModel.Hubs
{
    [AbpAuthorize]
    public class VesselGeoLocationHub : Hub, ITransientDependency
    {
        //private readonly ActorSystem _actorSystem;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        private readonly ITargetQuery _targetQuery;
        private readonly IDapperAdmin _dapperAdmin;
        private readonly IHubContext<VesselGeoLocationHub> _hubContext;
        protected IActorRef _actorRef;
        protected int count;
        private readonly ISubscriber _redisSub;
        protected VtsDatabaseContext _vtsDbContext;

        public MapTransactionSpecification _mapTransaction { get; }
        public IAbpSession AbpSession { get; set; }

        private readonly static ConnectionMapping<string> _connections =
           new ConnectionMapping<string>();

        public VesselGeoLocationHub(MapTransactionSpecification mapTransaction, IHostingEnvironment hostingEnvironment,
            IConnectionMultiplexer connectionMultiplexer, ITargetQuery targetQuery, IDapperAdmin dapperAdmin, IHubContext<VesselGeoLocationHub> hubContext)
        {
            Console.WriteLine("Start VesselGeoLocationHub");
            AbpSession = NullAbpSession.Instance;
            //_actorSystem = actorSystem;
            _mapTransaction = mapTransaction;
            _hostingEnvironment = hostingEnvironment;
            _connectionMultiplexer = connectionMultiplexer;
            _targetQuery = targetQuery;
            _dapperAdmin = dapperAdmin;
            _hubContext = hubContext;
            count = 1;
            _redisSub = _connectionMultiplexer.GetSubscriber();
        }

        public override async Task OnConnectedAsync()
        {
            try
            {               
                string name = Context.User.Identity.Name;

                Console.WriteLine("Connect " + Context.ConnectionId);

                _connections.Add(name, Context.ConnectionId);
            }
            catch { }

            await base.OnConnectedAsync();

            await SubsribeBus(Context.ConnectionId, Context.UserIdentifier);
        }

        private async Task SubsribeBus(string connectionId, string userId)
        {
            RecurringJob
               .AddOrUpdate<ITargetQuery>($"targets_{AppHelper.GetEnvironmentName()}_{connectionId}",
                   x => x.QueryUpdateTargets(DateTime.Now.AddMinutes(-1).ToUniversalTime(), connectionId), Cron.Minutely);
            Console.WriteLine("subscribe");
            var filter = await _dapperAdmin.GetTenantFilter(userId);
            await _redisSub.SubscribeAsync($"targetsub_{AppHelper.GetEnvironmentName()}_{connectionId}", async (channel, message) =>
            {
                var data = JsonConvert.DeserializeObject<List<Vtmis.Core.Model.AkkaModel.Messages.VesselPosition>>(message);
                await _hubContext.Clients.User(userId).SendAsync("UpdateCurrentVessel", new Vessel
                {
                    VesselPositions = data.Where(filter.Compile()).ToList()
                });
                Console.WriteLine("Published");
            });
            Console.WriteLine("finish subscribe");
        }

        private async Task UnsubsribeBus(string connectionId)
        {
            Console.WriteLine("unsubscribe");
            RecurringJob.RemoveIfExists($"targets_{AppHelper.GetEnvironmentName()}_{connectionId}");
            await _redisSub.UnsubscribeAsync($"targetsub_{AppHelper.GetEnvironmentName()}_{connectionId}");
        }

        public override async Task OnDisconnectedAsync(Exception stopCalled)
        {
            try
            {
                await UnsubsribeBus(Context.ConnectionId);
                string name = Context.User.Identity.Name;

                Console.WriteLine("Disconnect " + Context.ConnectionId);

                _connections.Remove(name, Context.ConnectionId);
            }
            catch { }

            await base.OnDisconnectedAsync(stopCalled);
        }
       
        public void CheckEvents(string message)
        {

        }

        public async Task RestoreHistory()
        {
            var filter = await _dapperAdmin.GetTenantFilter(Context.UserIdentifier);
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Restore History");
            Console.ResetColor();
            Console.WriteLine($"User ||{Context.UserIdentifier}");
            var connectionId = Context.ConnectionId;
            //TargetOwner userTargets = await _dapperAdmin.GetTargetsByUserId(Context.UserIdentifier.ToInt());

            int pageSize = 4000;
            int skip = 0;

            var redisDb = _connectionMultiplexer.GetDatabase();
            var historyKeys = await redisDb.HashGetAllAsync($"vesselHistory_{AppHelper.GetEnvironmentName()}");
            List<VesselPosition> histories = new List<VesselPosition>();

            if (historyKeys.Any())
            {
                foreach (var keys in historyKeys)
                {
                    var obj = JsonConvert.DeserializeObject<VesselPosition>(keys.Value);
                    histories.Add(obj);
                }
            }

            //var data = userTargets.IsHost ? histories :
            //    histories.Where(c => userTargets.TargetIdentifiers.Any(a => c.MMSI == a.MMSI)).ToList();
            var data = histories.Where(filter.Compile());

            var totalRecords = data.Count();

            Console.WriteLine("Total records " + totalRecords);
            for (int i = 1; skip <= totalRecords; i++)
            {

                var take = pageSize;

                var vesselPositionsHistory = data.Take(take).Skip(skip).ToList();

                await Clients.User(Context.UserIdentifier).SendAsync("UpdateRestoreVessel",
                    new Vessel
                    {
                        VesselPositions = vesselPositionsHistory
                    });

                skip = i * take;

                var restore = 0;

                if (totalRecords <= skip)
                    restore = skip;
                else
                    restore = totalRecords - skip;

                Console.WriteLine($"{_hostingEnvironment.EnvironmentName} || Restoring vessel - {restore}");
            }
        }

        public void ReplayServer(string message)
        {
            var connectionId = Context.ConnectionId;
            _mapTransaction.AddSpecificationTransaction(connectionId, TranSpec.Replay).SetReplay(true);

            Console.WriteLine("Start replay server");
        }
    };
}
