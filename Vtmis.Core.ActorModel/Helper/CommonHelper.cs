﻿using System;

namespace Vtmis.Core.ActorModel.Helper
{
    public static class CommonHelper
    {

        public static double GetLiveValue(DateTime receivedTime)
        {
            var diffTime = DateTime.Now - receivedTime;
            if (diffTime.TotalHours < 1)
            {
                return Math.Round(diffTime.TotalMinutes, 2);
            }
            return 0;
        }
        public static double GetOutOfRangeValue(DateTime receivedTime)
        {
            var diffTime = DateTime.Now - receivedTime;
            if (diffTime.TotalHours > 0)
            {
                return Math.Round(diffTime.TotalMinutes, 2);
            }
            return 0;
        }
        public static string GetFlagIcon(string flagName)
        {
            return null;
        }
        public static string GetFlagName(int? mmsi)
        {
            if (mmsi == null)
            {
                return null;
            }
            var flagId = Convert.ToInt32(mmsi.ToString().Substring(0, 3));
            if (flagId == 201)
            {
                return "Albania";
            }
            else if (flagId == 202)
            {
                return "Andorra";
            }
            else if (flagId == 203)
            {
                return "Austria";
            }
            else if (flagId == 204)
            {
                return "Azores";
            }
            else if (flagId == 205)
            {
                return "Belgium";
            }
            else if (flagId == 206)
            {
                return "Belarus";
            }
            else if (flagId == 207)
            {
                return "Bulgaria";
            }
            else if (flagId == 208)
            {
                return "Vatican City State";
            }
            else if (flagId == 209 || flagId == 210)
            {
                return "Cyprus";
            }
            else if (flagId == 211)
            {
                return "Germany";
            }
            else if (flagId == 212)
            {
                return "Cyprus";
            }
            else if (flagId == 213)
            {
                return "Georgia";
            }
            else if (flagId == 214)
            {
                return "Moldova";
            }
            else if (flagId == 215)
            {
                return "Malta";
            }
            else if (flagId == 216)
            {
                return "Armenia";
            }
            else
            {
                return null;
            }
        }
        public static string GetTargetTypeName(int? targetType)
        {
            if ((targetType >= 10) && (targetType <= 19))
            {
                return "Reserved";
            }
            else if ((targetType >= 20) && (targetType <= 28))
            {
                return "Wing In Ground";
            }
            else if (targetType == 29)
            {
                return "SAR Aircraft";
            }
            else if (targetType == 30)
            {
                return "Fishing";
            }
            else if (targetType == 31)
            {
                return "Tug";
            }
            else if (targetType == 32)
            {
                return "Tug";
            }
            else if (targetType == 33)
            {
                return "Dredger";
            }
            else if (targetType == 34)
            {
                return "Dive Vessel";
            }
            else if (targetType == 35)
            {
                return "Military Ops";
            }
            else if (targetType == 36)
            {
                return "Sailing Vessel";
            }
            else if (targetType == 37)
            {
                return "Pleasure Craft";
            }
            else if (targetType == 38)
            {
                return "Reserved";
            }
            else if (targetType == 39)
            {
                return "Reserved";
            }
            else if ((targetType >= 40) && (targetType <= 49))
            {
                return "High-Speed Craft";
            }
            else if (targetType == 50)
            {
                return "Pilot Vessel";
            }
            else if (targetType == 51)
            {
                return "SAR";
            }
            else if (targetType == 52)
            {
                return "Tug";
            }
            else if (targetType == 53)
            {
                return "Port Tender";
            }
            else if (targetType == 54)
            {
                return "Anti-Pollution";
            }
            else if (targetType == 55)
            {
                return "Law Enforce";
            }
            else if (targetType == 56)
            {
                return "Local Vessel";
            }
            else if (targetType == 57)
            {
                return "Local Vessel";
            }
            else if (targetType == 58)
            {
                return "Medical Trans";
            }
            else if (targetType == 59)
            {
                return "Special Craft";
            }
            else if ((targetType >= 60) && (targetType <= 69))
            {
                return "Passenger";
            }
            else if (targetType == 70)
            {
                return "Cargo";
            }
            else if (targetType == 71)
            {
                return "Cargo - Hazard A (Major)";
            }
            else if (targetType == 72)
            {
                return "Cargo - Hazard B";
            }
            else if (targetType == 73)
            {
                return "Cargo - Hazard C (Minor)";
            }
            else if (targetType == 74)
            {
                return "Cargo - Hazard D (Recognizable)";
            }
            else if ((targetType >= 75) && (targetType <= 79))
            {
                return "Cargo";
            }
            else if (targetType == 80)
            {
                return "Tanker";
            }
            else if (targetType == 81)
            {
                return "Tanker - Hazard A (Major)";
            }
            else if (targetType == 82)
            {
                return "Tanker - Hazard B";
            }
            else if (targetType == 83)
            {
                return "Tanker - Hazard C (Minor)";
            }
            else if (targetType == 84)
            {
                return "Tanker - Hazard D (Recognizable)";
            }
            else if ((targetType >= 85) && (targetType <= 89))
            {
                return "Tanker";
            }
            else if ((targetType >= 90) && (targetType <= 99))
            {
                return "Other";
            }
            else if (targetType == 100)
            {
                return "Navigation Aid";
            }
            else if (targetType == 101)
            {
                return "Reference Point";
            }
            else if (targetType == 102)
            {
                return "RACON";
            }
            else if (targetType == 103)
            {
                return "OffShore Structure";
            }
            else if (targetType == 104)
            {
                return "Spare";
            }
            else if (targetType == 105)
            {
                return "Light, without Sectors";
            }
            else if (targetType == 106)
            {
                return "Light, with Sectors";
            }
            else if (targetType == 107)
            {
                return "Leading Light Front";
            }
            else if (targetType == 108)
            {
                return "Leading Light Rear";
            }
            else if (targetType == 109)
            {
                return "Beacon, Cardinal N";
            }
            else if (targetType == 110)
            {
                return "Beacon, Cardinal E";
            }
            else if (targetType == 111)
            {
                return "Beacon, Cardinal S";
            }
            else if (targetType == 112)
            {
                return "Beacon, Cardinal W";
            }
            else if (targetType == 113)
            {
                return "Beacon, Port Hand";
            }
            else if (targetType == 114)
            {
                return "Beacon, Starboard Hand";
            }
            else if (targetType == 115)
            {
                return "Beacon, Preferred Channel Port hand";
            }
            else if (targetType == 116)
            {
                return "Beacon, Preferred Channel Starboard hand";
            }
            else if (targetType == 117)
            {
                return "Beacon, Isolated danger";
            }
            else if (targetType == 118)
            {
                return "Beacon, Safe Water";
            }
            else if (targetType == 119)
            {
                return "Beacon, Special Mark";
            }
            else if (targetType == 120)
            {
                return "Cardinal Mark N";
            }
            else if (targetType == 121)
            {
                return "Cardinal Mark E";
            }
            else if (targetType == 122)
            {
                return "Cardinal Mark S";
            }
            else if (targetType == 123)
            {
                return "Cardinal Mark W";
            }
            else if (targetType == 124)
            {
                return "Port Hand Mark";
            }
            else if (targetType == 125)
            {
                return "Starboard Hand Mark";
            }
            else if (targetType == 126)
            {
                return "Preferred Channel Port Hand";
            }
            else if (targetType == 127)
            {
                return "Preferred Channel Starboard Hand";
            }
            else if (targetType == 128)
            {
                return "Isolated Danger";
            }
            else if (targetType == 129)
            {
                return "Safe Water";
            }
            else if (targetType == 130)
            {
                return "Manned VTS";
            }
            else if (targetType == 131)
            {
                return "Light Vessel";
            }
            else
            {
                return null;
            }
        }
    }
}
