﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Vtmis.Core.ActorModel.Messages;
using Vtmis.Core.VesselEntityFrameworkCore;

namespace Vtmis.Core.ActorModel.Actors
{
    public class VesselReplayQueryActor : ReceiveActor
    {
        protected List<VesselPosition> vesselPositions { get; set; }

        public VesselReplayQueryActor()
        {
            Console.WriteLine("ReplayQueryActor started");


            //Ask
            Receive<RequestCurrentVesselReplayPosition>(m =>
            {
                vesselPositions = new List<VesselPosition>();
                int batchNum = 0;
                int countBatch = 0;

                using (var dbContext = new VesselReplayDatabaseContext(m.PostFixDbName))
                {
                    Console.WriteLine("Start transaction query " + m.PostFixDbName);

                    var si = dbContext.ShipStatics.Where(c => c.MMSI != -1 & (c.MMSI == 372095000)).FirstOrDefault();

                    var vpList = dbContext.ShipPositions.Where(c => c.MMSI == 372095000).OrderBy(o => o.LocalRecvTime)
                    .Select(s => new VesselPosition(s.MMSI, s.Latitude.ToString(), s.Longitude.ToString(),
                            si.Name, si.CallSign, s.LocalRecvTime, si.Destination, si.Vendor, si.IMO, s.ROT,
                            s.SOG, s.COG, s.TrueHeading, si.ShipType, s.MessageId, si.Dimension_A,
                                                      si.Dimension_B, si.Dimension_C, si.Dimension_D, s.PositionAccuracy,
                                                      si.PositionFixingDevice));

                    Console.WriteLine("Count transaction perday " + vpList.Count());


                    foreach (var vp in vpList)
                    {
                        vesselPositions.Add(vp);
                        batchNum++;

                        Console.WriteLine(batchNum);

                        if (batchNum == 10)
                        {
                            Console.WriteLine("Count Batch " + countBatch + " " + m.PostFixDbName);

                            if (Sender.Ask<bool>(new UpdateVesselPosition(vesselPositions, VesselPositionCategory.Replay)).Result)
                            {
                                batchNum = 0;
                                Console.WriteLine("Send vessel position in batch " + vesselPositions.Count() + " from router " + Self.Path);
                                vesselPositions.Clear();
                                countBatch++;
                                Thread.Sleep(1000);
                            }
                        }


                    }

                    if (batchNum < 10)
                    {
                        if (Sender.Ask<bool>(new UpdateVesselPosition(vesselPositions, VesselPositionCategory.Replay)).Result)
                        {
                            Console.WriteLine("Send vessel remaining position in batch " + vesselPositions.Count() + " from router " + Self.Path);
                            vesselPositions.Clear();
                        }
                    }

                    Sender.Tell(new ReplayVessel(m.Count));


                }
            });
        }
    }
}
