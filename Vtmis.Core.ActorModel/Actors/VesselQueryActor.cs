﻿using Akka.Actor;
using Akka.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Common.Helpers.RMS;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;

namespace Vtmis.Core.ActorModel.Actors
{
    public class VesselQueryActor : ReceiveActor
    {
        protected int NumberOfRequest { get; set; }

        private readonly ILoggingAdapter _logger;

        private readonly ITargetQuery _targetQuery;
        private readonly ICacheService _cacheService;

        public VesselQueryActor(ITargetQuery targetQuery, ICacheService cacheService)
        {
            _targetQuery = targetQuery;
            _cacheService = cacheService;
            _logger = Context.GetLogger();
            Console.WriteLine("VesselQueryActor started");

            ReceiveAsync<RequestCurrentVesselPosition>(async m =>
            {
                _logger.Info($"Start RequestCurrentVesselPosition");
                Console.WriteLine($"Start RequestCurrentVesselPosition");               
             
                List<VesselPosition> resultVesselPositions = new List<VesselPosition>();

                try
                {

                    var p0 = DateTime.SpecifyKind(DateTime.Now.AddMinutes(-1), DateTimeKind.Local).ToUniversalTime();
                    var qryTarget = await _targetQuery.QueryUpdateTargetsAsync(p0);

                    var finalResult = from q in qryTarget
                                      select new VesselPosition(q.StationId, q.MMSI, q.Latitude.Value, q.Longitude.Value, q.Name, q.CallSign, q.LocalRecvTime,
                                      q.Destination, q.Vendor, q.IMO, q.ROT, q.SOG, q.COG, q.TrueHeading.HasValue ? q.TrueHeading.Value : (short)0, q.ShipType, q.MessageId, q.Dimension_A,
                                      q.Dimension_B, q.Dimension_C, q.Dimension_D, q.PositionAccuracy,
                                      q.PositionFixingDevice, q.Name.IsEqual("sart active"), q.RAIM, q.RecvTime, q.ETA, (float?)q.MaxDraught, q.NavigationalStatus, q.IP,
                                      q.IsMethydro, q.RMS?.DigitalInput,
                                      _cacheService.GetEICSEQuipment(q.MMSI), _cacheService.GetLloydsShipType(q.IMO, q.CallSign, q.Name),
                                      //msg21
                                      new Msg21(q.RMS?.OffPosition21, q.RMS?.Health21, q.RMS?.AtonStatus, q.RMS?.Light21, q.RMS?.RACON21,
                                      q.MessageId == 21 && RMSHelper.GetRMSType(q.DAC, q.FI) == RMSTypes.LighthouseRenewableEnergy,
                                      q.VirtualAtoN == null ? -1 : q.VirtualAtoN, q.AtoNType),
                                      // rms
                                      new RMSStats(q.RMS?.AnalogueExt1, q.RMS?.AnalogueExt2, q.RMS?.AnalogueInt, q.RMS?.OffPosition68, q.RMS?.Health,
                                      q.RMS?.Light, q.RMS?.RACON, q.RMS?.AtonStatus68, q.RMS?.LanternBatt, q.RMS?.BatteryStat)
                                      );


                    resultVesselPositions = finalResult.ToList();
                    Console.WriteLine($"Total: {resultVesselPositions.Count}");
                    _logger.Info($"Total: {resultVesselPositions.Count}");

                    if (await Context.ActorSelection("../MapTrackingActor").Ask<bool>(new UpdateVesselPosition(resultVesselPositions, VesselPositionCategory.Current)))
                    {
                        Context.ActorSelection("../MapTrackingActor").Tell(new VesselQueryComplete(m.UserId, resultVesselPositions));
                        //vesselPositions.Clear();
                    }

                    _logger.Info($"Done RequestCurrentVesselPosition");
                    Console.WriteLine($"Done RequestCurrentVesselPosition");
                }
                catch (Exception err)
                {
                    _logger.Error(err, err.Message);
                    //Console.WriteLine(err.ToString());
                    await Context.ActorSelection("../MapTrackingActor").Ask<bool>(new UpdateVesselPosition(resultVesselPositions, VesselPositionCategory.Current));
                }
            });

        }
    }
}
