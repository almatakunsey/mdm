﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel;
using Vtmis.Core.Model.AkkaModel.Messages;

namespace Vtmis.Core.ActorModel.Actors
{
    public class VesselApiActor : ReceiveActor
    {
        public List<VesselPosition> VesselPositions { get; set; }

        public string ServerName { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vesselQuery"></param>
        /// <param name="vesselPersistent"></param>
        /// <param name="vesselReplayQuery"></param>
        /// <param name="startDate">Value set dari docker-compose environment</param>
        /// <param name="serverName"></param>
        /// <param name="userName"></param>
        /// <param name="passWord"></param>
        /// <param name="environment"></param>
        public VesselApiActor(IActorRef vesselQuery, IActorRef vesselReplayQuery, DateTime startDate, string serverName, string userName, string passWord, string environment)
        {
            //Console.WriteLine($"VesselApiActor started... || Server Name:{serverName} Environment:{environment}");

            ServerName = serverName;

            #region Live


            int addMinutes = 0;
            Receive<StartMap>(m =>
            {
                DateTime filterDate = DateTime.Now;

                Console.WriteLine($"VesselApiActor: Current Environment: {environment.ToLower()} || {Enum.GetName(typeof(EnvironmentName), EnvironmentName.HafizDev).ToLower()}");

                //if (string.Equals(environment, Common.Constants.HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase))
                //{
                //    filterDate = startDate.AddMinutes(addMinutes);
                //}

                string postFixDbName = $"AIS{filterDate.ToString("yyMMdd")}";


                //After pukul 12 sehingga pukul 8 database masih daabase sehari sebelum
                if (filterDate >= Convert.ToDateTime($"{filterDate.ToString("yyyy-MM-dd")} 08:00:00"))
                    postFixDbName = $"AIS{filterDate.ToString("yyMMdd")}";
                else
                    postFixDbName = $"AIS{filterDate.AddDays(-1).ToString("yyMMdd")}";


                Console.WriteLine($"{DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss")} {this.GetType().Name} : Received request from {Sender.GetType().Name} - Filter By Datetime {filterDate} on {postFixDbName}");

                if (string.Equals(environment, Common.Constants.HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase))
                {
                    Context.ActorSelection("/user/GlobalActor").Tell(new SetTargetMonitorDateTime(filterDate));
                }

                //Console.WriteLine($"Global Selection: {global..ToStringWithAddress()}");

                //Temp disable
                Console.WriteLine($"VesselQueryActor->Tell(RequestCurrentVesselPosition)");
                //vesselQuery.Tell(new RequestCurrentVesselPosition(filterDate, VesselPositions, postFixDbName, ServerName, userName, passWord, m.UserId), Sender);

                addMinutes = addMinutes + 1;
            });
            #endregion

        }

        public DateTime GetFilterDate(DateTime currentDate, int diffInitDate)
        {
            var filterDate = currentDate.AddDays(diffInitDate)
                                       .AddHours(DateTime.Now.Hour)
                                       .AddMinutes(DateTime.Now.Minute)
                                       .AddSeconds(DateTime.Now.Second);
            Console.WriteLine($"Get FilterDate {filterDate}");
            filterDate = filterDate.AddMinutes(-5);

            //filterDate = Convert.ToDateTime("2017-08-30 08:00:00").AddMinutes(DateTime.Now.Minute);
            return filterDate;
        }
    }
}
