﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using Vtmis.Core.VesselEntityFrameworkCore;
using System.Linq;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using System.Threading;
using Vtmis.Core.Model.AkkaModel.Messages;
using Akka.Event;
using Microsoft.EntityFrameworkCore;

namespace Vtmis.Core.ActorModel.Actors
{
    public class VesselQueryPlaybackActor : ReceiveActor
    {
        private readonly ILoggingAdapter _logger;

        protected override void PreStart()
        {
            Console.WriteLine("VesselQueryPlaybackActor pre start");
            base.PreStart();
        }

        public VesselQueryPlaybackActor(string dbServer, string userName, string passWord, IDateFromAndToService dateFromAndToService)
        {
            _logger = Context.GetLogger();
            List<ShipPosition> shipPositions = null;

            //return Bool to Sender
            Receive<VesselTimelineRequest>(m =>
            {
                try
                {
                    _logger.Info($"[Playback] Start - {m.MMSI} || From: {m.FromDateTime} || To: {m.ToDateTime}");
                    shipPositions = new List<ShipPosition>();

                    DateTime filterDate = m.FromDateTime;

                    List<string> prefixDbName = new List<string>();

                    List<ShipPosition> batchShipPosition = new List<ShipPosition>();

                    if (ValidateDateRangeInput(filterDate, m.FromDateTime, m.ToDateTime))
                    {
                        int dayCount = m.ToDateTime.Subtract(m.FromDateTime).Days + 1;

                        for (int i = 1; i <= dayCount; i++)
                        {
                            //Get fromDate & toDate for every database query
                            DateFromAndTo dateFromAndTo = dateFromAndToService.GetDateFromAndTo(filterDate, m.ToDateTime, i, dayCount);

                            var postFixDbName = $"AIS{dateFromAndTo.fromDateTime.ToString("yyMMdd")}";

                            prefixDbName.Add(postFixDbName);

                            //Console.WriteLine("VesselQueryPlaybackActor working");

                            using (var dbContext = new VesselDatabaseContext(postFixDbName, dbServer, userName, passWord))
                            {
                                var results = dbContext.ShipPositions.AsNoTracking().Where(c => c.MMSI == m.MMSI &&
                                c.LocalRecvTime >= m.FromDateTime && c.LocalRecvTime < m.ToDateTime)
                                .Select(c => new ShipPosition
                                {
                                    Latitude = c.Latitude,
                                    Longitude = c.Longitude,
                                    LocalRecvTime = c.LocalRecvTime
                                });

                                shipPositions.AddRange(results.ToList());

                                //Sender.Tell(new PlayBackGetVesselTimeLineResponse { Status = true, ShipPositions = results.ToList<ShipPosition>() });
                            }
                        }

                        Console.WriteLine("Total " + shipPositions.Count());

                        //TODO: Refactor this.. refer to https://stackoverflow.com/questions/53955009/query-date-time-range-with-interval for solution
                        var filterShipPositions = new List<ShipPosition>();
                        if (shipPositions.Any())
                        {
                            double prevLat = 0;
                            double prevLong = 0;
                            DateTime? prevDateTime = null;
                            var count = 0;
                            foreach (var item in shipPositions)
                            {
                                if (count == 0)
                                {
                                    filterShipPositions.Add(item);
                                    prevLat = item.Latitude;
                                    prevLong = item.Longitude;
                                    prevDateTime = item.LocalRecvTime;
                                    count += 1;
                                    //Console.WriteLine($"Latitude: {item.Latitude} || Longitude: {item.Longitude} || Date : {item.LocalRecvTime}");
                                    continue;
                                }
                                if ((item.LocalRecvTime - prevDateTime).Value.TotalSeconds < 120)
                                {
                                    count += 1;
                                    continue;
                                }
                                if (prevLat != item.Latitude && prevLong != item.Longitude &&
                                        prevDateTime != item.LocalRecvTime)
                                {
                                    filterShipPositions.Add(item);
                                    prevLat = item.Latitude;
                                    prevLong = item.Longitude;
                                    prevDateTime = item.LocalRecvTime;
                                    //Console.WriteLine($"Latitude: {item.Latitude} || Longitude: {item.Longitude} || Date : {item.LocalRecvTime}");
                                }

                                count += 1;
                            }
                        }
                        //Sender.Tell(true);
                        Sender.Tell(new VesselTimelineResponse { Status = true, ShipPositions = filterShipPositions.ToList() });

                    }
                    else
                    {
                        Sender.Tell(new VesselTimelineResponse { Status = false });
                        //Sender.Tell(false);
                        //Sender.Tell(new PlayBackGetVesselTimeLineResponse { Status = false });
                    }
                    _logger.Info($"[Playback] Done - {m.MMSI} || From: {m.FromDateTime} || To: {m.ToDateTime}");
                }
                catch (Exception err)
                {
                    Sender.Tell(new VesselTimelineResponse { Status = false, Error = err.Message });
                    _logger.Error(err, err.Message);
                    //throw err;
                }
               
            });

            //return ShipPositionResponse to Sender
            //Receive<ShipPositionRequest>(m =>
            //{
            //    bool stream = true;
            //    var skip = (m.PageNumber - 1) * m.PageSize;

            //    if (skip < shipPositions.Count())
            //        stream = true;
            //    else
            //        stream = false;

            //    Console.WriteLine("Current Ship " + shipPositions.Count());

            //    var l = shipPositions.Skip((m.PageNumber - 1) * m.PageSize).Take(m.PageSize);

            //    Sender.Tell(new ShipPositionResponse { Stream =stream, ShipPositions = l.ToList() });
            //});

            //return VesselsByFromDateResponse to Sender
            Receive<VesselsByFromDateRequest>(m =>
            {
                _logger.Info("VesselsByFromDateRequest");
                try
                {
                    var postFixDbName = $"AIS{m.FromDate.ToString("yyMMdd")}";

                    using (var dbContext = new VesselDatabaseContext(postFixDbName, dbServer, userName, passWord))
                    {
                        var result = from s in dbContext.ShipPositions
                                     join si in dbContext.ShipStatics on s.MMSI equals si.MMSI
                                     where si.Name != null
                                     group si by new { si.Name, si.MMSI } into g
                                     select new VesselDetail
                                     {
                                         VesselName = g.Key.Name,
                                         MMSI = g.Key.MMSI
                                     };

                        Console.WriteLine(result.ToList().Count());

                        Thread.Sleep(10000);

                        Sender.Tell(new VesselsByFromDateResponse { Status = "Ok", VesselDetails = result.ToList() });
                    }
                }
                catch (Exception err)
                {
                    Sender.Tell(new VesselsByFromDateResponse { Status = "Error", Error = err.Message });
                    _logger.Error(err, err.Message);
                    //throw err;
                }
                

            });
        }

        public bool ValidateDateRangeInput(DateTime filterDate,DateTime fromDateTime,DateTime toDateTime)
        {
            int monthCurrentDate = filterDate.Month;
            int yearCurrentDate = filterDate.Year;

            int monthInputDate = fromDateTime.Month;
            int yearInputDate = fromDateTime.Year;

            if (monthCurrentDate != monthInputDate || yearCurrentDate != yearInputDate)
                return false;

            return true;
        }
    }

    public interface IDateFromAndToService
    {
        DateFromAndTo GetDateFromAndTo(DateTime fromDateTime,DateTime toDateTime,int index,int countDays);
    }

    public class DateFromAndTo
    {
        public DateTime fromDateTime { get; set; }
        public DateTime toDateTime { get; set; }
    }

    public class DateFromAndToService : IDateFromAndToService
    {
        DateTime _fromDate;
        DateTime _toDate;

        public DateFromAndTo GetDateFromAndTo(DateTime fromDateTime, DateTime toDateTime,int index,int countDays)
        {
            if (index == 1)
            {
                _fromDate = fromDateTime;
                if (countDays > 1)
                    _toDate = Convert.ToDateTime(_fromDate.ToString("yyyy-MM-dd") + " 23:59:00");
                else
                    _toDate = toDateTime;
            }
            else
            {
                _fromDate = Convert.ToDateTime(fromDateTime.AddDays(index-1).ToString("yyyy-MM-dd") + " 00:00:00");
                _toDate = _toDate = Convert.ToDateTime(_fromDate.ToString("yyyy-MM-dd") + " 23:59:00");
            }

            if (index==countDays)
            {
                _toDate = toDateTime;
               
            }
            return new DateFromAndTo { fromDateTime = _fromDate, toDateTime = _toDate};
        }
    }
}
