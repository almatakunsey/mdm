﻿using Akka.Actor;
using Akka.Event;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.AkkaModel;
using Vtmis.Core.Model.AkkaModel.Messages.Route;
using Vtmis.Core.Model.ViewModels.Route.Dto;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.Core.ActorModel.Actors.Route
{
    public class RouteActor : ReceiveActor
    {
        protected IActorRef _routeEtaWorker;
        private readonly IRouteService _routeService;
        private int _numOfRouteWorker = 0;
        private ILoggingAdapter _logger;
        protected override void PreStart()
        {
            _logger = Context.GetLogger();
            _logger.Info("Route Actor Started");
            Console.WriteLine("Route Actor Started");
        }

        protected override void PreRestart(Exception reason, object message)
        {
            _logger.Info("Route Actor Restarting");
            Console.WriteLine("Route Actor Restarting");
            base.PreRestart(reason, message);
        }

        public RouteActor(IRouteService routeService)
        {
            _routeService = routeService;          

            //IActorRef actorBase = null;

            ReceiveAsync<RequestRouteEta>(x => CalculateRouteEta(x));

            ReceiveAsync<RequestVesselEta>(m => CalculateVesselRoute(m));
        }

        private async Task CalculateVesselRoute(RequestVesselEta m)
        {
            try
            {
                
                var result = await _routeService.VesselEtaAsync(m);
                Sender.Tell(new ResponseVesselEtaMsg(result));
            }
            catch (Exception err)
            {
                _logger.Error("", err);
                Sender.Tell(new ResponseErrorMessage(err.Message, err));
            }
        }

        private async Task CalculateRouteEta(RequestRouteEta m)
        {
            try
            {
                var result = await _routeService.RouteEtaAsync(m);
                Sender.Tell(new ResponseRouteEta(result));
            }
            catch (Exception err)
            {
                _logger.Error("", err);
                Sender.Tell(new ResponseErrorMessage(err.Message, err));
            }

        }

        private void Temp(RequestRouteEta x)
        {
            // get only shown routes
            var visibleRoutes = x.Route.Waypoints.Where(w => w.IsShown == true).ToList();
            _numOfRouteWorker = visibleRoutes.Count();

            var result = new List<QueryShipPosStatic>();
            using (var aisDbContext = new VesselDatabaseContext(x.RequestDate))
            {
                aisDbContext.Database.SetCommandTimeout(120);

                var p0 = x.RequestDate;
                var p1 = 10; // vessel must received time less than 3 minutes
                             //message id
                var p2 = 1;
                var p3 = 2;
                var p4 = 3;
                var p5 = 18;
                var qry = aisDbContext.Query<QueryShipPosStatic>().FromSql($@"
                           SELECT spG.LocalRecvTime, spG.MMSI, Name, ShipType, SOG, COG, Latitude, Longitude
                            FROM (SELECT sp.MMSI, sp.MessageId, sp.SOG, sp.COG, sp.LocalRecvTime, sp.RecvTime, sp.Latitude, sp.NavigationalStatus, sp.Longitude FROM
                            (SELECT MMSI, MAX(LocalRecvTime) LocalRecvTime FROM ShipPosition WHERE LocalRecvTime <= @p0 AND 
                            (DATEDIFF(minute,LocalRecvTime, @p0) <= @p1) AND (MessageId = @p2 OR MessageId = @p3 OR MessageId = @p4 OR MessageId = @p5)
                            GROUP BY MMSI) spList 
                            INNER JOIN ShipPosition sp on spList.MMSI = sp.MMSI WHERE spList.LocalRecvTime = sp.LocalRecvTime) spG
                            LEFT JOIN ShipStatic ss on spG.MMSI = ss.MMSI          
                        ", p0, p1, p2, p3, p4, p5).AsNoTracking().AsQueryable().OrderByDescending(o => o.LocalRecvTime);

                result = qry.ToList();
            }

            var finalResults = new List<ResponseRouteEtaDto>();
            var filters = new List<dynamic>();

            // Start filter query based on user requirements
            for (int i = 0; i < _numOfRouteWorker; i++)
            {
                var pointA = new PointF
                {
                    X = (float)visibleRoutes[i].Latitude,
                    Y = (float)visibleRoutes[i].Longitude
                };

                var pointB = new PointF();
                if (i >= _numOfRouteWorker - 1)
                {
                    pointB.X = (float)visibleRoutes[i - 1].Latitude;
                    pointB.Y = (float)visibleRoutes[i - 1].Longitude;
                }
                else
                {
                    pointB.X = (float)visibleRoutes[i + 1].Latitude;
                    pointB.Y = (float)visibleRoutes[i + 1].Longitude;
                }

                // XTE LEFT RIGHT is in meters
                var qryEta = result.Where(w => w.SOG >= visibleRoutes[i].MinSpeed && w.SOG <= visibleRoutes[i].MaxSpeed &&
                 (CommonHelper.IsInRange(new PointF { X = (float)w.Latitude, Y = (float)w.Longitude }, pointA, pointB, visibleRoutes[i].XteLeft, Unit.Meters) ||
                 CommonHelper.IsInRange(new PointF { X = (float)w.Latitude, Y = (float)w.Longitude }, pointA, pointB, visibleRoutes[i].XteRight, Unit.Meters)))
                    .Select(s => new
                    {
                        Distance = CommonHelper.GetDistanceBetweenTwoCoordinates(new PointF { X = (float)s.Latitude, Y = (float)s.Longitude },
                                        new PointF { X = (float)visibleRoutes[i].Latitude, Y = (float)visibleRoutes[i].Longitude }),
                        s.SOG,
                        s.Name,
                        s.COG,
                        s.Latitude,
                        s.Longitude,
                        s.LocalRecvTime,
                        s.MMSI,
                        s.ShipType,
                        Waypoints = new List<ResponseWaypointRouteEtaDto>
                        {
                                    new ResponseWaypointRouteEtaDto
                                    {
                                        Name = visibleRoutes[i].Name
                                    }
                        }
                    }).ToList();
                filters.AddRange(qryEta);
            }
            // End filter query based on user requirements

            //Start get vessel info
            foreach (var filter in filters)
            {
                if (filters.Where(w => w.MMSI == filter.MMSI).Count() > 1 && !finalResults.Any(a => a.MMSI == filter.MMSI))
                {
                    var minDistance = filters.Where(w => w.MMSI == filter.MMSI).Min(m => m.Distance);
                    var qryItem = filters.FirstOrDefault(w => w.MMSI == filter.MMSI && w.Distance == minDistance);
                    finalResults.Add(new ResponseRouteEtaDto
                    {
                        COG = qryItem.COG,
                        Latitude = qryItem.Latitude.ToString(),
                        LocalRecvTime = qryItem.LocalRecvTime,
                        Longitude = qryItem.Longitude.ToString(),
                        MMSI = qryItem.MMSI,
                        Name = qryItem.Name,
                        SOG = qryItem.SOG,
                        ShipTypeId = qryItem.ShipType,
                        Waypoints = qryItem.Waypoints
                    });
                }
                else if (!finalResults.Any(a => a.MMSI == filter.MMSI))
                {
                    finalResults.Add(new ResponseRouteEtaDto
                    {
                        COG = filter.COG,
                        Latitude = filter.Latitude.ToString(),
                        LocalRecvTime = filter.LocalRecvTime,
                        Longitude = filter.Longitude.ToString(),
                        MMSI = filter.MMSI,
                        Name = filter.Name,
                        SOG = filter.SOG,
                        ShipTypeId = filter.ShipType,
                        Waypoints = filter.Waypoints
                    });
                }
            }
            //End get vessel info

            var response = finalResults.ToList();
            var finalCounter = 0;

            //Start ETA Calculations
            foreach (var item in finalResults)
            {
                var etaWaypoints = new List<ResponseWaypointRouteEtaDto>();
                var currentWaypointEta = item.Waypoints.FirstOrDefault();
                var wpData = visibleRoutes.First(w => w.Name == currentWaypointEta.Name);
                var prevWpData = visibleRoutes.Where(w => w.Id != wpData.Id && w.Id < wpData.Id).OrderByDescending(o => o.Id).FirstOrDefault();
                var nextWpData = visibleRoutes.Where(w => w.Id != wpData.Id && w.Id > wpData.Id).OrderBy(o => o.Id).FirstOrDefault();
                var isWaypointAscending = true;
                var waypointBearing = nextWpData != null ? CommonHelper.DegreeBearing(wpData.Latitude, wpData.Longitude, nextWpData.Latitude, nextWpData.Longitude)
                                            : CommonHelper.DegreeBearing(prevWpData.Latitude, prevWpData.Longitude, wpData.Latitude, wpData.Longitude);
                isWaypointAscending = CommonHelper.IsWaypointDirectionAsc(item.COG.Value, waypointBearing);
                //Console.WriteLine($"{item.Name} || {item.COG} || {waypointBearing} || {isWaypointAscending}");
                if (isWaypointAscending == false)
                {
                    visibleRoutes = visibleRoutes.OrderByDescending(o => o.Id).ToList();
                }
                else
                {
                    visibleRoutes = visibleRoutes.OrderBy(o => o.Id).ToList();
                }

                var waypointIndex = 0;
                DateTime? eta = null;
                double? distanceWp = null;
                var reachedCurrentWaypoint = false;
                var reachWpCounter = 0;
                ShipPosition nextVesselPosition = null;
                using (var aisDbContext = new VesselDatabaseContext(x.RequestDate))
                {
                    nextVesselPosition = aisDbContext.ShipPositions.AsNoTracking().Where(w => w.MMSI == item.MMSI &&
                                   w.LocalRecvTime > item.LocalRecvTime).OrderBy(o => o.LocalRecvTime).FirstOrDefault();
                }

                foreach (var waypoint in visibleRoutes)
                {
                    var etaWaypoint = new ResponseWaypointRouteEtaDto { Id = waypoint.Id, Name = waypoint.Name };
                    var isCurrentWaypointEta = (wpData.Name == waypoint.Name);


                    if (wpData.Name == waypoint.Name)
                    {
                        reachedCurrentWaypoint = true;
                        etaWaypoint.IsArrived = true;
                    }

                    // if vessel reached the waypoints thats in range
                    if (reachedCurrentWaypoint)
                    {
                        var vesselPoint = new PointF();
                        var destinationPoint = new PointF();
                        var fromPoint = new PointF();
                        double newDistanceWp = 0;

                        // set inital value for waypoint's eta and distance 
                        if (eta == null)
                        {
                            eta = item.LocalRecvTime;
                        }
                        if (distanceWp == null)
                        {
                            distanceWp = 0;
                        }

                        destinationPoint.X = (float)waypoint.Latitude;
                        destinationPoint.Y = (float)waypoint.Longitude;

                        // Determine if vessel has pass through the waypoint or not
                        if (nextVesselPosition != null)
                        {
                            var nextPositionDistance = CommonHelper.GetDistanceBetweenTwoCoordinates(new PointF
                            {
                                X = (float)nextVesselPosition.Latitude,
                                Y = (float)nextVesselPosition.Longitude
                            }, new PointF { X = (float)waypoint.Latitude, Y = (float)waypoint.Longitude });

                            var currentPositionDistance = CommonHelper.GetDistanceBetweenTwoCoordinates(new PointF
                            {
                                X = (float)Convert.ToDouble(item.Latitude),
                                Y = (float)Convert.ToDouble(item.Longitude)
                            }, new PointF { X = (float)waypoint.Latitude, Y = (float)waypoint.Longitude });

                            // if current distance is more than next distance, vessel have not pass the waypoint
                            if (currentPositionDistance > nextPositionDistance)
                            {

                                if (isCurrentWaypointEta)
                                {
                                    vesselPoint.X = (float)Convert.ToDouble(item.Latitude);
                                    vesselPoint.Y = (float)Convert.ToDouble(item.Longitude);

                                    if (waypointIndex <= 0)
                                    {
                                        fromPoint.X = (float)Convert.ToDouble(item.Latitude);
                                        fromPoint.Y = (float)Convert.ToDouble(item.Longitude);
                                    }
                                    else
                                    {
                                        fromPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                        fromPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;
                                    }


                                }
                                else
                                {
                                    fromPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                    fromPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;

                                    vesselPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                    vesselPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;
                                }

                            }
                            else
                            {
                                if (isCurrentWaypointEta)
                                {
                                    reachWpCounter += 1;
                                    etaWaypoints.Add(etaWaypoint);
                                    waypointIndex += 1;
                                    continue;
                                }
                                else
                                {
                                    fromPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                    fromPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;
                                }

                                vesselPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                vesselPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;
                            }

                        }
                        else
                        {
                            if (isCurrentWaypointEta)
                            {
                                reachWpCounter += 1;
                                etaWaypoints.Add(etaWaypoint);
                                waypointIndex += 1;
                                continue;
                            }
                            fromPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                            fromPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;

                            vesselPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                            vesselPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;
                        }

                        newDistanceWp = CommonHelper.GetDistanceBetweenTwoCoordinates(vesselPoint, destinationPoint, Unit.NauticalMiles);
                        etaWaypoint.ETA = CommonHelper.GetETAByDistance(eta.Value, item.SOG.Value, newDistanceWp);
                        etaWaypoint.Distance = newDistanceWp;
                        eta = etaWaypoint.ETA;
                        etaWaypoint.Aging = CommonHelper.GetEstimationAging(eta.Value);
                    }
                    etaWaypoints.Add(etaWaypoint);
                    waypointIndex += 1;
                }

                response[finalCounter].Waypoints = etaWaypoints.OrderBy(o => o.Id).ToList();
                finalCounter += 1;
            }
            //End ETA Calculations

            Sender.Tell(new ResponseRouteEta(response));
        }

        private void TempVesselEta(RequestVesselEta m)
        {
            using (var aisDbContext = new VesselDatabaseContext(m.RequestDate))
            {
                var vessel = (from sp in aisDbContext.ShipPositions.AsNoTracking()
                              where sp.MMSI == m.MMSI && sp.LocalRecvTime <= m.RequestDate && (m.RequestDate - sp.LocalRecvTime).Value.TotalMinutes <= 3
                              join ss in aisDbContext.ShipStatics.AsNoTracking() on sp.MMSI equals ss.MMSI
                              into spSs
                              from ss in spSs.DefaultIfEmpty()
                              orderby sp.LocalRecvTime descending
                              select new ResponseVesselEta
                              {
                                  COG = sp.COG.Value,
                                  Latitude = sp.Latitude.ToString(),
                                  LocalRecvTime = sp.LocalRecvTime.Value,
                                  Longitude = sp.Longitude.ToString(),
                                  MMSI = sp.MMSI,
                                  Name = ss != null ? ss.Name : null,
                                  SOG = sp.SOG.Value,
                                  ShipTypeId = ss != null ? ss.ShipType : null,
                              }).FirstOrDefault();


                var visibleRoutes = m.Route.Waypoints.Where(w => w.IsShown == true).ToList();
                var wpDataList = new List<WaypointDto>();
                var etaWaypoints = new List<ResponseWaypointRouteEtaDto>();
                for (int i = 0; i < visibleRoutes.Count; i++)
                {
                    var pointA = new PointF
                    {
                        X = (float)visibleRoutes[i].Latitude,
                        Y = (float)visibleRoutes[i].Longitude
                    };
                    var pointB = new PointF();
                    if (i >= visibleRoutes.Count - 1)
                    {
                        pointB.X = (float)visibleRoutes[i - 1].Latitude;
                        pointB.Y = (float)visibleRoutes[i - 1].Longitude;
                    }
                    else
                    {
                        pointB.X = (float)visibleRoutes[i + 1].Latitude;
                        pointB.Y = (float)visibleRoutes[i + 1].Longitude;
                    }

                    if (vessel == null)
                    {
                        etaWaypoints.AddRange(visibleRoutes.Select(s => new ResponseWaypointRouteEtaDto { Id = s.Id, Name = s.Name, Distance = 0 }));
                        var nullResult = new ResponseVesselEta
                        {
                            Waypoints = etaWaypoints.OrderBy(o => o.Id).ToList()
                        };
                        Sender.Tell(new ResponseVesselEtaMsg(nullResult));
                        return;
                    }
                    var etaWaypoint = new ResponseWaypointRouteEtaDto { Id = visibleRoutes[i].Id, Name = visibleRoutes[i].Name };
                    var isInRange = ((CommonHelper.IsInRange(new PointF
                    {
                        X = (float)Convert.ToDouble(vessel.Latitude),
                        Y = (float)Convert.ToDouble(vessel.Longitude)
                    }, pointA, pointB, visibleRoutes[i].XteRight, Unit.Meters) || CommonHelper.IsInRange(new PointF
                    {
                        X = (float)Convert.ToDouble(vessel.Latitude),
                        Y = (float)Convert.ToDouble(vessel.Longitude)
                    }, pointA, pointB, visibleRoutes[i].XteLeft, Unit.Meters)) && vessel.SOG >= visibleRoutes[i].MinSpeed && vessel.SOG <= visibleRoutes[i].MaxSpeed);

                    if (isInRange)
                    {
                        wpDataList.Add(new WaypointDto
                        {
                            AverageSpeed = visibleRoutes[i].AverageSpeed,
                            Id = visibleRoutes[i].Id,
                            IsShown = visibleRoutes[i].IsShown,
                            Latitude = visibleRoutes[i].Latitude,
                            Longitude = visibleRoutes[i].Longitude,
                            MaxSpeed = visibleRoutes[i].MaxSpeed,
                            MinSpeed = visibleRoutes[i].MinSpeed,
                            Name = visibleRoutes[i].Name,
                            RouteId = visibleRoutes[i].RouteId,
                            XteLeft = visibleRoutes[i].XteLeft,
                            XteRight = visibleRoutes[i].XteRight
                        });
                    }
                }



                if (wpDataList.Any())
                {
                    WaypointDto wpData = null;
                    if (wpDataList.Count > 1)
                    {
                        double distance = 0;
                        foreach (var item in wpDataList)
                        {
                            var newDistance = CommonHelper.GetDistanceBetweenTwoCoordinates(new PointF { X = (float)Convert.ToDouble(vessel.Latitude), Y = (float)Convert.ToDouble(vessel.Longitude) },
                                              new PointF { X = (float)item.Latitude, Y = (float)item.Longitude });
                            if (distance == 0)
                            {
                                distance = newDistance;
                                wpData = new WaypointDto();
                                wpData = item;
                            }
                            else
                            {
                                if (newDistance < distance)
                                {
                                    distance = newDistance;
                                    wpData = new WaypointDto();
                                    wpData = item;
                                }
                            }
                        }
                    }
                    else
                    {
                        wpData = new WaypointDto();
                        wpData = wpDataList.FirstOrDefault();
                    }

                    DateTime? eta = null;
                    double? distanceWp = null;
                    var reachedCurrentWaypoint = false;
                    var waypointIndex = 0;
                    var prevWpData = visibleRoutes.Where(w => w.Id != wpData.Id && w.Id < wpData.Id).OrderByDescending(o => o.Id).FirstOrDefault();
                    var nextWpData = visibleRoutes.Where(w => w.Id != wpData.Id && w.Id > wpData.Id).OrderBy(o => o.Id).FirstOrDefault();
                    var isWaypointAscending = true;
                    var waypointBearing = nextWpData != null ? CommonHelper.DegreeBearing(wpData.Latitude, wpData.Longitude, nextWpData.Latitude, nextWpData.Longitude)
                                                : CommonHelper.DegreeBearing(prevWpData.Latitude, prevWpData.Longitude, wpData.Latitude, wpData.Longitude);
                    isWaypointAscending = CommonHelper.IsWaypointDirectionAsc(vessel.COG.Value, waypointBearing);
                    if (isWaypointAscending == false)
                    {
                        visibleRoutes = visibleRoutes.OrderByDescending(o => o.Id).ToList();
                    }
                    else
                    {
                        visibleRoutes = visibleRoutes.OrderBy(o => o.Id).ToList();
                    }

                    var reachWpCounter = 0;
                    // get the next vessel's position (coordinate)
                    ShipPosition nextVesselPosition = aisDbContext.ShipPositions.AsNoTracking().Where(w => w.MMSI == vessel.MMSI &&
                                         w.LocalRecvTime > vessel.LocalRecvTime).FirstOrDefault();
                    //DateTime? test = null;
                    foreach (var waypoint in visibleRoutes)
                    {
                        var etaWaypoint = new ResponseWaypointRouteEtaDto { Id = waypoint.Id, Name = waypoint.Name };
                        var isCurrentWaypointEta = (wpData.Name == waypoint.Name);

                        if (wpData.Name == waypoint.Name)
                        {
                            reachedCurrentWaypoint = true;
                        }

                        // if vessel reached the waypoints thats in range
                        if (reachedCurrentWaypoint)
                        {
                            var vesselPoint = new PointF();
                            var destinationPoint = new PointF();
                            var fromPoint = new PointF();
                            double newDistanceWp = 0;

                            // set inital value for waypoint's eta and distance 
                            if (eta == null)
                            {
                                eta = vessel.LocalRecvTime;
                                //test = vessel.LocalRecvTime;
                            }
                            if (distanceWp == null)
                            {
                                distanceWp = 0;
                            }

                            destinationPoint.X = (float)waypoint.Latitude;
                            destinationPoint.Y = (float)waypoint.Longitude;

                            // Determine if vessel has pass through the waypoint or not
                            if (nextVesselPosition != null)
                            {
                                var nextPositionDistance = CommonHelper.GetDistanceBetweenTwoCoordinates(new PointF
                                {
                                    X = (float)nextVesselPosition.Latitude,
                                    Y = (float)nextVesselPosition.Longitude
                                }, new PointF { X = (float)waypoint.Latitude, Y = (float)waypoint.Longitude });

                                var currentPositionDistance = CommonHelper.GetDistanceBetweenTwoCoordinates(new PointF
                                {
                                    X = (float)Convert.ToDouble(vessel.Latitude),
                                    Y = (float)Convert.ToDouble(vessel.Longitude)
                                }, new PointF { X = (float)waypoint.Latitude, Y = (float)waypoint.Longitude });

                                // if current distance is more than next distance, vessel have not pass the waypoint
                                if (currentPositionDistance > nextPositionDistance)
                                {

                                    if (isCurrentWaypointEta)
                                    {
                                        vesselPoint.X = (float)Convert.ToDouble(vessel.Latitude);
                                        vesselPoint.Y = (float)Convert.ToDouble(vessel.Longitude);

                                        if (waypointIndex <= 0)
                                        {
                                            fromPoint.X = (float)Convert.ToDouble(vessel.Latitude);
                                            fromPoint.Y = (float)Convert.ToDouble(vessel.Longitude);
                                        }
                                        else
                                        {
                                            fromPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                            fromPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;
                                        }


                                    }
                                    else
                                    {
                                        fromPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                        fromPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;

                                        vesselPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                        vesselPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;
                                    }

                                }
                                else
                                {
                                    if (isCurrentWaypointEta)
                                    {
                                        reachWpCounter += 1;
                                        etaWaypoints.Add(etaWaypoint);
                                        waypointIndex += 1;
                                        continue;
                                    }
                                    else
                                    {
                                        fromPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                        fromPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;
                                    }

                                    vesselPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                    vesselPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;
                                }

                            }
                            else
                            {
                                if (isCurrentWaypointEta)
                                {
                                    reachWpCounter += 1;
                                    etaWaypoints.Add(etaWaypoint);
                                    waypointIndex += 1;
                                    continue;
                                }
                                fromPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                fromPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;

                                vesselPoint.X = (float)visibleRoutes[waypointIndex - 1].Latitude;
                                vesselPoint.Y = (float)visibleRoutes[waypointIndex - 1].Longitude;
                            }

                            newDistanceWp = CommonHelper.GetDistanceBetweenTwoCoordinates(vesselPoint, destinationPoint, Unit.NauticalMiles);
                            etaWaypoint.ETA = CommonHelper.GetETAByDistance(eta.Value, vessel.SOG.Value, newDistanceWp);
                            etaWaypoint.Distance = newDistanceWp;

                            eta = etaWaypoint.ETA;
                            etaWaypoint.Aging = CommonHelper.GetEstimationAging(eta.Value);
                        }
                        etaWaypoints.Add(etaWaypoint);
                        waypointIndex += 1;
                    }
                }
                else
                {
                    etaWaypoints.AddRange(visibleRoutes.Select(s => new ResponseWaypointRouteEtaDto { Id = s.Id, Name = s.Name, Distance = 0 }));
                }

                vessel.Waypoints = etaWaypoints.OrderBy(o => o.Id).ToList();
                Sender.Tell(new ResponseVesselEtaMsg(vessel));
            }
        }
    }
}