﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.AkkaModel.Messages.Route;
using Vtmis.Core.Model.ViewModels.Route.Dto;

namespace Vtmis.Core.ActorModel.Actors.Route
{
    public class RouteEtaWorker : ReceiveActor
    {
        public RouteEtaWorker()
        {

            Receive<QueryRouteEta>(m =>
            {
                Console.WriteLine("Route worker started");

                var pointA = new PointF
                {
                    X = (float)m.Waypoint.Latitude,
                    Y = (float)m.Waypoint.Longitude
                };

                var pointB = new PointF();
                if (m.CurrentWaypointIndex >= m.Waypoints.Count - 1)
                {
                    pointB.X = (float)m.Waypoints[m.CurrentWaypointIndex - 1].Latitude;
                    pointB.Y = (float)m.Waypoints[m.CurrentWaypointIndex - 1].Longitude;
                }
                else
                {
                    pointB.X = (float)m.Waypoints[m.CurrentWaypointIndex + 1].Latitude;
                    pointB.Y = (float)m.Waypoints[m.CurrentWaypointIndex + 1].Longitude;
                }

                var result = m.Result.Where(x => x.SOG >= m.Waypoint.MinSpeed && x.SOG <= m.Waypoint.MaxSpeed &&
                 (CommonHelper.IsInRange(new PointF { X = (float)x.Latitude, Y = (float)x.Longitude }, pointA, pointB, m.Waypoint.XteLeft) ||
                 CommonHelper.IsInRange(new PointF { X = (float)x.Latitude, Y = (float)x.Longitude }, pointA, pointB, m.Waypoint.XteRight)))
                    .Select(x => new ResponseRouteEtaDto
                    {
                        SOG = x.SOG,
                        Name = x.Name,
                        COG = x.COG,
                        Latitude = x.Latitude.ToString(),
                        Longitude = x.Longitude.ToString(),
                        LocalRecvTime = x.LocalRecvTime,
                        MMSI = x.MMSI,
                        Waypoints = new List<ResponseWaypointRouteEtaDto>
                        {
                            new ResponseWaypointRouteEtaDto
                            {
                                Name = m.Waypoint.Name,
                                ETA = CommonHelper.GetETAByDistance(x.LocalRecvTime, new PointF
                                    {
                                        X = (float)x.Latitude,
                                        Y = (float)x.Longitude
                                    }, pointB, pointA, x.SOG)
                            }
                        }
                    }).ToList();

                foreach (var item in result)
                {
                    var allWaypointsEta = new List<ResponseWaypointRouteEtaDto>();
                    allWaypointsEta.AddRange(item.Waypoints);
                    //foreach (var wp in m.Waypoints)
                    //{
                    //    if (!string.Equals(wp.Name, m.Waypoint.Name, StringComparison.CurrentCultureIgnoreCase))
                    //    {
                    //        var wpEta = new ResponseWaypointRouteEtaDto { Name = wp.Name };
                    //        var fromPointA = new PointF();
                    //        var toPointB = new PointF();
                            
                    //        wpEta.ETA = CommonHelper.GetETAByDistance(item.LocalRecvTime, new PointF
                    //        {
                    //            X = (float)Convert.ToDouble(item.Latitude),
                    //            Y = (float)Convert.ToDouble(item.Longitude)
                    //        }, toPointB, fromPointA, item.SOG);
                    //    }
                    //}
                }

                //Sender.Tell(new QueryResultRouteEta(result, m.Waypoint));


            });
        }
    }
}
