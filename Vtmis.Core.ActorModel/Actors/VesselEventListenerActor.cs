﻿using Akka.Actor;
using System;
using System.Linq;
using Vtmis.Core.VesselEntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Vtmis.Core.Common.Constants;
using Flurl.Http;
using Vtmis.WebAdmin.Logger;
using Vtmis.Core.Model.AkkaModel.Messages;

namespace Vtmis.Core.ActorModel.Actors
{
    public class VesselEventListenerActor : ReceiveActor
    {
        private string _env;

        public DateTime LatestEventDatetime { get; set; }
        public int LatestAlarmId { get; set; }
        public bool NewStarted { get; set; }
        //TODO : use proper method instead of hardcoded
        private string _create_Log_Uri;

        protected override void PreStart()
        {
            Console.WriteLine("VesselEventListenerActor");
            LatestEventDatetime = DateTime.Now;
            LatestAlarmId = 0;
            NewStarted = true;
            _env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var port = 50837;
            if (string.Equals(_env, "staging", StringComparison.CurrentCultureIgnoreCase))
            {
                //port = 60837;
            }
            _create_Log_Uri = $"https://vtmis_webadmin_web_host:{port}/api/services/app/LogService/CreateLogAsync";
        }

        public VesselEventListenerActor()
        {
            ReceiveAsync<CheckEvent>(async m =>
            {
                //Console.WriteLine("VesselEventListenerActor working...");


                using (var dbContext = new VtsDatabaseContext(m.VtsConnectionString))
                {
                    //Console.WriteLine("VesselEventListenerActor listening...");
                    //Check Update
                    //using (var command = dbContext.Database.GetDbConnection().CreateCommand())
                    //{
                    //    command.CommandText = @"SELECT o.name as ObjectName, last_user_update
                    //                            FROM sys.dm_db_index_usage_stats s
                    //                            join sys.objects o on s.object_id = o.object_id
                    //                            WHERE database_id = DB_ID('VTS')";
                    //    dbContext.Database.OpenConnection();
                    //    using (var result = command.ExecuteReader())
                    //    {
                    //        // do something with result
                    //        while (result.Read())
                    //        {
                    //            //Console.WriteLine(result["ObjectName"] + " " + result["last_user_update"]);
                    //        }
                    //    }
                    //}
                    var eventRow = dbContext.Events.Include(x => x.Alarm).OrderByDescending(t => t.TimeStmp).FirstOrDefault();
                    //var alarmRow = dbContext.Alarms.OrderByDescending(t => t.AlarmID).FirstOrDefault();
                    var vesselDetail = eventRow != null ? await dbContext.ShipLists
                        .Where(x => x.MMSI == eventRow.MMSI).FirstOrDefaultAsync() : null;


                    if (NewStarted)
                    {
                        LatestEventDatetime = eventRow.TimeStmp;
                        //LatestAlarmId = alarmRow.AlarmID;
                        NewStarted = false;
                    }
                    else
                    {
                     
                        if (eventRow.TimeStmp > LatestEventDatetime)
                        {
                            //Call API to insert event data
                            LatestEventDatetime = eventRow.TimeStmp;

                            var alarmResult = eventRow.Alarm?.AlarmName;
                            var utcTime = DateTime.SpecifyKind(eventRow.TimeStmp, DateTimeKind.Utc);
                            var log = $"{eventRow.MMSI} || {eventRow.ShipName} || {alarmResult} || UTC: {utcTime} || Local: {utcTime.ToLocalTime()}";
                            //if (alarmResult != null)
                            //{
                            // Dredging
                            if ((eventRow.AlarmId == 35 || eventRow.AlarmId == 64 || eventRow.AlarmId == 65) && eventRow.Alarm.Enable)
                            {
                                await _create_Log_Uri
                                 .PostJsonAsync(new Logger
                                 {
                                     Log = log
                                 });
                                Console.WriteLine(
                                    $"insert into MMDIS event -- Dredging {log}");

                            }


                            // vessel clearance
                            else if ((eventRow.AlarmId == 4 || eventRow.AlarmId == 5 || eventRow.AlarmId == 7 || eventRow.AlarmId == 8) && eventRow.Alarm.Enable)
                            {
                                await _create_Log_Uri
                                 .PostJsonAsync(new Logger
                                 {
                                     Log = log
                                 });
                                Console.WriteLine(
                                    $"insert into MMDIS event -- Clearance {log}");
                            }

                            // light errors
                            else if (eventRow.AlarmId == 145 && eventRow.Alarm.Enable)
                            {
                                await _create_Log_Uri
                               .PostJsonAsync(new Logger
                               {
                                   Log = log
                               });
                                Console.WriteLine(
                                    $"insert into MMDIS event -- Light Error {log}");
                            }

                            // Strait Reps
                            else if ((eventRow.AlarmId == 148 || eventRow.AlarmId == 149 || eventRow.AlarmId == 150 || eventRow.AlarmId == 152) && eventRow.Alarm.Enable)
                            {
                                await _create_Log_Uri
                             .PostJsonAsync(new Logger
                             {
                                 Log = log
                             });
                                Console.WriteLine(
                                    $"insert into MMDIS event -- Strait Reps {log}");
                            }
                            //}
                        }
                    }
                }
            });
        }
    }
}
