﻿using Akka.Actor;
using Akka.Event;
using System;
using System.Linq;
using System.Threading;
using Vtmis.Core.Model.AkkaModel.Messages.Alarm;
using Vtmis.WebAdmin.ReadOnlyDB;
using Microsoft.EntityFrameworkCore;
using Vtmis.Core.Common.Constants.Alarm;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using System.Collections.Generic;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.WebVessel.Tracking.Services.Hubs;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.AspNetCore.SignalR;
using System.Data.SqlClient;
using Dapper;
using Vtmis.WebAdmin.Events;
using Vtmis.WebAdmin.Alarms.Dapper;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories.Alarms;
using Vtmis.WebAdmin.ReadOnlyDB.Services;

namespace Vtmis.Core.ActorModel.Actors.Alarm
{
    public class UseChar
    {
        public string Character { get; set; }
        public bool IsUsed { get; set; } = false;
        public bool IsLineString { get; set; } = false;
    }
    public class AlarmWorkerActor : ReceiveActor
    {
        private ILoggingAdapter _logger;
        private readonly MdmAdminDbContext _mdmAdminDb;
        private readonly IHubContext<AlarmHub> _alarmHubContext;
        private readonly IDapperEventRepository _dapperEventRepository;
        private readonly IDapperAlarmRepository _dapperAlarmRepository;
        private readonly IAlarmNotificationRepository _alarmNotificationRepository;
        private readonly IAlarmService _alarmService;

        protected override void PreStart()
        {
            _logger = Context.GetLogger();
            Console.WriteLine("Starting AlarmWorker");
            _logger.Info("Starting Alarm worker");
        }

        protected override void PreRestart(Exception reason, object message)
        {
            _logger.Info("Restarting Alarm worker");
            Console.WriteLine("Restarting Alarm worker");
        }

        protected override void PostStop()
        {
            _logger.Info("Stopping Alarm worker");
        }

        public AlarmWorkerActor(MdmAdminDbContext mdmAdminDb, IHubContext<AlarmHub> alarmHubContext,
            IDapperEventRepository dapperEventRepository, IDapperAlarmRepository dapperAlarmRepository,
            IAlarmNotificationRepository alarmNotificationRepository, IAlarmService alarmService)
        {
            _mdmAdminDb = mdmAdminDb;
            _alarmHubContext = alarmHubContext;
            _dapperEventRepository = dapperEventRepository;
            _dapperAlarmRepository = dapperAlarmRepository;
            _alarmNotificationRepository = alarmNotificationRepository;
            _alarmService = alarmService;
            ReceiveAsync<AlarmWorkerMsg>(m => Execute(m));
        }

        /// <summary>
        /// Unsolve:-      
        /// CONDITION 1
        /// - Low Battery
        /// - Photocell Error
        /// - DSC Distress Alert
        /// - DSC Urgency Call
        /// - DSC Safety Call
        /// - DSC Routine Call
        /// - New Ship Data
        /// - Mile Maker
        /// - Geo fence Alert
        /// - SOS Alert
        /// - Bracket Alert
        /// - Power Allert
        /// - On Battery
        /// - Open Doors 0 & 1
        /// - GPS Alert
        /// - Empty
        /// - Loaded
        /// - OverLoad
        /// - SRM Message
        /// CONDITION 2
        /// - Reporting Interval (min)
        /// - Approach (meters|min)
        /// - Min Speed And Time (knots|min)
        /// - Max Speed And Time (knots|min)
        /// - DDMS Battery Voltage
        /// - Hopper (No)
        /// Filter
        /// - Min Length
        /// - Max Length
        /// - Data Source
        /// - Ship Types (Only able to do Cargo, Passenger, Tanker, Fishing)
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        private async Task Execute(AlarmWorkerMsg m)
        {
            try
            {
                Console.WriteLine($"Receiving eventId: {m.EventId}");

                var alarmEvent = _mdmAdminDb.Events.AsNoTracking().Include(i => i.Alarm).ThenInclude(t => t.AlarmItems)
                                    .FirstOrDefault(x => x.Id == m.EventId && x.Kill == false);

                if (alarmEvent == null || alarmEvent.Alarm.IsEnabled == false || alarmEvent.Alarm.IsDeleted == true)
                {
                    _logger.Info($"No Event {m.EventId}. Killing process");
                }
                else
                {
                    _logger.Info($"Trigger Event {m.EventId}.");
                    await Trigger(alarmEvent, m);

                }
                await _dapperEventRepository.UpdateKillByIds(new List<long> { m.EventId });
                await _dapperAlarmRepository.UpdateAlarmChanges(false, alarmEvent.AlarmId);
                await Self.GracefulStop(new TimeSpan(0, 0, 1));
            }
            catch (Exception err)
            {
                if (err is ActorKilledException || err is TaskCanceledException)
                {

                }
                else
                {
                    _logger.Error("", err);
                    _logger.Info("Restarting AlarmWorker");
                    Self.Tell(new AlarmWorkerMsg(m.EventId));
                }
            }
        }

        private async Task Trigger(Event alarmEvent, AlarmWorkerMsg m)
        {
            (string qry, DynamicParameters parameter) = await _alarmService.GenerateAlarmQueryAsync(alarmEvent);

            using (var aisDbContext = new SqlConnection(AppHelper.GetAisConnectionString(AppHelper.GetAisDateBasedOnCurrentEnv())))
            {
                Console.WriteLine($"Start query alarm event");

                var zoneIds = alarmEvent.Alarm.AlarmItems.Where(x => string.Equals(x.ColumnType, "Zone",
                                    StringComparison.CurrentCultureIgnoreCase)).Select(x => x.ColumnValue);

                var zoneNames = new List<string>();
                if (zoneIds.Any())
                {
                    zoneNames.AddRange(_mdmAdminDb.Zones.AsNoTracking().Where(x => zoneIds.Any(z => Convert.ToInt32(z) == x.Id)).Select(x => x.Name));
                }

                var qryResult = aisDbContext.Query<QueryAlarmTrigger>(qry, parameter).Select(x => new
                EventHistory
                {
                    MMSI = x.MMSI,
                    TargetRecvTime = x.RecvTime,
                    Message = alarmEvent.Alarm.Message,
                    ShipName = x.Name,
                    LocalRecvTime = x.LocalRecvTime,
                    ZoneName = string.Join(", ", zoneNames.ToArray())
            });

                Console.WriteLine($"end query alarm event");

                if (qryResult.Count() > 0)
                {
                    var eventDetails = new EventDetails
                    {
                        EventId = alarmEvent.Id,
                        EventHistories = qryResult.ToList()
                    };
                    await _mdmAdminDb.EventDetails.AddAsync(eventDetails);
                    await _mdmAdminDb.SaveChangesAsync();

                    var users = new List<string>();
                    var alarmOwnerId = alarmEvent.Alarm.UserId;
                    var alarmOwnerTenantId = alarmEvent.Alarm.TenantId;

                    var qyrUsers = _mdmAdminDb.AbpUserRoles.AsNoTracking().Include(i => i.User).Include(i => i.Role);
                    var qryAdmin = qyrUsers.Where(x => string.Equals("admin", x.Role.Name, StringComparison.CurrentCultureIgnoreCase) && (x.TenantId == null || x.TenantId == alarmOwnerTenantId));
                    if (await qryAdmin.AnyAsync())
                    {
                        foreach (var admin in qryAdmin)
                        {
                            users.Add(admin.UserId.ToString());
                        }
                    }
                    if (!users.Where(x => x == alarmOwnerId.ToString()).Any())
                    {
                        users.Add(alarmOwnerId.ToString());
                    }

                    await _alarmHubContext.Clients.Users(users).SendAsync("ReceiveAlarmNotification",
                        new ResponseAlarmNotification(alarmEvent.AlarmId, alarmEvent.Alarm?.Name, alarmEvent.Id, eventDetails.Id,
                            qryResult.Count(), alarmEvent.Alarm?.AISMessage, alarmEvent.Alarm?.Message,
                            Enum.GetName(typeof(AlarmPriority), alarmEvent.Alarm?.Priority),
                            alarmEvent.Alarm?.Color));
                    await _alarmNotificationRepository.CreateAsync(new WebAdmin.Alarms.AlarmNotification
                    {
                        EventDetailsId = eventDetails.Id,
                        TenantId = alarmOwnerTenantId,
                        UserId = alarmOwnerId
                    });
                    await _alarmNotificationRepository.SaveChangesAsync();
                    Console.WriteLine($"[{Context.Self.Path}]Trigger Event : {alarmEvent.Id} || EventDetails: {eventDetails.Id} || Alarm : {alarmEvent.AlarmId}");
                    _logger.Info($"Trigger Event : {alarmEvent.Id} || EventDetails: {eventDetails.Id} || Alarm : {alarmEvent.AlarmId}");
                }
            }
        }
    }
}
