﻿using Akka.Actor;
using Akka.DI.Core;
using Akka.Event;
using System;
using Vtmis.Core.Model.AkkaModel.Messages.Alarm;

namespace Vtmis.Core.ActorModel.Actors.Alarm
{
    public class AlarmEngineActor : ReceiveActor
    {
        private readonly ILoggingAdapter _logger;
        protected override void PreStart()
        {
            _logger.Info("Starting Alarm Engine");
        }

        protected override void PreRestart(Exception reason, object message)
        {
            _logger.Info("Restarting Alarm Engine");
        }

        protected override void PostStop()
        {
            _logger.Info("Stopping Alarm Engine");
        }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(err =>
            {
                _logger.Info("SV : Restart started...");
                return Directive.Restart;
            });
        }

        public AlarmEngineActor()
        {
            _logger = Context.GetLogger();
            var actorProps = Context.DI().Props<AlarmListenerActor>();
            var alarmListerner = Context.ActorOf(actorProps, "AlarmListenerActor");
            alarmListerner.Tell(new AlarmRestartMsg());
            alarmListerner.Tell(new AlarmListenChangesMsg());
        }

    }
}
