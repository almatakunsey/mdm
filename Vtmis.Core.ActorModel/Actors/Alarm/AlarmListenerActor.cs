﻿using Akka.Actor;
using Akka.DI.Core;
using Akka.Event;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Vtmis.Core.Model.AkkaModel.Messages.Alarm;
using Vtmis.WebAdmin.Alarms.Dapper;
using Vtmis.WebAdmin.ReadOnlyDB;

namespace Vtmis.Core.ActorModel.Actors.Alarm
{
    public class AlarmListenerActor : ReceiveActor
    {
        private readonly ILoggingAdapter _logger;
        private readonly MdmAdminDbContext _mdmDbContext;
        private readonly IDapperAlarmRepository _dapperAlarmRepository;
        private readonly IDapperEventRepository _dapperEventRepository;
        private string _alarmWorkerActorPath = "AlarmWorkerActor";
        protected override void PreStart()
        {
            _logger.Info("Starting Alarm Listener");
        }

        protected override void PreRestart(Exception reason, object message)
        {
            _logger.Info("Restarting Alarm Listner");
        }

        protected override void PostStop()
        {
            _logger.Info("Stopping Alarm Listner");
        }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(err =>
            {
                _logger.Info("SV : Restart started...");
                return Directive.Restart;
            });
        }

        public AlarmListenerActor(MdmAdminDbContext mdmDbContext, IDapperAlarmRepository dapperAlarmRepository, IDapperEventRepository dapperEventRepository)
        {
            _logger = Context.GetLogger();
            _mdmDbContext = mdmDbContext;
            _dapperAlarmRepository = dapperAlarmRepository;
            _dapperEventRepository = dapperEventRepository;
            ReceiveAsync<AlarmListenChangesMsg>(m => Listen(m));
            Receive<AlarmRestartMsg>(m => AlarmRestart(m));
        }

        private void AlarmRestart(AlarmRestartMsg m)
        {
            Console.WriteLine("Restart Worker");

            var newEvents = _mdmDbContext.Events.Include(x => x.Alarm).Where(x => x.Kill == false && x.IsDeleted == false && x.Alarm.IsEnabled);

            foreach (var item in newEvents)
            {
                Console.WriteLine($"Creating worker eventid: {item.Id} || alarmid: {item.AlarmId}");
                var alarmWorkerPros = Context.DI().Props<AlarmWorkerActor>();
                Context.ActorOf(alarmWorkerPros, $"{_alarmWorkerActorPath}{item.Id}").Tell(new AlarmWorkerMsg(item.Id));
            }
        }


        private async Task Listen(AlarmListenChangesMsg m)
        {
            Console.WriteLine("Listening");
            while (true)
            {
                try
                {
                    var alarms = _mdmDbContext.Alarms.AsNoTracking().Include(x => x.AlarmItems).Where(x => x.Changes && x.IsEnabled == true && x.IsDeleted == false);
                    //var disableAlarms = _mdmDbContext.Alarms.AsNoTracking().Where(x => x.IsEnabled == false && x.IsDeleted == false);
                    //var deletedAlarms = _mdmDbContext.Alarms.AsNoTracking().Where(x => x.IsDeleted == true && x.Changes);
                    //// [Start] Kill Actor if exist
                    //if (await deletedAlarms.AnyAsync())
                    //{
                    //    var events = await _mdmDbContext.Events.AsNoTracking().Where(x => deletedAlarms.Any(a => a.Id == x.AlarmId)).ToListAsync();
                    //    if (events.Any())
                    //    {
                    //        Console.WriteLine("Alarm: Killing Process.....");
                    //        foreach (var item in events)
                    //        {
                    //            Context.ActorSelection($"{_alarmWorkerActorPath}{item.Id}").Tell(PoisonPill.Instance);
                    //        }
                    //        await _dapperEventRepository.UpdateKillByIds(events.Select(x => x.Id));
                    //    }
                    //}
                    //if (await disableAlarms.AnyAsync())
                    //{
                    //    var events = await _mdmDbContext.Events.AsNoTracking().Where(x => disableAlarms.Any(a => a.Id == x.AlarmId)).ToListAsync();
                    //    if (events.Any())
                    //    {
                    //        Console.WriteLine("Alarm: Killing Process");
                    //        foreach (var item in events)
                    //        {
                    //            //Context.Stop(Context.ActorSelection($"{_alarmWorkerActorPath}{item.Id}").Anchor);
                    //            Context.ActorSelection($"{_alarmWorkerActorPath}{item.Id}").Tell(PoisonPill.Instance);
                    //        }

                    //        //var modifiedEvents = events.Select(x => { x.Kill = true; return x; });
                    //        //_mdmDbContext.Events.UpdateRange(modifiedEvents);
                    //        //await _mdmDbContext.SaveChangesAsync();
                    //        await _dapperEventRepository.UpdateKillByIds(events.Select(x => x.Id));
                    //    }
                    //}
                    if (await alarms.AnyAsync())
                    {
                        Console.WriteLine("Alarm: Changes Exist");
                        var events = await _mdmDbContext.Events.AsNoTracking().Where(x => alarms.Any(a => a.Id == x.AlarmId)).ToListAsync();
                        if (events.Any())
                        {
                            Console.WriteLine("Alarm: Killing Process");
                            foreach (var item in events)
                            {
                                //Context.Stop(Context.ActorSelection($"{_alarmWorkerActorPath}{item.Id}").Anchor);
                                Context.ActorSelection($"{_alarmWorkerActorPath}{item.Id}").Tell(PoisonPill.Instance);
                            }
                            await _dapperEventRepository.UpdateKillByIds(events.Select(x => x.Id));
                            //var modifiedEvents = events.Select(x => { x.Kill = true; return x; });
                            //_mdmDbContext.Events.UpdateRange(modifiedEvents);
                            //await _mdmDbContext.SaveChangesAsync();
                        }

                        //var modifiedAlarms = (await alarms.AsNoTracking().ToListAsync()).Select(x => { x.Changes = false; return x; });
                        //_mdmDbContext.Alarms.UpdateRange(modifiedAlarms);
                        //await _mdmDbContext.SaveChangesAsync();
                        var modifiedAlarms = await _dapperAlarmRepository.UpdateAlarmChanges(false);
                        if (modifiedAlarms.Any())
                        {
                            var newEvents = modifiedAlarms.Select(s => new WebAdmin.Events.Event
                            {
                                AlarmId = s.Id,
                                CreationTime = DateTime.Now
                            }).ToList(); // Have to use ToList in order to retrieve Id's from savechanges result.. Do not use IEnumerable
                            await _mdmDbContext.Events.AddRangeAsync(newEvents);
                            await _mdmDbContext.SaveChangesAsync();
                            if (newEvents.Any())
                            {
                                foreach (var item in newEvents)
                                {
                                    Console.WriteLine($"Creating worker eventid: {item.Id} || alarmid: {item.AlarmId}");
                                    var alarmWorkerPros = Context.DI().Props<AlarmWorkerActor>();
                                    Context.ActorOf(alarmWorkerPros, $"{_alarmWorkerActorPath}{item.Id}").Tell(new AlarmWorkerMsg(item.Id));
                                }
                            }
                        }

                    }
                    // [End] Kill Actor if exist
                }
                catch (Exception err)
                {

                    _logger.Error("", err);
                    _logger.Info("Restarting Listner");
                    Self.Tell(new AlarmListenChangesMsg());


                }

            }
        }
    }
}
