﻿using Akka.Actor;
using Akka.Routing;
using System;
using System.Collections.Generic;
using Vtmis.Core.Model.AkkaModel;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.Core.ActorModel.Actors.Report;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.Reports.Dto;
using Vtmis.Core.Common.Enums;
using System.Linq;
using Vtmis.Core.Model.AkkaModel.Messages.Report;
using Vtmis.Core.Common.Constants;
using Akka.Event;

namespace Vtmis.Core.ActorModel.Actors
{
    #region Akka Multi Tasking
    public class VesselReportSupervisorActor : ReceiveActor
    {
        protected IActorRef _vesselReportWorker;
        private readonly ILoggingAdapter _logger;
        protected override void PreStart()
        {
            Console.WriteLine("VesselReportSupervisorActor start...");
        }

        public VesselReportSupervisorActor(AppInfoModel param)
        {
            _logger = Context.GetLogger();
            int numberOfDays = 0;
            IActorRef actorBase = null;
            int currentCountReceiveAggregation = 0;
            var data = new List<ArrivalDepartureResponse>();
            var reportTypeId = 0;
            var minSpeedViolationResult = new List<ResponseSpeedViolationMinSpeed>();
            var maxSpeedViolationResult = new List<ResponseSpeedViolationMaxSpeed>();

            Receive<ReportGenerator>(m =>
            {
                _logger.Info($"Start generating report: {m.Report.Name} || From: {m.StartDate} To: {m.EndDate}");
                var filterReportActor = Context.ActorOf(Props.Create<FilterReportActor>(), "FilterReportActor");
                filterReportActor.Tell(new FilterReport(m, Sender));
            });

            //Receive<ReportGenerator>(m =>
            //{
            //    actorBase = Sender;
            //    var sod = m.StartDate;
            //    var eod = m.EndDate;
            //    numberOfDays = 0;
            //    currentCountReceiveAggregation = 0;
            //    reportTypeId = m.Report.ReportType.ReportTypeNumber.ToInt();
            //    data = new List<ArrivalDepartureResponse>();
            //    minSpeedViolationResult = new List<ResponseSpeedViolationMinSpeed>();
            //    maxSpeedViolationResult = new List<ResponseSpeedViolationMaxSpeed>();

            //    if (m.TimeSpan != ReportGeneratorTimeSpan.User_Defined)
            //    {
            //        var startEndDate = CommonHelper.ResolveReportTimeSpanToStartEndDate(m.TimeSpan);
            //        sod = startEndDate.First().Key;
            //        eod = startEndDate.First().Value;
            //    }

            //    if (eod.Date == sod.Date)
            //    {
            //        numberOfDays = 1;
            //    }
            //    else
            //    {
            //        numberOfDays = eod.Subtract(sod).Days + 1;
            //    }                

            //    _vesselReportWorker = Context.ActorOf(Props.Create(() => new VesselReportWorker())
            //        .WithRouter(new RoundRobinPool(numberOfDays)));

            //    foreach (var requestDate in CommonHelper.EachDay(sod, eod))
            //    {
            //        _vesselReportWorker.Tell(new StartQueryDatabase(requestDate, m.Report, sod));
            //    }
            //});

            Receive<CollectSpeedViolationWorker>(m =>
            {
                currentCountReceiveAggregation++;
                minSpeedViolationResult.AddRange(m.MinSpeedResultList);
                maxSpeedViolationResult.AddRange(m.MaxSpeedResultList);

                if (currentCountReceiveAggregation == numberOfDays)
                {
                    var result = data;
                    Console.WriteLine("Receive from all child worker");
                    actorBase.Tell(new { min = minSpeedViolationResult, max = maxSpeedViolationResult });
                }
            });

            Receive<QueryAggregation>(m =>
            {
                currentCountReceiveAggregation++;
                data.AddRange(m.Data);

                if (currentCountReceiveAggregation >= numberOfDays)
                {
                    var result = data;
                    Console.WriteLine("Receive from all child worker");
                    actorBase.Tell(new { data = result });
                }
            });
        }
    }

    public class FilterReport
    {
        public FilterReport(ReportGenerator reportGenerator, IActorRef sender)
        {
            ReportGenerator = reportGenerator;
            Sender = sender;
        }

        public ReportGenerator ReportGenerator { get; }
        public IActorRef Sender { get; }
    }

    //Message
    public class ReportGenerator
    {
        public ReportGenerator(DateTime startDate, DateTime endDate, ReportGeneratorTimeSpan timeSpan, ReportReponseDetailDto report)
        {
            StartDate = startDate;
            EndDate = endDate;
            Report = report;
            TimeSpan = timeSpan;
        }

        public DateTime StartDate { get; }
        public DateTime EndDate { get; }
        public ReportReponseDetailDto Report { get; }
        public ReportGeneratorTimeSpan TimeSpan { get; }
    }

    public class StartQueryDatabase
    {
        public StartQueryDatabase(DateTime date, ReportReponseDetailDto report, DateTime sod)
        {
            Date = date;
            Report = report;
            Sod = sod;
        }

        public DateTime Date { get; }
        public ReportReponseDetailDto Report { get; }
        public DateTime Sod { get; set; }
    }

    public class QueryAggregation
    {
        public QueryAggregation(DateTime date, List<ArrivalDepartureResponse> data)
        {
            Date = date;
            Data = data;
        }

        public DateTime Date { get; }
        public List<ArrivalDepartureResponse> Data { get; set; }
    }

    #endregion
}
