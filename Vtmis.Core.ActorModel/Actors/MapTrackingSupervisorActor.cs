﻿using Akka.Actor;
using Akka.DI.Core;
using Vtmis.Core.Model.ViewModels;

namespace Vtmis.Core.ActorModel.Actors
{
    public class MapTrackingSupervisorActor : ReceiveActor
    {
        public MapTrackingSupervisorActor()
        {
            var actorProps = Context.DI().Props<MapTrackingActor>();
            Context.ActorOf(actorProps, "MapTrackingActor");

            var actorProps2 = Context.DI().Props<VesselQueryActor>();
            Context.ActorOf(actorProps2, "VesselQueryActor");
        }
    }
}
