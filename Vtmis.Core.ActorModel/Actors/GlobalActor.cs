﻿using Akka.Actor;
using System;
using Vtmis.Core.Model.AkkaModel;

namespace Vtmis.Core.ActorModel.Actors
{
    public class GlobalActor : ReceiveActor
    {
        private DateTime _currentDateTime;
        public override void AroundPreStart()
        {
            base.AroundPreStart();
            Console.WriteLine("Starting Global Actor");
        }

        public GlobalActor()
        {
            Receive<RequestTargetMonitorDateTime>(m =>
            {
                Sender.Tell(_currentDateTime);
            });

            Receive<SetTargetMonitorDateTime>(m =>
            {
                Console.WriteLine($"Setting global date: {m.CurrentDate}");
                _currentDateTime = m.CurrentDate;
            });
        }
    }
}
