﻿using Akka.Actor;
using Akka.Event;
using EasyNetQ;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using Vtmis.Core.ActorModel.Hubs;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.Model.DTO.Target;
using Vtmis.Core.Model.ViewModels;
using Vtmis.WebVessel.Tracking.Services.Concrete;

namespace Vtmis.Core.ActorModel.Actors
{
    public class MapTrackingActor : ReceiveActor
    {
        private readonly ILoggingAdapter _logger;
        protected IHubContext<VesselGeoLocationHub> _hubContext;
        private readonly IConnectionMultiplexer _redis;
        protected List<VesselPosition> historyVesselPosition;
        protected List<VesselPosition> poolVesselPosition;
        protected string _queueId;
        protected IBus _bus;
        private readonly IDatabase _redisDb;

        public MapTrackingActor(IHubContext<VesselGeoLocationHub> hubContext, MapTransactionSpecification mapTransaction,
            IConnectionMultiplexer redis)
        {
            _logger = Context.GetLogger();
            _redis = redis;
            //_userIdProvider = userIdProvider;
            _redisDb = _redis.GetDatabase();
            _hubContext = hubContext;
            MapTransaction = mapTransaction;

            historyVesselPosition = new List<VesselPosition>();
            poolVesselPosition = new List<VesselPosition>();

            SetupHubReceive();
        }

        private void SetupHubReceive()
        {
            //Send to API actor
            // UpdateMap -> UpdateVesselPosition
            Receive<UpdateMap>(m =>
            {
                Console.WriteLine($"   {GetType().Name} : Execute receive actor => update map");

                //VesselAPI Actor
                //apiActor.Tell(new StartMap(m.UserId, VesselActorSystem.ApiUrl));

            });

            //Receive from VesselQuery
            ReceiveAsync<UpdateVesselPosition>(async m =>
            {

                if (!m.VesselPositions.Any())
                {
                    Console.WriteLine($"Update current maps - No vessel positions to update");
                    _logger.Warning($"Update current maps - No vessel positions to update");
                }
                try
                {
                    //var existingHp = await _redisDb.HashGetAllAsync($"vesselHistory_{AppHelper.GetEnvironmentName()}");
                    //if (existingHp.HasValue)
                    //{
                    //    MapTransaction.HistoricalVesselPosition = JsonConvert.DeserializeObject<List<VesselPosition>>(existingHp);
                    //}

                    var vpStorage = await _redisDb.HashGetAllAsync($"vesselHistory_{AppHelper.GetEnvironmentName()}");
                    if (vpStorage.Any())
                    {
                        var listVp = new List<VesselPosition>();
                        foreach (var item in vpStorage)
                        {
                            //var vr = new VesselPosition { VesselId = (int)item.Name };
                            var vp = JsonConvert.DeserializeObject<VesselPosition>(item.Value);
                            listVp.Add(vp);
                        }
                        MapTransaction.HistoricalVesselPosition = listVp;
                    }

                    var vtStorage = await _redisDb.HashGetAllAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}");
                    if (vtStorage.Any())
                    {
                        var listVt = new List<VesselRoute>();
                        foreach (var item in vtStorage)
                        {
                            var vr = new VesselRoute { VesselId = item.Name };
                            vr.VesselRoutePosition = JsonConvert.DeserializeObject<LinkedList<VesselRoutePosition>>(item.Value);
                            listVt.Add(vr);
                        }
                        MapTransaction.VesselRoutes = listVt;
                    }

                    foreach (var vp in m.VesselPositions)
                    {
                        var vesselPostIndex = MapTransaction.HistoricalVesselPosition.FindIndex(c => c.VesselId == vp.VesselId);
                        //var poolVesselPositionIndex = poolVesselPosition.FindIndex(x => x.VesselId == vp.VesselId);

                        //if (poolVesselPositionIndex != -1)
                        //{
                        //    poolVesselPosition[poolVesselPositionIndex] = vp;

                        //}
                        //else
                        //{
                        //    poolVesselPosition.Add(vp);
                        //}

                        if (vesselPostIndex != -1)
                        {
                            MapTransaction.HistoricalVesselPosition[vesselPostIndex] = vp;

                        }
                        else
                        {
                            MapTransaction.HistoricalVesselPosition.Add(vp);
                        }

                        //if (MapTransaction.HistoricalVesselPosition.Any())
                        //{
                        //    string serializedHp = JsonConvert.SerializeObject(MapTransaction.HistoricalVesselPosition);
                        //    await _redisDb.StringSetAsync($"vesselHistory_{AppHelper.GetEnvironmentName()}", serializedHp);
                        //}
                        #region VesselRoute


                        var vesselTrack = new AisMapTrackingService().AddTargetPreviousRoute(vp.VesselId,
                            vp.Lat, vp.Lgt, MapTransaction.VesselRoutes);


                        #endregion

                    }

                    if (MapTransaction.VesselRoutes.Any())
                    {
                        foreach (var item in MapTransaction.VesselRoutes)
                        {
                            var coordinate = JsonConvert.SerializeObject(item.VesselRoutePosition);
                            await _redisDb.HashSetAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}", item.VesselId, coordinate, When.Always, CommandFlags.FireAndForget);
                        }
                    }

                    if (MapTransaction.HistoricalVesselPosition.Any())
                    {
                        foreach (var item in MapTransaction.HistoricalVesselPosition)
                        {
                            var position = JsonConvert.SerializeObject(item);
                            await _redisDb.HashSetAsync($"vesselHistory_{AppHelper.GetEnvironmentName()}", item.VesselId, position, When.Always, CommandFlags.FireAndForget);
                        }
                    }

                    //var existingVt = await _redisDb.HashGetAllAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}");
                    //if (existingVt.Any())
                    //{
                    //    var listVesselRoute = new List<VesselRoute>();
                    //    foreach (var item in existingVt)
                    //    {
                    //        var vr = await _redisDb.HashGetAsync($"vesselTrack_{AppHelper.GetEnvironmentName()}", item.Name);

                    //        var p = JsonConvert.DeserializeObject<LinkedList<VesselRoutePosition>>(vr);
                    //        listVesselRoute.Add(new VesselRoute { VesselId = (int)item.Name, VesselRoutePosition = p });
                    //    }
                    //    MapTransaction.VesselRoutes = listVesselRoute;
                    //}
                }
                catch (Exception err)
                {
                    _logger.Error(err, err.Message);
                }


                Sender.Tell(true);

            });

            ReceiveAsync<VesselQueryComplete>(async m =>
            {
                //TargetOwner userTargets = await _dapperAdmin.GetTargetsByUserId(m.UserId.ToInt());
                //Console.WriteLine(DateTime.Now.ToString("dd/MM/yyyy hh:mm tt") + " Total vessels on maps " + mapTransaction.HistoricalVesselPosition.Count());
                //var data = userTargets.IsHost ? poolVesselPosition.ToList() : poolVesselPosition.Where(x => userTargets.TargetIdentifiers.Any(a => x.MMSI == a.MMSI)).ToList();
                var data = m.ResultVesselPositions;
                await _hubContext.Clients.All.SendAsync("UpdateCurrentVessel", new Vessel
                {
                    VesselPositions = data
                });

                //Publish data
                if (string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase))
                {
                    Console.WriteLine($" Update current maps - VesselCount");
                    Console.WriteLine("Start publish UpdateCurrentVessel");
                    Console.WriteLine($"PUSHED AT {data.Where(x=> x.MMSI == 995331081).OrderByDescending(o => o.UtcReceivedTime).Select(x => x.UtcReceivedTime).FirstOrDefault()}");
                }
            });

        }

        public MapTransactionSpecification MapTransaction { get; }
    }


    public class EventListenerStartupActor : ReceiveActor
    {
        public EventListenerStartupActor(IActorRef eventListenerActor)
        {
            //Receive<CheckEvent>(m =>
            //{
            //    Console.WriteLine("EventListenerActor");
            //    eventListenerActor.Tell(m);
            //});
        }
    }

    #region Messages
    public class UpdateCurrentVessel
    {
        public UpdateCurrentVessel(string queueId, List<VesselPosition> vesselPosition)
        {
            QueueId = queueId;
            VesselPosition = vesselPosition;
        }

        public string QueueId { get; }
        public List<VesselPosition> VesselPosition { get; }
    }

    public class Vessel
    {
        public List<VesselPosition> VesselPositions { get; set; }
        public List<VesselRoute> VesselRoutes { get; set; }
    }

    public class RestoreHistory
    {
        public RestoreHistory(string connectionId)
        {
            ConnectionId = connectionId;
        }

        public string ConnectionId { get; }
    }

    public class PersonalizeFilter
    {
        public PersonalizeFilter(string connectionId)
        {
            ConnectionId = connectionId;
        }

        public string ConnectionId { get; }
    }

    public class ExcludedConnectionId
    {
        public string ConnectionId { get; set; }
    }

    #endregion
}
