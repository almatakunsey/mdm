﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using Vtmis.Core.VesselEntityFrameworkCore;
using System.Linq;
using Vtmis.Core.Model.AkkaModel;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Abp.UI;
using Vtmis.Core.Model;
using Vtmis.WebVessel.Tracking.Services.Concrete;
using Vtmis.Core.Model.DTO;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Model.AkkaModel.Messages.Target;
using Akka.Event;
using Vtmis.Core.Model.AkkaModel.Messages.Target.RMS;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.ActorModel.Actors.Target;
using Akka.DI.Core;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Model.AkkaModel.Messages.Target.DDMS;

namespace Vtmis.Core.ActorModel.Actors
{
    public class VesselSearchApiActor : ReceiveActor
    {

        private AisDbInfoDto _aisDbInfo;
        private readonly ILoggingAdapter _logger;
        private readonly AppInfoModel _paramObject;
        private readonly IAisMapTrackingService _aisMapTrackingService;
        private readonly IMethydroService _methydroService;
        private readonly ITargetQuery _targetQuery;

        protected override void PreStart()
        {
            Console.WriteLine("Starting VesselSearchApiActor");
        }

        public override void AroundPreRestart(Exception cause, object message)
        {
            Console.WriteLine("Restarting VesselSearchApiActor");
        }

        public VesselSearchApiActor(AppInfoModel paramObject, IAisMapTrackingService aisMapTrackingService,
            IMethydroService methydroService, ITargetQuery targetQuery)
        {
            _logger = Context.GetLogger();
            _paramObject = paramObject;
            _aisMapTrackingService = aisMapTrackingService;
            _methydroService = methydroService;
            _targetQuery = targetQuery;
            var actorProps = Context.DI().Props<TargetQueryActor>();
            var targetQueryActor = Context.ActorOf(actorProps);
            Receive<RequestAton>(m =>
            {
                try
                {
                    _logger.Info("RequestAton started");
                    //var targetQueryActor = Context.ActorOf(Props.Create<TargetQueryActor>(), "TargetQueryActor");
                    if (m.Type == TargetClass.Aton) // AtonPosData
                    {
                        targetQueryActor.Tell(new RequestAtonList(m.PageIndex, m.PageSize), Sender);
                    }
                    else if (m.Type == TargetClass.MonitoringData) // AtonMonData
                    {
                        targetQueryActor.Tell(new RequestMonList(m.PageIndex, m.PageSize), Sender);
                    }
                    else if (m.Type == TargetClass.Methydro) // AtonMetData
                    {
                        targetQueryActor.Tell(new RequestMethydroList(m.PageIndex, m.PageSize), Sender);
                    }
                    else
                    {
                        Sender.Tell(new NullMessageResponse());
                    }
                }
                catch (Exception err)
                {
                    _logger.Error(err.Message, err);
                    Sender.Tell(new NullMessageResponse(err.Message, err.InnerException));
                }

            });

            ReceiveAsync<RequestRMS>(async m => await GetRMS(m));
            ReceiveAsync<RequestDDMS>(async m => await GetDDMS(m));
            ReceiveAsync<RequestDDMSHistorical>(async m => await GetDDMSHistorical(m));
            ReceiveAsync<RequestRmsHistorical>(async m => await GetRmsHistorical(m));
            //TODO: Will be remove
            Receive<GetAllShipPosition>(m =>
            {
                Console.WriteLine("GetAllShipPosition");
                using (var dbContext = new VesselDatabaseContext(m.PostFixDbName, m.SqlConnectionString, _paramObject.Username, _paramObject.Password))
                {
                    var shipPositions = dbContext.ShipPositions.Where(c => c.LocalRecvTime >= Convert.ToDateTime("2017-08-31 23:00:00"));
                    Sender.Tell(new ResponseGetAllShipPosition(shipPositions, _paramObject.ServerName));
                }
            });
            ReceiveAsync<RequestVesselInquiry>(async m => await GetTargetInfo(m));
            ReceiveAsync<TargetInfo>(async m => await QueryTargetInfoAsync(m));
            Receive<CommonTargetParams>(targetParams => QueryLastTargetPosition(targetParams));
            ReceiveAsync<VesselIWithPaginatedParams>(m => QueryTargetInfoByShipPositionIdAsync(m));
            ReceiveAsync<RequestMethydroStation>(async m => await GetAllMethydroStation());
            ReceiveAsync<RequestMethydroDetails>(async m => await GetMethydroList(m.FromDate, m.ToDate, m.MetHydroDataType));
            ReceiveAsync<RequestMethydroInstruments>(async m => await GetMethydroInstruments(m.MMSI, m.FromDate, m.ToDate, m.MetHydroDataType));
            ReceiveAsync<RequestLighthouse>(async m => await GetLighthouseAsync(m.MMSI));
        }

        private async Task GetDDMSHistorical(RequestDDMSHistorical m)
        {
            try
            {                
                _logger.Info($"Start Get DDMS Historical: MMSI - {m.MMSI}");
                var result = await _targetQuery.QueryDDMSHistoricalAsync(m.MMSI, m.StartDate, m.EndDate);
                if (result.Any() == false)
                {
                    Sender.Tell(new NullMessageResponse());
                }
                else
                {
                    Sender.Tell(result);
                }
                _logger.Info("End Get DDMS Historical");
            }
            catch (Exception err)
            {
                _logger.Error(err.Message, err);
                Sender.Tell(new NullMessageResponse(err.Message, err.InnerException));
            }
        }

        private async Task GetDDMS(RequestDDMS m)
        {
            try
            {
                _logger.Info($"Start Get DDMS: MMSI - {m.MMSI}");
                var result = await _targetQuery.QueryDDMSInfo(m.MMSI);
                if (result == null)
                {
                    Sender.Tell(new NullMessageResponse());
                }
                else
                {
                    Sender.Tell(result);
                }
                _logger.Info("End Get DDMS");
            }
            catch (Exception err)
            {
                _logger.Error(err.Message, err);
                Sender.Tell(new NullMessageResponse(err.Message, err.InnerException));
            }
        }

        private async Task GetLighthouseAsync(int mmsi)
        {
            try
            {
                var result = await _targetQuery.QueryLighthouse(mmsi);
                Sender.Tell(result);
            }
            catch (Exception err)
            {
                if (err is NotFoundException)
                {
                    var error = (NotFoundException)err;
                    Sender.Tell(error.Message);
                }
            }
        }

        private async Task GetRMS(RequestRMS m)
        {
            try
            {
                _logger.Info($"Start GetRMS: MMSI - {m.MMSI}");
                var result = await _targetQuery.QueryRMSInfoAsync(m.MMSI);
                if (result == null)
                {
                    Sender.Tell(new NullMessageResponse());
                }
                else
                {
                    Sender.Tell(result);
                }
                _logger.Info("End GetRMS");
            }
            catch (Exception err)
            {
                _logger.Error(err.Message, err);
                Sender.Tell(new NullMessageResponse(err.Message, err.InnerException));
            }
        }

        private async Task GetRmsHistorical(RequestRmsHistorical m)
        {
            try
            {
                _logger.Info("Starting GetRmsStatistic");
                var result = await _targetQuery.QueryRMSHistoricalAsync(m.MMSI, m.StartDate, m.EndDate);
                Sender.Tell(result);
                _logger.Info("End GetRmsStatistic");
            }
            catch (Exception err)
            {
                _logger.Error(err.Message, err);
                Sender.Tell(new NullMessageResponse(err.Message, err.InnerException));
            }
        }

        private async Task GetTargetInfo(RequestVesselInquiry m)
        {
            _logger.Info("Request Vessel Inquiry");
            try
            {
                var result = await _aisMapTrackingService.Get(m.MMSI, m.IMO, m.CallSign, m.TargetName, m.PageIndex, m.PageSize);
                if (result == null)
                {
                    Sender.Tell(new NullMessageResponse());
                }
                else
                {
                    Sender.Tell(result);
                }
            }
            catch (Exception err)
            {
                _logger.Error(err, err.Message);
                Sender.Tell(new NullMessageResponse());
            }
        }

        private async Task GetMethydroInstruments(int mmsi, DateTime fromDate, DateTime toDate, MetHydroDataType? metHydroDataType)
        {
            _logger.Info($"Start GetMethydroInstruments - {mmsi} || From: {fromDate} || To: {toDate}");
            try
            {
                var result = await _methydroService.GetInstrumentDataByMmsiDateRangeAsync(mmsi, fromDate, toDate);
                Sender.Tell(result);
                _logger.Info($"Done GetMethydroInstruments - {mmsi} || From: {fromDate} || To: {toDate}");
            }
            catch (Exception err)
            {
                _logger.Error(err, err.Message);
                Sender.Tell(new NullMessageResponse());
                //throw err;
            }

        }

        private async Task GetMethydroList(DateTime fromDate, DateTime toDate, MetHydroDataType? metHydroDataType)
        {
            _logger.Info($"Start GetMethydroList - From: {fromDate} || To: {toDate}");
            try
            {
                var result = await _methydroService.GetInstrumentDataByDateRangeAsync(fromDate, toDate,
                                   metHydroDataType);
                Sender.Tell(result);
                _logger.Info($"Done GetMethydroList - From: {fromDate} || To: {toDate}");
            }
            catch (Exception err)
            {
                _logger.Error(err, err.Message);
                Sender.Tell(new NullMessageResponse());
                //throw err;
            }

        }

        private async Task GetAllMethydroStation()
        {
            _logger.Info("Start GetAllMethydroStation");
            try
            {
                _aisDbInfo = CommonHelper.GetAisDbInfo(_paramObject.Environment, _paramObject.DbDate, _paramObject.ServerName,
              _paramObject.Username, _paramObject.Password);
                var result = await _methydroService.GetAllMethydroStationAsync();
                Sender.Tell(result);
                _logger.Info("Done GetAllMethydroStation");
            }
            catch (Exception err)
            {
                _logger.Error(err, err.Message);
                Sender.Tell(new NullMessageResponse());
                //throw err;
            }

        }

        private void QueryLastTargetPosition(CommonTargetParams targetParams)
        {
            _logger.Info("Start QueryLastTargetPosition");
            try
            {
                _aisDbInfo = CommonHelper.GetAisDbInfo(_paramObject.Environment, _paramObject.DbDate, _paramObject.ServerName,
             _paramObject.Username, _paramObject.Password);
                using (var dbContext = new VesselDatabaseContext(_aisDbInfo.AisDbName, _aisDbInfo.ServerName, _aisDbInfo.Username, _aisDbInfo.Password))
                {
                    //Console.WriteLine($"DB Name:{_postFixDbName} || Server Name:{ServerName} || Username:{Username} || Password:{Password}");
                    var shipsResult = dbContext.ShipStatics.Join(dbContext.ShipPositions, s => s.MMSI, p => p.MMSI, (s, p) =>
                                    new
                                    { s, p });

                    if (targetParams.MMSI > 0)
                    {
                        shipsResult = shipsResult.Where(x => x.s.MMSI == targetParams.MMSI);
                    }
                    if (targetParams.IMO > 0)
                    {
                        shipsResult = shipsResult.Where(x => x.s.IMO == targetParams.IMO);
                    }
                    if (!string.IsNullOrEmpty(targetParams.CallSign))
                    {
                        shipsResult = shipsResult.Where(x => x.s.CallSign == targetParams.CallSign);
                    }

                    var result = targetParams.TargetId > 0 ? dbContext.ShipStatics.Join(dbContext.ShipPositions, s => s.MMSI, p => p.MMSI, (s, p) =>
                                    new
                                    { s, p }).Where(x => x.s.MMSI == targetParams.TargetId).OrderByDescending(o => o.p.RecvTime)
                                    .Select(s => new
                                    {
                                        s.s.Name,
                                        s.s.IMO,
                                        s.s.MMSI,
                                        s.s.CallSign,
                                        s.p.Latitude,
                                        s.p.Longitude,
                                        s.s.ShipType,
                                        s.p.ROT,
                                        s.p.SOG,
                                        s.p.COG,
                                        s.p.TrueHeading,
                                        s.p.RecvTime,
                                        s.p.UTC,
                                        s.p.LocalRecvTime
                                    }).FirstOrDefault() :
                                    shipsResult.OrderByDescending(o => o.p.RecvTime)
                                    .Select(s => new
                                    {
                                        s.s.Name,
                                        s.s.IMO,
                                        s.s.MMSI,
                                        s.s.CallSign,
                                        s.p.Latitude,
                                        s.p.Longitude,
                                        s.s.ShipType,
                                        s.p.ROT,
                                        s.p.SOG,
                                        s.p.COG,
                                        s.p.TrueHeading,
                                        s.p.RecvTime,
                                        s.p.UTC,
                                        s.p.LocalRecvTime
                                    }).FirstOrDefault();


                    if (result != null)
                    {
                        Sender.Tell(new MmdisTargetInfo(result.Name, result.IMO, result.MMSI, null, result.CallSign,
                            result.Latitude, result.Longitude, result.ShipType, (float)result.ROT, (float)result.SOG,
                            (float)result.COG, result.TrueHeading, result.RecvTime, result.UTC, result.LocalRecvTime));
                    }
                    else
                    {
                        Sender.Tell(new MmdisTargetInfo());
                    }

                }
                _logger.Info("Done QueryLastTargetPosition");
            }
            catch (Exception err)
            {
                _logger.Error(err, err.Message);
                throw err;
            }

        }

        private async Task QueryTargetInfoByShipPositionIdAsync(VesselIWithPaginatedParams m)
        {
            _logger.Info("Start QueryTargetInfo By Ship Position ID");
            _aisDbInfo = CommonHelper.GetAisDbInfo(_paramObject.Environment, _paramObject.DbDate, _paramObject.ServerName,
                _paramObject.Username, _paramObject.Password);

            var objResult = new ModelResult<TargetInfo>();

            try
            {
                objResult.Result = await _aisMapTrackingService.GetTargetDetailsByShipPositionIdAsync(_aisDbInfo, m.VesselId);
                if (objResult.Result != null)
                {
                    objResult.IsSuccessful = true;
                }
                Sender.Tell(objResult);
                _logger.Info("Done QueryTargetInfo By Ship Position ID");
            }
            catch (Exception ex)
            {
                objResult.Errors.Add(ex.Message);
                Sender.Tell(objResult);
                _logger.Error(ex, ex.Message);
            }
        }
        private async Task QueryTargetInfoAsync(TargetInfo m)
        {
            Console.WriteLine("QueryTargetInfo");
            try
            {
                _aisDbInfo = CommonHelper.GetAisDbInfo(_paramObject.Environment, _paramObject.DbDate, _paramObject.ServerName,
              _paramObject.Username, _paramObject.Password);
                var result = await _aisMapTrackingService.GetTargetDetailsByShipPositionIdAsync(_aisDbInfo, m.Id);
                Sender.Tell(result);
            }
            catch (Exception err)
            {
                _logger.Error(err, err.Message);
                throw err;
            }

        }
    }
}
