﻿
using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Text;
using Vtmis.WebAdmin.Filters.v2.Dto;
using System.Linq;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.Common.Enums;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Vtmis.Core.ActorModel.Actors
{
    public class VesselFilterSupervisorApi:ReceiveActor
    {
        protected override void PreStart()
        {
            Console.WriteLine("VesselFilterSupervisorApi start");
        }
        public VesselFilterSupervisorApi(DateTime dbDate,string serverName,string userName,string passWord,string environment)
        {
           
            Receive<List<FilterItemResponse>>(m =>
            {
                var filterApiActor = Context.ActorOf(Props.Create(() => new VesselFilterApiActor(dbDate,serverName,userName,passWord,environment)));
                var queryString = filterApiActor.Ask<List<VesselColor>>(m).Result;
                Sender.Tell(queryString);
            });

        }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(
                maxNrOfRetries: 10,
                withinTimeRange: TimeSpan.FromMinutes(1),
                localOnlyDecider: ex =>
                {
                    if (ex is Exception)
                    {
                        Console.WriteLine("Restart");
                        return Directive.Restart;
                    }

                    return Akka.Actor.SupervisorStrategy.DefaultStrategy.Decider.Decide(ex);
                });
        }

    }

    public class CompleteTask
    {
        public CompleteTask(List<VesselColor> vesselColors, string color, string borderColor)
        {

            VesselColors = vesselColors;
            Color = color;
            BorderColor = borderColor;
        }


        public List<VesselColor> VesselColors { get; }
        public string Color { get; }
        public string BorderColor { get; }
    }

    #region ChildActor
    public class VesselFilterApiActor :ReceiveActor
    {
        public VesselFilterApiActor(DateTime dbDate, string serverName, string userName, string passWord, string environment)
        {
            string whereQueryString = "";
            int x = 0;


            string ServerName = serverName;
            DateTime StartDate = dbDate;
            string Username = userName;
            string Password = passWord;
            string Environment = environment;

            DateTime filterDate = DateTime.Now;

            string toDateTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string fromDateTime = DateTime.Now.AddMinutes(-1).ToString("yyyy-MM-dd HH:mm:ss");

            if (string.Equals(environment, Common.Constants.HostingEnvironment.LOCAL,StringComparison.CurrentCultureIgnoreCase))
            {
                filterDate = StartDate.AddMinutes(10);

                toDateTime = filterDate.ToString("yyyy-MM-dd HH:mm:ss");

                fromDateTime = filterDate.AddMinutes(-2).ToString("yyyy-MM-dd HH:mm:ss");

            }

            string _postFixDbName = $"AIS{filterDate.ToString("yyMMdd")}";

            int taskCount = 0;
            IActorRef originalSender = ActorRefs.Nobody;

            Receive<List<FilterItemResponse>>(m =>
            {
                originalSender = Sender;

                var colors = m.GroupBy(c => new { c.Color, c.BorderColor });
                taskCount = colors.Count();

                foreach (var color in colors)
                {
                    var vesselFilterActor = Context.ActorOf(Props.Create(()=>new VesselFilterCriteriaActor(toDateTime,fromDateTime, _postFixDbName,serverName,userName,passWord)));
                    vesselFilterActor.Tell(m.Where(y => y.Color == color.Key.Color).ToList());
                }


            });

            int receiveCompleteTaskCount = 0;
            List<VesselColor> vesselColors = new List<VesselColor>();
            Receive<CompleteTask>(m =>
            {
                receiveCompleteTaskCount++;
                Console.WriteLine(receiveCompleteTaskCount + " " + taskCount);

                Console.WriteLine($"{_postFixDbName} {serverName} {userName} {passWord}");

                vesselColors.AddRange(m.VesselColors);

                if (receiveCompleteTaskCount == taskCount)
                    originalSender.Tell(vesselColors);
            });
        }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(
                maxNrOfRetries: 10,
                withinTimeRange: TimeSpan.FromMinutes(1),
                localOnlyDecider: ex =>
                {
                    if (ex is Exception)
                    {
                        Console.WriteLine("Restart");
                        return Directive.Restart;
                    }

                    return Akka.Actor.SupervisorStrategy.DefaultStrategy.Decider.Decide(ex);
                });
        }
    }

    public class VesselFilterCriteriaActor : ReceiveActor
    {
        public VesselFilterCriteriaActor(string toDateTime,string fromDateTime, string _postFixDbName,string serverName, string userName, string passWord)
        {
            
            Receive<List<FilterItemResponse>>(m =>
            {
                int index = 0;
                string whereQueryString = $"select a.MMSI from ShipPosition a inner join ShipStatic b on a.MMSI=b.MMSI where a.LocalRecvTime>'{fromDateTime}' and a.LocalRecvTime<='{toDateTime}' and ";
                string color = "";
                string borderColor = "";

                foreach (var filterItemResponse in m)
                {
                    color = filterItemResponse.Color;
                    borderColor = filterItemResponse.BorderColor;

                    var querySpec = Context.ActorOf(Props.Create(() => new VesselFilterApiWorkerActor()));
                    string op = "";
                    if (index > 0)
                        op = filterItemResponse.AndOrOperator;

                    whereQueryString += $"{op} {querySpec.Ask<string>(filterItemResponse).Result}";
                    index++;
                }

                Console.WriteLine(whereQueryString);

                Task.Run(() =>
                {
                    Console.WriteLine("Execute..." + whereQueryString);
                    using (var dbContext = new VesselDatabaseContext(_postFixDbName, serverName, userName, passWord))
                    {
                        var vesselresults = dbContext.ShipPositions.FromSql(whereQueryString + " group by a.MMSI").Select(v => new VesselColor { MMSI = v.MMSI.ToString(), Border = color, BorderColor = borderColor }).ToList();
                        return new CompleteTask(vesselresults, color, borderColor);
                    }
                }).PipeTo(Sender);
            });
        }
    }

    public class VesselFilterApiWorkerActor :ReceiveActor
    {
        public string whereQueryString = "";

        public VesselFilterApiWorkerActor()
        {
            Receive<FilterItemResponse>(
                p => (p.Name.Equals("MMSI") || p.Name.Equals("Name") || p.Name.Equals("Call Sign")) && p.Operator.Equals("="),
                m =>
                {
                    var columnName = m.Name.Replace(" ", "");
                    if (m.Value.Contains("*"))
                    {
                        var value = m.Value.Replace("*", "");

                        Sender.Tell($" a.{columnName} like '{value}%' ");
                    }
                    else
                    {
                        Sender.Tell($" a.{columnName} = '{m.Value}' ");
                    }
                }
            );
            Receive<FilterItemResponse>(
                p => (p.Name.Equals("MMSI") || p.Name.Equals("Name") || p.Name.Equals("Call Sign")) && p.Operator.Equals("!="),
                m =>
                {
                    var columnName = m.Name.Replace(" ", "");
                    if (m.Value.Contains("*"))
                    {
                        var value = m.Value.Replace("*", "");
                        Sender.Tell($" a.{columnName} not like '{value}%' ");
                    }
                    else
                    {
                        Sender.Tell($" a.{columnName} != '{m.Value}' ");
                    }
                }
            );
            Receive<FilterItemResponse>(
                p => p.Name.Equals("Ship Type"),
                m => {
                    Sender.Tell($" b.ShipType = {m.Value} ");
                }
            );
            Receive<FilterItemResponse>(
                p => p.Name.Equals("AIS Type"),
                m => {
                    Sender.Tell($" a.MessageId = {m.Value} ");
                }
            );

            Receive<FilterItemResponse>(
               p => p.Name.Equals("Min COG"),
               m => {
                   if(m.Operator.Equals(">"))
                   {
                       Sender.Tell($" a.COG > {m.Value} ");
                   }
                   else if(m.Operator.Equals(">="))
                   {
                       Sender.Tell($" a.COG >= {m.Value} ");
                   }
                   else if (m.Operator.Equals("="))
                   {
                       Sender.Tell($" a.COG = {m.Value} ");
                   }
               }
            );
            Receive<FilterItemResponse>(
               p => p.Name.Equals("Max COG"),
               m => {
                   if (m.Operator.Equals("<"))
                   {
                       Sender.Tell($" a.COG < {m.Value} ");
                   }
                   else if (m.Operator.Equals("<="))
                   {
                       Sender.Tell($" a.COG <= {m.Value} ");
                   }
                   else if (m.Operator.Equals("="))
                   {
                       Sender.Tell($" a.COG = {m.Value} ");
                   }
               }
           );

            Receive<FilterItemResponse>(
               p => p.Name.Equals("Min SOG"),
               m => {
                   if (m.Operator.Equals(">"))
                   {
                       Sender.Tell($" a.SOG > {m.Value} ");
                   }
                   else if (m.Operator.Equals(">="))
                   {
                       Sender.Tell($" a.SOG >= {m.Value} ");
                   }
                   else if (m.Operator.Equals("="))
                   {
                       Sender.Tell($" a.SOG = {m.Value} ");
                   }
               }
            );
            Receive<FilterItemResponse>(
               p => p.Name.Equals("Max SOG"),
               m => {
                   if (m.Operator.Equals("<"))
                   {
                       Sender.Tell($" a.SOG < {m.Value} ");
                   }
                   else if (m.Operator.Equals("<="))
                   {
                       Sender.Tell($" a.SOG <= {m.Value} ");
                   }
                   else if (m.Operator.Equals("="))
                   {
                       Sender.Tell($" a.SOG = {m.Value} ");
                   }
               }
           );

            Receive<FilterByNameResponse>(m =>
            {
                Sender.Tell(new Exception("Invalid input entered"));
            });
        }
    }
    #endregion
}
