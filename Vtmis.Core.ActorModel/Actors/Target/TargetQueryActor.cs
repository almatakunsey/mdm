﻿using Akka.Actor;
using Akka.Event;
using System;
using System.Threading.Tasks;
using Vtmis.Core.Model.AkkaModel;
using Vtmis.Core.Model.AkkaModel.Messages.Target;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;

namespace Vtmis.Core.ActorModel.Actors.Target
{
    public class TargetQueryActor : ReceiveActor
    {
        private readonly ILoggingAdapter _logger;
        private readonly VtsDatabaseContext _vtsDatabaseContext;
        private readonly ITargetQuery _targetQuery;

        public TargetQueryActor(VtsDatabaseContext vtsDatabaseContext, ITargetQuery targetQuery)
        {
            _logger = Context.GetLogger();
            _logger.Info("TargetQueryActor started");
            _vtsDatabaseContext = vtsDatabaseContext;
            _targetQuery = targetQuery;
            ReceiveAsync<RequestAtonList>(m => GetAtonList(m));
            //Receive<RequestMethydroList>(m => GetMetList(m));
            ReceiveAsync<RequestMonList>(m => GetMonList(m));
        }

        private async Task GetMonList(RequestMonList m)
        {
            _logger.Info("RequestMonList started");
            try
            {
                var result = await _targetQuery.QueryMonListAsync(m);
                if (result == null)
                {
                    Sender.Tell(new NullMessageResponse());
                }
                else
                {
                    Sender.Tell(result);
                }

            }
            catch (Exception err)
            {
                Sender.Tell(new NullMessageResponse(err.Message, err.InnerException));
            }
            _logger.Info("RequestMonList ended");
        }           

        //private void GetMetList(RequestMethydroList m)
        //{
        //    _logger.Info("RequestMethydroList started");
        //    var requestDate = AppHelper.GetAisDateBasedOnCurrentEnv();
        //    using (var aisDbContext = new VesselDatabaseContext(requestDate))
        //    {
        //        var qry = aisDbContext.Query<QueryMethydroList>().FromSql(@"
        //            SELECT (ap.Name) AisName, ap.Name, ap.Latitude, ap.Longitude, ap.AtoNType,
        //            ap.VirtualAtoN, ap.OffPosition21, ap.RecvTime6, ap.RecvTime8,
        //            ap.RecvTime21, am.OffPosition68, met.AirTemp, met.AirPres, met.WndSpeed, met.WndGust, 
        //            met.WndDir, met.MMSI
        //            --AtonMetData
        //            FROM (
        //                SELECT imet.AirTemp, imet.AirPres, imet.WndSpeed, imet.WndGust, imet.WndDir, imet.MMSI
        //                --AtonMetDataGroup
        //                FROM (
        //                    SELECT MMSI, MAX(RecvTime) RecvTime FROM AtonMetData
        //                    WHERE AirTemp IS NOT NULL OR AirPres IS NOT NULL OR WndSpeed IS NOT NULL OR WndGust IS NOT NULL OR WndDir IS NOT NULL
        //                    GROUP BY MMSI
        //                ) gmet
        //                INNER JOIN AtonMetData imet on gmet.MMSI=imet.MMSI
        //                WHERE imet.RecvTime=gmet.RecvTime
        //            ) met
        //            --AtonMonData
        //            OUTER APPLY
        //            (
        //                SELECT TOP 1 OffPosition68
        //                FROM AtonMonData gam
        //                WHERE gam.MMSI=met.MMSI
        //                ORDER BY gam.RecvTime DESC
        //            ) am
        //            --AtonPosData
        //            OUTER APPLY
        //            (
        //                SELECT TOP 1 Latitude, Longitude, Name, AtoNType, VirtualAtoN,
        //                OffPosition21, RecvTime6, RecvTime8, RecvTime21
        //                FROM AtonPosData gap
        //                WHERE gap.MMSI=met.MMSI
        //                ORDER BY gap.RecvTime DESC
        //            ) ap
        //        ").AsQueryable();

        //        var p = new PaginatedList<QueryMethydroList>().CreateTemp(qry, m.PageIndex, m.PageSize);
        //        var result = new PaginatedList<ResponseMethydroList>().SetNull();

        //        result.Items = p.Items
        //       .Select(s => new ResponseMethydroList
        //       {
        //           Name =
        //           _vtsDatabaseContext.Atons.FirstOrDefault(x => x.AtonMMSI == s.MMSI) != null ?
        //            _vtsDatabaseContext.Atons.FirstOrDefault(x => x.AtonMMSI == s.MMSI).AtonName :
        //            s.AisName,
        //           AisName = s.AisName,
        //           LastReport21 = s.RecvTime21.HasValue ? DateTime.SpecifyKind(s.RecvTime21.Value, DateTimeKind.Utc).ToLocalTime().TimeAgo() : null,
        //           LastReport6 = s.RecvTime6.HasValue ? DateTime.SpecifyKind(s.RecvTime6.Value, DateTimeKind.Utc).ToLocalTime().TimeAgo() : null,
        //           LastReport8 = s.RecvTime8.HasValue ? DateTime.SpecifyKind(s.RecvTime8.Value, DateTimeKind.Utc).ToLocalTime().TimeAgo() : null,
        //           Latitude = s.Latitude.ToStringOrDefault(),
        //           Longitude = s.Longitude.ToStringOrDefault(),
        //           MMSI = s.MMSI,
        //           OffPosition21 = TargetHelper.GetOffPosition(s.OffPosition21),
        //           OffPosition68 = TargetHelper.GetOffPosition(s.OffPosition68),
        //           Type = TargetHelper.GetAtonTypeByAtonTypeId(s.AtoNType),
        //           VirtualAton = TargetHelper.GetVirtualAtonStatus(s.VirtualAtoN),
        //           AirPres = s.AirPres,
        //           AirTemp = s.AirTemp,
        //           WndDir = s.WndDir,
        //           WndGust = s.WndGust,
        //           WndSpeed = s.WndSpeed
        //       }).ToList();

        //        result.TotalItems = p.TotalItems;
        //        result.PageIndex = p.PageIndex;
        //        result.TotalPages = p.TotalPages;

        //        Sender.Tell(result);
        //    }

        //    _logger.Info("RequestMethydroList ended");
        //}

        private async Task GetAtonList(RequestAtonList m)
        {
            _logger.Info("RequestAtonList started");
            try
            {
                var result = await _targetQuery.QueryAtonListAsync(m);
                if (result == null)
                {
                    Sender.Tell(new NullMessageResponse());
                }
                else
                {
                    Sender.Tell(result);
                }
              
            }
            catch (Exception err)
            {
                Sender.Tell(new NullMessageResponse(err.Message, err.InnerException));
            }


            _logger.Info("RequestAtonList ended");
        }


    }
}
