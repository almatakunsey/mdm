﻿using Akka.Actor;
using Akka.Event;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Request.HostAdmin;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.WebAdmin.Filters.v2.Dto;

namespace Vtmis.Core.ActorModel.Actors.Report
{
    public class FilterReportActor : ReceiveActor
    {
        private readonly ILoggingAdapter _logger;

        public FilterReportActor()
        {
            _logger = Context.GetLogger();
            //Receive<FilterReport>(m => ProcessReport(m));
        }

        private void ProcessReport(FilterReport m)
        {
            _logger.Info("Filtering Report");
            Expression<Func<QueryAllShipPosStatic, bool>> expression = e => true;
            foreach (var reportItem in m.ReportGenerator.Report.ReportItems)
            {
                expression = expression.Or(Filter(reportItem.Filter));
            }

            var queryReportActor = Context.ActorOf(Props.Create<QueryReportActor>(), "QueryReportActor");
            var startDate = m.ReportGenerator.StartDate;
            var endDate = m.ReportGenerator.EndDate;

            if (m.ReportGenerator.TimeSpan != ReportGeneratorTimeSpan.User_Defined)
            {
                var startEndDate = CommonHelper.ResolveReportTimeSpanToStartEndDate(m.ReportGenerator.TimeSpan);
                startDate = startEndDate.First().Key;
                endDate = startEndDate.First().Value;
            }

            using (var aisContext = new VesselDatabaseContext($"2017-08-31 10:00:00".ToDateTime()))
            {
                var p0 = 372095000;
                //var qry = aisContext.Query<ExampleModel>().FromSql($@"
                //    SELECT TOP 100 MMSI, Latitude, Longitude, RecvTime, LocalRecvTime, MessageId FROM ShipPosition
                //    WHERE {TestExp(p0)}
                //");
                //var result = qry.ToList();
                //m.Sender.Tell(new { result });
            }
            //queryReportActor.Tell(new QueryReport(startDate, endDate, m.ReportGenerator.Report));
        }

        //    using (var aisContext = new VesselDatabaseContext($"2017-08-31 10:00:00".ToDateTime()))
        //    {
        //        var p0 = 372095000;
        //        var qry = aisContext.Query<ExampleModel>().FromSql($@"
        //            SELECT TOP 100 MMSI, Latitude, Longitude, RecvTime, LocalRecvTime, MessageId FROM ShipPosition
        //            WHERE {TestExp(p0)}
        //        ");
        //        var result = qry.ToList();
        //        m.Sender.Tell(new { result });
        //    }
        //    //queryReportActor.Tell(new QueryReport(startDate, endDate, m.ReportGenerator.Report));
        //}
        
        private Expression<Func<QueryAllShipPosStatic, bool>> Filter(FilterResponseDto filter)
        {
            Expression<Func<QueryAllShipPosStatic, bool>> expression = x => true;
            if (filter.IsDeleted == false)
            {
                foreach (var filterItem in filter.FilterItems)
                {
                    foreach (var filterDetail in filterItem.FilterDetails)
                    {
                        Expression<Func<QueryAllShipPosStatic, bool>> expression2 = x => true;

                        if (string.Equals(filterDetail.ColumnName,
                            FilterFieldsConst.MMSI, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => x.MMSI != Convert.ToInt32(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.CONTAINS)
                            {
                                expression2 = x => x.MMSI.ToString().Contains(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.START_WITH)
                            {
                                expression2 = x => x.MMSI.ToString().StartsWith(filterDetail.ColumnValue,
                                        StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => x.MMSI == Convert.ToInt32(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                            FilterFieldsConst.IMO, StringComparison.CurrentCultureIgnoreCase))
                        {

                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => x.IMO != Convert.ToInt32(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.CONTAINS)
                            {
                                expression2 = x => x.IMO.HasValue &&
                                    x.IMO.ToString().Contains(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.START_WITH)
                            {
                                expression2 = x => x.IMO.HasValue
                                  && x.IMO.ToString().StartsWith(filterDetail.ColumnValue, StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => x.IMO.HasValue && x.IMO == Convert.ToInt32(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.CallSign, StringComparison.CurrentCultureIgnoreCase))
                        {

                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.CallSign) && !string.Equals(x.CallSign,
                                    filterDetail.ColumnValue, StringComparison.CurrentCultureIgnoreCase);
                            }
                            else if (filterDetail.Operation == Operation.CONTAINS)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.CallSign)
                                    && x.CallSign.Contains(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.START_WITH)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.CallSign)
                                  && x.CallSign.ToString().StartsWith(filterDetail.ColumnValue,
                                  StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.CallSign) && string.Equals(x.CallSign,
                                    filterDetail.ColumnValue, StringComparison.CurrentCultureIgnoreCase);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.ShipName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.Name) && !string.Equals(x.Name,
                                    filterDetail.ColumnValue, StringComparison.CurrentCultureIgnoreCase);
                            }
                            else if (filterDetail.Operation == Operation.CONTAINS)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.Name)
                                    && x.Name.Contains(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.START_WITH)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.Name)
                                  && x.Name.ToString().StartsWith(filterDetail.ColumnValue,
                                    StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.Name)
                                && string.Equals(x.Name, filterDetail.ColumnValue,
                                    StringComparison.CurrentCultureIgnoreCase);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.ShipType, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var shipType = TargetHelper.GetShipTypeByName(filterDetail.ColumnValue);
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => !string.Equals(x.ShipType.HasValue ? TargetHelper.GetShipTypeByShipTypeId(x.ShipType)
                                   : null, shipType, StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => string.Equals(x.ShipType.HasValue ? TargetHelper.GetShipTypeByShipTypeId(x.ShipType)
                                    : null, shipType, StringComparison.CurrentCultureIgnoreCase);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.GroupType, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var result = new FilterGroupRequest().GetFilterGroupById(Convert.ToInt32(filterDetail.ColumnName)).Result;

                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => !result.MMSIList.Any(i => i == x.MMSI.ToString()) ||
                                   result.CallSignList.Any(c => string.Equals(c, x.CallSign)) ||
                                   result.NameList.Any(n => string.Equals(n, x.Name));
                            }
                            else
                            {
                                expression2 = x => result.MMSIList.Any(i => i == x.MMSI.ToString()) ||
                                   result.CallSignList.Any(c => string.Equals(c, x.CallSign)) ||
                                   result.NameList.Any(n => string.Equals(n, x.Name));
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.NavStatus, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var navStatus = TargetHelper.GetNavStatusByName(filterDetail.ColumnValue);
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => !string.Equals(x.NavigationalStatus.HasValue ? TargetHelper.GetNavStatusDescriptionByNavStatusId(x.NavigationalStatus)
                                   : null, navStatus, StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => string.Equals(x.NavigationalStatus.HasValue ? TargetHelper.GetNavStatusDescriptionByNavStatusId(x.NavigationalStatus)
                                    : null, navStatus, StringComparison.CurrentCultureIgnoreCase);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.AisType, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var messageId = TargetHelper.GetMessageIdByTargetClass(filterDetail.ColumnValue);

                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => messageId.Any(p => p != x.MessageId);
                            }
                            else
                            {
                                expression2 = x => messageId.Any(p => p == x.MessageId);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.DataSource, StringComparison.CurrentCultureIgnoreCase))
                        {
                            //TODO
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MinSpeed, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                //expression2 = x => x.sp.SOG >= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                            else
                            {
                                expression2 = x => x.SOG >= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MaxSpeed, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                //expression2 = x => x.sp.SOG >= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                            else
                            {
                                expression2 = x => x.SOG <= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MinCourse, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                expression2 = x => x.COG >= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MaxCourse, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                expression2 = x => x.COG <= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MinLength, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                expression2 = x => x.Dimension_A.HasValue && x.Dimension_B.HasValue
                                    && Convert.ToInt32(filterDetail.ColumnValue) >=
                                    (x.Dimension_A + x.Dimension_B);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MaxLength, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                expression2 = x => x.Dimension_A.HasValue && x.Dimension_B.HasValue
                                    && Convert.ToInt32(filterDetail.ColumnValue) <=
                                    (x.Dimension_A + x.Dimension_B);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                          FilterFieldsConst.MinAge, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                // In minutes
                                expression2 = x => CommonHelper.GetAgingViaLocalRecvTime(x.LocalRecvTime.Value).TotalMinutes >=
                                    Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                          FilterFieldsConst.MaxAge, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                // In minutes
                                expression2 = x => CommonHelper.GetAgingViaLocalRecvTime(x.LocalRecvTime.Value).TotalMinutes <=
                                    Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        Expression<Func<QueryAllShipPosStatic, bool>> expression3 = filterDetail.Condition == Condition.AND ?
                            expression.And(expression2) : expression.Or(expression2);

                        expression = expression3;
                    }
                }
            }
            return expression;
        }
    }
}
