﻿using Akka.Actor;
using Akka.Event;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages.Report;
using Vtmis.Core.Request.HostAdmin;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.WebAdmin.Filters.v2.Dto;
using Vtmis.WebAdmin.Reports.Dto;
using Vtmis.WebAdmin.Zones;

namespace Vtmis.Core.ActorModel.Actors.Report
{

    public class VesselReportWorker : ReceiveActor
    {
        private readonly ILoggingAdapter _logger;

        public VesselReportWorker()
        {
            _logger = Context.GetLogger();
            Console.WriteLine("Report worker started");
            Receive<StartQueryDatabase>(m =>
            {
                if (m.Report.ReportType.ReportTypeNumber.ToInt() == ReportTypeConst.ArrivalDeparture)
                {
                    ArrivalDeparture(m);
                }
                else if (m.Report.ReportType.ReportTypeNumber.ToInt() == ReportTypeConst.SpeedViolation)
                {
                    SpeedViolation(m);
                }
            });
        }

        private void SpeedViolation(StartQueryDatabase m)
        {
            _logger.Info($"Start Quering Speed Violation Report: {m.Date} || {m.Report.Id} || {m.Report.Name}");
            try
            {
                using (var aisContext = new VesselDatabaseContext(m.Date))
                {
                    //var qry = from sp in aisContext.ShipPositions.AsNoTracking().Where(y => y.LocalRecvTime.Value.Date == m.Date.Date && y.LocalRecvTime.Value.TimeOfDay <= m.Date.TimeOfDay) //orderby s.RecvTime descending
                    //          from ss in aisContext.ShipStatics.AsNoTracking().Where(x => x.MMSI == sp.MMSI).DefaultIfEmpty()
                    //          select new ShipPositionStatic { sp = sp, ss = ss };

                    var p0 = m.Date;

                    var qry = aisContext.Query<QueryAllShipPosStatic>().FromSql(@"
                    SELECT spG.RecvTime,spG.LocalRecvTime, spG.MMSI, IMO, CallSign, Name, ShipType, spG.NavigationalStatus, MessageId, SOG, COG, 
                    Dimension_A, Dimension_B, Dimension_C, Dimension_D, Latitude, Longitude,
                    Destination, ETA FROM (SELECT sp.MMSI, sp.MessageId, sp.SOG, sp.COG, sp.LocalRecvTime, sp.RecvTime, sp.Latitude, sp.NavigationalStatus, sp.Longitude FROM
                    (SELECT MMSI, MAX(LocalRecvTime) LocalRecvTime FROM ShipPosition WHERE LocalRecvTime <= @p0 GROUP BY MMSI) spList 
                    INNER JOIN ShipPosition sp on spList.MMSI = sp.MMSI WHERE spList.LocalRecvTime = sp.LocalRecvTime) spG
                    LEFT JOIN ShipStatic ss on spG.MMSI = ss.MMSI
                    ", p0).AsNoTracking().AsQueryable();

                    var minSpeedResultList = new List<ResponseSpeedViolationMinSpeed>();
                    var maxSpeedResultList = new List<ResponseSpeedViolationMaxSpeed>();
                    Expression<Func<QueryAllShipPosStatic, bool>> expression = x => x.LocalRecvTime.Value.Date == m.Date.Date;

                    foreach (var reportItem in m.Report.ReportItems)
                    {
                        var vesselWithHighSpeed = new List<VesselWithHighSpeed>();
                        var vesselWithLowSpeed = new List<VesselWithMinSpeed>();

                        expression = Filter(reportItem.Filter, m.Date);
                        var subQuery = qry.Where(expression);

                        if (reportItem.Zone.ZoneType == ZoneType.Circle)
                        {
                            var lat = reportItem.Zone.ZoneItems.First().Latitude;
                            var lng = reportItem.Zone.ZoneItems.First().Logitude;
                            var rad = reportItem.Zone.Radius;

                            subQuery = subQuery.Where(w => CommonHelper.IsPointInCircle(Convert.ToDouble(lat), Convert.ToDouble(lng), Convert.ToDouble(rad),
                                                w.Latitude, w.Longitude));

                            if (subQuery.FirstOrDefault() == null)
                            {
                                continue;
                            }

                            foreach (var item in subQuery.OrderBy(x => x.LocalRecvTime))
                            {
                                FilterSpeedViolationData(item, reportItem, vesselWithLowSpeed, vesselWithHighSpeed, 
                                    minSpeedResultList, maxSpeedResultList);
                            }
                        }

                        if (reportItem.Zone.ZoneType == ZoneType.Polygon)
                        {
                            var polygon = reportItem.Zone.ZoneItems.Select(s => new PointF
                            {
                                X = (float)Convert.ToDouble(s.Latitude),
                                Y = (float)Convert.ToDouble(s.Logitude)
                            });


                            subQuery = subQuery.Where(w => CommonHelper.IsPointInPolygon(polygon.ToArray(), new PointF
                            {
                                X = (float)w.Latitude,
                                Y = (float)w.Longitude
                            }));

                            if (subQuery.FirstOrDefault() == null)
                            {
                                continue;
                            }

                            foreach (var item in subQuery.OrderBy(x => x.LocalRecvTime))
                            {
                                FilterSpeedViolationData(item, reportItem, vesselWithLowSpeed, vesselWithHighSpeed, minSpeedResultList, maxSpeedResultList);
                            }
                        }


                        if (reportItem.Zone.ZoneType == ZoneType.Line_L || reportItem.Zone.ZoneType == ZoneType.Line_R)
                        {
                            var line = reportItem.Zone.ZoneItems.Select(s => new PointF
                            {
                                X = (float)Convert.ToDouble(s.Latitude),
                                Y = (float)Convert.ToDouble(s.Logitude)
                            }).ToList();

                            if (subQuery.FirstOrDefault() == null)
                            {
                                continue;
                            }
                            double latCounter = 0;
                            double longCounter = 0;

                            foreach (var item in subQuery.OrderBy(x => x.MMSI))
                            {
                                if (latCounter == 0 && longCounter == 0)
                                {
                                    latCounter = item.Latitude;
                                    longCounter = item.Longitude;
                                    continue;
                                }

                                if (CommonHelper.DoIntersect(line[0],
                                    line[1], new PointF { X = (float)latCounter, Y = (float)longCounter },
                                    new PointF { X = (float)item.Latitude, Y = (float)item.Longitude }
                                    ))
                                {

                                    FilterSpeedViolationData(item, reportItem, vesselWithLowSpeed, vesselWithHighSpeed, minSpeedResultList, maxSpeedResultList);

                                    latCounter = item.Latitude;
                                    longCounter = item.Longitude;
                                }
                            }
                        }

                    }
                    Sender.Tell(new CollectSpeedViolationWorker(minSpeedResultList, maxSpeedResultList));

                }
            }
            catch (Exception err)
            {
                _logger.Error(err, err.Message);
                throw err;
            }

            _logger.Info($"Done Quering Speed Violation Report: {m.Date} || {m.Report.Id} || {m.Report.Name}");
        }

        private void ArrivalDeparture(StartQueryDatabase m)
        {
            _logger.Info($"Start Quering Arrival Departure Report: {m.Date} || {m.Report.Id} || {m.Report.Name}");
            try
            {
                using (var aisContext = new VesselDatabaseContext(m.Date))
                {
                    Console.WriteLine($"Start query {m.Date}");
                    aisContext.Database.SetCommandTimeout(new TimeSpan(0, 5, 0));

                    //var qry = from sp in aisContext.ShipPositions.AsNoTracking().Where(y => y.LocalRecvTime.Value.Date == m.Date.Date && y.LocalRecvTime.Value.TimeOfDay <= m.Date.TimeOfDay)
                    //          from ss in aisContext.ShipStatics.AsNoTracking().Where(x => x.MMSI == sp.MMSI).DefaultIfEmpty()
                    //          select new ShipPositionStatic { sp = sp, ss = ss };
                    var p0 = m.Date;

                    var qry = aisContext.Query<QueryAllShipPosStatic>().FromSql(@"
                    SELECT spG.RecvTime,spG.LocalRecvTime, spG.MMSI, IMO, CallSign, Name, ShipType, spG.NavigationalStatus, MessageId, SOG, COG, 
                    Dimension_A, Dimension_B, Dimension_C, Dimension_D, Latitude, Longitude,
                    Destination, ETA FROM (SELECT sp.MMSI, sp.MessageId, sp.SOG, sp.COG, sp.LocalRecvTime, sp.RecvTime, sp.Latitude, sp.NavigationalStatus, sp.Longitude FROM
                    (SELECT MMSI, MAX(LocalRecvTime) LocalRecvTime FROM ShipPosition WHERE LocalRecvTime <= @p0 GROUP BY MMSI) spList 
                    INNER JOIN ShipPosition sp on spList.MMSI = sp.MMSI WHERE spList.LocalRecvTime = sp.LocalRecvTime) spG
                    LEFT JOIN ShipStatic ss on spG.MMSI = ss.MMSI
                    ", p0).AsNoTracking().AsQueryable();

                    var results = new List<ArrivalDepartureResponse>();

                    foreach (var reportItem in m.Report.ReportItems)
                    {
                        Expression<Func<QueryAllShipPosStatic, bool>> expression = x => x.LocalRecvTime.Value.Date == m.Date.Date;
                        expression = Filter(reportItem.Filter, m.Date);
                        var sideQry = qry.Where(expression);

                        if (reportItem.Zone.ZoneType == ZoneType.Circle)
                        {
                            var lat = reportItem.Zone.ZoneItems.First().Latitude;
                            var lng = reportItem.Zone.ZoneItems.First().Logitude;
                            var rad = reportItem.Zone.Radius;

                            if (sideQry.FirstOrDefault() == null)
                            {
                                continue;
                            }

                            foreach (var item in sideQry.OrderBy(x => x.LocalRecvTime))
                            {
                                if (CommonHelper.IsPointInCircle(Convert.ToDouble(lat), Convert.ToDouble(lng), Convert.ToDouble(rad),
                                           item.Latitude, item.Longitude))
                                {

                                }
                            }

                            foreach (var item in sideQry.OrderBy(x => x.LocalRecvTime))
                            {
                                //Console.WriteLine($"MMSI: {item.MMSI} || Time: {item.LocalRecvTime} || Latitude: {item.Latitude} || Longitude: {item.Longitude}");
                                var qryArrived = results.Where(y => y.MMSI ==
                                                item.MMSI && y.ArrivalTime <=
                                               item.LocalRecvTime)
                                               .OrderByDescending(x => x.ArrivalTime);
                                var isArrived = qryArrived.FirstOrDefault();

                                var qryDeparted = results.Where(y => y.MMSI ==
                                                 item.MMSI && y.DepartureTime <=
                                                item.LocalRecvTime)
                                                .OrderByDescending(x => x.DepartureTime);

                                var isDeparted = qryDeparted.FirstOrDefault();

                                if (CommonHelper.IsPointInCircle(Convert.ToDouble(lat), Convert.ToDouble(lng), Convert.ToDouble(rad),
                                            item.Latitude, item.Longitude))
                                {

                                    if (isArrived == null && isDeparted == null)
                                    {
                                        var prevDate = m.Date.AddDays(-1);
                                        if (prevDate.Date >= m.Sod.Date)
                                        {
                                            using (var prevDbContext = new VesselDatabaseContext(m.Date.AddDays(-1)))
                                            {
                                                var lastPosition = prevDbContext.ShipPositions.Where(x => x.MMSI == item.MMSI)
                                                    .OrderByDescending(o => o.LocalRecvTime).Take(1).FirstOrDefault();

                                                if (lastPosition == null)
                                                {
                                                    results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                       item.Name, item.MMSI, item.CallSign,
                                                       TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                       TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                       item.Destination, item.ETA));
                                                }
                                                else
                                                {
                                                    if (!CommonHelper.IsPointInCircle(Convert.ToDouble(lat), Convert.ToDouble(lng), Convert.ToDouble(rad),
                                                         lastPosition.Latitude, lastPosition.Longitude))
                                                    {
                                                        results.Add(new ArrivalDepartureResponse(lastPosition.LocalRecvTime, null,
                                                            item.Name, item.MMSI, item.CallSign,
                                                            TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                            TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                            item.Destination, item.ETA));
                                                    }
                                                }


                                            }
                                        }
                                        else
                                        {
                                            results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                       item.Name, item.MMSI, item.CallSign,
                                                       TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                       TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                       item.Destination, item.ETA));
                                        }
                                    }
                                    else if (isArrived != null && isDeparted != null)
                                    {
                                        if (isDeparted.DepartureTime < item.LocalRecvTime)
                                        {
                                            //Console.WriteLine(2);
                                            results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                         item.Name, item.MMSI, item.CallSign,
                                                         TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                         TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                         item.Destination, item.ETA));
                                        }
                                    }
                                }
                                else
                                {
                                    if (isArrived != null && isDeparted == null)
                                    {
                                        if (results.Where(x => x.MMSI == item.MMSI).Any())
                                        {
                                            //Console.WriteLine(3);
                                            var data = results.LastOrDefault(x => x.MMSI == item.MMSI);
                                            data.DepartureTime = item.LocalRecvTime;
                                            //results.Add(new ArrivalDepartureResponse(data.ArrivalTime, item.sp.LocalRecvTime,
                                            //           data.Name, data.MMSI, data.CallSign,
                                            //           data.CallType,
                                            //          data.Length,
                                            //           data.Destination, data.ETA));
                                        }
                                    }
                                    else if (isArrived != null && isDeparted != null)
                                    {
                                        if (results.Where(x => x.MMSI == item.MMSI).Any())
                                        {
                                            if (qryArrived.Count() > qryDeparted.Count())
                                            {
                                                //Console.WriteLine(4);
                                                var data = results.LastOrDefault(x => x.MMSI == item.MMSI);
                                                data.DepartureTime = item.LocalRecvTime;
                                                //results.Add(new ArrivalDepartureResponse(data.ArrivalTime, item.sp.LocalRecvTime,
                                                //           data.Name, data.MMSI, data.CallSign,
                                                //           data.CallType,
                                                //          data.Length,
                                                //           data.Destination, data.ETA));
                                            }
                                        }
                                    }
                                }

                            }
                        }

                        if (reportItem.Zone.ZoneType == ZoneType.Polygon)
                        {
                            var polygon = new List<PointF>();
                            foreach (var zoneItem in reportItem.Zone.ZoneItems)
                            {
                                polygon.Add(new PointF
                                {
                                    X = (float)Convert.ToDouble(zoneItem.Latitude),
                                    Y = (float)Convert.ToDouble(zoneItem.Logitude)
                                });
                            }

                            sideQry = sideQry.Where(expression);
                            if (sideQry.FirstOrDefault() == null)
                            {
                                continue;
                            }
                            foreach (var item in sideQry.OrderBy(x => x.LocalRecvTime))
                            {
                                //Console.WriteLine($"MMSI: {item.MMSI} || Time: {item.LocalRecvTime} || Latitude: {item.Latitude} || Longitude: {item.Longitude}");
                                var qryArrived = results.Where(y => y.MMSI ==
                                                item.MMSI && y.ArrivalTime <=
                                               item.LocalRecvTime)
                                               .OrderByDescending(x => x.ArrivalTime);
                                var isArrived = qryArrived.FirstOrDefault();

                                var qryDeparted = results.Where(y => y.MMSI ==
                                                 item.MMSI && y.DepartureTime <=
                                                item.LocalRecvTime)
                                                .OrderByDescending(x => x.DepartureTime);

                                var isDeparted = qryDeparted.FirstOrDefault();

                                if (CommonHelper.IsPointInPolygon(polygon.ToArray(),
                                    new PointF { X = (float)item.Latitude, Y = (float)item.Longitude }))
                                {

                                    if (isArrived == null && isDeparted == null)
                                    {
                                        var prevDate = m.Date.AddDays(-1);
                                        if (prevDate.Date >= m.Sod.Date)
                                        {
                                            using (var prevDbContext = new VesselDatabaseContext(m.Date.AddDays(-1)))
                                            {
                                                var lastPosition = prevDbContext.ShipPositions.Where(x => x.MMSI == item.MMSI)
                                                    .OrderByDescending(o => o.LocalRecvTime).Take(1).FirstOrDefault();

                                                if (lastPosition == null)
                                                {
                                                    results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                         item.Name, item.MMSI, item.CallSign,
                                                         TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                         TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                         item.Destination, item.ETA));
                                                }
                                                else
                                                {
                                                    if (CommonHelper.IsPointInPolygon(polygon.ToArray(),
                                              new PointF { X = (float)lastPosition.Longitude, Y = (float)lastPosition.Latitude }))
                                                    {
                                                        results.Add(new ArrivalDepartureResponse(lastPosition.LocalRecvTime, null,
                                                             item.Name, item.MMSI, item.CallSign,
                                                             TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                             TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                             item.Destination, item.ETA));
                                                    }
                                                }


                                            }
                                        }
                                        else
                                        {
                                            results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                          item.Name, item.MMSI, item.CallSign,
                                                          TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                          TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                          item.Destination, item.ETA));
                                        }
                                    }
                                    else if (isArrived != null && isDeparted != null)
                                    {
                                        if (isDeparted.DepartureTime < item.LocalRecvTime)
                                        {
                                            //Console.WriteLine(2);
                                            results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                          item.Name, item.MMSI, item.CallSign,
                                                          TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                          TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                          item.Destination, item.ETA));
                                        }
                                    }
                                }
                                else
                                {
                                    if (isArrived != null && isDeparted == null)
                                    {
                                        if (results.Where(x => x.MMSI == item.MMSI).Any())
                                        {
                                            //Console.WriteLine(3);
                                            var data = results.LastOrDefault(x => x.MMSI == item.MMSI);
                                            data.DepartureTime = item.LocalRecvTime;
                                            //results.Add(new ArrivalDepartureResponse(data.ArrivalTime, item.sp.LocalRecvTime,
                                            //           data.Name, data.MMSI, data.CallSign,
                                            //           data.CallType,
                                            //          data.Length,
                                            //           data.Destination, data.ETA));
                                        }
                                    }
                                    else if (isArrived != null && isDeparted != null)
                                    {
                                        if (results.Where(x => x.MMSI == item.MMSI).Any())
                                        {
                                            if (qryArrived.Count() > qryDeparted.Count())
                                            {
                                                //Console.WriteLine(4);
                                                var data = results.LastOrDefault(x => x.MMSI == item.MMSI);
                                                data.DepartureTime = item.LocalRecvTime;
                                                //results.Add(new ArrivalDepartureResponse(data.ArrivalTime, item.sp.LocalRecvTime,
                                                //           data.Name, data.MMSI, data.CallSign,
                                                //           data.CallType,
                                                //          data.Length,
                                                //           data.Destination, data.ETA));
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        // IN to the right
                        // OUT to the left
                        if (reportItem.Zone.ZoneType == ZoneType.Line_L)
                        {
                            var line = new List<PointF>();
                            foreach (var zoneItem in reportItem.Zone.ZoneItems)
                            {
                                line.Add(new PointF
                                {
                                    X = (float)Convert.ToDouble(zoneItem.Latitude),
                                    Y = (float)Convert.ToDouble(zoneItem.Logitude)
                                });
                            }

                            sideQry = qry.Where(expression);
                            if (sideQry.FirstOrDefault() == null)
                            {
                                continue;
                            }
                            double latCounter = 0;
                            double longCounter = 0;
                            foreach (var item in sideQry.OrderBy(x => x.MMSI))
                            {
                                //Console.WriteLine($"MMSI: {item.MMSI} || Time: {item.LocalRecvTime} || Latitude: {item.Latitude} || Longitude: {item.Longitude}");
                                var qryArrived = results.Where(y => y.MMSI ==
                                                item.MMSI && y.ArrivalTime <=
                                               item.LocalRecvTime)
                                               .OrderByDescending(x => x.ArrivalTime);
                                var isArrived = qryArrived.FirstOrDefault();

                                var qryDeparted = results.Where(y => y.MMSI ==
                                                 item.MMSI && y.DepartureTime <=
                                                item.LocalRecvTime)
                                                .OrderByDescending(x => x.DepartureTime);

                                var isDeparted = qryDeparted.FirstOrDefault();
                                if (latCounter == 0 && longCounter == 0)
                                {
                                    latCounter = item.Latitude;
                                    longCounter = item.Longitude;
                                    continue;
                                }

                                if (CommonHelper.DoIntersect(line[0],
                                    line[1], new PointF { X = (float)latCounter, Y = (float)longCounter },
                                    new PointF { X = (float)item.Latitude, Y = (float)item.Longitude }
                                    ))
                                {
                                    if (item.Longitude > longCounter)
                                    {
                                        if (isArrived == null && isDeparted == null)
                                        {
                                            var prevDate = m.Date.AddDays(-1);
                                            if (prevDate.Date >= m.Sod.Date)
                                            {
                                                using (var prevDbContext = new VesselDatabaseContext(m.Date.AddDays(-1)))
                                                {
                                                    var lastPosition = prevDbContext.ShipPositions.Where(x => x.MMSI == item.MMSI)
                                                        .OrderByDescending(o => o.LocalRecvTime).Take(1).FirstOrDefault();
                                                    if (lastPosition == null)
                                                    {
                                                        results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                        item.Name, item.MMSI, item.CallSign,
                                                        TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                        TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                        item.Destination, item.ETA));
                                                    }
                                                    else
                                                    {
                                                        if (CommonHelper.DoIntersect(line[0],
                                                        line[1], new PointF { X = (float)latCounter, Y = (float)longCounter },
                                                        new PointF { X = (float)lastPosition.Latitude, Y = (float)lastPosition.Longitude }
                                                        ))
                                                        {
                                                            results.Add(new ArrivalDepartureResponse(lastPosition.LocalRecvTime, null,
                                                              item.Name, item.MMSI, item.CallSign,
                                                              TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                              TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                              item.Destination, item.ETA));
                                                        }
                                                    }

                                                }
                                            }
                                            else
                                            {
                                                results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                          item.Name, item.MMSI, item.CallSign,
                                                          TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                          TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                          item.Destination, item.ETA));
                                            }
                                        }
                                        else if (isArrived != null && isDeparted != null)
                                        {
                                            if (isDeparted.DepartureTime < item.LocalRecvTime)
                                            {
                                                //Console.WriteLine(2);
                                                results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                           item.Name, item.MMSI, item.CallSign,
                                                           TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                           TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                           item.Destination, item.ETA));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (isArrived != null && isDeparted == null)
                                        {
                                            if (results.Where(x => x.MMSI == item.MMSI).Any())
                                            {
                                                //Console.WriteLine(3);
                                                var data = results.LastOrDefault(x => x.MMSI == item.MMSI);
                                                data.DepartureTime = item.LocalRecvTime;
                                                //results.Add(new ArrivalDepartureResponse(data.ArrivalTime, item.sp.LocalRecvTime,
                                                //           data.Name, data.MMSI, data.CallSign,
                                                //           data.CallType,
                                                //          data.Length,
                                                //           data.Destination, data.ETA));
                                            }
                                        }
                                        else if (isArrived != null && isDeparted != null)
                                        {
                                            if (results.Where(x => x.MMSI == item.MMSI).Any())
                                            {
                                                if (qryArrived.Count() > qryDeparted.Count())
                                                {
                                                    //Console.WriteLine(4);
                                                    var data = results.LastOrDefault(x => x.MMSI == item.MMSI);
                                                    data.DepartureTime = item.LocalRecvTime;
                                                    //results.Add(new ArrivalDepartureResponse(data.ArrivalTime, item.sp.LocalRecvTime,
                                                    //           data.Name, data.MMSI, data.CallSign,
                                                    //           data.CallType,
                                                    //          data.Length,
                                                    //           data.Destination, data.ETA));
                                                }
                                            }
                                        }
                                    }
                                    latCounter = item.Latitude;
                                    longCounter = item.Longitude;
                                }
                            }
                        }

                        // IN to the left
                        // OUT to the right
                        if (reportItem.Zone.ZoneType == ZoneType.Line_R)
                        {
                            var line = new List<PointF>();
                            foreach (var zoneItem in reportItem.Zone.ZoneItems)
                            {
                                line.Add(new PointF
                                {
                                    X = (float)Convert.ToDouble(zoneItem.Latitude),
                                    Y = (float)Convert.ToDouble(zoneItem.Logitude)
                                });
                            }

                            sideQry = qry.Where(expression);
                            if (sideQry.FirstOrDefault() == null)
                            {
                                continue;
                            }
                            double latCounter = 0;
                            double longCounter = 0;
                            foreach (var item in sideQry.OrderBy(x => x.MMSI))
                            {
                                //Console.WriteLine($"MMSI: {item.MMSI} || Time: {item.LocalRecvTime} || Latitude: {item.Latitude} || Longitude: {item.Longitude}");
                                var qryArrived = results.Where(y => y.MMSI ==
                                                item.MMSI && y.ArrivalTime <=
                                               item.LocalRecvTime)
                                               .OrderByDescending(x => x.ArrivalTime);
                                var isArrived = qryArrived.FirstOrDefault();

                                var qryDeparted = results.Where(y => y.MMSI ==
                                                 item.MMSI && y.DepartureTime <=
                                                item.LocalRecvTime)
                                                .OrderByDescending(x => x.DepartureTime);

                                var isDeparted = qryDeparted.FirstOrDefault();
                                if (latCounter == 0 && longCounter == 0)
                                {
                                    latCounter = item.Latitude;
                                    longCounter = item.Longitude;
                                    continue;
                                }

                                if (CommonHelper.DoIntersect(line[0],
                                    line[1], new PointF { X = (float)latCounter, Y = (float)longCounter },
                                    new PointF { X = (float)item.Latitude, Y = (float)item.Longitude }
                                    ))
                                {
                                    if (item.Longitude < longCounter)
                                    {
                                        if (isArrived == null && isDeparted == null)
                                        {
                                            var prevDate = m.Date.AddDays(-1);
                                            if (prevDate.Date >= m.Sod.Date)
                                            {
                                                using (var prevDbContext = new VesselDatabaseContext(m.Date.AddDays(-1)))
                                                {
                                                    var lastPosition = prevDbContext.ShipPositions.Where(x => x.MMSI == item.MMSI)
                                                        .OrderByDescending(o => o.LocalRecvTime).Take(1).FirstOrDefault();
                                                    if (lastPosition == null)
                                                    {
                                                        results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                       item.Name, item.MMSI, item.CallSign,
                                                       TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                       TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                       item.Destination, item.ETA));
                                                    }
                                                    else
                                                    {
                                                        if (CommonHelper.DoIntersect(line[0],
                                                        line[1], new PointF { X = (float)latCounter, Y = (float)longCounter },
                                                        new PointF { X = (float)lastPosition.Latitude, Y = (float)lastPosition.Longitude }
                                                        ))
                                                        {
                                                            results.Add(new ArrivalDepartureResponse(lastPosition.LocalRecvTime, null,
                                                               item.Name, item.MMSI, item.CallSign,
                                                               TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                               TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                               item.Destination, item.ETA));
                                                        }
                                                    }

                                                }
                                            }
                                            else
                                            {
                                                results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                         item.Name, item.MMSI, item.CallSign,
                                                         TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                         TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                         item.Destination, item.ETA));
                                            }
                                        }
                                        else if (isArrived != null && isDeparted != null)
                                        {
                                            if (isDeparted.DepartureTime < item.LocalRecvTime)
                                            {
                                                results.Add(new ArrivalDepartureResponse(item.LocalRecvTime, null,
                                                            item.Name, item.MMSI, item.CallSign,
                                                            TargetHelper.GetShipTypeByShipTypeId(item.ShipType),
                                                            TargetHelper.GetTargetLength(item.Dimension_A, item.Dimension_B),
                                                            item.Destination, item.ETA));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (isArrived != null && isDeparted == null)
                                        {
                                            if (results.Where(x => x.MMSI == item.MMSI).Any())
                                            {
                                                //Console.WriteLine(3);
                                                var data = results.LastOrDefault(x => x.MMSI == item.MMSI);
                                                data.DepartureTime = item.LocalRecvTime;
                                                //results.Add(new ArrivalDepartureResponse(data.ArrivalTime, item.sp.LocalRecvTime,
                                                //           data.Name, data.MMSI, data.CallSign,
                                                //           data.CallType,
                                                //          data.Length,
                                                //           data.Destination, data.ETA));
                                            }
                                        }
                                        else if (isArrived != null && isDeparted != null)
                                        {
                                            if (results.Where(x => x.MMSI == item.MMSI).Any())
                                            {
                                                if (qryArrived.Count() > qryDeparted.Count())
                                                {
                                                    //Console.WriteLine(4);
                                                    var data = results.LastOrDefault(x => x.MMSI == item.MMSI);
                                                    data.DepartureTime = item.LocalRecvTime;
                                                    //results.Add(new ArrivalDepartureResponse(data.ArrivalTime, item.sp.LocalRecvTime,
                                                    //           data.Name, data.MMSI, data.CallSign,
                                                    //           data.CallType,
                                                    //          data.Length,
                                                    //           data.Destination, data.ETA));
                                                }
                                            }
                                        }
                                    }
                                    latCounter = item.Latitude;
                                    longCounter = item.Longitude;
                                }

                            }
                        }
                    }
                    //Console.WriteLine($"Ending reporting : {m.Date}");
                    Console.WriteLine($"End query {m.Date}");
                    Sender.Tell(new QueryAggregation(m.Date, results));
                }
            }
            catch (Exception err)
            {
                _logger.Error(err, err.Message);
                throw err;
            }

            _logger.Info($"Done Quering Arrival Departure Report: {m.Date} || {m.Report.Id} || {m.Report.Name}");
        }

        private Expression<Func<QueryAllShipPosStatic, bool>> Filter(FilterResponseDto filter, DateTime requestDate)
        {
            Expression<Func<QueryAllShipPosStatic, bool>> expression = x => x.LocalRecvTime.Value.Date == requestDate;
            if (filter.IsDeleted == false)
            {
                foreach (var filterItem in filter.FilterItems)
                {
                    foreach (var filterDetail in filterItem.FilterDetails)
                    {
                        Expression<Func<QueryAllShipPosStatic, bool>> expression2 = x => x.LocalRecvTime.Value.Date == requestDate;

                        if (string.Equals(filterDetail.ColumnName,
                            FilterFieldsConst.MMSI, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => x.MMSI != Convert.ToInt32(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.CONTAINS)
                            {
                                expression2 = x => x.MMSI.ToString().Contains(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.START_WITH)
                            {
                                expression2 = x => x.MMSI.ToString().StartsWith(filterDetail.ColumnValue,
                                        StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => x.MMSI == Convert.ToInt32(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                            FilterFieldsConst.IMO, StringComparison.CurrentCultureIgnoreCase))
                        {

                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => x.IMO != Convert.ToInt32(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.CONTAINS)
                            {
                                expression2 = x => x.IMO.HasValue &&
                                    x.IMO.ToString().Contains(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.START_WITH)
                            {
                                expression2 = x => x.IMO.HasValue
                                  && x.IMO.ToString().StartsWith(filterDetail.ColumnValue, StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => x.IMO.HasValue && x.IMO == Convert.ToInt32(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.CallSign, StringComparison.CurrentCultureIgnoreCase))
                        {

                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.CallSign) && !string.Equals(x.CallSign,
                                    filterDetail.ColumnValue, StringComparison.CurrentCultureIgnoreCase);
                            }
                            else if (filterDetail.Operation == Operation.CONTAINS)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.CallSign)
                                    && x.CallSign.Contains(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.START_WITH)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.CallSign)
                                  && x.CallSign.ToString().StartsWith(filterDetail.ColumnValue,
                                  StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.CallSign) && string.Equals(x.CallSign,
                                    filterDetail.ColumnValue, StringComparison.CurrentCultureIgnoreCase);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.ShipName, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.Name) && !string.Equals(x.Name,
                                    filterDetail.ColumnValue, StringComparison.CurrentCultureIgnoreCase);
                            }
                            else if (filterDetail.Operation == Operation.CONTAINS)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.Name)
                                    && x.Name.Contains(filterDetail.ColumnValue);
                            }
                            else if (filterDetail.Operation == Operation.START_WITH)
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.Name)
                                  && x.Name.ToString().StartsWith(filterDetail.ColumnValue,
                                    StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => !string.IsNullOrEmpty(x.Name)
                                && string.Equals(x.Name, filterDetail.ColumnValue,
                                    StringComparison.CurrentCultureIgnoreCase);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.ShipType, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var shipType = TargetHelper.GetShipTypeByName(filterDetail.ColumnValue);
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => !string.Equals(x.ShipType.HasValue ? TargetHelper.GetShipTypeByShipTypeId(x.ShipType)
                                   : null, shipType, StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => string.Equals(x.ShipType.HasValue ? TargetHelper.GetShipTypeByShipTypeId(x.ShipType)
                                    : null, shipType, StringComparison.CurrentCultureIgnoreCase);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.GroupType, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var result = new FilterGroupRequest().GetFilterGroupById(Convert.ToInt32(filterDetail.ColumnName)).Result;

                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => !result.MMSIList.Any(i => i == x.MMSI.ToString()) ||
                                   result.CallSignList.Any(c => string.Equals(c, x.CallSign)) ||
                                   result.NameList.Any(n => string.Equals(n, x.Name));
                            }
                            else
                            {
                                expression2 = x => result.MMSIList.Any(i => i == x.MMSI.ToString()) ||
                                   result.CallSignList.Any(c => string.Equals(c, x.CallSign)) ||
                                   result.NameList.Any(n => string.Equals(n, x.Name));
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.NavStatus, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var navStatus = TargetHelper.GetNavStatusByName(filterDetail.ColumnValue);
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => !string.Equals(x.NavigationalStatus.HasValue ? TargetHelper.GetNavStatusDescriptionByNavStatusId(x.NavigationalStatus)
                                   : null, navStatus, StringComparison.CurrentCultureIgnoreCase);
                            }
                            else
                            {
                                expression2 = x => string.Equals(x.NavigationalStatus.HasValue ? TargetHelper.GetNavStatusDescriptionByNavStatusId(x.NavigationalStatus)
                                    : null, navStatus, StringComparison.CurrentCultureIgnoreCase);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.AisType, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var messageId = TargetHelper.GetMessageIdByTargetClass(filterDetail.ColumnValue);

                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                expression2 = x => messageId.Any(p => p != x.MessageId);
                            }
                            else
                            {
                                expression2 = x => messageId.Any(p => p == x.MessageId);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.DataSource, StringComparison.CurrentCultureIgnoreCase))
                        {
                            //TODO
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MinSpeed, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                //expression2 = x => x.sp.SOG >= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                            else
                            {
                                expression2 = x => x.SOG >= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MaxSpeed, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {
                                //expression2 = x => x.sp.SOG >= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                            else
                            {
                                expression2 = x => x.SOG <= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MinCourse, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                expression2 = x => x.COG >= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MaxCourse, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                expression2 = x => x.COG <= Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MinLength, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                expression2 = x => x.Dimension_A.HasValue && x.Dimension_B.HasValue
                                    && Convert.ToInt32(filterDetail.ColumnValue) >=
                                    (x.Dimension_A + x.Dimension_B);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                           FilterFieldsConst.MaxLength, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                expression2 = x => x.Dimension_A.HasValue && x.Dimension_B.HasValue
                                    && Convert.ToInt32(filterDetail.ColumnValue) <=
                                    (x.Dimension_A + x.Dimension_B);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                          FilterFieldsConst.MinAge, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                // In minutes
                                expression2 = x => CommonHelper.GetAgingViaLocalRecvTime(x.LocalRecvTime.Value).TotalMinutes >=
                                    Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        if (string.Equals(filterDetail.ColumnName,
                          FilterFieldsConst.MaxAge, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (filterDetail.Operation == Operation.NOT_EQUALS_TO)
                            {

                            }
                            else
                            {
                                // In minutes
                                expression2 = x => CommonHelper.GetAgingViaLocalRecvTime(x.LocalRecvTime.Value).TotalMinutes <=
                                    Convert.ToDouble(filterDetail.ColumnValue);
                            }
                        }

                        Expression<Func<QueryAllShipPosStatic, bool>> expression3 = filterDetail.Condition == Condition.AND ?
                            expression.And(expression2) : expression.Or(expression2);

                        expression = expression3;
                    }
                }
            }
            return expression;
        }

        private void FilterSpeedViolationData(QueryAllShipPosStatic item, ReportItemResponseDetailDto reportItem, List<VesselWithMinSpeed> vesselWithLowSpeed,
            List<VesselWithHighSpeed> vesselWithHighSpeed, List<ResponseSpeedViolationMinSpeed> minSpeedResultList, List<ResponseSpeedViolationMaxSpeed> maxSpeedResultList)
        {
            (string latDegree, string lngDegree) = CommonHelper.RadianToDegree(item.Latitude, item.Longitude);
            if (item.SOG < (double)reportItem.MinSpeed)
            {
                if (vesselWithLowSpeed.Where(x => x.MMSI == item.MMSI).Any())
                {
                    var temp = minSpeedResultList.LastOrDefault(w => w.MMSI == item.MMSI);
                    var v = vesselWithLowSpeed.Where(x => x.MMSI == item.MMSI).FirstOrDefault();
                    v.DurationForMinSpeed = (item.LocalRecvTime - v.FirstVesselViolationTimeForMinSpeed).Value.TotalMinutes;

                    v.AvgSpeedForMinSpeed = v.AvgSpeedForMinSpeed + (item.SOG != null ? item.SOG.Value : 0);
                    v.AvgSpeedCounterForMinSpeed += 1;

                    temp.Latitude = item.Latitude.ToString();
                    temp.Lat_Degree = latDegree;
                    temp.Longitude = item.Longitude.ToString();
                    temp.Long_Degree = lngDegree;
                    temp.LocalRecvTime = item.LocalRecvTime.Value;
                    temp.AverageSpeed = (v.AvgSpeedForMinSpeed / v.AvgSpeedCounterForMinSpeed).ToString("0.0");
                    temp.Duration = v.DurationForMinSpeed.ToString("0.0");
                }
                else
                {
                    var v = new VesselWithMinSpeed
                    {
                        MMSI = item.MMSI,
                        AvgSpeedCounterForMinSpeed = 1,
                        AvgSpeedForMinSpeed = (item.SOG != null ? item.SOG.Value : 0),
                        DurationForMinSpeed = 0,
                        FirstVesselViolationTimeForMinSpeed = item.LocalRecvTime
                    };
                    vesselWithLowSpeed.Add(v);

                    //if (firstVesselViolationTimeForMinSpeed == null)
                    //{
                    //    firstVesselViolationTimeForMinSpeed = item.sp.LocalRecvTime;
                    //}
                    //avgSpeedForMinSpeed = (item.sp.SOG != null ? item.sp.SOG.Value : 0);
                    minSpeedResultList.Add(new ResponseSpeedViolationMinSpeed
                    {
                        IMO = item.IMO != null ? item.IMO.Value : 0,
                        Latitude = item.Latitude.ToString(),
                        Lat_Degree = latDegree,
                        Longitude = item.Longitude.ToString(),
                        Long_Degree = lngDegree,
                        LocalRecvTime = item.LocalRecvTime.Value,
                        MinSpeed = item.SOG.Value,
                        MMSI = item.MMSI,
                        Name = item.Name,
                        AverageSpeed = v.AvgSpeedForMinSpeed.ToString("0.0"),
                        Duration = v.DurationForMinSpeed.ToString("0.0")
                    });
                }
                //avgSpeedCounterForMinSpeed += 1;
            }
            else
            {
                //Console.WriteLine(3);
                if (vesselWithLowSpeed.Where(w => w.MMSI == item.MMSI).Any())
                {
                    vesselWithLowSpeed.Remove(vesselWithLowSpeed.Where(w => w.MMSI == item.MMSI).FirstOrDefault());
                }

            }

            if (item.SOG > (double)reportItem.MaxSpeed)
            {
                if (vesselWithHighSpeed.Where(x => x.MMSI == item.MMSI).Any())
                {
                    var temp = maxSpeedResultList.LastOrDefault(w => w.MMSI == item.MMSI);
                    var v = vesselWithHighSpeed.Where(x => x.MMSI == item.MMSI).FirstOrDefault();
                    v.DurationForMaxSpeed = (item.LocalRecvTime - v.FirstVesselViolationTimeForMaxSpeed).Value.TotalMinutes;
                    v.AvgSpeedForMaxSpeed = v.AvgSpeedForMaxSpeed + (item.SOG != null ? item.SOG.Value : 0);
                    v.AvgSpeedCounterForMaxSpeed += 1;
                    temp.Latitude = item.Latitude.ToString();
                    temp.Lat_Degree = latDegree;
                    temp.Longitude = item.Longitude.ToString();
                    temp.Long_Degree = lngDegree;
                    temp.LocalRecvTime = item.LocalRecvTime.Value;
                    temp.AverageSpeed = (v.AvgSpeedForMaxSpeed / v.AvgSpeedCounterForMaxSpeed).ToString("0.0");
                    temp.Duration = v.DurationForMaxSpeed.ToString("0.0");
                }
                else
                {
                    var v = new VesselWithHighSpeed
                    {
                        MMSI = item.MMSI,
                        AvgSpeedCounterForMaxSpeed = 1,
                        AvgSpeedForMaxSpeed = (item.SOG != null ? item.SOG.Value : 0),
                        DurationForMaxSpeed = 0,
                        FirstVesselViolationTimeForMaxSpeed = item.LocalRecvTime
                    };
                    vesselWithHighSpeed.Add(v);

                    maxSpeedResultList.Add(new ResponseSpeedViolationMaxSpeed
                    {
                        IMO = item.IMO != null ? item.IMO.Value : 0,
                        Latitude = item.Latitude.ToString(),
                        Lat_Degree = latDegree,
                        Longitude = item.Longitude.ToString(),
                        Long_Degree = lngDegree,
                        LocalRecvTime = item.LocalRecvTime.Value,
                        MaxSpeed = item.SOG.Value,
                        MMSI = item.MMSI,
                        Name = item.Name,
                        AverageSpeed = v.AvgSpeedForMaxSpeed.ToString("0.0"),
                        Duration = v.DurationForMaxSpeed.ToString("0.0")
                    });
                }
                //avgSpeedCounterForMaxSpeed += 1;
            }
            else
            {
                if (vesselWithHighSpeed.Where(w => w.MMSI == item.MMSI).Any())
                {
                    vesselWithHighSpeed.Remove(vesselWithHighSpeed.Where(w => w.MMSI == item.MMSI).FirstOrDefault());
                }
            }
        }


    }
}
