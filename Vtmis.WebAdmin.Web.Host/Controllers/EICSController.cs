﻿using Abp.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.WebAdmin.EICS;
using Vtmis.WebAdmin.EICS.DTO;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
  //  [AbpAuthorize]
    [Route("/api/[controller]")]
    public class EICSController : AbpController
    {
        private readonly IEICSService _createEICSService;

        public EICSController(IEICSService createEICSService)
        {
            _createEICSService = createEICSService;
        }

        [HttpPost]
        public async Task<IActionResult> Create(RequestCreateEICSInfoDto input)
        {
            try
            {
                var result = await _createEICSService.CreateAsync(input);
                return Ok(result);
            }
            catch (Exception error)
            {

                throw error;
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync(int pageIndex, int pageSize)
        {
            try
            {
                var result = await _createEICSService.GetAllAsync(pageIndex, pageSize);
                return Ok(result);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpGet]
        [Route("{mmsi}")]
        public async Task<IActionResult> GetAsync (int mmsi)
        {
            var result = await _createEICSService.GetAsync(mmsi);
            return Ok(result);
        }

        [HttpPut]
        [Route("{mmsi}")]
        public async Task<IActionResult> UpdateAsync(int mmsi, RequestUpdateInfoEICS input)
        {
            var result = await _createEICSService.UpdateAsync(mmsi, input);
            return Ok(result);

        }

        [HttpDelete]
        [Route("{mmsi}")]
        public async Task<IActionResult> DeleteAsync(int mmsi)
        {
            var result = await _createEICSService.DeleteAsync(mmsi);
            return Ok(result);
        }
    }
}
