﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Alarms;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [AbpAuthorize]
    [Route("/api/alarm")]
    public class AlarmController : AbpController
    {
        private readonly IAlarmNotificationService _alarmNotification;

        public AlarmController(IAlarmNotificationService alarmNotification)
        {
            _alarmNotification = alarmNotification;
        }

        [HttpGet]
        [Route("notifications")]     
        public async Task<IActionResult> Get(int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var result = await _alarmNotification.GetAsync(pageIndex, pageSize);
                return Ok(result);
            }            
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("notifications/{eventDetailId}")]     
        public async Task<IActionResult> Get(int eventDetailId)
        {
            try
            {
                var result = await _alarmNotification.GetAsync(eventDetailId);
                return Ok(result);
            }            
            catch (NotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
