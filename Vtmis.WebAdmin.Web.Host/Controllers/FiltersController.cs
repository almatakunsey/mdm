﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Filters.v2.Dto;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [AbpAuthorize]
    [Route("/api/[controller]")]
    //[SwaggerResponse((int)HttpStatusCode.InternalServerError, typeof(string))]
    public class FiltersController : AbpController
    {
        private readonly IFilterAppService _service;
        public FiltersController(IFilterAppService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("{filterId}")]
        public async Task<IActionResult> Get(int filterId, bool includingFilterItems = true, bool includingFilterDetails = true)
        {
            try
            {
                var result = await _service.GetByIdAsync(filterId, includingFilterItems, includingFilterDetails);
                return Ok(result);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpGet]       
        public async Task<IActionResult> GetAsync(bool includingFilterItems = true, bool includingFilterDetails = true,
                int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var result = await _service.GetAllAsync(false, includingFilterItems, includingFilterDetails, pageIndex, pageSize);
                return Ok(result);
            }
            catch (ForbiddenException ex)
            {
                throw ex;
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [AbpMvcAuthorize(MdmPermissionsConst.CreateFilter)]
        public async Task<IActionResult> Post(CreateFilterDto input)
        {
            try
            {
                var result = await _service.CreateAsync(input);
                return Created($"/api/filters/{result.Id}", result);
            }
            catch (ForbiddenException ex)
            {
                return Forbid(ex.Message);
            }
            catch (AlreadyExistException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("{filterId}")]
        public async Task<IActionResult> Put(int filterId, UpdateFilterDto input)
        {
            if (filterId != input.Id)
            {
                return BadRequest("Filter Id does not match with the object id provided");
            }
            try
            {
                var result = await _service.UpdateAsync(input);
                return Ok(result);
            }
            catch (AlreadyExistException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        [Route("{filterId}")]
        public async Task<IActionResult> Delete(int filterId)
        {
            try
            {
                await _service.DeleteAsync(filterId);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }
    }
}
