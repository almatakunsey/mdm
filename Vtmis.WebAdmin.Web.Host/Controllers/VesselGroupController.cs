﻿using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Vessels;
using Vtmis.WebAdmin.Vessels.Dto;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    [AbpAuthorize]
    public class VesselGroupController : AbpController
    {
        private readonly IVesselGroupAppService _vesselGroupAppService;

        public VesselGroupController(IVesselGroupAppService vesselGroupAppService)
        {
            _vesselGroupAppService = vesselGroupAppService;
        }

//[AbpAllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateAsync(RequestCreateVesselGroup input)
        {
            var result = await _vesselGroupAppService.CreateAsync(input);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _vesselGroupAppService.GetAllAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var result = await _vesselGroupAppService.GetAsync(id);
            return Ok(result);
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> UpdateAsync(int id,RequestCreateVesselGroup input)
        {
            var result = await _vesselGroupAppService.UpdateAsync(id, input);
            return Ok(result);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
             await _vesselGroupAppService.DeleteAsync(id);
            return Ok();
        }

    }
}
