﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Abp.UI;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Personalizations;
using Vtmis.WebAdmin.Personalizations.Dto;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class PersonalizationController : AbpController
    {
        private readonly IPersonalizationService _personalizationService;

        public PersonalizationController(IPersonalizationService personalizationService)
        {
            _personalizationService = personalizationService;
        }

        [AbpAllowAnonymous]
        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> GetKey(string userLevel, string key, long? userId, int? tenantId, string tenantName)
        {
            try
            {
                var result = await _personalizationService.GetSpecificKeyAsync(new RequestSpecificPersonaKey
                { Key = key, TenantId = tenantId, TenantName = tenantName, UserId = userId, UserLevel = userLevel });
                return Ok(result);
            }
            catch (Exception err)
            {
                throw new UserFriendlyException("", err.Message);
            }
        }

        [HttpPost]
        [AbpMvcAuthorize]
        public async Task<IActionResult> Post(RequestCreatePersonalizationDto input)
        {
            try
            {
                var result = await _personalizationService.CreateOrUpdateAsync(input);
                return Created($"/api/personalization/{result.Id}", result);
            }
            catch (ForbiddenException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (AlreadyExistException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPost]
        [AbpMvcAuthorize]
        [Route("bulk")]
        public async Task<IActionResult> BulkInsert([FromBody]RequestCreateBulkPersonalization input)
        {
            try
            {
                Console.WriteLine("saving personalization");
                var result = await _personalizationService.CreateOrUpdateAsync(input);
                return Ok(result);
            }
            catch (ForbiddenException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (AlreadyExistException ex)
            {
                Console.WriteLine($"persona || exist || {ex.ToString()}");
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"persona || err || {ex.ToString()}");
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [AbpMvcAuthorize]
        public async Task<IActionResult> Get(int? tenantId, string userLevel)
        {
            try
            {
                var result = await _personalizationService.GetAsync(AbpSession.UserId.Value, tenantId, userLevel);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet]
        [Route("{key}")]
        [AbpMvcAuthorize]
        public async Task<IActionResult> Get(string key, int? tenantId, string userLevel)
        {
            try
            {
                var result = await _personalizationService.GetAsync(key, AbpSession.UserId.Value, tenantId, userLevel);
                return Ok(result);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpDelete]
        [Route("{key}")]
        [AbpMvcAuthorize]
        public async Task<IActionResult> Delete(string key, int? tenantId, string userLevel)
        {
            try
            {
                await _personalizationService.DeleteAsync(key, AbpSession.UserId.Value, tenantId, userLevel);
                return Ok();
            }
            catch (NotFoundException err)
            {
                return BadRequest(err);
            }
            catch (Exception err)
            {
                return BadRequest(err);
            }
        }

        [HttpDelete]
        [Route("bulk")]
        [AbpMvcAuthorize]
        public async Task<IActionResult> Delete([FromBody]RequestDeleteBulkPersonalization input)
        {
            try
            {
                await _personalizationService.DeleteAsync(input.Keys, AbpSession.UserId.Value, input.TenantId, input.UserLevel);
                return Ok();
            }
            catch (NotFoundException err)
            {
                return BadRequest(err);
            }
            catch (Exception err)
            {
                return BadRequest(err);
            }
        }
    }
}
