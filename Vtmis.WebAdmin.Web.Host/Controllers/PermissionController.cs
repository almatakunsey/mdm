﻿using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Permissions;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [AbpAuthorize]
    [Route("/api/[controller]")]
    public class PermissionController : AbpController
    {
        private readonly IPermissionService _permissionService;
        private readonly IMdmPermissionService _mdmPermissionService;

        public PermissionController(IPermissionService permissionService, IMdmPermissionService mdmPermissionService)
        {
            _permissionService = permissionService;
            _mdmPermissionService = mdmPermissionService;
        }

       
        [HttpGet]
        public async Task<IActionResult> Get(bool isGranted = true, bool fullname = false)
        {
            try
            {
                return Ok(await _mdmPermissionService.GetAllAsync(isGranted, fullname));
            }
            catch (Exception err)
            {
                throw err;
            }

        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]PermissionViewModel vm)
        {
            try
            {
                return Ok(await _mdmPermissionService.IsGrantedAsync(vm.PermissionName, vm.Fullname));
            }
            catch (Exception err)
            {
                return BadRequest(err.Message);
            }

        }
    }

    public class PermissionViewModel
    {
        public string PermissionName { get; set; }
        public bool Fullname { get; set; } = false;
    }
}
