﻿using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Reports;
using Vtmis.WebAdmin.Reports.Dto;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [AbpAuthorize]
    [Route("/api/[controller]")]
    public class ReportsController : AbpController
    {
        private readonly IReportService _service;
        public ReportsController(IReportService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> Get(int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                return Ok(await _service.GetAllAsync(pageIndex, pageSize));
            }
            catch (ForbiddenException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException err)
            {
                throw err;
            }
        }

        [HttpGet]
        [Route("{reportId}")]
        public async Task<IActionResult> Get(int reportId, bool isDetail = false)
        {
            try
            {
                return Ok(await _service.GetByIdAsync(reportId, isDetail));
            }
            catch (ForbiddenException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException err)
            {
                return BadRequest(err.Message);
            }
            catch (ServerFaultException err)
            {
                throw err;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateReportDto input)
        {
            try
            {
               
                var result = await _service.CreateWithCurrentUser(input);
                return Created($"/api/reports/{result.Id}", result);
            }
            catch (RequestFaultException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ForbiddenException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (AlreadyExistException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("{reportId}")]
        public async Task<IActionResult> Put(int reportId, UpdateReportDto input)
        {
            try
            {
                return Ok(await _service.UpdateAsync(input));
            }
            catch (ForbiddenException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (AlreadyExistException err)
            {
                return BadRequest(err.Message);
            }
            catch (NotFoundException err)
            {
                return BadRequest(err.Message);
            }
            catch (ServerFaultException err)
            {
                throw err;
            }
        }

        [HttpDelete]
        [Route("{reportId}")]
        public async Task<IActionResult> Delete(int reportId)
        {
            try
            {
                await _service.DeleteAsync(reportId);
                return Ok();
            }
            catch (ForbiddenException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException err)
            {
                return BadRequest(err.Message);
            }
            catch (ServerFaultException err)
            {
                throw err;
            }
        }

    }
}
