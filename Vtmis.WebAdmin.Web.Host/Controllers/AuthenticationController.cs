﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Vtmis.WebAdmin.Authorization;
using Vtmis.WebAdmin.Models.TokenAuth;
using Abp.Authorization;
using System.Security.Claims;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        public AuthenticationController(LogInManager logInManager)
        {
            LogInManager = logInManager;
        }

        public LogInManager LogInManager { get; }

        // POST api/values
        //[HttpPost]

        //public async Task<AuthenticateResultModel> Post([FromBody]string body)
        //{
        //    var result = JsonConvert.DeserializeObject<AuthenticationUserModel>(body);

        //    //Later change to Log debug
        //    Console.WriteLine("Username "+result.UserName);

        //    var loginResult = await LogInManager.LoginAsync(result.UserName, result.Password, "1");

        //    switch (loginResult.Result)
        //    {
        //        case AbpLoginResultType.Success:
        //            return new AuthenticateResultModel { UserName = "", DisplayName = "", Claims = loginResult.Identity.Claims.ToList() };
        //        default:
        //            throw new Exception("Authentication execption");
        //    }
        //}

    }

    
}
