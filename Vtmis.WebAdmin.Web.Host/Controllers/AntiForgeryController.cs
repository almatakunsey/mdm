using Microsoft.AspNetCore.Antiforgery;
using Vtmis.WebAdmin.Controllers;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    public class AntiForgeryController : WebAdminControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
