using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp;
using Abp.Extensions;
using Abp.Notifications;
using Abp.Timing;
using Vtmis.WebAdmin.Controllers;
//using Vtmis.WebAdmin.ReportTypes;
//using Vtmis.WebAdmin.ReportTypes.Dto;
using System.Linq;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    public class HomeController : WebAdminControllerBase
    {
        private readonly INotificationPublisher _notificationPublisher;
        //private readonly IReportTypeService _reportTypeService;
        public HomeController(INotificationPublisher notificationPublisher)//, IReportTypeService reportTypeService)
        {
            _notificationPublisher = notificationPublisher;
           // _reportTypeService = reportTypeService;
        }

        public IActionResult Index()
        {
            //await SeedAsync();
            return Redirect("/swagger");
        }

        //public async Task SeedAsync()
        //{
        //    var result = await _reportTypeService.Get();
        //    if (!result.Any())
        //    {
        //        await _reportTypeService.Create(new ReportTypeDto
        //        {
        //            Name = "Speed Summary"
        //        });
        //        await _reportTypeService.Create(new ReportTypeDto
        //        {
        //            Name = "Speed Violation"
        //        });
        //        await _reportTypeService.Create(new ReportTypeDto
        //        {
        //            Name = "Arrival/Departure"
        //        });
        //    }
        //}

        
        /// <summary>
        /// This is a demo code to demonstrate sending notification to default tenant admin and host admin uers.
        /// Don't use this code in production !!!
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<ActionResult> TestNotification(string message = "")
        {
            if (message.IsNullOrEmpty())
            {
                message = "This is a test notification, created at " + Clock.Now;
            }

            var defaultTenantAdmin = new UserIdentifier(1, 2);
            var hostAdmin = new UserIdentifier(null, 1);

            await _notificationPublisher.PublishAsync(
                    "App.SimpleMessage",
                    new MessageNotificationData(message),
                    severity: NotificationSeverity.Info,
                    userIds: new[] { defaultTenantAdmin, hostAdmin }
                 );

            return Content("Sent notification: " + message);
        }
    }
}
