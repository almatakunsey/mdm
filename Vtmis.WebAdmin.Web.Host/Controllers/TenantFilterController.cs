﻿using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.TenantFilters;
using Vtmis.WebAdmin.TenantFilters.Dto;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [AbpAuthorize]
    [Route("/api/[controller]")]
    public class TenantFilterController : AbpController
    {
        private readonly ITenantFilterService _filterService;

        public TenantFilterController(ITenantFilterService filterService)
        {
            _filterService = filterService;
        }
        [HttpGet]
        public async Task<IActionResult> Get(int? pageIndex = 1, int? pageSize = 10)
        {
            try
            {
                return Ok(await _filterService.GetAllAsync(pageIndex.Value, pageSize.Value));
            }
            catch (ForbiddenException err)
            {
                return BadRequest(err.Message);
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                return Ok(await _filterService.GetByIdAsync(id));
            }
            catch (ForbiddenException err)
            {
                return BadRequest(err.Message);
            }
            catch (NotFoundException err)
            {
                return BadRequest(err.Message);
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestCreateTenantFilter input)
        {
            try
            {
                var result = await _filterService.CreateAsync(input);
                return Created($"/api/TenantFilter/{result.Id}", result);
            }
            catch (ForbiddenException ex)
            {
                return Forbid(ex.Message);
            }
            catch (AlreadyExistException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("assign")]
        public async Task<IActionResult> Assign(RequestAssignTenantFilter input)
        {
            try
            {
                var result = await _filterService.AssignFilterAsync(input);
                return Ok(result);
            }
            catch (ForbiddenException ex)
            {
                return Forbid(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }
    }
}
