﻿using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Abp.UI;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Model.DTO.Image;
using Vtmis.WebAdmin.Image;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
   // [AbpAuthorize]
    public class ImageController : AbpController
    {
        private readonly IImageService _imageService;

        public ImageController(IImageService imageService)
        {
            _imageService = imageService;
        }

      //  [AbpAllowAnonymous]
        [HttpDelete]
        [Route("Delete")]
        public async Task<IActionResult> DeleteImage(RequestDeleteDto input)
        {
           var result = await _imageService.DeleteImages(input);
           if (result == true)
            {
                return Ok();
            }
           else
            {
                throw new UserFriendlyException("Image Not Found");
            }
        }

        [HttpGet]
        public async Task<IEnumerable<ResponseImageDto>> Get(string imageId, ImageType imageType)
        {
            try
            {
                var baseUri = Request.Host.Value;
                var result = await _imageService.GetImages(new RequestImageDto { ImageId = imageId, ImageType = imageType }, baseUri);
                return result;
            }
            catch (UserFriendlyException err)
            {
                throw err;
            }
        }

        [AbpAllowAnonymous]
        [HttpGet]
        [Route("seed")]
        public async Task<IActionResult> Seed()
        {
            return Ok(await _imageService.Seed());
        }

        [Route("Upload/Form")]
        [HttpPost]
        public async Task<IEnumerable<ResponseImageDto>> FormUpload([FromForm]RequestUploadImage inputs)
        {
            try
            {
                var baseUri = Request.Host.Value;
                var result = await _imageService.UploadImageAsync(inputs, baseUri);
                return result;
            }
            catch (System.Exception err)
            {
                throw err;
            }
        }

        [Route("Upload")]
        [HttpPost]
        public async Task<IEnumerable<ResponseImageDto>> BaseUpload([FromBody]RequestUploadImageBase64 inputs)
        {
            try
            {
                var baseUri = Request.Host.Value;
                var result = await _imageService.UploadImageAsync(inputs, baseUri);
                return result;
            }
            catch (System.Exception err)
            {
                throw err;
            }
        }
    }
}
