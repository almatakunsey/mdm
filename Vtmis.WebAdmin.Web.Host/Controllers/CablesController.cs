﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Cable;
using Vtmis.WebAdmin.Cable.Dto;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [AbpAuthorize]
    [Route("/api/[controller]")]
    public class CablesController : AbpController
    {
        private readonly ICableService _cableService;

        public CablesController(ICableService cableService)
        {
            _cableService = cableService;
        }

        [HttpGet]       
        public async Task<IActionResult> Get(int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var result = await _cableService.GetAllAsync(pageIndex, pageSize);
                return Ok(result);
            }
            catch (ForbiddenException ex)
            {
                throw ex;
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("{cableId}")]    
        public async Task<IActionResult> Get(int cableId)
        {
            try
            {
                var result = await _cableService.GetAsync(cableId);
                return Ok(result);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpPost]     
        public async Task<IActionResult> Post(RequestCreateCableDto input)
        {
            try
            {
                var result = await _cableService.CreateAsync(input);
                return Created($"/api/cables/{result.Id}", result);
            }
            catch (ForbiddenException ex)
            {
                return Forbid(ex.Message);
            }
            catch (AlreadyExistException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("{cableId}")]     
        public async Task<IActionResult> Put(int cableId, RequestUpdateCableDto input)
        {
            if (cableId != input.Id)
            {
                return BadRequest("Cable Id does not match with the object id provided");
            }
            try
            {
                var result = await _cableService.UpdateAsync(input);
                return Ok(result);
            }
            catch (AlreadyExistException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        [Route("{cableId}")]     
        public async Task<IActionResult> Delete(int cableId)
        {
            try
            {
                await _cableService.DeleteAsync(cableId);
                return Ok();
            }
            catch (NotFoundException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }
    }
}
