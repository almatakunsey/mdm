﻿using Abp.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.WebAdmin.EICS_Equipment;
using Vtmis.WebAdmin.EICS_Equipment.DTO;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{

    [ApiController]
    //  [AbpAuthorize]
    [Route("/api/EICS/[controller]")]
    public class EquipmentController : AbpController
    {
        private readonly IEICS_EquipmentService _equipmentService;

        public EquipmentController(IEICS_EquipmentService _EquipmentService)
        {
            _equipmentService = _EquipmentService;
        }

        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            try
            {
                var result = await _equipmentService.Create(name);
                return Ok(result);
            }
            catch (Exception error)
            {

                throw error;
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _equipmentService.GetAllAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var result = await _equipmentService.Get(id);
            return Ok(result);
        }

        [HttpPut]
        [Route("{id}")]
        public async Task<IActionResult> Update(EICS_EquipmentDto input)
        {
            var result = await _equipmentService.Update(input);
            return Ok(result);
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
          await _equipmentService.DeleteAsync(id);
            return Ok();
        }
    }
}
