﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Abp.UI;
using Akka.Actor;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Constants.Akka;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.AkkaModel;
using Vtmis.Core.Model.AkkaModel.Messages.Route;
using Vtmis.Core.Model.ViewModels.Route.Dto;
using Vtmis.WebAdmin.Routes;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [AbpAuthorize]
    [Route("/api/[controller]")]
    public class RoutesController : AbpController
    {
        private readonly IRouteService _service;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly WebVessel.Tracking.Services.Interfaces.IRouteService _routeService;

        public RoutesController(IRouteService service, IHostingEnvironment hostingEnvironment, Vtmis.WebVessel.Tracking.Services.Interfaces.IRouteService routeService)
        {
            _service = service;
            _hostingEnvironment = hostingEnvironment;
            _routeService = routeService;
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestCreateRoute input)
        {
            try
            {
                var result = await _service.CreateAsync(input);
                return Created($"/api/routes/{result.Id}", result);
            }
            catch (ForbiddenException ex)
            {
                return Forbid(ex.Message);
            }
            catch (AlreadyExistException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        [Route("{routeId}")]
        public async Task<IActionResult> Get(int routeId)
        {
            try
            {
                var result = await _service.GetAsync(routeId);
                return Ok(result);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get(int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var result = await _service.GetAllAsync(pageIndex, pageSize);
                return Ok(result);
            }
            catch (ForbiddenException ex)
            {
                throw ex;
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        [Route("{routeId}")]
        public async Task<IActionResult> Put(int routeId, RequestUpdateRouteDto input)
        {
            if (routeId != input.Id)
            {
                return BadRequest("Route Id does not match with the object id provided");
            }
            try
            {
                var result = await _service.UpdateAsync(input);
                return Ok(result);
            }
            catch (AlreadyExistException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ServerFaultException ex)
            {
                throw ex;
            }
        }

        [HttpDelete]
        [Route("{routeId}")]
        public async Task<IActionResult> Delete(int routeId)
        {
            try
            {
                await _service.DeleteAsync(routeId);
                return Ok();
            }
            catch (NotFoundException err)
            {
                throw err;
            }
            catch (Exception err)
            {
                throw new ServerFaultException(err.Message);
            }
        }

        [HttpGet]
        [Route("{routeId}/eta")]
        public async Task<IActionResult> Get(int routeId, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var result = await _service.GetAsync(routeId);
                //var requestDate = _hostingEnvironment.IsEnvironment(HostingEnvironment.LOCAL) ? await ActorRefsConst.Global.Ask<DateTime>(new RequestTargetMonitorDateTime())
                //    : DateTime.Now;
                //var requestDate = await ActorRefsConst.Global.Ask<DateTime>(new RequestTargetMonitorDateTime());
                //Console.WriteLine($"Global Date: {requestDate}");
                //var queryRouteEta = await ActorRefsConst.Route.Ask(new RequestRouteEta(result, requestDate, pageIndex, pageSize));
                var queryRouteEta = await _routeService.RouteEtaAsync(new RequestRouteEta(result, DateTime.Now, pageIndex, pageSize));
                return Ok(new ResponseRouteEta(queryRouteEta));
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        [HttpGet]
        [Route("{routeId}/eta/{mmsi}")]
        public async Task<IActionResult> VesselEta(int routeId, int mmsi)
        {
            try
            {
                var route = await _service.GetAsync(routeId);
                var requestDate = _hostingEnvironment.IsEnvironment(HostingEnvironment.LOCAL) ? await ActorRefsConst.Global.Ask<DateTime>(new RequestTargetMonitorDateTime())
                    : DateTime.Now;
                //var result = await ActorRefsConst.Route.Ask(new RequestVesselEta(route, mmsi, requestDate));
                var result = await _routeService.VesselEtaAsync(new RequestVesselEta(route, mmsi, requestDate));
                return Ok(new ResponseVesselEtaMsg(result));
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
