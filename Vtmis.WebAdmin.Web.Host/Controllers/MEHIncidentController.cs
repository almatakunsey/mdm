﻿using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.WebAdmin.MEH;
using Vtmis.WebAdmin.MEH.DTO;

namespace Vtmis.WebAdmin.Web.Host.Controllers
{
    [ApiController]
    [AbpAuthorize]
    [Route("/api/MEH/Incident")]
    public class MEHIncidentController : AbpController
    {
        private readonly IMEHIncidentService _service;

        public MEHIncidentController(IMEHIncidentService service)
        {
            _service = service;
        }

        [HttpPost]
        public async Task<IActionResult> Post(RequestCreateMEHIncident input)
        {
            try
            {
                var result = await _service.CreateAsync(input);
                return Ok(result);
            }
            catch (Exception error)
            {
                throw error;
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _service.GetAllAsync();
            return Ok(result);
        }
    }
}
