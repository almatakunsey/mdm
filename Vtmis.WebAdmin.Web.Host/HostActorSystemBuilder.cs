﻿using Akka.Actor;
using Akka.Configuration;
using Akka.DI.AutoFac;
using Akka.Routing;
using Autofac;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using System;
using Vtmis.Core.ActorModel.Actors.Alarm;
using System.Data;
using System.Data.SqlClient;
using Vtmis.Core.ActorModel.Actors;
using Vtmis.Core.ActorModel.Actors.Route;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common;
using Vtmis.Core.Common.Constants.Akka;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.WebAdmin.Alarms.Dapper;
using Vtmis.WebAdmin.ReadOnlyDB;
using Vtmis.WebVessel.Tracking.Services.Hubs;
using Vtmis.WebVessel.Tracking.Repository.Concrete;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Vtmis.WebVessel.Tracking.Services.Concrete;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories.Alarms;
using Vtmis.WebAdmin.ReadOnlyDB.Services;
using System.Linq;
using System.Net.Sockets;

namespace Vtmis.WebAdmin.Web.Host
{
    public static class HostActorSystemBuilder
    {
        public static ActorSystem Execute(IConfiguration configuration, IHubContext<AlarmHub> alarmHubContext)
        {         

           

            var port = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PORT")) ? configuration["PORT"] : Environment.GetEnvironmentVariable("PORT");
            var seedPort = AppHelper.GetSeedPort();
            var seed = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("SEED")) ? configuration["SEED"] : Environment.GetEnvironmentVariable("SEED");
            //var seed = ".";
            var hostName = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("HOSTNAME")) ? configuration["HOSTNAME"] : Environment.GetEnvironmentVariable("HOSTNAME");

            var publicHostName = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PUBLIC_HOSTNAME")) ?
              configuration["PUBLIC_HOSTNAME"] : Environment.GetEnvironmentVariable("PUBLIC_HOSTNAME");

            if (string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase) == false)
            {
                var name = System.Net.Dns.GetHostName(); // get container id
                var ip = System.Net.Dns.GetHostEntry(name).AddressList.FirstOrDefault(x => x.AddressFamily == AddressFamily.InterNetwork);
                publicHostName = ip.ToString();
            }

            new AkkaLoggerConfig().SetupLog("HostAdmin");
            var globalActorConfig = string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL,
                StringComparison.CurrentCultureIgnoreCase) ? @" /GlobalActor {
                                router = round-robin-group
                                routees.paths = [""/user/GlobalActor""]
                                nr-of-instances = 1
                                       cluster {
                                          enabled = on
                                          max-nr-of-instances-per-node = 1                                        
                                          use-role = api
                                       }
                            }" : string.Empty;
            var config = ConfigurationFactory.ParseString(@"
                akka {
               loglevel=INFO,
                loggers=[""Akka.Logger.Serilog.SerilogLogger, Akka.Logger.Serilog""],
                    actor {
                        provider = ""Akka.Cluster.ClusterActorRefProvider, Akka.Cluster""
                   
                        deployment {                         
                          
                            " + globalActorConfig + @"
                            /route {
                               router = round-robin-pool
                               nr-of-intances = 4
                               cluster {
                                  enabled = on
                                  max-nr-of-instances-per-node = 2
                                  use-role = event
                               }
                            }

                            /AlarmEngineActor {
                               router = round-robin-pool
                               nr-of-intances = 4
                               cluster {
                                  enabled = on
                                  max-nr-of-instances-per-node = 2
                                  use-role = event
                               }
                            }
                        }
                    }

                    remote {
                           maximum-payload-bytes = 30000000 bytes
                           dot-netty.tcp {	
                                message-frame-size =  30000000b
                                send-buffer-size =  30000000b
                                receive-buffer-size =  30000000b
                                maximum-frame-size = 30000000b
                                #bind-hostname= 0.0.0.0
                                public-hostname = " + publicHostName + @"
								hostname = " + hostName + @"
								port = " + port + @"
                               
							}
                          }

                    cluster {
				            #will inject this node as a self-seed node at run-time
					        seed-nodes = [""akka.tcp://vtmis@" + seed + @":" + seedPort + @"""]
				            roles = [web]
			        }

             } #akka           
            ");

            // Create and build your container
            var builder = new ContainerBuilder();
            //builder.Register(ctx =>
            //{
            //    return alarmHubContext;
            //}).As<IHubContext<AlarmHub>>().InstancePerDependency();
            builder.Register(ctx =>
            {
                return new SqlConnection(AppHelper.GetAisConnectionString(DateTime.Now));
            }).As<IDbConnection>().SingleInstance();

            builder.RegisterType<MdmAdminDbContext>().InstancePerDependency();
            //builder.RegisterType<DapperAlarmRepository>().As<IDapperAlarmRepository>().InstancePerDependency();
            //builder.RegisterType<DapperEventRepository>().As<IDapperEventRepository>().InstancePerDependency();
            //builder.RegisterType<DapperEventDetailsRepository>().As<IDapperEventDetailsRepository>().InstancePerDependency();
            //builder.RegisterType<AlarmEngineActor>();
            //builder.RegisterType<AlarmListenerActor>();
            //builder.RegisterType<AlarmWorkerActor>();
            builder.RegisterType<VtsDatabaseContext>().InstancePerDependency();
            builder.RegisterType<DapperAisRepository>().As<IDapperAisRepository>();
            //builder.RegisterType<RouteService>().As<IRouteService>();
            //builder.RegisterType<AlarmNotificationRepository>().As<IAlarmNotificationRepository>();
            //builder.RegisterType<AlarmService>().As<IAlarmService>();
            //builder.RegisterType<RouteActor>();
            if (string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase))
            {
                builder.RegisterType<GlobalActor>();
            }


            var container = builder.Build();

            ActorSystem actorSystem = ActorSystem.Create("vtmis", config);
            var resolver = new AutoFacDependencyResolver(container, actorSystem);
            ActorRefsConst.ActorSystem = actorSystem;
            //actorSystem.ActorOf(resolver.Create<AlarmEngineActor>(), "AlarmEngineActor");

            //ActorRefsConst.Route = actorSystem.ActorOf(resolver.Create<RouteActor>(), "route");
            if (string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase))
            {
                ActorRefsConst.Global = actorSystem.ActorOf(Props.Empty.WithRouter(FromConfig.Instance), "GlobalActor");
            }

            return actorSystem;
        }
    }
}
