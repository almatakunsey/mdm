﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Vtmis.WebAdmin.Configuration;
using Abp.AspNetCore.SignalR;

namespace Vtmis.WebAdmin.Web.Host.Startup
{
    [DependsOn(
       typeof(WebAdminWebCoreModule))]
    [DependsOn(typeof(AbpAspNetCoreSignalRModule))]
    public class WebAdminWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public WebAdminWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WebAdminWebHostModule).GetAssembly());
        }
    }
}
