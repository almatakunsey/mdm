﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Castle.Facilities.Logging;
using Swashbuckle.AspNetCore.Swagger;
using Abp.AspNetCore;
using Abp.Castle.Logging.Log4Net;
using Vtmis.WebAdmin.Authentication.JwtBearer;
using Vtmis.WebAdmin.Configuration;
using Vtmis.WebAdmin.Identity;
using System.Collections.Generic;
using Vtmis.Core.Common.Helpers;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Vtmis.WebAdmin.ReadOnlyDB;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories;
using Vtmis.WebAdmin.Permissions;
using Vtmis.WebVessel.Tracking.Services.Hubs;
using Microsoft.AspNetCore.SignalR;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories.Alarms;
using Vtmis.WebAdmin.ReadOnlyDB.Services;
using Vtmis.WebAdmin.Alarms;
using Vtmis.Core.Common.Constants;
using Vtmis.WebVessel.Tracking.Repository.Concrete;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using System.Data;
using System.Data.SqlClient;
using Vtmis.WebAdmin.EICS;
using Npgsql;
using StackExchange.Redis;

namespace Vtmis.WebAdmin.Web.Host.Startup
{
    public class Startup
    {
        private const string _defaultCorsPolicyName = "localhost";

        private readonly IConfigurationRoot _appConfiguration;
        private readonly IHostingEnvironment _env;

        public Startup(IHostingEnvironment env)
        {
            _appConfiguration = env.GetAppConfiguration();
            _env = env;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {

            // MVC
            services.AddMvc(
                options => options.Filters.Add(new CorsAuthorizationFilterFactory(_defaultCorsPolicyName))
            ).AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
            // TODO: SSLConfig
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    services.AddHsts(options =>
            //    {
            //        options.Preload = true;
            //        options.IncludeSubDomains = true;
            //        options.MaxAge = TimeSpan.FromHours(23);
            //    });
            //    services.AddHttpsRedirection(options =>
            //    {
            //        options.RedirectStatusCode = StatusCodes.Status301MovedPermanently;
            //        options.HttpsPort = 50837;
            //    });
            //}
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    services.Configure<MvcOptions>(options =>
            //    {
            //        options.Filters.Add(new RequireHttpsAttribute());
            //    });
            //}

            IdentityRegistrar.Register(services);
            AuthConfigurer.Configure(services, _appConfiguration);

            //services.AddSignalR();
            if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.PRODUCTION))
            {
                services.AddSignalR()
             .AddStackExchangeRedis(new RedisConfig().RedisConnectionString, options =>
             {
                 options.Configuration.ChannelPrefix = "WebAdminPrefix";
                 options.Configuration.ClientName = "WebAdmin";
             });
            }
            else
            {
                services.AddSignalR();
            }

            // Configure CORS for angular2 UI
            services.AddCors(
                options => options.AddPolicy(
                    _defaultCorsPolicyName,
                    builder => builder
                        .WithOrigins(
                            AppHelper.GetCorsOriginList()
                         )
                         .SetIsOriginAllowed(isOriginAllowed: _ => true)
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                )
            );

            // Swagger - Enable this line and the related lines in Configure method to enable swagger UI
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "WebAdmin API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);

                // Define the BearerAuth scheme that's in use
                options.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });
                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };
                options.AddSecurityRequirement(security);
                // Assign scope requirements to operations based on AuthorizeAttribute
                options.OperationFilter<SecurityRequirementsOperationFilter>();
                options.DescribeAllEnumsAsStrings();
                options.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
            });
            services.AddDbContext<MdmAdminDbContext>(ServiceLifetime.Transient);
            services.AddTransient<IAbpPermissionRepository, AbpPermissionRepository>();
            services.AddTransient<IMdmUserRoleRepository, MdmUserRoleRepository>();
            services.AddTransient<IMdmPermissionService, MdmPermissionService>();
            services.AddTransient<IAlarmNotificationRepository, AlarmNotificationRepository>();
            services.AddTransient<IAlarmNotificationService, AlarmNotificationService>();
            services.AddSingleton<IConnectionMultiplexer>(x =>
                         ConnectionMultiplexer.Connect(AppHelper.GetRedisConnectionString()));
            services.AddTransient<IAlarmService, AlarmService>();
            services.AddSingleton<IUserIdProvider, NameUserIdProvider>();
            services.AddTransient<IDbConnection>(x => { return new SqlConnection(AppHelper.GetAisConnectionString(DateTime.Now)); });
            services.AddTransient<IDapperAisRepository, DapperAisRepository>();
            services.AddTransient(ctx =>
            {
                return new NpgsqlConnection(AppHelper.GetMdmDbConnectionString());
            });
            services.AddTransient<ITargetQuery, AISRepo_mongoDB>();
            services.AddTransient<ICacheService, RedisService>();
            services.AddTransient<IDapperAdmin, DapperAdmin>();
            services.AddTransient<Vtmis.WebVessel.Tracking.Services.Interfaces.IRouteService, Vtmis.WebVessel.Tracking.Services.Concrete.RouteService>();
            // Configure Abp and Dependency Injection
            return services.AddAbp<WebAdminWebHostModule>(
                // Configure Log4Net logging
                options => options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig("log4net.config")
                )
            );
        }

        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            app.UseAbp(options => { options.UseAbpRequestLocalization = false; }); // Initializes ABP framework.
            // TODO: SSLConfig
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    app.UseHsts();
            //    app.UseHttpsRedirection();
            //}
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    var options = new RewriteOptions()
            //               .AddRedirectToHttps(308, 50837);

            //    app.UseRewriter(options);
            //}

            app.UseCors(_defaultCorsPolicyName); // Enable CORS!

            app.UseStaticFiles();

            app.UseAuthentication();
            app.UseJwtTokenMiddleware();

            app.UseAbpRequestLocalization();

            app.UseSignalR(routes =>
            {
                routes.MapHub<AlarmHub>("/alarmhub");
                //routes.MapHub<AbpCommonHub>("/signalr");
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "defaultWithArea",
                    template: "{area}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //var alarmHubContext = app.ApplicationServices.GetService<IHubContext<AlarmHub>>();
            //HostActorSystemBuilder.Execute(_appConfiguration, alarmHubContext);

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();
            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUI(options =>
            {
                options.DocExpansion(Swashbuckle.AspNetCore.SwaggerUI.DocExpansion.None);
                options.InjectJavascript("/swagger/ui/abp.js");
                options.InjectJavascript("/swagger/ui/on-complete.js");
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "WebAdmin API V1");
            }); // URL: /swagger
         
        }
    }
}