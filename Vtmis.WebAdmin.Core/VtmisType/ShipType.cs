﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Vtmis.WebAdmin.VtmisType
{
    [Table("ShipTypes")]
    public class ShipType : Entity
    {
       
       
        public int ShipTypeNumber { get; set; }

        public string ShipTypeName { get; set; }

        
    }
}
