﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Zones;

namespace Vtmis.WebAdmin.Reports
{
    [Table("ReportItems")]
    public class ReportItem : Entity
    {


        public ReportItem()
        {

        }

        [ForeignKey("ReportId")]
        public virtual Report Report { get; set; }
        public virtual int ReportId { get; set; }


        public virtual Zone Zone { get; set; }
        public virtual int ZoneId { get; set; }

        public int? FilterGroupId { get; set; }
        public virtual FilterGroup FilterGroup { get; set; }

        public virtual Filter Filter { get; set; }
        public virtual int FilterId { get; set; }
    }

    public class DefaultReportItem : ReportItem
    {

    }

    //03
    public class ArrivalDeparture : ReportItem
    {

    }

    //18
    public class ZoneTrip : ReportItem
    {
        public decimal Fee { get; set; }
    }

    //05
    public class BerthTimeSummary : ReportItem
    {
        public decimal Fee { get; set; }
    }

    //07
    public class SpeedViolation : ReportItem
    {
        public decimal MinSpeed { get; set; }
        public decimal MaxSpeed { get; set; }
    }

    //16
    public class AnchorageTime : ReportItem
    {
        public decimal MaxSpeed { get; set; }
        public decimal Fee { get; set; }
    }

    
}
