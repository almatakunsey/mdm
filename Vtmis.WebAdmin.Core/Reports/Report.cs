﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.Reports
{
    [Table("Reports")]
    public class Report : FullAuditedEntity<int>, IMayHaveTenant
    {
        public Report()
        {
            CreationTime = DateTime.Now;
        }
        public string Name { get; set; }
        public double TimeSpan { get; set; }
        public TimeSpanType TimeSpanType { get; set; }
        public bool IsEnabled { get; set; }

        public virtual ReportType ReportType { get; set; }
        public virtual int ReportTypeId { get; set; }

        //[ForeignKey("ReportId")]
        public virtual List<ReportItem> ReportItems { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; protected set; }
        public virtual long UserId { get; protected set; }

        public int? TenantId { get; set; }

        public void SetPersonalizeUser(User user)
        {
            this.User = user;
            this.UserId = user.Id;
            this.TenantId = user.TenantId;
        }

        public void SetCurrentUser(int? tenantId, long? userId)
        {
            TenantId = tenantId;
            UserId = userId.Value;
            CreatorUserId = userId.Value;
        }

        public void UpdateReportItemWithId()
        {
            foreach (var ri in ReportItems)
            {
                ri.Report = this;
                //fi.FilterId = id;
            }
        }
    }

    public enum TimeSpanType
    {
        Minutes,
        Hours,
        Days,
        Months
    }
}
