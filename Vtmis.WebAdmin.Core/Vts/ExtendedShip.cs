﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.Vts
{
    [Table("ExtendedShips")]
    public class ExtendedShip : Entity
    {
        public int ShipId { get; set; }
        public bool IsBlacklisted { get; set; }
        public bool IsDeleted { get; set; }
        public int? MMSI { get; set; }
        public int? IMO { get; set; }
        public string CallSign { get; set; }
    }
}
