﻿using Abp;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq;
using Abp.Organizations;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Vtmis.WebAdmin.Authorization
{
    public abstract class MdmUserStoreBase<TRole, TUser> :
      
      ITransientDependency

      where TRole : AbpRole<TUser>
      where TUser : AbpUser<TUser>
    {
        public IAsyncQueryableExecuter AsyncQueryableExecuter { get; set; }

        private readonly IRepository<TUser, long> _userRepository;
        private readonly IRepository<UserLogin, long> _userLoginRepository;
        private readonly IRepository<UserRole, long> _userRoleRepository;
        private readonly IRepository<UserClaim, long> _userClaimRepository;
        private readonly IRepository<TRole> _roleRepository;
        private readonly IRepository<UserPermissionSetting, long> _userPermissionSettingRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<UserOrganizationUnit, long> _userOrganizationUnitRepository;
        private readonly IRepository<OrganizationUnitRole, long> _organizationUnitRoleRepository;

        /// <summary>
        /// Constructor.
        /// </summary>
        protected MdmUserStoreBase(
            IRepository<TUser, long> userRepository,
            IRepository<UserLogin, long> userLoginRepository,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<TRole> roleRepository,
            IRepository<UserPermissionSetting, long> userPermissionSettingRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<UserClaim, long> userClaimRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnitRepository,
            IRepository<OrganizationUnitRole, long> organizationUnitRoleRepository)
        {
            _userRepository = userRepository;
            _userLoginRepository = userLoginRepository;
            _userRoleRepository = userRoleRepository;
            _roleRepository = roleRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _userClaimRepository = userClaimRepository;
            _userOrganizationUnitRepository = userOrganizationUnitRepository;
            _organizationUnitRoleRepository = organizationUnitRoleRepository;
            _userPermissionSettingRepository = userPermissionSettingRepository;

            AsyncQueryableExecuter = NullAsyncQueryableExecuter.Instance;
        }

        #region IQueryableUserStore

        public virtual IQueryable<TUser> Users => _userRepository.GetAll();

        #endregion

        #region IUserStore

        [UnitOfWork]
        public virtual async Task<TUser> FindByIdAsync(long userId)
        {
            var result = await AsyncQueryableExecuter.FirstOrDefaultAsync(
                from u in _userRepository.GetAll().IgnoreQueryFilters().Include(x=>x.Roles)
                where u.Id == userId select u
                );
            return result;
        }

        [UnitOfWork]
        public virtual async Task SetRoleToUser(long userId,int roleId,int? tenantId)
        {
            var userRole = new UserRole
            {
                CreationTime = DateTime.Now,
                RoleId = roleId,
                UserId = userId,
                TenantId = tenantId
            };
            await _userRoleRepository.InsertAsync(userRole);
            await _unitOfWorkManager.Current.SaveChangesAsync();
        }

        public virtual async Task<TUser> FindByNameAsync(string userName)
        {
            var normalizedUsername = NormalizeKey(userName);

            return await _userRepository.FirstOrDefaultAsync(
                user => user.NormalizedUserName == normalizedUsername
            );
        }

        [UnitOfWork]
        public virtual async Task<IList<string>> GetRolesAsync(TUser user)
        {
            var userRoles = await AsyncQueryableExecuter.ToListAsync(from userRole in _userRoleRepository.GetAll()
                                                                     join role in _roleRepository.GetAll() on userRole.RoleId equals role.Id
                                                                     where userRole.UserId == user.Id
                                                                     select role.Name);

            var userOrganizationUnitRoles = await AsyncQueryableExecuter.ToListAsync(
                from userOu in _userOrganizationUnitRepository.GetAll()
                join roleOu in _organizationUnitRoleRepository.GetAll() on userOu.OrganizationUnitId equals roleOu
                    .OrganizationUnitId
                join userOuRoles in _roleRepository.GetAll() on roleOu.RoleId equals userOuRoles.Id
                where userOu.UserId == user.Id
                select userOuRoles.Name);

            return userRoles.Union(userOrganizationUnitRoles).ToList();
        }

        #endregion

        #region IDisposable

        public virtual void Dispose()
        {
            //No need to dispose since using IOC.
        }

        #endregion

      

        protected virtual string NormalizeKey(string key)
        {
            return key.ToUpperInvariant();
        }
    }
}
