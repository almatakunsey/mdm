using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq;
using Abp.Organizations;
using Microsoft.EntityFrameworkCore;
using Vtmis.WebAdmin.Authorization.Roles;

namespace Vtmis.WebAdmin.Authorization.Users
{
    public class UserStore : AbpUserStore<Role, User>
    {
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<User, long> _userRepository;

        public UserStore(
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<User, long> userRepository,
            IRepository<Role> roleRepository,
            IAsyncQueryableExecuter asyncQueryableExecuter,
            IRepository<UserRole, long> userRoleRepository,
            IRepository<UserLogin, long> userLoginRepository,
            IRepository<UserClaim, long> userClaimRepository,
            IRepository<UserPermissionSetting, long> userPermissionSettingRepository,
            IRepository<UserOrganizationUnit, long> userOrganizationUnit,
            IRepository<OrganizationUnitRole, long> organizationUnitRole)
            : base(
                  unitOfWorkManager,
                  userRepository,
                  roleRepository,
                  asyncQueryableExecuter,
                  userRoleRepository,
                  userLoginRepository,
                  userClaimRepository,
                  userPermissionSettingRepository,
                  userOrganizationUnit,
                  organizationUnitRole
                  )
        {
            _unitOfWorkManager = unitOfWorkManager;
            _userRepository = userRepository;
        }

        public IEnumerable<User> TestAll()
        {
            return Users.ToList();
        }

        public async Task<User> FindByIdAsync(long userId)
        {
            return await base.FindByIdAsync(userId.ToString());
        }

        //public async Task<User> FindByIdAsync(long userId)
        //{

        //    var result = await _userRepository.GetAll().IgnoreQueryFilters()
        //                  .Include(x => x.Roles)
        //                  .FirstOrDefaultAsync(x => x.Id == userId);
        //    return result;
        //}
    }
}
