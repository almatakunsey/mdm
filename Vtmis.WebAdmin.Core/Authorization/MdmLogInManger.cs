﻿using Microsoft.AspNetCore.Identity;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.Configuration;
using Abp.Configuration.Startup;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Zero.Configuration;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.MultiTenancy;

namespace Vtmis.WebAdmin.Authorization
{
    public class MdmLogInManger : MdmLogInManagerBase<Tenant, Role, User>
    {
        public MdmLogInManger(
             UserManager userManager,
             IMultiTenancyConfig multiTenancyConfig,
             IRepository<Tenant> tenantRepository,
             IUnitOfWorkManager unitOfWorkManager,
             ISettingManager settingManager,
             IRepository<UserLoginAttempt, long> userLoginAttemptRepository,
             IUserManagementConfig userManagementConfig,
             IIocResolver iocResolver,
             IPasswordHasher<User> passwordHasher,
             RoleManager roleManager,
             UserClaimsPrincipalFactory claimsPrincipalFactory,
             IRepository<User, long> userRepo)
             : base(
                   userManager,
                   multiTenancyConfig,
                   tenantRepository,
                   unitOfWorkManager,
                   settingManager,
                   userLoginAttemptRepository,
                   userManagementConfig,
                   iocResolver,
                   passwordHasher,
                   roleManager,
                   claimsPrincipalFactory,
                   userRepo
                   )
        {
        }

    }
}
