﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Uow;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        private readonly UserManager _userManager;
        private readonly RoleManager _roleManager;

        public PermissionChecker(UserManager userManager, RoleManager roleManager)
            : base(userManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public override async Task<bool> IsGrantedAsync(long userId, string permissionName)
        {

            var user = await _userManager.FindUserByIdAsync(userId);
            var isPermitted = false;
            foreach (var item in user.Roles)
            {
                if (await _roleManager.IsGrantedAsync(item.RoleId, permissionName))
                {
                    isPermitted = true;
                    break;
                }
            }
            return isPermitted;

        }

        public override async Task<bool> IsGrantedAsync(string permissionName)
        {

            var userId = AbpSession.UserId.Value;
            var user = await _userManager.FindUserByIdAsync(userId);
            var isPermitted = false;
            foreach (var item in user.Roles)
            {
                if (await _roleManager.IsGrantedAsync(item.RoleId, permissionName))
                {
                    isPermitted = true;
                    break;
                }
            }
            return isPermitted;
        }
    }
}
