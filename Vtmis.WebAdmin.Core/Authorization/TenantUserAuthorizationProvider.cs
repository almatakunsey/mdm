﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;
using Vtmis.Core.Common.Constants;

namespace Vtmis.WebAdmin.Authorization
{
    public class TenantUserAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Tenant);
            foreach (var permission in MdmPermissionsConst.DefaultUser())
            {
                context.CreatePermission(permission, L(permission));
            }
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, WebAdminConsts.LocalizationSourceName);
        }
    }
}
