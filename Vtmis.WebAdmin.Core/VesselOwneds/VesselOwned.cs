﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.VesselOwneds
{
    [Table("VesselOwned")]
    public class VesselOwned : Entity, IMayHaveTenant, IHasCreationTime
    {
        public int? TenantId { get; set; }
        public DateTime CreationTime { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public virtual long UserId { get; set; }

        public string Username { get; set; }
    
        //Array of MMSI
        public string MMSI { get; set; }
        
    }
}
