﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.VtmisType;

namespace Vtmis.WebAdmin.Colors
{
    [Table("AISColourSchemas")]
    public class AISColourSchema : Entity,IMustHaveTenant, IHasCreationTime
    {
        public AISColourSchema()
        {

            CreationTime = DateTime.Now;
        }
        public string Name { get; set; }
        public DateTime CreationTime { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
        public long UserId { get; set; }

        //[ForeignKey("AISColourSchemaId")]
        public List<AISColourSchemaItem> AISColourSchemaItems { get; set; }

        public int TenantId { get; set; }

        public void ClearItemBeforeUpdate()
        {
            this.AISColourSchemaItems.Clear();
        }

        public async Task UpdateAISColourSchemaItemRelation(IRepository<ShipType> shipTypeRepository)
        {
            foreach (var si in AISColourSchemaItems)
            {
                var shipType = await shipTypeRepository.FirstOrDefaultAsync(si.ShipTypeId);
                if (shipType != null)
                {
                    si.ShipType = shipType;
                    si.ShipTypeId = shipType.Id;
                    si.ShipTypeName = shipType.ShipTypeName;
                    si.ShipTypeNumber = shipType.ShipTypeNumber;
                }
                si.AISColourSchema = this;
            }
        }

        public void SetPersonalizeUser(User user)
        {
            this.User = user;
            this.UserId = user.Id;
            if(user.TenantId!=null)
                this.TenantId = user.TenantId.Value;
        }
    }

   
}
