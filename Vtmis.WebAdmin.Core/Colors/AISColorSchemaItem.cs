﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Vtmis.WebAdmin.VtmisType;

namespace Vtmis.WebAdmin.Colors
{
    [Table("AISColourSchemaItems")]
    public class AISColourSchemaItem : Entity, IHasCreationTime
    {
        public AISColourSchemaItem()
        {
            CreationTime = DateTime.Now;
        }

        public string ColorName { get; set; }
        public DateTime CreationTime { get; set; }

        public string ShipTypeName { get; set; }
        public int ShipTypeNumber { get; set; }

        [ForeignKey("ShipTypeId")]
        public int ShipTypeId { get; set; }
        public ShipType ShipType { get; set; }

        public AISType AISType { get; set; }

        [ForeignKey("AISColourSchemaId")]
        public AISColourSchema AISColourSchema { get; set; }
        public int AISColourSchemaId { get; set; }

    }
}
