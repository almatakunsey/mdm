﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Vtmis.Core.Common.Enums;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.Zones
{

    [Table("Zones")]
    public class Zone : Entity, IMayHaveTenant, IHasCreationTime
    {
        public Zone()
        {
            CreationTime = DateTime.Now;
            ZoneItems = new List<ZoneItem>();
        }
        [ForeignKey("UserId")]
        public virtual User User { get; protected set; }
        public virtual long UserId { get; protected set; }

        public int? TenantId { get; set; }
        public DateTime CreationTime { get; set; }

        public string Name { get; set; }
        public bool IsEnable { get; set; }

        public decimal Radius { get; set; }
        public string Colour { get; set; }
        public bool IsFill { get; set; }

        public ZoneType ZoneType { get; set; }

        //[ForeignKey("ZoneId")]
        public virtual ICollection<ZoneItem> ZoneItems { get; set; }

        public void SetPersonalizeUser(User user)
        {
            this.User = user;
            this.UserId = user.Id;
            this.TenantId = user.TenantId;

        }

        public void SetCurrentUser(int? tenantId, long? userId)
        {
            TenantId = tenantId;
            UserId = userId.Value;
        }

        public void UpdateZoneItemWithId()
        {
            foreach (var fi in ZoneItems)
            {
                fi.Zone = this;
            }
        }

      

        public void ClearItemBeforeUpdate()
        {
            this.ZoneItems.Clear();
        }
    }

    
}
