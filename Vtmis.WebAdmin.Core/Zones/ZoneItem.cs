﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Vtmis.WebAdmin.Zones
{

    [Table("ZoneItems")]
    public class ZoneItem : Entity, IHasCreationTime
    {
        public DateTime CreationTime { get; set; }

        public double Latitude { get; set; }
        public double Logitude { get; set; }
        public int Order { get; set; }

        [ForeignKey("ZoneId")]
        public virtual Zone Zone { get; set; }
        public virtual int ZoneId { get; set; }
    }
}
