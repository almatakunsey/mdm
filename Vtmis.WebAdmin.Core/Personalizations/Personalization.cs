﻿using Abp.Domain.Entities;

namespace Vtmis.WebAdmin.Personalizations
{
    public class Personalization : Entity
    {
        public int? TenantId { get; set; }
        public long UserId { get; set; }
        public string UserLevel { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
