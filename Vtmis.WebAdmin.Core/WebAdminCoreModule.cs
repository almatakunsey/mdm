﻿using Abp.AutoMapper;
using Abp.Localization;
using Abp.Modules;
using Abp.Reflection.Extensions;
using Abp.Timing;
using Abp.Zero;
using Abp.Zero.Configuration;
using System.Reflection;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Configuration;
using Vtmis.WebAdmin.Localization;
using Vtmis.WebAdmin.MultiTenancy;
using Vtmis.WebAdmin.Timing;

namespace Vtmis.WebAdmin
{
    [DependsOn(typeof(AbpZeroCoreModule))]
    public class WebAdminCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            // Declare entity types
            Configuration.Modules.Zero().EntityTypes.Tenant = typeof(Tenant);
            Configuration.Modules.Zero().EntityTypes.Role = typeof(Role);
            Configuration.Modules.Zero().EntityTypes.User = typeof(User);

            
            WebAdminLocalizationConfigurer.Configure(Configuration.Localization);
           
            // Enable this line to create a multi-tenant application.
            Configuration.MultiTenancy.IsEnabled = WebAdminConsts.MultiTenancyEnabled;

            // Configure roles
            AppRoleConfig.Configure(Configuration.Modules.Zero().RoleManagement);

            Configuration.Settings.Providers.Add<AppSettingProvider>();

            //Configuration.Modules.AbpAutoMapper().Configurators.Add(config =>
            //{
            //    config.CreateMap<CreateUserInput, User>()
            //          .ForMember(u => u.Password, options => options.Ignore())
            //          .ForMember(u => u.Email, options => options.MapFrom(input => input.EmailAddress));
            //});
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            //IocManager.RegisterAssemblyByConvention(typeof(WebAdminCoreModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            IocManager.Resolve<AppTimes>().StartupTime = Clock.Now;
        }
    }
}
