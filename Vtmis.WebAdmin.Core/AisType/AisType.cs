﻿using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.AisType
{    
    [Table("AisTypes")]
    public class AisType : FullAuditedEntity<Guid>
    {
        public string Name { get; set; }
        public int MessageId { get; set; }
    }
}
