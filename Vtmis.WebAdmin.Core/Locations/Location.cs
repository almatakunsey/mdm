﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Repositories;

namespace Vtmis.WebAdmin.Locations
{
    [Table("Locations")]
    public class Location : FullAuditedEntity<Guid>
    {
        public int? TenantId { get; set; }
        public long UserId { get; set; }

        public string Name { get; set; }

        public string Longitude { get; set; }

        public string Latitude { get; set; }

        public int ZoomLevel { get; set; }

        public static Location Create(int? tenantId, string name, string latitude, string longitude, int zoomLevel)
        {
            return new Location { Id = Guid.NewGuid(), ZoomLevel = zoomLevel, TenantId = tenantId, Name = name, Latitude = latitude, Longitude = longitude };
        }

        public async Task DeleteAsync(IRepository<Location, Guid> repository)
        {
            if (repository == null) { throw new ArgumentNullException("repository"); }

            await repository.DeleteAsync(this);
        }

        public async Task UpdateAsync(IRepository<Location, Guid> repository)
        {
            if (repository == null) { throw new ArgumentNullException("repository"); }

            await repository.UpdateAsync(this);
        }

        public void SetCurrentUser(int? tenantId, long? userId)
        {
            TenantId = tenantId;
            UserId = userId.Value;
            CreatorUserId = userId.Value;
        }
    }
}
