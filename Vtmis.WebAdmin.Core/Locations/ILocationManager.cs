﻿using Abp.Domain.Services;
using System;
using System.Threading.Tasks;

namespace Vtmis.WebAdmin.Locations
{
    public interface ILocationManager : IDomainService
    {
        Task<Location> GetAsync(Guid id);

        Task CreateAsync(Location location);

        Task UpdateSync(Location location);

        Task Remove(Guid id);
    }
}
