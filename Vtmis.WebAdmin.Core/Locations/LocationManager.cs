﻿using System;
using System.Threading.Tasks;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using Abp.UI;

namespace Vtmis.WebAdmin.Locations
{
    public class LocationManager : ILocationManager
    {
        private readonly IRepository<Location, Guid> _repository;
        private readonly AutoMapperObjectMapper _mapper;

        public LocationManager(IRepository<Location, Guid> repository)
        {
            _repository = repository;
        }

        public async Task CreateAsync(Location location)
        {
            await _repository.InsertAsync(location);
        }

        public async Task<Location> GetAsync(Guid id)
        {
            var location = await _repository.FirstOrDefaultAsync(id);
            if (location == null)
            {
                throw new UserFriendlyException("Could not found the location, maybe it's deleted!");
            }

            return location;
        }

        public async Task Remove(Guid id)
        {
            var location = await GetAsync(id);
            if (location == null) throw new Exception("Invalid location");

            await location.DeleteAsync(_repository);
        }

        public async Task UpdateSync(Location location)
        {
            var entity = await GetAsync(location.Id);
            if (entity == null) return;

            entity = _mapper.Map<Location>(location);

            await entity.UpdateAsync(_repository);
        }
    }
}
