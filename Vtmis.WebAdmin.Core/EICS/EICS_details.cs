﻿using Abp.Domain.Entities;
using Vtmis.WebAdmin.EICS_Equipments;

namespace Vtmis.WebAdmin.EICS
{
    public class EICS_details : Entity
    {
        public int EquipmentId { get; set; }
        public int EICS_infoid { get; set; }

        public virtual EICS_Equipment Equipment { get; set; }
    }
}
