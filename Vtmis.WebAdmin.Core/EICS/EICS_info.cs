﻿using Abp.Domain.Entities;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.EICS
{
    public class EICS_info : Entity
    {
        public EICS_info()
        {
            EICS_Details = new List<EICS_details>();
        }

        public int MMSI { get; set; }
        public ICollection<EICS_details> EICS_Details { get; set; }
    }
}
