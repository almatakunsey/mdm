﻿namespace Vtmis.WebAdmin
{
    public class WebAdminConsts
    {
        public const string LocalizationSourceName = "WebAdmin";

        public const string ConnectionStringName = "Default";

        public const string VtsConnectionStringName = "Vts";

        public const bool MultiTenancyEnabled = true;

        public const string DEFAULT = "default";

        public const string CUSTOM = "custom";
    }
}
