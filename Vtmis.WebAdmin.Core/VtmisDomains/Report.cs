﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;

namespace Vtmis.WebAdmin.VtmisDomains
{
    #region Reports
    public class Report : Entity, IHasCreationTime
    {
        public Report()
        {
            CreationTime = DateTime.Now;
        }
        public DateTime CreationTime { get; set; }
        public string Name { get; set; }
        public double TimeSpan { get; set; }
        public TimeSpanType TimeSpanType { get; set; }
        public bool IsEnabled { get; set; }
        public int ReportTypeId { get; set; }
        public virtual ReportType ReportType { get; set; }
        public int PersonalizeId { get; set; }
        public virtual Personalize Personalize { get; set; }
    }

    public enum TimeSpanType
    {
        Minutes,
        Hours,
        Days,
        Months
    }
    #endregion
}
