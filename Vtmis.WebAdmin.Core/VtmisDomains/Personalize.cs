﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.VtmisDomains
{
    public class Personalize : Entity, IHasCreationTime
    {
        public Personalize()
        {
            CreationTime = DateTime.Now;
        }
        public DateTime CreationTime { get; set; }
        public string Name { get; set; }
        public PersonalizeType PersonalizeType { get; set; }
        public virtual ICollection<Report> Reports { get; set; }
        //public long UserId { get; set; }
        //public virtual User User { get; set; }
    }

    public enum PersonalizeType
    {
        Report
    }
}
