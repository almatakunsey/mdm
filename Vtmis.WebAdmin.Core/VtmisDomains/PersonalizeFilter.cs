﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.VtmisDomains
{
    [Table("PersonalizeFilters")]
    public class PersonalizeFilter :Entity, IHasCreationTime
    {
        public PersonalizeFilter()
        {
            CreationTime = DateTime.Now;
            PersonalizeFilterItems = new List<PersonalizeFilterItem>();
        }

        public DateTime CreationTime { get; set; }
        public string FilterName { get; set; }
        //public long UserId { get; set; }
        //public User User { get; set; }

        public List<PersonalizeFilterItem> PersonalizeFilterItems { get; set; }

        public void AddPersonlizeFilterItem(PersonalizeFilterItem item)
        {
            if (PersonalizeFilterItems == null)
                PersonalizeFilterItems = new List<PersonalizeFilterItem>();

            item.PersonalizeFilter = this;
            PersonalizeFilterItems.Add(item);
        }
        
    }
    #region FilterItems
    [Table("PersonalizeFilterItems")]
    public class PersonalizeFilterItem : Entity
    {
        public PersonalizeFilterItem()
        {

        }
        
        public int PersonalizeFilterId { get; set; }
        public PersonalizeFilter PersonalizeFilter { get; set; }

        public bool IsEnable { get; set; }
        public bool IsShowShips { get; set; }
        public int FilterGroupId { get; set; }
        public FilterGroup FilterGroup { get; set; }
        public int MMSI { get; set; }
        public string ShipName { get; set; }
        public string CallSign { get; set; }
        public string ShipType { get; set; }
        public string AISType { get; set; }
        public decimal? MinSpeed { get; set; }
        public decimal? MaxSpeed { get; set; }
        public decimal? MinCourse { get; set; }
        public decimal? MinLength { get; set; }
        public decimal? MaxLength { get; set; }
        public string DataSource { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public string Color { get; set; }
        public string BorderColor { get; set; }
    }

    [Table("FilterGroups")]
    public class FilterGroup : Entity
    {
        public FilterGroup()
        {

        }

        public string GroupName { get; set; }
        public string MMSIs { get; set; }
        public string Names { get; set; }
        public string CallSigns { get; set; }
    }
    #endregion

   
}
