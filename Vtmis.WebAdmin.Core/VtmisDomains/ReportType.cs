﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;

namespace Vtmis.WebAdmin.VtmisDomains
{
    public class ReportType : Entity, IHasCreationTime
    {
        public ReportType()
        {
            CreationTime = DateTime.Now;
        }
        public DateTime CreationTime { get; set; }
        public string Name { get; set; }
    }
}
