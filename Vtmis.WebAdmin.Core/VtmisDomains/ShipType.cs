﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Vtmis.WebAdmin.VtmisDomains
{
    [Table("ShipTypes")]
    public class ShipType : Entity, IHasCreationTime
    {
        public ShipType()
        {
            CreationTime = DateTime.Now;
        }

        public DateTime CreationTime { get; set; }

        public int ShipTypeNumber { get; set; }

        public string ShipTypeName { get; set; }

        public string DetailType { get; set; }
    }
}
