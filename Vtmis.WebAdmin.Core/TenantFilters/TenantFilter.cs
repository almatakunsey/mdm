﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Vtmis.Core.Common.Enums.TenantFilters;

namespace Vtmis.WebAdmin.TenantFilters
{
    public class TenantFilter : FullAuditedEntity<int>, IMayHaveTenant
    {
        public TenantFilter()
        {
            FilterItems = new List<TenantFilterItem>();
            AssignedTenants = new List<int>();
            AssignedUsers = new List<long>();
        }
        [StringLength(50)]
        public string FilterName { get; set; }
        public TenantFilterType Type { get; set; }
        public List<TenantFilterItem> FilterItems { get; set; }
        public int? TenantId { get; set; }
        public List<int> AssignedTenants { get; set; }
        public List<long> AssignedUsers { get; set; }
    }
}
