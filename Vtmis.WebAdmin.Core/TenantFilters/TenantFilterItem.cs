﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.TenantFilters
{
    public class TenantFilterItem : Entity
    {
        public Condition Condition { get; set; }
        [StringLength(50)]
        public string ColumnName { get; set; }
        [StringLength(10)]
        public string Operation { get; set; }       
        public string ColumnValue { get; set; }

        public int RowIndex { get; set; }
    }
}