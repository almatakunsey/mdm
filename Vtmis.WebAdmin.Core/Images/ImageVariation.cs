﻿using Abp.Domain.Entities;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Images
{
    public class ImageVariation : Entity<long>
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public string FullPath { get; set; }
        public ImageVariationType VariationName { get; set; }
    }
}
