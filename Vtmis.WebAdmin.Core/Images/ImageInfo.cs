﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Images
{
    public class ImageInfo : Entity<long>
    {
        public ImageInfo()
        {
            ImageDetails = new List<ImageDetail>();
        }
        public string ImageId { get; set; }
        public ImageType ImageType { get; set; }
        public virtual IList<ImageDetail> ImageDetails { get; set; }
    }
}
