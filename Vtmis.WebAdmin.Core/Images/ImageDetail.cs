﻿using Abp.Domain.Entities;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.Images
{
    public class ImageDetail : Entity<long>
    {        
        public string Name { get; set; }
        public string Path { get; set; }
        public string FullPath { get; set; }
        public string Original { get; set; }       
    }
}
