﻿using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using Vtmis.WebAdmin.Images;

namespace Vtmis.WebAdmin.Targets
{
    public class TargetInfo : FullAuditedEntity<long>
    {
        public TargetInfo()
        {
            ImageDetails = new List<ImageDetail>();
        }
        public int MMSI { get; set; }
        public virtual IList<ImageDetail> ImageDetails { get; set; }
    }
}
