﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using Vtmis.WebAdmin.Alarms;

namespace Vtmis.WebAdmin.Events
{
    public class Event : Entity<long>, ISoftDelete, IHasCreationTime
    {
        public DateTime CreationTime { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletionTime { get; set; }
        public bool Kill { get; set; }
        public virtual List<EventDetails> EventDetails { get; set; }

        //Alarm
        public int AlarmId { get; set; }
        public virtual Alarm Alarm { get; set; }
    }
}
