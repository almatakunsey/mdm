﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.Events
{
    [Table("EventHistories")]
    public class EventHistory : Entity<long>
    {
        
        public int? MMSI { get; set; }
        public DateTime? TargetRecvTime { get; set; } // RecvTime from AIS ShipPosition   
        public string ShipName { get; set; }
        public string ZoneName { get; set; }
        public string Info { get; set; }
        public string Message { get; set; }
        public DateTime? LocalRecvTime { get; set; }
    }
}
