﻿using Abp.Domain.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.Events
{
    [Table("EventDetails")]
    public class EventDetails : Entity<long>
    {
        public EventDetails()
        {
            EventHistories = new List<EventHistory>();
        }
        public virtual List<EventHistory> EventHistories { get; set; }
        public long EventId { get; set; }
        public virtual Event Event { get; set; }
    }
}
