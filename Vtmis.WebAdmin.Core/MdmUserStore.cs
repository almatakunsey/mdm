﻿using Abp.Authorization.Users;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq;
using Abp.Organizations;
using Vtmis.WebAdmin.Authorization;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin
{
    public class MdmUserStore : MdmUserStoreBase<Role, User>
    {
        public MdmUserStore(
          IUnitOfWorkManager unitOfWorkManager,
          IRepository<User, long> userRepository,
          IRepository<Role> roleRepository,
          IAsyncQueryableExecuter asyncQueryableExecuter,
          IRepository<UserRole, long> userRoleRepository,
          IRepository<UserLogin, long> userLoginRepository,
          IRepository<UserClaim, long> userClaimRepository,
          IRepository<UserPermissionSetting, long> userPermissionSettingRepository,
          IRepository<UserOrganizationUnit, long> userOrganizationUnit,
          IRepository<OrganizationUnitRole, long> organizationUnitRole)
          : base(
                userRepository,
                userLoginRepository,
                userRoleRepository,
                roleRepository,
                userPermissionSettingRepository,
                unitOfWorkManager,
                userClaimRepository,
                userOrganizationUnit,
                organizationUnitRole
                )
        {           
        }
    }
}
