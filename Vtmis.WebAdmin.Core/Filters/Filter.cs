﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.Filters
{
    [Table("Filters")]
    public class Filter : FullAuditedEntity<int>, IMayHaveTenant
    {
        public int? TenantId { set; get; }

        [ForeignKey("UserId")]
        public virtual User User { get; protected set; }
        public virtual long UserId { get; protected set; }

        [ForeignKey("FilterId")]
        public virtual ICollection<FilterItem> FilterItems { get; set; }

        public string Name { get; set; }

        public bool IsEnable { get; set; }
        public bool IsShowShips { get; set; }

        protected Filter()
        {
            FilterItems = new List<FilterItem>();
            CreationTime = DateTime.Now;
        }

        public static Filter Create(string name, bool isEnable, bool isShowShips, User user, List<FilterItem> filterItems)
        {
            var filter = new Filter
            {
                TenantId = user.TenantId,
                Name = name,
                User = user,
                UserId = user.Id,
                IsEnable = isEnable,
                IsShowShips = isShowShips
            };

            filter.FilterItems = filterItems;

            return filter;
        }

        public void SetCurrentUser(int? tenantId, long? userId)
        {
            TenantId = tenantId;
            UserId = userId.Value;
            CreatorUserId = userId.Value;
        }

        //public void SetPersonalizeUser(User user)
        //{
        //    this.User = user;
        //    this.UserId = user.Id;
        //    this.TenantId = user.TenantId;

        //}

        //public void UpdateFilterItemWithId()
        //{
        //    foreach (var fi in FilterItems)
        //    {
        //        fi.Filter = this;
        //        //fi.FilterId = id;
        //    }
        //}

        //public void ClearItemBeforeUpdate()
        //{
        //    this.FilterItems.Clear();
        //}
    }
}
