﻿using Abp.Domain.Entities;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Filters
{
    public class FilterDetail : Entity
    {
        public Condition Condition { get; set; }
        public string ColumnName { get; set; }
        public string Operation { get; set; }
        public string ColumnValue { get; set; }

        public int RowIndex { get; set; }      
    }
}
