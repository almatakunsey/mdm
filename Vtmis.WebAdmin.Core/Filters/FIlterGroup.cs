﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.Filters
{
    [Table("FilterGroups")]
    public class FilterGroup : Entity, IHasCreationTime, IMayHaveTenant
    {

        public string Name { get; set; }
        public int? TenantId { get; set; }

        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        public virtual long UserId { get; set; }

        public DateTime CreationTime { set; get; }

        public string MMSIList { get; set; }
        public string NameList { get; set; }
        public string CallSignList { get; set; }


        public FilterGroup()
        {
            CreationTime = DateTime.Now;
        }

        //Create entry
        public static FilterGroup CreateFilterGroup(string name, string mmsilist, string namelist, string callsignlist)
        {
            var filterGroup = new FilterGroup
            {
                Name = name,
                MMSIList = mmsilist,
                NameList = namelist,
                CallSignList = callsignlist

            };

            return filterGroup;
        }
    }
}
