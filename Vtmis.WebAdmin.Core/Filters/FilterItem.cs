﻿using Abp.Domain.Entities;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.Filters
{
    //[Table("FilterItems")]
    public class FilterItem : Entity
    {
        public FilterItem()
        {
            FilterDetails = new List<FilterDetail>();
        }
        public string Colour { get; set; }
        public string BorderColour { get; set; }
        public List<FilterDetail> FilterDetails { get; set; }
    }
}
