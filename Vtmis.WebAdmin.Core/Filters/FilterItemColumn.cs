﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Vtmis.WebAdmin.Filters
{
    public class FilterItemColumn : Entity, IHasCreationTime,ICreationAudited
    {
        public DateTime CreationTime { get; set; }

        public FilterItemColumn()
        {
            this.CreationTime = DateTime.Now;
        }

        public string Name { get; set; }
        public long? CreatorUserId { get; set; }

        public static FilterItemColumn Create(string name,long creatorUserId)
        {
            return new FilterItemColumn
            {
                Name = name,
                CreatorUserId = creatorUserId
            };
        }
    }
}
