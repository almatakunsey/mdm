﻿using System.Collections.Generic;

namespace Vtmis.WebAdmin.MEH
{
    public interface IMEHLocation
    {
        ICollection<(float, float)> Location { get; set; }
    }
}
