﻿using Abp.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Vtmis.WebAdmin.MEH
{
    public class MEHIncidentReport : Entity<int>, IIncidentReportMeta
    {
        public MEHIncidentReport()
        {
            Locations = new List<MEHIncidentLocation>();
        }
        public int IncidentId { get; set; }
        public DateTime DateInsertedByMDM { get; set; }
        public DateTime? LastModifiedDateByMDM { get; set; }
        public DateTime DateInsertedFromMEH { get; set; }
        public DateTime? LastModifiedDateFromMEH { get; set; }
        [StringLength(50)]
        public string MetaStatus { get; set; }
        [StringLength(50)]
        public string MetaType { get; set; }
        public DateTime? Date { get; set; }
        [StringLength(50)]
        public string TypeOfCasualty { get; set; }
        [StringLength(50)]
        public string CoordType { get; set; }
        public List<MEHIncidentLocation> Locations { get; set; }
        public int? LossOfLife { get; set; }
        [StringLength(50)]
        public string MarinePollution { get; set; }
        [StringLength(50)]
        public string Visibility { get; set; }
        [StringLength(50)]
        public string SeaState { get; set; }
        [StringLength(50)]
        public string ForceWind { get; set; }
        [StringLength(50)]
        public string VesselNameA { get; set; }
        [StringLength(50)]
        public string CallSignA { get; set; }
        [StringLength(50)]
        public int? MmsiA { get; set; }
        [StringLength(50)]
        public string TypeA { get; set; }
        [StringLength(50)]
        public string FlagA { get; set; }
        [StringLength(50)]
        public string DestinationA { get; set; }
        [StringLength(50)]
        public string CargoA { get; set; }
        [StringLength(50)]
        public string VesselNameB { get; set; }
        [StringLength(50)]
        public string CallSignB { get; set; }
        [StringLength(50)]
        public int? MmsiB { get; set; }
        [StringLength(50)]
        public string TypeB { get; set; }
        [StringLength(50)]
        public string FlagB { get; set; }
        [StringLength(50)]
        public string DestinationB { get; set; }
        [StringLength(50)]
        public string CargoB { get; set; }
        public string AccidentDescription { get; set; }
        [StringLength(50)]
        public string OperatorInCharge { get; set; }
    }
    public class MEHIncidentCoordinate : Entity
    {
        public int? MEHIncidentLocationId { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
    }
    public class MEHIncidentLocation : Entity
    {
        public MEHIncidentLocation()
        {
            Coordinates = new List<MEHIncidentCoordinate>();
        }
        public int? MEHIncidentReportId { get; set; }
        public List<MEHIncidentCoordinate> Coordinates { get; set; }
    }
}
