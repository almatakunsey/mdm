﻿namespace Vtmis.WebAdmin.MEH
{
    public interface IIncidentReportMeta
    {      
        string MetaStatus { get; set; }       
        string MetaType { get; set; }
    }
}
