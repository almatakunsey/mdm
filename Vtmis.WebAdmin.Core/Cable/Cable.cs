﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.Cable
{
    [Table("Cables")]
    public class Cable : FullAuditedEntity<int>, IMayHaveTenant
    {
        public Cable()
        {
            CableDetails = new List<CableDetail>();
        }
        public long UserId { get; set; }
        public int? TenantId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public bool IsEnable { get; set; }
        public string Color { get; set; }
        public DateTime Date { get; set; }
        public List<CableDetail> CableDetails { get; set; }
    }
}
