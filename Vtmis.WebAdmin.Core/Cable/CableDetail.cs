﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.Cable
{
    [Table("CableDetails")]
    public class CableDetail : Entity<int>
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Order { get; set; }
    }
}
