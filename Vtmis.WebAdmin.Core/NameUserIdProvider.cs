﻿using Microsoft.AspNetCore.SignalR;
using System.Linq;
using System.Security.Claims;

namespace Vtmis.WebAdmin
{
    public class NameUserIdProvider : IUserIdProvider
    {
        public string GetUserId(HubConnectionContext connection)
        {
            return connection.User?.Claims.FirstOrDefault(x=> x.Type == ClaimTypes.NameIdentifier)?.Value;
        }
    }
}
