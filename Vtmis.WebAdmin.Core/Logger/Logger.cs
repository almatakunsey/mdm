﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.Logger
{
    [Table("Logs")]
    public class Logger : Entity, IHasCreationTime
    {
        public Logger()
        {
            CreationTime = DateTime.Now;
        }
        public string Log { get; set; }
        public DateTime CreationTime { get; set; }        
    }
}
