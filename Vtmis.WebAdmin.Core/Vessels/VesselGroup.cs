﻿using Abp.Domain.Entities;
using System.Collections.Generic;

namespace Vtmis.WebAdmin.Vessels
{
    public class VesselGroup : Entity
    {
        public long? UserId { get; set; }
        public int? TenantId { get; set; }
        public VesselGroup()
        {
            VesselGroupDetails = new List<VesselGroupDetail>();
        }
        public string Name { get; set; }
        public ICollection<VesselGroupDetail> VesselGroupDetails { get; set; }

        public void SetCurrentUser(int? tenantId, long? userId)
        {
            TenantId = tenantId;
            UserId = userId.Value;
        }
    }
}
