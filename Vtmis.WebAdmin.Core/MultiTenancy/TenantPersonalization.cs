﻿using Abp.Domain.Entities;

namespace Vtmis.WebAdmin.MultiTenancy
{
    public class TenantPersonalization : Entity<long>
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int? TenantId { get; set; }
        public virtual Tenant Tenant { get; set; }
    }
}
