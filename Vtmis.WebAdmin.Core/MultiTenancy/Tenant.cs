﻿using Abp.MultiTenancy;
using Vtmis.WebAdmin.Authorization.Users;

namespace Vtmis.WebAdmin.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
