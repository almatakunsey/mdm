﻿using Abp.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.Alarms
{
    [Table("AlarmNotifications")]
    public class AlarmNotification : Entity<long>
    {         
        public long EventDetailsId { get; set; }
        public bool IsRead { get; set; }
        public long UserId { get; set; }
        public int? TenantId { get; set; }
        
        public virtual Events.EventDetails EventDetails { get; set; }
    }
}
