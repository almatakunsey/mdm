﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Enums;

namespace Vtmis.WebAdmin.Alarms
{
    [Table("AlarmItems")]
    public class AlarmItem : Entity, IHasCreationTime
    {
        public AlarmItem()
        {
            CreationTime = DateTime.Now;
        }
       
        [ForeignKey("AlarmId")]
        public virtual Alarm Alarm { get; set; }
        public virtual int AlarmId { get; set; }

        public Condition Condition { get; set; }
        public string ColumnType { get; set; }
        public string ColumnItem { get; set; }
        public string Operation { get; set; }
        public string ColumnValue { get; set; }
        public int RowIndex { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
