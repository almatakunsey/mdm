﻿namespace Vtmis.WebAdmin.Alarms
{
    public enum AlarmPriority
    {
        Low,
        Medium,
        High
    }
}
