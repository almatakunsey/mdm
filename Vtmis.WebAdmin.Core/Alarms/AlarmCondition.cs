﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Zones;

namespace Vtmis.WebAdmin.Alarms
{
    [Table("AlarmConditions")]
    public class AlarmCondition : Entity, IMustHaveTenant, IHasCreationTime
    {
        public Zone Zone { get; set; }

        public Filter Filter { get; set; }

        // Condition

        public int Min { get; set; }

        public int Max { get; set; }

        public int TenantId { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
