﻿using System;
using System.IO;
using System.Linq;
using Abp.Reflection.Extensions;
using Microsoft.Extensions.Configuration;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.Configuration;

namespace Vtmis.WebAdmin.Web
{
    /// <summary>
    /// This class is used to find root path of the web project in;
    /// unit tests (to find views) and entity framework core command line commands (to find conn string).
    /// </summary>
    public static class WebContentDirectoryFinder
    {       

        public static string GetDefaultConnectionString()
        {
            //add new line to get path
            string startupPath = Directory.GetCurrentDirectory();
            
            //remove this line
            //var rootDoc = WebContentDirectoryFinder.CalculateContentRootFolder();
            var configuration = AppConfigurations.Get(startupPath);
          
            //return configuration.GetConnectionString(
            //    WebAdminConsts.ConnectionStringName
            //    );
            return AppHelper.GetMdmDbConnectionString();
        }
        public static string CalculateContentRootFolder()
        {
            var coreAssemblyDirectoryPath = Path.GetDirectoryName(typeof(WebAdminCoreModule).GetAssembly().Location);
            if (coreAssemblyDirectoryPath == null)
            {
                throw new Exception("Could not find location of Vtmis.WebAdmin.Core assembly!");
            }

            var directoryInfo = new DirectoryInfo(coreAssemblyDirectoryPath);
            while (!DirectoryContains(directoryInfo.FullName, "VtmisApps.sln"))
            {
                if (directoryInfo.Parent == null)
                {
                    throw new Exception("Could not find content root folder!");
                }

                directoryInfo = directoryInfo.Parent;
            }

            var webMvcFolder = Path.Combine(directoryInfo.FullName, "Vtmis.WebAdmin.Web.Mvc");
            if (Directory.Exists(webMvcFolder))
            {
                return webMvcFolder;
            }

            var webHostFolder = Path.Combine(directoryInfo.FullName, "Vtmis.WebAdmin.Web.Host");
            Console.WriteLine("WebHostFolder " + webHostFolder);

            if (Directory.Exists(webHostFolder))
            {
                return webHostFolder;
            }

            throw new Exception("Could not find root folder of the web project!");
        }

        private static bool DirectoryContains(string directory, string fileName)
        {
            return Directory.GetFiles(directory).Any(filePath => string.Equals(Path.GetFileName(filePath), fileName));
        }
    }
}
