﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Vtmis.WebAdmin.Routes
{
    [Table("Routes")]
    public class Route : FullAuditedEntity<int>, IMayHaveTenant
    {
        public Route()
        {
            Waypoints = new List<Waypoint>();
        }
        public string Name { get; set; }
        public bool IsEnable { get; set; }
        public string Color { get; set; }
        public virtual List<Waypoint> Waypoints { get; set; }

        public long UserId { get; set; }
        public int? TenantId { get; set; }
    }
}
