﻿dotnet publish Vtmis.WebAdmin.Web.Host/Vtmis.WebAdmin.Web.Host.csproj -c Release -o publish_cmd

dotnet publish Vtmis.WebAdmin.Web.Mvc/Vtmis.WebAdmin.Web.Mvc.csproj -c Release -o publish_cmd

dotnet publish Vtmis.Cluster.Seed/Vtmis.Cluster.Seed.csproj -c Release -o publish_cmd

dotnet publish Vtmis.Cluster.VesselQuery/Vtmis.Cluster.VesselQuery.csproj -c Release -o publish_cmd

dotnet publish Vtmis.Cluster.VesselApi/Vtmis.Cluster.VesselApi.csproj -c Release -o publish_cmd

dotnet publish Vtmis.Api.MapTracking/Vtmis.Api.MapTracking.csproj -c Release -o publish_cmd

dotnet publish Vtmis.WebVessel.Tracking/Vtmis.WebVessel.Tracking.csproj -c Release -o publish_cmd

sleep 3

docker-compose -f ".\docker-compose.staging.yml" up --build