﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.WebVessel.Tracking.Services.Concrete;
using Xunit;

namespace Vtmis.MapTracking.Integration.Tests.Services
{
    public class CorrectData : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
             new object[]
                {
                    new DateTime(2017,8,31),
                    new DateTime(2017,8,31)
                },
                new object[]
                {
                    new DateTime(2017,8,31),
                    new DateTime(2017,8,31),
                    MetHydroDataType.Air
                },
                new object[]
                {
                    new DateTime(2017,8,31),
                    new DateTime(2017,8,31),
                    MetHydroDataType.Water
                }
        };
        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class CorrectData2 : IEnumerable<object[]>
    {
        private readonly List<object[]> _data = new List<object[]>
        {
             new object[]
                {
                    995251951,
                    new DateTime(2017,8,31),
                    new DateTime(2017,8,31)
                },
                new object[]
                {
                    5631129,
                    new DateTime(2017,8,31),
                    new DateTime(2017,8,31),
                    MetHydroDataType.Air
                },
                new object[]
                {
                    5631130,
                    new DateTime(2017,8,31),
                    new DateTime(2017,8,31),
                    MetHydroDataType.Water
                }
        };
        public IEnumerator<object[]> GetEnumerator() => _data.GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }

    public class MethydroService_Test
    {
        private readonly IMethydroService _service;
        public MethydroService_Test()
        {
            //_service = new MethydroService();
        }

        [Theory]
        [ClassData(typeof(CorrectData))]
        public async Task Should_Get_All_Instruments_Data_ByDateRange(DateTime fromDate, DateTime toDate, MetHydroDataType? dataType = null)
        {
            var result = await _service.GetInstrumentDataByDateRangeAsync(fromDate, toDate, dataType);
            var waterData = result.FirstOrDefault().Methydros.FirstOrDefault().Instruments.MetWaterData;
            var airData = result.FirstOrDefault().Methydros.FirstOrDefault().Instruments.MetAirData;

            Assert.True(result.Any());
            if (dataType == null)
            {
                Assert.NotNull(waterData);
                Assert.NotNull(airData);
            }
            if (dataType == MetHydroDataType.Air)
            {
                Assert.Null(waterData);
                Assert.NotNull(airData);
            }
            if (dataType == MetHydroDataType.Water)
            {
                Assert.NotNull(waterData);
                Assert.Null(airData);
            }
        }

        [Fact]
        public async Task Should_Throw_Exception_If_Date_Supplied_Is_Higher_Than_Current_date()
        {
            await Assert.ThrowsAsync<Exception>(() =>
                _service.GetInstrumentDataByDateRangeAsync(new DateTime(3000, 1, 1), new DateTime(3000, 1, 2)));

            await Assert.ThrowsAsync<Exception>(() =>
                _service.GetInstrumentDataByMmsiDateRangeAsync(111111, new DateTime(3000, 1, 1), new DateTime(3000, 1, 2)));
        }

        [Fact]
        public async Task Should_Throw_Exception_If_FromDate_Is_Higher_Than_ToDate()
        {
            await Assert.ThrowsAsync<Exception>(() =>
                _service.GetInstrumentDataByDateRangeAsync(new DateTime(2017, 10, 1), new DateTime(2017, 1, 2)));

            await Assert.ThrowsAsync<Exception>(() =>
                _service.GetInstrumentDataByMmsiDateRangeAsync(222222, new DateTime(2017, 10, 1), new DateTime(2017, 1, 2)));
        }

        [Theory]
        [ClassData(typeof(CorrectData2))]
        public async Task Should_Get_Instrument_Data_By_MMSI_And_DateRange(int mmsi, DateTime fromDate, DateTime toDate,
            MetHydroDataType? metHydroDataType = null)
        {
            var result = await _service.GetInstrumentDataByMmsiDateRangeAsync(mmsi, fromDate, toDate);
            var waterData = result.Instruments.FirstOrDefault().MetWaterData;
            var airData = result.Instruments.FirstOrDefault().MetAirData;

            Assert.NotNull(result);
            Assert.True(result.Instruments.Any());
            if (metHydroDataType == null)
            {
                Assert.NotNull(waterData);
                Assert.NotNull(airData);
            }
            if (metHydroDataType == MetHydroDataType.Air)
            {
                Assert.Null(waterData);
                Assert.NotNull(airData);
            }
            if (metHydroDataType == MetHydroDataType.Water)
            {
                Assert.NotNull(waterData);
                Assert.Null(airData);
            }
        }
    }
}
