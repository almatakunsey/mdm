﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.DTO.Target;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.WebVessel.Tracking.Services.Concrete;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using Xunit;

namespace Vtmis.MapTracking.Integration.Tests
{
    public class AisMapTrackingService_Test
    {
        private readonly IAisMapTrackingService _aisMapTrackingService;
        private readonly VesselDatabaseContext _vesselRepo;
        private const string _dbName = "AIS170830";
        private const string _environmentName = Core.Common.Constants.HostingEnvironment.LOCAL;
        private const string _serverName = "localhost";
        private const string _aisDbUserId = "vessel_sa";
        private const string _aisDbPassword = "P@ssw0rd123";
        private readonly DateTime _startTime = Convert.ToDateTime("2017-08-30 08:00:00.000");
        public AisMapTrackingService_Test()
        {
            _vesselRepo = new VesselDatabaseContext(_dbName, _serverName,
               _aisDbUserId, _aisDbPassword);
            _aisMapTrackingService = new AisMapTrackingService();
        }

        [Fact]
        public async Task Should_Throw_Exception_When_Target_Not_Found()
        {
            var aisDbInfo = CommonHelper.GetAisDbInfo(_environmentName, _startTime, _serverName,
                _aisDbUserId, _aisDbPassword);
            var result = await _aisMapTrackingService.GetTargetDetailsByShipPositionIdAsync(aisDbInfo, 99);
            Assert.Null(result);
        }

        [Fact]
        public void Should_Add_New_Coordinates_In_Target_Previous_Routes()
        {
            var targetRoutes = _aisMapTrackingService.AddTargetPreviousRoute("1", 1.2, 2.1,
                new List<VesselRoute>());

            var result = targetRoutes.Where(x => x.VesselId == "1");
            var routesResult = targetRoutes.Where(x => x.VesselId == "1").FirstOrDefault().VesselRoutePosition;
            Assert.Single(result);
            Assert.Single(routesResult);
            Assert.Equal(1.2, routesResult.First.Value.Lat);
            Assert.Equal(2.1, routesResult.First.Value.Lgt);
            Assert.Equal(1.2, routesResult.Last.Value.Lat);
            Assert.Equal(2.1, routesResult.Last.Value.Lgt);
        }

        [Fact]
        public void Should_Add_New_Coordinate_Collection_In_Existing_Target_Previous_Routes()
        {
            var existingRoutes = _aisMapTrackingService.AddTargetPreviousRoute("1", 1.2, 2.1,
                new List<VesselRoute>());

            var targetRoutes = _aisMapTrackingService.AddTargetPreviousRoute("1", 11.2, 22.1,
                existingRoutes);

            var result = targetRoutes.Where(x => x.VesselId == "1");
            var routesResult = targetRoutes.Where(x => x.VesselId == "1").FirstOrDefault().VesselRoutePosition;
            Assert.Single(result);
            Assert.Equal(2, routesResult.Count());
            Assert.Equal(1.2, routesResult.First.Value.Lat);
            Assert.Equal(2.1, routesResult.First.Value.Lgt);
            Assert.Equal(11.2, routesResult.Last.Value.Lat);
            Assert.Equal(22.1, routesResult.Last.Value.Lgt);
        }

        [Fact]
        public void Should_Not_Add_New_Item_Collection_When_Lat_Long_Is_The_Same_As_Last_Coordinate_InCollection()
        {
            var existingRoutes = _aisMapTrackingService.AddTargetPreviousRoute("1", 1.2, 2.1,
                new List<VesselRoute>());

            var targetRoutes = _aisMapTrackingService.AddTargetPreviousRoute("1", 1.2, 2.1,
                existingRoutes);

            var result = targetRoutes.Where(x => x.VesselId == "1");
            var routesResult = targetRoutes.Where(x => x.VesselId == "1").FirstOrDefault().VesselRoutePosition;
            Assert.Single(result);
            Assert.Single(routesResult);
            Assert.Equal(1.2, routesResult.First.Value.Lat);
            Assert.Equal(2.1, routesResult.First.Value.Lgt);
            Assert.Equal(1.2, routesResult.Last.Value.Lat);
            Assert.Equal(2.1, routesResult.Last.Value.Lgt);
        }

        //max 10 items
        [Fact]
        public void Should_Remove_First_And_Add_New_As_Last_Coordinate_When_Route_Reach_Max_Collection()
        {
            var existingRoutes = _aisMapTrackingService.AddTargetPreviousRoute("1", 1.2, 2.1,
               new List<VesselRoute>());
            var existingRoutes2 = _aisMapTrackingService.AddTargetPreviousRoute("1", 11.2, 22.1,
               existingRoutes);
            var existingRoutes3 = _aisMapTrackingService.AddTargetPreviousRoute("1", 111.2, 222.1,
               existingRoutes2);
            var existingRoutes4 = _aisMapTrackingService.AddTargetPreviousRoute("1", 1111.2, 2222.1,
               existingRoutes3);
            var existingRoutes5 = _aisMapTrackingService.AddTargetPreviousRoute("1", 11111.2, 22222.1,
               existingRoutes4);
            var existingRoutes6 = _aisMapTrackingService.AddTargetPreviousRoute("1", 123.2, 223.1,
               existingRoutes5);
            var existingRoutes7 = _aisMapTrackingService.AddTargetPreviousRoute("1", 1345.2, 2234.1,
               existingRoutes6);
            var existingRoutes8 = _aisMapTrackingService.AddTargetPreviousRoute("1", 157.2, 2456.1,
               existingRoutes7);
            var existingRoutes9 = _aisMapTrackingService.AddTargetPreviousRoute("1", 1456.2, 276.1,
               existingRoutes8);
            var existingRoutes10 = _aisMapTrackingService.AddTargetPreviousRoute("1", 1364.2, 22545.1,
               existingRoutes9);


            var targetRoutes = _aisMapTrackingService.AddTargetPreviousRoute("1", 99.2, 22545.1,
                existingRoutes10);

            var result = targetRoutes.Where(x => x.VesselId == "1");
            var routesResult = targetRoutes.Where(x => x.VesselId == "1").FirstOrDefault().VesselRoutePosition;
            Assert.Single(result);
            Assert.Equal(10, routesResult.Count());
            Assert.False(routesResult.First.List.Where(x => x.Lat == 1.2 && x.Lgt == 2.1).Any());
            Assert.True(routesResult.Last.List.Where(x => x.Lat == 99.2 && x.Lgt == 22545.1).Any());
        }
    }
}
