﻿using Dapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vtmis.Core.ActorModel.Actors.Alarm;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Constants.Alarm;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.AkkaModel.Messages.Alarm;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.WebAdmin.ReadOnlyDB;
using Xunit;
using Xunit.Abstractions;

namespace Vtmis.MapTracking.Integration.Tests.Services.Alarm
{
    public class AlarmService_Test
    {
        private readonly MdmAdminDbContext _mdmAdminDb;
        private readonly ITestOutputHelper _output;

        public AlarmService_Test(ITestOutputHelper output)
        {
            _mdmAdminDb = new MdmAdminDbContext("Server=.,1433; Database=Vtmis_WebAdminDb;User Id=vessel_sa;Password=P@ssw0rd123; Trusted_Connection=False;");
            _output = output;
        }


        [Fact]
        public async Task Should_Generate_SQL()
        {
            try
            {
                var eventId = 10019;
                var alarmEvent = _mdmAdminDb.Events.AsNoTracking().Include(i => i.Alarm).ThenInclude(t => t.AlarmItems).FirstOrDefault(x => x.Id == eventId && x.Kill == false);
                var expWhere = "";
                var charCounter = 1;
                var isCharUsed = new List<UseChar>();
                var isCharUsedForCond2 = new List<UseChar>();
                var declareExpression = "";
                var joinAtonPosData = "";
                var joinAtonMonData = "";
                var joinAtonMetData = "";
                var joinDraught = "";
                //var globalWhere = "";
                foreach (var item in alarmEvent.Alarm.AlarmItems)
                {
                    var usableChar = isCharUsed.Where(x => x.IsUsed == false).OrderBy(o => o.Character).ToList();
                    var usableCharForCond2 = isCharUsedForCond2.Where(x => x.IsUsed == false).OrderBy(o => o.Character).ToList();
                    var internalWhere = "";
                    var internalDeclareExp = "";

                    //Start OR AND Condtion
                    if (item.Condition == Core.Common.Enums.Condition.OR)
                    {
                        if (!string.IsNullOrWhiteSpace(expWhere) && !string.Equals(item.ColumnType, "Zone", StringComparison.CurrentCultureIgnoreCase))
                        {
                            expWhere = $"{expWhere} OR ";
                        }
                    }
                    else if (item.Condition == Core.Common.Enums.Condition.AND)
                    {
                        if (!string.IsNullOrWhiteSpace(expWhere) && !string.Equals(item.ColumnType, "Zone", StringComparison.CurrentCultureIgnoreCase))
                        {
                            expWhere = $"{expWhere} AND ";
                        }
                    }
                    //End OR AND Condtion

                    //Start Zone
                    if (string.Equals(item.ColumnType, "Zone", StringComparison.CurrentCultureIgnoreCase))
                    {
                        var zone = await _mdmAdminDb.Zones.AsNoTracking().Include(i => i.ZoneItems)
                                      .FirstOrDefaultAsync(x => x.Id == Convert.ToInt32(item.ColumnValue));

                        if (zone.ZoneType == ZoneType.Polygon)
                        {

                            var polygon = new List<string>();
                            foreach (var zoneItem in zone.ZoneItems)
                            {
                                polygon.Add($"{zoneItem.Logitude} {zoneItem.Latitude}");
                            }

                            internalDeclareExp = $"{internalDeclareExp} DECLARE @{charCounter.ToChar()} geography = geography::STGeomFromText('POLYGON(({string.Join(", ", polygon.ToArray())}))', 4326);";

                            isCharUsed.Add(new UseChar { Character = charCounter.ToChar() });
                        }
                        else if (zone.ZoneType == ZoneType.Circle)
                        {

                            internalDeclareExp = $"{internalDeclareExp} DECLARE @{charCounter.ToChar()} geography = geography::Point({zone.ZoneItems.First().Latitude},{zone.ZoneItems.First().Logitude},4326).STBuffer({zone.Radius.NMToMeters()});";

                            isCharUsed.Add(new UseChar { Character = charCounter.ToChar() });
                        }
                        else if (zone.ZoneType == ZoneType.Line_L || zone.ZoneType == ZoneType.Line_R)
                        {
                            var lineString = new List<string>();
                            foreach (var zoneItem in zone.ZoneItems)
                            {
                                lineString.Add($"{zoneItem.Logitude} {zoneItem.Latitude}");
                            }

                            internalDeclareExp = $"{internalDeclareExp} DECLARE @{charCounter.ToChar()} geography = geography::STGeomFromText('LINESTRING({string.Join(", ", lineString.ToArray())})', 4326);";

                            isCharUsed.Add(new UseChar { Character = charCounter.ToChar(), IsLineString = true });
                        }

                    }
                    //End Zone

                    //Start Condition 1
                    if (string.Equals(item.ColumnType, Condition1.Self, StringComparison.CurrentCultureIgnoreCase))
                    {
                        // Start InZone
                        if (string.Equals(item.ColumnValue, Condition1.InZone, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var selectedChar = usableChar.FirstOrDefault();
                            if (selectedChar != null)
                            {
                                usableCharForCond2.Add(selectedChar);
                                usableChar.Remove(selectedChar);
                            }
                            var intersect = $"geography::Point(m.Latitude, m.Longitude, 4326)";
                            if (selectedChar.IsLineString)
                            {
                                intersect = $"geography::STGeomFromText('LINESTRING('+ CAST(CAST(m.Longitude AS float) AS varchar) +' '+ CAST(CAST(m.Latitude AS float) AS varchar) +', '+ CAST(CAST(m.PrevLong AS float) AS varchar) +' '+ CAST(CAST(m.PrevLat AS float) AS varchar) +')', 4326)";
                            }
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(@{selectedChar.Character}.STIntersects({intersect})=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (@{selectedChar.Character}.STIntersects({intersect})=1)";
                            }
                        }
                        // End InZone

                        // Start NotInZone
                        else if (string.Equals(item.ColumnValue, Condition1.NotInZone, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var selectedChar = usableChar.FirstOrDefault();
                            if (selectedChar != null)
                            {
                                usableCharForCond2.Add(selectedChar);
                                usableChar.Remove(selectedChar);
                            }
                            var intersect = $"geography::Point(m.Latitude, m.Longitude, 4326)";
                            if (selectedChar.IsLineString)
                            {

                                intersect = $"geography::STGeomFromText('LINESTRING('+ CAST(CAST(m.Longitude AS float) AS varchar) +' '+ CAST(CAST(m.Latitude AS float) AS varchar) +', '+ CAST(CAST(m.PrevLong AS float) AS varchar) +' '+ CAST(CAST(m.PrevLat AS float) AS varchar) +')', 4326)";
                            }
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(@{selectedChar.Character}.STIntersects({intersect})=0)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (@{selectedChar.Character}.STIntersects({intersect})=0)";
                            }
                        }
                        // End NotInZone

                        // Start OnEnter
                        else if (string.Equals(item.ColumnValue, Condition1.OnEnter, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var selectedChar = usableChar.FirstOrDefault();
                            if (selectedChar != null)
                            {
                                usableCharForCond2.Add(selectedChar);
                                usableChar.Remove(selectedChar);
                            }
                            var intersect = $"geography::Point(m.Latitude, m.Longitude, 4326)";

                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(@{selectedChar.Character}.STIntersects({intersect})=1 AND m.LocalRecvTime > '{alarmEvent.CreationTime.ToString("yyyy-MM-dd HH:mm:ss")}')";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (@{selectedChar.Character}.STIntersects({intersect})=1 AND m.LocalRecvTime > '{alarmEvent.CreationTime.ToString("yyyy-MM-dd HH:mm:ss")}')";
                            }
                        }
                        // End OnEnter

                        // Start OnExit
                        else if (string.Equals(item.ColumnValue, Condition1.OnExit, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var selectedChar = usableChar.FirstOrDefault();
                            if (selectedChar != null)
                            {
                                usableCharForCond2.Add(selectedChar);
                                usableChar.Remove(selectedChar);
                            }
                            var prevIntersect = $"geography::Point(m.PrevLat, m.PrevLong, 4326)";
                            var intersect = $"geography::Point(m.Latitude, m.Longitude, 4326)";
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(@{selectedChar.Character}.STIntersects({prevIntersect})=1 AND @{selectedChar.Character}.STIntersects({intersect})=0 AND m.LocalRecvTime > {alarmEvent.CreationTime.ToString()})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (@{selectedChar.Character}.STIntersects({prevIntersect})=1 AND @{selectedChar.Character}.STIntersects({intersect})=0 AND m.LocalRecvTime > {alarmEvent.CreationTime.ToString()})";
                            }
                        }
                        // End OnExit

                        // Start OnEnter/Exit
                        else if (string.Equals(item.ColumnValue, Condition1.OnEnterExit, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var selectedChar = usableChar.FirstOrDefault();
                            if (selectedChar != null)
                            {
                                usableCharForCond2.Add(selectedChar);
                                usableChar.Remove(selectedChar);
                            }
                            var prevIntersect = $"geography::Point(m.PrevLat, m.PrevLong, 4326)";
                            var intersect = $"geography::Point(m.Latitude, m.Longitude, 4326)";
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"((@{selectedChar.Character}.STIntersects({prevIntersect})=1 AND @{selectedChar.Character}.STIntersects({intersect})=0 AND m.LocalRecvTime > {alarmEvent.CreationTime.ToString()}) OR (@{selectedChar.Character}.STIntersects({intersect})=1 AND m.LocalRecvTime > {alarmEvent.CreationTime.ToString("yyyy-MM-dd HH:MM:ss")}))";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} ((@{selectedChar.Character}.STIntersects({prevIntersect})=1 AND @{selectedChar.Character}.STIntersects({intersect})=0 AND m.LocalRecvTime > {alarmEvent.CreationTime.ToString()}) OR (@{selectedChar.Character}.STIntersects({intersect})=1 AND m.LocalRecvTime > {alarmEvent.CreationTime.ToString("yyyy-MM-dd HH:MM:ss")}))";
                            }
                        }
                        // End OnEnter/Exit

                        // Start Light Off
                        else if (string.Equals(item.ColumnValue, Condition1.LightOff, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(ap.Light21=2 OR am.Light=2)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (ap.Light21=2 OR am.Light=2)";
                            }
                        }
                        // End Light Off

                        // Start Light Err
                        else if (string.Equals(item.ColumnValue, Condition1.LightError, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(ap.Light21=3 OR am.Light=3)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (ap.Light21=3 OR am.Light=3)";
                            }
                        }
                        // End Light Err

                        // Start RACON Err
                        else if (string.Equals(item.ColumnValue, Condition1.RaconError, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(ap.RACON21=3 OR am.RACON=3)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (ap.RACON21=3 OR am.RACON=3)";
                            }
                        }
                        // End RACON Err

                        // Start Health Alarm
                        else if (string.Equals(item.ColumnValue, Condition1.HealthAlarm, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(ap.Health21=1 OR am.Health=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (ap.Health21=1 OR am.Health=1)";
                            }
                        }
                        // End Health Alarm

                        // Start OffPosition
                        else if (string.Equals(item.ColumnValue, Condition1.OffPosition, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(ap.OffPosition21=1 OR am.OffPosition68=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (ap.OffPosition21=1 OR am.OffPosition68=1)";
                            }
                        }
                        // End OffPosition

                        // Start bit0
                        else if (string.Equals(item.ColumnValue, Condition1.Bit0, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(am.B0=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (am.B0=1)";
                            }
                        }
                        // End bit0

                        // Start bit1
                        else if (string.Equals(item.ColumnValue, Condition1.Bit1, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(am.B1=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (am.B1=1)";
                            }
                        }
                        // End bit1

                        // Start bit2
                        else if (string.Equals(item.ColumnValue, Condition1.Bit2, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(am.B2=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (am.B2=1)";
                            }
                        }
                        // End bit2

                        // Start bit3
                        else if (string.Equals(item.ColumnValue, Condition1.Bit3, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(am.B3=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (am.B3=1)";
                            }
                        }
                        // End bit3

                        // Start bit4
                        else if (string.Equals(item.ColumnValue, Condition1.Bit4, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(am.B4=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (am.B4=1)";
                            }
                        }
                        // End bit4

                        // Start bit5
                        else if (string.Equals(item.ColumnValue, Condition1.Bit5, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(am.B5=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (am.B5=1)";
                            }
                        }
                        // End bit5

                        // Start bit6
                        else if (string.Equals(item.ColumnValue, Condition1.Bit6, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(am.B6=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (am.B6=1)";
                            }
                        }
                        // End bit6

                        // Start bit7
                        else if (string.Equals(item.ColumnValue, Condition1.Bit7, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(am.B7=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (am.B7=1)";
                            }
                        }
                        // End bit7

                        // Start Cover Open
                        else if (string.Equals(item.ColumnValue, Condition1.CoverOpen, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(d.Cover=1)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (d.Cover=1)";
                            }
                        }
                        // End Cover Open

                        // Start Cover Closed
                        else if (string.Equals(item.ColumnValue, Condition1.CoverClose, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(d.Cover=0)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (d.Cover=0)";
                            }
                        }
                        // End Cover Closed

                        // Start Sonar Error
                        else if (string.Equals(item.ColumnValue, Condition1.SonarError, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = "(d.Sonar=2)";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (d.Cover=2)";
                            }
                        }
                        // End Sonar Error
                    }
                    // End Condition 1

                    //Start Condition 2
                    if (string.Equals(item.ColumnType, Condition2.Self, StringComparison.CurrentCultureIgnoreCase))
                    {
                        var op = "=";
                        if (string.Equals(item.Operation, "<>", StringComparison.CurrentCultureIgnoreCase))
                        {
                            op = "<>";
                        }
                        else if (string.Equals(item.Operation, Operation.MORE_THAN, StringComparison.CurrentCultureIgnoreCase))
                        {
                            op = Operation.MORE_THAN;
                        }
                        else if (string.Equals(item.Operation, Operation.MORE_THAN_OR_EQUALS_TO, StringComparison.CurrentCultureIgnoreCase))
                        {
                            op = Operation.MORE_THAN_OR_EQUALS_TO;
                        }
                        else if (string.Equals(item.Operation, Operation.LESS_THAN, StringComparison.CurrentCultureIgnoreCase))
                        {
                            op = Operation.LESS_THAN;
                        }
                        else if (string.Equals(item.Operation, Operation.LESS_THAN_OR_EQUALS_TO, StringComparison.CurrentCultureIgnoreCase))
                        {
                            op = Operation.LESS_THAN_OR_EQUALS_TO;
                        }

                        //Start Speed
                        if (string.Equals(item.ColumnItem, Condition2.Speed, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(m.SOG {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(m.SOG {op} {item.ColumnValue})";
                            }
                        }
                        //End Speed

                        // Start Wind Speed
                        else if (string.Equals(item.ColumnItem, Condition2.WindSpeed, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(met.WndSpeed {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(met.WndSpeed {op} {item.ColumnValue})";
                            }
                        }
                        // End Wind Speed

                        // Start Wind Gust
                        else if (string.Equals(item.ColumnItem, Condition2.WindGust, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(met.WndGust {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(met.WndGust {op} {item.ColumnValue})";
                            }
                        }
                        // End Wind Gust

                        // Start Air Pressure
                        else if (string.Equals(item.ColumnItem, Condition2.AirPressure, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(met.AirPres {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(met.AirPres {op} {item.ColumnValue})";
                            }
                        }
                        // End Air Pressure

                        // Start Air Temperature
                        else if (string.Equals(item.ColumnItem, Condition2.AirTemperature, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(met.AirTemp {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(met.AirTemp {op} {item.ColumnValue})";
                            }
                        }
                        // End Air Temperature

                        // Start Analogue Int
                        else if (string.Equals(item.ColumnItem, Condition2.AnalogueInt, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(am.AnalogueInt {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(am.AnalogueInt {op} {item.ColumnValue})";
                            }
                        }
                        // End Analogue Int

                        // Start Analogue External 1
                        else if (string.Equals(item.ColumnItem, Condition2.AnalogueExternal1, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(am.AnalogueExt1 {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(am.AnalogueExt1 {op} {item.ColumnValue})";
                            }
                        }
                        // End Analogue External 1

                        // Start Analogue External 2
                        else if (string.Equals(item.ColumnItem, Condition2.AnalogueExternal2, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(am.AnalogueExt2 {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(am.AnalogueExt2 {op} {item.ColumnValue})";
                            }
                        }
                        // End Analogue External 2

                        // Start Voltage
                        else if (string.Equals(item.ColumnItem, Condition2.Voltage, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(am.VoltageData {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(am.VoltageData {op} {item.ColumnValue})";
                            }
                        }
                        // End Voltage

                        // Start Current
                        else if (string.Equals(item.ColumnItem, Condition2.Current, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(am.CurrentData {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(am.CurrentData {op} {item.ColumnValue})";
                            }
                        }
                        // End Current

                        // Start TimeToEnter
                        else if (string.Equals(item.ColumnItem, Condition2.TimeToEnter, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var selectedChar = usableCharForCond2.FirstOrDefault();
                            if (selectedChar != null)
                            {
                                usableCharForCond2.Remove(selectedChar);
                            }
                            var intersect = $"geography::Point(m.Latitude, m.Longitude, 4326)";

                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(@{selectedChar.Character}.STIntersects({intersect})=1 AND m.LocalRecvTime > {alarmEvent.CreationTime.ToString("yyyy-MM-dd HH:MM:ss")} AND m.LocalRecvTime {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (@{selectedChar.Character}.STIntersects({intersect})=1 AND m.LocalRecvTime > {alarmEvent.CreationTime.ToString("yyyy-MM-dd HH:MM:ss")} AND m.LocalRecvTime {op} {item.ColumnValue})";
                            }
                        }
                        // End TimeToEnter

                        // Start TimeToExit
                        else if (string.Equals(item.ColumnItem, Condition2.TimeToExit, StringComparison.CurrentCultureIgnoreCase))
                        {
                            var selectedChar = usableCharForCond2.FirstOrDefault();
                            if (selectedChar != null)
                            {
                                usableChar.Remove(selectedChar);
                            }
                            var prevIntersect = $"geography::Point(m.PrevLat, m.PrevLong, 4326)";
                            var intersect = $"geography::Point(m.Latitude, m.Longitude, 4326)";
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(@{selectedChar.Character}.STIntersects({prevIntersect})=1 AND @{selectedChar.Character}.STIntersects({intersect})=0 AND m.LocalRecvTime > {alarmEvent.CreationTime.ToString()} AND m.LocalRecvTime {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere} (@{selectedChar.Character}.STIntersects({prevIntersect})=1 AND @{selectedChar.Character}.STIntersects({intersect})=0 AND m.LocalRecvTime > {alarmEvent.CreationTime.ToString()} AND m.LocalRecvTime {op} {item.ColumnValue})";
                            }
                        }
                        // End TimeToExit

                        // Start Course Change
                        else if (string.Equals(item.ColumnItem, Condition2.CourseChange, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(m.COG {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(m.COG {op} {item.ColumnValue})";
                            }
                        }
                        // End Course Change

                        // Start Time
                        else if (string.Equals(item.ColumnItem, Condition2.Time, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(m.LocalRecvTime {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(m.LocalRecvTime {op} {item.ColumnValue})";
                            }
                        }
                        // End Time

                        // Start UTCTime
                        else if (string.Equals(item.ColumnItem, Condition2.Time, StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (string.IsNullOrWhiteSpace(internalWhere))
                            {
                                internalWhere = $"(m.RecvTime {op} {item.ColumnValue})";
                            }
                            else
                            {
                                internalWhere = $"{internalWhere}(m.RecvTime {op} {item.ColumnValue})";
                            }
                        }
                        // End UTCTime
                    }
                    //End Condition 2

                    //Start Filter
                    if (string.Equals(item.ColumnType, "Filter"))
                    {
                        var filter = await _mdmAdminDb.Filters.AsNoTracking().Include("FilterItems").Include("FilterItems.FilterDetails")
                                      .FirstOrDefaultAsync(x => x.Id == Convert.ToInt32(item.ColumnValue));

                        if (filter.FilterItems.Any())
                        {
                            var filterSpecExp = "";
                            foreach (var filterItem in filter.FilterItems)
                            {
                                if (filterItem.FilterDetails.Any())
                                {
                                    var filterCriteriaExp = "";
                                    foreach (var filterDetail in filterItem.FilterDetails)
                                    {
                                        var internalCriteriaExp = "";
                                        if (!string.IsNullOrWhiteSpace(filterCriteriaExp))
                                        {
                                            filterCriteriaExp = $"{filterCriteriaExp} OR ";
                                        }
                                        //Start ShipName
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.ShipName, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                            {
                                                internalCriteriaExp = $"(ss.Name {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)})";
                                            }
                                            else
                                            {
                                                internalCriteriaExp = $"{internalCriteriaExp} (ss.Name {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)})";
                                            }
                                        }
                                        //End ShipName

                                        //Start MMSI
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.MMSI, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                            {
                                                internalCriteriaExp = $"(m.MMSI {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)})";
                                            }
                                            else
                                            {
                                                internalCriteriaExp = $"{internalCriteriaExp} (m.MMSI {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)})";
                                            }
                                        }
                                        //End MMSI

                                        //Start CallSign
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.CallSign, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                            {
                                                internalCriteriaExp = $"(ss.CallSign {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)})";
                                            }
                                            else
                                            {
                                                internalCriteriaExp = $"{internalCriteriaExp} (ss.CallSign {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)})";
                                            }
                                        }
                                        //End CallSign

                                        //Start Group Type
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.GroupType, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            var groupExp = "";
                                            var groupType = await _mdmAdminDb.FilterGroups.AsNoTracking().FirstOrDefaultAsync(x => x.Id == Convert.ToInt32(filterDetail.ColumnValue));
                                            var mmsiList = groupType.MMSIList.JsonArrayToList<string>();
                                            var callSignList = groupType.CallSignList.JsonArrayToList<string>();
                                            var nameList = groupType.NameList.JsonArrayToList<string>();

                                            if (mmsiList.Any())
                                            {
                                                var internalMMSIExp = "";
                                                foreach (var mmsi in mmsiList)
                                                {
                                                    if (string.IsNullOrWhiteSpace(internalMMSIExp))
                                                    {
                                                        internalMMSIExp = $"(m.MMSI {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)}";
                                                    }
                                                    else
                                                    {
                                                        internalMMSIExp = $"{internalMMSIExp} OR m.MMSI {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)}";
                                                    }
                                                }
                                                if (string.IsNullOrWhiteSpace(groupExp))
                                                {
                                                    groupExp = $"{internalMMSIExp})";
                                                }
                                                else
                                                {
                                                    groupExp = $"{groupExp} OR {internalMMSIExp})";
                                                }
                                            }

                                            if (callSignList.Any())
                                            {
                                                var internalcallSignExp = "";
                                                foreach (var callSign in callSignList)
                                                {
                                                    if (string.IsNullOrWhiteSpace(internalcallSignExp))
                                                    {
                                                        internalcallSignExp = $"(ss.CallSign {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)}";
                                                    }
                                                    else
                                                    {
                                                        internalcallSignExp = $"{internalcallSignExp} OR ss.CallSign {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)}";
                                                    }
                                                }
                                                if (string.IsNullOrWhiteSpace(groupExp))
                                                {
                                                    groupExp = $"{internalcallSignExp})";
                                                }
                                                else
                                                {
                                                    groupExp = $"{groupExp} OR {internalcallSignExp})";
                                                }
                                            }

                                            if (nameList.Any())
                                            {
                                                var internalNameExp = "";
                                                foreach (var name in nameList)
                                                {
                                                    if (string.IsNullOrWhiteSpace(internalNameExp))
                                                    {
                                                        internalNameExp = $"(ss.Name {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)}";
                                                    }
                                                    else
                                                    {
                                                        internalNameExp = $"{internalNameExp} OR ss.Name {Operation.SQLOp(filterDetail.ColumnValue, filterDetail.Operation)}";
                                                    }
                                                }
                                                if (string.IsNullOrWhiteSpace(groupExp))
                                                {
                                                    groupExp = $"{internalNameExp})";
                                                }
                                                else
                                                {
                                                    groupExp = $"{groupExp} OR {internalNameExp})";
                                                }
                                            }

                                            if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                            {
                                                internalCriteriaExp = $"({groupExp}";
                                            }
                                            else
                                            {
                                                internalCriteriaExp = $"{internalCriteriaExp} ({groupExp}";
                                            }
                                        }
                                        //End Group Type

                                        //Start ShipType
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.ShipType, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            var shipType = TargetHelper.GetShipTypeIdByName(filterDetail.ColumnValue);
                                            if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                            {
                                                internalCriteriaExp = $"(ss.ShipType {Operation.SQLOp(shipType.ToString(), filterDetail.Operation)})";
                                            }
                                            else
                                            {
                                                internalCriteriaExp = $"{internalCriteriaExp} (ss.ShipType {Operation.SQLOp(shipType.ToString(), filterDetail.Operation)})";
                                            }
                                        }
                                        //End ShipType

                                        //Start NavStatus
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.NavStatus, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            var navStatus = TargetHelper.GetNavStatusIdByName(filterDetail.ColumnValue);
                                            if (navStatus >= 0)
                                            {
                                                if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                                {
                                                    internalCriteriaExp = $"(m.NavigationalStatus {Operation.SQLOp(navStatus.ToString(), filterDetail.Operation)})";
                                                }
                                                else
                                                {
                                                    internalCriteriaExp = $"{internalCriteriaExp} (m.NavigationalStatus {Operation.SQLOp(navStatus.ToString(), filterDetail.Operation)})";
                                                }
                                            }
                                        }
                                        //End NavStatus

                                        //Start AIS Type
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.AisType, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            //Start Class A
                                            if (string.Equals(filterDetail.ColumnValue, TargetClassConst.ClassA, StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                                {
                                                    internalCriteriaExp = $"(m.MessageId {Operation.SQLOp(1.ToString(), filterDetail.Operation)} OR m.MessageId {Operation.SQLOp(2.ToString(), filterDetail.Operation)} OR m.MessageId {Operation.SQLOp(3.ToString(), filterDetail.Operation)})";
                                                }
                                                else
                                                {
                                                    internalCriteriaExp = $"{internalCriteriaExp} (m.MessageId {Operation.SQLOp(1.ToString(), filterDetail.Operation)} OR m.MessageId {Operation.SQLOp(2.ToString(), filterDetail.Operation)} OR m.MessageId {Operation.SQLOp(3.ToString(), filterDetail.Operation)})";
                                                }
                                            }
                                            //End Class A

                                            //Start Class B
                                            if (string.Equals(filterDetail.ColumnValue, TargetClassConst.ClassB, StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                                {
                                                    internalCriteriaExp = $"(m.MessageId {Operation.SQLOp(18.ToString(), filterDetail.Operation)})";
                                                }
                                                else
                                                {
                                                    internalCriteriaExp = $"{internalCriteriaExp} (m.MessageId {Operation.SQLOp(18.ToString(), filterDetail.Operation)})";
                                                }
                                            }
                                            //End Class B

                                            //Start Aton
                                            if (string.Equals(filterDetail.ColumnValue, TargetClassConst.AtoN, StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                var op = "NOT EXISTS";
                                                if (string.Equals(filterDetail.Operation, Operation.EQUALS_TO, StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    op = "EXISTS";
                                                }
                                                else
                                                {
                                                    op = "NOT EXISTS";
                                                }
                                                if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                                {
                                                    internalCriteriaExp = $"({op}(SELECT TOP 1 e.MMSI FROM AtonPosData e WHERE e.MMSI=m.MMSI))";
                                                }
                                                else
                                                {
                                                    internalCriteriaExp = $"{internalCriteriaExp} ({op}(SELECT TOP 1 e.MMSI FROM AtonPosData e WHERE e.MMSI=m.MMSI))";
                                                }
                                            }
                                            //End Aton

                                            //Start SART
                                            if (string.Equals(filterDetail.ColumnValue, TargetClassConst.SART, StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                var op = "NOT EXISTS";
                                                if (string.Equals(filterDetail.Operation, Operation.EQUALS_TO, StringComparison.CurrentCultureIgnoreCase))
                                                {
                                                    op = "EXISTS";
                                                }
                                                else
                                                {
                                                    op = "NOT EXISTS";
                                                }
                                                if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                                {
                                                    internalCriteriaExp = $"({op}(SELECT TOP 1 e.Name FROM ShipStatic e WHERE e.MMSI LIKE '%SART%')";
                                                }
                                                else
                                                {
                                                    internalCriteriaExp = $"{internalCriteriaExp} ({op}(SELECT TOP 1 e.Name FROM ShipStatic e WHERE e.MMSI LIKE '%SART%')";
                                                }
                                            }
                                            //End SART

                                            //Start Base Station
                                            if (string.Equals(filterDetail.ColumnValue, "Base St", StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                                {
                                                    internalCriteriaExp = $"(m.MessageId {Operation.SQLOp(4.ToString(), filterDetail.Operation)}";
                                                }
                                                else
                                                {
                                                    internalCriteriaExp = $"{internalCriteriaExp} (m.MessageId {Operation.SQLOp(4.ToString(), filterDetail.Operation)}";
                                                }
                                            }
                                            //End Base Station

                                            //Start SAR
                                            if (string.Equals(filterDetail.ColumnValue, TargetClassConst.SAR_Air, StringComparison.CurrentCultureIgnoreCase))
                                            {
                                                if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                                {
                                                    internalCriteriaExp = $"(m.MessageId {Operation.SQLOp(9.ToString(), filterDetail.Operation)}";
                                                }
                                                else
                                                {
                                                    internalCriteriaExp = $"{internalCriteriaExp} (m.MessageId {Operation.SQLOp(9.ToString(), filterDetail.Operation)}";
                                                }
                                            }
                                            //End SAR
                                        }
                                        //End AIS Type

                                        //Start Min Speed
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.MinSpeed, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                            {
                                                internalCriteriaExp = $"(m.SOG >= {filterDetail.ColumnValue})";
                                            }
                                            else
                                            {
                                                internalCriteriaExp = $"{internalCriteriaExp} (m.SOG >= {filterDetail.ColumnValue})";
                                            }
                                        }
                                        //End Min Speed

                                        //Start Min Course
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.MinCourse, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                            {
                                                internalCriteriaExp = $"(m.COG >= {filterDetail.ColumnValue})";
                                            }
                                            else
                                            {
                                                internalCriteriaExp = $"{internalCriteriaExp} (m.COG >= {filterDetail.ColumnValue})";
                                            }
                                        }
                                        //End Min Course

                                        //Start Max Course
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.MinCourse, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                            {
                                                internalCriteriaExp = $"(m.COG <= {filterDetail.ColumnValue})";
                                            }
                                            else
                                            {
                                                internalCriteriaExp = $"{internalCriteriaExp} (m.COG <= {filterDetail.ColumnValue})";
                                            }
                                        }
                                        //End Max Course

                                        //Start Min Age
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.MinAge, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                            {
                                                internalCriteriaExp = $"(DATEDIFF(minute, m.LocalRecvTime, '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}') >= {filterDetail.ColumnValue})";
                                            }
                                            else
                                            {
                                                internalCriteriaExp = $"{internalCriteriaExp} (DATEDIFF(minute, m.LocalRecvTime, '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}') >= {filterDetail.ColumnValue})";
                                            }
                                        }
                                        //End Min Age

                                        //Start Max Age
                                        if (string.Equals(filterDetail.ColumnName, FilterFieldsConst.MaxAge, StringComparison.CurrentCultureIgnoreCase))
                                        {
                                            if (string.IsNullOrWhiteSpace(internalCriteriaExp))
                                            {
                                                internalCriteriaExp = $"(DATEDIFF(minute, m.LocalRecvTime, '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}') <= {filterDetail.ColumnValue})";
                                            }
                                            else
                                            {
                                                internalCriteriaExp = $"{internalCriteriaExp} (DATEDIFF(minute, m.LocalRecvTime, '{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}') <= {filterDetail.ColumnValue})";
                                            }
                                        }
                                        //End Max Age

                                        filterCriteriaExp = $"{filterCriteriaExp} {internalCriteriaExp}";
                                    }
                                    filterSpecExp = $"{filterSpecExp} {filterCriteriaExp}";

                                }
                            }
                            if (string.IsNullOrWhiteSpace(filterSpecExp))
                            {
                                internalWhere = $"({filterSpecExp})";
                            }
                            else
                            {
                                internalWhere = $"({internalWhere}{filterSpecExp})";
                            }
                        }

                    }
                    //End Filter

                    declareExpression = $"{declareExpression} {internalDeclareExp}";
                    expWhere = $"{expWhere} {internalWhere}";
                    charCounter += 1;
                }

                joinAtonPosData = expWhere.Contains("ap.") ? @"OUTER APPLY
								(
									SELECT TOP 1 iap.MMSI, iap.RecvTime, iap.VirtualAtoN, iap.OffPosition21, iap.Health21, iap.Light21, iap.RACON21
									FROM AtonPosData iap 
									WHERE iap.MMSI = m.MMSI AND iap.RecvTime <= m.RecvTime
									ORDER BY iap.RecvTime DESC
								) ap" : "";
                joinAtonMetData = expWhere.Contains("met.") ? @"OUTER APPLY
                                (
                                    SELECT TOP 1 imet.MMSI, imet.WndSpeed, imet.WndGust, imet.AirPres, imet.AirTemp
                                    FROM AtonMetData imet 
                                    WHERE imet.MMSI = m.MMSI AND imet.RecvTime <= m.RecvTime
                                    ORDER BY imet.RecvTime DESC
                                ) met" : "";
                joinAtonMonData = expWhere.Contains("am.") ? @"OUTER APPLY
                                (
                                    SELECT TOP 1 iam.MMSI, iam.RecvTime, iam.AnalogueExt1, iam.AnalogueExt2, iam.AnalogueInt, iam.OffPosition68, 
                                    iam.Health, iam.Light, iam.RACON, iam.DigitalInput,
                                    iam.B0, iam.B1, iam.B2, iam.B3, iam.B4, iam.B5, iam.B6, iam.B7, iam.VoltageData, iam.CurrentData,
                                    
                                    FROM AtonMonData iam 
                                    WHERE iam.MMSI = m.MMSI AND iam.RecvTime <= m.RecvTime
                                    ORDER BY iam.RecvTime DESC
                                ) am" : "";
                joinDraught = expWhere.Contains("d.") ? @"OUTER APPLY
                                (
                                    SELECT TOP 1 id.MMSI, id.Cover, id.Sonar
                                    FROM Draught id 
                                    WHERE id.MMSI = m.MMSI AND id.RecvTime <= m.RecvTime
                                    ORDER BY id.RecvTime DESC
                                ) d" : "";

                using (var aisDbContext = new SqlConnection($"Server=.,1433;Database=AIS170831;User Id=vessel_sa;Password=P@ssw0rd123;MultipleActiveResultSets=True;ConnectRetryCount=5"))
                {
                    Console.WriteLine($"Start query alarm event");
                    var qry = $@"
                                {declareExpression}
                                SELECT TOP 10 m.RecvTime, m.LocalRecvTime, m.Latitude, m.Longitude, m.MMSI, ss.Name FROM
                                    (SELECT sp.RecvTime, sp.LocalRecvTime, sp.SOG, sp.COG, sp.Latitude, sp.Longitude, sp.MMSI, sp.ROT, sp.TrueHeading, sp.MessageId, sp.NavigationalStatus,
                                        (SELECT TOP 1 Latitude FROM ShipPosition WHERE MMSI=sp.MMSI AND RecvTime<sp.RecvTime ORDER BY sp.RecvTime DESC) PrevLat, 
									(SELECT TOP 1 Longitude FROM ShipPosition WHERE MMSI=sp.MMSI AND RecvTime<sp.RecvTime ORDER BY sp.RecvTime DESC) PrevLong 
                                    FROM (
                                        SELECT x.MMSI, MAX(x.RecvTime) RecvTime FROM ShipPosition x GROUP BY MMSI
                                    ) g
                                    INNER JOIN ShipPosition sp ON g.MMSI=sp.MMSI
                                    WHERE sp.RecvTime=g.RecvTime) m
                                 {joinAtonPosData}
                                {joinAtonMonData}
                                {joinDraught}
                                {joinAtonMetData}
                                OUTER APPLY
                                (
                                    SELECT TOP 1 iss.MMSI, iss.Name, iss.ShipType, iss.CallSign, iss.IMO
                                    FROM ShipStatic iss 
                                    WHERE iss.MMSI = m.MMSI
                                ) ss
                                WHERE {expWhere}
                            ";
                    var result = aisDbContext.Query<QueryAlarmTrigger>(qry);

                    Console.WriteLine($"end query alarm event");
                    //alarmEvent.EventDetails.AddRange(qry);
                    //_mdmAdminDb.Events.Update(alarmEvent);
                    //await _mdmAdminDb.SaveChangesAsync();

                    var r = Newtonsoft.Json.JsonConvert.SerializeObject(result);
                    _output.WriteLine(r);
                    _output.WriteLine(result.Count().ToString());
                }
            }
            catch (Exception err)
            {
                throw err;
            }
        }
    }
}
