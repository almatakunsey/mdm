﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.VesselEntityFrameworkCore;
using Vtmis.Core.VesselEntityFrameworkCore.Models.Vtmis;
using Vtmis.WebAdmin.Zones;
using Xunit;
using Xunit.Abstractions;

namespace Vtmis.MapTracking.Integration.Tests
{
  
    public class Expression_Test
    {
        private readonly ITestOutputHelper _output;

        public Expression_Test(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void Shoud_Deserialize_Json()
        {
            var a = "[1,2,3]".JsonArrayToList<string>();
        }

        [Fact]
        public void Should_Return_Char_Incremently()
        {
            for (int i = 1; i <= 50; i++)
            {
                _output.WriteLine(i.ToChar());
            }
            
        }
       

        [Fact]
        public void Should_Filter_MMSI()
        {
            using (var db = new VesselDatabaseContext("AIS170831",".","vessel_sa","P@ssw0rd123"))
            {
                Expression<Func<SP, bool>> expression = x => x.MMSI == 563281000;
                //var polygon = new List<PointF>();
                //var zones = new List<ZoneItem> {
                //    new ZoneItem
                //    {
                //        Latitude = 7.1117946 , Logitude = 99.35897827
                //    },
                //    new ZoneItem{ Latitude = 7.11043187, Logitude = 99.41665649  },
                //    new ZoneItem{ Latitude = 7.06954817, Logitude = 99.42626953  },
                //    new ZoneItem{ Latitude = 7.04092742, Logitude = 99.40841675  },
                //    new ZoneItem{ Latitude = 7.06000812, Logitude = 99.37271118  }
                //};
                //foreach (var zoneItem in zones)
                //{
                //    polygon.Add(new PointF
                //    {
                //        X = (float)Convert.ToDouble(zoneItem.Latitude),
                //        Y = (float)Convert.ToDouble(zoneItem.Logitude)
                //    });
                //}
                //Expression<Func<SP, bool>> expression2 = x => CommonHelper.IsPointInPolygon(polygon.ToArray(), new PointF
                //{
                //    X = (float)x.Latitude,
                //    Y = (float)x.Longitude
                //});
                //expression = expression.And(expression2);
                //var result = db.ShipPositions.FirstOrDefault(expression).Latitude;
                //_output.WriteLine(result.ToString());

                var qry = db.Query<SP>().FromSql($@"
                   
                    SELECT sp.RecvTime, sp.LocalRecvTime, sp.SOG, sp.COG, sp.Latitude, sp.Longitude, sp.MMSI, sp.ROT, sp.TrueHeading
                    FROM (
                        SELECT y.MMSI, MAX(y.RecvTime) RecvTime FROM ShipPosition y WHERE y.LocalRecvTime > {DateTime.Now.ToString("yyyy-MM-dd HH:MM:ss")} GROUP BY y.MMSI
                    ) g
                    INNER JOIN ShipPosition sp ON g.MMSI=sp.MMSI
                    WHERE sp.RecvTime=g.RecvTime
                ").ToList();
                var g = qry.Where(expression.Compile());
             
                _output.WriteLine(g.Count().ToString());
            }
        }
    }

}
