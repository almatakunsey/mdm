#@echo off
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
dotnet build VtmisApps.sln
dotnet publish Vtmis.WebAdmin.Web.Host -c Release -o out
dotnet publish Vtmis.WebVessel.Tracking -c Release -o out
dotnet publish Vtmis.WebAdmin.Web.Mvc -c Release -o out
dotnet publish Vtmis.Cluster.Seed -c Release -o out
dotnet publish Vtmis.Cluster.VesselQuery -c Release -o out
dotnet publish Vtmis.Cluster.VesselApi -c Release -o out
dotnet publish Vtmis.Api.MapTracking -c Release -o out
dotnet publish Vtmis.Api.MMDIS -c Release -o out
dotnet publish Vtmis.Cluster.EventListener -c Release -o out
docker-compose -f ".\docker-compose-local.yml" down --rmi all
docker-compose -f ".\docker-compose-local.yml" up --build
pause