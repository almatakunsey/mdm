﻿using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Configuration;
using Abp.Modules;
using Abp.Reflection;
using Abp.Zero.EntityFrameworkCore;
using Castle.Core.Logging;
using System.Reflection;
using Vtmis.WebAdmin.EntityFrameworkCore.Seed;

namespace Vtmis.WebAdmin.EntityFrameworkCore
{
    [DependsOn(
        typeof(WebAdminCoreModule),
        typeof(AbpZeroCoreEntityFrameworkCoreModule))]
    public class WebAdminEntityFrameworkModule : AbpModule
    {
        private readonly ITypeFinder _typeFinder;
        public WebAdminEntityFrameworkModule(ITypeFinder typeFinder)
        {
            this._typeFinder = typeFinder;
            this.Logger = NullLogger.Instance;
        }
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            //Configuration.UnitOfWork.OverrideFilter(AbpDataFilters.MayHaveTenant, false);
            //Configuration.UnitOfWork.OverrideFilter(AbpDataFilters.MustHaveTenant, false);
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<WebAdminDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        WebAdminDbContextConfigurer.Configure(options.DbContextOptions,
                            options.ExistingConnection);
                        //options.DbContextOptions.UseSqlServer(WebContentDirectoryFinder.GetDefaultConnectionString());
                    }
                    else
                    {
                        WebAdminDbContextConfigurer.Configure(options.DbContextOptions,
                            options.ConnectionString);
                        //options.DbContextOptions.UseSqlServer(WebContentDirectoryFinder.GetDefaultConnectionString());
                    }
                });
            }
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            //IocManager.RegisterAssemblyByConvention(typeof(WebAdminEntityFrameworkModule).GetAssembly());
        }

        public override void PostInitialize()
        {
            if (!SkipDbSeed)
            {
                //SeedHelper.SeedHostDb(IocManager);
            }
        }

    }
}

