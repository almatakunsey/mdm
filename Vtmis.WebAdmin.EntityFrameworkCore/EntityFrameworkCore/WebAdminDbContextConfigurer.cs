using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using System;

namespace Vtmis.WebAdmin.EntityFrameworkCore
{
    public static class WebAdminDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<WebAdminDbContext> builder, string connectionString)
        {
            //Console.WriteLine(connectionString);
            builder.UseNpgsql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<WebAdminDbContext> builder, DbConnection connection)
        {
            builder.UseNpgsql(connection);
        }
    }
}
