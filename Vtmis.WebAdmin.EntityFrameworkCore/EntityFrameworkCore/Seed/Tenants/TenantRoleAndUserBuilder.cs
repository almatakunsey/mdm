﻿using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using Vtmis.WebAdmin.Authorization;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.MultiTenancy;
using Vtmis.Core.Common.Constants;

namespace Vtmis.WebAdmin.EntityFrameworkCore.Seed.Tenants
{
    public class TenantRoleAndUserBuilder
    {
        private readonly WebAdminDbContext _context;
        private readonly int _tenantId;

        public TenantRoleAndUserBuilder(WebAdminDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateRolesAndUsers();
        }

        private void CreateRolesAndUsers()
        {
            // Admin role

            var adminRole = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Admin);
            if (adminRole == null)
            {
                adminRole = _context.Roles.Add(new Role(_tenantId, StaticRoleNames.Tenants.Admin, $"{StaticRoleNames.Tenants.Admin} {AbpTenantBase.DefaultTenantName}") { IsStatic = false }).Entity;
                _context.SaveChanges();
            }

            // Grant all permissions to admin role

            var grantedPermissions = _context.Permissions.IgnoreQueryFilters()
                .OfType<RolePermissionSetting>()
                .Where(p => p.TenantId == _tenantId && p.RoleId == adminRole.Id)
                .Select(p => p.Name)
                .ToList();

            var permissions = PermissionFinder
                .GetAllPermissions(new TenantAdminAuthorizationProvider())
                .ToList();

            var existingPermissions = _context.RolePermissions.IgnoreQueryFilters().Any(p => p.TenantId == _tenantId);
            //var a = _context.RolePermissions.IgnoreQueryFilters().Where(x => x.TenantId == _tenantId).FirstOrDefault();
            if (existingPermissions == false)
            {
                if (permissions.Any())
                {
                    _context.Permissions.AddRange(
                        permissions.Select(permission => new RolePermissionSetting
                        {
                            TenantId = _tenantId,
                            Name = permission.Name,
                            IsGranted = true,
                            RoleId = adminRole.Id
                        })
                    );
                    _context.SaveChanges();
                }
            }

            var existingVesselEta = _context.RolePermissions.IgnoreQueryFilters().Any(p => p.Name.Contains(MdmPermissionsConst.VesselETA_View_Mdm, System.StringComparison.CurrentCultureIgnoreCase) && p.TenantId == _tenantId);
            if (existingVesselEta == false)
            {
                _context.Permissions.Add(new RolePermissionSetting
                {
                    TenantId = _tenantId,
                    Name = MdmPermissionsConst.VesselETA_View_Mdm,
                    IsGranted = true,
                    RoleId = adminRole.Id
                });
                _context.SaveChanges();
            }

            var existingRouteEta = _context.RolePermissions.IgnoreQueryFilters().Any(p => p.Name.Contains(MdmPermissionsConst.RouteETA_View_Mdm, System.StringComparison.CurrentCultureIgnoreCase) && p.TenantId == _tenantId);
            if (existingRouteEta == false)
            {
                _context.Permissions.Add(new RolePermissionSetting
                {
                    TenantId = _tenantId,
                    Name = MdmPermissionsConst.RouteETA_View_Mdm,
                    IsGranted = true,
                    RoleId = adminRole.Id
                });
                _context.SaveChanges();
            }

            var existingAtonPerm = _context.RolePermissions.IgnoreQueryFilters().Any(p => p.Name.Contains(MdmPermissionsConst.Aton_View_Mdm, System.StringComparison.CurrentCultureIgnoreCase) && p.TenantId == _tenantId);
            if (existingAtonPerm == false)
            {
                _context.Permissions.AddRange(
                       MdmPermissionsConst.AtonManager().Select(permission => new RolePermissionSetting
                       {
                           TenantId = _tenantId,
                           Name = permission,
                           IsGranted = true,
                           RoleId = adminRole.Id
                       })
                   );
                _context.SaveChanges();
            }


            var existingUserPersona = _context.RolePermissions.IgnoreQueryFilters().Any(p => p.Name.Contains(MdmPermissionsConst.UserPersonalization_Create, System.StringComparison.CurrentCultureIgnoreCase) && p.TenantId == _tenantId);
            if (existingUserPersona == false)
            {
                _context.Permissions.AddRange(
                       MdmPermissionsConst.UserPersonalization().Select(permission => new RolePermissionSetting
                       {
                           TenantId = _tenantId,
                           Name = permission,
                           IsGranted = true,
                           RoleId = adminRole.Id
                       })
                   );
                _context.SaveChanges();
            }

            var existingTenantPersona = _context.RolePermissions.IgnoreQueryFilters().Any(p => p.Name.Contains(MdmPermissionsConst.TenantPersonalization_Create, System.StringComparison.CurrentCultureIgnoreCase) && p.TenantId == _tenantId);
            if (existingTenantPersona == false)
            {
                _context.Permissions.AddRange(
                       MdmPermissionsConst.TenantPersonalization().Select(permission => new RolePermissionSetting
                       {
                           TenantId = _tenantId,
                           Name = permission,
                           IsGranted = true,
                           RoleId = adminRole.Id
                       })
                   );
                _context.SaveChanges();
            }
            // Admin user

            var adminUser = _context.Users.IgnoreQueryFilters().FirstOrDefault(u => u.TenantId == _tenantId && u.UserName == AbpUserBase.AdminUserName);
            if (adminUser == null)
            {
                adminUser = User.CreateTenantAdminUser(_tenantId, "admin@defaulttenant.com");
                adminUser.Password = new PasswordHasher<User>(new OptionsWrapper<PasswordHasherOptions>(new PasswordHasherOptions())).HashPassword(adminUser, "123qwe");
                adminUser.IsEmailConfirmed = true;
                adminUser.IsActive = true;

                _context.Users.Add(adminUser);
                _context.SaveChanges();

                // Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(_tenantId, adminUser.Id, adminRole.Id));
                _context.SaveChanges();

                // User account of admin user
                if (_tenantId == 1)
                {
                    _context.UserAccounts.Add(new UserAccount
                    {
                        TenantId = _tenantId,
                        UserId = adminUser.Id,
                        UserName = AbpUserBase.AdminUserName,
                        EmailAddress = adminUser.EmailAddress
                    });
                    _context.SaveChanges();
                }
            }
        }
    }
}
