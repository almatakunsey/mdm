﻿using System;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Vtmis.WebAdmin.Editions;
using Vtmis.WebAdmin.MultiTenancy;

namespace Vtmis.WebAdmin.EntityFrameworkCore.Seed.Tenants
{
    public class DefaultTenantBuilder
    {
        private readonly WebAdminDbContext _context;

        public DefaultTenantBuilder(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateDefaultTenant();

        }

        private void CreateDefaultTenant()
        {
            // Default tenant

            var defaultTenant = _context.Tenants.IgnoreQueryFilters().FirstOrDefault(t => t.TenancyName == Tenant.DefaultTenantName);
            if (defaultTenant == null)
            {
                defaultTenant = new Tenant(Tenant.DefaultTenantName, Tenant.DefaultTenantName);

                var defaultEdition = _context.Editions.IgnoreQueryFilters().FirstOrDefault(e => e.Name == EditionManager.DefaultEditionName);
                if (defaultEdition != null)
                {
                    defaultTenant.EditionId = defaultEdition.Id;
                }

                _context.Tenants.Add(defaultTenant);
                _context.SaveChanges();
            }
        }
    }
}
