﻿using System;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Uow;
using Abp.MultiTenancy;
using Vtmis.WebAdmin.EntityFrameworkCore.Seed.Host;
using Vtmis.WebAdmin.EntityFrameworkCore.Seed.Tenants;

namespace Vtmis.WebAdmin.EntityFrameworkCore.Seed
{
    public static class SeedHelper
    {
        public static void SeedHostDb(IIocResolver iocResolver)
        {
            WithDbContext<WebAdminDbContext>(iocResolver, SeedHostDb);
        }

        public static void SeedHostDb(WebAdminDbContext context)
        {
            try
            {
                //Console.WriteLine("Migrating");
                //context.Database.Migrate();
                Console.WriteLine("SEEDING");
                context.SuppressAutoSetTenantId = true;
                context.Database.Migrate();
                // Host seed
                Console.WriteLine("InitialHostDbBuilder SEEDING");
                new InitialHostDbBuilder(context).Create();

                // Default tenant seed (in host database).
                Console.WriteLine("DefaultTenantBuilder SEEDING");
                new DefaultTenantBuilder(context).Create();
                Console.WriteLine("TenantRoleAndUserBuilder SEEDING");
                new TenantRoleAndUserBuilder(context, 1).Create();
                Console.WriteLine("SEEDING Complete");
            }
            catch (Exception err)
            {
                Console.WriteLine("ERROR SEEDING:-");
                Console.WriteLine(err.Message);
                Console.WriteLine(err.InnerException);
            }
        }

        private static void WithDbContext<TDbContext>(IIocResolver iocResolver, Action<TDbContext> contextAction)
            where TDbContext : DbContext
        {
            using (var uowManager = iocResolver.ResolveAsDisposable<IUnitOfWorkManager>())
            {
                using (var uow = uowManager.Object.Begin(TransactionScopeOption.Suppress))
                {
                   
                    var context = uowManager.Object.Current.GetDbContext<TDbContext>(MultiTenancySides.Host);
                    
                    contextAction(context);

                    uow.Complete();
                }
            }
        }
    }
}
