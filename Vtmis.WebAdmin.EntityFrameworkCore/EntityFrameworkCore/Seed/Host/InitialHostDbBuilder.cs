﻿namespace Vtmis.WebAdmin.EntityFrameworkCore.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly WebAdminDbContext _context;

        public InitialHostDbBuilder(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
            //new ShipTypeSeed(_context).Create();
            //new ReportTypeSeed(_context).Create();
            //new FilterGroupSeed(_context).Create();

            _context.SaveChanges();
        }
    }
}
