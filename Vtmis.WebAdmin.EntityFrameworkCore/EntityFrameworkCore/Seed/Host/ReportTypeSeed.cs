﻿using System.Linq;
using Vtmis.WebAdmin.Reports;

namespace Vtmis.WebAdmin.EntityFrameworkCore.Seed.Host
{
    public class ReportTypeSeed
    {
        private readonly WebAdminDbContext _context;

        public ReportTypeSeed(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            var reportTypes = _context.ReportTypes;

            if (!reportTypes.Where(x=>x.ReportTypeNumber == "06" || x.ReportTypeNumber == "07").Any())
            {
                reportTypes.Add(new ReportType
                {
                    Name = "Speed Violation",
                    ReportTypeNumber = "07",
                    Status = VtmisType.Status.Active
                });

                //reportTypes.Add(new ReportType
                //{
                //    Name = "Speed Summary",
                //    ReportTypeNumber = "06",
                //    Status = VtmisType.Status.Active
                //});

                _context.SaveChanges();
            }          
        }
    }
}
