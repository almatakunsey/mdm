﻿using System.Collections.Generic;
using System.Linq;
using Vtmis.WebAdmin.Filters;

namespace Vtmis.WebAdmin.EntityFrameworkCore.Seed.Host
{
    public class FilterGroupSeed
    {
        private readonly WebAdminDbContext _context;
        public FilterGroupSeed(WebAdminDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateSeed();
        }

        private void CreateSeed()
        {
            var filterGroups = _context.FilterGroups;

            if (!filterGroups.Any())
            {               
                filterGroups.AddRange(
                    new FilterGroup
                    {
                        Name = "[All Cyan]"
                    },
                    new FilterGroup
                    {
                        Name = "[AtoN Only]"
                    },
                    new FilterGroup
                    {
                        Name = "[BaseStation Only]"
                    },
                    new FilterGroup
                    {
                        Name = "[Color-Coded Types]"
                    },
                    new FilterGroup
                    {
                        Name = "[Fishing Vessel Only]"
                    },
                    new FilterGroup
                    {
                        Name = "[Passenger Vessel Only]"
                    });
                
                _context.SaveChanges();
            }
        }
    }
}
