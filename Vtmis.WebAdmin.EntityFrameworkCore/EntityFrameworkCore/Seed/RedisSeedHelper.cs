﻿using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.EntityFrameworkCore.Uow;
using Abp.MultiTenancy;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.WebAdmin.EntityFrameworkCore.Seed
{
    public class RedisSeedHelper : IRedisSeed
    {
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        private readonly IocManager _iocResolver;
        private WebAdminDbContext _adminDbContext;
        private readonly string _redisCon;
        public RedisSeedHelper(IConnectionMultiplexer connectionMultiplexer, IocManager iocResolver)
        {
            _connectionMultiplexer = connectionMultiplexer;
            _iocResolver = iocResolver;
            _redisCon = AppHelper.GetRedisConnectionString();
        }

        public void Seed()
        {
            try
            {
                using (var uowManager = _iocResolver.ResolveAsDisposable<IUnitOfWorkManager>())
                {
                    using (var uow = uowManager.Object.Begin(TransactionScopeOption.Suppress))
                    {

                        var context = uowManager.Object.Current.GetDbContext<WebAdminDbContext>(MultiTenancySides.Host);

                        _adminDbContext = context;

                        //====================
                      
                        var redis = _connectionMultiplexer.GetDatabase();
                        var lloydsHash = redis.HashGetAll($"Lloyds_{AppHelper.GetEnvironmentName()}");
                        if (lloydsHash.Length > 0)
                        {
                            Console.WriteLine("Removing existing lloyds data");
                            foreach (var item in lloydsHash)
                            {
                                redis.HashDelete($"Lloyds_{AppHelper.GetEnvironmentName()}", item.Name, CommandFlags.FireAndForget);
                            }
                        }

                        var lloydsRows = _adminDbContext.Lloyds.Count();
                        if (lloydsRows > 0)
                        {
                            double numOfPage = Math.Round((double)((double)lloydsRows / 100), 2);
                            int actualNumOfPage = 0;
                            if ((numOfPage % 1) == 0)
                            {
                                actualNumOfPage = (int)numOfPage;
                            }
                            else
                            {
                                actualNumOfPage = (int)(numOfPage + 1);
                            }

                            List<Lloyds.Lloyd> lloyds = new List<Lloyds.Lloyd>();
                            for (int i = 0; i < actualNumOfPage; i++)
                            {
                                var data = _adminDbContext.Lloyds.AsNoTracking().Skip(i).Take(100).ToList();
                                lloyds.AddRange(data);
                            }
                            Console.WriteLine("Seeding lloyds data");
                            for (int i = 0; i < lloyds.Count; i++)
                            {
                                var data = JsonConvert.SerializeObject(lloyds[i]);
                                var key = lloyds[i].IMO.HasValue ? lloyds[i].IMO.ToString() : lloyds[i].CallSign;
                                if (key == null)
                                {
                                    key = lloyds[i].Name;
                                }
                                if (string.IsNullOrWhiteSpace(key))
                                {
                                    key = i.ToString();
                                }
                                redis.HashSet($"Lloyds_{AppHelper.GetEnvironmentName()}", key, data, When.Always, CommandFlags.FireAndForget);
                            }
                        }

                        var eicsHash = redis.HashGetAll($"eics_{AppHelper.GetEnvironmentName()}");
                        if (eicsHash.Length > 0)
                        {
                            Console.WriteLine("Removing existing EICS data");
                            foreach (var item in eicsHash)
                            {
                                redis.HashDelete($"eics_{AppHelper.GetEnvironmentName()}", item.Name, CommandFlags.FireAndForget);
                            }
                        }

                        var eicsRows = _adminDbContext.EICS_Infos.Count();
                        if (eicsRows > 0)
                        {
                            double numOfPage = Math.Round((double)((double)eicsRows / 100), 5);
                            int actualNumOfPage = 0;
                            if ((numOfPage % 1) == 0)
                            {
                                actualNumOfPage = (int)numOfPage;
                            }
                            else
                            {
                                actualNumOfPage = (int)(numOfPage + 1);
                            }

                            List<EICSSeedData> eics = new List<EICSSeedData>();
                            for (int i = 0; i < actualNumOfPage; i++)
                            {
                                var data = _adminDbContext.EICS_Infos.Include("EICS_Details").Include("EICS_Details.Equipment").AsNoTracking().Select(s => new EICSSeedData
                                {
                                    MMSI = s.MMSI,
                                    EICS = s.EICS_Details.Select(x => x.Equipment.Name).ToList()
                                }).Skip(i).Take(100).ToList();
                                eics.AddRange(data);
                            }
                            Console.WriteLine("Seeding EICS data");
                            for (int i = 0; i < eics.Count; i++)
                            {
                                var data = JsonConvert.SerializeObject(eics[i]);
                                redis.HashSet($"eics_{AppHelper.GetEnvironmentName()}", eics[i].MMSI.ToString(), data, When.Always, CommandFlags.FireAndForget);
                            }
                        }
                        //====================
                        uow.Complete();
                    }
                }
                Console.WriteLine("Redis seeding complete");
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }
        }
    }

    public class EICSSeedData
    {
        public EICSSeedData()
        {
            EICS = new List<string>();
        }
        public int MMSI { get; set; }
        public List<string> EICS { get; set; }
    }
}
