﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using Vtmis.WebAdmin.Authorization.Roles;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.MultiTenancy;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.VtmisType;
using Vtmis.WebAdmin.Reports;
using Vtmis.WebAdmin.Colors;
using Vtmis.WebAdmin.Zones;
using Vtmis.WebAdmin.Vts;
using Vtmis.WebAdmin.Locations;
using Vtmis.WebAdmin.Alarms;
using Vtmis.WebAdmin.Vessels;
using Vtmis.WebAdmin.VesselOwneds;
using Vtmis.WebAdmin.Lloyds;
using Vtmis.WebAdmin.Routes;
using Vtmis.WebAdmin.Personalizations;
using Vtmis.WebAdmin.Targets;
using Vtmis.WebAdmin.Images;
using Vtmis.WebAdmin.EICS_Equipments;
using Vtmis.WebAdmin.EICS;
using Vtmis.WebAdmin.TenantFilters;
using Vtmis.WebAdmin.MEH;

namespace Vtmis.WebAdmin.EntityFrameworkCore
{

    public class WebAdminDbContext : AbpZeroDbContext<Tenant, Role, User, WebAdminDbContext>
    {
        /* Define an IDbSet for each entity of the application */

        public WebAdminDbContext(DbContextOptions<WebAdminDbContext> options)
            : base(options)
        {

        }

        public virtual DbSet<Filter> Filters { get; set; }
        //public virtual DbSet<FilterItem> FilterItems { get; set; }
        public virtual DbSet<ShipType> ShipTypes { get; set; }

        public virtual DbSet<ReportType> ReportTypes { get; set; }
        public virtual DbSet<FilterGroup> FilterGroups { get; set; }

        public virtual DbSet<Zone> Zones { get; set; }
        public virtual DbSet<ZoneItem> ZoneItems { get; set; }

        public DbSet<AISColourSchema> AISColourSchemas { get; set; }
        public DbSet<AISColourSchemaItem> AISColourSchemaItems { get; set; }

        public DbSet<Report> Reports { get; set; }
        public DbSet<ReportItem> ReportItems { get; set; }

        public DbSet<ExtendedShip> ExtendedShips { get; set; }

        public DbSet<Location> Locations { get; set; }
        public DbSet<Alarm> Alarms { get; set; }
        public DbSet<AlarmItem> AlarmItems { get; set; }
        public DbSet<VesselGroup> VesselGroups { get; set; }
        public DbSet<Vtmis.WebAdmin.AisType.AisType> AisTypes { get; set; }
        public DbSet<VesselOwned> VesselOwneds { get; set; }
        public DbSet<Vtmis.WebAdmin.Logger.Logger> Logs { get; set; }
        public DbSet<FilterItemColumn> FilterItemColumns { get; set; }
        public DbSet<Lloyd> Lloyds { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<Waypoint> Waypoints { get; set; }
        public DbSet<Cable.Cable> Cables { get; set; }
        public DbSet<Events.Event> Events { get; set; }
        public DbSet<AlarmNotification> AlarmNotifications { get; set; }
        public DbSet<ImageInfo> ImageInfos { get; set; }

        public DbSet<Personalization> Personalizations { get; set; }
        public DbSet<TenantPersonalization> TenantPersonalizations { get; set; }

        public DbSet<EICS_Equipment> EICS_Equipment { get; set; }
        public DbSet<EICS_info> EICS_Infos { get; set; }
        public DbSet<EICS_details> EICS_Details { get; set; }
        public DbSet<TenantFilter> TenantFilters { get; set; }
        public DbSet<MEHIncidentReport> MEHIncidentReports { get; set; }
        public DbSet<MEHIncidentLocation> MEHIncidentLocations { get; set; }
        public DbSet<MEHIncidentCoordinate> MEHIncidentCoordinates { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ZoneTrip>();
            builder.Entity<BerthTimeSummary>();
            builder.Entity<SpeedViolation>();
            builder.Entity<AnchorageTime>();
            builder.Entity<TenantFilter>().HasIndex(
                  i => new { i.FilterName, i.Type, i.TenantId, i.IsDeleted }).IsUnique(false);
            builder.Entity<MEHIncidentReport>()
                .HasIndex(s => new { s.Date, s.VesselNameA, s.VesselNameB, s.CallSignA, s.CallSignB }).IsUnique(false);
            builder.Entity<MEHIncidentReport>()
               .HasIndex(s => s.IncidentId).IsUnique(true);
            builder.ForNpgsqlUseSerialColumns();

            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }


    }
}
