﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebAdmin.Configuration;
using Vtmis.WebAdmin.Web;
using System;

namespace Vtmis.WebAdmin.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class WebAdminDbContextFactory : IDesignTimeDbContextFactory<WebAdminDbContext>
    {
        public WebAdminDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<WebAdminDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            WebAdminDbContextConfigurer.Configure(builder, configuration.GetConnectionString(WebAdminConsts.ConnectionStringName));
            //WebAdminDbContextConfigurer.Configure(builder, AppHelper.GetMdmDbConnectionString());
            return new WebAdminDbContext(builder.Options);
        }
    }
}
