﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class AddTenantFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TenantFilters",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    FilterName = table.Column<string>(maxLength: 50, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantFilters", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TenantFilterItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Condition = table.Column<int>(nullable: false),
                    ColumnName = table.Column<string>(maxLength: 50, nullable: true),
                    Operation = table.Column<string>(maxLength: 10, nullable: true),
                    ColumnValue = table.Column<string>(maxLength: 100, nullable: true),
                    RowIndex = table.Column<int>(nullable: false),
                    TenantFilterId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TenantFilterItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TenantFilterItem_TenantFilters_TenantFilterId",
                        column: x => x.TenantFilterId,
                        principalTable: "TenantFilters",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TenantFilterItem_TenantFilterId",
                table: "TenantFilterItem",
                column: "TenantFilterId");

            migrationBuilder.CreateIndex(
                name: "IX_TenantFilters_FilterName_Type",
                table: "TenantFilters",
                columns: new[] { "FilterName", "Type" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TenantFilterItem");

            migrationBuilder.DropTable(
                name: "TenantFilters");
        }
    }
}
