﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class RemoveImageVariation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ImageVariation");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ImageVariation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    FullPath = table.Column<string>(nullable: true),
                    ImageDetailId = table.Column<long>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Path = table.Column<string>(nullable: true),
                    VariationName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageVariation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ImageVariation_ImageDetail_ImageDetailId",
                        column: x => x.ImageDetailId,
                        principalTable: "ImageDetail",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ImageVariation_ImageDetailId",
                table: "ImageVariation",
                column: "ImageDetailId");
        }
    }
}
