﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class AddTenantFilterRelationWithTenantAndUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<List<int>>(
                name: "AssignedTenants",
                table: "TenantFilters",
                nullable: true);

            migrationBuilder.AddColumn<List<long>>(
                name: "AssignedUsers",
                table: "TenantFilters",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AssignedTenants",
                table: "TenantFilters");

            migrationBuilder.DropColumn(
                name: "AssignedUsers",
                table: "TenantFilters");
        }
    }
}
