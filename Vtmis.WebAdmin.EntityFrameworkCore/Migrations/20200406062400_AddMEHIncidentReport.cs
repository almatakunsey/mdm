﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class AddMEHIncidentReport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MEHIncidentReports",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    MetaStatus = table.Column<string>(maxLength: 50, nullable: true),
                    MetaType = table.Column<string>(maxLength: 50, nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    TypeOfCasualty = table.Column<string>(maxLength: 50, nullable: true),
                    LossOfLife = table.Column<string>(maxLength: 50, nullable: true),
                    MarinePollution = table.Column<string>(maxLength: 50, nullable: true),
                    Visibility = table.Column<string>(maxLength: 50, nullable: true),
                    SeaState = table.Column<string>(maxLength: 50, nullable: true),
                    ForceWind = table.Column<string>(maxLength: 50, nullable: true),
                    VesselNameA = table.Column<string>(maxLength: 50, nullable: true),
                    CallSignA = table.Column<string>(maxLength: 50, nullable: true),
                    MmsiA = table.Column<int>(maxLength: 50, nullable: true),
                    TypeA = table.Column<string>(maxLength: 50, nullable: true),
                    FlagA = table.Column<string>(maxLength: 50, nullable: true),
                    DestinationA = table.Column<string>(maxLength: 50, nullable: true),
                    CargoA = table.Column<string>(maxLength: 50, nullable: true),
                    VesselNameB = table.Column<string>(maxLength: 50, nullable: true),
                    CallSignB = table.Column<string>(maxLength: 50, nullable: true),
                    MmsiB = table.Column<int>(maxLength: 50, nullable: true),
                    TypeB = table.Column<string>(maxLength: 50, nullable: true),
                    FlagB = table.Column<string>(maxLength: 50, nullable: true),
                    DestinationB = table.Column<string>(maxLength: 50, nullable: true),
                    CargoB = table.Column<string>(maxLength: 50, nullable: true),
                    AccidentDescription = table.Column<string>(nullable: true),
                    OperatorInCharge = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MEHIncidentReports", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MEHIncidentLocation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Latitude = table.Column<float>(nullable: false),
                    Longitude = table.Column<float>(nullable: false),
                    MEHIncidentReportId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MEHIncidentLocation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MEHIncidentLocation_MEHIncidentReports_MEHIncidentReportId",
                        column: x => x.MEHIncidentReportId,
                        principalTable: "MEHIncidentReports",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MEHIncidentLocation_MEHIncidentReportId",
                table: "MEHIncidentLocation",
                column: "MEHIncidentReportId");

            migrationBuilder.CreateIndex(
                name: "IX_MEHIncidentReports_Date_VesselNameA_VesselNameB_CallSignA_C~",
                table: "MEHIncidentReports",
                columns: new[] { "Date", "VesselNameA", "VesselNameB", "CallSignA", "CallSignB" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MEHIncidentLocation");

            migrationBuilder.DropTable(
                name: "MEHIncidentReports");
        }
    }
}
