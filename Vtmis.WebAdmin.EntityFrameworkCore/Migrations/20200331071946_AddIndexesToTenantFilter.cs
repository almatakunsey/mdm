﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class AddIndexesToTenantFilter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TenantFilters_FilterName_Type",
                table: "TenantFilters");

            migrationBuilder.CreateIndex(
                name: "IX_TenantFilters_FilterName_Type_TenantId_IsDeleted",
                table: "TenantFilters",
                columns: new[] { "FilterName", "Type", "TenantId", "IsDeleted" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TenantFilters_FilterName_Type_TenantId_IsDeleted",
                table: "TenantFilters");

            migrationBuilder.CreateIndex(
                name: "IX_TenantFilters_FilterName_Type",
                table: "TenantFilters",
                columns: new[] { "FilterName", "Type" });
        }
    }
}
