﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class AddRelationForMEHIncident : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MEHIncidentCoordinate_MEHIncidentLocation_MEHIncidentLocati~",
                table: "MEHIncidentCoordinate");

            migrationBuilder.DropForeignKey(
                name: "FK_MEHIncidentLocation_MEHIncidentReports_MEHIncidentReportId",
                table: "MEHIncidentLocation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MEHIncidentLocation",
                table: "MEHIncidentLocation");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MEHIncidentCoordinate",
                table: "MEHIncidentCoordinate");

            migrationBuilder.RenameTable(
                name: "MEHIncidentLocation",
                newName: "MEHIncidentLocations");

            migrationBuilder.RenameTable(
                name: "MEHIncidentCoordinate",
                newName: "MEHIncidentCoordinates");

            migrationBuilder.RenameIndex(
                name: "IX_MEHIncidentLocation_MEHIncidentReportId",
                table: "MEHIncidentLocations",
                newName: "IX_MEHIncidentLocations_MEHIncidentReportId");

            migrationBuilder.RenameIndex(
                name: "IX_MEHIncidentCoordinate_MEHIncidentLocationId",
                table: "MEHIncidentCoordinates",
                newName: "IX_MEHIncidentCoordinates_MEHIncidentLocationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MEHIncidentLocations",
                table: "MEHIncidentLocations",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MEHIncidentCoordinates",
                table: "MEHIncidentCoordinates",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MEHIncidentCoordinates_MEHIncidentLocations_MEHIncidentLoca~",
                table: "MEHIncidentCoordinates",
                column: "MEHIncidentLocationId",
                principalTable: "MEHIncidentLocations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MEHIncidentLocations_MEHIncidentReports_MEHIncidentReportId",
                table: "MEHIncidentLocations",
                column: "MEHIncidentReportId",
                principalTable: "MEHIncidentReports",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MEHIncidentCoordinates_MEHIncidentLocations_MEHIncidentLoca~",
                table: "MEHIncidentCoordinates");

            migrationBuilder.DropForeignKey(
                name: "FK_MEHIncidentLocations_MEHIncidentReports_MEHIncidentReportId",
                table: "MEHIncidentLocations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MEHIncidentLocations",
                table: "MEHIncidentLocations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MEHIncidentCoordinates",
                table: "MEHIncidentCoordinates");

            migrationBuilder.RenameTable(
                name: "MEHIncidentLocations",
                newName: "MEHIncidentLocation");

            migrationBuilder.RenameTable(
                name: "MEHIncidentCoordinates",
                newName: "MEHIncidentCoordinate");

            migrationBuilder.RenameIndex(
                name: "IX_MEHIncidentLocations_MEHIncidentReportId",
                table: "MEHIncidentLocation",
                newName: "IX_MEHIncidentLocation_MEHIncidentReportId");

            migrationBuilder.RenameIndex(
                name: "IX_MEHIncidentCoordinates_MEHIncidentLocationId",
                table: "MEHIncidentCoordinate",
                newName: "IX_MEHIncidentCoordinate_MEHIncidentLocationId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MEHIncidentLocation",
                table: "MEHIncidentLocation",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MEHIncidentCoordinate",
                table: "MEHIncidentCoordinate",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_MEHIncidentCoordinate_MEHIncidentLocation_MEHIncidentLocati~",
                table: "MEHIncidentCoordinate",
                column: "MEHIncidentLocationId",
                principalTable: "MEHIncidentLocation",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_MEHIncidentLocation_MEHIncidentReports_MEHIncidentReportId",
                table: "MEHIncidentLocation",
                column: "MEHIncidentReportId",
                principalTable: "MEHIncidentReports",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
