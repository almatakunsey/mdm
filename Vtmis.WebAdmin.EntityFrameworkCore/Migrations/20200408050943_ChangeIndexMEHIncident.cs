﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class ChangeIndexMEHIncident : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_MEHIncidentReports_IncidentId_Date_VesselNameA_VesselNameB_~",
                table: "MEHIncidentReports");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateInsertedFromMEH",
                table: "MEHIncidentReports",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModifiedDateFromMEH",
                table: "MEHIncidentReports",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_MEHIncidentReports_IncidentId",
                table: "MEHIncidentReports",
                column: "IncidentId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_MEHIncidentReports_Date_VesselNameA_VesselNameB_CallSignA_C~",
                table: "MEHIncidentReports",
                columns: new[] { "Date", "VesselNameA", "VesselNameB", "CallSignA", "CallSignB" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_MEHIncidentReports_IncidentId",
                table: "MEHIncidentReports");

            migrationBuilder.DropIndex(
                name: "IX_MEHIncidentReports_Date_VesselNameA_VesselNameB_CallSignA_C~",
                table: "MEHIncidentReports");

            migrationBuilder.DropColumn(
                name: "DateInsertedFromMEH",
                table: "MEHIncidentReports");

            migrationBuilder.DropColumn(
                name: "LastModifiedDateFromMEH",
                table: "MEHIncidentReports");

            migrationBuilder.CreateIndex(
                name: "IX_MEHIncidentReports_IncidentId_Date_VesselNameA_VesselNameB_~",
                table: "MEHIncidentReports",
                columns: new[] { "IncidentId", "Date", "VesselNameA", "VesselNameB", "CallSignA", "CallSignB" });
        }
    }
}
