﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class AddIncidentIdToMEHIncident : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_MEHIncidentReports_Date_VesselNameA_VesselNameB_CallSignA_C~",
                table: "MEHIncidentReports");

            migrationBuilder.AddColumn<int>(
                name: "IncidentId",
                table: "MEHIncidentReports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MEHIncidentReports_IncidentId_Date_VesselNameA_VesselNameB_~",
                table: "MEHIncidentReports",
                columns: new[] { "IncidentId", "Date", "VesselNameA", "VesselNameB", "CallSignA", "CallSignB" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_MEHIncidentReports_IncidentId_Date_VesselNameA_VesselNameB_~",
                table: "MEHIncidentReports");

            migrationBuilder.DropColumn(
                name: "IncidentId",
                table: "MEHIncidentReports");

            migrationBuilder.CreateIndex(
                name: "IX_MEHIncidentReports_Date_VesselNameA_VesselNameB_CallSignA_C~",
                table: "MEHIncidentReports",
                columns: new[] { "Date", "VesselNameA", "VesselNameB", "CallSignA", "CallSignB" });
        }
    }
}
