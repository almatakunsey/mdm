﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class EditMEHIncident : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateInsertedByMDM",
                table: "MEHIncidentReports",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModifiedDateByMDM",
                table: "MEHIncidentReports",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateInsertedByMDM",
                table: "MEHIncidentReports");

            migrationBuilder.DropColumn(
                name: "LastModifiedDateByMDM",
                table: "MEHIncidentReports");
        }
    }
}
