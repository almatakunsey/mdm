﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class AddUserIdToVesselGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "VesselGroups",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserId",
                table: "VesselGroups");
        }
    }
}
