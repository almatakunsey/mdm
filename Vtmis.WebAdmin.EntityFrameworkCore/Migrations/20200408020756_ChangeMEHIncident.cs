﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class ChangeMEHIncident : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Latitude",
                table: "MEHIncidentLocation");

            migrationBuilder.DropColumn(
                name: "Longitude",
                table: "MEHIncidentLocation");

            migrationBuilder.DropColumn(
               name: "LossOfLife",
               table: "MEHIncidentReports");

            migrationBuilder.AddColumn<int>(
                name: "LossOfLife",
                table: "MEHIncidentReports",              
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CoordType",
                table: "MEHIncidentReports",
                maxLength: 50,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MEHIncidentCoordinate",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    Latitude = table.Column<float>(nullable: false),
                    Longitude = table.Column<float>(nullable: false),
                    MEHIncidentLocationId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MEHIncidentCoordinate", x => x.Id);
                    table.ForeignKey(
                        name: "FK_MEHIncidentCoordinate_MEHIncidentLocation_MEHIncidentLocati~",
                        column: x => x.MEHIncidentLocationId,
                        principalTable: "MEHIncidentLocation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MEHIncidentCoordinate_MEHIncidentLocationId",
                table: "MEHIncidentCoordinate",
                column: "MEHIncidentLocationId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MEHIncidentCoordinate");

            migrationBuilder.DropColumn(
                name: "CoordType",
                table: "MEHIncidentReports");

            migrationBuilder.AlterColumn<string>(
                name: "LossOfLife",
                table: "MEHIncidentReports",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<float>(
                name: "Latitude",
                table: "MEHIncidentLocation",
                nullable: false,
                defaultValue: 0f);

            migrationBuilder.AddColumn<float>(
                name: "Longitude",
                table: "MEHIncidentLocation",
                nullable: false,
                defaultValue: 0f);
        }
    }
}
