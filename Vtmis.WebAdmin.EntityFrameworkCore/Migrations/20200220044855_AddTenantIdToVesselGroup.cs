﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Vtmis.WebAdmin.Migrations
{
    public partial class AddTenantIdToVesselGroup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "VesselGroups",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "VesselGroups");
        }
    }
}
