using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Vtmis.WebAdmin.Authorization;
using Vtmis.WebAdmin.Controllers;
using Vtmis.WebAdmin.Roles;
using Vtmis.WebAdmin.Web.Models.Roles;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Vtmis.WebAdmin.Authorization.Roles;
using Abp.Domain.Uow;
using Abp.Authorization;
using System.Collections.Generic;
using Vtmis.WebAdmin.Roles.Dto;
using Vtmis.WebAdmin.ReadOnlyDB.Services;

namespace Vtmis.WebAdmin.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Roles)]
    public class RolesController : WebAdminControllerBase
    {
        private readonly IRoleAppService _roleAppService;
        private readonly RoleManager _roleManager;
        private readonly IAbpPermissionService _abpPermissionService;

        public RolesController(IRoleAppService roleAppService, RoleManager roleManager, IAbpPermissionService abpPermissionService)
        {
            _roleAppService = roleAppService;
            _roleManager = roleManager;
            _abpPermissionService = abpPermissionService;
        }


        public async Task<IActionResult> Index()
        {
            var roles = await _roleAppService.GetAllAsync();
            var qryPermissions = AbpSession.TenantId == null ? PermissionFinder.GetAllPermissions(new WebAdminAuthorizationProvider()).ToList() :
                                   PermissionFinder.GetAllPermissions(new TenantAdminAuthorizationProvider()).ToList();
            var permissions = ObjectMapper.Map<List<PermissionDto>>(qryPermissions);
            //var permissions = await _roleAppService.GetAllPermissions();
            //var customPermissions = _roleAppService.GetAllCustomPermissions();
            //permissions.AddRange(customPermissions);
            var model = new RoleListViewModel
            {
                Roles = roles,
                Permissions = permissions
            };

            return View(model);
        }

        public async Task<ActionResult> EditRoleModal(int roleId)
        {
            using (CurrentUnitOfWork.DisableFilter(AbpDataFilters.MayHaveTenant,
                AbpDataFilters.MustHaveTenant))
            {
                var role = await _roleAppService.Get(roleId);
                var qryPermissions = AbpSession.TenantId == null ? PermissionFinder.GetAllPermissions(new WebAdminAuthorizationProvider()).ToList() :
                                      PermissionFinder.GetAllPermissions(new TenantAdminAuthorizationProvider()).ToList();
                var permissions = ObjectMapper.Map<List<PermissionDto>>(qryPermissions);

                if (permissions.Any())
                {
                    foreach (var item in permissions)
                    {
                        item.IsGranted = await _abpPermissionService.IsGrantedByRoleIdAsync(item.Name, role.Id);
                    }
                }
              
                var model = new EditRoleModalViewModel
                {
                    Role = role,
                    Permissions = permissions
                };
                return View("_EditRoleModal", model);
            }

        }
    }
}