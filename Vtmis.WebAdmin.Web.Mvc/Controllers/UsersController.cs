﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AspNetCore.Mvc.Authorization;
using Vtmis.WebAdmin.Authorization;
using Vtmis.WebAdmin.Controllers;
using Vtmis.WebAdmin.Users;
using Vtmis.WebAdmin.Web.Models.Users;
using Microsoft.AspNetCore.Mvc;

namespace Vtmis.WebAdmin.Web.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Users)]
    public class UsersController : WebAdminControllerBase
    {
        private readonly IUserAppService _userAppService;

        public UsersController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        public async Task<ActionResult> Index()
        {
            var users = await _userAppService.GetAllAsync(new PagedResultRequestDto()); //Paging not implemented yet
            var roles = await _userAppService.GetRoles();
            var model = new UserListViewModel
            {
                Users = users.Items,
                Roles = roles.Items
            };
            return View(model);
        }

        public async Task<ActionResult> EditUserModal(long userId)
        {
            var user = await _userAppService.Get(new EntityDto<long>(userId));
            var roles = await _userAppService.GetRoles();
            var model = new EditUserModalViewModel
            {
                User = user,
                Roles = roles.Items
            };
            return View("_EditUserModal", model);
        }
    }
}