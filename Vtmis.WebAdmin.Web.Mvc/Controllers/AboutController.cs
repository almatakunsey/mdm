﻿using Abp.AspNetCore.Mvc.Authorization;
using Vtmis.WebAdmin.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Vtmis.WebAdmin.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : WebAdminControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}