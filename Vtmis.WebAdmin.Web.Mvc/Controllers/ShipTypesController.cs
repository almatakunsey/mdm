﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Vtmis.WebAdmin.Controllers;
using Abp.AspNetCore.Mvc.Authorization;
using Vtmis.WebAdmin.Authorization;

namespace Vtmis.WebAdmin.Web.Mvc.Controllers
{
    [AbpMvcAuthorize(PermissionNames.Pages_Users)]
    public class ShipTypesController : WebAdminControllerBase
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}