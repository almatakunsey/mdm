﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Vtmis.WebAdmin.Web.Views
{
    public abstract class WebAdminViewComponent : AbpViewComponent
    {
        protected WebAdminViewComponent()
        {
            LocalizationSourceName = WebAdminConsts.LocalizationSourceName;
        }
    }
}