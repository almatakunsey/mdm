﻿using Vtmis.WebAdmin.Configuration.Ui;

namespace Vtmis.WebAdmin.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
