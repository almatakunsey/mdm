using Abp.AutoMapper;
using Vtmis.WebAdmin.Sessions.Dto;

namespace Vtmis.WebAdmin.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}