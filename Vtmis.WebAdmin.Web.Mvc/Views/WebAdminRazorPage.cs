﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace Vtmis.WebAdmin.Web.Views
{
    public abstract class WebAdminRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected WebAdminRazorPage()
        {
            LocalizationSourceName = WebAdminConsts.LocalizationSourceName;
        }
    }
}
