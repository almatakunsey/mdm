﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System.Net;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.WebAdmin.Web.Startup
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }
        // TODO: SSLConfig
        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                 //.UseUrls($"{AppHelper.GetProtocol()}://0.0.0.0:8082")
                 .UseKestrel(options =>
                 {
                     options.Listen(IPAddress.Any, 5015, listenOptions =>
                     {
                        
                     });
                     //options.Listen(IPAddress.Any, 8082, listenOptions =>
                     //{
                     //    //var configuration = (IConfiguration)options.ApplicationServices.GetService(typeof(IConfiguration));
                     //    if (string.Equals(AppHelper.GetEnvironmentName(), EnvironmentName.Staging, System.StringComparison.CurrentCultureIgnoreCase))
                     //    {
                     //        listenOptions.UseHttps(AppHelper.GetSSLCertificationPath(), AppHelper.GetSSLCertificationPassword());
                     //    }
                     //});
                 })
                .UseStartup<Startup>()
                 .ConfigureAppConfiguration((context, config) =>
                 {
                     IHostingEnvironment env = context.HostingEnvironment;

                     config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                           .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                           .AddEnvironmentVariables();
                 })
                .Build();
        }
    }
}
