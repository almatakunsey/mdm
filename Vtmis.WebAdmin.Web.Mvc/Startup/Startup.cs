﻿using System;
using Abp.AspNetCore;
using Abp.Castle.Logging.Log4Net;
using Vtmis.WebAdmin.Authentication.JwtBearer;
using Vtmis.WebAdmin.Configuration;
using Vtmis.WebAdmin.Identity;
using Vtmis.WebAdmin.Web.Resources;
using Castle.Facilities.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Vtmis.Core.Common.Constants;
using Vtmis.WebAdmin.ReadOnlyDB;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories;
using Vtmis.WebAdmin.EntityFrameworkCore.Seed;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Abp.Dependency;
using Vtmis.WebAdmin.ReadOnlyDB.Services;
using Npgsql;
using Vtmis.WebAdmin.Permissions;
using StackExchange.Redis;
using Vtmis.Core.Common.Helpers;

#if FEATURE_SIGNALR
using Owin;
using Abp.Owin;
using Vtmis.WebAdmin.Owin;
#endif

namespace Vtmis.WebAdmin.Web.Startup
{
    public class Startup
    {
        private readonly IConfigurationRoot _appConfiguration;
        private readonly IHostingEnvironment _env;

        public Startup(IHostingEnvironment env)
        {
            _appConfiguration = env.GetAppConfiguration();
            _env = env;
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //MVC
            services.AddMvc(options =>
            {
                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            });
            // TODO: SSLConfig
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    services.Configure<MvcOptions>(options =>
            //    {
            //        options.Filters.Add(new RequireHttpsAttribute());
            //    });
            //}
            IdentityRegistrar.Register(services);
            AuthConfigurer.Configure(services, _appConfiguration);

            services.AddScoped<IWebResourceManager, WebResourceManager>();
            services.AddDbContext<MdmAdminDbContext>(ServiceLifetime.Transient);
            services.AddSingleton<IConnectionMultiplexer>(x =>
                      ConnectionMultiplexer.Connect(AppHelper.GetRedisConnectionString()));
            services.AddTransient<IMdmUserRoleRepository, MdmUserRoleRepository>();
            services.AddTransient<IAbpPermissionRepository, AbpPermissionRepository>();
            services.AddTransient<IMdmPermissionService, MdmPermissionService>();
            services.AddTransient<IAbpPermissionService, AbpPermissionService>();       
            
            //var connection = @"Data Source=localhost\\SQLExpress; Initial Catalog=Vtmis_WebAdminDb; Trusted_Connection=True;";
            //services.AddDbContext<WebAdminDbContext>(options => options.UseSqlServer(connection));

            //Configure Abp and Dependency Injection
            return services.AddAbp<WebAdminWebMvcModule>(options =>
            {
                //Configure Log4Net logging
                options.IocManager.IocContainer.AddFacility<LoggingFacility>(
                    f => f.UseAbpLog4Net().WithConfig("log4net.config")
                );
            });
        }

        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            app.UseAbp(); //Initializes ABP framework.
            // TODO: SSLConfig
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    var options = new RewriteOptions()
            //               .AddRedirectToHttps(308, 8082);

            //    app.UseRewriter(options);
            //}
            if (_env.IsEnvironment(HostingEnvironment.LOCAL))
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            var redis = app.ApplicationServices.GetService<IConnectionMultiplexer>();
            SeedHelper.SeedHostDb(iocResolver: IocManager.Instance);
            new RedisSeedHelper(redis, IocManager.Instance).Seed();
            app.UseAuthentication();
            app.UseJwtTokenMiddleware();

#if FEATURE_SIGNALR
            //Integrate to OWIN
            app.UseAppBuilder(ConfigureOwinServices);
#endif

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "defaultWithArea",
                    template: "{area}/{controller=Users}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Users}/{action=Index}/{id?}");
            });
        }

#if FEATURE_SIGNALR
        private static void ConfigureOwinServices(IAppBuilder app)
        {
            app.Properties["host.AppName"] = "WebAdmin";

            app.UseAbp();

            app.MapSignalR();
        }
#endif
    }
}
