﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Vtmis.WebAdmin.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace Vtmis.WebAdmin.Web.Startup
{
    [DependsOn(typeof(WebAdminWebCoreModule))]
    public class WebAdminWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public WebAdminWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<WebAdminNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WebAdminWebMvcModule).GetAssembly());
        }
    }
}