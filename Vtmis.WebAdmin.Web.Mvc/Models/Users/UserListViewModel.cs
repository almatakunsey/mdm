using System.Collections.Generic;
using Vtmis.WebAdmin.Roles.Dto;
using Vtmis.WebAdmin.Users.Dto;

namespace Vtmis.WebAdmin.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}