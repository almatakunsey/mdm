﻿using System.Collections.Generic;
using Vtmis.WebAdmin.Roles.Dto;

namespace Vtmis.WebAdmin.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleDto> Roles { get; set; }

        public ICollection<PermissionDto> Permissions { get; set; }
    }
}
