﻿using Shouldly;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Locations;
using Vtmis.WebAdmin.Locations.Dto;
using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Abp.Application.Services.Dto;
using Abp.ObjectMapping;
using Abp.UI;
using Xunit;
using Vtmis.Core.Common.Exceptions;

namespace Vtmis.WebAdmin.Tests.Locations
{

    public class LocationAppService_Test : WebAdminTestBase
    {
        private readonly ILocationAppService _locationAppService;
        private readonly ILookupNormalizer _lookupNormalizer;
        private readonly IObjectMapper _objectMapper;
        private Guid _locationId;
        private readonly string _locationName;
        public LocationAppService_Test()
        {
            LoginAsDefaultTenantAdmin();
            _objectMapper = Resolve<IObjectMapper>();
            _locationAppService = Resolve<ILocationAppService>();
            _lookupNormalizer = Resolve<ILookupNormalizer>();
            //_locationId = new Guid("00713486-994A-40A4-BFFF-92876B64D3E8"); // Temp solution b4 services can be mock
            _locationName = "New Test Location";
        }

        private async Task CreateLocationAsync()
        {
            await _locationAppService.CreateAsync(new CreateLocationDto
            {
                Name = _locationName,
                Latitude = "12345",
                Longitude = "67890"
            });

            await UsingDbContextAsync(async context =>
            {
                _locationId = (await context.Locations.FirstOrDefaultAsync(e => e.Name == _locationName)).Id;
            });
        }

        [Fact]
        public async Task Should_Create_Location()
        {
            await CreateLocationAsync();
            // Assert
            await UsingDbContextAsync(async context =>
            {
                (await context.Locations.FirstOrDefaultAsync(e => e.Name == _locationName)).ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Return_Location_List()
        {
            await CreateLocationAsync();
            var locations = await _locationAppService.GetListAsync();

            locations.Items.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Return_Location_List_By_UserId()
        {
            await CreateLocationAsync();
            var locations = await _locationAppService.GetListByUserIdAsync(2);

            locations.Items.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Throw_Exceptions_When_Invalid_User_Is_Provided()
        {
            await CreateLocationAsync();
            var locations = await _locationAppService.GetListByUserIdAsync(9999)
                .ShouldThrowAsync<UserFriendlyException>("Invalid user!");            
        }

        [Fact]
        public async Task Should_Return_Location_List_By_UserCurrentUserSession()
        {
            await CreateLocationAsync();
            var locations = await _locationAppService.GetListByCurrentUserSessionAsync();

            locations.Items.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Return_Location_List_By_TenantId()
        {
            await CreateLocationAsync();
            var locations = await _locationAppService.GetListByTenantIdAsync(1);

            locations.Items.Count.ShouldBeGreaterThan(0);
        }



        [Fact]
        public async Task Should_Not_Return_Location_List_When_No_Location_Data_For_The_Tenant()
        {
            await CreateLocationAsync();
            var locations = await _locationAppService.GetListByTenantIdAsync(9999);

            locations.Items.Count.ShouldBe(0);
        }

        [Fact]
        public async Task Should_Return_Location_Detail()
        {
            await CreateLocationAsync();
            await UsingDbContextAsync(async context =>
            {
                var location = await context.Locations.FirstOrDefaultAsync(e => e.Name == _locationName);

                (await _locationAppService.GetDetailAsync(new EntityDto<Guid> { Id = location.Id })).ShouldNotBeNull();
            });
        }

        [Fact]
        public async Task Shoud_Update_Location()
        {
            await CreateLocationAsync();
            var input = new UpdateLocationDto
            {
                Id = _locationId,
                Name = "New Updated Location",
                Latitude = "12345",
                Longitude = "67890"
            };
            await _locationAppService.UpdateAsync(input);
            await UsingDbContextAsync(async context =>
            {              
                var updatedLocation = await context.Locations.FirstOrDefaultAsync(e => e.Id == input.Id);
                updatedLocation.Name.ShouldBeSameAs(input.Name);
            });
        }

        [Fact]
        public async Task Shoud_Not_Update_Location_When_Location_IsNull()
        {
            var input = new UpdateLocationDto
            {
                Id = Guid.NewGuid(),
                Name = "New Updated Location",
                Latitude = "12345",
                Longitude = "67890"
            };

            await UsingDbContextAsync(async context =>
            {
                var location = await context.Locations.FirstOrDefaultAsync(e => e.Id == input.Id);
                location.ShouldBe(null);
                await _locationAppService.UpdateAsync(input)
                    .ShouldThrowAsync<NotFoundException>();
            });

        }

        [Fact]
        public async Task Should_Delete_Location()
        {
            await CreateLocationAsync();
            await UsingDbContextAsync(async context =>
            {
                context.Locations.FirstOrDefaultAsync(e => e.Id == _locationId).ShouldNotBeNull();
                await _locationAppService.DeleteAsync(_locationId);
            });

        }

        [Fact]
        public async Task Should_Throw_Exception_When_Delete_Location()
        {
            await UsingDbContextAsync(async context =>
            {
                var result = await context.Locations.FirstOrDefaultAsync(e => e.Id == Guid.NewGuid());
                result.ShouldBe(null);
            });
            await _locationAppService.DeleteAsync(_locationId)
                .ShouldThrowAsync<NotFoundException>();
        }


    }
}
