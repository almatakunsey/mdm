﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using Vtmis.WebAdmin.Roles;
using Vtmis.WebAdmin.Roles.Dto;
using Xunit;
using System.Linq;

namespace Vtmis.WebAdmin.Tests.RolePermissions
{
    public class RoleAppService_Test : WebAdminTestBase
    {
        private readonly IRoleAppService _roleAppService;

        public RoleAppService_Test()
        {
            _roleAppService = Resolve<IRoleAppService>();
        }

        [Fact]
        public async Task Host_CreateRole_WithNewRoleName_ShouldCreateRole()
        {
            // Act
            var role = await _roleAppService.Create(
                new CreateRoleDto
                {
                    Name = "Editor",
                    DisplayName = "Editor",
                    IsDefault = true,
                    Description = "",
                    IsStatic = true

                });

            await UsingDbContextAsync(async context =>
            {

                var _role = await context.Roles.FirstOrDefaultAsync(u => u.Name == "Editor");
                _role.ShouldNotBeNull();

            });
        }


        [Fact]
        public async Task Tenant_CreateRole_WithNewRoleName_ShouldCreateRole()
        {
            // Act
            var role = await _roleAppService.Create(
                new CreateRoleDto
                {
                    TenantId = 1,
                    Name = "Editor",
                    DisplayName = "Editor",
                    IsDefault = true, Description ="", IsStatic=true
                   
                });

            await UsingDbContextAsync(async context =>
            {
                var _role = await context.Roles.FirstOrDefaultAsync(u => u.Name == "Editor" && u.TenantId == 1);
                _role.ShouldNotBeNull();
                
            });
        }


        [Fact]
        public async Task Tenant_CreateRole_WhenAddTwoNewPermission_ShouldCreatePermissions()
        {
            // Act
            var role = await _roleAppService.Create(
                new CreateRoleDto
                {
                    TenantId = 1,
                    Name = "Editor",
                    DisplayName = "Editor",
                    IsDefault = true,
                    Description = "",
                    IsStatic = true,
                    Permissions = new List<string> { "CanChange", "CanDelete" }
                });

            await UsingDbContextAsync(async context =>
            {
                var _rolePermissions = context.RolePermissions.Where(c => c.RoleId == role.Id);


                _rolePermissions.Count().ShouldBe(2);

                _rolePermissions.ToList()[0].Name.ShouldBe("CanChange");
                _rolePermissions.ToList()[1].Name.ShouldBe("CanDelete");

                await Task<int>.FromResult(0);
            });
        }
    }
}
