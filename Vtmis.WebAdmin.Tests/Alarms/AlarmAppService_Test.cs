﻿using Abp.ObjectMapping;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Alarms;
using Vtmis.WebAdmin.Alarms.Dto;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Filters.v2.Dto;
using Vtmis.WebAdmin.Tests;
using Vtmis.WebAdmin.Zones;
using Vtmis.WebAdmin.Zones.Dto;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Alarms
{

    public class AlarmAppService_Test : WebAdminTestBase
    {
        private readonly IAlarmAppService _alarmAppService;
        private readonly IZoneService _zoneAppService;
        private readonly IFilterAppService _filterAppService;
        private readonly IObjectMapper _objectMapper;
        private int _alarmId;
        private readonly string _alarmName;
        private readonly UserManager _userManager;
        public AlarmAppService_Test()
        {
            LoginAsDefaultTenantAdmin();
            _objectMapper = Resolve<IObjectMapper>();
            _alarmAppService = Resolve<IAlarmAppService>();
            _zoneAppService = Resolve<IZoneService>();
            _filterAppService = Resolve<IFilterAppService>();
            _userManager = Resolve<UserManager>();
            _alarmName = "Alarm 1";
        }


        private async Task CreateNewAlarmAsync()
        {

            await _alarmAppService.CreateAsync(new CreateAlarmDto
            {
                Name = _alarmName,
                AISMessage = "AIS Msg",
                Color = "Red",
                EmailAddress = "user@email.com",
                IsEnabled = true,
                IsSendAISSafetyMessage = true,
                IsSendEmail = true,
                Message = "Test Msg",
                Priority = AlarmPriority.High,
                RepeatInterval = 1,
                AlarmItems = new List<CreateAlarmItemDto>{
                    new CreateAlarmItemDto
                    {
                        ColumnType = "Condition 1",
                        ColumnItem = "Min",
                        ColumnValue = "100",
                        Condition = Condition.AND,
                        Operation = Operation.EQUALS_TO
                    }
                }
            });

            await UsingDbContextAsync(async context =>
            {
                _alarmId = (await context.Alarms.FirstOrDefaultAsync(e => e.Name == _alarmName)).Id;
            });
        }

        private async Task CreateNewAlarmWithZoneAndFilterAsync()
        {
            await _alarmAppService.CreateAsync(new CreateAlarmDto
            {
                Name = _alarmName,
                AISMessage = "AIS Msg",
                Color = "Red",
                EmailAddress = "user@email.com",
                IsEnabled = true,
                IsSendAISSafetyMessage = true,
                IsSendEmail = true,
                Message = "Test Msg",
                Priority = AlarmPriority.High,
                RepeatInterval = 1,
                AlarmItems = new List<CreateAlarmItemDto>{
                    new CreateAlarmItemDto
                    {
                        ColumnType = "Condition 1",
                        ColumnItem = "Zones",
                        ColumnValue = "1",
                        Condition = Condition.OR,
                        Operation = Operation.EQUALS_TO
                    },
                    new CreateAlarmItemDto
                    {
                        ColumnType = "Condition 2",
                        ColumnItem = "Filters",
                        ColumnValue = "1",
                        Condition = Condition.OR,
                        Operation = Operation.EQUALS_TO
                    }
                }
            });

            await UsingDbContextAsync(async context =>
            {
                _alarmId = (await context.Alarms.FirstOrDefaultAsync(e => e.Name == _alarmName)).Id;
            });
        }

        [Fact]
        public void TestEmail()
        {

            SmtpClient client = new SmtpClient("mail.enav.my", 587);
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("mdm@enav.my", "3dmgl0b@lt3ch");

            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress("mdm@enav.my");
            mailMessage.To.Add("mn.hafiz23@gmail.com");
            mailMessage.Body = "My Email";
            mailMessage.Subject = "FROM MDM";
            client.Send(mailMessage);

        }

        [Fact]
        public async Task Should_Create_Alarm()
        {
            await CreateNewAlarmAsync();

            // Assert
            await UsingDbContextAsync(async context =>
            {
                (await context.Alarms.FirstOrDefaultAsync(e => e.Name == _alarmName)).ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Alarm_With_Zone_And_Filter()
        {
            await CreateNewAlarmWithZoneAndFilterAsync();

            await _zoneAppService.Create(new CreateZoneDto
            {
                UserId = (await GetCurrentUserAsync()).Id,
                ZoneDto = new ZoneDto
                {
                    Colour = "",
                    IsEnable = true,
                    IsFill = true,
                    Name = "Zone 1",
                    Radius = 100,
                    ZoneType = ZoneType.Circle,
                    ZoneItems = new List<ResponseZoneItemDto>
                        {
                            new ResponseZoneItemDto
                            {
                                Latitude = "100",
                                Logitude = "200"
                            }
                        }
                }
            });

            await _filterAppService.CreateAsync(new WebAdmin.Filters.v2.Dto.CreateFilterDto
            {
                Name = "New filter",
                IsEnable = true,
                IsShowShips = true,
                FilterItems = new List<CreateFilterItemDto>
                {
                    new CreateFilterItemDto
                    {
                        BorderColour = "Red",
                        Colour = "Black",
                        FilterDetails = new List<CreateFilterDetailDto>
                        {
                            new CreateFilterDetailDto
                            {
                                ColumnName = "MMSI",
                                ColumnValue = "123",
                                Condition = Condition.AND,
                                Operation = "=",
                                RowIndex = 0
                            }
                        }
                    }
                }
            });
            // Assert
            await UsingDbContextAsync(async context =>
            {

                var result = (await context.Alarms.Include(x => x.AlarmItems).FirstOrDefaultAsync(e => e.Name == _alarmName));
                result.ShouldNotBe(null);
                foreach (var item in result.AlarmItems)
                {
                    if (item.ColumnItem.ToUpper() == "ZONES")
                    {
                        var zoneId = Convert.ToInt32(item.ColumnValue);
                        (await context.Zones.FirstOrDefaultAsync(x => x.Id == zoneId)).ShouldNotBe(null);
                    }

                    if (item.ColumnItem.ToUpper() == "FILTERS")
                    {
                        var filterId = Convert.ToInt32(item.ColumnValue);
                        (await context.Filters.FirstOrDefaultAsync(x => x.Id == filterId)).ShouldNotBe(null);
                    }
                }
            });
        }

        [Fact]
        public async Task Should_Return_Alarm_List()
        {
            await CreateNewAlarmAsync();
            var alarms = await _alarmAppService.GetAll();

            alarms.Count().ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Return_Alarm_List_By_UserId()
        {
            await CreateNewAlarmAsync();
            var alarms = await _alarmAppService.GetAllByUserIdAsync(2);

            alarms.ToList().Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Throw_Exceptions_When_Invalid_User_Is_Provided()
        {
            await CreateNewAlarmAsync();
            var alarms = await _alarmAppService.GetAllByUserIdAsync(9999)
                .ShouldThrowAsync<UserFriendlyException>("Invalid user!");
        }

        [Fact]
        public async Task Should_Return_Alarms_List_By_UserCurrentUserSession()
        {
            await CreateNewAlarmAsync();
            var alarms = await _alarmAppService.GetAllByCurrentUserSessionAsync();

            alarms.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Return_Alarms_List_By_TenantId()
        {
            await CreateNewAlarmAsync();
            var alarms = await _alarmAppService.GetAllByTenantIdAsync(1);

            alarms.ToList().Count.ShouldBeGreaterThan(0);
        }



        [Fact]
        public async Task Should_Not_Return_Alarms_List_When_No_Location_Data_For_The_Tenant()
        {
            await CreateNewAlarmAsync();
            var alarms = await _alarmAppService.GetAllByTenantIdAsync(9999);

            alarms.ToList().Count.ShouldBe(0);
        }

        [Fact]
        public async Task Should_Return_Alarm_Detail()
        {
            await CreateNewAlarmAsync();
            await UsingDbContextAsync(async context =>
            {
                var alarm = await context.Alarms.FirstOrDefaultAsync(e => e.Name == _alarmName);

                (await _alarmAppService.GetByIdAsync(alarm.Id)).ShouldNotBe(null);
            });
        }


        [Fact]
        public async Task Should_Delete_Alarm()
        {
            await CreateNewAlarmAsync();
            await UsingDbContextAsync(async context =>
            {
                (await context.Alarms.FirstOrDefaultAsync(e => e.Id == _alarmId && e.IsDeleted == false)).ShouldNotBeNull();
                await _alarmAppService.DeleteAsync(_alarmId);
                var result = (await context.Alarms.FirstOrDefaultAsync(e => e.Id == _alarmId && e.IsDeleted == false));
                result.ShouldBeNull();
            });

        }

        [Fact]
        public async Task Should_Throw_Exception_When_Delete_Alarm()
        {
            await CreateNewAlarmAsync();
            await _alarmAppService.DeleteAsync(9999)
                .ShouldThrowAsync<NotFoundException>();

        }

        [Fact]
        public async Task Shoud_Update_Alarm()
        {
            await CreateNewAlarmAsync();
            var input = new AlarmDto
            {
                Id = _alarmId,
                Name = "New Updated Alarm",
                AISMessage = "test ais message",
                Color = "red",
                IsEnabled = true,
                IsSendAISSafetyMessage = true,
                IsSendEmail = true,
                AlarmItems = new List<AlarmItemDto>{
                    new AlarmItemDto
                    {
                        Id = 1,
                        ColumnType = "Updated Condition 1",
                        ColumnItem = "Zones",
                        ColumnValue = "1",
                        Condition = Condition.OR,
                        Operation = Operation.EQUALS_TO,
                        AlarmId = _alarmId
                    },
                    new AlarmItemDto
                    {
                        ColumnType = "New Condition 2",
                        ColumnItem = "Zones 2",
                        ColumnValue = "1",
                        Condition = Condition.OR,
                        Operation = Operation.EQUALS_TO
                    }
                },
                EmailAddress = "user@gmail.com",
                Message = "test msg",
                Priority = AlarmPriority.High,
                RepeatInterval = 1,
                TenantId = 1
            };
            await _alarmAppService.UpdateAsync(input);
            await UsingDbContextAsync(async context =>
            {
                var updatedAlarm = await context.Alarms.Include(x => x.AlarmItems).FirstOrDefaultAsync(e => e.Id == input.Id);
                updatedAlarm.Name.ShouldBeSameAs(input.Name);
                updatedAlarm.AlarmItems.Count.ShouldBe(2);
                updatedAlarm.AlarmItems.FirstOrDefault(x => x.Id == 1).ColumnItem.ShouldBeSameAs("Zones");
                updatedAlarm.AlarmItems.FirstOrDefault(x => x.Id == 1).ColumnType.ShouldBeSameAs("Updated Condition 1");
                updatedAlarm.AlarmItems.FirstOrDefault(x => x.Id == 2).ColumnItem.ShouldBeSameAs("Zones 2");
                updatedAlarm.AlarmItems.FirstOrDefault(x => x.Id == 2).ColumnType.ShouldBeSameAs("New Condition 2");
            });
        }

        [Fact]
        public async Task Shoud_Not_Update_Alarm_When_Alarm_IsNull()
        {
            await CreateNewAlarmAsync();
            var input = new AlarmDto
            {
                Id = 10
            };

            await UsingDbContextAsync(async context =>
            {
                var alarm = await context.Alarms.FirstOrDefaultAsync(e => e.Id == input.Id);
                alarm.ShouldBe(null);
                await _alarmAppService.UpdateAsync(input)
                    .ShouldThrowAsync<UserFriendlyException>("Could not found the alarm, maybe it's deleted.");
            });

        }
    }
}
