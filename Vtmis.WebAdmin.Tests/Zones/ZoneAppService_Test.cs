﻿using Abp.Domain.Repositories;
using Abp.ObjectMapping;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Zones;
using Vtmis.WebAdmin.Zones.Dto;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Zones
{

    public class ZoneAppService_Test : WebAdminTestBase
    {
        private readonly IZoneService _zoneService;
        private readonly UserManager _userManager;
        private readonly IObjectMapper _objectMapper;
        private readonly CreateZoneDto _newZone_invalid_userId;
        private int _zoneId;
        private readonly string _zoneName;
        public ZoneAppService_Test()
        {
            LoginAsDefaultTenantAdmin();
            _objectMapper = Resolve<IObjectMapper>();
            _zoneService = Resolve<IZoneService>();
            _userManager = Resolve<UserManager>();
            _newZone_invalid_userId = new CreateZoneDto
            {
                UserId = 977782364
            };
            //_zoneId = 2;
            _zoneName = "Zone 1";
        }

        private async Task CreateZoneAsync()
        {
            await _zoneService.Create(new CreateZoneDto
            {
                UserId = 2,
                ZoneDto = new ZoneDto
                {
                    Name = _zoneName,
                    Colour = "Red",
                    IsEnable = true,
                    IsFill = true,
                    Radius = 100,
                    ZoneType = ZoneType.Circle,
                    ZoneItems = new List<ResponseZoneItemDto>
                    {
                       new ResponseZoneItemDto
                       {
                           Latitude = "100.65468464654468",
                           Logitude = "200.234324234"
                       }
                    }
                }
            });

            await UsingDbContextAsync(async context =>
            {
                _zoneId = (await context.Zones.FirstOrDefaultAsync(e => e.Name == _zoneName)).Id;
            });
        }

        [Fact]
        public async Task Should_Create_Zone()
        {
            await CreateZoneAsync();
            // Assert
            await UsingDbContextAsync(async context =>
            {
                (await context.Zones.FirstOrDefaultAsync(e => e.Name == _zoneName)).ShouldNotBe(null);
                var result = await context.Zones.Include(i => i.ZoneItems).FirstOrDefaultAsync(e => e.Name == _zoneName);
                result.ZoneItems.FirstOrDefault().Order.ShouldBe(0);
            });

        }

        //[Fact]
        //public async Task Should_Throw_Exception_In_Create_Zone_When_Invalid_UserId_Is_Specified()
        //{
        //    await CreateZoneAsync();
        //    Should.Throw<UserFriendlyException>(() => _zoneService.Create(_newZone_invalid_userId))
        //                        .Message.ShouldBe("Invalid user!");
        //}

        [Fact]
        public async Task Should_Throw_Exception_In_Create_Zone_When_Duplicate_ZoneName_Is_Specified()
        {
            await CreateZoneAsync();
            Should.Throw<AlreadyExistException>(() => _zoneService.Create(new CreateZoneDto
            {
                UserId = 2,
                ZoneDto = new ZoneDto
                {
                    Name = _zoneName,
                    Colour = "Red",
                    IsEnable = true,
                    IsFill = true,
                    Radius = 100,
                    ZoneType = ZoneType.Circle,
                    ZoneItems = new List<ResponseZoneItemDto>
                    {
                       new ResponseZoneItemDto
                       {
                           Latitude = "100",
                           Logitude = "200"
                       }
                    }
                }
            }))
                                ;
        }

        [Fact]
        public async Task Should_Update_Zone()
        {
            await CreateZoneAsync();
            var input = new UpdateZoneDto
            {
                UserId = 2,
                ZoneDto = new ZoneDto
                {
                    Id = _zoneId,
                    Colour = "Updated Colour",
                    IsEnable = true,
                    IsFill = true,
                    Name = "Update Zone 1",
                    Radius = 100,
                    ZoneType = ZoneType.Circle,
                    ZoneItems = new List<ResponseZoneItemDto>
                    {
                       new ResponseZoneItemDto
                       {
                           Id = 1,
                           Latitude = "100",
                           Logitude = "300",
                           Order = 1
                       },
                        new ResponseZoneItemDto
                       {

                           Latitude = "500",
                           Logitude = "600"
                       },
                       new ResponseZoneItemDto
                       {

                           Latitude = "700",
                           Logitude = "800"
                       }
                    }
                }
            };
            await _zoneService.Update(input);
            await UsingDbContextAsync(async context =>
            {
                var updatedZone = await context.Zones.Include(x => x.ZoneItems).FirstOrDefaultAsync(e => e.Id == input.ZoneDto.Id);
                updatedZone.Name.ShouldBeSameAs(input.ZoneDto.Name);
                updatedZone.ZoneItems.Count.ShouldBe(3);
                foreach (var item in updatedZone.ZoneItems)
                {
                    if (item.Order == 1)
                    {
                        Assert.Equal(100, item.Latitude);
                    }
                    else if (item.Order == 2)
                    {
                        Assert.Equal(500, item.Latitude);
                    }
                    else if (item.Order == 3)
                    {
                        Assert.Equal(700, item.Latitude);
                    }
                }
            });
        }

        [Fact]
        public async Task Should_Delete_Zone()
        {
            await CreateZoneAsync();
            await UsingDbContextAsync(async context =>
            {
                context.Zones.FirstOrDefaultAsync(e => e.Id == _zoneId).ShouldNotBeNull();
                await _zoneService.Delete(_zoneId);
            });
        }

        [Fact]
        public async Task Should_Return_Zone_List()
        {
            await CreateZoneAsync();
            var zones = await _zoneService.GetAllAsync();

            zones.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Return_Zone_List_By_UserId()
        {
            await CreateZoneAsync();
            var zones = await _zoneService.GetAllByUserIdAsync(2);

            zones.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Throw_Exceptions_When_Invalid_User_Is_Provided()
        {
            await CreateZoneAsync();
            var zones = await _zoneService.GetAllByUserIdAsync(9999)
                .ShouldThrowAsync<UserFriendlyException>("Invalid user!");
        }

        [Fact]
        public async Task Should_Return_Alarms_List_By_UserCurrentUserSession()
        {
            await CreateZoneAsync();
            var zones = await _zoneService.GetAllByCurrentUserSessionAsync();

            zones.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Return_Alarms_List_By_TenantId()
        {
            await CreateZoneAsync();
            var zones = await _zoneService.GetAllByTenantIdAsync(1);

            zones.Count.ShouldBeGreaterThan(0);
        }



        [Fact]
        public async Task Should_Not_Return_Alarms_List_When_No_Location_Data_For_The_Tenant()
        {
            await CreateZoneAsync();
            var zones = await _zoneService.GetAllByTenantIdAsync(9999);

            zones.Count.ShouldBe(0);
        }


        [Fact]
        public async Task Should_Return_Zone_Detail()
        {
            await CreateZoneAsync();
            await UsingDbContextAsync(async context =>
            {
                var zone = await context.Zones.FirstOrDefaultAsync(e => e.Name == _zoneName);

                _zoneService.GetById(zone.Id).ShouldNotBeNull();
            });
        }
    }
}