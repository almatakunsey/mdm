﻿using Abp.Dependency;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.ReadOnlyDB;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories;
using Vtmis.WebAdmin.Roles;
using Vtmis.WebAdmin.Roles.Dto;
using Vtmis.WebAdmin.Users;
using Vtmis.WebAdmin.Users.Dto;

namespace Vtmis.WebAdmin.Tests
{
    public class ArrangeSuite : WebAdminTestBase
    {
        private readonly IMdmUserRoleRepository _userRoleRepo;
        private readonly IUserAppService _userAppService;
        private readonly IRoleAppService _roleAppService;


        private readonly UserManager _userManager;

        public ArrangeSuite()
        {
            IDbConnection sql = new SqlConnection($"Server=.,1433;Database=AIS181025;User Id=vessel_sa;Password=P@ssw0rd123;MultipleActiveResultSets=True;ConnectRetryCount=5;");
            LocalIocManager.IocContainer.Kernel.AddComponentInstance<IDbConnection>(sql);
            //LocalIocManager.Register(typeof(IDbConnection), typeof(new SqlConnection($"Server=.,1433;Database=AIS181025;User Id=vessel_sa;Password=P@ssw0rd123;MultipleActiveResultSets=True;ConnectRetryCount=5;")));
            LocalIocManager.Register<IHostingEnvironment, MockHostingEnvironment>(DependencyLifeStyle.Singleton);
            LocalIocManager.Register<WebVessel.Tracking.Services.Interfaces.IRouteService, WebVessel.Tracking.Services.Concrete.RouteService>(DependencyLifeStyle.Singleton);
            LocalIocManager.Register<WebVessel.Tracking.Repository.Interfaces.IDapperAisRepository, WebVessel.Tracking.Repository.Concrete.DapperAisRepository>(DependencyLifeStyle.Singleton);
            //LocalIocManager.Register<MdmAdminDbContext>();
            LocalIocManager.Register<IAbpPermissionRepository, AbpPermissionRepository>();
            _userRoleRepo = Resolve<IMdmUserRoleRepository>();
            _userAppService = Resolve<IUserAppService>();

            _roleAppService = Resolve<IRoleAppService>();
            _userManager = Resolve<UserManager>();
        }

        public async Task<UserDto> CreateNewUser_AndAssign_Role(string name, string email, string roleName)
        {
            await _roleAppService.Create(new CreateRoleDto
            {
                Name = roleName,
                DisplayName = roleName,
                Description = ""

            });

            var user = await _userAppService.Create(
                new CreateUserDto
                {
                    EmailAddress = email,
                    IsActive = true,
                    Name = name,
                    Surname = "Ahmad",
                    Password = "123qwe",
                    UserName = name,
                    TenantId = 1,
                    RoleNames = new string[] { roleName }
                });

            return user;
        }

        public async Task<UserDto> CreateNewUser_AndAssign_Role_WithTenant(string name, string email, string roleName, int tenantId)
        {
            await _roleAppService.Create(new CreateRoleDto
            {
                Name = roleName,
                DisplayName = roleName,
                IsDefault = true,
                Description = "",
                IsStatic = true,
                Permissions = new List<string>() { "CanView" }

            });

            var user = await _userAppService.Create(
                new CreateUserDto
                {
                    EmailAddress = email,
                    IsActive = true,
                    Name = name,
                    Surname = "Ahmad",
                    Password = "123qwe",
                    UserName = name,
                    TenantId = tenantId,
                    RoleNames = new string[] { roleName }
                });

            return user;
        }
    }
}
