﻿using Abp.ObjectMapping;
using System.Collections.Generic;
using System.Threading.Tasks;
using Vtmis.Core.Model.DTO.Image;
using Vtmis.WebAdmin.Image;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Image
{
    public class ImageService_Test : ArrangeSuite
    {
        private readonly IObjectMapper _objectMapper;
        private readonly IImageService _imageService;

        public ImageService_Test()
        {
            _objectMapper = Resolve<IObjectMapper>();
            LoginAsDefaultTenantAdmin();
            _imageService = Resolve<IImageService>();
        }

        //[Fact]
        //public async Task ShouldSaveImage()
        //{
        //    var result = await _imageService.CreateOrUpdateAsync("1234", new List<RequestCreateImage>
        //    {
        //        new RequestCreateImage
        //        {
        //            Name = "cawan.jpg",
        //            Path = "/var/images/",
        //            FullPath = $"/var/images/cawan.jpg"
        //        },
        //        new RequestCreateImage
        //        {
        //            Name = "cawan2.jpg",
        //            Path = "/var/images/",
        //            FullPath = $"/var/images/cawan2.jpg"
        //        }
        //    });

        //    Assert.True(result.ImageCollection.Count > 0);
        //}

        //[Fact]
        //public async Task ShouldUpdateImage()
        //{
        //    var create = await _imageService.CreateOrUpdateAsync("1234", new List<RequestCreateImage>
        //    {
        //        new RequestCreateImage
        //        {
        //            Name = "cawan.jpg",
        //            Path = "/var/images/",
        //            FullPath = $"/var/images/cawan.jpg"
        //        },
        //        new RequestCreateImage
        //        {
        //            Name = "cawan2.jpg",
        //            Path = "/var/images/",
        //            FullPath = $"/var/images/cawan2.jpg"
        //        }
        //    });

        //    var result = await _imageService.CreateOrUpdateAsync("1234", new List<RequestCreateImage>
        //    {
        //        new RequestCreateImage
        //        {
        //            Name = "cawan3.jpg",
        //            Path = "/var/images/",
        //            FullPath = $"/var/images/cawan3.jpg"
        //        },
        //        new RequestCreateImage
        //        {
        //            Name = "cawan4.jpg",
        //            Path = "/var/images/",
        //            FullPath = $"/var/images/cawan4.jpg"
        //        }
        //    });

        //    Assert.True(result.ImageCollection.Count == 4);
        //}
    }
}
