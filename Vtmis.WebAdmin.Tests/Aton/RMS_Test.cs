﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Common.Helpers;
using Xunit;
using Xunit.Abstractions;

namespace Vtmis.WebAdmin.Tests.Aton
{
    public class RMS_Test : ArrangeSuite
    {
        private readonly ITestOutputHelper output;


        public RMS_Test(ITestOutputHelper output)
        {
            LoginAsDefaultTenantAdmin();
            this.output = output;
        }

        [Fact]
        public void ConvertToBinary()
        {          
            var rmsResult = CommonHelper.GetRms(230);
            output.WriteLine(rmsResult.ToString());
        }
    }
}
