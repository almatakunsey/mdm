﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.VesselOwneds;
using Shouldly;
using Xunit;
using System.Linq;
using Vtmis.WebAdmin.EntityFrameworkCore;
using Vtmis.WebAdmin.Users;
using Vtmis.WebAdmin.Users.Dto;
using Vtmis.WebAdmin.Roles.Dto;
using Vtmis.WebAdmin.Roles;
using Vtmis.WebAdmin.VesselOwneds.Dto;

namespace Vtmis.WebAdmin.Tests.VesselOwneds
{
    public class VesselOwnedAppService_Tests : WebAdminTestBase
    {
        private readonly IUserAppService _userAppService;
        private readonly IRoleAppService _roleAppService;

        private readonly IVesselOwnedService _vesselOwnedAppService;
        private readonly UserManager _userManager;

        public VesselOwnedAppService_Tests()
        {
            _userAppService = Resolve<IUserAppService>();
            _roleAppService = Resolve<IRoleAppService>();
            _vesselOwnedAppService = Resolve<IVesselOwnedService>();
            _userManager = Resolve<UserManager>();
        }

        [Fact]
        public async Task AssignNewVessel_WhenRecordNotExist_ShouldInsertOneNewRecord()
        {
            // Act
            await _roleAppService.Create(new CreateRoleDto
            {
                Name = "Editor",
                DisplayName = "Editor",
                IsDefault = true,
                Description = "",
                IsStatic = true

            });

            var user = await _userAppService.Create(
                new CreateUserDto
                {
                    EmailAddress = "azam@gmail.com",
                    IsActive = true,
                    Name = "Azam",
                    Surname = "Ahamad",
                    Password = "123qwe",
                    UserName = "azam",
                    RoleNames = new string[] { "Editor" }
                });

            await _vesselOwnedAppService.CreateAsync(new WebAdmin.VesselOwneds.Dto.AssignVesselToTenantDto
            {
                UserId = user.Id,
                MMSI = new int[] { 12345 }
            });

            // Assert
            await UsingDbContextAsync(async context =>
            {
                var vesselOwneds = context.VesselOwneds.Where(u => u.Username == "azam");

                vesselOwneds.Count().ShouldBe(1);

                await Task<int>.FromResult(0);
            });
        }


        [Fact]
        public async Task AssignNewVessel_WhenRecordExist_ShouldUpdateRecord_and_AddNewMMSIInCollection()
        {
            // Act
            await _roleAppService.Create(new CreateRoleDto
            {
                Name = "Editor",
                DisplayName = "Editor",
                IsDefault = true,
                Description = "",
                IsStatic = true

            });

            var user = await _userAppService.Create(
                new CreateUserDto
                {
                    EmailAddress = "azam@gmail.com",
                    IsActive = true,
                    Name = "Azam",
                    Surname = "Ahamad",
                    Password = "123qwe",
                    UserName = "azam",
                    RoleNames = new string[] { "Editor" }
                });

            await _vesselOwnedAppService.CreateAsync(new WebAdmin.VesselOwneds.Dto.AssignVesselToTenantDto
            {
                UserId = user.Id,

                MMSI = new int[]{ 12345 }
            });

            await _vesselOwnedAppService.CreateAsync(new WebAdmin.VesselOwneds.Dto.AssignVesselToTenantDto
            {
                UserId = user.Id,

                MMSI = new int[]{ 12346, 12347 }
            });

            // Assert
            await UsingDbContextAsync(async context =>
            {
                var vesselOwneds = context.VesselOwneds.Where(u => u.Username == "azam");

                vesselOwneds.Count().ShouldBe(1);

                var vesselArrayId = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VesselArrayIdDto>>(vesselOwneds.FirstOrDefault().MMSI);

                vesselArrayId.Count().ShouldBe(3);

                vesselArrayId[1].MMSI.ShouldBe(12346);

                await Task<int>.FromResult(0);
            });
        }

        [Fact]
        public async Task DeleteAssignVessel_WhenRecordExist_ShouldRemoveMMSIInCollection()
        {
            // Act
            await _roleAppService.Create(new CreateRoleDto
            {
                Name = "Editor",
                DisplayName = "Editor",
                IsDefault = true,
                Description = "",
                IsStatic = true

            });

            var user = await _userAppService.Create(
                new CreateUserDto
                {
                    EmailAddress = "azam@gmail.com",
                    IsActive = true,
                    Name = "Azam",
                    Surname = "Ahamad",
                    Password = "123qwe",
                    UserName = "azam",
                    RoleNames = new string[] { "Editor" }
                });

            await _vesselOwnedAppService.CreateAsync(new WebAdmin.VesselOwneds.Dto.AssignVesselToTenantDto
            {
                UserId = user.Id,

                MMSI = new int[] { 12346 }
            });

            await _vesselOwnedAppService.RemoveAsync(new VesselToTenantAssignedDto { UserId =user.Id ,MMSI = 12346 });

            // Assert
            await UsingDbContextAsync(async context =>
            {
                var vesselOwneds = context.VesselOwneds.Where(u => u.Username == "azam");

                var vesselArrayId = Newtonsoft.Json.JsonConvert.DeserializeObject<List<VesselArrayIdDto>>(vesselOwneds.FirstOrDefault().MMSI);

                vesselArrayId.Count().ShouldBe(0);

                await Task<int>.FromResult(0);
            });
        }
    }

}
