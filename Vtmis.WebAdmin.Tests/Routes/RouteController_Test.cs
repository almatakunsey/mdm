﻿using Abp.ObjectMapping;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.Core.Model.ViewModels.Route.Dto;
using Vtmis.WebAdmin.Routes;
using Vtmis.WebAdmin.Web.Host.Controllers;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Routes
{
    public class RouteController_Test : ArrangeSuite
    {
        private readonly RoutesController _routesController;
        private readonly IRouteService _routeService;
        public readonly Abp.ObjectMapping.IObjectMapper _objectMapper;
        public RouteController_Test()
        {
           
            _objectMapper = Resolve<IObjectMapper>();
            LoginAsDefaultTenantAdmin();
           
            _routesController = Resolve<RoutesController>();
            _routeService = Resolve<IRouteService>();
        }

        private async Task<ResponseRouteDto> Create()
        {
            return await _routeService.CreateAsync(new RequestCreateRoute
            {
                Name = "Route 1",
                IsEnable = true,
                Color = "Red",
                Waypoints = new List<RequestWaypointDto>
                {
                    new RequestWaypointDto
                    {
                        AverageSpeed = 1,
                        IsShown = true,
                        Latitude = 10,
                        Longitude = 120,
                        MaxSpeed = 10,
                        MinSpeed = 1,
                        Name = "WP1",
                        XteLeft = 1,
                        XteRight = 2
                    },
                    new RequestWaypointDto
                    {
                        AverageSpeed = 1,
                        IsShown = true,
                        Latitude = 30,
                        Longitude = 60,
                        MaxSpeed = 10,
                        MinSpeed = 1,
                        Name = "WP2",
                        XteLeft = 1,
                        XteRight = 2
                    }
                }
            });
        }

        [Fact]
        public async Task Get_Should_Return_BadRequest_If_Route_Does_Not_Exist_When_Get_Route()
        {
            await Create();
            var controllerResult = await _routesController.Get(999);

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        [Fact]
        public async Task Get_Should_GetAllRoutes()
        {
            await Create();
            var controllerResult = await _routesController.Get();

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var results = Assert.IsAssignableFrom<PaginatedList<ResponseRouteDto>>(
                okResult.Value);

            Assert.Equal(1, results.TotalItems);
            Assert.Equal(2, results.Items.FirstOrDefault().Waypoints.Count);
        }

        [Fact]
        public async Task Get_Should_Get_Route_By_Id()
        {
            await Create();
            var controllerResult = await _routesController.Get(1);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<ResponseRouteDto>(
                okResult.Value);

            Assert.Same(result.Name, "Route 1");
            Assert.Equal(2, result.Waypoints.Count);
        }

        [Fact]
        public async Task Put_Should_Update_Route()
        {           
            var newRoute = await Create();
            var input = _objectMapper.Map<RequestUpdateRouteDto>(newRoute);
            input.Waypoints.AddRange(
                new List<RequestUpdateWaypointDto> {
                    new RequestUpdateWaypointDto
                    {
                        AverageSpeed = 1,
                        IsShown = true,
                        Latitude = 21,
                        Longitude = 121,
                        MaxSpeed = 10,
                        MinSpeed = 1,
                        Name = "WP3",
                        XteLeft = 1,
                        XteRight = 2
                    },
                    new RequestUpdateWaypointDto
                    {
                        AverageSpeed = 1,
                        IsShown = true,
                        Latitude = 31,
                        Longitude = 55,
                        MaxSpeed = 10,
                        MinSpeed = 1,
                        Name = "WP4",
                        XteLeft = 1,
                        XteRight = 2
                    }
                });

            input.Name = "update route 1";
            var controllerResult = await _routesController.Put(1, input);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<ResponseRouteDto>(
                okResult.Value);

            Assert.Same("update route 1", result.Name);
            Assert.Equal(4, result.Waypoints.Count);
            Assert.Equal("WP1", result.Waypoints[0].Name);
            Assert.Equal("WP2", result.Waypoints[1].Name);
            Assert.Equal("WP3", result.Waypoints[2].Name);
            Assert.Equal("WP4", result.Waypoints[3].Name);
        }

        [Fact]
        public async Task Put_Should_Return_NotFound_If_Route_Does_Not_Exist_When_Update_Route()
        {
          
            var controllerResult = await _routesController.Put(999, new RequestUpdateRouteDto { Id = 999 });
            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

    }
}
