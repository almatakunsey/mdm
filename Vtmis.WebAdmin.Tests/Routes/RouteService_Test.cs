﻿using Abp.Dependency;
using Abp.ObjectMapping;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.AkkaModel.Messages.Route;
using Vtmis.Core.Model.ViewModels.Route.Dto;
using Vtmis.WebAdmin.Routes;
using Xunit;
using Xunit.Abstractions;

namespace Vtmis.WebAdmin.Tests.Routes
{

    public class RouteService_Test : ArrangeSuite
    {
        public readonly IRouteService _routeService;
        public readonly WebVessel.Tracking.Services.Interfaces.IRouteService _mapTrackRouteService;
        public readonly Abp.ObjectMapping.IObjectMapper _objectMapper;
        private readonly ITestOutputHelper _output;

        public RouteService_Test(ITestOutputHelper output)
        {
            _objectMapper = Resolve<IObjectMapper>();
            LoginAsDefaultTenantAdmin();
            _routeService = Resolve<IRouteService>();
            _mapTrackRouteService = Resolve<WebVessel.Tracking.Services.Interfaces.IRouteService>();
            _output = output;
        }


        [Fact]
        public void Date()
        {
            DateTime? a = DateTime.Now;
            if (a > null)
            {

            }
        }

        [Fact]
        public async Task CalculateRouteETA()
        {
            var obj = await _mapTrackRouteService.RouteEtaAsync(new RequestRouteEta(new ResponseRouteDto
            {
                Id = 1,
                Color = "red",
                IsEnable = true,
                Name = "Kuala Kedah - Langkawi",
                Waypoints = new List<WaypointDto>
                {
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 2,
                         IsShown = true,
                         Latitude = 6.30468268692958,
                         Longitude = 99.843393266201,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "WP2",
                         Order = 1,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 3,
                         IsShown = true,
                         Latitude = 6.27398560591668,
                         Longitude = 99.8625791072845,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Pulau Bumbun",
                         Order = 2,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 4,
                         IsShown = true,
                         Latitude = 6.26760814951058,
                         Longitude = 99.8693490028381,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "WP4",
                         Order = 3,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 5,
                         IsShown = true,
                         Latitude = 6.26474999993063,
                         Longitude = 99.8729538917542,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "WP5",
                         Order = 4,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    },
                    new WaypointDto
                    {
                         AverageSpeed = 15,
                         Id = 1,
                         IsShown = true,
                         Latitude = 6.30580240024859,
                         Longitude = 99.8491841554642,
                         MaxSpeed = 25,
                         MinSpeed = 3,
                         Name = "Kuah",
                         Order = 0,
                         RouteId = 1,
                         XteLeft = 1852,
                         XteRight = 1852
                    }
                }
            }, Convert.ToDateTime("2018-10-25 08:00:00"), 1, 10));
            var result = JsonConvert.SerializeObject(obj);
            _output.WriteLine(result);
        }

        private async Task<ResponseRouteDto> Create()
        {
            return await _routeService.CreateAsync(new RequestCreateRoute
            {
                Name = "Route 1",
                IsEnable = true,
                Color = "Red",
                Waypoints = new List<RequestWaypointDto>
                {
                    new RequestWaypointDto
                    {
                        AverageSpeed = 1,
                        IsShown = true,
                        Latitude = 10,
                        Longitude = 120,
                        MaxSpeed = 10,
                        MinSpeed = 1,
                        Name = "WP1",
                        XteLeft = 1,
                        XteRight = 2
                    },
                    new RequestWaypointDto
                    {
                        AverageSpeed = 1,
                        IsShown = true,
                        Latitude = 30,
                        Longitude = 50,
                        MaxSpeed = 10,
                        MinSpeed = 1,
                        Name = "WP2",
                        XteLeft = 1,
                        XteRight = 2
                    }
                }
            });
        }

        [Fact]
        public async Task Should_Create_Route()
        {
            //await CreateFilter();
            await Create();
            await UsingDbContextAsync(async context =>
            {
                var route = await context.Routes.Include(x => x.Waypoints).FirstOrDefaultAsync(e => e.Name == "Route 1"
                      && e.UserId == AbpSession.UserId);

                Assert.NotNull(route);
                Assert.Same(route.Name, "Route 1");
                Assert.Equal(2, route.Waypoints.Count);
            });
        }

        [Fact]
        public async Task Should_Throw_Error_If_RouteName_Already_Exist_In_Tenant_When_Create_Route()
        {
            await Create();
            await Assert.ThrowsAsync<AlreadyExistException>(() => Create());
        }

        [Fact]
        public async Task Should_Throw_Error_If_User_Have_No_Create_Permission()
        {
            RemovePermission(2, MdmPermissionsConst.Route_Create);
            await Assert.ThrowsAsync<ForbiddenException>(() => Create());
        }

        [Fact]
        public async Task Should_Get_All_Routes()
        {
            await Create();
            var results = await _routeService.GetAllAsync();
            Assert.Equal(1, results.TotalItems);
            Assert.Equal(2, results.Items.FirstOrDefault().Waypoints.Count);
        }

        [Fact]
        public async Task Should_Get_Route_By_Id()
        {
            await Create();
            var result = await _routeService.GetAsync(1);
            Assert.NotNull(result);
            Assert.Equal(2, result.Waypoints.Count);
        }

        [Fact]
        public async Task Should_Throw_Error_If_Route_Does_Not_Exist_When_GetRouteById()
        {
            await Assert.ThrowsAsync<NotFoundException>(() => _routeService.GetAsync(1));
        }

        [Fact]
        public async Task Should_Throw_Error_If_Route_Does_Not_Exist_When_UpdateRoute()
        {
            await Assert.ThrowsAsync<NotFoundException>(() => _routeService.UpdateAsync(new RequestUpdateRouteDto { Id = 1 }));
        }

        //[Fact]
        //public async Task Should_Throw_Error_If_RouteName_Already_Exist_In_Tenant_When_UpdateRoute()
        //{
        //    await Create();
        //    await Assert.ThrowsAsync<NotFoundException>(() => _routeService.UpdateAsync(new RequestUpdateRouteDto
        //    {
        //        Id = 1,
        //        Name = "Route 1"
        //    }));
        //}

        [Fact]
        public async Task Should_Update_Route()
        {
            var newRoute = await Create();
            var input = _objectMapper.Map<RequestUpdateRouteDto>(newRoute);
            input.Waypoints.AddRange(
                new List<RequestUpdateWaypointDto> {
                    new RequestUpdateWaypointDto
                    {
                        AverageSpeed = 1,
                        IsShown = true,
                        Latitude = 11,
                        Longitude = 122,
                        MaxSpeed = 10,
                        MinSpeed = 1,
                        Name = "WP3",
                        XteLeft = 1,
                        XteRight = 2
                    },
                    new RequestUpdateWaypointDto
                    {
                        AverageSpeed = 1,
                        IsShown = true,
                        Latitude = 31,
                        Longitude = 55,
                        MaxSpeed = 10,
                        MinSpeed = 1,
                        Name = "WP4",
                        XteLeft = 1,
                        XteRight = 2
                    }
                });

            input.Name = "update route 1";
            var result = await _routeService.UpdateAsync(input);
            Assert.Same("update route 1", result.Name);
            Assert.Equal(4, result.Waypoints.Count);
            Assert.Equal("WP1", result.Waypoints[0].Name);
            Assert.Equal("WP2", result.Waypoints[1].Name);
            Assert.Equal("WP3", result.Waypoints[2].Name);
            Assert.Equal("WP4", result.Waypoints[3].Name);
        }


        [Fact]
        public void Test()
        {
            var a = new List<RequestWaypointDto> {
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP1",
                        Latitude = (float)6.30595702713597,
                        Longitude = (float)99.85023021698,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP2",
                        Latitude = (float)6.30429345268573,
                        Longitude = (float)99.8474836349487,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP3",
                        Latitude = (float)6.3020753451182,
                        Longitude = (float)99.84679698944097,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP4",
                        Latitude = (float)6.29465314701088,
                        Longitude = (float)99.8495435714722,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP5",
                        Latitude = (float)6.28859587225336,
                        Longitude = (float)99.853835105896,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP6",
                        Latitude = (float)6.28808398664476,
                        Longitude = (float)99.8584699630737,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP7",
                        Latitude = (float)6.2997719156837,
                        Longitude = (float)99.8717737197876,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP8",
                        Latitude = (float)6.31205675444332,
                        Longitude = (float)99.8829317092896,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP9",
                        Latitude = (float)6.31734597032662,
                        Longitude = (float)99.88593578338626,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP10",
                        Latitude = (float)6.320502412118062,
                        Longitude = (float)99.8981237411499,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP11",
                        Latitude = (float)6.36179030576602,
                        Longitude = (float)100.014853477478,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP12",
                        Latitude = (float)6.40085720063285,
                        Longitude = (float)100.097680091858,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP13",
                        Latitude = (float)6.40060131364412,
                        Longitude = (float)100.126690864563,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP14",
                        Latitude = (float)6.39863950913861,
                        Longitude = (float)100.128149986267,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    }
                };
            var pointA = new PointF();
            var pointB = new PointF();
            var target = new PointF
            {
                X = (float)3.02308,
                Y = (float)103.79137333
            };
            for (int i = 0; i < a.Count - 1; i++)
            {
                pointA.X = (float)a[i].Latitude;
                pointA.Y = (float)a[i].Longitude;
                if (i == (a.Count - 1))
                {
                    pointB.X = (float)a[i - 1].Latitude;
                    pointB.Y = (float)a[i - 1].Longitude;
                }
                else
                {
                    pointB.X = (float)a[i + 1].Latitude;
                    pointB.Y = (float)a[i + 1].Longitude;
                }
                var result =
                   (CommonHelper.IsInRange(target, pointA, pointB, a[i].XteLeft) ||
                   CommonHelper.IsInRange(target, pointA, pointB, a[i].XteRight));
                Console.WriteLine($"{a[i].Name} || {result}");
            }
        }

        [Fact]
        public async Task Should_Get_Route_ETA()
        {
            await _routeService.CreateAsync(new RequestCreateRoute
            {
                Color = "red",
                IsEnable = true,
                Name = "Route 1",
                Waypoints = new List<RequestWaypointDto> {
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP1",
                        Latitude = (float)6.30595702713597,
                        Longitude = (float)99.85023021698,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP2",
                        Latitude = (float)6.30429345268573,
                        Longitude = (float)99.8474836349487,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP3",
                        Latitude = (float)6.3020753451182,
                        Longitude = (float)99.84679698944097,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP4",
                        Latitude = (float)6.29465314701088,
                        Longitude = (float)99.8495435714722,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP5",
                        Latitude = (float)6.28859587225336,
                        Longitude = (float)99.853835105896,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP6",
                        Latitude = (float)6.28808398664476,
                        Longitude = (float)99.8584699630737,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP7",
                        Latitude = (float)6.2997719156837,
                        Longitude = (float)99.8717737197876,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP8",
                        Latitude = (float)6.31205675444332,
                        Longitude = (float)99.8829317092896,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP9",
                        Latitude = (float)6.31734597032662,
                        Longitude = (float)99.88593578338626,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP10",
                        Latitude = (float)6.320502412118062,
                        Longitude = (float)99.8981237411499,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP11",
                        Latitude = (float)6.36179030576602,
                        Longitude = (float)100.014853477478,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP12",
                        Latitude = (float)6.40085720063285,
                        Longitude = (float)100.097680091858,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP13",
                        Latitude = (float)6.40060131364412,
                        Longitude = (float)100.126690864563,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    },
                    new RequestWaypointDto{
                        AverageSpeed = 1,
                        IsShown = true,
                        Name = "WP14",
                        Latitude = (float)6.39863950913861,
                        Longitude = (float)100.128149986267,
                        MinSpeed = 0.5,
                        MaxSpeed = 100,
                        XteLeft = 4,
                        XteRight = 4
                    }
                }
            });
            //var result = await _routeService.GetAsync(1, 1, 10);
            //Assert.True(result.Count() > 0);
        }
    }
}
