﻿using Vtmis.WebAdmin.Filters;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Filters.v2.Dto;
using System.Collections.Generic;
using Vtmis.Core.Common.Enums;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Vtmis.Core.Common.Exceptions;
using Abp.ObjectMapping;
using System.Linq;

namespace Vtmis.WebAdmin.Tests.Filters
{
    public class CorrectFilterData
    {
        public static IEnumerable<object[]> UpdateFilter =>
            new List<object[]>
            {
                new object[]
                {
                    new UpdateFilterDto
                        {
                            Id = 1,
                            Name = "Updated Filter",
                            IsEnable = true,
                            IsShowShips = true,
                            FilterItems = new List<UpdateFilterItemDto>
                             {
                                 new UpdateFilterItemDto
                                 {
                                     Id = 1,
                                     BorderColour = "red",
                                     Colour = "red",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 1,
                                             ColumnName = "MMSI",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 2,
                                             ColumnName = "CallSign",
                                             ColumnValue = "X01",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new UpdateFilterItemDto
                                 {
                                     Id = 2,
                                     BorderColour = "blue",
                                     Colour = "blue",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 3,
                                             ColumnName = "IMO",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 4,
                                             ColumnName = "ShipType",
                                             ColumnValue = "Fishing",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 }
                             }
                        },
                    2,
                    "red",
                    "blue",
                    null,
                    4,
                    "MMSI",
                    "CallSign",
                    "IMO",
                    "ShipType",
                    null
                },
                new object[]
                {
                    new UpdateFilterDto
                        {
                            Id = 1,
                            Name = "Updated Filter",
                            IsEnable = true,
                            IsShowShips = true,
                            FilterItems = new List<UpdateFilterItemDto>
                             {
                                 new UpdateFilterItemDto
                                 {
                                     Id = 1,
                                     BorderColour = "green",
                                     Colour = "green",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 1,
                                             ColumnName = "MMSI",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 2,
                                             ColumnName = "CallSign",
                                             ColumnValue = "X01",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new UpdateFilterItemDto
                                 {
                                     Id = 2,
                                     BorderColour = "blue",
                                     Colour = "blue",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 3,
                                             ColumnName = "IMO",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 4,
                                             ColumnName = "ShipType",
                                             ColumnValue = "Fishing",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 }
                             }
                        },
                    2,
                    "green",
                    "blue",
                    null,
                    4,
                    "MMSI",
                    "CallSign",
                    "IMO",
                    "ShipType",
                    null
                },
                new object[]
                {
                    new UpdateFilterDto
                        {
                            Id = 1,
                            Name = "Updated Filter",
                            IsEnable = true,
                            IsShowShips = true,
                            FilterItems = new List<UpdateFilterItemDto>
                             {
                                 new UpdateFilterItemDto
                                 {
                                     Id = 1,
                                     BorderColour = "green",
                                     Colour = "green",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 1,
                                             ColumnName = "MMSI",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 2,
                                             ColumnName = "CallSign",
                                             ColumnValue = "X01",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                             }
                        },
                    1,
                    "green",
                    "blue",
                    null,
                    2,
                    "MMSI",
                    "CallSign",
                    "IMO",
                    "ShipType",
                    null
                },
                new object[]
                {
                    new UpdateFilterDto
                        {
                            Id = 1,
                            Name = "Updated Filter",
                            IsEnable = true,
                            IsShowShips = true,
                            FilterItems = new List<UpdateFilterItemDto>
                             {
                                 new UpdateFilterItemDto
                                 {
                                     Id = 1,
                                     BorderColour = "green",
                                     Colour = "green",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 1,
                                             ColumnName = "MMSI",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 2,
                                             ColumnName = "CallSign",
                                             ColumnValue = "X01",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new UpdateFilterItemDto
                                 {
                                     Id = 2,
                                     BorderColour = "blue",
                                     Colour = "blue",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 3,
                                             ColumnName = "IMO",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 4,
                                             ColumnName = "ShipType",
                                             ColumnValue = "Fishing",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new UpdateFilterItemDto
                                 {
                                     BorderColour = "black",
                                     Colour = "black",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             ColumnName = "MessageId",
                                             ColumnValue = "12",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         }
                                     }
                                 }
                             }
                        },
                    3,
                    "green",
                    "blue",
                    "black",
                    5,
                    "MMSI",
                    "CallSign",
                    "IMO",
                    "ShipType",
                    "MessageId"
                }
            };
        public static IEnumerable<object[]> Data =>
            new List<object[]>
            {
                new object[]
                {
                    new CreateFilterDto
                        {
                            Name = "New Filter",
                            IsEnable = true,
                            IsShowShips = true,
                            FilterItems = new List<CreateFilterItemDto>
                             {
                                 new CreateFilterItemDto
                                 {
                                     BorderColour = "red",
                                     Colour = "red",
                                     FilterDetails = new List<CreateFilterDetailDto>
                                     {
                                         new CreateFilterDetailDto
                                         {
                                             ColumnName = "MMSI",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new CreateFilterDetailDto
                                         {
                                             ColumnName = "CallSign",
                                             ColumnValue = "X01",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new CreateFilterItemDto
                                 {
                                     BorderColour = "blue",
                                     Colour = "blue",
                                     FilterDetails = new List<CreateFilterDetailDto>
                                     {
                                         new CreateFilterDetailDto
                                         {
                                             ColumnName = "IMO",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new CreateFilterDetailDto
                                         {
                                             ColumnName = "ShipType",
                                             ColumnValue = "Fishing",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 }
                             }
                        }
                }
            };
    }
    public class FilterAppServiceV2_Test : ArrangeSuite
    {
        public readonly IFilterAppService _filterAppService;
        public readonly IFilterGroupService _filterGroupService;
        public readonly IRepository<FilterGroup> _filterGroup;
        public readonly Abp.ObjectMapping.IObjectMapper _objectMapper;
        public FilterAppServiceV2_Test()
        {
            _objectMapper = Resolve<IObjectMapper>();
            LoginAsDefaultTenantAdmin();
            _filterAppService = Resolve<IFilterAppService>();
            _filterGroupService = Resolve<IFilterGroupService>();
        }

        private async Task<FilterResponseDto> CreateFilter()
        {
            return await _filterAppService.CreateAsync(new CreateFilterDto
            {
                Name = "New Filter",
                IsEnable = true,
                IsShowShips = true,
                FilterItems = new List<CreateFilterItemDto>
                 {
                     new CreateFilterItemDto
                     {
                         BorderColour = "red",
                         Colour = "red",
                         FilterDetails = new List<CreateFilterDetailDto>
                         {
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "MMSI",
                                 ColumnValue = "1234",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 1
                             },
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "CallSign",
                                 ColumnValue = "X01",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 2
                             }
                         }
                     },
                     new CreateFilterItemDto
                     {
                         BorderColour = "blue",
                         Colour = "blue",
                         FilterDetails = new List<CreateFilterDetailDto>
                         {
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "IMO",
                                 ColumnValue = "1234",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 1
                             },
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "ShipType",
                                 ColumnValue = "Fishing",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 2
                             }
                         }
                     }
                 }
            });
        }

        [Fact]
        public async Task Should_Throw_Error_If_Filter_NotFound_When_GetFilter()
        {
            var created = await CreateFilter();

            await Assert.ThrowsAsync<NotFoundException>(() => _filterAppService.GetByIdAsync(999));
        }

        [Fact]
        public async Task Should_Get_Filter_By_FilterId()
        {
            await CreateFilter();
            var result = await _filterAppService.GetByIdAsync(1);
            Assert.Same(result.Name, "New Filter");
            Assert.False(result.FilterItems.Any());
        }

        [Fact]
        public async Task Should_Get_Filter_By_FilterId_IncludingFilterItems()
        {
            await CreateFilter();
            var result = await _filterAppService.GetByIdAsync(1, true);
            Assert.Same(result.Name, "New Filter");
            Assert.True(result.FilterItems.Any());
            Assert.False(result.FilterItems.FirstOrDefault().FilterDetails.Any());
        }

        [Fact]
        public async Task Should_Get_Filter_By_FilterId_IncludingFilterItems_And_FilterDetails()
        {
            await CreateFilter();
            var result = await _filterAppService.GetByIdAsync(1, true, true);
            Assert.Same(result.Name, "New Filter");
            Assert.True(result.FilterItems.Any());
            Assert.True(result.FilterItems.FirstOrDefault().FilterDetails.Any());
        }

        [Fact]
        public async Task Should_Get_All_Filters_By_Current_LoggedIn_User()
        {
            await CreateFilter();
            var results = await _filterAppService.GetAllAsync();
            Assert.True(results.TotalItems > 0);
            Assert.Equal(1, results.TotalItems);
            Assert.False(results.Items.FirstOrDefault().FilterItems.Any());
        }

        [Fact]
        public async Task Should_Get_All_Filters_IncludingFilterItems_By_Current_LoggedIn_User()
        {
            await CreateFilter();
            var results = await _filterAppService.GetAllAsync(false, true);
            Assert.True(results.TotalItems > 0);
            Assert.Equal(1, results.TotalItems);
            Assert.True(results.Items.FirstOrDefault().FilterItems.Any());
            Assert.Equal(2, results.Items.FirstOrDefault().FilterItems.Count);
        }

        [Fact]
        public async Task Should_Get_All_Filters_IncludingFilterItems_And_IncludingFilterDetails_By_Current_LoggedIn_User()
        {
            await CreateFilter();
            var results = await _filterAppService.GetAllAsync(false, true, true);
            Assert.True(results.TotalItems > 0);
            Assert.Equal(1, results.TotalItems);
            Assert.True(results.Items.FirstOrDefault().FilterItems.Any());
            Assert.Equal(2, results.Items.FirstOrDefault().FilterItems.Count);
            Assert.True(results.Items.FirstOrDefault().FilterItems.FirstOrDefault().FilterDetails.Any());
            Assert.Equal(2, results.Items.FirstOrDefault().FilterItems.FirstOrDefault().FilterDetails.Count);
        }

        [Theory]
        [MemberData(nameof(CorrectFilterData.Data), MemberType = typeof(CorrectFilterData))]
        public async Task Should_Create_Filter(CreateFilterDto input)
        {
            //await CreateFilter();
            await _filterAppService.CreateAsync(input);
            await UsingDbContextAsync(async context =>
            {
                var _filter = await context.Filters.FirstOrDefaultAsync(e => e.Name == "New Filter"
                    && e.UserId == AbpSession.UserId);

                Assert.NotNull(_filter);
                Assert.Same(_filter.Name, "New Filter");
            });
        }

        [Fact]
        public async Task Should_Throw_Error_If_FilterName_Already_Exist_In_Tenant_When_Create_Filter()
        {
            await CreateFilter();
            await Assert.ThrowsAsync<AlreadyExistException>(() => CreateFilter());
        }

        [Theory]
        [MemberData(nameof(CorrectFilterData.UpdateFilter), MemberType = typeof(CorrectFilterData))]
        public async Task Should_Update_Filter(UpdateFilterDto input, int expectedNumOfFilterItems, string
                expectedBorderColor1, string expectedBorderColor2, string expectedBorderColor3, int expectedNumOfFilterDetails, string
                expectedColumnName1, string expectedColumnName2, string expectedColumnName3, string expectedColumnName4,
                string expectedColumnName5)
        {
            await CreateFilter();
            var result = await _filterAppService.UpdateAsync(input);
            await UsingDbContextAsync(async context =>
            {
                var _filter = await context.Filters.Include("FilterItems")
                                                .Include("FilterItems.FilterDetails")
                        .FirstOrDefaultAsync(e => e.Id == input.Id);
                var filterItems = _filter.FilterItems.ToList();
                Assert.NotNull(_filter);
                Assert.Equal(filterItems.Count, expectedNumOfFilterItems);
                Assert.Equal(filterItems[0].BorderColour, expectedBorderColor1);
                if (expectedNumOfFilterItems > 1)
                {
                    Assert.Equal(filterItems[1].BorderColour, expectedBorderColor2);
                    if (expectedNumOfFilterItems > 2)
                    {
                        Assert.Equal(filterItems[2].BorderColour, expectedBorderColor3);
                    }
                }

                var listOfFilterDetails = new List<string>();
                foreach (var item in filterItems)
                {
                    foreach (var item2 in item.FilterDetails)
                    {
                        listOfFilterDetails.Add(item2.ColumnName);
                    }
                }
                Assert.Equal(listOfFilterDetails.Count, expectedNumOfFilterDetails);
                Assert.Same(expectedColumnName1, listOfFilterDetails[0]);
                Assert.Same(expectedColumnName2, listOfFilterDetails[1]);

                if (expectedNumOfFilterDetails > 2)
                {
                    Assert.Same(expectedColumnName3, listOfFilterDetails[2]);
                    Assert.Same(expectedColumnName4, listOfFilterDetails[3]);

                    if (expectedNumOfFilterDetails > 4)
                    {
                        Assert.Same(expectedColumnName5, listOfFilterDetails[4]);
                    }
                }

            });
        }

        //[Fact]
        //public async Task Should_Throw_Error_If_FilterName_Already_Exist_In_Tenant_When_Update_Filter()
        //{
        //    var created = await CreateFilter();
        //    var updatedFilter = new UpdateFilterDto
        //    {
        //        Id = 1,
        //        Name = created.Name,
        //        IsEnable = true,
        //        IsShowShips = true,
        //        FilterItems = new List<UpdateFilterItemDto>
        //                     {
        //                         new UpdateFilterItemDto
        //                         {
        //                             Id = 1,
        //                             BorderColour = "green",
        //                             Colour = "green",
        //                             FilterDetails = new List<UpdateFilterDetailDto>
        //                             {
        //                                 new UpdateFilterDetailDto
        //                                 {
        //                                     Id = 1,
        //                                     ColumnName = "MMSI",
        //                                     ColumnValue = "1234",
        //                                     Condition = Condition.AND,
        //                                     Operation = "=",
        //                                     RowIndex = 1
        //                                 },
        //                                 new UpdateFilterDetailDto
        //                                 {
        //                                     Id = 2,
        //                                     ColumnName = "CallSign",
        //                                     ColumnValue = "X01",
        //                                     Condition = Condition.AND,
        //                                     Operation = "=",
        //                                     RowIndex = 2
        //                                 }
        //                             }
        //                         },
        //                         new UpdateFilterItemDto
        //                         {
        //                             Id = 2,
        //                             BorderColour = "blue",
        //                             Colour = "blue",
        //                             FilterDetails = new List<UpdateFilterDetailDto>
        //                             {
        //                                 new UpdateFilterDetailDto
        //                                 {
        //                                     Id = 3,
        //                                     ColumnName = "IMO",
        //                                     ColumnValue = "1234",
        //                                     Condition = Condition.AND,
        //                                     Operation = "=",
        //                                     RowIndex = 1
        //                                 },
        //                                 new UpdateFilterDetailDto
        //                                 {
        //                                     Id = 4,
        //                                     ColumnName = "ShipType",
        //                                     ColumnValue = "Fishing",
        //                                     Condition = Condition.AND,
        //                                     Operation = "=",
        //                                     RowIndex = 2
        //                                 }
        //                             }
        //                         },
        //                         new UpdateFilterItemDto
        //                         {
        //                             BorderColour = "black",
        //                             Colour = "black",
        //                             FilterDetails = new List<UpdateFilterDetailDto>
        //                             {
        //                                 new UpdateFilterDetailDto
        //                                 {
        //                                     ColumnName = "MessageId",
        //                                     ColumnValue = "12",
        //                                     Condition = Condition.AND,
        //                                     Operation = "=",
        //                                     RowIndex = 1
        //                                 }
        //                             }
        //                         }
        //                     }
        //    };
        //    await Assert.ThrowsAsync<AlreadyExistException>(() => _filterAppService.UpdateAsync(updatedFilter));
        //}

        [Fact]
        public async Task Should_Throw_Error_If_Filter_NotFound_When_Update_Filter()
        {
            var created = await CreateFilter();
            var updatedFilter = new UpdateFilterDto
            {
                Id = 999,
                Name = created.Name,
                IsEnable = true,
                IsShowShips = true,
                FilterItems = new List<UpdateFilterItemDto>
                             {
                                 new UpdateFilterItemDto
                                 {
                                     Id = 1,
                                     BorderColour = "green",
                                     Colour = "green",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 1,
                                             ColumnName = "MMSI",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 2,
                                             ColumnName = "CallSign",
                                             ColumnValue = "X01",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new UpdateFilterItemDto
                                 {
                                     Id = 2,
                                     BorderColour = "blue",
                                     Colour = "blue",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 3,
                                             ColumnName = "IMO",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 4,
                                             ColumnName = "ShipType",
                                             ColumnValue = "Fishing",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new UpdateFilterItemDto
                                 {
                                     BorderColour = "black",
                                     Colour = "black",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             ColumnName = "MessageId",
                                             ColumnValue = "12",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         }
                                     }
                                 }
                             }
            };
            await Assert.ThrowsAsync<NotFoundException>(() => _filterAppService.UpdateAsync(updatedFilter));
        }

        [Fact]
        public async Task Should_SoftDelete_Filter()
        {
            var created = await CreateFilter();
            await _filterAppService.DeleteAsync(created.Id);

            await UsingDbContextAsync(async context =>
            {
                var _filter = await context.Filters.FirstOrDefaultAsync(e => e.Id == created.Id && e.IsDeleted == false);
                Assert.Null(_filter);
            });
        }


        [Fact]
        public async Task Should_Throw_NotFoundException_When_Delete_Filter()
        {
            var created = await CreateFilter();
            await Assert.ThrowsAsync<NotFoundException>(() => _filterAppService.DeleteAsync(9999));
        }

        //[Fact]
        //public async Task CreateNewFilter_WhenNoSameRecordExist_ShouldInsert()
        //{
        //    var user = await CreateNewUser_AndAssign_Role("azam", "azam@gmail.com", "Editor");

        //    var _filterResponse = await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //         Name = "Filter Vessel Only", UserId = user.Id
        //    });

        //    _filterResponse.Name.ShouldBe("Filter Vessel Only");

        //    await UsingDbContextAsync(async context =>
        //    {
        //        var _filter = await context.Filters.FirstOrDefaultAsync(e => e.Name == "Filter Vessel Only" && e.UserId == user.Id);

        //        _filter.ShouldNotBeNull();
        //        _filter.Name.ShouldBe("Filter Vessel Only");
        //    });
        //}

        //[Fact]
        //public async Task CreateNewFilter_WhenSameNameAndUserIdExist_ShouldReturnException()
        //{
        //    var user = await CreateNewUser_AndAssign_Role("azam", "azam@gmail.com", "Editor");

        //    await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user.Id
        //    });


        //    var errorMsg = await Should.ThrowAsync<Abp.UI.UserFriendlyException>(async () => await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user.Id
        //    }));

        //    errorMsg.Message.ShouldBe("Save filter error");
        //}

        //[Fact]
        //public async Task CreateNewFilter_WhenAddFilterItem_ShouldAddFilterItem()
        //{
        //    //Arrange
        //    var user = await CreateNewUser_AndAssign_Role("azam", "azam@gmail.com", "Editor");


        //    //Act
        //    var filter = await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user.Id, FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="80" }
        //        }
        //    });

        //    //Assert
        //    await UsingDbContextAsync(async context =>
        //    {
        //        var _filter = await context.Filters.Include(i=>i.FilterItems)
        //            .FirstOrDefaultAsync(e => e.Id == filter.Id);

        //        _filter.FilterItems.Count().ShouldBe(1);
        //    });

        //}

        //[Fact]
        //public async Task CreateNewFilter_WhenAddFilterItemWithData_ShouldSaveFileterItemData()
        //{
        //    //Arrange
        //    var user = await CreateNewUser_AndAssign_Role_WithTenant("azam", "azam@gmail.com", "Editor",2);

        //    //Act
        //    var filter = await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="80" },

        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "MMSI", Operator="=", Value ="1234" }
        //        }
        //    });


        //    //Assert
        //    await UsingDbContextAsync(async context =>
        //    {
        //        var _filter = await context.Filters.Include(i => i.FilterItems)
        //            .FirstOrDefaultAsync(e => e.Id == filter.Id);

        //        //_filter.FilterItems.ToList()[0].ColumnName.ShouldBe("Speed");
        //        //_filter.FilterItems.ToList()[1].ColumnName.ShouldBe("MMSI");
        //    });
        //}

        //[Fact]
        //public async Task TenantAdmin_FilterByName_ShouldReturnFilterInSameTenant()
        //{
        //    //Arrange
        //    var user1 = await CreateNewUser_AndAssign_Role_WithTenant("azam", "azam@gmail.com", "Editor", 2);

        //    var user2 = await CreateNewUser_AndAssign_Role_WithTenant("man", "man@gmail.com", "Viewer", 3);


        //    var filter1 = await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user1.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="80" },

        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "MMSI", Operator="=", Value ="1234" }
        //        }
        //    });

        //    await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Tanker Vessel Only",
        //        UserId = user1.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="80" },

        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "MMSI", Operator="=", Value ="1235" }
        //        }
        //    });

        //    var filter2 = await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user2.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="81" },


        //        }
        //    });

        //    //Act
        //    var _queryFilter1 = await _filterAppService.GetFilterByName(new WebAdmin.Filters.v2.Dto.FilterByNameRequest { UserId = user1.Id, FilterName = filter1.Name });

        //    var _queryFilter2 = await _filterAppService.GetFilterByName(new WebAdmin.Filters.v2.Dto.FilterByNameRequest { UserId = user2.Id, FilterName = filter2.Name });


        //    //Assert
        //    _queryFilter1.Count().ShouldBe(1);

        //    _queryFilter2.Count().ShouldBe(1);
        //}

        //[Fact]
        //public async Task TenantAdmin_FilterByUserId_ShouldReturnFilterInSameTenant()
        //{
        //    //Arrange
        //    var user1 = await CreateNewUser_AndAssign_Role_WithTenant("azam", "azam@gmail.com", "Editor", 2);

        //    var user2 = await CreateNewUser_AndAssign_Role_WithTenant("man", "man@gmail.com", "Viewer", 3);


        //    var filter1 = await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user1.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="80" },

        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "MMSI", Operator="=", Value ="1234" }
        //        }
        //    });

        //    await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Tanker Vessel Only",
        //        UserId = user1.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="80" },

        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "MMSI", Operator="=", Value ="1235" }
        //        }
        //    });

        //    var filter2 = await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user2.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="81" },


        //        }
        //    });

        //    //Act
        //    var _queryFilter1 = await _filterAppService.GetFilterByUserId(new WebAdmin.Filters.v2.Dto.FilterByUserIdRequest { UserId = user1.Id });

        //    var _queryFilter2 = await _filterAppService.GetFilterByUserId(new WebAdmin.Filters.v2.Dto.FilterByUserIdRequest { UserId = user2.Id });


        //    //Assert
        //    _queryFilter1.Count().ShouldBe(2);

        //    _queryFilter2.Count().ShouldBe(1);
        //}

        //[Fact]
        //public async Task TenantAdmin_FilterDetailByUserId_ShouldReturnFIlterAndFilterItemsInSameTenat()
        //{
        //    var user1 = await CreateNewUser_AndAssign_Role_WithTenant("azam", "azam@gmail.com", "Editor", 2);

        //    var user2 = await CreateNewUser_AndAssign_Role_WithTenant("man", "man@gmail.com", "Viewer", 3);

        //    //Create Group
        //    await _filterGroupService.Create(new WebAdmin.Filters.Dto.FilterGroupRequestNew
        //    {
        //        Name = "Group Base", MMSIList = new List<string> { "1234","5678","9101"}, UserId = user1.Id
        //    });

        //    var filter1 = await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user1.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="80" },

        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "MMSI", Operator="=", Value ="1234" },

        //             new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Group", Operator="=", Value ="Group Base" }
        //        }
        //    });

        //    await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Tanker Vessel Only",
        //        UserId = user1.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="80" },

        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "MMSI", Operator="=", Value ="1235" }
        //        }
        //    });

        //    var filter2 = await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user2.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="81" },


        //        }
        //    });

        //    var _filter = await _filterAppService.GetFilterDetailByUserId(new WebAdmin.Filters.v2.Dto.FilterDetailByUserIdRequest { UserId = user1.Id, FilterName = filter1.Name });

        //    _filter.FilterItemResponses.Count.ShouldBe(3);
        //}

        //[Fact]
        //public async Task TenantAdmin_FilterDetailByUserId_WithFilterHasGroup_ShouldReturnFilterGroupInfo()
        //{
        //    var user1 = await CreateNewUser_AndAssign_Role_WithTenant("azam", "azam@gmail.com", "Editor", 2);

        //    var user2 = await CreateNewUser_AndAssign_Role_WithTenant("man", "man@gmail.com", "Viewer", 3);

        //    //Create Group
        //    await _filterGroupService.Create(new WebAdmin.Filters.Dto.FilterGroupRequestNew
        //    {
        //        Name = "Group Base",
        //        MMSIList = new List<string> { "1234", "5678", "9101" },
        //        UserId = user1.Id
        //    });

        //    var filter1 = await _filterAppService.Create(new WebAdmin.Filters.v2.Dto.FilterRequest
        //    {
        //        Name = "Filter Vessel Only",
        //        UserId = user1.Id,
        //        FilterItemRequests = new List<WebAdmin.Filters.v2.Dto.FilterItemRequest>
        //        {
        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Speed", Operator="=", Value ="80" },

        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "MMSI", Operator="=", Value ="1234" },

        //            new WebAdmin.Filters.v2.Dto.FilterItemRequest{ AndOrOperator = "And",

        //            Name = "Group", Operator="=", Value ="Group Base" }
        //        }
        //    });

        //    var _filter = await _filterAppService.GetFilterDetailByUserId(new WebAdmin.Filters.v2.Dto.FilterDetailByUserIdRequest { UserId = user1.Id, FilterName = filter1.Name });

        //    _filter.FilterItemResponses[2].Name.ShouldBe("Group");
        //    _filter.FilterItemResponses[2].GroupResponses[0].MMSIs.Count().ShouldBe(3);
        //}
    }
}
