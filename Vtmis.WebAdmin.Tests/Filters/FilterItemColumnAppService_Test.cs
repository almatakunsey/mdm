﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Shouldly;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Roles;
using Vtmis.WebAdmin.Users;
using Xunit;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Vtmis.WebAdmin.Tests.Filters
{
    public class FilterItemColumnAppService_Test :ArrangeSuite
    {
        private readonly IFilterItemColumnAppService _iFilterItemColumnAppService;
        private readonly IRoleAppService _roleAppService;

        private readonly UserManager _userManager;

        public FilterItemColumnAppService_Test()
        {
            LoginAsDefaultTenantAdmin();
            _iFilterItemColumnAppService = Resolve<IFilterItemColumnAppService>();
            _roleAppService = Resolve<IRoleAppService>();
            _userManager = Resolve<UserManager>();
        }

        [Fact]
        public async Task CreateNewFilterItemColumn_WhenNoSameRecordExist_ShouldInsert()
        {
            //Arrange
            var user = await GetCurrentUserAsync();

            //Act
            var filterItemColumn = await _iFilterItemColumnAppService.Create(new WebAdmin.Filters.Dto.FilterItemColumnRequest { Name = "Vessel Only", UserId = user.Id });

            await UsingDbContextAsync(async context =>
            {
                var _filterItemColumn = await context.FilterItemColumns.FirstOrDefaultAsync(e => e.Name == filterItemColumn.Name);

                _filterItemColumn.ShouldNotBeNull();
            });
        }

        [Fact]
        public async Task GetListFilterItemColumn_WithTwoRecordsExist_ShouldReturnTwoRecords()
        {
            //Arrange
            var user = await GetCurrentUserAsync();

            //Act
            await _iFilterItemColumnAppService.Create(new WebAdmin.Filters.Dto.FilterItemColumnRequest { Name = "Vessel Only", UserId = user.Id });
            await _iFilterItemColumnAppService.Create(new WebAdmin.Filters.Dto.FilterItemColumnRequest { Name = "Tanker Only", UserId = user.Id });

            var listResult = await _iFilterItemColumnAppService.GetAll();

            listResult.Count().ShouldBe(2);
        }
    }
}
