﻿using System.Collections.Generic;
using Xunit;
using Shouldly;
using Vtmis.WebAdmin.Authorization.Users;
using Vtmis.WebAdmin.Filters;
using System.Threading.Tasks;
using System.Linq;
using Abp.Domain.Repositories;

namespace Vtmis.WebAdmin.Tests.Filters
{
    public  class FilterGroupAppService_Test : ArrangeSuite
    {
        private readonly IFilterGroupService _filterGroupService;
        private readonly IRepository<FilterGroup> _filterGroupRepository;

        private readonly UserManager _userManager;

        public FilterGroupAppService_Test()
        {
            LoginAsDefaultTenantAdmin();
            _filterGroupService = Resolve<IFilterGroupService>();
            _userManager = Resolve<UserManager>();
            _filterGroupRepository = Resolve<IRepository<FilterGroup>>();
        }

        [Fact]
        public async Task Tenant_CreateNewFilterGroup_WhenRecordNotExist_ShouldInsertIntoDatabase()
        {
            //Arrange
            var user = await GetCurrentUserAsync();

            //Act
            var filterGroup = await _filterGroupService.Create(new WebAdmin.Filters.Dto.FilterGroupRequestNew
            {
                 Name = "Group Vessel Cargo", TenantId = AbpSession.TenantId, UserId = user.Id
            });

            //Assert
            await UsingDbContextAsync(async context =>
            {
                var _filterGroup = context.FilterGroups.Where(u => u.Id == filterGroup.Id);

                _filterGroup.Count().ShouldBe(1);

                _filterGroup.FirstOrDefault().Name = "Group Vessel Cargo";

                await Task<int>.FromResult(0);
            });

            //var filterGroupRepo = await _filterGroupRepository.FirstOrDefaultAsync(c => c.Name == "Group Vessel Cargo");
            //filterGroupRepo.Name.ShouldBe("Group Vessel Cargo");
            

        }

        [Fact]
        public async Task Tenant_CreateNewFilterGroup_WhenRecordExist_ShouldRaiseException()
        {
            //Arrange
            //AbpSession.TenantId = 1;

            var user = await GetCurrentUserAsync();

            await _filterGroupService.Create(new WebAdmin.Filters.Dto.FilterGroupRequestNew
            {
                Name = "Group Vessel Cargo",
                TenantId = AbpSession.TenantId,
                UserId = user.Id
            });

            //Act
           
            var errorMsg = await Should.ThrowAsync<Abp.UI.UserFriendlyException>(async () => await _filterGroupService.Create(new WebAdmin.Filters.Dto.FilterGroupRequestNew
            {
                Name = "Group Vessel Cargo",
                TenantId = AbpSession.TenantId,
                UserId = user.Id
            }));

            errorMsg.Message.ShouldBe("Save filter group error");
        }

        [Fact]
        public async Task Tenant_CreateNewFilterGroup_WhenAddedMMSIList_ShouldSaveIntoMMSIList()
        {
            //Arrange
            var user = await GetCurrentUserAsync();

            //Act
            var filterGroup = await _filterGroupService.Create(new WebAdmin.Filters.Dto.FilterGroupRequestNew
            {
                Name = "Group Vessel Cargo",
                TenantId = AbpSession.TenantId,
                UserId = user.Id,
                MMSIList = new List<string> { "1234567", "1234568" }
            });

            await UsingDbContextAsync(async context =>
            {
                var _filterGroup = context.FilterGroups.Where(u => u.Id == filterGroup.Id).FirstOrDefault();

                var mmsiArray = Newtonsoft.Json.JsonConvert.DeserializeObject<List<string>>(_filterGroup.MMSIList);

                mmsiArray.Count.ShouldBe(2);
                mmsiArray[0].ShouldBe("1234567");
            });
        }
    }
}
