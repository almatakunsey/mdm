﻿using Abp.ObjectMapping;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Filters.Dto;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Filters
{
    public class FilterAppService_Test : WebAdminTestBase
    {
        //private readonly IFilterService _filterService;
        private readonly IObjectMapper _objectMapper;
        private int _filterId;
        private readonly string _filterName;
        public FilterAppService_Test()
        {
            LoginAsDefaultTenantAdmin();
            _objectMapper = Resolve<IObjectMapper>();
            _filterService = Resolve<IFilterService>();
            _filterName = "Filter 1";
        }

        private async Task CreateFilterAsync()
        {
            await _filterService.Create(new CreateFilter
            {
                UserId = 2,
                FilterDto = new FilterDto
                {
                    Name = _filterName,
                    FilterItemDtos = new List<FilterItemDto>
                    {
                        new FilterItemDto
                        {
                            AISType = "class a",
                            BorderColor = "#2434",
                            CallSign = "ab",
                            Color = "#234",
                            DataSource = "ds",
                            GroupType = new FilterGroupListResponse
                            {
                                Name = "Filter Group Type 1"                                
                            },
                            IsEnable = true,
                            IsShowShips = true,
                            MaxAge = 20,
                            MaxLength = 100,
                            MaxSpeed = 200,
                            MinAge = 1,
                            MinCourse = 34,
                            MinLength = 1,
                            MinSpeed = 1,
                            MMSI = 3423434,
                            ShipName = "Ship 1",
                            ShipType = "Fishing"                            
                        }
                    }
                }
            });

            await UsingDbContextAsync(async context =>
            {
                _filterId = (await context.Filters.FirstOrDefaultAsync(e => e.Name == _filterName)).Id;
            });
        }

        [Fact]
        public async Task Should_Return_Filter_List()
        {
            await CreateFilterAsync();
            var filters = await _filterService.GetAllAsync();

            filters.Count().ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Return_Filter_List_By_UserId()
        {
            await CreateFilterAsync();
            var filters = await _filterService.GetAllByUserIdAsync(2);

            filters.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Throw_Exceptions_When_Invalid_User_Is_Provided()
        {
            await CreateFilterAsync();
            var filters = await _filterService.GetAllByUserIdAsync(9999)
                .ShouldThrowAsync<UserFriendlyException>("Invalid user!");
        }

        [Fact]
        public async Task Should_Return_Filters_List_By_UserCurrentUserSession()
        {
            await CreateFilterAsync();
            var filters = await _filterService.GetAllByCurrentUserSessionAsync();

            filters.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task Should_Return_Filters_List_By_TenantId()
        {
            await CreateFilterAsync();
            var filters = await _filterService.GetAllByTenantIdAsync(1);

            filters.Count.ShouldBeGreaterThan(0);
        }



        [Fact]
        public async Task Should_Not_Return_Filter_List_When_No_Filter_Data_Provided_For_The_Tenant()
        {
            await CreateFilterAsync();
            var filters = await _filterService.GetAllByTenantIdAsync(9999);

            filters.Count.ShouldBe(0);
        }

        [Fact]
        public async Task Should_Return_Filter_Detail()
        {
            await CreateFilterAsync();
            await UsingDbContextAsync(async context =>
            {
                var filter = await context.Filters.FirstOrDefaultAsync(e => e.Name == _filterName);

                (await _filterService.GetById(filter.Id)).ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Create_Filter()
        {
            await CreateFilterAsync();

            // Assert
            await UsingDbContextAsync(async context =>
            {
                (await context.Filters.FirstOrDefaultAsync(e => e.Name == _filterName)).ShouldNotBe(null);
            });
        }

        [Fact]
        public async Task Should_Delete_Filter()
        {
            await CreateFilterAsync();
            await UsingDbContextAsync(async context =>
            {
                context.Filters.FirstOrDefaultAsync(e => e.Id == _filterId).ShouldNotBeNull();
                await _filterService.Delete(_filterId);
            });

        }
        
        [Fact]
        public async Task Shoud_Update_Filter()
        {
            await CreateFilterAsync();
            var input = new UpdateFilter
            {
                UserId = 2,
                FilterDto = new FilterDto
                {
                    Id = _filterId,
                    Name = "Update Filter"
                    
                }
            };
            await _filterService.Update(input);
            await UsingDbContextAsync(async context =>
            {
                var updatedFilter = await context.Filters.FirstOrDefaultAsync(e => e.Id == input.FilterDto.Id);
                updatedFilter.Name.ShouldBeSameAs(input.FilterDto.Name);
            });
        }
       
    }
}
