﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Filters.v2.Dto;
using Vtmis.WebAdmin.Web.Host.Controllers;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Filters
{
    public class FiltersController_Test : ArrangeSuite
    {
        private readonly FiltersController _filterController;
        private readonly IFilterAppService _filterAppService;

        public FiltersController_Test()
        {
            LoginAsDefaultTenantAdmin();
            _filterController = Resolve<FiltersController>();
            _filterAppService = Resolve<IFilterAppService>();
        }

        private async Task<FilterResponseDto> CreateFilter()
        {
            return await _filterAppService.CreateAsync(new CreateFilterDto
            {
                Name = "New Filter",
                IsEnable = true,
                IsShowShips = true,
                FilterItems = new List<CreateFilterItemDto>
                 {
                     new CreateFilterItemDto
                     {
                         BorderColour = "red",
                         Colour = "red",
                         FilterDetails = new List<CreateFilterDetailDto>
                         {
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "MMSI",
                                 ColumnValue = "1234",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 1
                             },
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "CallSign",
                                 ColumnValue = "X01",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 2
                             }
                         }
                     },
                     new CreateFilterItemDto
                     {
                         BorderColour = "blue",
                         Colour = "blue",
                         FilterDetails = new List<CreateFilterDetailDto>
                         {
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "IMO",
                                 ColumnValue = "1234",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 1
                             },
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "ShipType",
                                 ColumnValue = "Fishing",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 2
                             }
                         }
                     }
                 }
            });
        }

        [Fact]
        public async Task Get_Should_Return_NotFound_If_Filter_Does_Not_Exist_When_Get_Filter()
        {
            await CreateFilter();
            var controllerResult = await _filterController.Get(999);

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        [Fact]
        public async Task Get_Should_GetAllFilters_By_Current_LoggedIn_User()
        {
            await CreateFilter();
            var controllerResult = _filterController.GetAsync(false, false);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var results = Assert.IsAssignableFrom<PaginatedList<FilterResponseDto>>(
                okResult.Value);

            Assert.True(results.TotalItems > 0);
            Assert.Equal(1, results.TotalItems);
            Assert.False(results.Items.FirstOrDefault().FilterItems.Any());
        }

        [Fact]
        public async Task Get_Should_GetAllFilters_IncludingFilterItems_By_Current_LoggedIn_User()
        {
            await CreateFilter();
            var controllerResult = _filterController.GetAsync(true);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var results = Assert.IsAssignableFrom<PaginatedList<FilterResponseDto>>(
                okResult.Value);

            Assert.True(results.TotalItems > 0);
            Assert.Equal(1, results.TotalItems);
            Assert.True(results.Items.FirstOrDefault().FilterItems.Any());
            Assert.Equal(2, results.Items.FirstOrDefault().FilterItems.Count);
        }

        [Fact]
        public async Task Get_Should_GetAllFilters_IncludingFilterItems_And_FilterDetails_By_Current_LoggedIn_User()
        {
            await CreateFilter();
            var controllerResult = _filterController.GetAsync(true, true);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var results = Assert.IsAssignableFrom<PaginatedList<FilterResponseDto>>(
                okResult.Value);

            Assert.True(results.TotalItems > 0);
            Assert.Equal(1, results.TotalItems);
            Assert.True(results.Items.FirstOrDefault().FilterItems.Any());
            Assert.Equal(2, results.Items.FirstOrDefault().FilterItems.Count);
            Assert.True(results.Items.FirstOrDefault().FilterItems.FirstOrDefault().FilterDetails.Any());
            Assert.Equal(2, results.Items.FirstOrDefault().FilterItems.FirstOrDefault().FilterDetails.Count);
        }

        [Fact]
        public async Task Get_Should_Get_Filter_By_FilterId()
        {
            await CreateFilter();
            var controllerResult = await _filterController.Get(1, false, false);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<FilterResponseDto>(
                okResult.Value);

            Assert.Same(result.Name, "New Filter");
            Assert.False(result.FilterItems.Any());
        }

        [Fact]
        public async Task Get_Should_Get_Filter_By_FilterId_IncludingFilterItems()
        {
            await CreateFilter();
            var controllerResult = await _filterController.Get(1, true, false);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<FilterResponseDto>(
                okResult.Value);

            Assert.Same(result.Name, "New Filter");
            Assert.True(result.FilterItems.Any());
            Assert.False(result.FilterItems.FirstOrDefault().FilterDetails.Any());
        }

        [Fact]
        public async Task Get_Should_Get_Filter_By_FilterId_IncludingFilterItems_And_FilterDetails()
        {
            await CreateFilter();
            var controllerResult = await _filterController.Get(1, true, true);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<FilterResponseDto>(
                okResult.Value);

            Assert.Same(result.Name, "New Filter");
            Assert.True(result.FilterItems.Any());
            Assert.True(result.FilterItems.FirstOrDefault().FilterDetails.Any());
        }

        [Fact]
        public async Task Post_Should_Create_Filter()
        {
            var input = new CreateFilterDto
            {
                Name = "New Filter",
                IsEnable = true,
                IsShowShips = true,
                FilterItems = new List<CreateFilterItemDto>
                 {
                     new CreateFilterItemDto
                     {
                         BorderColour = "red",
                         Colour = "red",
                         FilterDetails = new List<CreateFilterDetailDto>
                         {
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "MMSI",
                                 ColumnValue = "1234",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 1
                             },
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "CallSign",
                                 ColumnValue = "X01",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 2
                             }
                         }
                     },
                     new CreateFilterItemDto
                     {
                         BorderColour = "blue",
                         Colour = "blue",
                         FilterDetails = new List<CreateFilterDetailDto>
                         {
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "IMO",
                                 ColumnValue = "1234",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 1
                             },
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "ShipType",
                                 ColumnValue = "Fishing",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 2
                             }
                         }
                     }
                 }
            };
            var controllerResult = await _filterController.Post(input);

            var okResult = Assert.IsType<CreatedResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.Created, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<FilterResponseDto>(
                okResult.Value);

            Assert.Same("New Filter", result.Name);
        }

        [Fact]
        public async Task Post_Should_Return_BadRequest_If_FilterName_Already_Exist_In_Tenant_When_Create_Filter()
        {
            var input = new CreateFilterDto
            {
                Name = "New Filter",
                IsEnable = true,
                IsShowShips = true,
                FilterItems = new List<CreateFilterItemDto>
                 {
                     new CreateFilterItemDto
                     {
                         BorderColour = "red",
                         Colour = "red",
                         FilterDetails = new List<CreateFilterDetailDto>
                         {
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "MMSI",
                                 ColumnValue = "1234",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 1
                             },
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "CallSign",
                                 ColumnValue = "X01",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 2
                             }
                         }
                     },
                     new CreateFilterItemDto
                     {
                         BorderColour = "blue",
                         Colour = "blue",
                         FilterDetails = new List<CreateFilterDetailDto>
                         {
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "IMO",
                                 ColumnValue = "1234",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 1
                             },
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "ShipType",
                                 ColumnValue = "Fishing",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 2
                             }
                         }
                     }
                 }
            };
            await CreateFilter();
            var controllerResult = await _filterController.Post(input);

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        [Fact]
        public async Task Put_Should_Update_Filter()
        {
            await CreateFilter();
            var updatedFilter = new UpdateFilterDto
            {
                Id = 1,
                Name = "Updated Filter",
                IsEnable = true,
                IsShowShips = true,
                FilterItems = new List<UpdateFilterItemDto>
                             {
                                 new UpdateFilterItemDto
                                 {
                                     Id = 1,
                                     BorderColour = "green",
                                     Colour = "green",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 1,
                                             ColumnName = "MMSI",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 2,
                                             ColumnName = "CallSign",
                                             ColumnValue = "X01",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new UpdateFilterItemDto
                                 {
                                     Id = 2,
                                     BorderColour = "blue",
                                     Colour = "blue",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 3,
                                             ColumnName = "IMO",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 4,
                                             ColumnName = "ShipType",
                                             ColumnValue = "Fishing",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new UpdateFilterItemDto
                                 {
                                     BorderColour = "black",
                                     Colour = "black",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             ColumnName = "MessageId",
                                             ColumnValue = "12",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         }
                                     }
                                 }
                             }
            };
            var controllerResult = await _filterController.Put(1, updatedFilter);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<FilterResponseDto>(
                okResult.Value);

            Assert.Same("Updated Filter", result.Name);
        }

        //[Fact]
        //public async Task Put_Should_Return_BadRequest_If_FilterName_Already_Exist_In_Tenant_When_Update_Filter()
        //{
        //    await CreateFilter();
        //    var updatedFilter = new UpdateFilterDto
        //    {
        //        Id = 1,
        //        Name = "New Filter",
        //        IsEnable = true,
        //        IsShowShips = true,
        //        FilterItems = new List<UpdateFilterItemDto>
        //                     {
        //                         new UpdateFilterItemDto
        //                         {
        //                             Id = 1,
        //                             BorderColour = "green",
        //                             Colour = "green",
        //                             FilterDetails = new List<UpdateFilterDetailDto>
        //                             {
        //                                 new UpdateFilterDetailDto
        //                                 {
        //                                     Id = 1,
        //                                     ColumnName = "MMSI",
        //                                     ColumnValue = "1234",
        //                                     Condition = Condition.AND,
        //                                     Operation = "=",
        //                                     RowIndex = 1
        //                                 },
        //                                 new UpdateFilterDetailDto
        //                                 {
        //                                     Id = 2,
        //                                     ColumnName = "CallSign",
        //                                     ColumnValue = "X01",
        //                                     Condition = Condition.AND,
        //                                     Operation = "=",
        //                                     RowIndex = 2
        //                                 }
        //                             }
        //                         },
        //                         new UpdateFilterItemDto
        //                         {
        //                             Id = 2,
        //                             BorderColour = "blue",
        //                             Colour = "blue",
        //                             FilterDetails = new List<UpdateFilterDetailDto>
        //                             {
        //                                 new UpdateFilterDetailDto
        //                                 {
        //                                     Id = 3,
        //                                     ColumnName = "IMO",
        //                                     ColumnValue = "1234",
        //                                     Condition = Condition.AND,
        //                                     Operation = "=",
        //                                     RowIndex = 1
        //                                 },
        //                                 new UpdateFilterDetailDto
        //                                 {
        //                                     Id = 4,
        //                                     ColumnName = "ShipType",
        //                                     ColumnValue = "Fishing",
        //                                     Condition = Condition.AND,
        //                                     Operation = "=",
        //                                     RowIndex = 2
        //                                 }
        //                             }
        //                         },
        //                         new UpdateFilterItemDto
        //                         {
        //                             BorderColour = "black",
        //                             Colour = "black",
        //                             FilterDetails = new List<UpdateFilterDetailDto>
        //                             {
        //                                 new UpdateFilterDetailDto
        //                                 {
        //                                     ColumnName = "MessageId",
        //                                     ColumnValue = "12",
        //                                     Condition = Condition.AND,
        //                                     Operation = "=",
        //                                     RowIndex = 1
        //                                 }
        //                             }
        //                         }
        //                     }
        //    };
        //    var controllerResult = await _filterController.Put(1,updatedFilter);

        //    var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
        //    Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        //}

        [Fact]
        public async Task Put_Should_Return_NotFound_If_Filter_Does_Not_Exist_When_Update_Filter()
        {
            await CreateFilter();
            var updatedFilter = new UpdateFilterDto
            {
                Id = 999,
                Name = "New Filter",
                IsEnable = true,
                IsShowShips = true,
                FilterItems = new List<UpdateFilterItemDto>
                             {
                                 new UpdateFilterItemDto
                                 {
                                     Id = 1,
                                     BorderColour = "green",
                                     Colour = "green",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 1,
                                             ColumnName = "MMSI",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 2,
                                             ColumnName = "CallSign",
                                             ColumnValue = "X01",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new UpdateFilterItemDto
                                 {
                                     Id = 2,
                                     BorderColour = "blue",
                                     Colour = "blue",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 3,
                                             ColumnName = "IMO",
                                             ColumnValue = "1234",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         },
                                         new UpdateFilterDetailDto
                                         {
                                             Id = 4,
                                             ColumnName = "ShipType",
                                             ColumnValue = "Fishing",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 2
                                         }
                                     }
                                 },
                                 new UpdateFilterItemDto
                                 {
                                     BorderColour = "black",
                                     Colour = "black",
                                     FilterDetails = new List<UpdateFilterDetailDto>
                                     {
                                         new UpdateFilterDetailDto
                                         {
                                             ColumnName = "MessageId",
                                             ColumnValue = "12",
                                             Condition = Condition.AND,
                                             Operation = "=",
                                             RowIndex = 1
                                         }
                                     }
                                 }
                             }
            };
            var controllerResult = await _filterController.Put(999, updatedFilter);

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        [Fact]
        public async Task Delete_Should_Delete_Filter()
        {
            await CreateFilter();
            var controllerResult = await _filterController.Delete(1);

            var okResult = Assert.IsType<OkResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);
        }

        [Fact]
        public async Task Delete_Should_Throw_NotFoundException_When_Delete_Filter()
        {
            await CreateFilter();
            var controllerResult = await _filterController.Delete(999);

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        //TODO: Unit test model validation
        //[Fact]
        //public void Post_Should_Throw_Validation_Error_If_FilterName_IsNotProvided_When_Create_Filter()
        //{
        //    var input = new CreateFilterDto
        //    {
        //        IsEnable = true,
        //        IsShowShips = true,
        //        FilterItems = new List<CreateFilterItemDto>
        //         {
        //             new CreateFilterItemDto
        //             {
        //                 BorderColour = "red",
        //                 Colour = "red",
        //                 FilterDetails = new List<CreateFilterDetailDto>
        //                 {
        //                     new CreateFilterDetailDto
        //                     {
        //                         ColumnName = "MMSI",
        //                         ColumnValue = "1234",
        //                         Condition = Condition.AND,
        //                         Operation = "=",
        //                         RowIndex = 1
        //                     },
        //                     new CreateFilterDetailDto
        //                     {
        //                         ColumnName = "CallSign",
        //                         ColumnValue = "X01",
        //                         Condition = Condition.AND,
        //                         Operation = "=",
        //                         RowIndex = 2
        //                     }
        //                 }
        //             },
        //             new CreateFilterItemDto
        //             {
        //                 BorderColour = "blue",
        //                 Colour = "blue",
        //                 FilterDetails = new List<CreateFilterDetailDto>
        //                 {
        //                     new CreateFilterDetailDto
        //                     {
        //                         ColumnName = "IMO",
        //                         ColumnValue = "1234",
        //                         Condition = Condition.AND,
        //                         Operation = "=",
        //                         RowIndex = 1
        //                     },
        //                     new CreateFilterDetailDto
        //                     {
        //                         ColumnName = "ShipType",
        //                         ColumnValue = "Fishing",
        //                         Condition = Condition.AND,
        //                         Operation = "=",
        //                         RowIndex = 2
        //                     }
        //                 }
        //             }
        //         }
        //    };
        //    //_filterController.ModelState.AddModelError("Name", "Required");
        //    var controllerResult =  _filterController.TryValidateModel(input);
        //    Assert.False(controllerResult);
        //    //Assert.IsType<BadRequestObjectResult>(controllerResult);
        //    //Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        //}
    }
}
