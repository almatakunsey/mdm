﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using Xunit;
using Abp.Application.Services.Dto;
using Vtmis.WebAdmin.Users;
using Vtmis.WebAdmin.Users.Dto;
using Vtmis.WebAdmin.Roles;
using Vtmis.WebAdmin.Roles.Dto;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;

namespace Vtmis.WebAdmin.Tests.Users
{
    public class UserAppService_Tests : ArrangeSuite
    {
        private readonly IUserAppService _userAppService;
        private readonly IRoleAppService _roleAppService;
        private readonly IObjectMapper _objectMapper;
        public UserAppService_Tests()
        {
            _userAppService = Resolve<IUserAppService>();
            _roleAppService = Resolve<IRoleAppService>();
            _objectMapper = Resolve<IObjectMapper>();
        }

        //[Fact]
        //public async Task GetUsers_Test()
        //{
        //    // Act
        //    var output = await _userAppService.GetAll(new PagedResultRequestDto{MaxResultCount=20, SkipCount=0} );

        //    // Assert
        //    output.Items.Count.ShouldBeGreaterThan(0);
        //}

        [Fact]
        public async Task CreateUser_Test()
        {
            // Act
            await _userAppService.Create(
                new CreateUserDto
                {
                    EmailAddress = "john@volosoft.com",
                    IsActive = true,
                    Name = "John",
                    Surname = "Nash",
                    Password = "123qwe",
                    UserName = "john.nash"
                });

            await UsingDbContextAsync(async context =>
            {
                var johnNashUser = await context.Users.FirstOrDefaultAsync(u => u.UserName == "john.nash");
                johnNashUser.ShouldNotBeNull();
            });
        }

        [Fact]
        public async Task CreateUser_WithRoleAssigned_Should_SetRole_To_A_User()
        {
            //Arrange
            await _roleAppService.Create(new CreateRoleDto
            {
                Name = "Editor",
                DisplayName = "Editor",
                IsDefault = true,
                Description = "",
                IsStatic = true

            });

            await _userAppService.Create(
                new CreateUserDto
                {
                    EmailAddress = "azam@gmail.com",
                    IsActive =true,
                    Name = "Azam", Surname="Ahamad",
                    Password="123qwe", UserName="azam",
                    RoleNames = new string[] {"Editor"}
                });

            await UsingDbContextAsync(async context =>
            {
                var user = await context.Users.FirstOrDefaultAsync(u => u.UserName == "azam");
                user.ShouldNotBeNull();

                var role = await context.Roles.FirstOrDefaultAsync(u => u.Name == "Editor");

                var userRole = await context.UserRoles.Where(c => c.RoleId == role.Id).FirstOrDefaultAsync();
                userRole.ShouldNotBeNull();
            });
        }

        [Fact]
        public async Task CreateUser_WithRoleNotExist_Should_ThrowException_RoleNotExist()
        {
            //Arrange
            await _roleAppService.Create(new CreateRoleDto
            {
                Name = "Editor",
                DisplayName = "Editor",
                IsDefault = true,
                Description = "",
                IsStatic = true,
                Permissions = new List<string> { "CanChange", "CanDelete" }
            });

            await Should.ThrowAsync<Abp.AbpException>(async () => await _userAppService.Create(
                new CreateUserDto
                {
                    EmailAddress = "azam@gmail.com",
                    IsActive = true,
                    Name = "Azam",
                    Surname = "Ahamad",
                    Password = "123qwe",
                    UserName = "azam",
                    RoleNames = new string[] { "Reader" }
                }));
        }

        [Fact]
        public async Task GetUser_WithGivenUserId_ShouldReturnUserEntity()
        {
            //Arrange
            var user = await CreateNewUser_AndAssign_Role("azam", "azam@gmail.com", "admin");

            //Act
            var userDto = await _userAppService.GetUserByIdAsync(user.Id);

            //Assert
            userDto.Name.ShouldBe("azam");

         }

        [Fact]
        public async Task GetUser_WithGivenUserId_ShouldReturnUserRoles()
        {
            //Arrange
            var user = await CreateNewUser_AndAssign_Role("azam", "azam@gmail.com", "devadmin");

            
            //Act
            var userDto = await _userAppService.GetUserByIdAsync(user.Id);

            //Assert
            userDto.RoleNames[0].ShouldBe("DEVADMIN");

        }
    }
}
