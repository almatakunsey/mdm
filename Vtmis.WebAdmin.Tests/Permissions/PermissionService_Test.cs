﻿using System;
using System.Linq;
using Vtmis.WebAdmin.Permissions;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Permissions
{
    public class PermissionService_Test : ArrangeSuite
    {
        private readonly IPermissionService _permissionService;

        public PermissionService_Test()
        {
            LoginAsDefaultTenantAdmin();
            _permissionService = Resolve<IPermissionService>();
        }

        [Fact]
        public void Should_Get_All_Permissions()
        {
            var resultWithFullName = _permissionService.Get(false,true);
            var result = _permissionService.Get(false);
            Assert.True(result.Any());
            Assert.True(resultWithFullName.Any());
            Assert.True(resultWithFullName.Where(x => string.Equals(x, "Tenant.Update.Mdm", StringComparison.CurrentCultureIgnoreCase)).Any());
            Assert.True(result.Where(x => string.Equals(x, "Tenant.Update", StringComparison.CurrentCultureIgnoreCase)).Any());
        }

        [Fact]
        public void Should_Get_All_Granted_Permissions()
        {
            var resultWithFullName = _permissionService.Get(true, true);
            var result = _permissionService.Get();
            Assert.True(result.Any());
            Assert.True(resultWithFullName.Any());
            Assert.True(resultWithFullName.Where(x => string.Equals(x, "Location.Update.Tenant", StringComparison.CurrentCultureIgnoreCase)).Any());
            Assert.True(result.Where(x => string.Equals(x, "Location.Update", StringComparison.CurrentCultureIgnoreCase)).Any());
        }       
    }
}
