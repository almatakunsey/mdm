﻿using Abp.ObjectMapping;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Cable;
using Vtmis.WebAdmin.Cable.Dto;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Cables
{
    public class CableService_Test : ArrangeSuite
    {
        private readonly IObjectMapper _objectMapper;
        private readonly ICableService _cableService;

        public CableService_Test()
        {
            _objectMapper = Resolve<IObjectMapper>();
            LoginAsDefaultTenantAdmin();
            _cableService = Resolve<ICableService>();
        }

        private async Task<ResponseCableDto> Create()
        {
            return await _cableService.CreateAsync(new RequestCreateCableDto
            {
                Name = "Cable 1",
                IsEnable = true,
                Color = "Red",
                CableDetails = new List<RequestCreateCableDetailDto>
                {
                    new RequestCreateCableDetailDto
                    {
                        Latitude = 100,
                        Longitude = 200
                    },
                    new RequestCreateCableDetailDto
                    {
                        Latitude = 300,
                        Longitude = 400
                    }
                }
            });
        }

        [Fact]
        public async Task Should_Get_All_Cables()
        {
            await Create();
            var results = await _cableService.GetAllAsync(1, 10);
            Assert.Equal(1, results.TotalItems);
            Assert.Equal(2, results.Items.FirstOrDefault().CableDetails.Count);
        }

        [Fact]
        public async Task Should_Create_Cable()
        {
            //await CreateFilter();
            await Create();
            await UsingDbContextAsync(async context =>
            {
                var route = await context.Cables.Include(x => x.CableDetails)
                        .FirstOrDefaultAsync(e => e.Name == "Cable 1"
                      && e.UserId == AbpSession.UserId);

                Assert.NotNull(route);
                Assert.Same(route.Name, "Cable 1");
                Assert.Equal(2, route.CableDetails.Count);
            });
        }

        [Fact]
        public async Task Should_Throw_Error_If_CableName_Already_Exist_In_Tenant_When_Create_Cable()
        {
            await Create();
            await Assert.ThrowsAsync<AlreadyExistException>(() => Create());
        }

        [Fact]
        public async Task Should_Update_Cable()
        {
            var newCable = await Create();
            var input = _objectMapper.Map<RequestUpdateCableDto>(newCable);
            input.CableDetails.AddRange(
                new List<RequestUpdateCableDetailDto> {
                    new RequestUpdateCableDetailDto
                    {                       
                        Latitude = 500,
                        Longitude = 600
                    },
                    new RequestUpdateCableDetailDto
                    {                       
                        Latitude = 700,
                        Longitude = 800
                    }
                });

            input.Name = "update cable 1";
            var result = await _cableService.UpdateAsync(input);
            Assert.Same("update cable 1", result.Name);
            Assert.Equal(4, result.CableDetails.Count);         
        }

        [Fact]
        public async Task Should_Get_Route_By_Id()
        {
            await Create();
            var result = await _cableService.GetAsync(1);
            Assert.NotNull(result);
            Assert.Equal(2, result.CableDetails.Count);
        }

        [Fact]
        public async Task Should_Throw_Error_If_Cable_Does_Not_Exist_When_GetCableById()
        {
            await Assert.ThrowsAsync<NotFoundException>(() => _cableService.GetAsync(1));
        }

        [Fact]
        public async Task Should_Throw_Error_If_Cable_Does_Not_Exist_When_UpdateCable()
        {
            await Assert.ThrowsAsync<NotFoundException>(() => _cableService
                .UpdateAsync(new RequestUpdateCableDto { Id = 1 }));
        }

        [Fact]
        public async Task Should_Throw_Error_If_User_Have_No_Create_Permission()
        {
            RemovePermission(2, MdmPermissionsConst.Cable_Create);
            await Assert.ThrowsAsync<ForbiddenException>(() => Create());
        }

    }
}
