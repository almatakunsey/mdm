﻿using Abp.ObjectMapping;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Model;
using Vtmis.WebAdmin.Cable;
using Vtmis.WebAdmin.Cable.Dto;
using Vtmis.WebAdmin.Web.Host.Controllers;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Cables
{ 
    public class CableController_Test : ArrangeSuite
    {
        private readonly IObjectMapper _objectMapper;
        private readonly CablesController _cablesController;
        private readonly ICableService _cableService;

        public CableController_Test()
        {
            _objectMapper = Resolve<IObjectMapper>();
            LoginAsDefaultTenantAdmin();
            _cablesController = Resolve<CablesController>();
            _cableService = Resolve<ICableService>();
        }

        private async Task<ResponseCableDto> Create()
        {
            return await _cableService.CreateAsync(new RequestCreateCableDto
            {
                Name = "Cable 1",
                IsEnable = true,
                Color = "Red",
                CableDetails = new List<RequestCreateCableDetailDto>
                {
                    new RequestCreateCableDetailDto
                    {
                        Latitude = 100,
                        Longitude = 200
                    },
                    new RequestCreateCableDetailDto
                    {
                        Latitude = 300,
                        Longitude = 400
                    }
                }
            });
        }

        [Fact]
        public async Task Get_Should_GetAllCables()
        {
            await Create();
            var controllerResult = await _cablesController.Get();

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var results = Assert.IsAssignableFrom<PaginatedList<ResponseCableDto>>(
                okResult.Value);

            Assert.Equal(1, results.TotalItems);
            Assert.Equal(2, results.Items.FirstOrDefault().CableDetails.Count);
        }

        [Fact]
        public async Task Get_Should_Get_Route_By_Id()
        {
            await Create();
            var controllerResult = await _cablesController.Get(1);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<ResponseCableDto>(
                okResult.Value);

            Assert.Same(result.Name, "Cable 1");
            Assert.Equal(2, result.CableDetails.Count);
        }

        [Fact]
        public async Task Get_Should_Return_BadRequest_If_Route_Does_Not_Exist_When_Get_Cable()
        {
            await Create();
            var controllerResult = await _cablesController.Get(999);

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

        [Fact]
        public async Task Put_Should_Update_Cable()
        {
            var newCable = await Create();
            var input = _objectMapper.Map<RequestUpdateCableDto>(newCable);
            input.CableDetails.AddRange(
                new List<RequestUpdateCableDetailDto> {
                    new RequestUpdateCableDetailDto
                    {                      
                        Latitude = 500,
                        Longitude = 600
                    },
                    new RequestUpdateCableDetailDto
                    {                      
                        Latitude = 700,
                        Longitude = 800
                    }
                });

            input.Name = "update cable 1";
            var controllerResult = await _cablesController.Put(1, input);

            var okResult = Assert.IsType<OkObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.OK, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<ResponseCableDto>(
                okResult.Value);

            Assert.Same("update cable 1", result.Name);
            Assert.Equal(4, result.CableDetails.Count);
        }

        [Fact]
        public async Task Put_Should_Return_NotFound_If_Cable_Does_Not_Exist_When_Update_Cable()
        {
            var controllerResult = await _cablesController.Put(999, 
                        new RequestUpdateCableDto { Id = 999 });
            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }
    }
}
