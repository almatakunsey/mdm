﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Filters.v2.Dto;
using Vtmis.WebAdmin.Reports;
using Vtmis.WebAdmin.Reports.Dto;
using Vtmis.WebAdmin.ReportTypes;
using Vtmis.WebAdmin.ReportTypes.Dto;
using Vtmis.WebAdmin.Web.Host.Controllers;
using Vtmis.WebAdmin.Zones;
using Vtmis.WebAdmin.Zones.Dto;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Reports
{
    public class ReportsController_Test : ArrangeSuite
    {
        private readonly ReportsController _reportsController;
        private readonly IReportService _service;
        private readonly IReportTypeService _reportTypeService;
        private readonly IZoneService _zoneService;
        private readonly IFilterAppService _filterService;

        public ReportsController_Test()
        {
            _reportsController = Resolve<ReportsController>();
            _service = Resolve<IReportService>();
            _reportTypeService = Resolve<IReportTypeService>();
            _zoneService = Resolve<IZoneService>();
            _filterService = Resolve<IFilterAppService>();
        }

        private async Task CreateArrivalDepartureReportType()
        {
            await _reportTypeService.Create(new ReportTypeDto
            {
                Name = "Arrival/Departure",
                ReportTypeNumber = "03",
                Status = VtmisType.Status.Active
            });           
        }
        private async Task CreateAlarmAndFilter()
        {
            await _zoneService.Create(new CreateZoneDto
            {
                UserId = (await GetCurrentUserAsync()).Id,
                ZoneDto = new ZoneDto
                {
                    Colour = "",
                    IsEnable = true,
                    IsFill = true,
                    Name = "Zone 1",
                    Radius = 100,
                    ZoneType = ZoneType.Circle,
                    ZoneItems = new List<ResponseZoneItemDto>
                        {
                            new ResponseZoneItemDto
                            {
                                Latitude = "100",
                                Logitude = "200"
                            }
                        }
                }
            });
            await _filterService.CreateAsync(new CreateFilterDto
            {
                Name = "New Filter",
                IsEnable = true,
                IsShowShips = true,
                FilterItems = new List<CreateFilterItemDto>
                 {
                     new CreateFilterItemDto
                     {
                         BorderColour = "red",
                         Colour = "red",
                         FilterDetails = new List<CreateFilterDetailDto>
                         {
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "MMSI",
                                 ColumnValue = "1234",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 1
                             },
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "CallSign",
                                 ColumnValue = "X01",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 2
                             }
                         }
                     },
                     new CreateFilterItemDto
                     {
                         BorderColour = "blue",
                         Colour = "blue",
                         FilterDetails = new List<CreateFilterDetailDto>
                         {
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "IMO",
                                 ColumnValue = "1234",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 1
                             },
                             new CreateFilterDetailDto
                             {
                                 ColumnName = "ShipType",
                                 ColumnValue = "Fishing",
                                 Condition = Condition.AND,
                                 Operation = "=",
                                 RowIndex = 2
                             }
                         }
                     }
                 }
            });
        }
        private async Task<ReportResponseDto> CreateArrivalDepartureReportWithCurrentUser()
        {
            return await _service.CreateWithCurrentUser(new CreateReportDto
            {
                IsEnabled = true,
                Name = "New Report",
                ReportTypeId = 1,
                TimeSpan = 2,
                TimeSpanType = TimeSpanType.Hours,                
                ReportItems = new List<CreateReportItemDto>
                {
                    new CreateReportItemDto
                    {
                        FilterId = 1,
                        ZoneId = 1                        
                    }
                }
            });            
        }

        [Fact]
        public async Task Post_Should_Create_Report()
        {
            await CreateArrivalDepartureReportType();
            await CreateAlarmAndFilter();
            
            var input = new CreateReportDto
            {
                IsEnabled = true,
                Name = "New Report",
                ReportTypeId = 1,
                TimeSpan = 2,
                TimeSpanType = TimeSpanType.Hours,
                ReportItems = new List<CreateReportItemDto>
                {
                    new CreateReportItemDto
                    {
                        FilterId = 1,
                        ZoneId = 1
                    }
                }
            };
            var controllerResult = await _reportsController.Post(input);

            var okResult = Assert.IsType<CreatedResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.Created, okResult.StatusCode);

            var result = Assert.IsAssignableFrom<ReportResponseDto>(
                okResult.Value);

            Assert.Same("New Report", result.Name);
        }

        [Fact]
        public async Task Post_Should_Return_BadRequest_If_ReportName_Already_Exist_In_Tenant_When_Create_Report()
        {
            var input = new CreateReportDto
            {
                IsEnabled = true,
                Name = "New Report",
                ReportTypeId = 1,
                TimeSpan = 2,
                TimeSpanType = TimeSpanType.Hours,
                ReportItems = new List<CreateReportItemDto>
                {
                    new CreateReportItemDto
                    {
                        FilterId = 1,
                        ZoneId = 1
                    }
                }
            };
            await CreateArrivalDepartureReportType();
            await CreateAlarmAndFilter();
            await CreateArrivalDepartureReportWithCurrentUser();
            var controllerResult = await _reportsController.Post(input);

            var okResult = Assert.IsType<BadRequestObjectResult>(controllerResult);
            Assert.Equal((int)HttpStatusCode.BadRequest, okResult.StatusCode);
        }

    }
}
