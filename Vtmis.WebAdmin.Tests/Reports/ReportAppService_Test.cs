﻿using Abp.ObjectMapping;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Exceptions;
using Vtmis.WebAdmin.Filters;
using Vtmis.WebAdmin.Filters.v2.Dto;
using Vtmis.WebAdmin.Reports;
using Vtmis.WebAdmin.Reports.Dto;
using Vtmis.WebAdmin.ReportTypes;
using Vtmis.WebAdmin.ReportTypes.Dto;
using Vtmis.WebAdmin.Zones;
using Vtmis.WebAdmin.Zones.Dto;
using Xunit;

namespace Vtmis.WebAdmin.Tests.Reports
{
    public class ReportAppService_Test : ArrangeSuite
    {
        public readonly IReportService _reportService;
        public readonly IObjectMapper _objectMapper;
        public readonly IReportTypeService _reportTypeService;
        public readonly IZoneService _zoneAppService;
        public readonly IFilterAppService _filterAppService;
        public ReportAppService_Test()
        {
            LoginAsDefaultTenantAdmin();
            _objectMapper = Resolve<IObjectMapper>();
            _reportService = Resolve<IReportService>();
            _reportTypeService = Resolve<IReportTypeService>();
            _zoneAppService = Resolve<IZoneService>();
            _filterAppService = Resolve<IFilterAppService>();
        }

        private async Task CreateArrivalDepartureReportType()
        {
            await _reportTypeService.Create(new ReportTypeDto
            {
                Name = "Arrival/Departure",
                ReportTypeNumber = "03",
                Status = VtmisType.Status.Active
            });
        }

        private async Task CreateSpeedViolationReportType()
        {
            await _reportTypeService.Create(new ReportTypeDto
            {
                Name = "Speed Violation",
                ReportTypeNumber = "07",
                Status = VtmisType.Status.Active
            });
        }

        private async Task CreateAlarmAndFilter()
        {
            await _zoneAppService.Create(new CreateZoneDto
            {
                UserId = (await GetCurrentUserAsync()).Id,
                ZoneDto = new ZoneDto
                {
                    Colour = "",
                    IsEnable = true,
                    IsFill = true,
                    Name = "Zone 1",
                    Radius = 100,
                    ZoneType = ZoneType.Circle,
                    ZoneItems = new List<ResponseZoneItemDto>
                        {
                            new ResponseZoneItemDto
                            {
                                Latitude = "100",
                                Logitude = "200"
                            }
                        }
                }
            });

            await _filterAppService.CreateAsync(
                new WebAdmin.Filters.v2.Dto.CreateFilterDto
                {
                    IsEnable = true,
                    IsShowShips = true,
                    Name = "Filter A",
                    FilterItems = new List<CreateFilterItemDto>
                    {
                        new CreateFilterItemDto
                        {
                            BorderColour = "black",
                            Colour = "red",
                            FilterDetails = new List<CreateFilterDetailDto>
                            {
                                new CreateFilterDetailDto
                                {
                                    ColumnName = "MMSI",
                                    ColumnValue = "123",
                                    Condition = Core.Common.Enums.Condition.AND,
                                    Operation = Operation.EQUALS_TO,
                                    RowIndex = 0
                                }
                            }
                        }
                    }
                }
             );
        }

        private async Task<ReportResponseDto> CreateArrivalDepartureReportWithCurrentUser()
        {
            return await _reportService.CreateWithCurrentUser(new CreateReportDto
            {
                IsEnabled = true,
                Name = "New Report",
                ReportTypeId = 1,
                TimeSpan = 2,
                TimeSpanType = TimeSpanType.Hours,
                ReportItems = new List<CreateReportItemDto>
                {
                    new CreateReportItemDto
                    {
                        FilterId = 1,
                        ZoneId = 1
                    }
                }
            });
        }
        private async Task<ReportResponseDto> CreateSpeedViolationReportWithCurrentUser()
        {
            return await _reportService.CreateWithCurrentUser(new CreateReportDto
            {
                IsEnabled = true,
                Name = "Speed Violation Report",
                ReportTypeId = 1,
                TimeSpan = 2,
                TimeSpanType = TimeSpanType.Hours,
                ReportItems = new List<CreateReportItemDto>
                {
                    new CreateReportItemDto
                    {
                        FilterId = 1,
                        ZoneId = 1,
                        MaxSpeed = 10,
                        MinSpeed = 1
                    },
                     new CreateReportItemDto
                    {
                        FilterId = 1,
                        ZoneId = 1,
                        MaxSpeed = 20,
                        MinSpeed = 2
                    }
                }
            });
        }

        [Fact]
        public async Task Should_Create_SpeedViolation_Report_With_Current_User()
        {
            await CreateSpeedViolationReportType();
            await CreateAlarmAndFilter();
            var report = await CreateSpeedViolationReportWithCurrentUser();

            // Assert         
            await UsingDbContextAsync(async context =>
            {
                var result = await context.Reports.Include(i => i.ReportItems).FirstOrDefaultAsync(x => x.UserId == AbpSession.UserId && x.IsDeleted == false &&
                                  x.Name == report.Name);

                Assert.NotNull(result);
                Assert.True(result.ReportItems.Count == 2);
                var items = result.ReportItems.OfType<WebAdmin.Reports.SpeedViolation>().ToList();
                Assert.Equal(1, items[0].MinSpeed);
                Assert.Equal(10, items[0].MaxSpeed);
                Assert.Equal(2, items[1].MinSpeed);
                Assert.Equal(20, items[1].MaxSpeed);
            });
        }

        [Fact]
        public async Task Should_Update_SpeedViolation_Report()
        {
            await CreateSpeedViolationReportType();
            await CreateAlarmAndFilter();
            await CreateSpeedViolationReportWithCurrentUser();

            var result = await _reportService.UpdateAsync(new UpdateReportDto
            {
                Id = 1,
                IsEnabled = true,
                Name = "Updated Speed Violation Report",
                ReportTypeId = 1,
                TimeSpan = 2,
                TimeSpanType = TimeSpanType.Hours,
                ReportItems = new List<UpdateReportItemDto>
                {
                    new UpdateReportItemDto
                    {
                        Id = 1,
                        FilterId = 1,
                        ZoneId = 1,
                        MaxSpeed = 30,
                        MinSpeed = 1
                    },
                     new UpdateReportItemDto
                    {
                        Id = 2,
                        FilterId = 1,
                        ZoneId = 1,
                        MaxSpeed = 20,
                        MinSpeed = 2
                    }
                }
            });
          
            var items = result.ReportItems.OfType<Vtmis.WebAdmin.Reports.Dto.SpeedViolationResponseDto>().ToList();
            Assert.Equal("Updated Speed Violation Report", result.Name);
            Assert.Equal(2, result.ReportItems.Count());
            Assert.Equal(30, items[0].MaxSpeed);
            Assert.Equal(1, items[0].MinSpeed);
            Assert.Equal(20, items[1].MaxSpeed);
            Assert.Equal(2, items[1].MinSpeed);
        }

        [Fact]
        public async Task Should_Create_Arrival_Departure_Report_With_Current_User()
        {
            await CreateArrivalDepartureReportType();
            await CreateAlarmAndFilter();
            var report = await CreateArrivalDepartureReportWithCurrentUser();

            // Assert
            var userId = (await GetCurrentUserAsync()).Id;
            var tenantId = (await GetCurrentTenantAsync()).Id;
            await UsingDbContextAsync(async context =>
            {
                var result = await context.Reports.FirstOrDefaultAsync(x => x.UserId == AbpSession.UserId && x.IsDeleted == false &&
                                x.Name == report.Name);
                Assert.NotNull(result);
            });
        }

        [Fact]
        public async Task Should_Throw_Exception_When_Create_Report_If_Report_Type_Does_Not_Exist()
        {
            await _reportService.CreateWithCurrentUser(new CreateReportDto
            {
                ReportTypeId = 99999
            }).ShouldThrowAsync<NotFoundException>();
        }

        [Fact]
        public async Task Should_Throw_Exception_When_Create_Report_If_Report_Name_Already_Exist()
        {
            await CreateArrivalDepartureReportType();
            await CreateAlarmAndFilter();
            await CreateArrivalDepartureReportWithCurrentUser();
            await _reportService.CreateWithCurrentUser(new CreateReportDto
            {
                IsEnabled = true,
                Name = "New Report",
                ReportTypeId = 1,
                TimeSpan = 2,
                TimeSpanType = TimeSpanType.Hours,
                ReportItems = new List<CreateReportItemDto>
                {
                    new CreateReportItemDto
                    {
                        FilterId = 1,
                        ZoneId = 1
                    }
                }
            }).ShouldThrowAsync<AlreadyExistException>();
        }

        [Fact]
        public async Task Should_Get_List_Of_Reports()
        {
            await CreateArrivalDepartureReportType();
            await CreateAlarmAndFilter();
            await CreateArrivalDepartureReportWithCurrentUser();

            var userId = (await GetCurrentUserAsync()).Id;
            var tenantId = (await GetCurrentTenantAsync()).Id;
            UsingDbContext(context =>
            {
                var result = context.Reports.Where(e => e.UserId == userId && e.TenantId == tenantId);
                result.Any().ShouldBeTrue();
            });
        }

        [Fact]
        public async Task Should_Update_Report()
        {
            await CreateArrivalDepartureReportType();
            await CreateAlarmAndFilter();
            await CreateArrivalDepartureReportWithCurrentUser();

            var result = await _reportService.UpdateAsync(new UpdateReportDto
            {
                Id = 1,
                IsEnabled = true,
                Name = "Updated Report",
                ReportTypeId = 1,
                TimeSpan = 2,
                TimeSpanType = TimeSpanType.Hours,
                ReportItems = new List<UpdateReportItemDto>
                {
                    new UpdateReportItemDto
                    {
                        Id = 1,
                        FilterId = 1,
                        ZoneId = 1
                    },
                     new UpdateReportItemDto
                    {
                        FilterId = 1,
                        ZoneId = 1
                    }
                }
            });

            Assert.Equal("Updated Report", result.Name);
            Assert.Equal(2, result.ReportItems.Count());
        }

        [Fact]
        public async Task Should_Delete_Report()
        {
            await CreateArrivalDepartureReportType();
            await CreateAlarmAndFilter();
            var report = await CreateArrivalDepartureReportWithCurrentUser();

            await _reportService.DeleteAsync(report.Id);
            UsingDbContext(context =>
            {
                var result = context.Reports.IgnoreQueryFilters().FirstOrDefault(e => e.Id == report.Id);
                Assert.True(result.IsDeleted);
                Assert.NotNull(result.DeletionTime);
            });
        }
    }
}
