﻿using Abp.AspNetCore;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Vtmis.Core.Common;
using Vtmis.Core.Common.Helpers;

namespace Vtmis.Hangfire.App
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            try
            {
                var hangfireDbContext = new HangfireDbContext();
                hangfireDbContext.Database.EnsureCreated();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            services.AddHangfire(config =>
                  config.UsePostgreSqlStorage(AppHelper.GetHangireDbConnectionString()));
            JobStorage.Current = new PostgreSqlStorage(AppHelper.GetHangireDbConnectionString());
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            return services.AddAbp<AbpHangfireModule>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime applicationLifetime)
        {
            app.UseAbp(options => { options.UseAbpRequestLocalization = false; });
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }
            //applicationLifetime.ApplicationStopping.Register(OnAppStopping);
            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseHangfireServer();
            app.UseHangfireDashboard();
            app.UseMvc();
        }
    }

}
