﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Castle.MicroKernel.Registration;
using Hangfire;
using StackExchange.Redis;
using System;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Request.External;
using Vtmis.WebAdmin;
using Vtmis.WebAdmin.MEH;
using Vtmis.WebAdmin.Permissions;
using Vtmis.WebAdmin.ReadOnlyDB;
using Vtmis.WebAdmin.ReadOnlyDB.Repositories;
using Vtmis.WebVessel.Tracking.Repository.Concrete;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;

namespace Vtmis.Hangfire.App
{
    [DependsOn(
      typeof(WebAdminDefaultModule))]
    public class AbpHangfireModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(AbpHangfireModule).GetAssembly());
        }
        public override void PostInitialize()
        {
            IocManager.Register<ITargetQuery, AISRepo_mongoDB>();
            IocManager.Register<MEHRequest>();
            IocManager.Register<Worker>();
            IocManager.Register<ICacheService, RedisService>();
            IocManager.Register<IMdmPermissionService, MdmPermissionService>();
            IocManager.Register<IAbpPermissionRepository, AbpPermissionRepository>();
            IocManager.IocContainer.Register(Component.For<IConnectionMultiplexer>()
                    .UsingFactoryMethod(() => ConnectionMultiplexer.Connect(AppHelper.GetRedisConnectionString())).LifestyleTransient());

            IocManager.IocContainer.Register(Component.For<MdmAdminDbContext>()
                  .UsingFactoryMethod(() => new MdmAdminDbContext()).LifestyleTransient());

            Jobs();
        }

        public void Jobs()
        {
            //BackgroundJob.Enqueue<IMEHIncidentService>(x => x.PullIncidentDataFromMEH(new DateTime(2010, 01, 01)));
            //"0 12 * * *"
            RecurringJob.AddOrUpdate<IMEHIncidentService>($"MEHIncidentReport_{AppHelper.GetEnvironmentName()}",
                 x => x.PullIncidentDataFromMEH(DateTime.Now), "10 0 * * *");

            var historyId = BackgroundJob.Enqueue<ITargetQuery>(x => x.SeedVesselHistory());
            var trackId = BackgroundJob.Enqueue<ITargetQuery>(x => x.SeedVesselTrack());
            BackgroundJob.ContinueJobWith<Worker>(historyId, x =>
                x.UpdateHistory()
            );
        }
    }

    public class Worker
    {
        public void UpdateHistory()
        {
            RecurringJob.AddOrUpdate<ITargetQuery>(x => x.UpdateVesselHistoryAndTrack(), Cron.Minutely);
        }
    }
}
