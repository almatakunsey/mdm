﻿using Akka.Actor;

namespace Vtmis.Api.MapTracking.Models
{
    public static class VesselActorSystem
    {
        public static ActorSystem ActorSystem;
        public static IActorRef Search = ActorRefs.Nobody;
        public static IActorRef Playback = ActorRefs.Nobody;
        public static IActorRef Filter = ActorRefs.Nobody;
        public static IActorRef Report = ActorRefs.Nobody;
    }
}
