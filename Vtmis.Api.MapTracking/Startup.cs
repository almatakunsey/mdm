﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Vtmis.Core.VesselEntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using Vtmis.WebVessel.Tracking.Services.Concrete;
using Microsoft.EntityFrameworkCore;
using Vtmis.Core.Common.Helpers;
using Abp.AspNetCore;
using Vtmis.WebAdmin.Identity;
using Vtmis.WebAdmin.Web.Host.Startup;
using Vtmis.WebAdmin.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Vtmis.WebVessel.Tracking.Repository.Interfaces.VTS;
using Vtmis.WebVessel.Tracking.Repository.Concrete.VTS;
using Vtmis.WebVessel.Tracking.Services.Interfaces.VTS;
using Vtmis.WebVessel.Tracking.Services.Concrete.VTS;
using Vtmis.WebVessel.Tracking.Repository.Interfaces;
using Vtmis.WebVessel.Tracking.Repository.Concrete;
using System.Data;
using System.Data.SqlClient;
using Vtmis.WebAdmin.EICS;
using Npgsql;
using StackExchange.Redis;

namespace Vtmis.Api.MapTracking
{
    public class Startup
    {
        private const string _defaultCorsPolicyName = "localhost";
        private readonly IHostingEnvironment _env;

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            this.configuration = configuration;
            _env = env;
        }

        public IConfiguration configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //ActorSystemBuilder.Execute(configuration);

            services.AddMvc(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory(_defaultCorsPolicyName));

            });
            // TODO: SSLConfig
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    services.AddHsts(options =>
            //    {
            //        options.Preload = true;
            //        options.IncludeSubDomains = true;
            //        options.MaxAge = TimeSpan.FromHours(23);
            //    });
            //    services.AddHttpsRedirection(options =>
            //    {
            //        options.RedirectStatusCode = StatusCodes.Status308PermanentRedirect;
            //        options.HttpsPort = 443;
            //    });
            //}

            services.AddCors(options =>
            {
                options.AddPolicy(_defaultCorsPolicyName, builder =>
                {
                    // App:CorsOrigins in appsettings.json can contain more than one address separated by comma.
                    builder
                        .WithOrigins(AppHelper.GetCorsOriginList().Split(",", StringSplitOptions.RemoveEmptyEntries)
                                                                         .ToArray())
                         .SetIsOriginAllowed(isOriginAllowed: _ => true)
                        .AllowAnyHeader()
                        .AllowAnyMethod();
                });
            });

            IdentityRegistrar.Register(services);
            AuthConfigurer.Configure(services, configuration);           

            var connectionString = AppHelper.GetVtsProdDbConnectionString();
            services.AddTransient<IImageService, ImageService>();
            services.AddDbContext<VtsDatabaseContext>(o => o.UseSqlServer(connectionString), ServiceLifetime.Scoped);
            services.AddSingleton<MongoDbContext>();
            services.AddTransient<ITargetQuery, AISRepo_mongoDB>();
            services.AddTransient<ICacheService, RedisService>();
            services.AddTransient<IReportingService, ReportingService>();
            services.AddTransient<IDraughtRepository, DraughtRepository>();
            services.AddTransient<IDraughtService, DraughtService>();
            services.AddTransient<IAxShipRepository, AxShipRepository>();
            services.AddTransient<IAxShipService, AxShipService>();
            services.AddTransient<IAisMapTrackingService, AisMapTrackingService>();
            services.AddTransient<IDbConnection>(ctx =>
            {
                return new SqlConnection(AppHelper.GetAisConnectionString(DateTime.Now));
            });
            services.AddTransient(ctx =>
            {
                return new NpgsqlConnection(AppHelper.GetMdmDbConnectionString());
            });
            services.AddTransient<IDapperAisRepository, DapperAisRepository>();
            services.AddTransient<IMethydroService, MethydroService>();
            services.AddTransient<IDapperAdmin, DapperAdmin>();
            services.AddTransient<IEnterExitReportService, EnterExitReportService>();
            services.AddTransient<IReportREpository, ReportRepository>();
            services.AddSingleton<IConnectionMultiplexer>(x =>
             ConnectionMultiplexer.Connect(AppHelper.GetRedisConnectionString()));
            services.AddTransient<IShipTypesRepository, ShipTypesRepository>();
            services.AddTransient<IShipTypesService, ShipTypesService>();

            return services.AddAbp<ApiMapTrackingAbpModule>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseAbp(options => { options.UseAbpRequestLocalization = false; });
            if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.LOCAL))
            {
                app.UseDeveloperExceptionPage();
            }
            // TODO: SSLConfig
            //if (_env.IsEnvironment(Core.Common.Constants.HostingEnvironment.STAGING))
            //{
            //    app.UseHsts();
            //    app.UseHttpsRedirection();
            //}

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseJwtTokenMiddleware();
            app.UseCors(_defaultCorsPolicyName);
            app.UseMvc();

        }
    }
}
