﻿using System;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.Api.MapTracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlaybacksController : AbpController
    {
        private readonly IAisMapTrackingService _aisMapTrackingService;

        public PlaybacksController(IAisMapTrackingService aisMapTrackingService)
        {           
            _aisMapTrackingService = aisMapTrackingService;
        }

        [HttpGet]
        public async Task<IActionResult> GetVesselTimeline(int mmsi, DateTime fromDateTime, DateTime toDateTime)
        {

            //var geoJson = await ActorRef.Ask<GeoJsonResponse>(new VesselTimelineRequest { MMSI = mmsi, FromDateTime = fromDateTime, ToDateTime = toDateTime });
            var geoJson = await _aisMapTrackingService.GetPlaybackData(mmsi, fromDateTime, toDateTime);
            if (geoJson.Status)
            {
                HttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type");

                return Ok(geoJson);
            }
            else
            {
                return Ok(null);
            }
               
        }        
    }

}