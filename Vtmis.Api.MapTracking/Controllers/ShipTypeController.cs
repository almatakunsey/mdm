﻿using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.WebVessel.Tracking.Services.Interfaces.VTS;

namespace Vtmis.Api.MapTracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AbpAuthorize]
    public class ShipTypeController : AbpController
    {
        private readonly IShipTypesService _shipTypesService;

        public ShipTypeController(IShipTypesService shipTypesService)
        {
            _shipTypesService = shipTypesService;
        }

        [HttpGet]
        public IActionResult GetResult()
        {
            try
            {
                return Ok(_shipTypesService.GetAll());
            }
            catch (Exception err)
            {
                return BadRequest(err);
            }
        }
    }
}
