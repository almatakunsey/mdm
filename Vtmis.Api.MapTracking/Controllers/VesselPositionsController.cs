﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Abp.AspNetCore.Mvc.Controllers;

namespace Vtmis.Api.MapTracking.Controllers
{
    [Produces("application/json")]
    [Route("api/VesselPositions")]
    public class VesselPositionsController : AbpController
    {
        //private readonly VesselDatabaseContext dbContext;

        //protected List<VesselPosition> vesselPositions { get; set; }
        private readonly IConfiguration configuration;

        public VesselPositionsController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        //[HttpPost]
        //public List<VesselPosition> Post([FromBody] VesselInfoQuery m)
        //{
        //    var dbServer = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("DB_SERVER")) ? configuration["DB_SERVER"] : Environment.GetEnvironmentVariable("DB_SERVER");

        //    var userName = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("USERNAME")) ? configuration["USERNAME"] : Environment.GetEnvironmentVariable("USERNAME");

        //    var passWord = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PASSWORD")) ? configuration["PASSWORD"] : Environment.GetEnvironmentVariable("PASSWORD");

        //    using (var dbContext = new VesselDatabaseContext(m.PostFixDbName, dbServer, userName, passWord))
        //    {
        //        //Console.WriteLine("Db ok");


        //        Console.WriteLine("RequestCurrentVesselPosition");

        //        //var vp = dbContext.ShipPositions.Where(c => c.MMSI !=-1 && c.LocalRecvTime.Value == DateTime.Parse(m.FilterDate.ToString("yyyy-MM-dd HH:mm:ss"))).
        //        //       Select(s => new VesselPosition(s.MMSI, s.Latitude.ToString(), s.Longitude.ToString(),
        //        //       si.Name, si.CallSign, s.LocalRecvTime, si.Destination, si.Vendor, si.IMO, s.ROT, s.SOG, s.COG, s.TrueHeading)).FirstOrDefault();
        //        var results = from s in dbContext.ShipPositions
        //                      join si in dbContext.ShipStatics on s.MMSI equals si.MMSI
        //                      where s.LocalRecvTime.Value == DateTime.Parse(m.Date)
        //                      select new VesselPosition(s.ShipPosID, s.MMSI, s.Latitude, s.Longitude,
        //                       si.Name, si.CallSign, s.LocalRecvTime, si.Destination, si.Vendor, si.IMO, s.ROT,
        //                       s.SOG, s.COG, s.TrueHeading, si.ShipType, s.MessageId, si.Dimension_A,
        //                                              si.Dimension_B, si.Dimension_C, si.Dimension_D, s.PositionAccuracy,
        //                                              si.PositionFixingDevice, si.VirtualAtoN,
        //                                              (s.MessageId == 21 &&
        //                                             !dbContext.AtonPosDatas.Where(x => x.MMSI == s.MMSI)
        //                                              .Any()),s.RAIM,s.RecvTime,si.ETA,si.AtoNType,si.MaxDraught,s.NavigationalStatus,);


        //        return results.ToList();

        //    }
        //}
    }

    public class VesselInfoQuery
    {
        public string Date { get; set; }
        public string PostFixDbName { get; set; }
        public string SqlConnectionString { get; set; }
        public string UserName { get; set; }
        public string PassWord { get; set; }
    }
}