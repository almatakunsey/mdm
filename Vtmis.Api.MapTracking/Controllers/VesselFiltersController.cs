﻿using Microsoft.AspNetCore.Mvc;
using Akka.Actor;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Api.MapTracking.Models;
using Vtmis.WebAdmin.Filters.v2.Dto;
using System.Collections.Generic;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.VesselEntityFrameworkCore.Models;
using Vtmis.Core.Model;
using Abp.AspNetCore.Mvc.Controllers;

namespace Vtmis.Api.MapTracking.Controllers
{
    [Produces("application/json")]
    [Route("api/VesselFilters/[action]")]
    public class VesselFiltersController : AbpController
    {
        //private readonly MapTransactionSpecification _mapTransaction;
        private readonly IImageService _imageService;


        public VesselFiltersController(IImageService imageService)
        {

            _imageService = imageService;
        }


        public async Task<IActionResult> GetAllShipPosition()
        {
            return Ok((await VesselActorSystem.Search.Ask<ResponseGetAllShipPosition>(new GetAllShipPosition())).ShipPositions);
        }

        public async Task<IActionResult> GetTargetInfoByMmsi(int? mmsi, int? currentPage = 1, int? numberOfRows = 10)
        {
            //var result = await VesselActorSystem.Search.Ask<TargetInfo>(new TargetInfo(0, null, null, null, mmsi, null, null, null, null, 0, null, null,
            //            null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null));
            return Ok();
        }

        //TODO: Refactor result using custom attribute
        [HttpPost]
        public async Task<IActionResult> GetTargetInfoByVesselId([FromBody]VesselIWithPaginatedParams input)
        {
            return Ok();
            //var result = await VesselActorSystem.Search.Ask<ShipPosition>(input.VesselId);
            //var result = await VesselActorSystem.Search.Ask<ModelResult<TargetInfo>>(input);
            //if (result.IsSuccessful)
            //{
            //    return Ok(result.Result);
            //}
            //return BadRequest(result);
        }

        public IActionResult GetVesselImage(int mmsi)
        {
            return Ok(_imageService.GetTargetImage(mmsi, ImageSize.Medium, ImageSize.Medium));
        }

        [HttpPost]
        public IActionResult GetFilterSpecification([FromBody]List<FilterItemResponse> filterItemResponses)
        {
            return Ok(VesselActorSystem.Filter.Ask<List<VesselColor>>(filterItemResponses));
        }
    }
}