﻿using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vtmis.Core.Model.DTO;
using Vtmis.WebVessel.Tracking.Services.Interfaces;

namespace Vtmis.Api.MapTracking.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    [AbpAuthorize]
    public class EnterExitReportController : AbpController
    {
        private readonly IEnterExitReportService _reportService;

        public EnterExitReportController(IEnterExitReportService reportService)
        {
            _reportService = reportService;
        }

        [HttpPost]
        public async Task<IActionResult> Get(RequestReportEnterExit input)
        {
            try
            {
                var result = await _reportService.GetResponseReportEnterExit(input);
                return Ok(result);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
