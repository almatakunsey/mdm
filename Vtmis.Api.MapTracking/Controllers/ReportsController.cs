﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Abp.AspNetCore.Mvc.Controllers;
using Vtmis.WebAdmin.Zones;
using Vtmis.WebAdmin.Reports;
using System.Threading.Tasks;
using Abp.Authorization;
using Vtmis.Api.MapTracking.Models;
using Akka.Actor;
using Vtmis.Core.ActorModel.Actors;
using Vtmis.WebAdmin.Filters;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Common.Helpers;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using Vtmis.Core.Model.DTO;

namespace Vtmis.Api.MapTracking.Controllers
{

    [Route("api/[controller]/generate")]
    [ApiController]
    [AbpAuthorize]
    public class ReportsController : AbpController
    {
        private readonly IConfiguration configuration;
        private readonly IZoneService _zoneService;
        private readonly IReportService _reportService;
        private readonly IFilterAppService _filterAppService;
        private readonly IEnterExitReportService _enterexitreportService;

        public ReportsController(IConfiguration configuration, IZoneService zoneService
            , IReportService reportService, IFilterAppService filterAppService, IEnterExitReportService enterexitreportService)
        {
            this.configuration = configuration;
            _zoneService = zoneService;
            _reportService = reportService;
            _filterAppService = filterAppService;
            _enterexitreportService = enterexitreportService;
        }

        [HttpGet]
        [Route("{reportId}")]
        public async Task<IActionResult> Get(int reportId, ReportGeneratorTimeSpan timeSpan, DateTime from, DateTime to)
        {
            try
            {
                //var dev = "2017-08-31 14:00:00";
                //from = Convert.ToDateTime(dev);
                //to = Convert.ToDateTime("2017-08-31 16:00:0");
                //timeSpan = ReportGeneratorTimeSpan.User_Defined;

                var report = await _reportService.GetByIdAsync(reportId, true);
                if (report.IsEnabled == false)
                {
                    return BadRequest("Report is disabled");
                }
                return Ok(await VesselActorSystem.Report.Ask(new ReportGenerator(from,
                   to, timeSpan, report)));
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        [HttpPost]
        public async Task<IActionResult> GetAsync(RequestReportEnterExit input)
        {
            try
            {
                var result = await _enterexitreportService.GetResponseReportEnterExit(input);
                return Ok(result);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("ArrivalDeparture")]
        public async Task<IActionResult> GetAllAsync(RequestReportEnterExit input)
        {
            try
            {
                var result = await _enterexitreportService.GetReportArrivalDeparture(input);
                return Ok(result);
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        //[HttpGet("Filter",Name ="ReportByFilter")]
        //public IActionResult GetReport()
        //{
        //    VesselActorSystem.Report.Tell(new ReportGenerator(DateTime.Today.AddDays(-10),DateTime.Today));
        //    return Ok();
        //}
    }

}