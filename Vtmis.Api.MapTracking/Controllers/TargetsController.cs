﻿using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Abp.ObjectMapping;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.Core.Common.Enums;
using Vtmis.Core.Model;
using Vtmis.Core.Model.AkkaModel.Messages.Target;
using Vtmis.Core.Model.ViewModels.Lloyds;
using Vtmis.WebAdmin.Lloyds;
using Vtmis.WebVessel.Tracking.Services.Concrete;
using Vtmis.WebVessel.Tracking.Services.Interfaces;
using Vtmis.WebVessel.Tracking.Services.Interfaces.VTS;

namespace Vtmis.Api.MapTracking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TargetsController : AbpController
    {
        private readonly ILloydsService _lloydsService;
        private readonly IObjectMapper _objectMapper;
        private readonly IDraughtService _draughtService;
        private readonly IAxShipService _axShipService;
        private readonly IAisMapTrackingService _aisMapTrackingService;

        public TargetsController(ILloydsService lloydsService, IObjectMapper objectMapper, IDraughtService draughtService, IAxShipService axShipService, IAisMapTrackingService aisMapTrackingService)
        {
            _lloydsService = lloydsService;
            _objectMapper = objectMapper;
            _draughtService = draughtService;
            _axShipService = axShipService;
            _aisMapTrackingService = aisMapTrackingService;
        }       

        [AbpAuthorize]
        public async Task<IActionResult> Get(int? imo, int? mmsi, string callSign, string targetName, int pageIndex = 1, int pageSize = 10)
        {

            if (imo == null && mmsi == null && string.IsNullOrEmpty(callSign) && string.IsNullOrEmpty(targetName))
            {
                return BadRequest("Invalid paramaters");
            }
            if (imo != null)
            {
                if (imo.Value.ToString().Length < 3)
                {
                    return BadRequest("IMO must be minimum of 3 characters");
                }
            }
            if (mmsi != null)
            {
                if (mmsi.Value.ToString().Length < 3)
                {
                    return BadRequest("MMSI must be minimum of 3 characters");
                }
            }
            if (!string.IsNullOrEmpty(callSign))
            {
                if (callSign.Length < 3)
                {
                    return BadRequest("Call Sign must be minimum of 3 characters");
                }
            }
            if (!string.IsNullOrEmpty(targetName))
            {
                if (targetName.Length < 3)
                {
                    return BadRequest("Target Name must be minimum of 3 characters");
                }
            }
            //var response = await VesselActorSystem.Search.Ask(new RequestVesselInquiry(mmsi, imo, callSign, targetName, pageIndex, pageSize));
            var response = await _aisMapTrackingService.Get(mmsi, imo, callSign, targetName, pageIndex, pageSize);
            if (response != null)
            {
                foreach (var item in response.Items)
                {
                    var l = _objectMapper.Map<ResponseLloyds>(await _lloydsService.GetAsync(item.IMO, item.CallSign, item.TargetName));
                    item.Lloyds = l ?? new ResponseLloyds();
                }
                return Ok(response);
            }

            return Ok(new PaginatedList<ResponseVesselInquiry>().SetNull());
        }

        [Route("ddms/{mmsi}")]
        [AbpAuthorize]
        public async Task<IActionResult> DDMS(int mmsi)
        {
            return Ok(await _aisMapTrackingService.GetDDMSInfoAsync(mmsi));
        }

        [Route("rms/{mmsi}")]
        //[AbpAuthorize]
        public async Task<IActionResult> RMS(int mmsi)
        {
            try
            {
                var result = await _aisMapTrackingService.GetRMSInfoAsync(mmsi);
                return Ok(result);
            }
            catch (Exception err)
            {

                throw;
            }
           
        }

        [Route("rms/{mmsi}/[action]")]
        [AbpAuthorize]
        public async Task<IActionResult> Historical(int mmsi, DateTime startDate, DateTime endDate)
        {
            try
            {
                return Ok(await _aisMapTrackingService.RmsStatisticAsync(mmsi, startDate, endDate));
                //return Ok(await VesselActorSystem.Search.Ask(new RequestRmsHistorical(mmsi, startDate, endDate)));
            }
            catch (Exception err)
            {
                return BadRequest(err);
            }

        }

        [Route("aton")]
        [AbpAuthorize]
        public async Task<IActionResult> Aton(TargetClass type, int pageIndex = 1, int pageSize = 10)
        {
            try
            {
                var result = await _aisMapTrackingService.GetAtonList(new RequestAton(type, pageIndex, pageSize));
                return Ok(result);
            }
            catch (Exception err)
            {

                throw;
            }
          
            //return Ok(await VesselActorSystem.Search.Ask(new RequestAton(type, pageIndex, pageSize)));
        }

        [Route("ddms/{mmsi}/Historical")]
        [AbpAuthorize]
        public async Task<IActionResult> DDMS(int mmsi, DateTime startdate, DateTime enddate)
        {
            return Ok(await _aisMapTrackingService.GetDDMSHistorical(mmsi, startdate, enddate));
        }

        [Route("[action]/{mmsi}")]
        public async Task<IActionResult> Lighthouse(int mmsi)
        {
            return Ok(await _aisMapTrackingService.GetLighthouseAsync(mmsi));
        }
    }
}
