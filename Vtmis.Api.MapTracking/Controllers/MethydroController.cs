﻿using Abp.AspNetCore.Mvc.Controllers;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Vtmis.WebVessel.Tracking.Services.Concrete;

namespace Vtmis.Api.MapTracking.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]   
    public class MethydroController : AbpController
    {
        private readonly IMethydroService _methydroService;

        public MethydroController(IMethydroService methydroService)
        {
            _methydroService = methydroService;
        }

        [HttpGet]
        [Route("pub/station")]
        [AbpAllowAnonymous]
        public async Task<IActionResult> PubGet()
        {
            var result = await _methydroService.GetAllMethydroStationAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("pub/{mmsi}")]
        [AbpAllowAnonymous]
        public async Task<IActionResult> PubGet(int mmsi, DateTime? fromDate = null, DateTime? toDate = null)
        {
            if (fromDate == null && toDate == null)
            {
                toDate = DateTime.Now;
                fromDate = toDate.Value.AddMinutes(-5);
            }

            if (fromDate > toDate)
            {
                return BadRequest("From Date cannot be higher than To Date");
            }

            if (fromDate == null || toDate == null)
            {
                return BadRequest("Invalid date");
            }

            var result = await _methydroService.GetInstrumentDataByMmsiDateRangeAsync(mmsi, fromDate.Value, toDate.Value);
            return Ok(result);
        }

        [HttpGet]
        [Route("pub/{mmsi}/historical")]
        [AbpAllowAnonymous]
        public async Task<IActionResult> PubHistorical(int mmsi, DateTime startDate, DateTime endDate)
        {
            try
            {
                return Ok(await _methydroService.GetWeatherHistoricalAsync(mmsi, startDate, endDate));
            }
            catch (Exception err)
            {
                throw err;
            }

        }

        [HttpGet]
        [Route("station")]
        [AbpAuthorize]
        public async Task<IActionResult> Get()
        {
            var result = await _methydroService.GetAllMethydroStationAsync();
            return Ok(result);
        }

        [HttpGet]
        [Route("{mmsi}")]
        [AbpAuthorize]
        public async Task<IActionResult> Get(int mmsi, DateTime? fromDate = null, DateTime? toDate = null)
        {
            if (fromDate == null && toDate == null)
            {
                toDate = DateTime.Now;
                fromDate = toDate.Value.AddMinutes(-5);
            }

            if (fromDate > toDate)
            {
                return BadRequest("From Date cannot be higher than To Date");
            }

            if (fromDate == null || toDate == null)
            {
                return BadRequest("Invalid date");
            }

            var result = await _methydroService.GetInstrumentDataByMmsiDateRangeAsync(mmsi, fromDate.Value, toDate.Value);
            return Ok(result);
        }

        [HttpGet]
        [Route("{mmsi}/[action]")]
        [AbpAuthorize]
        public async Task<IActionResult> Historical(int mmsi, DateTime startDate, DateTime endDate)
        {
            try
            {
                return Ok(await _methydroService.GetWeatherHistoricalAsync(mmsi, startDate, endDate));
            }
            catch (Exception err)
            {
                throw err;
            }
          
        }
    }
}
