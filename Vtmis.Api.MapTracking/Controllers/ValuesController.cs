﻿using System;
using System.Collections.Generic;
using Abp.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace Vtmis.Api.MapTracking.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : AbpController
    {

        [HttpGet]
        [Route("Date")]
        public IActionResult Date()
        {
            return Ok(new
            {
                localtime = DateTime.Now,
                localtime_withKindLocal = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local),
                localtime_withKindLocalApply = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local).ToLocalTime(),
                utcTime = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Local).ToUniversalTime()
            });
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
