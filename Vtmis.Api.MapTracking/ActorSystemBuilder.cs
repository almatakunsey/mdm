﻿using Akka.Actor;
using Akka.Configuration;
using Akka.Routing;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Net.Sockets;
using Vtmis.Api.MapTracking.Models;
using Vtmis.Core.ActorModel.Actors;
using Vtmis.Core.Common;
using Vtmis.Core.Common.Constants;
using Vtmis.Core.Common.Helpers;
using Vtmis.Core.Model.AkkaModel;

namespace Vtmis.Api.MapTracking
{
    public class ActorSystemBuilder
    {
        public static ActorSystem Execute(IConfiguration configuration)
        {
            var port = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PORT")) ? configuration["PORT"] : Environment.GetEnvironmentVariable("PORT");
            var seedPort = AppHelper.GetSeedPort();
            var seed = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("SEED")) ? configuration["SEED"] : Environment.GetEnvironmentVariable("SEED");
            //var seed = ".";
            var hostName = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("HOSTNAME")) ? configuration["HOSTNAME"] : Environment.GetEnvironmentVariable("HOSTNAME");

            string environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            var dbServer = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("DB_SERVER")) ? configuration["DB_SERVER"] : Environment.GetEnvironmentVariable("DB_SERVER");

            var dbDateEnv = Environment.GetEnvironmentVariable("DB_DATE");

            var userName = Environment.GetEnvironmentVariable("USERNAME");

            var passWord = Environment.GetEnvironmentVariable("PASSWORD");
            
            var publicHostName = string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PUBLIC_HOSTNAME")) ?
              configuration["PUBLIC_HOSTNAME"] : Environment.GetEnvironmentVariable("PUBLIC_HOSTNAME");

            if (string.Equals(AppHelper.GetEnvironmentName(), HostingEnvironment.LOCAL, StringComparison.CurrentCultureIgnoreCase) == false)
            {
                var name = System.Net.Dns.GetHostName(); // get container id
                var ip = System.Net.Dns.GetHostEntry(name).AddressList.FirstOrDefault(x => x.AddressFamily == AddressFamily.InterNetwork);
                publicHostName = ip.ToString();
            }
            DateTime dbDate = DateTime.Now;

            if (!string.IsNullOrEmpty(dbDateEnv))
            {
                dbDate = Convert.ToDateTime(dbDateEnv);
                Console.WriteLine("DB_DATE " + dbDate);
            }

            new AkkaLoggerConfig().SetupLog("ApiMapTracking");
            var config = ConfigurationFactory.ParseString(@"
                akka {
                    loglevel=INFO,
                    loggers=[""Akka.Logger.Serilog.SerilogLogger, Akka.Logger.Serilog""],
                    actor {
                        provider = ""Akka.Cluster.ClusterActorRefProvider, Akka.Cluster""
                   
                        deployment {
                           
                            /search {
                                router = round-robin-group
                                routees.paths = [""/user/search""]
                                nr-of-instances = 4
                                       cluster {
                                          enabled = on
                                          max-nr-of-instances-per-node = 2
                                          #allow-local-routees = on
                                          use-role = search
                                        }
                            }

                            /playback {
                                router = round-robin-group
                                routees.paths = [""/user/playback""]
                                nr-of-instances = 4
                                       cluster {
                                          enabled = on
                                          max-nr-of-instances-per-node = 2
                                          #allow-local-routees = on
                                          use-role = search
                                        }
                            }

                            /filter {
                               router = round-robin-pool
                               nr-of-intances = 4
                               cluster {
                                  enabled = on
                                  max-nr-of-instances-per-node = 2
                                  use-role = search
                               }
                            }

                            /report {
                               router = round-robin-pool
                               nr-of-intances = 4
                               cluster {
                                  enabled = on
                                  max-nr-of-instances-per-node = 2
                                  use-role = event
                               }
                            }
                        }
                    }

                    remote {
                           maximum-payload-bytes = 30000000 bytes
                           dot-netty.tcp {	
                                maximum-frame-size = 5242880b
                                #bind-hostname= 0.0.0.0
                                public-hostname = " + publicHostName + @"
								hostname = " + hostName + @"
								port = " + port + @"
                               
							}
                          }

                    cluster {
				            #will inject this node as a self-seed node at run-time
					        seed-nodes = [""akka.tcp://vtmis@" + seed + @":"+ seedPort + @"""]
				            roles = [web]
			        }

             } #akka           
            ");

            //var builder = new ContainerBuilder();

            //builder.Register(ctx =>
            //{
            //    return new AppInfoModel
            //    {
            //        DbDate = dbDate,
            //        Environment = environmentName,
            //        Password = passWord,
            //        ServerName = dbServer,
            //        Username = userName
            //    };
            //}).As<AppInfoModel>().InstancePerLifetimeScope();
            //builder.RegisterType<VesselReportSupervisorActor>();

            //var container = builder.Build();

            ActorSystem actorSystem = ActorSystem.Create("vtmis", config);

            //var resolver = new AutoFacDependencyResolver(container, actorSystem);

            VesselActorSystem.ActorSystem = actorSystem;
            VesselActorSystem.Search = actorSystem.ActorOf(Props.Empty.WithRouter(FromConfig.Instance), "search");
            //VesselActorSystem.Target = actorSystem.ActorOf(Props.Empty.WithRouter(FromConfig.Instance), "TargetQueryActor");
            VesselActorSystem.Playback = actorSystem.ActorOf(Props.Empty.WithRouter(FromConfig.Instance), "playback");
            VesselActorSystem.Filter = actorSystem.ActorOf(Props.Create(() => new VesselFilterSupervisorApi(dbDate, dbServer, userName, passWord, environmentName)).WithRouter(FromConfig.Instance), "filter");
            VesselActorSystem.Report = actorSystem.ActorOf(Props.Create(() => new VesselReportSupervisorActor(new AppInfoModel {
                DbDate = dbDate,
                Environment = environmentName,
                Password = passWord,
                ServerName = dbServer,
                Username = userName
            })).WithRouter(FromConfig.Instance), "report");

            return actorSystem;

            //services.AddSingleton<ActorSystem>(serviceProvider => actorSystem);
            //services.AddSingleton<VesselActorSystem>(v => VesselActorSystem);
            //services.AddSingleton<IActorRef>(VesselActorSystem.Playback);

            //services.AddSingleton<MapTransactionSpecification>(mt => new MapTransactionSpecification());
           
        }
    }
}
