﻿using Akka.Actor;
using Akka.Event;
using Akka.Routing;
using GeoJSON.Net.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using Vtmis.Core.Model.AkkaModel.Messages;
using Vtmis.Core.Model.AkkaModel.Messages.Playback;

namespace Vtmis.Api.MapTracking.Actor
{

    public class ClientPlaybackActor : ReceiveActor
    {
        private readonly ILoggingAdapter _logger;
        IActorRef playBack;
        IActorRef childActor;

        protected override void PreStart()
        {
            childActor = Context.ActorOf(Props.Create(() => new ClientPlayBackChildActor(playBack)).WithRouter(new RoundRobinPool(100)));

        }

        public ClientPlaybackActor(IActorRef playBackActor)
        {
            _logger = Context.GetLogger();
            this.playBack = playBackActor;

            Receive<VesselTimelineRequest>(m =>
            {
                childActor.Tell(m, Sender);
            });

            Receive<VesselsByFromDateRequest>(m =>
            {
                childActor.Tell(m, Sender);
            });
        }


        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(err =>
            {
                if (err is HttpRequestException)
                {
                    Console.WriteLine("HttpRequest Restart started...");
                    return Directive.Restart;
                }
                else if (err is AskTimeoutException)
                {
                    Console.WriteLine("Timeout Restart started...");
                    return Directive.Restart;
                }
                else
                {
                    Console.WriteLine("Else restart started...");
                    return Directive.Restart;
                }
            });
        }
    }

    public class ClientPlayBackChildActor : ReceiveActor
    {
        private readonly ILoggingAdapter _logger;
        IActorRef playBack;

        public ClientPlayBackChildActor(IActorRef playBackActor)
        {
            _logger = Context.GetLogger();
            this.playBack = playBackActor;

            Context.SetReceiveTimeout(TimeSpan.FromSeconds(3));

            List<TimePosition> listTimePosition = new List<TimePosition>();

            var geoJsonResponse = new GeoJsonResponse();

            //Return GeoJsonResponse
            ReceiveAsync<VesselTimelineRequest>(async m =>
            {
                _logger.Info("Start VesselTimelineRequest");
                try
                {
                    listTimePosition.Clear();

                    var result = await playBack.Ask<VesselTimelineResponse>(m);

                    if (result.Status)
                    {
                        listTimePosition.AddRange(result.ShipPositions.Select(s => new TimePosition
                        {
                            Point =
                            new Point(new Position(s.Latitude, s.Longitude)),
                            UnixTime = new DateTimeOffset(s.LocalRecvTime.Value).ToUnixTimeMilliseconds()
                        }));

                        var multiPoint = new MultiPoint(listTimePosition.Select(c => c.Point));

                        var timeArray = listTimePosition.Select(c => c.UnixTime).ToArray();
                        geoJsonResponse.Status = true;
                        geoJsonResponse.type = "Feature";
                        geoJsonResponse.geometry = multiPoint;
                        geoJsonResponse.properties.Time = timeArray;
                        Sender.Tell(geoJsonResponse);
                        _logger.Info("End VesselTimelineRequest");
                    }
                    else
                    {
                        geoJsonResponse.Status = false;
                        Sender.Tell(geoJsonResponse);
                    }
                }
                catch (Exception err)
                {
                    _logger.Error(err, err.Message);
                    geoJsonResponse.Status = false;
                    Sender.Tell(geoJsonResponse);
                    //throw err;
                }
               
                              
            });

            //return VesselsByFromDateResponse to Sender
            ReceiveAsync<VesselsByFromDateRequest>(async m =>
            {

                var vesselsByFromDate = await playBack.Ask<VesselsByFromDateResponse>(m);
                Sender.Tell(vesselsByFromDate);
                
                //Context.SetReceiveTimeout(null);
            });

            //Receive<ReceiveTimeout>(timeout =>
            //{
            //    Console.WriteLine("Receive timeout");

            //    Context.SetReceiveTimeout(null);
            //    Sender.Tell(new VesselsByFromDateResponse { Status = "TimeOut" });
            //    //Context.Stop(Context.Self);
            //});
        }
    }
}