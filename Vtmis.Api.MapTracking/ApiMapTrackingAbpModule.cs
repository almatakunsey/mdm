﻿using Abp.Modules;
using Abp.Reflection.Extensions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Vtmis.WebAdmin;
using Vtmis.WebAdmin.Configuration;

namespace Vtmis.Api.MapTracking
{
    [DependsOn(
       typeof(WebAdminDefaultModule))]
    public class ApiMapTrackingAbpModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ApiMapTrackingAbpModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ApiMapTrackingAbpModule).GetAssembly());
        }
    }
}
