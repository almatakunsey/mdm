﻿using Akka.Actor;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Vtmis.Signalr.ClientServices
{
    public class Program
    {
        public static ManualResetEvent Wait;
        static HubConnection connection;


        public static void Main(string[] args)
        {
            using (ActorSystem system = ActorSystem.Create("vtmissignalr"))
            {
                Wait = new ManualResetEvent(false);
                var address = args[0];

                //system.ActorOf(Props.Create(() => new SignalRActor(address)));
                system.ActorOf(Props.Create<SignalRActor>(address));
                Wait.WaitOne();
            }
            using (ActorSystem system = ActorSystem.Create("vtmis"))
            {
                //var address = Environment.GetEnvironmentVariable("SEED");
                //var signalRActor = system.ActorOf(Props.Create(() => new SignalRActor(address)));
             
                //Console.ReadLine();
               
            }
        }

        private static async Task Connection_Closed(Exception arg)
        {
            try
            {
                Console.WriteLine("Connection close");
                await Task.Delay(2000);
                await Start();
            }
            catch {

              
            }
        }

        public static async Task Start()
        {
            try
            {
                Console.WriteLine("Connect...");
                await connection.StartAsync();
                await connection.InvokeAsync("StartServer", "");
               
            }
            catch(HttpRequestException err)
            {
                //Console.WriteLine(err.Message);
                await Task.Delay(2000);
                //Console.WriteLine("Retry...");
                await Start();
            }

        }
    }
}
