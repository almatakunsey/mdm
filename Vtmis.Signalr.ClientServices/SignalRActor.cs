﻿using Akka.Actor;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Net.Http;

namespace Vtmis.Signalr.ClientServices
{
    public class SignalRActor : ReceiveActor
    {
        protected IActorRef ActorRef;
        protected string Url;

        protected override void PreStart()
        {
            Console.WriteLine("SignalRActor PreStart");
           

        }

        public SignalRActor(string url)
        {
            Url = url;
            ActorRef = Context.ActorOf(Props.Create(() => new SignalRConnect(this.Url)));
            Console.WriteLine("SignalRActor");

        }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(err =>
            {
                if (err is HttpRequestException)
                {
                    Console.WriteLine("Restart started...");
                    return Directive.Restart;
                }
                else
                {
                    Console.WriteLine("Else restart started...");
                    return Directive.Restart;
                }


            });
        }
    }

    public class SignalRConnect : ReceiveActor
    {
        HubConnection connection;
        string Url;

        protected override void PreStart()
        {
            Console.WriteLine("SignalRConnect pre start ...");
            Self.Tell(new Connect { Url = Url });
        }

        public SignalRConnect(string url)
        {
            Url = url;

            ReceiveAsync<Connect>(async m =>
            {
                Url = m.Url;

                if (connection == null)
                {
                    // TODO: SSLConfig
                    connection = new HubConnectionBuilder()
                       .WithUrl($"http://{m.Url}/VesselGeoLocationHub")
                       .Build();
                    Console.WriteLine("Server trying to cnnect ...");
                    await connection.StartAsync();

                    Console.WriteLine($"({DateTime.Now}) || Server Connected ...");
                    await connection.InvokeAsync("StartServer", "");
                }
            });
        }


    }

    public class Connect
    {
        public string Url { get; set; }
    }
}
